public class SVMX_DynamicPMConfigTrgHandler extends SVMX_TriggerHandler{

    private list<SVMX_Dynamic_PM_Configuration__c > newSCList;
    private list<SVMX_Dynamic_PM_Configuration__c > oldSCList;
    private Map<Id, SVMX_Dynamic_PM_Configuration__c > newSCMap;
    private Map<Id, SVMX_Dynamic_PM_Configuration__c > oldSCMap;

    public SVMX_DynamicPMConfigTrgHandler(){
        
        this.newSCList = (list<SVMX_Dynamic_PM_Configuration__c>) Trigger.new;
        this.oldSCList = (list<SVMX_Dynamic_PM_Configuration__c>) Trigger.old;
        this.newSCMap = (Map<Id, SVMX_Dynamic_PM_Configuration__c>) Trigger.newMap;
        this.oldSCMap = (Map<Id, SVMX_Dynamic_PM_Configuration__c>) Trigger.oldMap;
    }

     public override void afterInsert(){
         set<Id> scIds = new set<Id>();
         List<SVMXC__Service_Contract__c> scList = new List<SVMXC__Service_Contract__c>();
           for(SVMX_Dynamic_PM_Configuration__c wo : newSCList ){
                if(wo.SVMX_Service_Maintenance_Contract__c != null ){
                    scIds.add(wo.SVMX_Service_Maintenance_Contract__c);
                
                }
        }
        
        for(Id sc:scIds){
            SVMXC__Service_Contract__c scl = new SVMXC__Service_Contract__c(Id=sc);
            scList.add(scl);
        }
        system.debug(' ## runonce ' +SVMX_DefinitionClass.runOnceDynamicPM);
        
        if(scList.size() > 0 && SVMX_DefinitionClass.runOnceDynamicPM == true)
            SVMX_ServiceContractServiceManager.dynamicPMPlan(scList);
     
     }

}