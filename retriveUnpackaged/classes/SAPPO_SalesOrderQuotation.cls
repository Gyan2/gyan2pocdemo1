/**
 * Created by trakers on 12.07.18.
 */

public with sharing class SAPPO_SalesOrderQuotation extends SAPPO_SalesOrderAbstract{
    // In test : QkNfU0ZPUkNFX0dMT0JBTDphU3QzbTM0cFJAZjI=
    public SAPPO_SalesOrderQuotation(){
        super();
    }

    /**********************************************************************
     *
     *          QUOTE Create
     *
     ***********************************************************************/
     //SalesOrderCreateRequest

    public Response sendQuotation(Wrapper wrapper){
        SAPPO_SalesOrderRequest.SalesOrderCreateRequest request = new SAPPO_SalesOrderRequest.SalesOrderCreateRequest();
        Boolean sapResponse = false;


        // Header
        request.MessageHeader   = new SAPPO_CommonDataTypes.BusinessDocumentMessageHeader();
        request.MessageHeader.ID = new SAPPO_CommonDataTypes.BusinessDocumentMessageID();
        request.MessageHeader.ID.Content = (Id)wrapper.quote.get('Id'); //wrapper.quote.Id;

        request.MessageHeader.PositiveAckRequestedIndicator = 'true';
        request.MessageHeader.NegativeAckRequestedIndicator = 'true';
        request.MessageHeader.BusinessScope = new List<SAPPO_CommonDataTypes.BusinessScope>();
        request.MessageHeader.BusinessScope.add(new SAPPO_CommonDataTypes.BusinessScope('SFG',wrapper.quote.ExternalQuoteNumber__c,'Quote'));



        try{
            request.SalesOrder = this.createQuotationSalesOrder(wrapper);
            sapResponse = new SAPPO_SalesOrderRequest.SalesOrderOutPort().CreateSync(request.MessageHeader,request.SalesOrder);
        }catch(Exception e){
            Response result = new Response();
                     result.data = new List<Map<String,String>>();
                     result.status = 400;
                     result.message = 'Line '+e.getLineNumber()+' | '+e.getMessage();
            return result;
        }

        //System.debug(JSON.serialize(this.response));
        return this.handleCreationQuotationResponse(sapResponse);

    }

    private SAPPO_SalesOrderRequest.SalesOrderCreateRequestSalesOrder  createQuotationSalesOrder(Wrapper wrapper){
        /*
            Similar to the simulation but with more data (simulation is only made to get the price)
        */
        List<String> languages = wrapper.quote.Languages__c.split(';');
        List<Text__c> quoteText = [select id,Text__c,Language__C,SAPKey__c from Text__c where QuoteRef__c = : wrapper.quote.Id and Language__C in: languages];
        Map<String,List<Text__c>> mapQuoteLineItemsText = new Map<String,List<Text__c>>();
        for(Text__c text : [select id,Text__c,Language__C,SAPKey__c,QuoteLineItemRef__c from Text__c where QuoteLineItemRef__c in: wrapper.quoteLineItems and Language__C in: languages]){
            if(!mapQuoteLineItemsText.containsKey(text.QuoteLineItemRef__c)){
                mapQuoteLineItemsText.put(text.QuoteLineItemRef__c,new List<Text__c>());
            }
            mapQuoteLineItemsText.get(text.QuoteLineItemRef__c).add(text);

        }

        SAPPO_SalesOrderRequest.SalesOrderCreateRequestSalesOrder salesOrder = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestSalesOrder();

        // salesOrder.ID = null;
        salesOrder.ProcessingTypeCode = 'SV';
        salesOrder.BusinessProcessVariantTypeCode = 'SV08';

        // BUYER PARTY
        salesOrder.BuyerParty = new SAPPO_SalesOrderRequest.SalesDocumentRequestBuyerParty();
        salesOrder.BuyerParty.InternalID = new SAPPO_CommonDataTypes.PartyInternalID();
        salesOrder.BuyerParty.InternalID.Content = wrapper.account.SAPNumber__c;

        // SELLER PARTY
        salesOrder.SellerParty = new SAPPO_SalesOrderRequest.SalesDocumentRequestSellerParty();
        salesOrder.SellerParty.StandardID = new SAPPO_CommonDataTypes.PartyStandardID();
        salesOrder.SellerParty.StandardID.Content = '99999999999CH';//wrapper.quote.GLN__c; // GLN of the seller
        salesOrder.SellerParty.StandardID.SchemeAgencyID = '009';

        // DATE TERMS
        salesOrder.DateTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestDateTerms();
        salesOrder.DateTerms.RequestDate = String.ValueOf(wrapper.quote.ScheduledDate__c);

        // PRICING TERMS
        salesOrder.PricingTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestPricingTerms();
        salesOrder.PricingTerms.CurrencyCode = wrapper.account.CurrencyIsoCode;

        // TEXT COLLECTION
        salesOrder.TextCollection = new SAPPO_CommonDataTypes.BusinessDocumentTextCollection(); // todo: add the text collection
        salesOrder.TextCollection.Text = new List<SAPPO_CommonDataTypes.BusinessDocumentTextCollectionText>();

        for(Text__c text : quoteText){
            SAPPO_CommonDataTypes.Text content = new SAPPO_CommonDataTypes.Text(text.Text__c,text.Language__c,text.Language__c);
            salesOrder.TextCollection.Text.add(new SAPPO_CommonDataTypes.BusinessDocumentTextCollectionText(text.SAPKey__c,content));
        }


        // Create List to store the items (Work Details). This must be done outside the loop below, otherwise the list will be overwritten
        salesOrder.Item = new List<SAPPO_SalesOrderRequest.SalesOrderCreateRequestItem>();
        Integer index = 1;
        for(QuoteLineItem quoteLineItem : wrapper.quoteLineItems){
            SAPPO_SalesOrderRequest.SalesOrderCreateRequestItem item = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestItem();


            item.ID = String.valueOf(index); // We can't use the position because the serviceMax team is using it too: max Char = 6 digits

            // DESCRIPTION
            item.Description = new SAPPO_CommonDataTypes.SHORT_Description(); // todo: get the correct description and language to put here
            item.Description.Content = 'blabla';
            item.Description.LanguageCode = 'EN';

            // Transaction Document Reference (QuoteLineItems IDs)
            item.BusinessTransactionDocumentReference = new List<SAPPO_SalesOrderRequest.SalesDocumentRequestItemBusiTransDocRef>();
            item.BusinessTransactionDocumentReference.add(
                    new SAPPO_SalesOrderRequest.SalesDocumentRequestItemBusiTransDocRef(quoteLineItem.Id,'externalId','030') // todo: Create ExternalId on quoteLineItem
            );

            // PRODUCT
            item.Product = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemProduct();
            item.Product.InternalID = new SAPPO_CommonDataTypes.ProductInternalID();
            item.Product.InternalID.Content = quoteLineItem.Product2.SAPNumber__c;

            //DATE TERMS
            item.DateTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemDateTerms();
            item.DateTerms.RequestDate = String.valueOf(wrapper.quote.ScheduledDate__c);

            // PRICING

            item.PriceComponent = new List<SAPPO_EsmEdt.PriceComponent>(); // todo: add the price

            //item.PriceComponent.add(new SAPPO_EsmEdt.PriceComponent(null,null));
            //item.PriceComponent.add(new SAPPO_EsmEdt.PriceComponent(null,null));
            //item.PriceComponent.add(new SAPPO_EsmEdt.PriceComponent(null,null));


            // TEXT COLLECTION

            item.TextCollection = new SAPPO_CommonDataTypes.BusinessDocumentTextCollection(); // todo: add the text collection
            item.TextCollection.Text = new List<SAPPO_CommonDataTypes.BusinessDocumentTextCollectionText>();

            for(Text__c text : mapQuoteLineItemsText.get(quoteLineItem.Id)){
                SAPPO_CommonDataTypes.Text content = new SAPPO_CommonDataTypes.Text(text.Text__c,text.Language__c,text.Language__c);
                item.TextCollection.Text.add(new SAPPO_CommonDataTypes.BusinessDocumentTextCollectionText(text.SAPKey__c,content));
            }



            // QUANTITY
            item.TotalValues = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemTotalValues();
            item.TotalValues.RequestedQuantity = new SAPPO_CommonDataTypes.Quantity();
            item.TotalValues.RequestedQuantity.Content    = String.valueOf(quoteLineItem.Quantity);
            item.TotalValues.RequestedQuantity.UnitCode   = quoteLineItem.Product2.QuantityUnitOfMeasure;

            SalesOrder.Item.add(item);

            index++;
        }

        return salesOrder;
    }

    private Response handleCreationQuotationResponse(Boolean response){

        Response result = new Response();
                 result.data = new List<Map<String,String>>();
                 result.status = response ? 200 : 400;
                 result.message = response ? null : 'ACK is false';

        return result;
    }

    /*************************************************************************************
    *
    *          METHODS
    *
    *************************************************************************************/

    public override void handleExceptions(Object paramResponse,Object paramResult,Exception e){

    }

    public override List<String> validateMandatoryFields(Object obj){

       return null;
    }

    protected override Map<String,List<String>> getMandatoryFields(){
        return null;
    }

    /*************************************************************************************
    *
    *          CLASSES
    *
    *************************************************************************************/

    public class Wrapper {
        public Account account; // for Informations
        public Quote quote; // for Informations
        public List<QuoteLineItem> quoteLineItems; // Updatable
    }


}