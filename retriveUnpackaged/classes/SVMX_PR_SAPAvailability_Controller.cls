/************************************************************************************************************
Description: Controller for SVMX_PR_SAPAvailability VF page.
 
Author: Ranjitha Shenoy
Date: 26-02-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

public class SVMX_PR_SAPAvailability_Controller {

    public ID partReqId{get;set;}
    public Map<id,SVMXC__Parts_Request_Line__c> partReqLineMap;
    public List<WrapPR> partReqLineWrap {get;set;}
    public SVMXC__Parts_Request__c pr {get;set;}
    public string prCountry{get;set;}
    public string state {get;set;}
    public string city {get;set;}
    public string zip {get;set;}
    public string country {get;set;}
    public string street {get;set;}
    public id shippingToLoc {get;set;}
    public id reqAtLoc {get;set;}
    public static List<SVMXC__Product_Stock__c> psList;
    public set<id> productIds;
    public string available = Label.available; 
    public boolean changed{get;set;}
    public Date ReqDate{get;set;}
    public string FromPlant{get;set;}
    public string sapNum{get;set;}
    public string locname{get;set;}
    public string glnNum{get;set;}
    public string locid{get;set;}
    public boolean selDeselc{get;set;}
        
    public SVMX_PR_SAPAvailability_Controller(){
        
        partReqId = ApexPages.currentPage().getParameters().get('partReqId');
        pr = [select id, name, To_Shipping_Address__c , SVMX_Language__c, SVMX_From_SAP_Storage_Location__c,SVMX_From_SAP_Storage_Location__r.SVMXC_SAP_Storage_Location__c, SVMX_From_SAP_Storage_Location__r.SVMXC_Global_Location_Name__c, SVMX_From_SAP_Storage_Location__r.SVMXC_Plant_Number__c, SVMX_From_SAP_Storage_Location__r.name, SVMXC__Required_At_Location__c, SVMXC__Required_At_Location__r.SVMXC__Country__c, SVMXC__Required_At_Location__r.SVMXC__City__c, SVMXC__Required_At_Location__r.SVMXC__Zip__c , SVMXC__Required_At_Location__r.SVMXC__State__c , SVMXC__Required_At_Location__r.SVMXC__Street__c, (Select id, name, SVMXC__Product__c, SVMXC__Product__r.name, SVMXC__Quantity_Required2__c, SVMX_Request_Lines_Converted__c , SVMXC__Parts_Request__r.SVMX_From_SAP_Storage_Location__c, SVMXC__Parts_Request__r.SVMX_From_SAP_Storage_Location__r.name , SVMXC__Parts_Request__r.SVMX_From_SAP_Storage_Location__r.SVMXC_Plant_Number__c, SVMXC__Product__r.SAPNumber__c, SVMX_Product_Detail__c from  SVMXC__Parts_Request_Line__r where SVMXC__Line_Status__c = 'Open' and SVMX_Request_Lines_Converted__c= false and SVMXC__Product__r.SVMX_SAP_Product_Type__c = :System.Label.Stock_Part)  from SVMXC__Parts_Request__c   where id = :partReqId];     
        partReqLineMap = new Map<id,SVMXC__Parts_Request_Line__c>();
        pr.To_Shipping_Address__c = pr.SVMXC__Required_At_Location__c;
        reqAtLoc = pr.SVMXC__Required_At_Location__c;
        FromPlant  = pr.SVMX_From_SAP_Storage_Location__r.SVMXC_Plant_Number__c;
        state = pr.SVMXC__Required_At_Location__r.SVMXC__State__c;
        city = pr.SVMXC__Required_At_Location__r.SVMXC__City__c;
        zip = pr.SVMXC__Required_At_Location__r.SVMXC__Zip__c ;
        street =  pr.SVMXC__Required_At_Location__r.SVMXC__Street__c;
        country = pr.SVMXC__Required_At_Location__r.SVMXC__Country__c;
        prCountry = pr.SVMXC__Required_At_Location__r.SVMXC__Country__c;
        sapNum = pr.SVMX_From_SAP_Storage_Location__r.SVMXC_SAP_Storage_Location__c ;
        shippingToLoc = pr.To_Shipping_Address__c;
        glnNum = pr.SVMX_From_SAP_Storage_Location__r.SVMXC_Global_Location_Name__c;
        locname = pr.SVMX_From_SAP_Storage_Location__r.name;
        locid = pr.SVMX_From_SAP_Storage_Location__c;
        ReqDate = system.today();
        productIds = new set<id>();
        selDeselc = true;
        for(SVMXC__Parts_Request_Line__c pl : pr.SVMXC__Parts_Request_Line__r ) {
            partReqLineMap.put(pl.id, pl);
            productIds.add(pl.SVMXC__Product__c);
        }
        
        partReqLineWrap = new List<WrapPR>();
        for(SVMXC__Parts_Request_Line__c pl: partReqLineMap.values()){
            partReqLineWrap.add(new WrapPR(pl));
        }
    }  
    
    public class WrapPR{
        
        public id plId{get;set;}
        public string plName{get;set;}
        public boolean sel{get;set;}
        public string partName{get;set;}
        public decimal qty{get;set;}
        public string avlQty{get;set;}
        public id prod{get;set;}
        public id availableFrmLoc{get;set;}
        public string avlDate{get;set;}
        public string availableFrmLocName{get;set;}
        public string prodSapnum{get;set;}
        public string prodDetail;
          
        public WrapPR(SVMXC__Parts_Request_Line__c pl){
            plId = pl.id;
            plName = pl.name;
            sel = true;
            partName = pl.SVMXC__Product__r.Name;
            qty = pl.SVMXC__Quantity_Required2__c;
            prod = pl.SVMXC__Product__c;
            prodSapnum = pl.SVMXC__Product__r.SAPNumber__c;
            availableFrmLoc = pl.SVMXC__Parts_Request__r.SVMX_From_SAP_Storage_Location__c;
            availableFrmLocName = pl.SVMXC__Parts_Request__r.SVMX_From_SAP_Storage_Location__r.name;
            prodDetail = pl.SVMX_Product_Detail__c;
        }     
    } 
    
    public void chkbox(){
        for(WrapPr w: PartReqLineWrap){
            if(selDeselc==false)
                w.sel = false;
            if(selDeselc==true)
                w.sel = true;
        }
    } 
    
     public void toLocChange(){
        SVMXC__Site__c loc;
         if (pr.SVMXC__Required_At_Location__c == null) {
            loc = new SVMXC__Site__c(SVMXC__State__c = '', SVMXC__City__c = '', SVMXC__Zip__c = '', SVMXC__Street__c = '', SVMXC__Country__c = '');
        } else {
            loc = [select id, name, SVMXC__City__c , SVMXC__Country__c , SVMXC__State__c , SVMXC__Street__c , SVMXC__Zip__c  from SVMXC__Site__c where id =:pr.SVMXC__Required_At_Location__c];
        }
        reqAtLoc = loc.id;
        if(pr.To_Shipping_Address__c == null ){
            pr.To_Shipping_Address__c = loc.id;
            shippingToLoc = loc.id;
            state = loc.SVMXC__State__c;
            city = loc.SVMXC__City__c;
            zip = loc.SVMXC__Zip__c ;
            street =  loc.SVMXC__Street__c;
            country = loc.SVMXC__Country__c;
        }
    }
    
    public void ch(){
        SVMXC__Site__c loc;
        if (pr.SVMX_From_SAP_Storage_Location__c == null) {
            loc = new SVMXC__Site__c(Name = '', SVMXC_Plant_Number__c = '', SVMXC_Global_Location_Name__c = '', SVMXC_SAP_Storage_Location__c = '');
        } else {
            loc = [select id, name, SVMXC_Plant_Number__c,SVMXC_Global_Location_Name__c, SVMXC_SAP_Storage_Location__c  from SVMXC__Site__c where id =:pr.SVMX_From_SAP_Storage_Location__c];
        }
        FromPlant = loc.SVMXC_Plant_Number__c;
        sapnum = loc.SVMXC_SAP_Storage_Location__c ;
        glnNum = loc.SVMXC_Global_Location_Name__c;
        locId = loc.id;
        locName = loc.name;
    }
    
    public void change(){
        SVMXC__Site__c loc;
        if (pr.To_Shipping_Address__c == null) {
            loc = new SVMXC__Site__c(SVMXC__State__c = '', SVMXC__City__c = '', SVMXC__Zip__c = '', SVMXC__Street__c = '', SVMXC__Country__c = '');
        } else {
            loc = [select id, SVMXC__City__c ,SVMXC__Country__c , SVMXC__State__c , SVMXC__Street__c , SVMXC__Zip__c from SVMXC__Site__c where id =: pr.To_Shipping_Address__c];
        }
        shippingToLoc = loc.id;
        state = loc.SVMXC__State__c;
        city = loc.SVMXC__City__c;
        zip = loc.SVMXC__Zip__c ;
        street =  loc.SVMXC__Street__c;
        country = loc.SVMXC__Country__c;
    }
    
    public pageReference Back(){
         
        pagereference p =  new pagereference('/'+partReqId); 
        return p;
    }
    
    public pageReference ChkAvailability(){
        
        boolean ch = false;
        //Map<string,integer> materialQty = new Map<string,integer>();
        Map<String,SAPPO_SalesOrderService.ATPCheckMapping> materialMapping = new Map<String,SAPPO_SalesOrderService.ATPCheckMapping>();       
        SVMX_Sales_Office__c so = new SVMX_Sales_Office__c();
        for(WrapPR w: PartReqLineWrap){
            if(w.sel==true){
                ch = true;
                materialMapping.put(w.prodSapnum,new SAPPO_SalesOrderService.ATPCheckMapping(integer.valueOf(w.qty),sapnum,FromPlant));
                w.avlDate = '';
                w.avlqty = '';
                w.availableFrmLocName = '';
            }
        }
        if(pr.SVMX_From_SAP_Storage_Location__c==null){
           apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Select From SAP Storage Location to Check Availability')); 
        }
        else if(!ch){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Select one or more items to Check Availability'));
        }
        else if(pr.SVMX_From_SAP_Storage_Location__c == reqAtLoc){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'From location cannot be same as To location'));
        }
        else if(ch){
            SAPPO_SalesOrderService priceService = new SAPPO_SalesOrderService();
            so = [select SVMX_Price_Order_Account__r.SAPNumber__c  from SVMX_Sales_Office__c  where SVMX_Country__c =:pr.SVMXC__Required_At_Location__r.SVMXC__Country__c]; 
            string accSAPnum = so.SVMX_Price_Order_Account__r.SAPNumber__c;
            List<Map<String,String>> results = new List<Map<String,String>>();
            try{
                results = priceService.getATPCheck(accSAPnum, ReqDate, glnNum, materialMapping).data;
                system.debug(' ## results ' +results);
            }
            catch(System.CalloutException e){
                apexpages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'No Response from SAP'));
                return null;
            }
            if(!results.isEmpty()){
                for(WrapPR w: PartReqLineWrap){ 
                    for(Map<String,String> rs:results){
                        if(w.sel == true && rs.get('MaterialId') == w.prodSapnum && decimal.valueof(rs.get('ConfirmedQuantity')) >= w.qty){ 
                            w.avlDate = rs.get('AvailableOnDate');
                            w.avlqty = string.valueOf(w.qty);
                            w.availableFrmLocName = locName;
                        }
                    }
                }
            }
            else{
                 apexpages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'No Availability from SAP'));
            }
        }
        return null;
    }
    
    public pagereference PartOrder(){
        if(pr.SVMXC__Required_At_Location__c == null){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.Error,'To Stocking Location is Blank'));
            return null;
        }
        Map<string,SVMXC__RMA_Shipment_Order__c > partOrderMap = new Map<string,SVMXC__RMA_Shipment_Order__c >();
        List<SVMXC__RMA_Shipment_Line__c > partOrLineList = new List<SVMXC__RMA_Shipment_Line__c>();
        List<SVMXC__Parts_Request_Line__c> plLines = new List<SVMXC__Parts_Request_Line__c>();
        Boolean selec =false;
        id shipRecTypeId; 
        id shipRecId = SVMX_RecordTypeDataManager.GetRecordTypeID('Shipment','SVMXC__RMA_Shipment_Order__c');
        for(WrapPR w: PartReqLineWrap){
            if(w.sel == true && (w.availableFrmLocName == null || w.availableFrmLocName == '')){
                apexpages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Available from location is blank for a selected Item'));
                return null;
            }
            else if(w.sel==true && !partOrderMap.containsKey(w.availableFrmLoc)){
                SVMXC__RMA_Shipment_Order__c prtOrder = new SVMXC__RMA_Shipment_Order__c();
                prtOrder.RecordTypeId = shipRecId;
                prtOrder.SVMXC__Destination_Location__c = pr.SVMXC__Required_At_Location__c;
                prtOrder.SVMX_Shipping_Location__c = shippingToLoc;
                prtOrder.SVMXC__Source_Location__c = locId;
                prtOrder.SVMXC__Destination_City__c = city;
                prtOrder.Language__c = pr.SVMX_Language__c;
                prtOrder.SVMXC__Destination_Country__c = country;
                prtOrder.SVMXC__Destination_State__c = state;
                prtOrder.SVMXC__Destination_Zip__c = zip;
                prtOrder.SVMXC__Destination_Street__c =street;
                prtOrder.SVMXC__Parts_Request__c = partReqId;
                prtOrder.SVMXC__Order_Type__c = System.Label.Stock_Part;
               // prtOrder.SVMXC_Approved__c = true;
                partOrderMap.put(w.availableFrmLoc,prtOrder);
                selec = true;
            }
        }
        
        if(!selec){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Select one or more items to create SAP Shipment'));
            return null;
        }
        
        if(selec){
            shipRecTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Shipment','SVMXC__RMA_Shipment_Line__c');
        }
        
        if(!partOrderMap.isEmpty()){
            insert partOrderMap.values();
            
            for(WrapPR w: PartReqLineWrap){
                if(w.sel==true && partOrderMap.containsKey(w.availableFrmLoc)){
                    SVMXC__RMA_Shipment_Line__c prOLine = new SVMXC__RMA_Shipment_Line__c();
                    prOLine.SVMXC__Product__c = w.prod;
                    prOLine.SVMXC__Expected_Quantity2__c = decimal.valueOf(w.avlQty);
                    prOLine.SVMXC__Expected_Receipt_Date__c = date.valueOf(w.avlDate);
                    prOLine.SVMXC__RMA_Shipment_Order__c  = partOrderMap.get(w.availableFrmLoc).id;
                    prOLine.SVMX_ProductDetail__c = w.prodDetail;
                    prOLine.RecordTypeId = shipRecTypeId;
                    prOLine.SVMXC__Line_Type__c = System.Label.Stock_Part;
                    partOrLineList.add(prOLine);
                    SVMXC__Parts_Request_Line__c pl = new SVMXC__Parts_Request_Line__c();
                    pl.SVMX_Request_Lines_Converted__c = true;
                    pl.id = w.plId;
                    plLines.add(pl);
                }
            } 
        }
            
        if(!partOrLineList.isEmpty())
            insert partOrLineList;
        
        if(!plLines.isEmpty())
            update plLines;
        
        pagereference p =  new pagereference('/'+partReqId); 
        return p;
        
    }
}