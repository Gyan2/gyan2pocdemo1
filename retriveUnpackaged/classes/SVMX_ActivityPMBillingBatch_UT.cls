/************************************************************************************************************
Description: Test class for SVMX_UpdateWorkOrderStatusBatch class
 
Author: 
Date: 

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest
public class SVMX_ActivityPMBillingBatch_UT { 

 static testMethod void testActivityPMBillingBatch()
       {
         Account acc = SVMX_TestUtility.CreateAccount('Test account', 'Norway', true);
         Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
         SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('test location', acc.Id, true);
         SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
         Profile prof = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
         User u = SVMX_TestUtility.CreateUser('testTech', prof.Id, false);
         u.UserCountry__c ='US';
         u.SalesOrg__c ='9999';
         insert u;
         SVMX_Custom_Feature_Enabler__c  cfe=SVMX_TestUtility.CreateCustomFeature('Germany',true);

         SVMX_Billing_Rules__c   br=SVMX_TestUtility.CreateBillingRule('Work Order','SVMX_PS_Revisit_Reason__c','Option 1','',true);
         
         SVMX_Billing_Rules__c   bWdls=SVMX_TestUtility.CreateBillingRule('Work Details','SVMX_PS_VS_Consumed_Part_Code__c','Straight','Labor',true);
         
         SVMXC__Service_Group__c servGp = SVMX_TestUtility.CreateServiceGroup('Test Service Group', true);
         //Create product
         Product2 p = SVMX_TestUtility.CreateProduct('Test Product', true);
         Case c = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'Ready To Invoice', true);
         Case caseTwo = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'Ready To Invoice', true);
         
         //Create technician
         SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Test Tech', u.Id, servGp.id, true);
         List<SVMXC__Service_Contract__c>   sclist=new List<SVMXC__Service_Contract__c>();
         SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Service Contract', 'Global Std', con.Id, True);
         sc.SVMXC__Billing_Schedule__c='All Locations per visit';
       
           
         SVMXC__Service_Contract__c sc1 = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Service Contract', 'Global Std', con.Id, True);
         sc1.SVMXC__Billing_Schedule__c='Per Location Per Visit';
         
     
         sclist.add(sc);
         sclist.add(sc1);
         
         update sclist;
         
         List<SVMXC__PM_Plan__c> pmPlanList = new List<SVMXC__PM_Plan__c>();
         SVMXC__PM_Plan__c pmPlan = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
         pmPlan.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
         pmPlanList.add(pmPlan);
         SVMXC__PM_Plan__c pmPlan1 = SVMX_TestUtility.CreatePMPlan(sc1.Id, 'test Pm Plan', false);
         pmPlan1.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
         pmPlan1.SVMX_Converted_Plan__c = true;
         pmPlanList.add(pmPlan1);
         insert pmPlanList;
         
         SVMXC__PM_Schedule_Definition__c pmd=SVMX_TestUtility.CreatePMDefinition(pmPlan1.Id,true);
         SVMXC__PM_Schedule_Definition__c pmd2=SVMX_TestUtility.CreatePMDefinition(pmPlan.Id,true);
         
         List<SVMXC__Service_Order_Line__c> sOrdList = new List<SVMXC__Service_Order_Line__c>();
         List<SVMXC__Service_Order__c> woList = new List<SVMXC__Service_Order__c>();
         SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Ready to Invoice', 'Planned Services (PPM)', 'Global Std', sc.Id, tech.Id, c.Id, false);
        
         wo.SVMXC__PM_Plan__c = pmPlan.Id;
         wo.SVMX_PM_Schedule_Definition__c=pmd.Id;
         woList.add(wo);
         SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Ready to Invoice', 'Planned Services (PPM)', 'Global Std', sc1.Id, tech.Id, caseTwo.Id, false);
        
         wo1.SVMXC__PM_Plan__c = pmPlan1.Id;
         wo.SVMX_PM_Schedule_Definition__c=pmd2.Id;
         woList.add(wo1);
         
         insert woList;
        
         
         Test.startTest();

            SVMX_ActivityPMBillingBatch obj = new SVMX_ActivityPMBillingBatch();
            DataBase.executeBatch(obj);

        Test.stopTest();
       }
  /*    static testMethod void testActivityPMBillingPerLocation()
       {
         Account acc = SVMX_TestUtility.CreateAccount('Test account', 'Norway', true);
         Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
         SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('test location', acc.Id, true);
         SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
         Profile prof = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
         User u = SVMX_TestUtility.CreateUser('testTech', prof.Id, false);
         u.UserCountry__c ='US';
         u.SalesOrg__c ='9999';
         insert u;
         SVMX_Custom_Feature_Enabler__c  cfe=SVMX_TestUtility.CreateCustomFeature('Germany',true);

         SVMX_Billing_Rules__c   br=SVMX_TestUtility.CreateBillingRule('Work Order','SVMX_PS_Revisit_Reason__c','Option 1','',true);
         
         SVMX_Billing_Rules__c   bWdls=SVMX_TestUtility.CreateBillingRule('Work Details','SVMX_PS_VS_Consumed_Part_Code__c','Straight','Labor',true);
         
         SVMXC__Service_Group__c servGp = SVMX_TestUtility.CreateServiceGroup('Test Service Group', true);
         //Create product
         Product2 p = SVMX_TestUtility.CreateProduct('Test Product', true);
         Case c = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'Ready To Invoice', true);
         Case caseTwo = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'Ready To Invoice', true);
         
         //Create technician
         SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Test Tech', u.Id, servGp.id, true);
         List<SVMXC__Service_Contract__c>   sclist=new List<SVMXC__Service_Contract__c>();
         SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Service Contract', 'Global Std', con.Id, true);
         sc.SVMXC__Billing_Schedule__c='Per Location Per Visit';
         
         SVMXC__Service_Contract__c sc1 = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Service Contract', 'Global Std', con.Id, true);
         sc1.SVMXC__Billing_Schedule__c='Per Location Per Visit';
         sclist.add(sc);
         sclist.add(sc1);
         
         update sclist;
         
         List<SVMXC__PM_Plan__c> pmPlanList = new List<SVMXC__PM_Plan__c>();
         SVMXC__PM_Plan__c pmPlan = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
         pmPlan.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
         pmPlanList.add(pmPlan);
         SVMXC__PM_Plan__c pmPlan1 = SVMX_TestUtility.CreatePMPlan(sc1.Id, 'test Pm Plan', false);
         pmPlan1.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
         pmPlan1.SVMX_Converted_Plan__c = true;
         pmPlanList.add(pmPlan1);
         insert pmPlanList;
         
         SVMXC__PM_Schedule_Definition__c pmd=SVMX_TestUtility.CreatePMDefinition(pmPlan1.Id,true);
         SVMXC__PM_Schedule_Definition__c pmd2=SVMX_TestUtility.CreatePMDefinition(pmPlan.Id,true);
         
         List<SVMXC__Service_Order_Line__c> sOrdList = new List<SVMXC__Service_Order_Line__c>();
         List<SVMXC__Service_Order__c> woList = new List<SVMXC__Service_Order__c>();
         SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Work Complete', 'Planned Services (PPM)', 'Global Std', sc.Id, tech.Id, c.Id, false);
        
         wo.SVMXC__PM_Plan__c = pmPlan.Id;
         wo.SVMX_PM_Schedule_Definition__c=pmd.Id;
         woList.add(wo);
         SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Work Complete', 'Planned Services (PPM)', 'Global Std', sc1.Id, tech.Id, caseTwo.Id, false);
        
         wo1.SVMXC__PM_Plan__c = pmPlan1.Id;
         wo.SVMX_PM_Schedule_Definition__c=pmd2.Id;
         woList.add(wo1);
         
         insert woList;
        
         
         Test.startTest();

            SVMX_ActivityPMBillingBatch obj = new SVMX_ActivityPMBillingBatch();
            DataBase.executeBatch(obj);

        Test.stopTest();
       }
*/
}