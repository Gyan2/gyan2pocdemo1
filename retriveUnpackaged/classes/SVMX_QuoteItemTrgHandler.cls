/*******************************************************************************************************
Description: Trigger handler for Service Quote Trigger

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger: SVMX_QuoteItemTrggr.cls
    Service Manager: SVMX_QuoteItemTrggrServiceManagaer.cls
    Test class: SVMX_QuoteItemTrgHandler_UT.cls
    
 
Author: Keshava Prasad
Date: 04-06-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/
public class SVMX_QuoteItemTrgHandler extends SVMX_TriggerHandler {
    private list<SVMXC__Quote_Line__c > newSQList;
    private list<SVMXC__Quote_Line__c > oldSQList;
    private Map<Id, SVMXC__Quote_Line__c > newSQMap;
    private Map<Id, SVMXC__Quote_Line__c > oldSQMap;
    
    public SVMX_QuoteItemTrgHandler() {
        this.newSQList = (list<SVMXC__Quote_Line__c >) Trigger.new;
        this.oldSQList = (list<SVMXC__Quote_Line__c >) Trigger.old;
        this.newSQMap = (Map<Id, SVMXC__Quote_Line__c >) Trigger.newMap;
        this.oldSQMap = (Map<Id, SVMXC__Quote_Line__c >) Trigger.oldMap;
    }
    
    public override void beforeInsert() {
        
        List<SVMXC_Quote_Configuration__c> quoteConfigList = new List<SVMXC_Quote_Configuration__c>() ;
        Boolean updateHeader =false;
        Set<id> serviceQuoteID = new Set<id>() ;
        Set<String> countries = new Set<String>() ;
        List<SVMXC__Quote_Line__c> quoteItemList = new List<SVMXC__Quote_Line__c>();
        for(SVMXC__Quote_Line__c quoteLine : newSQList){
            if(quoteLine.SVMX_Discount__c != null || quoteLine.SVMX_Additional_Discount__c != null){
                countries.add(quoteLine.SVMX_PS_Country__c ) ;
                serviceQuoteID.add(quoteLine.SVMXC__Quote__c) ;
                quoteItemList.add(quoteLine);
            }
            
        }
        if(quoteItemList.size() > 0){
            Map<id,SVMXC__Quote__c> serviceQuoteMap = new Map<id,SVMXC__Quote__c>(SVMX_QuoteItemDataManager.serviceQuoteIds(serviceQuoteID)) ;
           // serviceQuoteMap = [SELECT id,SVMX_Sp_Approval_Trgg__c FROM SVMXC__Quote__c WHERE Id IN :serviceQuoteID] ;
            quoteConfigList = [SELECT id,SVMX_Discount_threshold__c,SVMX_Country__c,SVMX_No_Of_Approvals__c,SVMX_Special_Approval__c, SVMX_Additional_Threshold__c  FROM SVMXC_Quote_Configuration__c WHERE SVMX_Country__c IN :countries] ;
             
            if(!quoteConfigList.isEmpty()) {
                for(SVMXC_Quote_Configuration__c quoteConfig : quoteConfigList){
                    for(SVMXC__Quote_Line__c quoteItems : quoteItemList) {
                        if( serviceQuoteMap.containsKey( quoteItems.SVMXC__Quote__c) && serviceQuoteMap.get(quoteItems.SVMXC__Quote__c).SVMX_Special_Approval__c == true) {
                            quoteItems.SVMX_Discount_threshold__c = quoteConfig.SVMX_Discount_threshold__c ;
                            quoteItems.SVMX_Additional_Discount_Threshold__c = quoteConfig.SVMX_Additional_Threshold__c ;
                            if(quoteItems.SVMX_Additional_Discount__c > quoteItems.SVMX_Additional_Discount_Threshold__c || quoteItems.SVMX_Discount__c > quoteItems.SVMX_Discount_threshold__c ) {
                                updateHeader = true;
                               serviceQuoteMap.get(quoteItems.SVMXC__Quote__c).SVMX_Sp_Approval_Trgg__c = true ; 
                                
                            }
                        }
                    }
                }
            }
            if(updateHeader)
            update serviceQuoteMap.values() ;
        }
        
    }

       public override void beforeUpdate() {
        
        List<SVMXC_Quote_Configuration__c> quoteConfigList = new List<SVMXC_Quote_Configuration__c>() ;
        Boolean updateHeader =false;
        Set<id> serviceQuoteID = new Set<id>() ;
        Set<String> countries = new Set<String>() ;
        List<SVMXC__Quote_Line__c> quoteItemList = new List<SVMXC__Quote_Line__c>();
        for(SVMXC__Quote_Line__c quoteLine : newSQList){
            if((quoteLine.SVMX_Discount__c != null && quoteLine.SVMX_Discount__c != oldSQMap.get(quoteLine.id).SVMX_Discount__c ) || (quoteLine.SVMX_Additional_Discount__c != null && quoteLine.SVMX_Discount__c != oldSQMap.get(quoteLine.id).SVMX_Discount__c )){
                countries.add(quoteLine.SVMX_PS_Country__c ) ;
                serviceQuoteID.add(quoteLine.SVMXC__Quote__c) ;
                quoteItemList.add(quoteLine);
            }
            
        }
        if(quoteItemList.size() > 0){
            Map<id,SVMXC__Quote__c> serviceQuoteMap = new Map<id,SVMXC__Quote__c>(SVMX_QuoteItemDataManager.serviceQuoteIds(serviceQuoteID)) ;
           // serviceQuoteMap = [SELECT id,SVMX_Sp_Approval_Trgg__c FROM SVMXC__Quote__c WHERE Id IN :serviceQuoteID] ;
            quoteConfigList = [SELECT id,SVMX_Discount_threshold__c,SVMX_Country__c,SVMX_No_Of_Approvals__c,SVMX_Special_Approval__c, SVMX_Additional_Threshold__c  FROM SVMXC_Quote_Configuration__c WHERE SVMX_Country__c IN :countries] ;
             
            if(!quoteConfigList.isEmpty()) {
                for(SVMXC_Quote_Configuration__c quoteConfig : quoteConfigList){
                    for(SVMXC__Quote_Line__c quoteItems : quoteItemList) {
                        if(serviceQuoteMap.containsKey( quoteItems.SVMXC__Quote__c) && serviceQuoteMap.get(quoteItems.SVMXC__Quote__c).SVMX_Special_Approval__c == true) {
                            quoteItems.SVMX_Discount_threshold__c = quoteConfig.SVMX_Discount_threshold__c ;
                            quoteItems.SVMX_Additional_Discount_Threshold__c   = quoteConfig.SVMX_Additional_Threshold__c ;
                            if(quoteItems.SVMX_Additional_Discount__c > quoteItems.SVMX_Additional_Discount_Threshold__c || quoteItems.SVMX_Discount__c > quoteItems.SVMX_Discount_threshold__c ) {
                                updateHeader = true;
                               serviceQuoteMap.get(quoteItems.SVMXC__Quote__c).SVMX_Sp_Approval_Trgg__c = true ; 
                                
                            }
                        }
                    }
                }
            }
            if(updateHeader){
            update serviceQuoteMap.values() ;
        }
        }
        
    }
}