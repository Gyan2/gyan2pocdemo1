public class SVMX_LocationDataManager {
    
   /* public static Map<Id,SVMXC__Site__c> woQuery(list<SVMXC__Site__c> oldlocList){
        Map<Id,SVMXC__Site__c> LocMap = new Map<Id,SVMXC__Site__c> ([select id,SVMXC__Account__c,SVMX_Update_Account__c from SVMXC__Site__c Where id in :oldlocList]);
        return LocMap;
    }
    
    */
    
    public static list<SVMXC__Site__c> accountsLocationQuery(set<Id> accountIds){
        list<SVMXC__Site__c> LocLst = new list<SVMXC__Site__c> ([select id,SVMXC__Account__c, SVMXC_Global_Location_Name__c from SVMXC__Site__c Where SVMXC__Account__c in :accountIds]);
        return LocLst;
    }

    public static list<SVMXC__Site__c> LocationQuery(set<Id> locIds){
        list<SVMXC__Site__c> LocLst = new list<SVMXC__Site__c> ([select id,SVMXC__Account__c,SVMX_Approval_Required__c,SVMXC_Global_Location_Name__c from SVMXC__Site__c Where  Id in :locIds AND SVMX_Approval_Required__c = false]);
        return LocLst;
    }
    
  /*  public static list<SVMXC__Site__c> relatedLocationQuery(List<SVMXC__Site__c> locIds){
        list<SVMXC__Site__c> LocLst = new list<SVMXC__Site__c> ([select id, SVMXC__Account__c, SVMXC__Account__r.name, SVMXC__Parent__c from SVMXC__Site__c Where  SVMXC__Parent__c in :locIds]);
        return LocLst;
    } */
 
    public static void updateIP(list<SVMXC__Installed_Product__c> updateIPs){
        
     
            update updateIPs;
        }
     public static void inserRelAcc(list<SVMX_Related_Account__c> RelAccountList){
        
     
            insert RelAccountList;
        }
    
    public static void updateLoc(list<SVMXC__Site__c> updateLocs){
        
     
            update updateLocs;
        }
           
        
    
}