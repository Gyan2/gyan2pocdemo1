/************************************************************************************************************
Description: Data Manager for Work Detail. All the SOQL queries and DML Operations are performed in this class.

Dependency: 
 
Author: Ranjitha S
Date: 17-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
9-Nov-2017      Pooja Singh     Added workDetailQuery method
*******************************************************************************************************/

public class SVMX_WorkDetailDataManager {

    public static map<id,SVMXC__Service_Order_Line__c> workDetailQuery(list<SVMXC__Service_Order_Line__c> woli){
        map<id,SVMXC__Service_Order_Line__c> wdQueryList = new map<id,SVMXC__Service_Order_Line__c>([select id, SVMXC__Service_Order__r.SVMXC__Purpose_of_Visit__c, SVMXC__Is_Billable__c, CreatedById, SVMXC__Line_Type__c, SVMXC__Start_Date_and_Time__c, SVMXC__Service_Order__c, SVMXC__Group_Member__c,  RecordTypeId, SVMXC__Service_Order__r.SVMXC__Country__c, SVMXC__Service_Order__r.SVMXC__Service_Contract__c, SVMXC__Service_Order__r.SVMXC__Order_Type__c, SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c,SVMXC__Service_Order__r.SVMXC__Group_Member__c, SVMXC__End_Date_and_Time__c, SVMX_No_of_Technicians__c, SVMX_Out_Of_Hours__c, SVMX_No_of_Hours_Onsite__c, SVMX_Billing_Type__c, SVMXC__Serial_Number__c, SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMX_Pricing_Method__c from  SVMXC__Service_Order_Line__c where id in : woli]);
        return wdQueryList;
    }
    
    public static Map<id,List<SVMXC__Service_Order_Line__c>> laborLinesQuery(map<id,List<SVMXC__Service_Order_Line__c>> wdLaborList, set<id> laborLinesIds){
        List<SVMXC__Service_Order_Line__c> laborLinesList = new List<SVMXC__Service_Order_Line__c>([select id, SVMXC__Service_Order__r.SVMXC__Order_Status__c, SVMXC__Line_Type__c, SVMXC__Start_Date_and_Time__c, SVMXC__Service_Order__c, SVMXC__Service_Order__r.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c, SVMXC__End_Date_and_Time__c  from  SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c in : wdLaborList.keySet() and SVMXC__Line_Type__c = 'Labor' and  id not in: laborLinesIds and SVMXC__Start_Date_and_Time__c != null]);
        Map<id,List<SVMXC__Service_Order_Line__c>> LaborLinesMap = new  Map<id,List<SVMXC__Service_Order_Line__c>>();
        for(SVMXC__Service_Order_Line__c wl: laborLinesList){
            if(LaborLinesMap.containsKey(wl.SVMXC__Service_Order__c))
                LaborLinesMap.get(wl.SVMXC__Service_Order__c).add(wl);                  
            else
                LaborLinesMap.put(wl.SVMXC__Service_Order__c,new List<SVMXC__Service_Order_Line__c> {wl});
        }
        return LaborLinesMap;
    }
    
    public static List<SVMXC__Expense_Pricing__c> ExpenseMarkupQuery(list<SVMXC__Service_Order_Line__c> woli){
      
        List<string> expenseType = new List<string>();
        set<id> scIds = new set<id>();
        
        for(SVMXC__Service_Order_Line__c wl: woli){
            expenseType.add(wl.SVMXC__Expense_Type__c);
            scIds.add(wl.SVMX_Service_Contract__c);
        } 
        system.debug(' ## Contract'+scIds);
        
        List<SVMXC__Expense_Pricing__c> expensePricingList = new List<SVMXC__Expense_Pricing__c>([select id, SVMXC__Expense_Type__c, SVMXC__Rate__c , SVMXC__Rate_Type__c , SVMXC__Service_Contract__c from SVMXC__Expense_Pricing__c where SVMXC__Expense_Type__c in :expenseType and SVMXC__Rate_Type__c = 'Markup %' and SVMXC__Service_Contract__c in :scIDs]);
    return expensePricingList;
    }

    public static  List<SVMXC__Service_Order_Line__c> ToCanceledWorkDetailLines(set<id> WOIds){
        List<SVMXC__Service_Order_Line__c> wdQueryList = new List<SVMXC__Service_Order_Line__c>([select id, SVMXC__Service_Order__c, SVMX_SAP_Status__c,SVMX_Sales_Order_Item_Number__c from  SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c in : WOIds AND SVMX_Sales_Order_Item_Number__c = null ]);
        return wdQueryList;
    }
    
    public static void UpdaWorkDetails(List<SVMXC__Service_Order_Line__c> workdetails){
        update workdetails;
    }    
    
    public static void InsWorkDetails(List<SVMXC__Service_Order_Line__c> workdetails){
        insert workdetails;
    } 
}