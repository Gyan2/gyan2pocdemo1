/**
 * Created by rebmangu on 28/02/2018.
 */

@IsTest
private class TestAccounts {
    @isTest
    public static void bulkTest() {
        Map<Id,Profile> profiles = DM004_User.getCustomProfiles(); // don't use it, but could
        Map<Id,User> users = DM004_User.getActiveUsersForProfile(new List<Id>(profiles.keySet()));
        User u = new List<User>(users.values())[0];

        Test.startTest();

        system.runAs(u) {
            List<Account> bulkAccounts = new List<Account>();
            for (Integer i=0; i<200; i++) {
                Account a = TESTData.createAccount('name-'+i, false);
                bulkAccounts.add(a);
            }

            insert bulkAccounts;
        }

        Test.stopTest();
        //System.debug(u.User_Country__c);
        //System.debug([select id,Management_Country__c from Account limit 1].Management_Country__c);
        //system.assertEquals(200,[select id from Account where Management_Country__c =: u.User_Country__c].size());

    }
}