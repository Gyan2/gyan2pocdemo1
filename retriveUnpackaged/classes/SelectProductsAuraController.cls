/**
 * Created by rebmangu on 20/02/2018.
 */

public with sharing class SelectProductsAuraController {
    public static string SUCCESS = 'SUCCESS';


    @AuraEnabled
    public static List<Object> getProducts(String search,String salesOrgId,String quoteId ,String fields,String language,String searchField,List<String> exclude){
        List<Object> result = new List<Object>();

        Set<String> fieldsSet = new Set<String>((List<String>)JSON.deserialize(fields,List<String>.class));
        search = '%'+search+'%';
        List<String> onlyForTest = new List<String>{'01t4E000002bG9KQAU','01t4E000002INU7QAO','01t4E000002INU6QAO','01t4E000002INU5QAO','01t4E000002INU4QAO'};
        exclude = new List<String>();

        String query = 'SELECT Id,Description__c,'+String.join(new List<String>(fieldsSet),',')+' FROM ProductDetail__c WHERE Language__c =: language AND '+searchField+' LIKE :search AND ProductRef__c IN (select Product2ID from PricebookEntry where Pricebook2.ExternalId__c =: salesOrgId AND IsActive = true) AND ProductRef__c in :onlyForTest AND ProductRef__c NOT IN : exclude limit 500';
        System.debug(exclude);
        System.debug(query);


        for(ProductDetail__c productDetail : (List<ProductDetail__c>)Database.query(query)){
            Map<String,Object> resultItem = new Map<String,Object>{
                    'data' => new List<Map<String,String>>(),
                    'selected' => false
            };

            for(String field : fieldsSet){
                ((List<Map<String,String>>)resultItem.get('data')).add(new Map<String,String>{
                    'Value'=>String.valueOf(Util.getValue((Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(productDetail)),field)), //Recursive way to catch the value -> see the utilityClass
                    'ApiName'=>field
                });
            }

            result.add(resultItem);

        }

        return result;

    }


    @AuraEnabled
    public static List<String> getLanguages(String selected){

        List<String> result = new List<String>();
        for(PicklistEntry entry : ProductDetail__c.Language__c.getDescribe().getPicklistValues()){
            result.add(entry.getValue());
        }

        return result;
    }

    @AuraEnabled
    public static String getSalesOrg(String quoteId){
        return [select id,SalesOrg__c from Quote where id =: quoteId].SalesOrg__c;
    }





    @AuraEnabled
    public static List<QuoteLineItem> generateQuoteLineItems(List<String> selected,String salesOrgId,String quoteId){

        List<QuoteLineItem> result = new List<QuoteLineItem>();
        String standardPriceBookId = Test.isRunningTest()?Test.getStandardPricebookId():[SELECT id FROM Pricebook2 WHERE isStandard = true LIMIT 1].Id;

        Quote quo = [SELECT id,Name,CurrencyIsoCode FROM Quote WHERE Id =: quoteId];

        Map<Id,Product2> productIdsMap = new Map<Id,Product2>([select id,ExternalId__c from Product2 where id in: selected]);
        List<PricebookEntry> pricebookEntries = [SELECT id,UnitPrice,Product2Id,ExternalId__c,CurrencyIsoCode FROM PricebookEntry
        WHERE Product2Id IN : selected
        AND CurrencyIsoCode =: quo.CurrencyIsoCode
        AND Pricebook2Id =: standardPriceBookId];


        Map<Id,PricebookEntry> productMap = new Map<Id,PricebookEntry>();

        // Pricebook to Product mapping
        for(PricebookEntry pbE : pricebookEntries){
            productMap.put(pbE.Product2Id,pbe);
        }

        // Check for missing pricebooks
        new Utility().insertMissingPricebookEntries(selected,productMap,quo,productIdsMap,standardPriceBookId);




        for(String productId : selected){
            QuoteLineItem item = new QuoteLineItem();
                item.PricebookEntry = productMap.get(productId);
                item.PricebookEntryId = productMap.get(productId).Id;
                item.Quantity = null;
                item.QuoteId = quoteId;
                item.UnitPrice = productMap.get(productId).UnitPrice;
                item.Product2 = new Product2(Id=productId,ExternalId__c=productIdsMap.get(productId).ExternalId__c);

            result.add(item);
        }
        return result;
    }

    @AuraEnabled
    public static String insertQuoteLineItems(List<SObject> quoteLineItems){
        String result = SUCCESS;
        //List<QuoteLineItem> quoteLineItems = (List<QuoteLineItem>)JSON.deserialize(quoteLineItemJSON,List<QuoteLineItem>.Class);
        System.debug(quoteLineItems);
        try{
            insert quoteLineItems;
        }catch(Exception e){
            System.debug(e);
            result = e.getMessage();
        }

        return result;
    }


    public class Utility {
        public void insertMissingPricebookEntries(List<String> selected,Map<Id,PricebookEntry> productMap,Quote quo,Map<Id,Product2> productIdsMap,String standardPriceBookId){
            List<PricebookEntry> pricebookEntriesToInsert = new List<PricebookEntry>();

            //Check for missing pricebookEntries and insert them
            for(String productId : selected){
                if(!productMap.containsKey(productId)){
                    PricebookEntry pbe = new PricebookEntry(
                            Pricebook2Id    = standardPriceBookId,
                            CurrencyIsoCode = quo.CurrencyIsoCode,
                            UnitPrice       = 0,
                            ExternalId__c   = 'standard-'+productIdsMap.get(productId).ExternalId__c+'-'+quo.CurrencyIsoCode,
                            Product2Id      = productId,
                            IsActive        = true
                    );


                    pricebookEntriesToInsert.add(pbe);
                }
            }
            insert pricebookEntriesToInsert;

            // Update ProductMap with the new PricebookEntries
            for(PricebookEntry pbE : pricebookEntriesToInsert){
                productMap.put(pbE.Product2Id,pbe);
            }
        }
    }

}