/************************************************************************************************************
Description: Controller for SVMX_WO_SAPAvailability VF page.
 
Author: Ranjitha Shenoy
Date: 26-02-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

public class SVMX_WO_SAPAvailability_Controller {

    public ID workOrderId{get;set;}
    public Map<id,SVMXC__Service_Order_Line__c> workDetailMap;
    public List<WrapWD> workdetailWrap {get;set;}
    public SVMXC__Service_Order__c wo {get;set;}
    public string woCountry{get;set;}
    public string state {get;set;}
    public string city {get;set;}
    public string zip {get;set;}
    public string country {get;set;}
    public string street {get;set;}
    public id shippingToLoc {get;set;}
    public id reqAtLoc {get;set;}
    public static List<SVMXC__Product_Stock__c> psList;
    public set<id> productIds;
    public string available = Label.available; 
    public boolean changed{get;set;}
    public Date ReqDate{get;set;}
    public string FromPlant{get;set;}
    public string sapNum{get;set;}
    public string locname{get;set;}
    public string locId{get;set;}
    public string glnNum{get;set;}
    public boolean selDeselc{get;set;}
    public string woOrderType{get;set;}
        
    public SVMX_WO_SAPAvailability_Controller(){
        Id reqReciRecType = SVMX_RecordTypeDataManager.GetRecordTypeID('RequestReceipt','SVMXC__Service_Order_Line__c');
        Id estRecType = SVMX_RecordTypeDataManager.GetRecordTypeID('Estimate','SVMXC__Service_Order_Line__c');
        workOrderId = ApexPages.currentPage().getParameters().get('workOrderId');
        woOrderType = ApexPages.currentPage().getParameters().get('woOrderType');
        if(woOrderType == System.Label.Spare_Parts_Order){
            wo = [select id, name, SVMX_Language__c, SVMXC__Site__c, SVMXC__Preferred_Start_Time__c, SVMX_Default_Stock_Location__c, SVMXC__Country__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__c, SVMX_From_SAP_Storage_Location__c, SVMX_From_SAP_Storage_Location__r.SVMXC_Plant_Number__c,SVMX_From_SAP_Storage_Location__r.name, SVMX_From_SAP_Storage_Location__r.SVMXC_SAP_Storage_Location__c , SVMX_From_SAP_Storage_Location__r.SVMXC_Global_Location_Name__c, SVMXC__Site__r.SVMXC__City__c, SVMXC__Site__r.SVMXC__Street__c,SVMXC__Site__r.SVMXC__State__c, SVMXC__Site__r.SVMXC__Zip__c, SVMXC__Site__r.SVMXC__Country__c  , SVMX_To_Stock_Location__c, (Select id, name, SVMXC__Product__c, SVMXC__Product__r.name, SVMX_Request_Lines_Converted__c, SVMXC__Product__r.SAPNumber__c,SVMXC__Estimated_Quantity2__c, RecordTypeid, SVMXC__Service_Order__r.SVMX_From_SAP_Storage_Location__r.name, SVMX_ProductDetail__c  from  SVMXC__Service_Order_Line__r  where RecordTypeid =: estRecType  and SVMX_Estimates_Raised__c = false and SVMX_SAP_Product_Type__c = :System.Label.Stock_Part)  from SVMXC__Service_Order__c   where id = :workOrderId]; wo.SVMX_Default_Stock_Location__c = wo.SVMXC__Site__c;
            wo.SVMX_Default_Stock_Location__c = wo.SVMXC__Site__c;
            wo.SVMX_To_Stock_Location__c = wo.SVMXC__Site__c;
            state = wo.SVMXC__Site__r.SVMXC__State__c;
            city = wo.SVMXC__Site__r.SVMXC__City__c;
            zip = wo.SVMXC__Site__r.SVMXC__Zip__c ;
            street =  wo.SVMXC__Site__r.SVMXC__Street__c;
            country = wo.SVMXC__Site__r.SVMXC__Country__c;
            reqAtLoc = wo.SVMXC__Site__c;
            shippingToLoc = wo.SVMXC__Site__c;
        }
        else{
            wo = [select id, name, SVMX_Language__c, SVMXC__Site__c, SVMXC__Preferred_Start_Time__c , SVMXC__Country__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Country__c, SVMX_From_SAP_Storage_Location__c, SVMX_From_SAP_Storage_Location__r.SVMXC_Plant_Number__c,SVMX_From_SAP_Storage_Location__r.name, SVMX_From_SAP_Storage_Location__r.SVMXC_SAP_Storage_Location__c , SVMX_From_SAP_Storage_Location__r.SVMXC_Global_Location_Name__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__City__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Zip__c , SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__State__c , SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Street__c , SVMX_To_Stock_Location__c, (Select id, name, SVMXC__Product__c, SVMXC__Product__r.name, SVMX_Request_Lines_Converted__c, SVMXC__Product__r.SAPNumber__c,SVMXC__Requested_Quantity2__c, SVMXC__Service_Order__r.SVMX_From_SAP_Storage_Location__r.name, SVMX_ProductDetail__c,RecordTypeId  from  SVMXC__Service_Order_Line__r  where RecordTypeid =: reqReciRecType  and SVMX_Request_Lines_Converted__c = false and SVMX_SAP_Product_Type__c = :System.Label.Stock_Part)  from SVMXC__Service_Order__c   where id = :workOrderId];     
            wo.SVMX_Default_Stock_Location__c = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c;
            wo.SVMX_To_Stock_Location__c = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c;
            reqAtLoc = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c;
            shippingToLoc = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c;
            state = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__State__c;
            city = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__City__c;
            zip = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Zip__c ;
            street =  wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Street__c;
            country = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Country__c;
        }
        
        workDetailMap = new Map<id,SVMXC__Service_Order_Line__c>();
        FromPlant  = wo.SVMX_From_SAP_Storage_Location__r.SVMXC_Plant_Number__c;
        sapNum = wo.SVMX_From_SAP_Storage_Location__r.SVMXC_SAP_Storage_Location__c;
        glnNum = wo.SVMX_From_SAP_Storage_Location__r.SVMXC_Global_Location_Name__c;
        locname = wo.SVMX_From_SAP_Storage_Location__r.name;
        locId = wo.SVMX_From_SAP_Storage_Location__c;
        selDeselc = true;
        system.debug('SAPNum ##'+sapNum);
        woCountry = wo.SVMXC__Country__c;
        ReqDate = date.valueOf(wo.SVMXC__Preferred_Start_Time__c);
        productIds = new set<id>();
        for(SVMXC__Service_Order_Line__c wd : wo.SVMXC__Service_Order_Line__r ) {
            workDetailMap.put(wd.id, wd);
            productIds.add(wd.SVMXC__Product__c);
        }
        
        workdetailWrap = new List<WrapWD>();
        for(SVMXC__Service_Order_Line__c wd: workDetailMap.values()){
            workdetailWrap.add(new WrapWD(wd));
        }
    }  
    
    public class WrapWD{
        public id wdId{get;set;}
        public string wdName{get;set;}
        public boolean sel{get;set;}
        public string partName{get;set;}
        public decimal qty{get;set;}
        public string avlQty{get;set;}
        public id prod{get;set;}
        public id availableFrmLoc{get;set;}
        public string avlDate{get;set;}
        public string availableFrmLocName{get;set;}
        public string prodSapnum{get;set;}
        public string prodDetail;
        Id estRecType = SVMX_RecordTypeDataManager.GetRecordTypeID('Estimate','SVMXC__Service_Order_Line__c');
          
        public WrapWD(SVMXC__Service_Order_Line__c wd){
            wdId = wd.id;
            wdName = wd.name;
            sel = true;
            partName = wd.SVMXC__Product__r.name;
            if(wd.RecordTypeId == estRecType){
                qty = wd.SVMXC__Estimated_Quantity2__c;
            }
            else{
                qty = wd.SVMXC__Requested_Quantity2__c;
            }
            prod = wd.SVMXC__Product__c;
            prodSapnum = wd.SVMXC__Product__r.SAPNumber__c;
            availableFrmLoc = wd.SVMXC__Service_Order__r.SVMX_From_SAP_Storage_Location__c;
            availableFrmLocName = wd.SVMXC__Service_Order__r.SVMX_From_SAP_Storage_Location__r.name;
            prodDetail = wd.SVMX_ProductDetail__c;
        }     
    } 
    
    public void chkbox(){
        for(WrapWD w: workdetailWrap){
            if(selDeselc==false)
                w.sel = false;
            if(selDeselc==true)
                w.sel = true;
        }
    } 
    
    public void toLocChange(){
        SVMXC__Site__c loc;
        if (wo.SVMX_Default_Stock_Location__c == null) {
            loc = new SVMXC__Site__c(SVMXC__State__c = '', SVMXC__City__c = '', SVMXC__Zip__c = '', SVMXC__Street__c = '', SVMXC__Country__c = '');
        } else {
            loc = [select id, name, SVMXC__City__c , SVMXC__Country__c , SVMXC__State__c , SVMXC__Street__c , SVMXC__Zip__c  from SVMXC__Site__c where id =:wo.SVMX_Default_Stock_Location__c];
        }
        reqAtLoc = loc.id;
        if(wo.SVMX_To_Stock_Location__c == null ){
            wo.SVMX_To_Stock_Location__c = loc.id;
            assignLocationToExposedVars(loc);
        }
        
    }
    
    private void assignLocationToExposedVars (SVMXC__Site__c loc) {
        shippingToLoc = loc.id;
        state = loc.SVMXC__State__c;
        city = loc.SVMXC__City__c;
        zip = loc.SVMXC__Zip__c ;
        street =  loc.SVMXC__Street__c;
        country = loc.SVMXC__Country__c;        
    }
    
    public void ch(){
        SVMXC__Site__c loc;
        if (wo.SVMX_From_SAP_Storage_Location__c == null) {
            loc = new SVMXC__Site__c(Name = '', SVMXC_Plant_Number__c = '', SVMXC_Global_Location_Name__c = '', SVMXC_SAP_Storage_Location__c = '');
        } else {
          loc = [select Name, SVMXC_Plant_Number__c, SVMXC_Global_Location_Name__c, SVMXC_SAP_Storage_Location__c  from SVMXC__Site__c where id =:wo.SVMX_From_SAP_Storage_Location__c];
        }
        FromPlant = loc.SVMXC_Plant_Number__c;
        sapnum = loc.SVMXC_SAP_Storage_Location__c ;
        glnNum = loc.SVMXC_Global_Location_Name__c;
        locName = loc.name;
        Locid = loc.id;
        system.debug(' ## From LOc in change ' +locid);
        
    }
    
    public void change(){
        SVMXC__Site__c loc;
        if (wo.SVMX_To_Stock_Location__c == null) {
            loc = new SVMXC__Site__c (SVMXC__City__c = '', SVMXC__Country__c = '', SVMXC__State__c = '', SVMXC__Street__c = '', SVMXC__Zip__c = '');
        } else {
            loc = [select SVMXC__City__c , SVMXC__Country__c , SVMXC__State__c , SVMXC__Street__c , SVMXC__Zip__c from SVMXC__Site__c where id =: wo.SVMX_To_Stock_Location__c];
        }
        assignLocationToExposedVars(loc);
    }
    
    public pageReference Back(){
         
        pagereference p =  new pagereference('/'+workOrderId); 
        return p;
    }
    
    public pageReference ChkAvailability(){
        boolean ch = false;
        //Map<string,integer> materialQty = new Map<string,integer>();
        Map<String,SAPPO_SalesOrderService.ATPCheckMapping> materialMapping = new Map<String,SAPPO_SalesOrderService.ATPCheckMapping>();       
        SVMX_Sales_Office__c so = new SVMX_Sales_Office__c();
        for(WrapWD w: workdetailWrap){
            if(w.sel==true){
                ch = true;
                if(!materialMapping.containskey(w.prodSapnum) || materialMapping.get(w.prodSapnum).quantity  < w.qty){
                    materialMapping.put(w.prodSapnum,new SAPPO_SalesOrderService.ATPCheckMapping(integer.valueOf(w.qty),sapnum,FromPlant));
                }
                w.avlDate = '';
                w.avlqty = '';
                w.availableFrmLocName = '';
            }
        }
        
        if(wo.SVMX_From_SAP_Storage_Location__c==null){
           apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Select From SAP Storage Location to Check Availability')); 
        }
        else if(!ch){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Select one or more items to Check Availability'));
        }
        else if(wo.SVMX_From_SAP_Storage_Location__c == reqAtLoc){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'From location cannot be same as To location'));
        }
        else if(ch){
            SAPPO_SalesOrderService priceService = new SAPPO_SalesOrderService();
            so = [select SVMX_Price_Order_Account__r.SAPNumber__c  from SVMX_Sales_Office__c  where SVMX_Country__c =:wo.SVMXC__Country__c ]; 
            string accSAPnum = so.SVMX_Price_Order_Account__r.SAPNumber__c;
            List<Map<String,String>> results = new List<Map<String,String>>();
            try{
                results = priceService.getATPCheck(accSAPnum, ReqDate, glnNum, materialMapping).data;
                system.debug(' ## response '+results);
            }
            catch(System.CalloutException e){
                apexpages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'No Response from SAP'));
                return null;
            }
            if(!results.isEmpty()) {
                for(WrapWD w: workdetailWrap){ 
                    for(Map<String,String> rs:results){
                        if(w.sel == true && rs.get('MaterialId') == w.prodSapnum && decimal.valueof(rs.get('ConfirmedQuantity')) >= w.qty){ 
                            w.avlDate = rs.get('AvailableOnDate');
                            //w.avlqty = rs.get('ConfirmedQuantity');
                            w.avlqty = string.valueOf(w.qty);
                            w.availableFrmLocName = locName;
                        }
                    }
                }
            }
            else{
                 apexpages.addmessage(new ApexPages.message(ApexPages.severity.Warning,'No Availability from SAP'));
            }
        }
        return null;
    }
    
    public pagereference PartOrder(){
        if(wo.SVMX_Default_Stock_Location__c == null){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.Error,'To Stocking Location is Blank'));
            return null;
        }
        Map<string,SVMXC__RMA_Shipment_Order__c > partOrderMap = new Map<string,SVMXC__RMA_Shipment_Order__c >();
        List<SVMXC__RMA_Shipment_Line__c > partOrLineList = new List<SVMXC__RMA_Shipment_Line__c>();
        List<SVMXC__Service_Order_Line__c> wdLines = new List<SVMXC__Service_Order_Line__c>();
        Boolean selec =false;
        id shipRecTypeId;
        id shipRecId = SVMX_RecordTypeDataManager.GetRecordTypeID('Shipment','SVMXC__RMA_Shipment_Order__c');
        for(WrapWD w: workdetailWrap){
            if(w.sel == true && (w.availableFrmLocname == null || w.availableFrmLocname == '' )){
                apexpages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Available from location is blank for a selected Item'));
                return null;
            }
            else if(w.sel==true && !partOrderMap.containsKey(w.availableFrmLoc)){
                SVMXC__RMA_Shipment_Order__c prtOrder = new SVMXC__RMA_Shipment_Order__c();
                prtOrder.SVMXC__Destination_Location__c = wo.SVMX_Default_Stock_Location__c;
                prtOrder.SVMX_Shipping_Location__c = shippingToLoc;
                prtOrder.RecordTypeId = shipRecId;
                prtOrder.Language__c = wo.SVMX_Language__c ;
                prtOrder.SVMXC__Source_Location__c = locId;
                prtOrder.SVMXC__Destination_City__c = city;
                prtOrder.SVMXC__Destination_Country__c = country;
                prtOrder.SVMXC__Destination_State__c = state;
                prtOrder.SVMXC__Destination_Zip__c = zip;
                prtOrder.SVMXC__Destination_Street__c =street;
                prtOrder.SVMXC__Service_Order__c = workOrderId;
                if(woOrderType == System.Label.Spare_Parts_Order)
                    prtOrder.SVMXC__Order_Type__c = System.Label.Spare_Parts_Order;
                else
                    prtOrder.SVMXC__Order_Type__c = System.Label.Stock_Part;
               // prtOrder.SVMXC_Approved__c = true;
                partOrderMap.put(w.availableFrmLoc,prtOrder);
                selec = true;
                
                system.debug(' ### Source Loc'+locId);
                system.debug(' ### Destination Loc'+wo.SVMX_To_Stock_Location__c);
            }
        }
        
        if(!selec){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Select one or more items to create SAP Shipment'));
            return null;
        }
        
        if(selec){
            shipRecTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Shipment','SVMXC__RMA_Shipment_Line__c');
        }
        
        if(!partOrderMap.isEmpty()) {
            insert partOrderMap.values();
            for(WrapWD w: workdetailWrap){
                if(w.sel==true && partOrderMap.containsKey(w.availableFrmLoc)){
                    SVMXC__RMA_Shipment_Line__c prOLine = new SVMXC__RMA_Shipment_Line__c();
                    prOLine.SVMXC__Product__c = w.prod;
                    prOLine.SVMXC__Expected_Quantity2__c = decimal.valueOf(w.avlQty);
                    prOLine.SVMXC__Expected_Receipt_Date__c = date.valueOf(w.avlDate);
                    prOLine.SVMXC__RMA_Shipment_Order__c  = partOrderMap.get(w.availableFrmLoc).id;
                    prOLine.SVMX_ProductDetail__c = w.prodDetail;
                    prOLine.RecordTypeId = shipRecTypeId;
                    if(woOrderType == System.Label.Spare_Parts_Order)
                        prOLine.SVMXC__Line_Type__c = System.Label.Spare_Parts_Order;
                    else
                        prOLine.SVMXC__Line_Type__c = System.Label.Stock_Part;
                    partOrLineList.add(prOLine);
                    SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
                    wd.SVMX_Request_Lines_Converted__c = true;
                    wd.id = w.wdId;
                    wdLines.add(wd);
                }
            } 
        }
            
        if(!partOrLineList.isEmpty())
            insert partOrLineList;
        
        if(!wdLines.isEmpty())
            update wdLines;
        
        pagereference p =  new pagereference('/'+workOrderId); 
        return p;
        
        
    }
}