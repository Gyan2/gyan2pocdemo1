public class SVMX_PartsOrderDataManager {   
     public static list<SVMXC__RMA_Shipment_Order__c> ParOrdQuery(set<id> ParOrdIdset){
        list<SVMXC__RMA_Shipment_Order__c> parOrdlist = new list<SVMXC__RMA_Shipment_Order__c>();

         parOrdlist = [SELECT id,Name,SVMXC__Company__c,SVMXC__Company__r.name,SVMXC__Service_Order__c,SVMXC__Source_Location__c,
                       SVMXC__Destination_Location__c, SVMXC__Case__c,SVMX_Service_Sales_Order_Number__c, 
                       SVMXC__Case__r.SVMX_Customer_PO_Number__c, SVMXC__Case__r.SVMX_Invoice_Status__c,SVMXC__Case__r.CaseNumber,
                       SVMXC__Company__r.SAPNumber__c,SVMXC__Case__r.SVMXC__Site__r.SVMXC_Global_Location_Name__c, 
                       SVMX_Purchase_Requisition_Number__c, SVMXC__Destination_Location__r.SVMXC_SAP_Storage_Location__c, 
                       SVMXC__Destination_Street__c, SVMXC__Destination_City__c, SVMXC__Destination_State__c,
                       SVMXC__Destination_Zip__c, SVMXC__Destination_Country__c, SVMXC__Destination_Location__r.SVMXC__Street__c, 
                       SVMXC__Destination_Location__r.SVMXC__City__c, SVMXC__Destination_Location__r.SVMXC__State__c,
                       SVMXC__Destination_Location__r.SVMXC__Zip__c, SVMXC__Destination_Location__r.SVMXC__Country__c, 
                       SVMXC__Expected_Receive_Date__c, SVMX_Vendor__r.SAPNumber__c, SVMXC__Destination_Location__r.SVMX_SAP_Number__c,
                       SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Customer_PO_Number__c, SVMXC__Destination_Location__r.Name,
                       SVMX_Shipping_Location__r.SVMXC_Plant_Number__c, SVMX_Shipping_Location__r.SVMXC_SAP_Storage_Location__c,
                       SVMX_Shipping_Location__r.Name
                       FROM SVMXC__RMA_Shipment_Order__c  
                       WHERE Id in:ParOrdIdset];
        
        return parOrdlist;
    }

    public static list<SVMXC__RMA_Shipment_Order__c> workorderparOrdQuery(set<id> workOrderIdset){
        list<SVMXC__RMA_Shipment_Order__c> parOrdlist = new list<SVMXC__RMA_Shipment_Order__c>();

         parOrdlist = [SELECT id,Name,SVMXC__Company__c,SVMXC__Company__r.name,SVMXC__Service_Order__c,SVMXC__Source_Location__c,
                       SVMXC__Destination_Location__c 
                       FROM SVMXC__RMA_Shipment_Order__c  
                       WHERE SVMXC__Service_Order__c in:workOrderIdset];
        
        return parOrdlist;
    }

    public static void updatepartsOrder(list<SVMXC__RMA_Shipment_Order__c> PartsOrderList){
        if(!PartsOrderList.isEmpty()){
            update PartsOrderList;
        }    
    }          
}