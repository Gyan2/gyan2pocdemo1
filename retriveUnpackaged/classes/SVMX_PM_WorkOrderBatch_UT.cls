/************************************************************************************************************
Description: Test class for SVMX_PM_WorkOrderBatch class
 
Author: Pooja Singh
Date: 27-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest
public class SVMX_PM_WorkOrderBatch_UT { 
 
    static testMethod void testPmWoBatch()
    {   

        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'ServiceContract';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
              
        SVMX_SAP_Integration_UserPass__c cs1 = new SVMX_SAP_Integration_UserPass__c();
              cs1.name ='SAP PO';
              cs1.WS_USER__c ='test';
              cs1.WS_PASS__c ='test';
        insert cs1;

        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Service Contract Site';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Start_Date__c';
        insert trigconfig2;

       
        Account acc = SVMX_TestUtility.CreateAccount('Test account', 'United Kingdom', true);
        Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('test location', acc.Id, true);
        
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);

        //SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, true);
        //Create product
        Product2 p = SVMX_TestUtility.CreateProduct('Test Product', true);

        Profile prof = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
        User u = SVMX_TestUtility.CreateUser('testTech', prof.Id, false);
        u.UserCountry__c ='US';
        u.SalesOrg__c ='9999';
        insert u;

        SVMXC__Service_Group__c servGp = SVMX_TestUtility.CreateServiceGroup('Test Service Group', true);

        //Create technician
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Test Tech', u.Id, servGp.id, true);
        
       

        //Create IP
        SVMXC__Installed_Product__c ip = SVMX_TestUtility.CreateInstalledProduct(acc.Id, p.Id, loc.Id, true);
        
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Service Contract', 'Global Std', con.Id, true);
        
        
        
        List<SVMXC__PM_Plan__c> pmPlanList = new List<SVMXC__PM_Plan__c>();
        SVMXC__PM_Plan__c pmPlan = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
        pmPlan.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
        pmPlanList.add(pmPlan);
        SVMXC__PM_Plan__c pmPlan1 = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
        pmPlan1.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
        pmPlan1.SVMX_Converted_Plan__c = true;
        pmPlanList.add(pmPlan1);
        insert pmPlanList;
        
        Case c = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', true);
        Date startDate=system.today();
        SVMXC__Service_Contract_Sites__c  serContrSite=SVMX_TestUtility.CreateServiceContractSite(sc.Id,'Monthly on First of Month',startDate,false);
        serContrSite.SVMX_Pricing_Methos__c='Time Based';
        insert serContrSite;
        
        
        List<SVMXC__Service_Order__c> woList = new List<SVMXC__Service_Order__c>();
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Open', 'Field Service', 'Global Std', sc.Id, tech.Id, c.Id, false);
        wo.SVMXC__PM_Plan__c = pmPlan.Id;
        woList.add(wo);
        SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Open', 'Field Service', 'Global Std', sc.Id, tech.Id, c.Id, false);
        wo1.SVMXC__PM_Plan__c = pmPlan.Id;
        woList.add(wo1);
        
        insert woList;
        SVMXC__Service_Contract_Products__c covProd = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c = sc.Id, SVMXC__Installed_Product__c = ip.Id, SVMXC__Product__c = p.Id);
        insert covProd;
        Test.startTest();

            Test.setMock(WebServiceMock.class, new SAPServiceContractUtilityMock());
            SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader messageheader = new SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader();
            SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract serviceContractUpdatereq = new SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract();
            SVMX_wwwDormakabaComContract.ServiceContractOutPort requstelemnt = new SVMX_wwwDormakabaComContract.ServiceContractOutPort();
            requstelemnt.UpdateSync(messageheader,serviceContractUpdatereq);

            SVMX_PM_WorkOrderBatch obj = new SVMX_PM_WorkOrderBatch();
            DataBase.executeBatch(obj);

        Test.stopTest();
    }
     static testMethod void testPmWoCVPBatch()
    {   
        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'ServiceContract';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
              
        SVMX_SAP_Integration_UserPass__c cs1 = new SVMX_SAP_Integration_UserPass__c();
              cs1.name ='SAP PO';
              cs1.WS_USER__c ='test';
              cs1.WS_PASS__c ='test';
        insert cs1;

        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Service Contract Site';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Start_Date__c';
        insert trigconfig2;
       
        Account acc = SVMX_TestUtility.CreateAccount('Test account', 'United Kingdom', true);
        Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('test location', acc.Id, true);
        
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);

        //SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, true);
        //Create product
        Product2 p = SVMX_TestUtility.CreateProduct('Test Product', true);

        Profile prof = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
        User u = SVMX_TestUtility.CreateUser('testTech', prof.Id, false);
        u.UserCountry__c ='US';
        u.SalesOrg__c ='9999';
        insert u;

        SVMXC__Service_Group__c servGp = SVMX_TestUtility.CreateServiceGroup('Test Service Group', true);

        //Create technician
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Test Tech', u.Id, servGp.id, true);
        
       

        //Create IP
        SVMXC__Installed_Product__c ip = SVMX_TestUtility.CreateInstalledProduct(acc.Id, p.Id, loc.Id, true);
        
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Service Contract', 'Global Std', con.Id, true);
        
        SVMXC__Service__c avialservice = new SVMXC__Service__c();
        avialservice.SVMXC__Service_Type__c = 'Planned Services (PPM)';
        insert avialservice;
        
        SVMXC__Service_Contract_Services__c inl = new SVMXC__Service_Contract_Services__c();
        inl.SVMXC__Service_Contract__c = sc.id;
        inl.SVMXC__Service__c= avialservice.id;
        inl.SVMX_Type__c = 'Planned Services (PPM)';
        insert inl;
        
        Case c = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'Ready To Invoice', true);
        Date startDate=system.today();
        SVMXC__Service_Contract_Sites__c  serContrSite=SVMX_TestUtility.CreateServiceContractSite(sc.Id,'Monthly on First of Month',startDate,false);
        serContrSite.SVMX_Pricing_Methos__c='Time Based';
        insert serContrSite;
        
        List<SVMXC__PM_Plan__c> pmPlanList = new List<SVMXC__PM_Plan__c>();
        SVMXC__PM_Plan__c pmPlan = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
        pmPlan.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
        pmPlan.SVMX_Converted_Plan__c = true;
        pmPlanList.add(pmPlan);
        SVMXC__PM_Plan__c pmPlan1 = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
        pmPlan1.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
        pmPlan1.SVMX_Converted_Plan__c = true;
        pmPlanList.add(pmPlan1);
        insert pmPlanList;
        
        List<SVMXC__Service_Order__c> woList = new List<SVMXC__Service_Order__c>();
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Open', 'Field Service', 'Global Std', sc.Id, tech.Id, c.Id, false);
        wo.SVMXC__PM_Plan__c = pmPlan.Id;
        woList.add(wo);
        SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Open', 'Field Service', 'Global Std', sc.Id, tech.Id, c.Id, false);
        wo1.SVMXC__PM_Plan__c = pmPlan.Id;
        woList.add(wo1);
        
        insert woList;
        SVMXC__Service_Contract_Products__c covProd = new SVMXC__Service_Contract_Products__c(SVMXC__Service_Contract__c = sc.Id, SVMXC__Installed_Product__c = ip.Id, SVMXC__Product__c = p.Id);
        insert covProd;
     
        Test.startTest();

            Test.setMock(WebServiceMock.class, new SAPServiceContractUtilityMock());
            SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader messageheader = new SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader();
            SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract serviceContractUpdatereq = new SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract();
            SVMX_wwwDormakabaComContract.ServiceContractOutPort requstelemnt = new SVMX_wwwDormakabaComContract.ServiceContractOutPort();
            requstelemnt.UpdateSync(messageheader,serviceContractUpdatereq);
            
            SVMX_PM_WorkOrderBatch obj = new SVMX_PM_WorkOrderBatch();
            DataBase.executeBatch(obj);

        Test.stopTest();
    }
   
}