public with sharing class AccountServices {

    public static void countContacts(List<Account> Records) {
        Map<Id, Integer> contactsPerAccount = new Map<Id, Integer>();

        for (AggregateResult forAG : [SELECT COUNT(Id) contactsCount, AccountId FROM Contact WHERE AccountId IN :Records GROUP BY AccountId]) {
            contactsPerAccount.put((Id)forAg.get('AccountId'), (Integer)forAG.get('contactsCount'));
        }

        for (Account forAcc : Records) {
            Integer count = contactsPerAccount.get(forAcc.Id);
            if (count == null) count = 0;
            forAcc.ContactsCount__c = count;
        }
    }

    public static void applyInternationalPhonePrefix(List<Account> paramRecords) {
        for (Account forAccount : paramRecords) {
            if (String.isBlank(forAccount.PhoneCountryCode__c)) {
                forAccount.PhoneCountryCode__c = forAccount.BillingCountryCode;
            }
            if (String.isBlank(forAccount.FaxCountryCode__c)) {
                forAccount.FaxCountryCode__c = forAccount.BillingCountryCode;
            }
        }
    }

    public static void splitInternationalPrefix(List<Account> paramAccounts) {
        for (Account forAccount : paramAccounts) {
            if (!String.isBlank(forAccount.PhoneCountryCode__c) && !String.isBlank(forAccount.Phone)) {
                forAccount.PhoneWithoutInternationalPrefix__c = forAccount.Phone.removeStart(Util.Countries.getByCode(forAccount.PhoneCountryCode__c).InternationalPhonePrefix__c).trim();
            }

            if (!String.isBlank(forAccount.FaxCountryCode__c) && !String.isBlank(forAccount.Fax)) {
                forAccount.FaxWithoutInternationalPrefix__c = forAccount.Fax.removeStart(Util.Countries.getByCode(forAccount.FaxCountryCode__c).InternationalPhonePrefix__c).trim();
            }
        }
    }

    public static void validateInternationalPhonePrefix(List<Account> paramRecords) {
        Set<String> countryCodes = new Set<String>();
        for (Account forAccount : paramRecords) {
            countryCodes.add(forAccount.PhoneCountryCode__c);
            countryCodes.add(forAccount.FaxCountryCode__c);
            //forAccount.addError('testing');
        }

        Map<String,String> phonePrefixMapping = new Map<String, String>(); // Map<{Country Code} , {InternationalPhonePrefix} >
        for (Country__mdt forCountry : [SELECT DeveloperName, InternationalPhonePrefix__c FROM Country__mdt WHERE DeveloperName IN :countryCodes]) {
            phonePrefixMapping.put(forCountry.DeveloperName, forCountry.InternationalPhonePrefix__c);
        }

        for (Account forAccount : paramRecords) {
            String forPhonePrefix = phonePrefixMapping.get(forAccount.PhoneCountryCode__c);
            if (forPhonePrefix != null) { // if there is a validation for that phone country code
                if (forAccount.Phone != null && !forAccount.Phone.startsWith(forPhonePrefix)) {
                    forAccount.Phone.addError(String.format('Must start with {0}', new List<String>{forPhonePrefix}));
                }
            }

            String forFaxPrefix = phonePrefixMapping.get(forAccount.FaxCountryCode__c);
            if (forFaxPrefix != null) {
                if (forAccount.Fax != null && !forAccount.Fax.startsWith(forFaxPrefix)) {
                    forAccount.Fax.addError(String.format('Must start with {0}', new List<String>{forFaxPrefix}));
                }
            }
        }
    }

    public static void manageQuoteLanguages(List<Account> accounts,Map<Id,Account> existingRecords){
        Map<Id,String> mapAccountIdLanguages = new Map<Id,String>();
        for(Account account : accounts){
            mapAccountIdLanguages.put(account.Id,Account.Language__c);
        }

        List<Quote> quoteToUpdate = [select id,AccountId,Languages__c from Quote where AccountId in: mapAccountIdLanguages.keySet()];
        for(Quote quote : quoteToUpdate){
            quote.Languages__c = mapAccountIdLanguages.get(quote.AccountId);
        }
        update quoteToUpdate;
    }
}