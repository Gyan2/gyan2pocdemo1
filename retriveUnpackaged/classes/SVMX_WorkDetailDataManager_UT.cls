@isTest
public class SVMX_WorkDetailDataManager_UT {

    public static testMethod void TestWDManager() {
        
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
      
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
           
        Case cs = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', true);
       
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, true);
        
        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        SVMX_Rate_Tiers__c rt2 = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, true, true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Germany',true);
        
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Reactive','Global Std', sc.Id, tech.Id, cs.id, false);
        wo.SVMXC__Country__c = 'Germany' ;
       
        SVMXC__Expense_Pricing__c expencePricing = new SVMXC__Expense_Pricing__c() ;
        expencePricing.SVMXC__Expense_Type__c = 'Parts';
        expencePricing.SVMXC__Service_Contract__c = sc.id ;
        expencePricing.SVMXC__Rate__c = 20.0;
        expencePricing.SVMXC__Rate_Type__c = 'Markup %';
        insert expencePricing ;
        
        //SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Planned Services (PPM)','Global Std', sc.Id, tech.Id, cs.id, true);
        Map<id,List<SVMXC__Service_Order_Line__c>> wdLineMap = new Map<id,List<SVMXC__Service_Order_Line__c>>() ;
        List<SVMXC__Service_Order_Line__c> wdList = new List<SVMXC__Service_Order_Line__c>();
        SVMXC__Service_Order_Line__c oli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',false);
        oli1.SVMXC__Start_Date_and_Time__c = system.now() ;
        oli1.SVMXC__Expense_Type__c = 'Parts';
        wdList.add(oli1);
        
        SVMXC__Service_Order_Line__c oli3 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Estimate', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',false);
        oli3.SVMXC__Start_Date_and_Time__c = system.now() ;
        oli1.SVMXC__Expense_Type__c = 'Parts';
        oli3.SVMX_No_of_Technicians__c = 1;
        wdList.add(oli3);
        
        wdLineMap.put(wo.id,wdList);
        insert oli1 ;
        insert oli3 ;
        
        
        Set<id> laborLineId = new Set<id>();
        SVMXC__Service_Order_Line__c oli2 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Expenses','Goodwill',false);
        oli2.SVMXC__Start_Date_and_Time__c = system.now() ;
        oli1.SVMXC__Expense_Type__c = 'Parts';
        insert oli2 ;

        SVMX_WorkDetailDataManager.laborLinesQuery(wdLineMap,laborLineId) ;
        SVMX_WorkDetailDataManager.ExpenseMarkupQuery(wdList);
        //SVMXC__Service_Order_Line__c oli3 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Travel','Non Warranty',false);
        //SVMXC__Service_Order_Line__c oli4 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Estimate', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',false);
        //SVMXC__Service_Order_Line__c oli5 = SVMX_TestUtility.CreateWorkOrderDetailEstimate(acc.Id, loc.Id, prod.Id, 'Estimate', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',2,true,false);
    }
}