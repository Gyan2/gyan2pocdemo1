/*Author : Keshava Prasad
  Date   : 13/04/2018
*/


@isTest
public class SVMX_EntitlementHistoryDataManager_UT {


public static testMethod void insertEntitlementHistory(){

SVMXC__Entitlement_History__c entitlementHistory = new SVMXC__Entitlement_History__c() ;
Date todayDate = system.today();
entitlementHistory.SVMXC__Date_of_entitlement__c = todayDate ;

//SVMX_EntitlementHistoryDataManager entitleMentHistoryDataManager = new SVMX_EntitlementHistoryDataManager();

List<SVMXC__Entitlement_History__c> entitlementHistoryList = new List<SVMXC__Entitlement_History__c>() ;

entitlementHistoryList.add(entitlementHistory) ;

SVMX_EntitlementHistoryDataManager.createEntitleHistory(entitlementHistoryList) ;

System.assert(!entitlementHistoryList.isEmpty(),'Entitlement List is empty') ;

}





/*private SVMXC__Entitlement_History__c createEntitlementHistory() {


return entitlementHistory ;
}*/


}