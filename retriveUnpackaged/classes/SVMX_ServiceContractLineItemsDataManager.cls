public class SVMX_ServiceContractLineItemsDataManager{
    
    public static List<SVMX_Contract_Line_Items__c> SCLIneItemsQuery(set<id> ContractLineId){
        List<SVMX_Contract_Line_Items__c> serviceConLineItemsMap = new List<SVMX_Contract_Line_Items__c>([select Id,name, SVMX_Is_Billable__c ,SVMX_Discount__c, SVMX_Total_Line_Price__c, SVMX_SAP_Material_Number__c, SVMX_Billing_schedule__c ,SVMX_Contract_Start__c, SVMX_Contract_End__c, SVMX_Billing_Start_Billing_End__c, SVMX_Line_Price__c,CurrencyIsoCode,SVMX_Service_Contract__c,SVMX_Line_Qty__c, SVMX_SAP_Contract_Item_Number__c, SVMX_Service_Contract__r.SVMXC__Company__r.Language__c, SVMX_Product__r.Name, SVMX_Billing_End_Date__c,SVMX_SAP_Status__c from SVMX_Contract_Line_Items__c where Id IN :ContractLineId]);
        return serviceConLineItemsMap;
    }
    public static List<SVMX_Contract_Line_Items__c> timeBasedSCLIneItemsQuery(set<id> serviceContractId){
        List<SVMX_Contract_Line_Items__c> serviceConLineItemsMap = new List<SVMX_Contract_Line_Items__c>([select Id,name, SVMX_Is_Billable__c ,SVMX_Discount__c, SVMX_Total_Line_Price__c, SVMX_SAP_Material_Number__c, SVMX_Billing_schedule__c ,SVMX_Contract_Start__c, SVMX_Contract_End__c, SVMX_Billing_Start_Billing_End__c, SVMX_Line_Price__c,CurrencyIsoCode,SVMX_Service_Contract__c,SVMX_Line_Qty__c, SVMX_SAP_Contract_Item_Number__c, SVMX_Service_Contract__r.SVMXC__Company__r.Language__c, SVMX_Product__r.Name, SVMX_Billing_End_Date__c,SVMX_SAP_Status__c from SVMX_Contract_Line_Items__c where SVMX_Service_Contract__c IN :serviceContractId AND SVMX_Is_Billable__c = True]);
        return serviceConLineItemsMap;
    }  
    public static List<SVMX_Contract_Line_Items__c> timeBasedSCLIneItemsBlankcheckQuery(set<id> serviceContractId){
        List<SVMX_Contract_Line_Items__c> serviceConLineItemsMap = new List<SVMX_Contract_Line_Items__c>([select Id, SVMX_Is_Billable__c ,name,SVMX_Discount__c, SVMX_Total_Line_Price__c, SVMX_SAP_Material_Number__c, SVMX_Billing_schedule__c ,SVMX_Contract_Start__c, SVMX_Contract_End__c, SVMX_Billing_Start_Billing_End__c, SVMX_Line_Price__c,CurrencyIsoCode,SVMX_Service_Contract__c,SVMX_Line_Qty__c, SVMX_SAP_Contract_Item_Number__c, SVMX_Service_Contract__r.SVMXC__Company__r.Language__c, SVMX_Product__r.Name, SVMX_Billing_End_Date__c,SVMX_SAP_Contract_Item_Num_Blank_Check__c,SVMX_SAP_Status__c from SVMX_Contract_Line_Items__c where SVMX_Service_Contract__c IN :serviceContractId AND SVMX_SAP_Contract_Item_Num_Blank_Check__c = True]);
        return serviceConLineItemsMap;
    }
    
}