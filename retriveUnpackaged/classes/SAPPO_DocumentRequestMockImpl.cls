/**
 * Created by trakers on 14.06.18.
 */

public with sharing class SAPPO_DocumentRequestMockImpl implements WebServiceMock  {
    private Map<String,String> mapping;
    public SAPPO_DocumentRequestMockImpl(Map<String,String> mapping){
        this.mapping = mapping;
    }

    public void doInvoke(
            Object stub,
            Object request,
            Map<String, Object> response,
            String endpoint,
            String soapAction,
            String requestName,
            String responseNS,
            String responseName,
            String responseType) {

        SAPPO_Document.DocumentReadResponse response_x = this.generateResponse(this.mapping);

        response.put('response_x', response_x);
    }


    public SAPPO_Document.DocumentReadResponse generateResponse(Map<String,String> mapping){
        SAPPO_Document.DocumentReadResponse response_x = new SAPPO_Document.DocumentReadResponse();
        response_x.ArchiveObject = new SAPPO_Document.ArchiveObject();
        response_x.ArchiveObject.ObjectId = mapping.get('Id');
        response_x.ArchiveObject.ArchiveDocument = new SAPPO_Document.ArchiveDocuments();//new List<SAPPO_Document.ArchiveDocument>();
        response_x.ArchiveObject.ArchiveDocument.item = new List<SAPPO_Document.ArchiveDocument>();

        SAPPO_Document.ArchiveDocument item = new SAPPO_Document.ArchiveDocument();
                                       item.Payload = mapping.get('Payload');

        response_x.ArchiveObject.ArchiveDocument.item.add(item);

        return response_x;
    }
}