/*******************************************************************************************************
Description: Scheduler class for SVMX_TimesheetBatch class

Dependancy:    
  Class: SVMX_TimesheetBatch
 
Author: Pooja Singh
Date: 15-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

global class SVMX_TimesheetBatch_Scheduler implements Schedulable {
    
    // Execute at regular intervals
    global void execute(SchedulableContext sc){
      SVMX_TimesheetBatch tsbatch = new SVMX_TimesheetBatch();
      Database.executebatch(tsbatch, 200);
    }
}