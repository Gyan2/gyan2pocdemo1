/*******************************************************************************************************
* Class Name      	: WS001_CustomerMaster
* Description     	: Data Manager for Customer Master Web Service (Initial version - is not optimised for > DML Limits  )
* Author          	: Paul Carmuciano
* Created On      	: 2017-09-27
* Modification Log	:
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-09-27         1000                 Initial version (only supports updates)
* Paul Carmuciano		2017-10-15         1001                 Updated webservice element definitions & for inserts
* Paul Carmuciano		2017-11-29         2000                 Added support for Contacts
* Sergio Alcocer        2017-12-20                              Added support for VENDOR Accounts
******************************************************************************************************/

global abstract class WS001_CustomerMaster {

    // Question: for Customer, do we assume SAP passes the full details, or do we need to keep flags such as Delete Flag in sales areas and partner roles?
    // Answer=> I will always transfer full data of customer; e.g. if accountName2 is blank then please delete accountName2.

    // The response object for Customer Master interface
    global class CustomerMasterRequest {
        webservice List<CustomerMaster> customers;
    }

    // A response object for Customer Master interface
    global class CustomerMasterResponse {
        webservice boolean success; // True if no errors were encountered
        webservice list<CustomerMasterError> errors; // a list of errors encountered
    }

    /***
* Method name	: CustomerMaster Apex Type
* Description	: Constructs a Customer Master records in aligmnet with PO
* Author		: Paul Carmuciano
* Return Type	: N/A
* Parameter		: N/A
*/

    Map<String, System.Type> accountGroupMapping = new Map<String, System.Type> {
        'ZSOP' => WS001_CustomerMaster.AccountHandler.class,
        'VEND' => WS001_CustomerMaster.AccountHandler.class,
        'ZLOC' => WS001_CustomerMaster.LocationHandler.class
    };

    private class AccountHandler extends WS001_CustomerMaster {

    }

    global class CustomerMaster {
        webservice String accountGroup; /*
YPRO	DOKA Project (Local)
YSAL	DOKA Sales Teams (Local)
YVER	DOKA Verticals (Local)
ZACF	DOKA Aff. companies (FIN)
ZACT	DOKA Aff. companies (TRADE)
ZARC	DOKA Architects/Contractor
ZBLP	DOKA Invoices recipient
ZCPD	DOKA One-time Customer
ZOBJ	DOKA Object
ZPAR	DOKA MKS Partner
ZPLA	DOKA Plant cust. Stock-Trans.
ZPRC	DOKA Prospective customer
ZPRO	DOKA Project (Global)
ZSHP	DOKA Goods recipient
ZSOP	DOKA Sold-to party
VEND    Vendor
ZLOC    Location
0001    Account from PP1
WELE    Location from PP1
*/
        webservice String accountName2;
        webservice String accountName3;
        webservice String accountName4;
        webservice String authorizationGroup;
        webservice Boolean billingBlock;
        webservice String billingBlockReason;
        webservice String city;
        webservice String country;
        webservice String countryCode;
        webservice Boolean deletionFlag;
        webservice Boolean deliveryBlock;
        webservice String deliveryBlockReason;
        webservice String corporateEmail;
        webservice String faxCountryCode;
        webservice String fax;
        webservice String language;
        webservice String name;
        webservice String sapInstance;
        webservice String sapNumber;
        webservice Boolean orderBlock;
        webservice String orderBlockType;
        webservice String phoneCountryCode;
        webservice String phone;
        webservice String salesChannel;
        webservice String stateProvince;
        webservice String stateProvinceCode;
        webservice String street;
        webservice String poBox;
        webservice String poBoxCity;
        webservice String poBoxStateProvince;
        webservice String poBoxStateProvinceCode;
        webservice String poBoxZipPostalCode;
        webservice String taxRegNumber1;
        webservice String taxRegNumber2;
        webservice String vatRegNumber;
        webservice String website;
        webservice String businessCategory;
        webservice String businessType;
        webservice String zipPostalCode;

        webservice List<SalesArrangement> salesArrangements;
        webservice List<CustomerContact> customerContacts;
    }

    // Represents a Contact associated to the Customer, generally an employee
    global class CustomerContact {
        webservice String academicTitle;
        webservice Boolean deletionFlag;
        webservice String email;
        webservice String firstName;
        webservice String language;
        /* Note SAP language is 2 or 4 digit, we should map to SF langLocaleKey, ie de_DE, de_CH, nl_BE as it can drive outbound translations */
        webservice String lastName;
        webservice String mobilePhoneCountryCode;
        webservice String mobilePhone;
        webservice String phoneCountryCode;
        webservice String phone;
        webservice String positionTitle;
        webservice String primaryFunction;
        // primaryFunctionCode is not Sync
        webservice String primaryFunctionCode;
        webservice String sapNumber;
        webService String salutationCode;
    }

    // Represents a junction between a Customer and a Sales Area
    global class SalesArrangement {
        //webservice String salesArrangementPK;	// legacy concatenation
        //webservice String territoryFK;		// legacy concatenation
        webservice String abcClassification;
        webservice Boolean billingBlock;
        webservice String customerPricingGroup;
        webservice String customerDistrict;
        webservice Boolean defaultIndicator;
        webservice Boolean deletionFlag;
        webservice Boolean deliveryBlock;
        webservice String deliveryTerms;
        webservice String division;
        webservice String distributionChannel;
        webservice Boolean orderBlock;
        webservice String paymentTerms;
        webservice String pricingGroup;
        webservice String salesOrg;
        webservice String shippingConditions;
        webservice List<Party> parties;
    }

    // Represents a party associated to the Customer, i.e ship-to
    global class Party {
        webservice String customerNumber;
        webservice Boolean defaultIndicator;
        webservice String roleCode; // e.g. DP
        webservice String roleName; // e.g. Delivery Party
    }

    // specifies a single error message
    global class CustomerMasterError {
        webservice string message; // the specific error message
    }

    /***
* Method name	: generateErpRecordDefinition
* Description	: Helper method to produce an ERP Record JSON definition
* Author		: Paul Carmuciano
* Return Type	: String
* Parameter		: Object to json, DataType for Identifier
*/
    /*private static ERPRecord__c generateErpRecord(
        String sapInstance,
        String sapNumber,
        String recordName,
        String dataType,
        String parentId,
        Object o) {
            /* Instantiate an ERP Record sobject * /
            ERPRecord__c erpRec = new ERPRecord__c();
            erpRec.DataType__c = dataType; // Would either be dynamic, or implicit via implementation of interface
            erpRec.ERPId__c = sapNumber;
            erpRec.ERPInstance__c = sapInstance;
            erpRec.ERPRecordName__c = recordName;
            erpRec.UniqueIdentifier__c = sapInstance + '-' + sapNumber + '-' + dataType;
            erpRec.RecordDefinitionJSON__c = json.serializePretty(o, true);
            // FK Relate to parent, but there is no UI interction at this stage
            erpRec.ParentId__c = parentId;
            return erpRec;
        }*/

    // accepts a list of customers
    /***
* Method name	: upsertCustomer
* Description	: Processes incoming customers from PO. Iterates the payload twice;
					Firstly to DML any parent Record so we have ErpRecord.ParentIds (it is not a real db relationship)
					Secondly to process the ERP Records themselves
* Author		: Paul Carmuciano
* Return Type	: n/a
* Parameter		: CustomerMasterRequest customers
*/
    webservice static CustomerMasterResponse upsertCustomer(CustomerMasterRequest customers) {

        /* Collections of the objects for DML */
        //Map<String, ERPRecord__c> erpRecordMap = new Map<String, ERPRecord__c>();
        Map<String, Account> erpAccountMap = new Map<String, Account>();
        Map<String, Contact> erpContactMap = new Map<String, Contact>();
        Map<String, AccountSalesArea__c> erpSalesAreaMap = new Map<String, AccountSalesArea__c>();
        Map<String, Location__c> erpLocationMap = new Map<String, Location__c>();

        // For DML results
        Boolean dmlSuccess = true;
        Map<String, String> recordsFailedMap = new Map<String, String>(); // G_ID2__c / Error Message

        if(customers.customers != null) {
            Map<String,Account> existingAccounts = new Map<String,Account>();
            Set<String> accountExternalIdsReceived = new Set<String>();
            for (CustomerMaster forC : customers.customers) {
                accountExternalIdsReceived.add(forC.sapInstance + '-' + forC.sapNumber);
            }

            for (List<Account> accts : [SELECT Id, ExternalID__c FROM Account WHERE ExternalID__c IN :accountExternalIdsReceived]) {
                for (Account acct : accts)
                    existingAccounts.put(acct.ExternalID__c, acct);
            }


            // Iterate the payload to find External Ids, and pre-process accounts
            Set<String> externalIds = new Set<String>();
            for(CustomerMaster c: customers.customers) {

                // Process Accounts
                // Only dealing with Sold Tos who have Sales Arrangements (Note may need to change if we have Vendor Accounts)
                if(((c.accountGroup == 'ZSOP' || c.accountGroup == '0001') && c.salesArrangements!=null) || c.accountGroup == 'VEND'){

                    /* Instantiate an Account sobject */
                    Account erpAccount = new Account(); //Would need to consider RecordType handling
                    if (c.accountGroup == 'VEND') {
                        erpAccount.RecordTypeId = Util.RecordTypes.get('Account:VENDOR_ERP').Id;
                    } else {
                        erpAccount.RecordTypeId = Util.RecordTypes.get('Account:SOLDTO_ERP').Id;
                    }
                    erpAccount.BillingCity = c.city;
                    erpAccount.BillingCountryCode = c.countryCode;
                    erpAccount.BillingPostalCode = c.zipPostalCode;
                    //erpAccount.BillingState = c.stateProvince;
                    erpAccount.BillingStreet = c.street;
                    erpAccount.CorporateEmail__c = c.corporateEmail;
                    erpAccount.ExternalId__c = c.sapInstance + '-' + c.sapNumber;
                    erpAccount.Name = c.name;
                    erpAccount.SAPNumber__c = c.sapNumber;
                    erpAccount.SAPInstance__c = c.sapInstance;

                    erpAccount.Language__c = c.language;
                    erpAccount.AccountName2__c = c.accountName2;
                    erpAccount.PObox__c = c.poBox;
                    erpAccount.POboxCity__c = c.poBoxCity;
                    erpAccount.PhoneCountryCode__c = c.phoneCountryCode;
                    if (c.phoneCountryCode != null && c.phoneCountryCode != '')
                        erpAccount.Phone = Util.Countries.getByCode(c.phoneCountryCode).InternationalPhonePrefix__c + ' ' + c.Phone;

                    erpAccount.FaxCountryCode__c = c.faxCountryCode;
                    if (c.faxCountryCode != null && c.faxCountryCode != '')
                        erpAccount.Fax = Util.Countries.getByCode(c.faxCountryCode).InternationalPhonePrefix__c + ' ' + c.fax;

                    erpAccount.Status__c = (c.deletionFlag == true ? 'Inactive' : 'Active');
                    erpAccount.POboxPostalCode__c = c.poBoxZipPostalCode;


                    erpAccount.AccountName3__c = c.accountName3;
                    erpAccount.AccountName4__c = c.accountName4;
                    erpAccount.AuthorizationGroup__c = c.authorizationGroup;
                    erpAccount.BlockBilling__c = c.billingBlock;
                    erpAccount.BlockBillingReasonCode__c = c.billingBlockReason;
                    //erpAccount.CountryCode__c = c.countryCode;
                    erpAccount.BlockDelivery__c = Boolean.valueOf(c.deliveryBlock);
                    erpAccount.BlockDeliveryReasonCode__c = c.deliveryBlockReason;
                    erpAccount.BlockOrder__c = Boolean.valueOf(c.orderBlock);
                    erpAccount.BlockOrderReasonCode__c = c.orderBlockType;
                    erpAccount.SalesChannel__c = c.salesChannel;
                    erpAccount.BillingStateCode = c.stateProvinceCode;
                    //erpAccount.POboxStateProvinceCode__c = c.poBoxStateProvinceCode;

                    erpAccount.TaxNumber1__c = c.taxRegNumber1;
                    erpAccount.TaxNumber2__c = c.taxRegNumber2;
                    erpAccount.VatNumber__c = c.vatRegNumber;

                    erpAccount.Website = c.website;
                    erpAccount.BusinessCategory__c = c.businessCategory;
                    erpAccount.BusinessType__c = c.businessType;

                    erpAccount.SAPJSON__c = JSON.serializePretty(c, true);


                    if (c.salesArrangements != null && c.salesArrangements.size() > 0) {
                        try {
                            erpAccount.ManagementCountry__c = Util.SalesOrganizationMetaData.getCountryCodeBySalesOrgId(c.salesArrangements.get(0).salesOrg);
                            // TODO: parametrize based on the SalesOrg
                            if (erpAccount.ManagementCountry__c == 'NO' && !existingAccounts.containsKey(erpAccount.ExternalId__c)) {
                                erpAccount.CurrencyIsoCode = 'NOK';
                            }
                        } catch (Util.SalesOrganizationMetaDataException e) {
                            // TODO: Send e-mail or post into chatter group the error, until then...
                            throw e;
                        }
                    }
                    // add to the dml map
                    erpAccountMap.put(erpAccount.ExternalID__c, erpAccount);

                    // Process Contacts
                    if (c.customerContacts != null ) {
                        for (CustomerContact cc : c.customerContacts) {
                            /* Instantiate an Contact sobject */
                            Contact erpContact = new Contact(); //Would need to consider RecordType handling
                            /* Account related fields */
                            erpContact.Account = new Account(ExternalID__c=erpAccount.ExternalID__c);
                            /* Contact related fields */
                            //erpContact = cc.academicTitle;
                            erpContact.Email = cc.email;
                            erpContact.ExternalID__c = c.sapInstance + '-' + cc.sapNumber;
                            erpContact.FirstName = cc.firstName;
                            erpContact.Language__c = cc.language;
                            erpContact.LastName = cc.lastName;
                            erpContact.MailingCity = c.city;
                            erpContact.MailingCountryCode = c.countryCode;
                            erpContact.MailingPostalCode = c.zipPostalCode;
                            erpContact.MailingStateCode = c.stateProvinceCode;
                            erpContact.MailingStreet = c.street;

                            erpContact.MobilePhoneCountryCode__c = cc.mobilePhoneCountryCode;
                            if (cc.mobilePhone != null && cc.mobilePhone != '')
                                erpContact.MobilePhone = Util.Countries.getByCode(cc.mobilePhoneCountryCode).InternationalPhonePrefix__c + ' ' + cc.mobilePhone;

                            erpContact.PhoneCountryCode__c = cc.phoneCountryCode;
                            if (cc.phone != null && cc.phone != '')
                                erpContact.Phone = Util.Countries.getByCode(cc.phoneCountryCode).InternationalPhonePrefix__c + ' ' + cc.phone;

                            erpContact.Status__c = (cc.deletionFlag ? 'Inactive' : 'Active');
                            erpContact.Salutation = cc.positionTitle;
                            erpContact.SAPNumber__c = cc.sapNumber;

                            // NOTE: There are two functions, that have different fields on SAP. We get both, but we only send the free text
                            //erpContact.FunctionCode__c = cc.primaryFunctionCode;
                            erpContact.Title = cc.primaryFunction;

                            erpContact.AcademicTitle__c = cc.academicTitle;

                            // add to the dml map
                            erpContactMap.put(erpContact.ExternalID__c, erpContact);

                        }
                    }

                    if (c.SalesArrangements != null) {
                        for (SalesArrangement forSA : c.SalesArrangements) {
                            AccountSalesArea__c erpSalesArea = new AccountSalesArea__c();
                            erpSalesArea.AccountRef__r = new Account(ExternalID__c=erpAccount.ExternalID__c);
                            erpSalesArea.SalesAreaRef__r = new SalesArea__c(ExternalId__c = forSA.SalesOrg + '-' + forSA.distributionChannel);

                            erpSalesArea.AbcClassification__c = forSA.abcClassification;
                            erpSalesArea.BlockBilling__c = forSA.billingBlock;
                            erpSalesArea.CustomerPricingGroup__c = forSA.customerPricingGroup;

                            erpSalesArea.ExternalId__c = c.sapInstance + '-' + c.sapNumber + '-' + forSA.SalesOrg + '-' + forSA.distributionChannel;
                            erpSalesArea.CustomerDistrict__c = forSA.customerDistrict;
                            erpSalesArea.IsDefault__c = forSA.defaultIndicator;
                            erpSalesArea.IsDeleted__c = forSA.deletionFlag;
                            erpSalesArea.BlockDelivery__c = forSA.deliveryBlock;
                            erpSalesArea.DeliveryTerms__c = forSA.deliveryTerms;
                            erpSalesArea.Division__c = forSA.division;
                            //webservice String distributionChannel;
                            erpSalesArea.BlockOrder__c = forSA.orderBlock;
                            erpSalesArea.PaymentTerms__c = forSA.paymentTerms;
                            erpSalesArea.PricingGroup__c = forSA.pricingGroup;
                            //webservice String salesOrg;
                            erpSalesArea.ShippingConditions__c = forSA.shippingConditions;
                            //webservice List<Party> parties;
                            erpSalesArea.SAPJSON__c = JSON.serialize(forSA);
                            erpSalesAreaMap.put(erpSalesArea.ExternalId__c, erpSalesArea);
                        }
                    }

                } else if (c.accountGroup == 'ZLOC' || c.accountGroup == 'WELE'){

                    Location__c erpLocation = new Location__c();

                    erpLocation.SVMX_AuthorizationGroup__c = c.authorizationGroup;

                    erpLocation.Name = c.name;
                    erpLocation.SVMX_LocationName2__c = c.accountName2;
                    erpLocation.SVMX_LocationName3__c = c.accountName3;
                    erpLocation.SVMX_LocationName4__c = c.accountName4;

                    erpLocation.SVMXC_Street__c = c.street;
                    //erpLocation.SVMX_HouseNumber__c = c.houseNumber; TODO Check / Add houseNumber to Webservice
                    erpLocation.SVMXC_City__c = c.city;
                    erpLocation.SVMXC_Country__c = c.countryCode;
                    erpLocation.SVMXC_Zip__c = c.zipPostalCode;
                    //erpLocation.State !!
                    erpLocation.SVMXC_Email__c = c.corporateEmail;
                    erpLocation.ExternalID__c = c.sapInstance + '-' + c.sapNumber;
                    erpLocation.SAPNumber__c = c.sapNumber;
                    erpLocation.SAPInstance__c = c.sapInstance;

                    erpLocation.SVMX_Language__c = c.language;

                    erpLocation.SVMX_POBox__c = c.poBox;
                    erpLocation.SVMX_POBoxCity__c = c.poBoxCity;
                    erpLocation.SVMX_POBoxPostalCode__c = c.poBoxZipPostalCode;
                    erpLocation.Status__c = (c.deletionFlag == true ? 'Inactive' : 'Active');

                    erpLocation.SVMX_TaxRegistrationNumber1__c = c.taxRegNumber1;
                    erpLocation.SVMX_TaxRegistrationNumber2__c = c.taxRegNumber2;
                    erpLocation.SVMX_VATRegistrationNumber__c = c.vatRegNumber;
                    erpLocation.SAPJSON__c = JSON.serializePretty(c, true);

                    erpLocationMap.put(erpLocation.ExternalId__c, erpLocation);
                }
            }

            // Set reusable variables
            // (Note you could put the Upsert into a helper method)
            Schema.SObjectField f;
            Database.UpsertResult[] upsertList;




            f = Location__c.Fields.ExternalID__c;
            upsertList = Database.upsert(erpLocationMap.values(), f, false);

            for(Integer idx = 0; idx < upsertList.size(); idx++) {
                System.debug('@@@ upsertList[idx]=' + upsertList[idx] + ' (' + idx + ')');
                if(!upsertList[idx].isSuccess()) {
                    dmlSuccess = false;
                    // Operation failed, so get all errors
                    String errorMessage = '';
                    for(Database.Error err : upsertList[idx].getErrors()) {
                        System.debug('@@@ StatusCode/Message:' + err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('@@@ Fields that affected this error: ' + err.getFields());
                        errorMessage += '|' + err.getStatusCode() + '|' + err.getMessage() + '|-';
                    }
                    recordsFailedMap.put(
                        Location__c.getSObjectType() + erpLocationMap.values()[idx].ExternalID__c,
                        erpLocationMap.values()[idx].ExternalID__c + ': ' + errorMessage
                    );
                }
            }

            // Perform the DML on Account
            f = Account.Fields.ExternalID__c;
            upsertList = Database.upsert(erpAccountMap.values(), f, false);
            //cr.getId(); ==> Returns the Account ID (SF ID)
            for(Integer idx = 0; idx < upsertList.size(); idx++) {
                System.debug('@@@ upsertList[idx]=' + upsertList[idx] + ' (' + idx + ')');
                if(!upsertList[idx].isSuccess()) {
                    dmlSuccess = false;
                    // Operation failed, so get all errors
                    String errorMessage = '';
                    for(Database.Error err : upsertList[idx].getErrors()) {
                        System.debug('@@@ StatusCode/Message:' + err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('@@@ Fields that affected this error: ' + err.getFields());
                        errorMessage += '|' + err.getStatusCode() + '|' + err.getMessage() + '|-';
                    }
                    recordsFailedMap.put(
                        Account.getSObjectType() + erpAccountMap.values()[idx].ExternalID__c,
                        erpAccountMap.values()[idx].ExternalID__c + ': ' + errorMessage
                    );
                }
            }
            System.debug('@@@ recordsFailedMap: ' + recordsFailedMap);

            // Perform the DML on Contact
            f = Contact.Fields.ExternalID__c;
            upsertList = Database.upsert(erpContactMap.values(), f, false);
            //cr.getId(); ==> Returns the Account ID (SF ID)
            for(Integer idx = 0; idx < upsertList.size(); idx++) {
                System.debug('@@@ upsertList[idx]=' + upsertList[idx] + ' (' + idx + ')');
                if(!upsertList[idx].isSuccess()) {
                    dmlSuccess = false;
                    // Operation failed, so get all errors
                    String errorMessage = '';
                    for(Database.Error err : upsertList[idx].getErrors()) {
                        System.debug('@@@ StatusCode/Message:' + err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('@@@ Fields that affected this error: ' + err.getFields());
                        errorMessage += '|' + err.getStatusCode() + '|' + err.getMessage() + '|-';
                    }
                    recordsFailedMap.put(
                        Contact.getSObjectType() + erpContactMap.values()[idx].ExternalID__c,
                        erpContactMap.values()[idx].ExternalID__c + ': ' + errorMessage
                    );
                }
            }
            System.debug('@@@ recordsFailedMap: ' + recordsFailedMap);


            f = AccountSalesArea__c.Fields.ExternalId__c;
            upsertList = Database.upsert(erpSalesAreaMap.values(), f, false);
            for (integer idx = 0; idx < upsertList.size(); idx++) {
                System.debug('@@@ upsertList[idx]=' + upsertList[idx] + ' (' + idx + ')');
                if(!upsertList[idx].isSuccess()) {
                    dmlSuccess = false;
                    // Operation failed, so get all errors
                    String errorMessage = '';
                    for(Database.Error err : upsertList[idx].getErrors()) {
                        System.debug('@@@ StatusCode/Message:' + err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('@@@ Fields that affected this error: ' + err.getFields());
                        errorMessage += '|' + err.getStatusCode() + '|' + err.getMessage() + '|-';
                    }
                    recordsFailedMap.put(
                            AccountSalesArea__c.getSObjectType() + erpSalesAreaMap.values()[idx].ExternalID__c,
                            erpSalesAreaMap.values()[idx].ExternalId__c + ': ' + errorMessage
                    );
                }
            }

        }


        if(customers.customers != null) {

            // Now iterate the Parents to process ERP Records
            for(CustomerMaster c: customers.customers) {

                // Only dealing with Sold Tos who have Sales Arrangements (Note may need to change if we have Vendor Accounts)
                if(c.accountGroup != 'ZSHP'
                   && (c.salesArrangements!=null || c.accountGroup == 'VEND') ) {

                    /* Instantiate an ERP Record sobject for the Account */
                    // FK Relate to parent, but there is no UI interction at this stage
                    String parentAccountId = (
                        erpAccountMap.containsKey(c.sapInstance + '-' + c.sapNumber)
                        ? erpAccountMap.get(c.sapInstance + '-' + c.sapNumber).Id
                        : null
                    );
                    /*ERPRecord__c erpRecordForAccount =
                        generateErpRecord(c.sapInstance, c.sapNumber, c.name, String.valueOf(Account.getSobjectType()), parentAccountId, c);
                    // Add to dml map
                    erpRecordMap.put(erpRecordForAccount.UniqueIdentifier__c, erpRecordForAccount);*/

                    // Process Contacts
                    if (c.customerContacts != null ) {
                        for (CustomerContact cc : c.customerContacts) {
                            /* Instantiate an ERP Record sobject for the Contact */
                            // FK Relate to parent, but there is no UI interction at this stage
                            String parentContactId = (
                                erpContactMap.containsKey(c.sapInstance + '-' + cc.sapNumber)
                                ? erpContactMap.get(c.sapInstance + '-' + cc.sapNumber).Id
                                : null
                            );
                            /*CustomerMaster ccIncludingCustomerMaster = c.clone();
                            ccIncludingCustomerMaster.salesArrangements = null;
                            ccIncludingCustomerMaster.customerContacts = new List<CustomerContact>();
                            ccIncludingCustomerMaster.customerContacts.add(cc);*/
                            /*ERPRecord__c erpRecordForContact =
                                generateErpRecord(c.sapInstance, cc.sapNumber, (cc.firstName+' '+cc.lastName), String.valueOf(Contact.getSobjectType()), parentContactId, cc);
                            // Add to dml map
                            erpRecordMap.put(erpRecordForContact.UniqueIdentifier__c, erpRecordForContact);*/
                        }
                    }
                }
            }

            // Perform the DML
            /*Schema.SObjectField f = ERPRecord__c.Fields.UniqueIdentifier__c;
            Database.UpsertResult[] upsertList = Database.upsert(erpRecordMap.values(), f, false);
            //cr.getId(); ==> Returns the Account ID (SF ID)
            for(Integer idx = 0; idx < upsertList.size(); idx++) {
                System.debug('@@@ upsertList[idx]=' + upsertList[idx] + ' (' + idx + ')');
                if(!upsertList[idx].isSuccess()) {
                    dmlSuccess = false;
                    // Operation failed, so get all errors
                    String errorMessage = '';
                    for(Database.Error err : upsertList[idx].getErrors()) {
                        System.debug('@@@ StatusCode/Message:' + err.getStatusCode() + ': ' + err.getMessage());
                        System.debug('@@@ Fields that affected this error: ' + err.getFields());
                        errorMessage += '|' + err.getStatusCode() + '|' + err.getMessage() + '|-';
                    }
                    recordsFailedMap.put(
                        erpRecordMap.values()[0].getSObjectType() + erpRecordMap.values()[idx].UniqueIdentifier__c,
                        erpRecordMap.values()[idx].UniqueIdentifier__c + ': ' + errorMessage
                    );
                }
            }
            System.debug('@@@ recordsFailedMap: ' + recordsFailedMap);*/


            // TODO: Mark all exisitng to be deleted (to be completed)


        }


        // Now build the Response...
        CustomerMasterResponse resp = new CustomerMasterResponse();

        if(dmlSuccess == true)
            resp.success = true;
        else
            resp.success = false;

        if(resp.success == false) {
            resp.errors = new List<CustomerMasterError>();
            for(String s: recordsFailedMap.values()){
                CustomerMasterError err = new CustomerMasterError();
                err.message = s;
                resp.errors.add(err);
            }
        }

        return resp;

    }

}