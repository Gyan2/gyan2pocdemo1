@isTest
public class SVMX_Monthly_TimesheetBatch_UT
{
    static testMethod void MonthlyTimesheet()
    
   {
     
    Set<string> CountrySet = new Set<string>();
    List<string> profileIdList = new List<string>();
    Profile profileId = [SELECT Id FROM Profile WHERE Name = 'SVMX - Global Technician' LIMIT 1];

    User usr = new User();
      usr.LastName = 'LIVESTON';
      usr.FirstName='JASON';
      usr.Alias = 'jliv';
      usr.Email = 'jason.liveston@asdf.com';
      usr.Username = 'test123000030@gmail.com';
      usr.ProfileId = profileId.id;
      usr.TimeZoneSidKey = 'GMT';
      usr.LanguageLocaleKey = 'en_US';
      usr.EmailEncodingKey = 'UTF-8';
      usr.LocaleSidKey = 'en_US';
      usr.UserCountry__c ='AD';
      usr.SalesOrg__c ='9999';
      usr.isActive=true;
      insert usr;
             
    SVMX_Timesheet_Configuration__c tc= new SVMX_Timesheet_Configuration__c();
        tc.Name='AD';
        tc.SVMX_Frequency__c='Daily';
        tc.SVMX_NonProductive_Events__c='Training';
        tc.SVMX_Profiles__c ='SVMX - Global Technician';
         insert tc;
        profileIdList.add('SVMX - Global Technician');
        CountrySet.add('AD');
         SVMXC__Timesheet__c timesheet = SVMX_TestUtility.CreateTimeSheet('open', true );    
       /*SVMXC__Timesheet__c timesheet = new SVMXC__Timesheet__c();
         timesheet.SVMXC__Status__c='open';
          timesheet.SVMXC__Start_Date__c= Date.newInstance(2016, 12, 26);
          timesheet.SVMXC__End_Date__c= Date.newInstance(2016, 12, 26);
          timesheet.SVMX_Current_Week__c=true;
          timesheet.SVMX_Previous_Week__c=false;
          timesheet.SVMXC__Period__c ='Daily';*/
           system.debug('testinsert'+timesheet);
             //insert timesheet;
            
            Test.startTest();
              
            SVMX_Monthly_TimesheetBatch obj = new SVMX_Monthly_TimesheetBatch(CountrySet,profileIdList);            
            DataBase.executeBatch(obj);
            Test.stopTest();

   
   
   }
   
   
   
   }