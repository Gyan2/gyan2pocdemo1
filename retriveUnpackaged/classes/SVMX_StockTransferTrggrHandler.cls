public class SVMX_StockTransferTrggrHandler extends SVMX_TriggerHandler {

    private list<SVMXC__Stock_Transfer__c> newSTList;
    private list<SVMXC__Stock_Transfer__c> oldSTList;
    private Map<Id, SVMXC__Stock_Transfer__c> newSTMap;
    private Map<Id, SVMXC__Stock_Transfer__c> oldSTMap;
    
    public SVMX_StockTransferTrggrHandler() {
        this.newSTList = (list<SVMXC__Stock_Transfer__c>) Trigger.new;
        this.oldSTList = (list<SVMXC__Stock_Transfer__c>) Trigger.old;
        this.newSTMap = (Map<Id, SVMXC__Stock_Transfer__c>) Trigger.newMap;
        this.oldSTMap = (Map<Id, SVMXC__Stock_Transfer__c>) Trigger.oldMap;
    }

    public override void afterupdate(){

        set<id> stIds = new set<id> ();

        set<id> stposttoInventoryUpdateIds = new set<id> ();
        List<SVMXC__Stock_Transfer_Line__c> stockTransferList = new List<SVMXC__Stock_Transfer_Line__c>();
        List<SVMXC__Stock_Transfer_Line__c> stockTransferList1 = new List<SVMXC__Stock_Transfer_Line__c>();

        for(SVMXC__Stock_Transfer__c st : newSTList){

            system.debug('st--->'+st);

                if(st.SVMXC_Approved__c == true && st.SVMXC_Approved__c != oldSTMap.get(st.Id).SVMXC_Approved__c){

                    stIds.add(st.Id);

                    system.debug('st.Id--->'+st.Id);
                }

                if(st.SVMX_Post_To_Inventory__c == true && st.SVMX_Post_To_Inventory__c != oldSTMap.get(st.Id).SVMX_Post_To_Inventory__c){

                    stposttoInventoryUpdateIds.add(st.Id);
                }
        }

        if(stIds.size() > 0){
            SVMX_StockTransferserviceManager.InventoryHandler(stIds);
        }
        
        //Update Approved field Of stock Transfer Lines
        if(stIds.size() > 0){
            stockTransferList1 =[Select Id,name,SVMXC__Posted_To_Inventory__c, SVMX_Approved__c, SVMXC__Stock_Transfer__c from SVMXC__Stock_Transfer_Line__c where SVMXC__Stock_Transfer__c IN:stIds AND SVMX_Approved__c = False];
        }
        if(stockTransferList1.size()>0){
            for(SVMXC__Stock_Transfer_Line__c stockTransfer :stockTransferList1){

                stockTransfer.SVMX_Approved__c = True;
            }
            update stockTransferList1;
        }   

        //Update Post To Inventory Of stock Transfer Lines
        if(stposttoInventoryUpdateIds.size() > 0){
            stockTransferList =[Select Id,name,SVMXC__Posted_To_Inventory__c, SVMXC__Stock_Transfer__c from SVMXC__Stock_Transfer_Line__c where SVMXC__Stock_Transfer__c In:stposttoInventoryUpdateIds AND SVMXC__Posted_To_Inventory__c = False];
        }
        if(stockTransferList.size()>0){
            for(SVMXC__Stock_Transfer_Line__c stockTransfer :stockTransferList){

                stockTransfer.SVMXC__Posted_To_Inventory__c = True;
            }
            update stockTransferList;
        }    
            
        
    }    
 
}