public with sharing class SVMX_SAPPurchaseRequestUtility{

    public static void handleSAPPurchaseRequestCreation(List<SVMXC__RMA_Shipment_Line__c> partsOrderLine){

        set<id> validPartsOrderLineIdset = new set<id>();
        set<id> partsOrderIdset = new set<id>();

        list<SVMXC__RMA_Shipment_Line__c> poLineList = new list<SVMXC__RMA_Shipment_Line__c>();
        list<SVMXC__RMA_Shipment_Line__c> blankCheckUpdates = new list<SVMXC__RMA_Shipment_Line__c>();
        //Map<Id,SVMXC__RMA_Shipment_Order__c> parOrderMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>();
        
        if( partsOrderLine != null && !partsOrderLine.isEmpty()) {

                system.debug('partsOrdLinesize'+partsOrderLine.size());

                for(SVMXC__RMA_Shipment_Line__c partOrdL: partsOrderLine){

                    system.debug('PurchaseRequestHandle-->'+partOrdL);

                    //partsOrderLineIDSet.add(partOrdL.id);
                    partsOrderIdset.add(partOrdL.SVMXC__RMA_Shipment_Order__c);
                }
                
                   Map<Id,SVMXC__RMA_Shipment_Order__c> parOrderMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>([SELECT Id, Name, SVMX_Purchase_Requisition_Number__c,
                                                                                SVMX_Awaiting_SAP_Response__c
                                                                                FROM SVMXC__RMA_Shipment_Order__c
                                                                                WHERE Id IN : partsOrderIdset]);
                


                for(SVMXC__RMA_Shipment_Line__c partsordline : partsOrderLine){
                    
                       if(parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Purchase_Requisition_Number__c == null
                        && parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Awaiting_SAP_Response__c == false) {

                            System.debug('SVMX_SAPPurchaseRequestUtility : PartsOrderLine ' + partsordline.Name + ' is ready for purchaseRequestion Creation');
                            validPartsOrderLineIdset.add(partsordline.Id);
                        }
                        else if(parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Purchase_Requisition_Number__c == null
                        && parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Awaiting_SAP_Response__c == true) {

                            poLineList.add(partsordline);    
                        }

                        else if(parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Purchase_Requisition_Number__c != null) {
                            // These PartsOrderLine Details need to sent as Update, although they are new. This is because SAP considers
                            // these as updates to an existing Sales Order, and not a create
                            System.debug('SVMX_SAPPurchaseRequestUtility : Parts Order ' + partsordline.Name + ' is ready for SAP SpurchaseRequestion Update and purchaseRequestion Creation');
                            validPartsOrderLineIdset.add(partsordline.Id);       
                        }
                }
                
                if(!poLineList.isEmpty()) {
                    
                    for(SVMXC__RMA_Shipment_Line__c pol : poLineList) {
                        
                        SVMXC__RMA_Shipment_Line__c partsordlin = new SVMXC__RMA_Shipment_Line__c(id=pol.id,SVMX_Item_Number_Blank_Check__c = true);
                        //pol.SVMX_Item_Number_Blank_Check__c = true;
                        blankCheckUpdates.add(partsordlin);
                    }
                } 

                if(!validPartsOrderLineIdset.isEmpty() ) {
                  
                  system.debug('PurchaseRequestHandle1-->'+validPartsOrderLineIdset);

                  createSoapRequestForSAPPurchaseRequestCreation(validPartsOrderLineIdset);
                }

                if(!blankCheckUpdates.isEmpty()) {
                    update blankCheckUpdates;
                }  
        }
    }    


    @future (callout=true)
    public static void createSoapRequestForSAPPurchaseRequestCreation(Set<Id> validPartsOrderLineIdSet){
        if(validPartsOrderLineIdSet != null &&  validPartsOrderLineIdSet.size() > 0){

                list<SVMXC__RMA_Shipment_Line__c> ParOrdLin = SVMX_PartsOrderLineDataManager.ParOrdLineQuery(validPartsOrderLineIdSet);

                Map<id,list<SVMXC__RMA_Shipment_Line__c>> partsOrderAndpartsOrderLineMap = new Map<id,list<SVMXC__RMA_Shipment_Line__c>> ();
                Map<Id,SVMXC__RMA_Shipment_Order__c> ParOrdMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>();
                list<SVMXC__RMA_Shipment_Order__c> poAwaitingResponseUpdates = new list<SVMXC__RMA_Shipment_Order__c>(); 

                List<SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest> PurchseRequestList = new  List<SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest>();
                Map<SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest,SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader> purchaseRequestItemHeaderMap = new Map<SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest,SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader>();
                
                for(SVMXC__RMA_Shipment_Line__c parOrdl : ParOrdLin) {
                    
                    List<SVMXC__RMA_Shipment_Line__c> temppartsOrderLinlist = new  List<SVMXC__RMA_Shipment_Line__c>();

                    if(partsOrderAndpartsOrderLineMap.containsKey(parOrdl.SVMXC__RMA_Shipment_Order__c)) {

                         temppartsOrderLinlist = partsOrderAndpartsOrderLineMap.get(parOrdl.SVMXC__RMA_Shipment_Order__c);

                    }
                    system.debug('temppartsOrderLinlist'+temppartsOrderLinlist);
                    temppartsOrderLinlist.add(parOrdl) ;

                    partsOrderAndpartsOrderLineMap.put(parOrdl.SVMXC__RMA_Shipment_Order__c, temppartsOrderLinlist);
                    system.debug('partsOrderAndpartsOrderLineMap'+partsOrderAndpartsOrderLineMap);
                }

                for(SVMXC__RMA_Shipment_Order__c parOrder : SVMX_PartsOrderDataManager.ParOrdQuery(partsOrderAndpartsOrderLineMap.keyset())){
                    system.debug('parOrder'+parOrder);
                    if(!ParOrdMap.containsKey(parOrder.id)){
                        ParOrdMap.put(parOrder.id,parOrder);
                        system.debug('ParOrdMap'+ParOrdMap);
                    }
                }

                for(SVMXC__RMA_Shipment_Order__c parOrd : ParOrdMap.values()){
                    
                    system.debug('parOrd'+parOrd);
                    SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader MessageHeader = new SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader ();
                    MessageHeader.ID = new SVMX_wwwDormakabaPurchase.BusinessDocumentMessageID ();
                    MessageHeader.ID.Content = parOrd.Id;

                    MessageHeader.BusinessScope = new list<SVMX_wwwDormakabaPurchase.BusinessScope>();
                    SVMX_wwwDormakabaPurchase.BusinessScope BusScope = new SVMX_wwwDormakabaPurchase.BusinessScope();
                    BusScope.TypeCode = 'SMX';
                    BusScope.InstanceID = new SVMX_wwwDormakabaPurchase.BusinessScopeInstanceID();
                    BusScope.InstanceID.Content= parOrd.Name;
                    BusScope.ID = new SVMX_wwwDormakabaPurchase.BusinessScopeID ();
                    BusScope.ID.Content = 'StockTransferOrder';

                    MessageHeader.BusinessScope.add(BusScope);

                    MessageHeader.PositiveAckRequestedIndicator = 'true';
                    MessageHeader.NegativeAckRequestedIndicator = 'false';

                    SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest purchaseRequest = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest ();

                    purchaseRequest.ID = new SVMX_wwwDormakabaPurchase.BusinessTransactionDocumentID ();
                    purchaseRequest.ID.Content = '';

                    purchaseRequest.ProcessingTypeCode = 'PR';
                    purchaseRequest.BusinessProcessVariantTypeCode = 'PR01';

                    purchaseRequest.BuyerParty = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestBuyerParty ();
                    purchaseRequest.BuyerParty.StandardID = new SVMX_wwwDormakabaPurchase.PartyStandardID ();
                    purchaseRequest.BuyerParty.StandardID.Content = '9921226005491';

                    list<SVMXC__RMA_Shipment_Line__c> PaOrdLineList = partsOrderAndpartsOrderLineMap.get(parOrd.Id);
                    
                    purchaseRequest.Item = new list<SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestItem>();
                    for(SVMXC__RMA_Shipment_Line__c pol : PaOrdLineList){

                        SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestItem purchaseRequestItem = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestItem ();
                        purchaseRequestItem.ID = '';

                        purchaseRequestItem.Product = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestItemProduct();
                        purchaseRequestItem.Product.InternalID = new SVMX_wwwDormakabaPurchase.ProductInternalID();
                        purchaseRequestItem.Product.InternalID.Content = pol.SVMXC__Product__r.SAPNumber__c;

                        purchaseRequestItem.SellerParty = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestItemSellerParty();
                        purchaseRequestItem.SellerParty.SupplyingPlantID = pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__r.SVMXC_Plant_Number__c;

                        purchaseRequestItem.ReceivingPlantID = parOrd.SVMX_Shipping_Location__r.SVMXC_Plant_Number__c;
                        purchaseRequestItem.StorageLocationID = parOrd.SVMX_Shipping_Location__r.SVMXC_SAP_Storage_Location__c;

                        purchaseRequestItem.ShipToLocation = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestItemShipToLocation ();
                        purchaseRequestItem.ShipToLocation.Address = new SVMX_wwwDormakabaPurchase.Address();
                        purchaseRequestItem.ShipToLocation.Address.PhysicalAddress = new SVMX_wwwDormakabaPurchase.PhysicalAddress();

                        if(parOrd.SVMXC__Destination_Street__c != parOrd.SVMXC__Destination_Location__r.SVMXC__Street__c
                            || parOrd.SVMXC__Destination_City__c != parOrd.SVMXC__Destination_Location__r.SVMXC__City__c
                            || parOrd.SVMXC__Destination_State__c != parOrd.SVMXC__Destination_Location__r.SVMXC__State__c
                            || parOrd.SVMXC__Destination_Zip__c != parOrd.SVMXC__Destination_Location__r.SVMXC__Zip__c
                            || parOrd.SVMXC__Destination_Country__c != parOrd.SVMXC__Destination_Location__r.SVMXC__Country__c) {

                            purchaseRequestItem.ShipToLocation.Address.PhysicalAddress.StreetName = pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Street__c;
                            purchaseRequestItem.ShipToLocation.Address.PhysicalAddress.CityName = pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_City__c;
                            purchaseRequestItem.ShipToLocation.Address.PhysicalAddress.DistrictName = pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_State__c;
                            purchaseRequestItem.ShipToLocation.Address.PhysicalAddress.StreetPostalCode = pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Zip__c;
                            purchaseRequestItem.ShipToLocation.Address.PhysicalAddress.CountryName = new SVMX_wwwDormakabaPurchase.MEDIUM_Name();
                            purchaseRequestItem.ShipToLocation.Address.PhysicalAddress.CountryName.Content = pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Country__c;

                            //get Country Code from Metadata
                            String countryCode = getCountryCode(pol.SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Country__c);
                            purchaseRequestItem.ShipToLocation.Address.PhysicalAddress.CountryCode = countryCode;

                            purchaseRequestItem.ShipToLocation.Address.CompanyName = new SVMX_dormaEsmEdt10Purchase.CompanyName();

                            String[] shipToLocNames = new List<String>();
                            shipToLocNames.add(parOrd.SVMX_Shipping_Location__r.Name);

                            purchaseRequestItem.ShipToLocation.Address.CompanyName.NameLine = shipToLocNames;
                        }
                        
                        String reqQty;
                        if(pol.SVMXC__Expected_Quantity2__c != null) {

                          Decimal expQtydecVal = pol.SVMXC__Expected_Quantity2__c.setScale(3);
                          System.debug('*** Expected Quantity ***: ' + expQtydecVal);
            
                          reqQty = String.valueOf(expQtydecVal);
                        }
                        else {

                          reqQty = '0.000';
                        }

                        purchaseRequestItem.RequestedQuantity = new SVMX_wwwDormakabaPurchase.Quantity();
                        purchaseRequestItem.RequestedQuantity.Content = reqQty;
                        purchaseRequestItem.RequestedQuantity.UnitCode = pol.SVMXC__Product__r.SVMXC__Unit_Of_Measure__c;
                        
                        purchaseRequestItem.DeliveryDate = String.valueOf(pol.SVMXC__Expected_Receipt_Date__c);
                       
                        purchaseRequest.Item.add(purchaseRequestItem);
                    }    

                    PurchseRequestList.add(purchaseRequest);
                    System.debug('purchaseRequest' + purchaseRequest);
                    // Add purchase Order  and corresponding Headers to a map
                    purchaseRequestItemHeaderMap.put(purchaseRequest, MessageHeader);
                } 
                
                
                Boolean response = null;
                SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort pop = getPort();
                    
                    System.debug('PurchseRequestList'+PurchseRequestList.size());
                    for(SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest purchaseRequest : PurchseRequestList) {

                        
                        System.debug('purchaseRequest' + purchaseRequest);
                        SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader header = purchaseRequestItemHeaderMap.get(purchaseRequest);

                        try {

                            response = pop.CreateSync(header, purchaseRequest);
                            System.debug('SVMX_SAPPurchaseRequestUtility : Call Service - Done' + response);
                            
                        }
                        catch (System.CalloutException exceptionRec) {
                            
                            System.debug('SVMX_SAPPurchaseRequestUtility : Exception while creating PurchaseRequest : ' + exceptionRec.getMessage());
                            
                        }        
                    } 

             /* for(SVMXC__RMA_Shipment_Order__c partsOrder : ParOrdMap.values()) {

                        if(partsOrder.SVMX_Purchase_Requisition_Number__c == null) {

                            partsOrder.SVMX_Awaiting_SAP_Response__c = true;
                            poAwaitingResponseUpdates.add(partsOrder);
                        }
                }

             if(!poAwaitingResponseUpdates.isEmpty()) {
                   update poAwaitingResponseUpdates;
             }     */  
                    

        }
    }       
            private static SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort getPort() {
                
                //String WS_USER = 'BC_SFORCE';
                //String WS_PASS = 'LHjqFTt?19@f';

                SVMX_SAP_Integration_UserPass__c cs = SVMX_SAP_Integration_UserPass__c.getInstance('SAP PO');
                    
                String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(cs.WS_USER__c + ':' + cs.WS_PASS__c));
                
                SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort pop = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort();
                pop.inputHttpHeaders_x = new Map<String,String>();
                pop.inputHttpHeaders_x.put('Content-type', 'application/soap+xml');        
                pop.inputHttpHeaders_x.put('Authorization', authorizationHeader);
                pop.timeout_x = 120000;
             
                return pop; 
            }

            //getCountryCode - Method to retrieve Country Code from Custom Metadata
            public static String getCountryCode(String country) {

                List<Country_Name_Alternatives__mdt> countryCodeList = [SELECT Label, CountryISOCode__c 
                                                                          FROM Country_Name_Alternatives__mdt
                                                                          WHERE Label =: country];

                String countryCode = countryCodeList[0].CountryISOCode__c;
                return countryCode;                      
            }  

}