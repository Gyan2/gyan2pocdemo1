/**
 * Created by trakers on 19.07.18.
 */

public with sharing class QuoteLineItemsServices {
    public static void deleteText(List<QuoteLineItem> Records){
        delete [select id from Text__c where QuoteLineItemRef__c in :Records];
    }
}