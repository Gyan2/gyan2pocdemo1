/************************************************************************************************************
Description: Test class for the Controller Class (SVMX_SelectQuoteItems_Controller) 
 
Author: Ranjitha S
Date: 11-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/
@isTest
public class SVMX_SelectQuoteItems_Controller_UT {

    static testMethod void selectQuoteItems()
    {
       
        Account acc = SVMX_TestUtility.CreateAccount('Test account', 'Norway', true);
        Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
        Opportunity opp = SVMX_TestUtility.CreateOpportunity(acc.Id, 'name', 'New', 50, 100, true);
        SVMXC__Quote__c quote = SVMX_TestUtility.CreateServiceQuote(acc.id,null,opp.id,'Draft',false);
        quote.SVMX_PS_Country__c ='Norway';
        insert quote;
        SVMXC__Site__c  loc = SVMX_TestUtility.CreateLocation('Test Loc',acc.id,true);
        SVMXC__Installed_Product__c ip1 = SVMX_TestUtility.CreateInstalledProduct(acc.id,null,loc.id,true);
        Product2  prod = SVMX_TestUtility.CreateProduct('Test prod',true);
        SVMX_Sales_Office__c  salOf = new SVMX_Sales_Office__c (SVMX_Country__c = 'Norway',SVMX_Quoted_Works_Dummy_Part__c = prod.id);
        insert salOf;
        SVMXC__Quote_Line__c qi1 = SVMX_TestUtility.CreateQuoteItems(ip1.id,quote.id,'Parts',true);
        SVMXC__Quote_Line__c qi2 = SVMX_TestUtility.CreateQuoteItems(ip1.id,quote.id,'Parts',true);
        List<SVMXC__Quote_Line__c> qiList = new List<SVMXC__Quote_Line__c>{qi1,qi2};
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
        List<SVMX_SelectQuoteItems_Controller.WrapIP> w = new List<SVMX_SelectQuoteItems_Controller.WrapIP>();
      
        PageReference pageRef = Page.SVMX_SelectQuoteItems;

        pageRef.getParameters().put('quoteId', quote.id);
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        SVMX_SelectQuoteItems_Controller obj01 = new SVMX_SelectQuoteItems_Controller();
        obj01.ipWrap.get(0).sel = true;
        obj01.CreateCaseAndWO();
        obj01.Cancel();
        obj01.RejectQuoteLines();
        obj01.RejectQuote();
        Test.stopTest();
    }
}