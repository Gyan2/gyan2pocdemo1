public class SVMX_ServiceContractSiteDataManager{

    public static List<SVMXC__Service_Contract_Sites__c> SCSiteQuery(set<id> serviceContractSiteId){
        List<SVMXC__Service_Contract_Sites__c> serviceConSiteMap = new List<SVMXC__Service_Contract_Sites__c>([SELECT id,Name, SVMXC__Is_Billable__c, SVMX_Billing_schedule__c, SVMX_Total_Line_Price__c,SVMX_Discount__c, SVMX_SAP_Material_Number__c, CurrencyIsoCode,SVMXC__Start_Date__c,SVMXC__End_Date__c, SVMX_Billing_Start_Date__c,SVMX_Billing_End_Date__c ,SVMXC__Line_Price__c ,SVMXC__Notes__c,SVMX_Pricing_Methos__c, SVMXC__Service_Contract__c, SVMXC__Service_Contract__r.SVMXC__Company__r.Language__c, SVMXC__Site__r.Name, SVMX_SAP_Contract_Item_Number__c from SVMXC__Service_Contract_Sites__c where Id IN :serviceContractSiteId  AND SVMX_Pricing_Methos__c = 'Time Based']);
        return serviceConSiteMap;
    }
    

    public static List<SVMXC__Service_Contract_Sites__c> timeBasedSCSiteQuery(set<id> serviceContractId){
        List<SVMXC__Service_Contract_Sites__c> serviceConSiteMap = new List<SVMXC__Service_Contract_Sites__c>([SELECT id,Name, SVMXC__Is_Billable__c, SVMX_Billing_schedule__c, SVMX_Total_Line_Price__c,SVMX_Discount__c, SVMX_SAP_Material_Number__c ,CurrencyIsoCode ,SVMXC__Start_Date__c,SVMXC__End_Date__c, SVMX_Billing_Start_Date__c,SVMX_Billing_End_Date__c ,SVMXC__Line_Price__c ,SVMXC__Notes__c,SVMX_Pricing_Methos__c, SVMXC__Service_Contract__c, SVMXC__Service_Contract__r.SVMXC__Company__r.Language__c, SVMXC__Site__r.Name, SVMX_SAP_Contract_Item_Number__c from SVMXC__Service_Contract_Sites__c where SVMXC__Service_Contract__c IN :serviceContractId  AND SVMX_Pricing_Methos__c = 'Time Based' AND SVMXC__Is_Billable__c = True]);
        return serviceConSiteMap;
    }
    
    public static List<SVMXC__Service_Contract_Sites__c> timeBasedSCSiteBlankcheckQuery(set<id> serviceContractId){
        List<SVMXC__Service_Contract_Sites__c> serviceConSiteMap = new List<SVMXC__Service_Contract_Sites__c>([SELECT id,Name, SVMXC__Is_Billable__c, SVMX_Billing_schedule__c, SVMX_Total_Line_Price__c,SVMX_Discount__c, SVMX_SAP_Material_Number__c ,CurrencyIsoCode ,SVMXC__Start_Date__c,SVMXC__End_Date__c, SVMX_Billing_Start_Date__c,SVMX_Billing_End_Date__c ,SVMXC__Line_Price__c ,SVMXC__Notes__c,SVMX_Pricing_Methos__c, SVMXC__Service_Contract__c, SVMXC__Service_Contract__r.SVMXC__Company__r.Language__c, SVMXC__Site__r.Name, SVMX_SAP_Contract_Item_Number__c,SVMX_SAP_Contract_Item_Num_Blank_Check__c from SVMXC__Service_Contract_Sites__c where SVMXC__Service_Contract__c IN :serviceContractId  AND SVMX_Pricing_Methos__c = 'Time Based' AND SVMX_SAP_Contract_Item_Num_Blank_Check__c = True]);
        return serviceConSiteMap;
    }
    
}