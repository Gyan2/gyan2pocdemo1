/**
 * @CoveredBy: LTNG_AccountToContactAddressSync_Test
 * @FieldsUsed: Account.Name, Account.BillingAddress, Account.BillingStreet, Account.BillingPostalCode, Account.BillingState, Account.BillingCity, Account.BillingCountry
 *              Contact.Name, Contact.MailingAddress, Contact.MailingStreet, Contact.MailingPostalCode, Contact.MailingState, Contact.MailingCity, Contact.MailingCountry
 */
public with sharing class LTNG_AccountToContactAddressSync {

    @AuraEnabled
    public static AccountWrapper auraGetAccountContactBillingInformation(Id recordId) {
        return new AccountWrapper([SELECT Name, BillingAddress, toLabel(BillingCountryCode) BillingCountryCodeLabel, (SELECT Name, MailingAddress, toLabel(MailingCountryCode) MailingCountryCodeLabel FROM Contacts) FROM Account WHERE Id = :recordId]);
    }

    @AuraEnabled
    public static void auraSetAccountContactBillingInformation(List<Contact> contacts) {
        update contacts;
    }

    public class AccountWrapper {
        @AuraEnabled public Account account;
        @AuraEnabled public Map<String,Map<String,String>> fieldLabels = new Map<String,Map<String,String>>{
            'Contact' => new Map<String,String>{
                'LabelPlural' => Contact.getSObjectType().getDescribe().getLabelPlural(),
                'Name' => Util.Describe.getField('Contact', 'Name').label,
                'MailingAddress' => Util.Describe.getField('Contact', 'MailingAddress').label,
                'MailingStreet' => Util.Describe.getField('Contact', 'MailingStreet').label,
                'MailingPostalCode' => Util.Describe.getField('Contact', 'MailingPostalCode').label,
                'MailingState' => Util.Describe.getField('Contact', 'MailingState').label,
                'MailingCity' => Util.Describe.getField('Contact', 'MailingCity').label,
                'MailingCountry' => Util.Describe.getField('Contact', 'MailingCountry').label
            },
            'Account' => new Map<String,String>{
                'Name' => Util.Describe.getField('Account', 'Name').label,
                'BillingStreet' => Util.Describe.getField('Account', 'BillingStreet').label,
                'BillingPostalCode' => Util.Describe.getField('Account', 'BillingPostalCode').label,
                'BillingState' => Util.Describe.getField('Account', 'BillingState').label,
                'BillingCity' => Util.Describe.getField('Account', 'BillingCity').label,
                'BillingCountry' => Util.Describe.getField('Account', 'BillingCountry').label
            }
        };

        public AccountWrapper(Account paramAccount) {
            account = paramAccount;
        }
    }
}