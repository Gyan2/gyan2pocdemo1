/**
 * Created by rebmangu on 02/05/2018.
 */

public with sharing class ProductHierarchyServices {

    public static Integer MAXLENGTH = 13;

    public static void handleExternalIdMapping(List<ProductHierarchy__c> Records) {
        Map<String,Integer> settings = new Map<String,Integer>{
            'NeedRef__c'   =>  2,
            'FamilyRef__c' =>  4,
            'ClassRef__c'  =>  7,
            'LineRef__c'   =>  10
            //'Type__c'   =>  13
        };

        List<String> externalids = new List<String>();

        // Extract the external id to query for
        for(ProductHierarchy__c ph : Records){
            for(String key : settings.keySet()){
                if(ph.ExternalId__c.length() > settings.get(key)){
                    externalids.add(ph.externalId__c.left(settings.get(key)));
                }
            }
        }

        // Query the ProductHierarchy objects to get map the External Id to the Salesforce Id
        Map<String,Id> productHierarchyMapping = new Map<String,Id>();
        for(ProductHierarchy__c ph : [select id,ExternalId__c from ProductHierarchy__c where ExternalId__c in : externalids]){
            productHierarchyMapping.put(ph.ExternalId__c,ph.Id);
        }
        System.debug(productHierarchyMapping);


        // map the lookups (Need,etc) to the correct Salesforce Id
        for(ProductHierarchy__c ph : Records){
            for(String key : settings.keySet()){
                if(ph.ExternalId__c.length() > settings.get(key)){
                    ph.put(key,productHierarchyMapping.get(ph.externalId__c.left(settings.get(key))));
                }
            }
        }

    }
}