public without sharing class SVMXC_Sites extends CoreSObjectDomain{
	public static Boolean deactivateMirroring = !Test.isRunningTest() && Boolean.valueOf(Label.TMP_MirrorEngineTest);
	public static Boolean disableTriggerCRUDSecurity = false;

	public SVMXC_Sites(List<SVMXC__Site__c> sObjectList) {
		super(sObjectList);
		if (disableTriggerCRUDSecurity) configuration.disableTriggerCRUDSecurity();
	}

	public override void onBeforeInsert() {
		if (deactivateMirroring != true) doMirroring();
	}

	public override void onAfterInsert() {
		if (deactivateMirroring != true) {
			List<Location__c> locationsLst = new List<Location__c>();
			for (SVMXC__Site__c forSite : (List<SVMXC__Site__c>)Records) {
				locationsLst.add(new Location__c(Id = forSite.LocationRef__c, SVMXC_SiteRef__c = forSite.Id, SVMXC_SiteId__c = forSite.Id));
			}
			Locations.deactivateMirroring = true;
			Locations.disableTriggerCRUDSecurity = true;
			update locationsLst;
		}
	}
	public override void onBeforeUpdate(Map<Id,sObject> existingRecords) {
		if (deactivateMirroring != true) doMirroring();
	}

	private void doMirroring() {
		List<Location__c> mirrorsToUpsert = new List<Location__c>();

		MirrorService ms = new MirrorService.DirectMirror('SVMXC__Site__c','Location__c');
		mirrorsToUpsert.addAll((List<Location__c>)ms.doMirroring(Records, Location__c.fields.SVMXC_SiteId__c));

		// TODO: database.upsert to check for errors
		Locations.deactivateMirroring = true;
		Locations.disableTriggerCRUDSecurity = true;
        if (Trigger.IsInsert) upsert mirrorsToUpsert;
        else upsert mirrorsToUpsert SVMXC_SiteId__c;

		Integer i = 0;
		for (SVMXC__Site__c forSite : (List<SVMXC__Site__c>)Records){
			forSite.LocationRef__c = forSite.LocationId__c = mirrorsToUpsert.get(i++).Id;
		}
	}

	public class Constructor implements CoreSObjectDomain.IConstructable{
		public CoreSObjectDomain construct(List<sObject> sObjectList) {
			return new SVMXC_Sites(sObjectList);
		}
	}
}