public class SVMX_IntegrationErrorManager {

	public static void sortSoapRequests(List<SVMX_Integration_Error_Tracking__c> integrationErrorList) {

		List<SVMX_Integration_Error_Tracking__c> fixedIntError001List = new List<SVMX_Integration_Error_Tracking__c>();
		List<SVMX_Integration_Error_Tracking__c> fixedIntError002List = new List<SVMX_Integration_Error_Tracking__c>();
		Map<Id,SVMX_Integration_Error_Tracking__c> intError001WOMap = new Map<Id,SVMX_Integration_Error_Tracking__c>();
		Map<Id,SVMX_Integration_Error_Tracking__c> intError002WOMap = new Map<Id,SVMX_Integration_Error_Tracking__c>();
		Set<Id> wdSoapCreateList = new Set<Id>();

		for(SVMX_Integration_Error_Tracking__c intErr : integrationErrorList) {

			if(intErr.SVMX_Error_Code__c == '001') {

				Id woId = Id.valueOf(intErr.SVMX_Header_Record_Id__c);
				intError001WOMap.put(woId,intErr);	
			}

			if(intErr.SVMX_Error_Code__c == '002') {

				Id woId = Id.valueOf(intErr.SVMX_Header_Record_Id__c);
				intError002WOMap.put(woId,intErr);
			}		
		}

		if(!intError001WOMap.isEmpty()) {
			
			fixedIntError001List = errorCheck001(intError001WOMap);
		}

		if(!intError002WOMap.isEmpty()) {
			
			fixedIntError002List = errorCheck002(intError002WOMap);
		}

		if(!fixedIntError001List.isEmpty()) {

			for(SVMX_Integration_Error_Tracking__c intError : fixedIntError001List) {

				for(String wDetIdString : intError.SVMX_Item_Record_Ids__c.split(',')) {

					Id wdId = Id.valueOf(wDetIdString);	
					wdSoapCreateList.add(wdId);
				}
			}

			if(!wdSoapCreateList.isEmpty()) {

				SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderCreation(wdSoapCreateList);
			}
		}

		if(!fixedIntError002List.isEmpty()) {

			for(SVMX_Integration_Error_Tracking__c intError : fixedIntError002List) {

				for(String wDetIdString : intError.SVMX_Item_Record_Ids__c.split(',')) {

					Id wdId = Id.valueOf(wDetIdString);	
					wdSoapCreateList.add(wdId);
				}
			}

			if(!wdSoapCreateList.isEmpty()) {

				SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderCreation(wdSoapCreateList);
			}
		}
	}

	public static List<SVMX_Integration_Error_Tracking__c> errorCheck001(Map<Id,SVMX_Integration_Error_Tracking__c> intErrorWOMap) {

		List<SVMXC__Service_Order__c> woErrList = new List<SVMXC__Service_Order__c>();
		List<SVMX_Integration_Error_Tracking__c> fixedIntErrorList = new List<SVMX_Integration_Error_Tracking__c>();

		woErrList = [SELECT Id, Name, SVMXC__Case__c, SVMXC__Case__r.Account.SAPNumber__c
					 FROM SVMXC__Service_Order__c WHERE Id IN : intErrorWOMap.KeySet()];

		for(SVMXC__Service_Order__c wo : woErrList) {

			SVMX_Integration_Error_Tracking__c intError = intErrorWOMap.get(wo.Id);

			if(wo.SVMXC__Case__r.Account.SAPNumber__c != null) {

				fixedIntErrorList.add(intErrorWOMap.get(wo.Id));

				// Set the Error Status to Successful
				intError.SVMX_Error_Status__c = 'Successful';
			}

			//Untick the Resend to SAP Flaf
			intError.SVMX_Resend_to_SAP__c = false;
		}

		return fixedIntErrorList;			 
	}

	public static List<SVMX_Integration_Error_Tracking__c> errorCheck002(Map<Id,SVMX_Integration_Error_Tracking__c> intErrorWOMap) {

		List<SVMXC__Service_Order__c> woErrList = new List<SVMXC__Service_Order__c>();
		List<SVMX_Integration_Error_Tracking__c> fixedIntErrorList = new List<SVMX_Integration_Error_Tracking__c>();
		
		woErrList = [SELECT Id, Name, SVMXC__Case__c, SVMXC__Case__r.SVMXC__Site__r.SVMXC_Global_Location_Name__c
					 FROM SVMXC__Service_Order__c WHERE Id IN : intErrorWOMap.KeySet()];

		for(SVMXC__Service_Order__c wo : woErrList) {

			SVMX_Integration_Error_Tracking__c intError = intErrorWOMap.get(wo.Id);

			if(wo.SVMXC__Case__r.SVMXC__Site__r.SVMXC_Global_Location_Name__c != null) {

				fixedIntErrorList.add(intErrorWOMap.get(wo.Id));

				// Set the Error Status to Successful
				intError.SVMX_Error_Status__c = 'Successful';
			}

			//Untick the Resend to SAP Flaf
			intError.SVMX_Resend_to_SAP__c = false;
		}

		return fixedIntErrorList;			 
	}
}