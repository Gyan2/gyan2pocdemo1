public with sharing class LTNG_PermissionsOverview {

    @AuraEnabled
    public static String auraGetSessionId(){
        return Page.SessionId.getContent().toString(); // UserInfo.getSessionId() does not provide an API valid SessionId
    }
}