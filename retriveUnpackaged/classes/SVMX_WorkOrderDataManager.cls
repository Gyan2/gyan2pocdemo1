/************************************************************************************************************
Description: Data Manager for Work Order. All the SQL queries and DML Operations are performed in this class.

Dependancy: 
 
Author: Ranjitha S
Date: 16-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
10-11-17        Ranjitha        Sprint2 change - Added query for fetching warranty discount

*******************************************************************************************************/

public class SVMX_WorkOrderDataManager {
    
    public static Map<Id,SVMXC__Service_Order__c> woQuery(list<SVMXC__Service_Order__c> newWOList){
        Map<Id,SVMXC__Service_Order__c> woMap = new Map<Id,SVMXC__Service_Order__c> ([select id, SVMXC__Company__c, SVMXC__Company__r.CurrencyIsoCode, CurrencyIsoCode, SVMXC__Company__r.BillingCountry,SVMXC__Country__c, SVMXC__Service_Contract__c,  SVMXC__Case__c, SVMX_PM_Schedule_Definition__r.SVMX_Is_Present_WO_Task__c, SVMXC__Case__r.SVMXC__Warranty__c, SVMXC__Product__r.Family, SVMXC__Service_Contract__r.SVMX_Priority__c, SVMXC__Service_Contract__r.SVMX_Product_Surcharge__c, SVMXC__Service_Contract__r.SVMX_Product_Surcharge_Type__c, SVMXC__Service_Contract__r.SVMX_Priority_Surcharge__c, SVMXC__Group_Member__r.SVMXC__Salesforce_User__c, SVMX_PM_Schedule_Definition__r.SVMXC__Work_Order_Purpose__c, SVMX_PM_Schedule_Definition__c from SVMXC__Service_Order__c where id in :newWOList]);
        return woMap;
    }
    
    public static Map<Id,SVMXC__Service_Order__c> woQuery2(list<Id> newWOList){
        Map<Id,SVMXC__Service_Order__c> woMap = new Map<Id,SVMXC__Service_Order__c> ([select id,SVMXC__Priority__c,  SVMXC__Company__c, SVMXC__Company__r.CurrencyIsoCode, CurrencyIsoCode, SVMXC__Company__r.BillingCountry,SVMXC__Country__c, SVMXC__Service_Contract__c,  SVMXC__Case__c, SVMX_PM_Schedule_Definition__r.SVMX_Is_Present_WO_Task__c, SVMXC__Case__r.SVMXC__Warranty__c, SVMXC__Product__r.Family, SVMXC__Service_Contract__r.SVMX_Priority__c, SVMXC__Service_Contract__r.SVMX_Product_Surcharge__c, SVMXC__Service_Contract__r.SVMX_Product_Surcharge_Type__c, SVMXC__Service_Contract__r.SVMX_Priority_Surcharge__c, SVMXC__Group_Member__r.SVMXC__Salesforce_User__c, SVMX_PM_Schedule_Definition__r.SVMXC__Work_Order_Purpose__c, SVMX_PM_Schedule_Definition__c from SVMXC__Service_Order__c where id in :newWOList]);
        return woMap;
    }
    
    public static map<id,SVMXC__Service_Order__c> warrantyQuery(map<id,List<SVMXC__Service_Order_Line__c>> woli){
        Map<id, SVMXC__Service_Order__c> queryList = new Map<id, SVMXC__Service_Order__c>([select Id, SVMXC__Case__r.SVMXC__Warranty__c, SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Time_Covered__c, SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Material_Covered__c, SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Expenses_Covered__c, SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Travel_Covered__c  from  SVMXC__Service_Order__c where Id in : woli.keyset()]);
        return queryList;
    }
    //query the wo  of workdetail line of recordtype usage/consumption
    public static list<SVMXC__Service_Order__c> WoOfWdQuery(set<id> WorkOrderIdset){
        list<SVMXC__Service_Order__c> WorkOrderlist = new list<SVMXC__Service_Order__c> ([select id,Name, SVMXC__Country__c,SVMXC__Case__c, SVMXC__Case__r.Description, SVMXC__Case__r.SVMX_Invoice_Description__c, SVMXC__Order_Type__c,SVMXC__Case__r.CaseNumber, SVMXC__Case__r.SVMX_Awaiting_SAP_Response__c,SVMXC__Case__r.Account.SAPNumber__c, SVMXC__Case__r.SVMX_Project__r.SAPNumber__c, SVMXC__Case__r.SVMXC__Site__c, SVMXC__Case__r.SVMX_Customer_PO_Number__c, SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c, SVMXC__Case__r.SVMX_Invoice_Status__c, SVMXC__Case__r.SVMXC__Site__r.SVMXC_Global_Location_Name__c, SVMXC__Scheduled_Date__c, SVMXC__Scheduled_Date_Time__c, CurrencyIsoCode, SVMXC__Company__c, SVMXC__Company__r.SAPNumber__c ,SVMXC__Service_Contract__c,SVMXC__Service_Contract__r.SVMX_GlobalContract_Number__c, SVMXC__Service_Contract__r.SVMX_SAP_Contract_Number__c, SVMX_Service_Sales_Order_Number__c, SVMX_SAP_Installation_SO_Item_Number__c, SVMXC__Case__r.SVMX_Project__r.ExternalId__c, SVMXC__Case__r.SVMX_Case_Owner_Employee_Number__c, SVMXC__Case__r.Contact.SAPNumber__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC_Plant_Number__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC_SAP_Storage_Location__c, SVMX_SAP_Installation_Sales_Order_Number__c from SVMXC__Service_Order__c  Where Id in:WorkOrderIdset ]);
        return WorkOrderlist;
    }  
    
    public static Map<Id,case> checkCase(set<Id> caseIds){
        Map<Id,case> caseList = new Map<Id,Case> ([select Id,SVMXC__Warranty__c from Case where Id In: CaseIds]);
        return caseList;
    
    }
    
    public static Map<id,list<SVMXC__Service_Order__c>> caseWOQuery(set<Id> woList){
        Map<id,list<SVMXC__Service_Order__c>> caseWOMap = new Map<id,list<SVMXC__Service_Order__c>>();
        list<SVMXC__Service_Order__c> WorkOrderlist = new list<SVMXC__Service_Order__c> ([select id, SVMXC__Case__c, SVMXC__Order_Status__c, SVMX_Revisit_Required__c, SVMX_Revisit_Work_Order_Created__c,SVMXC__Company__r.SVMX_Automatic_billing__c from SVMXC__Service_Order__c  Where SVMXC__Case__c in:woList]);
        for(SVMXC__Service_Order__c wo: WorkOrderlist){
            if(caseWOMap.containsKey(wo.SVMXC__Case__c))
                caseWOMap.get(wo.SVMXC__Case__c).add(wo);                   
            else
                caseWOMap.put(wo.SVMXC__Case__c,new List<SVMXC__Service_Order__c> {wo});
        }
        return caseWOMap;
    }

    public static List<SVMXC__Task_Template__c> taskTemplateQuery(Map<Id,Id> woIdAndWoPurposeIdMap){

        List<SVMXC__Task_Template__c> ttListQuery = [Select id, SVMXC__Task_Title__c, SVMXC__Description__c, SVMXC__Task_Template__c, SVMXC__Priority__c from SVMXC__Task_Template__c where SVMXC__Task_Template__c IN :woIdAndWoPurposeIdMap.values()];
        return ttListQuery;
    }

    public static list<SVMXC__Service_Order__c> WoOfcase(set<id> caseIdset){
        list<SVMXC__Service_Order__c> WorkOrderlist = new list<SVMXC__Service_Order__c> ([select id,Name, SVMXC__Country__c,SVMXC__Case__c,SVMXC__Order_Type__c from SVMXC__Service_Order__c  Where SVMXC__Case__c in:caseIdset ]);
        return WorkOrderlist;
    }      
 
    public static void updateWO(list<SVMXC__Service_Order__c> woUpdateList){
        
        if(!woUpdateList.isEmpty()){ 
            update woUpdateList;
        }
    }
    public static void insertTask(list<Task> taskList){
        if(!taskList.isEmpty())
            insert taskList;
    }
}