/*******************************************************************************************************
Description: Trigger handler for Service Quote Trigger

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger: SVMX_ServiceQuoteTrggr.cls
    Service Manager: SVMX_ServiceQuoteServiceManager
    Test class: SVMX_ServiceQuoteHandler_UT.cls
    
 
Author: Ranjitha S
Date: 16-11-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_ServiceQuoteTrggrHandler extends SVMX_TriggerHandler {

    private list<SVMXC__Quote__c > newSQList;
    private list<SVMXC__Quote__c > oldSQList;
    private Map<Id, SVMXC__Quote__c > newSQMap;
    private Map<Id, SVMXC__Quote__c > oldSQMap;
    
    public SVMX_ServiceQuoteTrggrHandler() {
        this.newSQList = (list<SVMXC__Quote__c >) Trigger.new;
        this.oldSQList = (list<SVMXC__Quote__c >) Trigger.old;
        this.newSQMap = (Map<Id, SVMXC__Quote__c >) Trigger.newMap;
        this.oldSQMap = (Map<Id, SVMXC__Quote__c >) Trigger.oldMap;
    }
    
    public override void afterUpdate(){
        List<SVMXC__Quote__c> quoteList = new List<SVMXC__Quote__c>();
         List<SVMXC__Quote__c> statquoteList = new List<SVMXC__Quote__c>();
          List<SVMXC__Quote__c> statChangeQList = new List<SVMXC__Quote__c>();
        
        
         for(SVMXC__Quote__c sq: newSQList){
            if((sq.SVMXC__Status__c == 'Quote Approved'|| sq.SVMXC__Status__c == 'Quote Rejected') && sq.SVMXC__Status__c != oldSQMap.get(sq.Id).SVMXC__Status__c){
                statquoteList.add(sq);
            }
        }
        if(!statquoteList.isEmpty()) {
            //SVMX_ServiceQuoteServiceManager.updateOpp(statquoteList);
        }

        for(SVMXC__Quote__c sq: newSQList){
            if(sq.SVMXC__Status__c != oldSQMap.get(sq.Id).SVMXC__Status__c){
                statChangeQList.add(sq);
            }
        }

         //if(statChangeQList.size() > 0)
         for(SVMXC__Quote__c sq: newSQList){
            if((sq.SVMXC__Status__c == 'Submitted For Approval') && sq.SVMXC__Status__c != oldSQMap.get(sq.Id).SVMXC__Status__c){
                quoteList.add(sq);
            }
        }
        
        for(SVMXC__Quote__c quote: quoteList){
            //if(sq.SVMX_Approval_Flag__c == True && sq.SVMX_Approval_Flag__c != oldSQMap.get(sq.id).SVMX_Approval_Flag__c){
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                //req.setComments('Submitted for approval. Please approve.');
                req.setObjectId(quote.id);
                // submit the approval request
                Approval.ProcessResult result = Approval.process(req);
            //}
        }
        
    
/*List<SVMXC__Quote__c>special = new List<SVMXC__Quote__c>();
    for(SVMXC__Quote__c sq: newSQList){
            if((sq.SVMXC__Status__c == 'Submit For Approval') &&(sq.SVMX_Special_Approval__c == true) && sq.SVMXC__Status__c != oldSQMap.get(sq.Id).SVMXC__Status__c){
                special.add(sq);
            }
        }
        for(SVMXC__Quote__c sq : special) {
            //if(sq.SVMX_Special_Approval__c == true) {
                
                Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                    req.setComments('Submitted for approval. Please approve.');
                    req.setObjectId(sq.id);
                    // submit the approval request
                    Approval.ProcessResult result = Approval.process(req);
            //}
            
        }*/

}
    public override void beforeUpdate() {
        List<SVMXC__Quote__c> quoteList = new List<SVMXC__Quote__c>();
  

        List<SVMXC__Quote__c> quoteAmountChangeList = new List<SVMXC__Quote__c>() ;
        for(SVMXC__Quote__c sq: newSQList) {
            if(sq.SVMXC__Quote_Amount2__c  != null && sq.SVMXC__Quote_Amount2__c !=oldSQMap.get(sq.id).SVMXC__Quote_Amount2__c && sq.SVMX_PS_Country__c != null ){
               
                quoteAmountChangeList.add(sq) ;
            }
            
        }

        if(quoteAmountChangeList.size() > 0)
            SVMX_ServiceQuoteServiceManager.updateQuoteAmountLevel(quoteAmountChangeList);

        Boolean upOwner= false;

        for(SVMXC__Quote__c sq: newSQList){
            system.debug('sq.SVMXC__Status__c '+sq.SVMXC__Status__c);
            if(sq.SVMXC__Status__c == 'Submitted For Approval' )
                upOwner=true;

            if((sq.SVMXC__Status__c == 'Submitted For Approval'|| (sq.SVMXC__Status__c == 'Level 1 Approved' && sq.SVMX_Approval_Levels__c > 1) || (sq.SVMXC__Status__c == 'Level 2 Approved' && sq.SVMX_Approval_Levels__c > 2)|| (sq.SVMXC__Status__c == 'Level 3 Approved' && sq.SVMX_Approval_Levels__c > 3)) && sq.SVMXC__Status__c != oldSQMap.get(sq.Id).SVMXC__Status__c){
                quoteList.add(sq);
            }


        }
        if(!quoteList.isEmpty()) {
            if(upOwner)
            SVMX_ServiceQuoteServiceManager.updateQuoteForOwnerChange(quoteList) ;

           SVMX_ServiceQuoteServiceManager.updateApproverLevel(quoteList) ;
           
            
            /*for(SVMXC__Quote__c sq: quoteList){
               
                //if(sq.SVMX_Approval_Flag__c == True && sq.SVMX_Approval_Flag__c != oldSQMap.get(sq.id).SVMX_Approval_Flag__c){
                    Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                    req.setComments('Submitted for approval. Please approve.');
                    req.setObjectId(sq.id);
                    // submit the approval request
                    Approval.ProcessResult result = Approval.process(req);
                
                //}
            } */
        }
    }


    public override void beforeInsert() {
       
        List<SVMXC__Quote__c> QList = new List<SVMXC__Quote__c>();
        List<SVMXC__Quote__c>quoteList = new List<SVMXC__Quote__c>() ;
        

        
        for(SVMXC__Quote__c quote : newSQList){
             if(quote.SVMX_PS_Country__c != null)
            QList.add(quote) ;
        }
        
        if(QList.size() > 0)
        SVMX_ServiceQuoteServiceManager.updateQuoteFieldsQC(QList) ;      
      
    }
}