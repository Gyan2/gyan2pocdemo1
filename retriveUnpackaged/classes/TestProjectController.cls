/**
 * Created by rebmangu on 23/04/2018.
 */

@IsTest
public with sharing class TestProjectController {

    @testSetup
    static void setup() {

        Building__c building = new Building__c(
                Name='Building',
                City__c='Strasbourg',
                Country__c='France',
                CountryIsoCode__c='FR',
                Street__c='24 Rue du general de gaulle',
                Zip__c='67000',
                Geolocation__Latitude__s=48.578606,
                Geolocation__Longitude__s=7.769301
        );

        insert building;


        Project__c project = new Project__c(
                Name='Test Project',
                City__c='Strasbourg',
                Country__c='France',
                CountryIsoCode__c='FR',
                Street__c='24 Rue du general de gaulle',
                MKS__c=true,
                Zip__c='67000',
                Geolocation__Latitude__s=48.578606,
                Geolocation__Longitude__s=7.769301,
                BuildingRef__c=building.Id


        );

        insert project;
    }




    static testMethod void TestCheckForProjectsAround(){
        Map<String,String> t = new Map<String,String>{
                'Geolocation__Latitude__s' => '48.578606',
                'Geolocation__Longitude__s' => '7.769301'
        };

        List<Project__c> projects = ProjectController.checkForProjectsAround(JSON.serialize(t),5);

        System.assertEquals(1,projects.size());

    }


}