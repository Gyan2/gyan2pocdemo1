/** 
* @author Service Max (pooja1.singh@ge.com)
* @date 02/03/2018 (dd/mm/yyyy) 
* @description This class is test class for ServiceMaxInboundAPI class
*
* VERSION HISTORY
* @author FULL_NAME (EMAIL)
* @date DD/MM/YYYY (dd/mm/yyyy)  
* @description SHORT FUNCTIONAL DESCRIPTION 
*/
@isTest
public class ServiceMaxInboundAPI_TEST {
    public static testMethod void ServiceMaxInboundMethod(){
        
        //insert Account
        Account acc = SVMX_TestUtility.CreateAccount('TestAccount', 'United Kingdom', false);
        acc.SVMX_Account_country__c = 'Norway';
        acc.ExternalId__c = '12345';
        insert acc;
        
        //insert Contact
        Contact con = SVMX_TestUtility.CreateContact(acc.id, false);
        con.SVMX_PS_External_ID__c = '12345';
        insert con;
        
        //insert Project
        Project__c proj = new Project__c (ExternalId__c = '12345');
        insert proj;
        
        //insert Location
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('TestLocation', acc.id, false);
        loc.SVMX_PS_External_ID__c = '12345';
        insert loc;
        
        //insert Technician
        /*Profile pfle = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
        User u = SVMX_TestUtility.CreateUser('uLastName', pfle.Id, false);
        u.EmployeeNumber = '1234';
        u.SalesOrg__c = '9999';
        u.UserCountry__c = 'US';
        insert u;
        SVMXC__Service_Group__c svg = SVMX_TestUtility.CreateServiceGroup('Test group',true);
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Test Technician', u.id, svg.id, true);*/
        
        List<ServiceMaxInboundAPI.SAPServiceOrderItem> servOrderItemList = new List<ServiceMaxInboundAPI.SAPServiceOrderItem>();
        ServiceMaxInboundAPI.SAPServiceOrderItem servOrderItem = new ServiceMaxInboundAPI.SAPServiceOrderItem();
        servOrderItem.AccountNumber = '12345';
        servOrderItem.ContactNumber = '12345';
        servOrderItem.Description = 'Test Description';
        servOrderItem.Duration = 5.0;
        servOrderItem.FirstDate = system.now();
        servOrderItem.LocationNumber = '12345';
        servOrderItem.Origin = 'Phone';
        servOrderItem.SalesOrderItemNumber = '001000';
        //servOrderItem.TechnicianNumber = '1234';
        servOrderItem.Type = 'New';
        
        servOrderItemList.add(servOrderItem);


        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader.AccountNumber = '12345';
        servOrderHeader.ContactNumber = '12345';
        servOrderHeader.CurrencyType = 'EUR';
        servOrderHeader.CustomerPONumber = '112233445566';
        servOrderHeader.ProjectNumber = '12345';
        servOrderHeader.SalesOrderNumber = '1234567';
        servOrderHeader.ServiceOrderItems = servOrderItemList;
        servOrderHeader.SID = 'PS8';

        ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader);
    }
    
    public static testMethod void ServiceMaxInboundMethod1() {
        
        //insert Account
        Account acc = SVMX_TestUtility.CreateAccount('TestAccount', 'United Kingdom', false);
        acc.ExternalId__c = '12345';
        acc.SVMX_Account_country__c = 'Norway';
        insert acc;
        
        //insert Contact
        Contact con = SVMX_TestUtility.CreateContact(acc.id, false);
        con.SVMX_PS_External_ID__c = '12345';
        insert con;
        
        //insert Project
        Project__c proj = new Project__c (ExternalId__c = '12345');
        insert proj;
        
        //insert Location
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('TestLocation', acc.id, false);
        loc.SVMX_PS_External_ID__c = '12345';
        insert loc;
        
        //insert Technician
        /*Profile pfle = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
        User u = SVMX_TestUtility.CreateUser('uLastName', pfle.Id, false);
        u.EmployeeNumber = '1234';
        u.SalesOrg__c = '9999';
        u.UserCountry__c = 'US';
        insert u;
        SVMXC__Service_Group__c svg = SVMX_TestUtility.CreateServiceGroup('Test group',true);
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Test Technician', u.id, svg.id, true);*/
        
        List<ServiceMaxInboundAPI.SAPServiceOrderItem> servOrderItemList = new List<ServiceMaxInboundAPI.SAPServiceOrderItem>();
        ServiceMaxInboundAPI.SAPServiceOrderItem servOrderItem = new ServiceMaxInboundAPI.SAPServiceOrderItem();
        servOrderItem.AccountNumber = '12345';
        servOrderItem.ContactNumber = '12345';
        servOrderItem.Description = 'Test Description';
        servOrderItem.Duration = 5.0;
        servOrderItem.FirstDate = system.now();
        servOrderItem.LocationNumber = '12345';
        servOrderItem.Origin = 'Phone';
        servOrderItem.SalesOrderItemNumber = '12345';
        //servOrderItem.TechnicianNumber = '1234';
        servOrderItem.Type = 'New';
        //servOrderItemList.add(servOrderItem);

        ServiceMaxInboundAPI.SAPServiceOrderItem servOrderItem1 = new ServiceMaxInboundAPI.SAPServiceOrderItem();
        servOrderItem1.AccountNumber = '';
        servOrderItem1.ContactNumber = '';
        servOrderItem1.Description = '';
        servOrderItem1.Duration = 5.0;
        servOrderItem1.FirstDate = system.now();
        servOrderItem1.LocationNumber = '12345';
        servOrderItem1.Origin = 'Phone';
        servOrderItem1.SalesOrderItemNumber = '';
        //servOrderItem1.TechnicianNumber = '';
        servOrderItem1.Type = 'New';
        servOrderItem1.Description = '';
        servOrderItemList.add(servOrderItem1);
        

        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader.AccountNumber = '';
        servOrderHeader.ProjectNumber = '';
        servOrderHeader.ProjectManager = '';
        servOrderHeader.SalesOrderNumber = '';
        servOrderHeader.SID ='';
        servOrderHeader.CurrencyType = '';
        servOrderHeader.CustomerPONumber = '';
        servOrderHeader.ContactNumber = '12345';
        servOrderHeader.ServiceOrderItems = servOrderItemList;

        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader1 = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader1.AccountNumber = '12345';
        servOrderHeader1.ProjectNumber = '';
        servOrderHeader1.ProjectManager = '';
        servOrderHeader1.SalesOrderNumber = '';
        servOrderHeader1.SID ='';
        servOrderHeader1.CurrencyType = '';
        servOrderHeader1.CustomerPONumber = '';
        servOrderHeader1.ContactNumber = '12345';
        servOrderHeader1.ServiceOrderItems = servOrderItemList;

        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader2 = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader2.AccountNumber = '12345';
        servOrderHeader2.ProjectNumber = '12345';
        servOrderHeader2.ProjectManager = '';
        servOrderHeader2.SalesOrderNumber = '';
        servOrderHeader2.SID ='';
        servOrderHeader2.CurrencyType = '';
        servOrderHeader2.CustomerPONumber = '';
        servOrderHeader2.ContactNumber = '12345';
        servOrderHeader2.ServiceOrderItems = servOrderItemList;

        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader3 = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader.AccountNumber = '12345';
        servOrderHeader.ProjectNumber = '12345';
        servOrderHeader.ProjectManager = '12345';
        servOrderHeader.SalesOrderNumber = '';
        servOrderHeader.SID ='';
        servOrderHeader.CurrencyType = '';
        servOrderHeader.CustomerPONumber = '';
        servOrderHeader.ContactNumber = '12345';
        servOrderHeader.ServiceOrderItems = servOrderItemList;

        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader4 = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader.AccountNumber = '12345';
        servOrderHeader.ProjectNumber = '12345';
        servOrderHeader.ProjectManager = '12345';
        servOrderHeader.SalesOrderNumber = '12345';
        servOrderHeader.SID ='';
        servOrderHeader.CurrencyType = '';
        servOrderHeader.CustomerPONumber = '';
        servOrderHeader.ContactNumber = '12345';
        servOrderHeader.ServiceOrderItems = servOrderItemList;

        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader5 = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader.AccountNumber = '12345';
        servOrderHeader.ProjectNumber = '12345';
        servOrderHeader.ProjectManager = '12345';
        servOrderHeader.SalesOrderNumber = '12345';
        servOrderHeader.SID ='12345';
        servOrderHeader.CurrencyType = '';
        servOrderHeader.CustomerPONumber = '';
        servOrderHeader.ContactNumber = '12345';
        servOrderHeader.ServiceOrderItems = servOrderItemList;

        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader6 = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader6.AccountNumber = '12345';
        servOrderHeader6.ProjectNumber = '12345';
        servOrderHeader6.ProjectManager = '12345';
        servOrderHeader6.SalesOrderNumber = '12345';
        servOrderHeader6.SID ='12345';
        servOrderHeader6.CurrencyType = '12345';
        servOrderHeader6.CustomerPONumber = '';
        servOrderHeader6.ContactNumber = '12345';
        servOrderHeader6.ServiceOrderItems = servOrderItemList;

        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader7 = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader7.AccountNumber = '12345';
        servOrderHeader7.ProjectNumber = '12345';
        servOrderHeader7.ProjectManager = '12345';
        servOrderHeader7.SalesOrderNumber = '12345';
        servOrderHeader7.SID ='12345';
        servOrderHeader7.CurrencyType = '12345';
        servOrderHeader7.CustomerPONumber = '12345';
        servOrderHeader7.ContactNumber = '';
        servOrderHeader7.ServiceOrderItems = servOrderItemList;

        ServiceMaxInboundAPI.SAPServiceOrderHeader servOrderHeader8 = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        servOrderHeader8.AccountNumber = '12345';
        servOrderHeader8.ProjectNumber = '12345';
        servOrderHeader8.ProjectManager = '12345';
        servOrderHeader8.SalesOrderNumber = '12345';
        servOrderHeader8.SID ='12345';
        servOrderHeader8.CurrencyType = '12345';
        servOrderHeader8.CustomerPONumber = '12345';
        servOrderHeader8.ContactNumber = '12345';
        servOrderHeader8.ServiceOrderItems = servOrderItemList;
        
       
        try{
          ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader);
          ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader1);
          ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader2);
          ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader3);
          ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader4);
          ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader5);
          ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader6);
          ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader7);
          ServiceMaxInboundAPI.inboundSAPServiceOrder(servOrderHeader8);
        }
        catch(Exception e){}
    }

    public static testMethod void ServiceMaxInboundMethod2() {

        //insert Account
        Account acc = SVMX_TestUtility.CreateAccount('TestAccount', 'United Kingdom', false);
        acc.SVMX_Account_country__c = 'Norway';
        acc.ExternalId__c = '12345';
        insert acc;
        
        //insert Contact
        Contact con = SVMX_TestUtility.CreateContact(acc.id, false);
        con.SVMX_PS_External_ID__c = '12345';
        insert con;
        
        //insert Project
        Project__c proj = new Project__c (ExternalId__c = '12345');
        insert proj;
        
        //insert Location
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('TestLocation', acc.id, false);
        loc.SVMX_PS_External_ID__c = '12345';
        loc.SVMXC_SAP_Storage_Location__c = 'T100';
        loc.SVMXC__Stocking_Location__c = true;
        insert loc;

        //insert Location
        SVMXC__Site__c loc2 = SVMX_TestUtility.CreateLocation('TestLocation2', acc.id, false);
        loc2.SVMX_PS_External_ID__c = '54321';
        loc2.SVMXC_SAP_Storage_Location__c = 'T200';
        loc2.SVMXC__Stocking_Location__c = true;
        insert loc2;

        Product2 prod = SVMX_TestUtility.CreateProduct('Test Product', false);
        prod.SAPNumber__c = 'PS8-12122112';
        prod.ExternalId__c = '12122112';
        insert prod;

        //insert Product Stock on Location
        SVMXC__Product_Stock__c prodStock = new SVMXC__Product_Stock__c();
        prodStock.SVMXC__Product__c = prod.id;
        prodStock.SVMXC__Location__c = loc.id;
        prodStock.SVMXC__Status__c = 'Available';
        prodStock.SVMXC__Quantity2__c = 10;
        //prodStock.SVMXC_SAP_Product_Number__c = 'PS8-12122112';
        insert prodStock;

        SVMXC__Product_Stock__c prodStock1 = new SVMXC__Product_Stock__c();
        prodStock1.SVMXC__Product__c = prod.id;
        prodStock1.SVMXC__Location__c = loc2.id;
        prodStock1.SVMXC__Status__c = 'Available';
        prodStock1.SVMXC__Quantity2__c = 10;
        //prodStock.SVMXC_SAP_Product_Number__c = 'PS8-12122112';
        insert prodStock1;

        //Create Stock Transfer
        SVMXC__Stock_Transfer__c stockTransfer = new SVMXC__Stock_Transfer__c();
        stockTransfer.SVMXC__Source_Location__c = loc.id;
        stockTransfer.SVMXC__Destination_Location__c = loc2.id;
        stockTransfer.SVMXC_Approved__c = false;
        insert stockTransfer;

        SVMXC__Stock_Transfer_Line__c stockTransferLine = new SVMXC__Stock_Transfer_Line__c();
        stockTransferLine.SVMXC__Product__c = prod.id;
        stockTransferLine.SVMXC__Quantity_Transferred2__c = 5.0;
        stockTransferLine.SVMXC_Sender_Stocking_Location__c = loc.id;
        stockTransferLine.SVMXC_Unit_of_Measure__c = 'PCE';
        stockTransferLine.SVMXC_Reason_For_Transfer__c = 'Restocking';
        stockTransferLine.SVMXC_SAP_Sender_Location__c = 'T100';
        stockTransferLine.SVMXC_SAP_Receiver_Location__c = 'T200';
        stockTransferLine.SVMXC__Stock_Transfer__c = stockTransfer.id;
        insert stockTransferLine;

        //Create Stock Transfer
        SVMXC__Stock_Transfer__c stockTransfer1 = new SVMXC__Stock_Transfer__c();
        stockTransfer1.SVMXC__Source_Location__c = loc2.id;
        stockTransfer1.SVMXC__Destination_Location__c = loc.id;
        stockTransfer1.SVMXC_Approved__c = false;
        insert stockTransfer1;

        SVMXC__Stock_Transfer_Line__c stockTransferLine1 = new SVMXC__Stock_Transfer_Line__c();
        stockTransferLine1.SVMXC__Product__c = prod.id;
        stockTransferLine1.SVMXC__Quantity_Transferred2__c = 5.0;
        stockTransferLine1.SVMXC_Sender_Stocking_Location__c = loc2.id;
        stockTransferLine1.SVMXC_Unit_of_Measure__c = 'PCE';
        stockTransferLine1.SVMXC_Reason_For_Transfer__c = 'Restocking';
        stockTransferLine1.SVMXC_SAP_Sender_Location__c = 'T100';
        stockTransferLine1.SVMXC_SAP_Receiver_Location__c = 'T200';
        stockTransferLine1.SVMXC__Stock_Transfer__c = stockTransfer.id;
        insert stockTransferLine1;


        //Populate structure and Call Apex Web Service
        ServiceMaxInboundAPI.SAPStockAdjustment stockAdjustment = new ServiceMaxInboundAPI.SAPStockAdjustment();
        stockAdjustment.MaterialNumber = 'PS8-12122112';
        stockAdjustment.Quantity = 5.0;
        stockAdjustment.FromLocationNumber = '12345';
        stockAdjustment.ToLocationNumber = '';

        ServiceMaxInboundAPI.SAPStockAdjustment stockAdjustment1 = new ServiceMaxInboundAPI.SAPStockAdjustment();
        stockAdjustment.MaterialNumber = 'PS8-12122112';
        stockAdjustment.Quantity = 5.0;
        stockAdjustment.FromLocationNumber = '';
        stockAdjustment.PurchaseRequisitionNumber =Decimal.valueof('232434');
        stockAdjustment.ToLocationNumber = '54321';
      
        ServiceMaxInboundAPI.inboundSAPStockMovement(stockAdjustment);
        ServiceMaxInboundAPI.inboundSAPStockMovement(stockAdjustment1);

        ServiceMaxInboundAPI.SAPServiceOrderHeader inboundclass = new ServiceMaxInboundAPI.SAPServiceOrderHeader();
        ServiceMaxInboundAPI.SAPServiceOrderItem inboundclass1 = new ServiceMaxInboundAPI.SAPServiceOrderItem();
        ServiceMaxInboundAPI.SVMXResponse inboundclass2 = new ServiceMaxInboundAPI.SVMXResponse();
        ServiceMaxInboundAPI.SVMXResponseDetail inboundclass3 = new ServiceMaxInboundAPI.SVMXResponseDetail();
        ServiceMaxInboundAPI.SAPStockAdjustment inboundclass4= new ServiceMaxInboundAPI.SAPStockAdjustment();
           
    }

    public static testMethod void ServiceMaxInboundMethod3() {

        //insert Account
        Account acc = SVMX_TestUtility.CreateAccount('TestAccount', 'United Kingdom', false);
        acc.SVMX_Account_country__c = 'Norway';
        acc.ExternalId__c = '12345';
        insert acc;
        
        //insert Contact
        Contact con = SVMX_TestUtility.CreateContact(acc.id, false);
        con.SVMX_PS_External_ID__c = '12345';
        insert con;
        
        //insert Project
        Project__c proj = new Project__c (ExternalId__c = '12345');
        insert proj;
        
        //insert Location
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('TestLocation', acc.id, false);
        loc.SVMX_PS_External_ID__c = '12345';
        loc.SVMXC_SAP_Storage_Location__c = 'T100';
        loc.SVMXC__Stocking_Location__c = true;
        insert loc;

        //insert Location
        SVMXC__Site__c loc2 = SVMX_TestUtility.CreateLocation('TestLocation2', acc.id, false);
        loc2.SVMX_PS_External_ID__c = '54321';
        loc2.SVMXC_SAP_Storage_Location__c = 'T200';
        loc2.SVMXC__Stocking_Location__c = true;
        insert loc2;

        Product2 prod = SVMX_TestUtility.CreateProduct('Test Product', false);
        prod.SAPNumber__c = 'PS8-12122112';
        prod.ExternalId__c = '12122112';
        insert prod;

        //insert Product Stock on Location
        SVMXC__Product_Stock__c prodStock = new SVMXC__Product_Stock__c();
        prodStock.SVMXC__Product__c = prod.id;
        prodStock.SVMXC__Location__c = loc.id;
        prodStock.SVMXC__Status__c = 'Available';
        prodStock.SVMXC__Quantity2__c = 10;
        //prodStock.SVMXC_SAP_Product_Number__c = 'PS8-12122112';
        insert prodStock;

        

        //Create Stock Transfer
        SVMXC__Stock_Transfer__c stockTransfer = new SVMXC__Stock_Transfer__c();
        stockTransfer.SVMXC__Source_Location__c = loc.id;
        stockTransfer.SVMXC__Destination_Location__c = loc2.id;
        stockTransfer.SVMXC_Approved__c = false;
        insert stockTransfer;

        SVMXC__Stock_Transfer_Line__c stockTransferLine = new SVMXC__Stock_Transfer_Line__c();
        stockTransferLine.SVMXC__Product__c = prod.id;
        stockTransferLine.SVMXC__Quantity_Transferred2__c = 5.0;
        stockTransferLine.SVMXC_Sender_Stocking_Location__c = loc.id;
        stockTransferLine.SVMXC_Unit_of_Measure__c = 'PCE';
        stockTransferLine.SVMXC_Reason_For_Transfer__c = 'Restocking';
        stockTransferLine.SVMXC_SAP_Sender_Location__c = 'T100';
        stockTransferLine.SVMXC_SAP_Receiver_Location__c = 'T200';
        stockTransferLine.SVMXC__Stock_Transfer__c = stockTransfer.id;
        insert stockTransferLine;

        

        //Populate structure and Call Apex Web Service
        ServiceMaxInboundAPI.SAPStockAdjustment stockAdjustment = new ServiceMaxInboundAPI.SAPStockAdjustment();
        stockAdjustment.MaterialNumber = 'PS8-12122112';
        stockAdjustment.Quantity = 5.0;
        stockAdjustment.FromLocationNumber = '12345';
        stockAdjustment.ToLocationNumber = '';

        
      
        ServiceMaxInboundAPI.inboundSAPStockMovement(stockAdjustment);
        //ServiceMaxInboundAPI.inboundSAPStockMovement(stockAdjustment1);

        
    }

}