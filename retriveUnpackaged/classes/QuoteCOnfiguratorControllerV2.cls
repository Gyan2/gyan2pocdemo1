/**
 * Created by trakers on 22.05.18.
 */

public with sharing class QuoteCOnfiguratorControllerV2 {

    public class QuoteWithLineItems {
        @AuraEnabled
        public Quote quote;
        @AuraEnabled
        public Map<String,String> quoteFields;
        @AuraEnabled
        public Map<String,String> quoteLineItemFields;
    }

    @AuraEnabled
    public static QuoteWithLineItems loadQuote(String paramId,List<String> paramQuoteFields,List<String> paramQuoteLineItemFields){
        QuoteWithLineItems  returned = new QuoteWithLineItems();
                            returned.quoteFields = new Map<String,String>();
                            returned.quoteLineItemFields = new Map<String,String>();

        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
        Schema.SObjectType quoteSchema = schemaMap.get('Quote');
        Map<String, Schema.SObjectField> quoteFieldsMap = quoteSchema.getDescribe().fields.getMap();
        Schema.SObjectType quoteLineItemSchema = schemaMap.get('QuoteLineItem');
        Map<String, Schema.SObjectField> quoteLineItemFieldsMap = quoteLineItemSchema.getDescribe().fields.getMap();

        for(String field : paramQuoteFields) {
            if (!field.contains('.')) { // TODO Prepare for fields like Account.Name
                returned.quoteFields.put(field, quoteFieldsMap.get(field).getDescribe().getLabel());
            }
        }

        for(String field : paramQuoteLineItemFields) {
            System.debug(field);
            if (!field.contains('.')) {// TODO Prepare for fields like Account.Name
                returned.quoteLineItemFields.put(field, quoteLineItemFieldsMap.get(field).getDescribe().getLabel());
            }
        }

        if (paramQuoteFields.isEmpty()) {
            paramQuoteFields.add('Id');
        }

        if (!paramQuoteLineItemFields.isEmpty()) {
            paramQuoteFields.add(String.format('(SELECT {0} FROM QuoteLineItems ORDER BY Position__c NULLS LAST)',new List<String>{String.join(paramQuoteLineItemFields,',')}));
        }

        String query = String.format(
                'SELECT {0} FROM Quote WHERE Id = \'\'{1}\'\'',
                new List<String>{String.join(paramQuoteFields,','), paramId});

        System.debug(query);
        returned.quote = database.query(query);
        return returned;
    }

}