public with sharing class PrintCallReport{ 
    
    private final Event event;
    public List<Contact> invitee {get; set;}
    public String localeDate {get; set;}
    public PrintCallReport() {
        event = [SELECT AccountId,ActivityDate,Description,Id,Location,Subject,OwnerID,owner.name,Account.Name,Account.BillingStreet,Account.BillingCity,Account.BillingPostalCode FROM Event WHERE Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];	
    	localeDate = event.ActivityDate.format();
        invitee = [SELECT
                      Id,
                      Name
                  From
                  	Contact
                  WHERE
                  	ID IN (
                    	SELECT 
                        	RelationID
                    	FROM
                    		EventRelation
                    	WHERE
                        	EventID = :event.id AND
                    		Relation.Type='Contact')];
    }
    
    
    public Event getEvent() {
        return Event;
    }
}