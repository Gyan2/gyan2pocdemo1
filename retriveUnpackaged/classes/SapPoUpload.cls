// @description Upload service for SapPo
// @copyright Parx
// @author MHA
// @lastmodify 24.04.2017
// 
public without sharing class SapPoUpload {
    //Prepare related data from Opportunity id
    private static Set<String> kabaContactUserRoles = new Set<String>{
        'Sales Rep','Back Office','Project Manager'
    };
    public static SapPoXML.Data setData(Id oppId)
    {
        //Opportunity fields
        Organization org = [Select Id,IsSandbox From Organization Limit 1];
        Opportunity opp = [Select Id, Name, zOfferCreationDate__c, zOfferNumber__c, zSapBillingNumber__c, zSapOrderNumber__c,
                           AccountId,Account.Name, Account.BillingStreet, Account.BillingPostalCode, Account.BillingCity,
                           ProjectRef__c,ProjectRef__r.Name
                           From Opportunity Where Id = :oppId Limit 1];
        
        SapPoXML.Data el = new SapPoXML.Data();
        el.objectName = opp.Name; 
        el.ProjectName = opp.ProjectRef__r.Name;
        if(org.IsSandbox)
        {
           el.objectName = 'Test-' +  opp.Name;
           el.ProjectName = 'Test-' +  opp.ProjectRef__r.Name;
        }
        if(opp.zOfferCreationDate__c != null)
        {
        	el.offer_Date = opp.zOfferCreationDate__c.format(); 
        }
        el.offer_No = opp.zOfferNumber__c;
        el.sapOrderNo = opp.zSapOrderNumber__c;
        el.sapBillingNo = opp.zSapBillingNumber__c;
        
        //Account fields
        el.companyName = opp.Account.Name;
        el.companyAdress = opp.Account.BillingStreet;
        el.companyZip = opp.Account.BillingPostalCode;
        el.companyCity = opp.Account.BillingCity;
        
        //Contact for this Account
		List<OpportunityContactRole> opportunityMainContactList = new List<OpportunityContactRole>();
        opportunityMainContactList = [SELECT Id, Contact.Firstname, Contact.Lastname, Contact.Salutation, Contact.Phone, 
                                  Contact.Fax, Contact.Email
                       			FROM OpportunityContactRole Where OpportunityId =:oppId AND IsPrimary = true 
                        		Limit 1];
        
        if(!opportunityMainContactList.isEmpty())
        {
			el.contactSalutation = opportunityMainContactList[0].Contact.Salutation;
            el.contactPrename = opportunityMainContactList[0].Contact.FirstName;
            el.contactName = opportunityMainContactList[0].Contact.LastName;
            el.contactPhone = opportunityMainContactList[0].Contact.Phone;
            el.contactFax = opportunityMainContactList[0].Contact.Fax;
            el.contactEmail = opportunityMainContactList[0].Contact.Email;            
        }
        
        //Contact 1,2,3 : Opportunity Team members are represented as contact 1,2,3 in the XML
        List<OpportunityTeamMember> teamList = new List<OpportunityTeamMember>();
        teamList = [SELECT Id, Name, toLabel(TeamMemberRole),
                    User.Firstname,User.Lastname,User.Phone,User.Fax,User.Email
                    FROM OpportunityTeamMember Where OpportunityId = :oppId AND TeamMemberRole IN :kabaContactUserRoles
                    Order By CH_Ordernumber__c ASC Limit 3 ];
        if(teamList.size() > 0)
        {
             el.kabaContact1_Firstname = teamList[0].User.Firstname;
             el.kabaContact1_Name = teamList[0].User.Lastname;
             el.kabaContact1_Function = teamList[0].TeamMemberRole;
             el.kabaContact1_Phone = teamList[0].User.Phone;
             el.kabaContact1_Fax = teamList[0].User.Fax;
             el.kabaContact1_Email = teamList[0].User.Email;          
        }
        if(teamList.size() > 1)
        {
             el.kabaContact2_Firstname = teamList[1].User.Firstname;
             el.kabaContact2_Name = teamList[1].User.Lastname;
             el.kabaContact2_Function = teamList[1].TeamMemberRole;
             el.kabaContact2_Phone = teamList[1].User.Phone;
             el.kabaContact2_Fax = teamList[1].User.Fax;
             el.kabaContact2_Email = teamList[1].User.Email;      
        }
        if(teamList.size() > 2)
        {
             el.kabaContact3_Firstname = teamList[2].User.Firstname;
             el.kabaContact3_Name = teamList[2].User.Lastname;
             el.kabaContact3_Function = teamList[2].TeamMemberRole;
             el.kabaContact3_Phone = teamList[2].User.Phone;
             el.kabaContact3_Fax = teamList[2].User.Fax;
             el.kabaContact3_Email = teamList[2].User.Email;                 
        }
        //Contact 4 is empty by design
        //Contact 5,6 Partner accounts are represented in kabaContact 5,6, for Name get latest Contact that matches Partner and can be found in contact roles

        List<Partner > partnersList = new List<Partner >();
        partnersList = [SELECT Id, Role, AccountToId, AccountTo.Name,AccountTo.Fax,
                        AccountTo.BillingPostalCode,AccountTo.BillingCity,AccountTo.Phone, AccountTo.CorporateEmail__c,
                        AccountTo.BillingStreet
                        FROM Partner  Where OpportunityId = :oppId 
                        AND Role NOT IN ('Client','Vendor') AND AccountToId != :opp.AccountId 
                        Order By LastmodifiedDate Desc Limit 2];
        Set<Id> partnerAccountIds = new Set<Id>();        
        for(Partner op : partnersList)
        {
            partnerAccountIds.add(op.AccountToId);
        }
        
        List<OpportunityContactRole> partherContactList = new List<OpportunityContactRole>();
        partherContactList = [Select Id,Contact.FirstName,Contact.LastName,Contact.Fax,Contact.AccountId,
                              Contact.Title
                              From OpportunityContactRole Where OpportunityId =:oppId 
                              AND Contact.AccountId IN :partnerAccountIds AND isPrimary = false 
                              Order By Contact.LastmodifiedDate Desc ];
        Map<Id,OpportunityContactRole> accIdContactMap = new Map<Id,OpportunityContactRole>();
        for(OpportunityContactRole c : partherContactList)
        {
            if(!accIdContactMap.containsKey(c.Contact.AccountId))
            {
                accIdContactMap.put(c.Contact.AccountId, c);
            }
        }
        OpportunityContactRole tempC;
        if(partnersList.size() > 0)
        {
            tempC = accIdContactMap.get(partnersList[0].AccountToId);
            if( tempC != null)
            {
                el.kabaContact5_Firstname = tempC.Contact.Firstname;
                el.kabaContact5_Name = tempC.Contact.Lastname;
                el.kabaContact5_Function = tempC.Contact.Title;
            }
            
            el.kabaContact5_Fax = partnersList[0].AccountTo.Fax;
            el.kabaContact5_Company = partnersList[0].AccountTo.Name;
        	el.kabaContact5_PlzCity = partnersList[0].AccountTo.BillingPostalCode +' '+ partnersList[0].AccountTo.BillingCity;
        	el.kabaContact5_Street = partnersList[0].AccountTo.BillingStreet;
            el.kabaContact5_Phone = partnersList[0].AccountTo.Phone;
            el.kabaContact5_Email = partnersList[0].AccountTo.CorporateEmail__c;    
        }
        if(partnersList.size() > 1)
        {
            tempC = accIdContactMap.get(partnersList[1].AccountToId);
            if( tempC != null)
            {
                el.kabaContact6_Firstname = tempC.Contact.Firstname;
                el.kabaContact6_Name = tempC.Contact.Lastname;             
                el.kabaContact6_Function = tempC.Contact.Title;
            }
            el.kabaContact6_Fax = partnersList[1].AccountTo.Fax;
            el.kabaContact6_Company = partnersList[1].AccountTo.Name;
        	el.kabaContact6_PlzCity = partnersList[1].AccountTo.BillingPostalCode +' '+ partnersList[1].AccountTo.BillingCity;
        	el.kabaContact6_Street = partnersList[1].AccountTo.BillingStreet;
            el.kabaContact6_Phone = partnersList[1].AccountTo.Phone;
            el.kabaContact6_Email = partnersList[1].AccountTo.CorporateEmail__c;    
        }       
		return el;        
    }
    //async send and update integration message
    @Future(callout=true)
    public static void sendMessage(Id oppId, Id integrationMessageId)
    {       
        SapPoXML.Data data = new SapPoXML.Data();
        data = setData(oppId);
        
        String status = '';        
        HttpResponse response;
        String result = '';
        try
        {      
        	response = SapPoXML.sendRequest(data);
            if(Test.isRunningTest() || response.getStatusCode()==200)
            {	
                status = 'Success';
                result = 'OK';
            }
            else
            {
                status ='Error';
            	result = response.getStatus();                
            }
        }
        catch (Exception e)
        {
           System.debug(Logginglevel.ERROR,e.getMessage()); 
           status = 'Error';
           result = response.getBody();
        }
        
        if(integrationMessageId != null)
        {
    		IntegrationMessage.updateMessage(integrationMessageId, status, result);
        }
    }
}