/*******************************************************************************************************
* Class Name      	: MDM001_ManagementSegmentation
* Description     	: Data Manager for Custom Metadata Type
* Author          	: Paul Carmuciano
* Created On      	: 2017-09-27
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-09-27         1000                 Initial version
******************************************************************************************************/
public class MDM001_ManagementSegmentation {
    
    /***
* Method name	: currencyRatesForSpecified
* Description	: This variable provides the Management_Segmentation__mdt mapping
* Author		: Paul Carmuciano
* Return Type	: Map<String,_mdt> of metadataTypes by Name
* Parameter		: None
*/
    public static Map<String,Management_Segmentation__mdt> ManagementSegmentationMapping {
        get {
            if (ManagementSegmentationMapping==null) {
                ManagementSegmentationMapping = new Map<String,Management_Segmentation__mdt>();
                for (List<Management_Segmentation__mdt> msmdts : [
                    SELECT DeveloperName,Id,Label,MasterLabel,Region__c,Segment__c FROM Management_Segmentation__mdt
                ]) {
                    for (Management_Segmentation__mdt msmdt : msmdts)
                        ManagementSegmentationMapping.put(msmdt.DeveloperName, msmdt);
                }
            }
            return ManagementSegmentationMapping;
        }
        set;
    }
    
    
}