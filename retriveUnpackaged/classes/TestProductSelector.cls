/**
 * Created by rebmangu on 05/03/2018.
 */

@IsTest
private class TestProductSelector {

    public static string SALESORG = '5500';

    @testSetup static void setup(){

        Product2 product = new Product2(
                Name='Test',
                SalesSpecifications__c='{"blabla":"blabla"}',
                ExternalId__c = 'PS8-00000001',
                SAPNumber__c = '00000001',
                QuantityUnitOfMeasure = 'PCE'
        );
        insert product;

        ProductDetail__c productDetail = new ProductDetail__c(
            Language__c = 'EN', Description__c = 'DESCRIPTION BLA BLA BLA',ProductRef__c=product.Id,ExternalId__c='PS8-00000001-EN'
        );
        insert productDetail;

        //Standard pricebook
        PricebookEntry pbEntryStandard = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(),Product2Id=product.Id,UnitPrice=1000,IsActive=true);
        insert pbEntryStandard;


        //Custom pricebook
        Pricebook2 pbC = new Pricebook2(ExternalId__c=SALESORG,Name=SALESORG,IsActive=true,CurrencyIsoCode='CHF');
        insert pbC;

        PricebookEntry pbE = new PricebookEntry(Pricebook2Id=pbC.Id,Product2Id=product.Id,UnitPrice=999,CurrencyIsoCode='CHF',IsActive=true);
        insert pbE;




        Account acc = new Account(SAPNumber__c='123456789',Name='Test Account');
        insert acc;

        Opportunity opp = new Opportunity(Name='Test opp',AccountId=acc.Id,StageName='Proposal/Price Quote',CloseDate=Date.valueOf(System.now()));
        insert opp;

        Quote quo = new Quote(OpportunityId=opp.Id,CurrencyIsoCode='CHF',Name='Test Quote',SalesOrg__c=SALESORG,Pricebook2Id=Test.getStandardPricebookId(),ScheduledDate__c=Date.valueOf(System.now()));
        insert quo;

        QuoteLineItem quoItem = new QuoteLineItem(Product2Id=product.Id,Quantity=10,QuoteId=quo.Id,PricebookEntryId=pbEntryStandard.Id,UnitPrice=1000);
        insert quoItem;



    }

    static testMethod void testGetProducts() {
        String quoteId = [select id from Quote limit 1].Id;
        String fields = '[{"Label":"Name","ApiName":"Name"},{"Label":"Code","ApiName":"Id"},{"Label":"Description","ApiName":"Description"}]';

        System.assertEquals(SelectProductsAuraController.getProducts('BLA',SALESORG,quoteId,fields,'EN').size() == 1,true);
        System.assertEquals(SelectProductsAuraController.getProducts('BLAAAAA',SALESORG,quoteId,fields,'EN').size() > 0,false);
    }

    static testMethod void coverSimpleMethods(){
        String quoteId = [select id from Quote limit 1].Id;

        System.assertEquals(SelectProductsAuraController.getLanguages('EN').size() > 0,true);
        System.assertEquals(SelectProductsAuraController.getSalesOrg(quoteId),SALESORG);
    }

    static testMethod void testGenerationAndInsertonOfQuoteLineItems(){
        String quoteId = [select id from Quote limit 1].Id;
        List<String> selected = new List<String>{[select id from Product2 limit 1].Id};

        List<QuoteLineItem>  result = SelectProductsAuraController.generateQuoteLineItems(selected,SALESORG,quoteId);
        System.assertEquals(result.size() == 1,true);

        // Simulate the user entering the quantity before clicking save
        for(QuoteLineItem ql : result){
            ql.Quantity = 1.0;
        }


        String response = SelectProductsAuraController.insertQuoteLineItems(result);
        System.assertEquals(response,SelectProductsAuraController.SUCCESS);
        System.assertEquals([select count() from QuoteLineItem],2);
    }

}