public class SVMX_StockTransferserviceManager{

    public static void InventoryHandler(set<Id> stockTransferIds){

        list<SVMXC__Stock_Transfer_Line__c> stlList = new list<SVMXC__Stock_Transfer_Line__c> ();
        list<SVMXC__Stock_Transfer_Line__c> stlOfNotApprovalReqLocationList = new list<SVMXC__Stock_Transfer_Line__c> ();
        list<SVMXC__Stock_Transfer_Line__c> stlOfApprovalReqLocationList = new list<SVMXC__Stock_Transfer_Line__c> ();

        if(stockTransferIds.size() > 0){

            system.debug('stockTransferIds.size()--->'+stockTransferIds.size());

            stlList = [select id,name,SVMXC__Product__c,SVMXC__Quantity_Transferred2__c,SVMXC_Sender_Stocking_Location__c,
                       SVMXC_Receiver_Stocking_Location__c,SVMXC__Stock_Transfer__c,
                       SVMXC__Stock_Transfer__r.SVMXC__Source_Location__r.SVMX_Approval_Required__c 
                       from SVMXC__Stock_Transfer_Line__c 
                       where SVMXC__Stock_Transfer__c in: stockTransferIds 
                       ];
        }

        system.debug('stlList--->'+stlList);

        if(stlList.size() > 0){

            system.debug('stlList.size()--->'+stlList.size());

            for(SVMXC__Stock_Transfer_Line__c stockTranfrline:stlList){

                if(stockTranfrline.SVMXC__Stock_Transfer__r.SVMXC__Source_Location__r.SVMX_Approval_Required__c = false){

                        stlOfNotApprovalReqLocationList.add(stockTranfrline);
                }

                if(stockTranfrline.SVMXC__Stock_Transfer__r.SVMXC__Source_Location__r.SVMX_Approval_Required__c = true){

                        stlOfApprovalReqLocationList.add(stockTranfrline);
                }
            }
                       
        }
        if(stlOfNotApprovalReqLocationList.size()>0){
            SVMX_InventoryManagement AutomaticInven=new SVMX_InventoryManagement();
            AutomaticInven.updateInventoryToInTransit(stlOfNotApprovalReqLocationList);
        } 
        
        if(stlOfApprovalReqLocationList.size()>0){
            SVMX_InventoryManagement AutomaticInven=new SVMX_InventoryManagement();
            AutomaticInven.updateInventoryToInTransit(stlOfApprovalReqLocationList);
        }     

    }
}