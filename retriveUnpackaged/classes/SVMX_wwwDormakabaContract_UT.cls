@IsTest
private class SVMX_wwwDormakabaContract_UT
{
    static testMethod void coverTypes(){

        new SVMX_wwwDormakabaContract.BusinessTransactionDocumentReference();
        new SVMX_wwwDormakabaContract.Description();
        new SVMX_wwwDormakabaContract.Email();
        new SVMX_wwwDormakabaContract.Address();
        new SVMX_wwwDormakabaContract.Telephone();
        new SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader();
        new SVMX_wwwDormakabaContract.ProductInternalID();
        new SVMX_wwwDormakabaContract.BusinessDocumentMessageID();
        new SVMX_wwwDormakabaContract.ContactPerson();
        new SVMX_wwwDormakabaContract.DateTime_x();
        new SVMX_wwwDormakabaContract.ProductStandardID();
        new SVMX_wwwDormakabaContract.Party();
        new SVMX_wwwDormakabaContract.MEDIUM_Name();
        new SVMX_wwwDormakabaContract.Facsimile();
        new SVMX_wwwDormakabaContract.BusinessDocumentTextCollectionText();
        new SVMX_wwwDormakabaContract.BusinessScopeID();
        new SVMX_wwwDormakabaContract.Communication();
        new SVMX_wwwDormakabaContract.BusinessDocumentTextCollection();
        new SVMX_wwwDormakabaContract.EmailURI();
        new SVMX_wwwDormakabaContract.BusinessScope();
        new SVMX_wwwDormakabaContract.BusinessTransactionDocumentID();
        new SVMX_wwwDormakabaContract.Quantity();
        new SVMX_wwwDormakabaContract.ContactPersonInternalID();
        new SVMX_wwwDormakabaContract.PhysicalAddress();
        new SVMX_wwwDormakabaContract.Text();

        new SVMX_wwwDormakabaContract.PartyStandardID();
        new SVMX_wwwDormakabaContract.Web();
        new SVMX_wwwDormakabaContract.PhoneNumber();
        new SVMX_wwwDormakabaContract.PartyInternalID();
        new SVMX_wwwDormakabaContract.WorkplaceAddress();
        new SVMX_wwwDormakabaContract.BusinessScopeInstanceID();
        
    }
}