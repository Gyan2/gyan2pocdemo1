global without sharing class WS_Hybris {
    
    private final static String QUOTE_BASKET_STATUS = 'Draft';


    /**
     *  TODO Check mapping for GenericProduct.CustomName = QuoteLineItem.Description__c ?
     *
     *
     */



    global interface HybrisRequestPart {
        Boolean hasRequiredFieldsMissing();
    }


/* Titles made by http://patorjk.com/software/taag/ Font Doom */

/**  _____                     _      ______          _           _
    /  ___|                   | |     | ___ \        (_)         | |
    \ `--.  ___  __ _ _ __ ___| |__   | |_/ / __ ___  _  ___  ___| |_
     `--. \/ _ \/ _` | '__/ __| '_ \  |  __/ '__/ _ \| |/ _ \/ __| __|
    /\__/ /  __/ (_| | | | (__| | | | | |  | | | (_) | |  __/ (__| |_
    \____/ \___|\__,_|_|  \___|_| |_| \_|  |_|  \___/| |\___|\___|\__|
                                                    _/ |
                                                   |__/               */

    global class SearchProjectRequest implements HybrisRequestPart {
        webservice String PartnerId;
        webservice String UserId;
        webservice String SearchTerm; // Searches for Project.Name (Opportunity.PPName__c),
        webservice Integer Page;
        webservice Integer PageSize;
        webservice SortOrderEnum SortOrder;

        public Boolean hasRequiredFieldsMissing() {
            return String.isBlank(PartnerId) || Page == null || PageSize == null || SortOrder == null;
        }
    }

    webservice static SearchProjectResponse SearchProject(SearchProjectRequest request) {
        SearchProjectResponse response = new SearchProjectResponse();
        // Check required fields
        if (request.hasRequiredFieldsMissing()) {
            //response.StatusCode = STATUS_REQUIRED_FIELDS_MISSING;
            response.StatusCode = ResponseStatusEnum.REQUIRED_FIELDS_MISSING;
        } else {
            try {
                Set<String> projectStatusFilter = new Set<String>{ 'READ', 'EDIT' };
                String externalIdFilter = 'PS8-' + request.PartnerId;

                List<String> whereClauseList = new List<String>{
                    'PPAccess__c IN :projectStatusFilter',
                    'Account.ExternalId__c = :externalIdFilter'
                };

                if (!String.isEmpty(request.UserId)) {
                    whereClauseList.add('CreatedById__c = :request.UserId');
                }
                if (!String.isEmpty(request.SearchTerm)) {
                    whereClauseList.add('PPName__c LIKE \'%' + String.escapeSingleQuotes(request.SearchTerm) + '%\'');
                }

                String query = String.format(
                    'SELECT {0} FROM Opportunity WHERE {1} ORDER BY {2} LIMIT {3} OFFSET {4}',
                    new List<String>{
                        String.join(ProjectOverviewFields, ','),
                        String.join(whereClauseList, ' AND '),
                        new Map<SortOrderEnum, String>{
                            SortOrderEnum.CREATED_DATE_ASC => 'PPCreatedDate__c ASC',
                            SortOrderEnum.CREATED_DATE_DESC => 'PPCreatedDate__c DESC',
                            SortOrderEnum.MODIFIED_DATE_ASC => 'PPLastModifiedDate__c ASC',
                            SortOrderEnum.MODIFIED_DATE_DESC => 'PPLastModifiedDate__c DESC',
                            SortOrderEnum.PROJECT_NAME_ASC => 'PPName__c ASC',
                            SortOrderEnum.PROJECT_NAME_DESC => 'PPName__c DESC'
                        }.get(request.SortOrder),
                        String.valueOf(request.PageSize),
                        String.valueOf((request.PageSize * (request.Page - 1)))
                    }
                );

                String queryCount = String.format(
                    'SELECT COUNT() FROM Opportunity WHERE {0}',
                    new List<String>{
                        String.join(whereClauseList, ' AND ')
                    }
                );


                System.debug(query);
                System.debug(queryCount);


                List<Opportunity> opportunities = Database.query(query);
                response.TotalRecords = database.countQuery(queryCount);

                //response.StatusCode = STATUS_SUCCESS;
                response.StatusCode = ResponseStatusEnum.SUCCESS;

                response.Projects = new List<ProjectOverview>();

                for (Opportunity forOpportunity : opportunities) {
                    response.Projects.add(new ProjectOverview(forOpportunity));
                }
            } catch (Exception e) {
                //response.StatusCode = STATUS_UNEXPECTED_EXCEPTION;
                response.StatusCode = ResponseStatusEnum.GENERAL_ERROR;
                response.UnhandledErrorMessage = String.valueOf(e);
            }
        }

        return response;
    }

    global class SearchProjectResponse{
        webservice ResponseStatusEnum StatusCode;
        webservice String UnhandledErrorMessage;
        webservice Integer TotalRecords;
        webservice List<ProjectOverview> Projects;
    }


/**  _____      _    ______          _           _
    |  __ \    | |   | ___ \        (_)         | |
    | |  \/ ___| |_  | |_/ / __ ___  _  ___  ___| |_
    | | __ / _ \ __| |  __/ '__/ _ \| |/ _ \/ __| __|
    | |_\ \  __/ |_  | |  | | | (_) | |  __/ (__| |_
     \____/\___|\__| \_|  |_|  \___/| |\___|\___|\__|
                                   _/ |
                                  |__/               */

    global class GetProjectRequest implements HybrisRequestPart{
        webservice String PartnerId;
        webservice String ProjectId;

        global Boolean hasRequiredFieldsMissing() {
            return String.isBlank(PartnerId) || String.isBlank(ProjectId);
        }
    }

    webservice static GetProjectResponse GetProject(GetProjectRequest request) {
        GetProjectResponse response = new GetProjectResponse();
        // Check required fields
        if (request.hasRequiredFieldsMissing()) {
            //response.StatusCode = STATUS_REQUIRED_FIELDS_MISSING;
            response.StatusCode = ResponseStatusEnum.REQUIRED_FIELDS_MISSING;
        } else {
            try {
                List<Opportunity> opportunities = [SELECT PPName__c, PPDescription__c, PPJSON__c, PPCreatedById__c, PPCreatedDate__c, PPLastModifiedById__c, PPLastModifiedDate__c, CloseDate, (SELECT Id from Quotes WHERE Status = :QUOTE_BASKET_STATUS) FROM Opportunity WHERE Account.ExternalId__c = :('PS8-' + request.PartnerId) AND Id = :request.ProjectId AND PPAccess__c IN ('READ','EDIT')];

                if (opportunities.isEmpty()) {
                    //response.StatusCode = STATUS_NOT_FOUND; // NOT FOUND
                    response.StatusCode = ResponseStatusEnum.NOT_FOUND;
                } else {
                    //response.StatusCode = STATUS_SUCCESS;
                    response.StatusCode = ResponseStatusEnum.SUCCESS;
                    response.Project = new ProjectDetail(opportunities.get(0));
                }
            } catch (Exception e) {
                //response.StatusCode = STATUS_UNEXPECTED_EXCEPTION;
                response.StatusCode = ResponseStatusEnum.GENERAL_ERROR;
                response.UnhandledErrorMessage = String.valueOf(e);
            }
        }

        return response;
    }

    global class GetProjectResponse {
        webservice ResponseStatusEnum StatusCode;
        webservice String UnhandledErrorMessage;

        webservice ProjectDetail Project;
    }

/*   _____                _        ______          _           _
    /  __ \              | |       | ___ \        (_)         | |
    | /  \/_ __ ___  __ _| |_ ___  | |_/ / __ ___  _  ___  ___| |_
    | |   | '__/ _ \/ _` | __/ _ \ |  __/ '__/ _ \| |/ _ \/ __| __|
    | \__/\ | |  __/ (_| | ||  __/ | |  | | | (_) | |  __/ (__| |_
     \____/_|  \___|\__,_|\__\___| \_|  |_|  \___/| |\___|\___|\__|
                                                 _/ |
                                                |__/               */

    global class CreateProjectRequest implements HybrisRequestPart{
        webservice String PartnerId;
        webservice String UserId;
        webservice String SalesOrg;
        webservice String Name;
        webservice String Description;
        webservice String CurrencyIsoCode;

        public Boolean hasRequiredFieldsMissing() {
            return String.isBlank(PartnerId) || String.isBlank(UserId) || String.isBlank(SalesOrg) || String.isBlank(Name) || String.isBlank(CurrencyIsoCode);
        }
    }

    webservice static CreateProjectResponse CreateProject(CreateProjectRequest request) {
        CreateProjectResponse response = new CreateProjectResponse();
        if (request.hasRequiredFieldsMissing()) {
            //response.StatusCode = STATUS_REQUIRED_FIELDS_MISSING;
            response.StatusCode = ResponseStatusEnum.REQUIRED_FIELDS_MISSING;
        } else {
            System.Savepoint savepoint = database.setSavepoint();

            try {
                Opportunity newOpportunity = new Opportunity(
                    Account = new Account(ExternalId__c = 'PS8-' + request.PartnerId),
                    Name = request.Name,
                    StageName = 'Prospecting',
                    CloseDate = System.today(),
                    PPName__c = request.Name,
                    PPDescription__c = request.Description,
                    //SalesOrg__c = request.SalesOrg,
                    PPAccess__c = 'EDIT',
                    PPCreatedById__c = request.UserId,
                    PPCreatedDate__c = System.now(),
                    PPLastModifiedById__c = request.UserId,
                    PPLastModifiedDate__c = System.now(),
                    CurrencyIsoCode = request.CurrencyIsoCode
                );
                CoreSObjectDomain.getTriggerEvent(Opportunities.class).disableAll();
                CoreSObjectDomain.getTriggerEvent(Quotes.class).disableAll();
                insert newOpportunity;
                insert new Quote(
                    OpportunityId = newOpportunity.Id,
                    Pricebook2 = new Pricebook2(ExternalId__c = request.SalesOrg),
                    Status = QUOTE_BASKET_STATUS,
                    SalesOrg__c = request.SalesOrg,
                    Name = QUOTE_BASKET_STATUS // TODO find a good Name for the Quote
                );
                //response.StatusCode = STATUS_SUCCESS;
                response.StatusCode = ResponseStatusEnum.SUCCESS;
                response.ProjectId = newOpportunity.Id;


            } catch (Exception e) {
                //response.StatusCode = STATUS_UNEXPECTED_EXCEPTION;
                response.StatusCode = ResponseStatusEnum.GENERAL_ERROR;
                response.UnhandledErrorMessage = String.valueOf(e);
            } finally {
                //if (response.StatusCode != STATUS_SUCCESS) database.rollback(savepoint);
                if (response.StatusCode != ResponseStatusEnum.SUCCESS) database.rollback(savepoint);
            }
        }

        return response;
    }

    global class CreateProjectResponse {
        webservice ResponseStatusEnum StatusCode;
        webservice String UnhandledErrorMessage;
        webservice String ProjectId;
    }


/**   ___      _     _  ______              _            _
     / _ \    | |   | | | ___ \            | |          | |
    / /_\ \ __| | __| | | |_/ / __ ___   __| |_   _  ___| |_
    |  _  |/ _` |/ _` | |  __/ '__/ _ \ / _` | | | |/ __| __|
    | | | | (_| | (_| | | |  | | | (_) | (_| | |_| | (__| |_
    \_| |_/\__,_|\__,_| \_|  |_|  \___/ \__,_|\__,_|\___|\__|

                                                             */

    global class AddProductRequest implements HybrisRequestPart{
        webservice String PartnerId;
        webservice String UserId;
        webservice String ProjectId;
        webservice GenericProduct Product;

        public Boolean hasRequiredFieldsMissing() {
            return String.isBlank(PartnerId) || String.isBlank(UserId) || String.isBlank(ProjectId) || Product.hasRequiredFieldsMissing();
        }
    }

    webservice static AddProductResponse AddProduct(AddProductRequest request) {
        AddProductResponse response = new AddProductResponse();
        if (request.hasRequiredFieldsMissing()) {
            //response.StatusCode = STATUS_REQUIRED_FIELDS_MISSING;
            response.StatusCode = ResponseStatusEnum.REQUIRED_FIELDS_MISSING;
        } else {
            System.Savepoint savepoint = database.setSavepoint();
            try {
                List<Quote> baskets = [SELECT Opportunity.PPAccess__c, SalesOrg__c FROM Quote WHERE Opportunity.Account.ExternalId__c = :('PS8-' + request.PartnerId) AND OpportunityId = :request.ProjectId AND Status = :QUOTE_BASKET_STATUS];

                if (baskets.isEmpty()) {
                    //response.StatusCode = STATUS_NOT_FOUND;
                    response.StatusCode = ResponseStatusEnum.NOT_FOUND;
                } else if (baskets.size() > 1) {
                    //response.StatusCode = STATUS_UNEXPECTED_EXCEPTION;
                    response.StatusCode = ResponseStatusEnum.GENERAL_ERROR;
                    response.UnhandledErrorMessage = 'More than one Record found';
                } else {
                    Quote quote = baskets.get(0);
                    if (quote.Opportunity.PPAccess__c != 'EDIT') {
                        //response.StatusCode = STATUS_NO_EDIT_ACCESS;
                        response.StatusCode = ResponseStatusEnum.NOT_EDITABLE;
                    } else {
                        QuoteLineItem qli = request.Product.getQuoteLineItem();
                        qli.QuoteId = quote.Id;

                        PriceBookEntry pbe = [SELECT UnitPrice FROM PricebookEntry WHERE Pricebook2.ExternalId__c = :quote.SalesOrg__c AND Product2.ExternalId__c = :('PS8-' + request.Product.SapNumber) AND CurrencyIsoCode = 'USD'];

                        qli.UnitPrice = pbe.UnitPrice;
                        qli.PricebookEntryId = pbe.Id;
                        //CoreSObjectDomain.getTriggerEvent(QuoteLineItems.class).disableAll();
                        CoreSObjectDomain.getTriggerEvent(Quotes.class).disableAll();
                        insert qli;
                        //response.StatusCode = STATUS_SUCCESS;
                        response.StatusCode = ResponseStatusEnum.SUCCESS;
                    }
                }
            } catch (Exception e) {
                //response.StatusCode = STATUS_UNEXPECTED_EXCEPTION;
                response.StatusCode = ResponseStatusEnum.GENERAL_ERROR;
                response.UnhandledErrorMessage = String.valueOf(e);
            } finally {
                //if (response.StatusCode != STATUS_SUCCESS) database.rollback(savepoint);
                if (response.StatusCode != ResponseStatusEnum.SUCCESS) database.rollback(savepoint);
            }
        }


        return response;
    }

    global class AddProductResponse {
        webservice ResponseStatusEnum StatusCode;
        webservice String UnhandledErrorMessage;
    }



/**  _   _           _       _        ______              _            _
    | | | |         | |     | |       | ___ \            | |          | |
    | | | |_ __   __| | __ _| |_ ___  | |_/ / __ ___   __| |_   _  ___| |_ ___
    | | | | '_ \ / _` |/ _` | __/ _ \ |  __/ '__/ _ \ / _` | | | |/ __| __/ __|
    | |_| | |_) | (_| | (_| | ||  __/ | |  | | | (_) | (_| | |_| | (__| |_\__ \
     \___/| .__/ \__,_|\__,_|\__\___| \_|  |_|  \___/ \__,_|\__,_|\___|\__|___/
          | |
          |_|                                                                  */


    global class UpdateProductsRequest implements HybrisRequestPart{
        webservice String PartnerId;
        webservice String UserId;
        webservice String ProjectId;
        webservice List<GenericProduct> Products;
        webservice Boolean KeepConfigurationXML;

        public Boolean hasRequiredFieldsMissing() {
            Boolean productsHasRequiredFieldsMissing = false;
            for (GenericProduct forGP : Products) {
                if (forGP.hasRequiredFieldsMissing()) {
                    productsHasRequiredFieldsMissing = true;
                    break;
                }
            }
            return String.isBlank(PartnerId) || String.isBlank(UserId) || String.isBlank(ProjectId) || productsHasRequiredFieldsMissing || KeepConfigurationXML == null;
        }
    }

    webservice static UpdateProductsResponse UpdateProducts(UpdateProductsRequest request) {
        UpdateProductsResponse response = new UpdateProductsResponse();

        if (request.hasRequiredFieldsMissing()) {
            //response.StatusCode = STATUS_REQUIRED_FIELDS_MISSING;
            response.StatusCode = ResponseStatusEnum.REQUIRED_FIELDS_MISSING;
        } else {
            System.Savepoint savepoint = database.setSavepoint();

            try {

                List<Quote> validDraftQuote = [
                    SELECT
                        (SELECT Id FROM QuoteLineItems)
                    FROM Quote
                    WHERE Opportunity.Account.SAPNumber__c = :request.PartnerId AND
                        OpportunityId = :request.ProjectId AND
                        Status = :QUOTE_BASKET_STATUS AND
                        Opportunity.PPAccess__c = 'EDIT'
                ];

                if (validDraftQuote.size() == 0) {
                    //response.StatusCode = STATUS_NOT_FOUND;
                    response.StatusCode = ResponseStatusEnum.NOT_FOUND;
                } else if (validDraftQuote.size() == 1) {
                    Set<Id> validQuoteLineItems = (new Map<Id, QuoteLineItem>(validDraftQuote.get(0).QuoteLineItems)).keySet();
                    List<QuoteLineItem> quoteLineItems = new List<QuoteLineItem>();
                    List<QuoteLineItem> quoteLineItemsToDelete = new List<QuoteLineItem>();

                    //response.StatusCode = STATUS_SUCCESS;
                    response.StatusCode = ResponseStatusEnum.SUCCESS;

                    for (GenericProduct forGP : request.Products) {
                        QuoteLineItem forQLI = forGP.getQuoteLineItem(request.KeepConfigurationXML);
                        // check that we are trying to update a quoteLineItem where we do have edit access with this partner
                        if (!validQuoteLineItems.contains(forQLI.Id)) {
                            //response.StatusCode = STATUS_NO_EDIT_ACCESS;
                            response.StatusCode = ResponseStatusEnum.NOT_EDITABLE;
                            break;
                        }
                        if (forQLI.Quantity == 0) { // Setting item quantity to 0, means to delete the item.
                            quoteLineItemsToDelete.add(forQLI);
                        } else {
                            quoteLineItems.add(forQLI);
                        }
                    }
                    //if (response.StatusCode == STATUS_SUCCESS) {
                    if (response.StatusCode == ResponseStatusEnum.SUCCESS) {
                        CoreSObjectDomain.getTriggerEvent(Quotes.class).disableAll();
                        delete quoteLineItemsToDelete;
                        update quoteLineItems;
                    }
                }

            } catch (Exception e) {
                //response.StatusCode = STATUS_UNEXPECTED_EXCEPTION;
                response.StatusCode = ResponseStatusEnum.GENERAL_ERROR;
                response.UnhandledErrorMessage = String.valueOf(e);
            } finally {
                //if (response.StatusCode != STATUS_SUCCESS) database.rollback(savepoint);
                if (response.StatusCode != ResponseStatusEnum.SUCCESS) database.rollback(savepoint);
            }
        }


        return response;
    }

    global class UpdateProductsResponse {
        webservice ResponseStatusEnum StatusCode;
        webservice String UnhandledErrorMessage;
    }

/** _____      _   _____             _
   |  __ \    | | |  _  |           | |
   | |  \/ ___| |_| | | |_   _  ___ | |_ ___  ___
   | | __ / _ \ __| | | | | | |/ _ \| __/ _ \/ __|
   | |_\ \  __/ |_\ \/' / |_| | (_) | ||  __/\__ \
    \____/\___|\__|\_/\_\\__,_|\___/ \__\___||___/
 */


    global class GetQuotesRequest implements HybrisRequestPart{
        webservice String PartnerId;
        webservice String ProjectId;

        public Boolean hasRequiredFieldsMissing() {
            return String.isBlank(PartnerId) || String.isBlank(ProjectId);
        }
    }

    webservice static GetQuotesResponse GetQuotes(GetQuotesRequest request) {
        GetQuotesResponse response = new GetQuotesResponse();
        if (request.hasRequiredFieldsMissing()) {
            response.StatusCode = ResponseStatusEnum.REQUIRED_FIELDS_MISSING;
        } else {
            try {
                List<Quote> quotes = [
                    SELECT Id
                    FROM Quote
                    WHERE Opportunity.Account.ExternalId__c = :('PS8-' + request.PartnerId) AND
                        OpportunityId = :request.ProjectId AND
                        Opportunity.PPAccess__c IN ('READ','EDIT') AND
                        Status != :QUOTE_BASKET_STATUS
                    ORDER BY CreatedDate DESC];

                for (Quote forQuote : quotes) {
                    response.quotes.add(new QuoteDetail(forQuote.Id)); // TODO: bulkify query so it does not fail when used in a long loop
                }

            } catch (Exception e) {
                response.StatusCode = ResponseStatusEnum.GENERAL_ERROR;
                response.UnhandledErrorMessage = String.valueOf(e);
            }
        }
        return response;
    }

    global class GetQuotesResponse {
        webservice ResponseStatusEnum StatusCode;
        webService String UnhandledErrorMessage;
        webservice Integer TotalRecords;
        webservice List<QuoteDetail> quotes = new List<QuoteDetail>();
    }


/*    ___      _     _ _ _   _                   _   _    _
     / _ \    | |   | (_) | (_)                 | | | |  | |
    / /_\ \ __| | __| |_| |_ _  ___  _ __   __ _| | | |  | |_ __ __ _ _ __  _ __   ___ _ __ ___
    |  _  |/ _` |/ _` | | __| |/ _ \| '_ \ / _` | | | |/\| | '__/ _` | '_ \| '_ \ / _ \ '__/ __|
    | | | | (_| | (_| | | |_| | (_) | | | | (_| | | \  /\  / | | (_| | |_) | |_) |  __/ |  \__ \
    \_| |_/\__,_|\__,_|_|\__|_|\___/|_| |_|\__,_|_|  \/  \/|_|  \__,_| .__/| .__/ \___|_|  |___/
                                                                     | |   | |
                                                                     |_|   |_|                  */

// https://dev-dormakaba-testing.cs83.force.com/Hybris/
    global class GenericProduct implements HybrisRequestPart{
        webservice ProductTypeEnum ProductType;
        webservice String SapNumber;
        webservice String BasketItemId;
        webservice Integer Quantity;
        webservice String AdditionalInfoJSON;
        webservice String CustomName;
        webservice String ConfigurationXML;
        webservice String ConfigurationId; // TODO - Map to field

        global GenericProduct(){}

        global QuoteLineItem getQuoteLineItem(Boolean paramKeepConfiguration){
            QuoteLineItem returned;
            if (String.isBlank(BasketItemId)) returned = new QuoteLineItem();
            else returned = new QuoteLineItem(Id = BasketItemId);

            returned.Product2 = new Product2(ExternalId__c = 'PS8-' + SapNumber);
            returned.Quantity = Quantity;
            returned.PPAdditionalInfoJSON__c = AdditionalInfoJSON;
            returned.PPJSON__c = JSON.serialize(new Map<String,String>{
                'ProductType' => new Map<ProductTypeEnum, String>{
                        ProductTypeEnum.STO => 'STO',
                        ProductTypeEnum.CPQ => 'CPQ',
                        ProductTypeEnum.MKS => 'MKS',
                        ProductTypeEnum.IGS => 'IGS'
                    }.get(ProductType),
                'CustomName' => CustomName
            });
            returned.Description__c = CustomName;
            if (paramKeepConfiguration == false) returned.Configuration__c = ConfigurationXML;
            return returned;
        }

        global QuoteLineItem getQuoteLineItem() {
            return getQuotelineItem(false);
        }

        public GenericProduct(QuoteLineItem paramQuoteLineItem) {
            if (!String.isBlank(paramQuoteLineItem.PPJSON__c) && paramQuoteLineItem.PPJSON__c.left(1) == '{') {
                Map<String,String> jsonData = (Map<String,String>)JSON.deserialize(paramQuoteLineItem.PPJSON__c, Map<String,String>.class);
                ProductType = new Map<String, ProductTypeEnum>{
                        'STO' => ProductTypeEnum.STO,
                        'CPQ' => ProductTypeEnum.CPQ,
                        'MKS' => ProductTypeEnum.MKS,
                        'IGS' => ProductTypeEnum.IGS
                }.get(jsonData.get('ProductType'));

                CustomName = jsonData.get('CustomName');
            }

            SapNumber = paramQuoteLineItem.Product2.SAPNumber__c;
            BasketItemId = paramQuoteLineItem.Id;
            Quantity = Integer.valueOf(paramQuoteLineItem.Quantity);
            AdditionalInfoJSON = paramQuoteLineItem.PPAdditionalInfoJSON__c;
            ConfigurationXML = paramQuoteLineItem.Configuration__c;
        }

        public Boolean hasRequiredFieldsMissing() {
            return String.isBlank(SapNumber) || Quantity == null;
        }
    }


    private static List<String> ProjectOverviewFields = new List<String>{'PPName__c','PPDescription__c','PPJSON__c','PPCreatedDate__c','PPCreatedById__c','PPLastModifiedDate__c','PPLastModifiedById__c'};
    global class ProjectOverview {
        webservice String ProjectId;
        webservice OwnerTypeEnum OwnerType; // OwnerType
        webservice String Name;
        webservice String Description;
        webservice ProjectStatusEnum Status;// status
        webservice String ClientName;
        webservice DateTime CreatedDate;
        webservice String CreatedById;
        webservice DateTime ModifiedDate;
        webservice String ModifiedById;


        global ProjectOverview(Opportunity paramOpportunity) {
            ProjectId = paramOpportunity.Id;
            OwnerType = OwnerTypeEnum.PARTNER; // TODO change to check if is DORMAKABA OR PARTNER OwnerType
            Name = paramOpportunity.PPName__c;
            Description = paramOpportunity.PPDescription__c;

            if (paramOpportunity.PPJSON__c != null && paramOpportunity.PPJSON__c.left(1) == '{') {
                Map<String, String> jsonData = (Map<String,String>)JSON.deserialize(paramOpportunity.PPJSON__c, Map<String,String>.class);
                Status = new Map<String, ProjectStatusEnum>{
                    'Open'=>ProjectStatusEnum.OPEN,
                    'Completed'=>ProjectStatusEnum.COMPLETED,
                    'Cancelled'=>ProjectStatusEnum.CANCELLED
                }.get(jsonData.get('ProjectStatus'));
                ClientName = jsonData.get('ClientName');
            }
            CreatedDate = paramOpportunity.PPCreatedDate__c;
            CreatedById = paramOpportunity.PPCreatedById__c;
            ModifiedDate = paramOpportunity.PPLastModifiedDate__c;
            ModifiedById = paramOpportunity.PPLastModifiedById__c;
        }
    }

    private static Set<String> ProjectDetailFields = new Set<String>{'PPName__c', 'PPDescription__c', 'PPJSON__c','CloseDate','PPCreatedDate__c','PPCreatedById__c','PPLastModifiedDate__c','PPLastModifiedById__c'};
    global class ProjectDetail {
        webservice String ProjectId;
        webservice String Name;
        webservice String Description;
        webservice ProjectStatusEnum Status;
        //webservice List<Address> Addresses; // ADDRESSES
        webservice String ArchitectName;
        webservice String GeneralContractorName;
        webservice Date EstimatedDate;
        webservice String ObjectZip;
        webservice String ObjectCity;
        webservice String Notes;
        webservice QuoteDetail Basket;// Basket
        webservice DateTime CreatedDate;
        webservice String CreatedById;
        webservice DateTime ModifiedDate;
        webservice String ModifiedById;


        public ProjectDetail(Opportunity paramOpportunity) {
            ProjectId = paramOpportunity.Id;
            //OwnerType
            Name = paramOpportunity.PPName__c;
            Description = paramOpportunity.PPDescription__c;
            if (paramOpportunity.PPJSON__c != null && paramOpportunity.PPJSON__c.left(1) == '{') {
                Map<String, String> jsonInfo = (Map<String, String>) JSON.deserialize(paramOpportunity.PPJSON__c, Map<String, String>.class);
                ArchitectName = jsonInfo.get('ArchitectName');
                GeneralContractorName = jsonInfo.get('GeneralContractorName');
                ObjectZip = jsonInfo.get('ObjectZip');
                ObjectCity = jsonInfo.get('ObjectCity');
                Notes = jsonInfo.get('Notes');
            }


            EstimatedDate = paramOpportunity.CloseDate;

            if (paramOpportunity.Quotes.size() == 1) {
                Basket = new QuoteDetail(paramOpportunity.Quotes.get(0).Id);
            }

            CreatedDate = paramOpportunity.PPCreatedDate__c;
            CreatedById = paramOpportunity.PPCreatedById__c;
            ModifiedDate = paramOpportunity.PPLastModifiedDate__c;
            ModifiedById = paramOpportunity.PPLastModifiedById__c;
        }
    }

    global class Address {
        webservice String Name;
        webservice AddressTypeEnum AddressType;

        webservice String CountryCode;
        webservice String PostalCode;
        webservice String City;
        webservice String Street;
        webservice String StateCode;
    }


    global class QuoteDetail {
        webservice String Id;
        webservice String ProjectId;
        webservice String SapQuoteNumber;
        webservice List<Address> Addresses = new List<Address>();
        webservice List<GenericProduct> Products;
        webservice Date CreatedDate;
        webservice Date ExpirationDate;

        /* this constructor is an exception, but its because it comes from opportunity, so it cannot get a subquery for Opportunity->Quote->QuoteLineItem */
        public QuoteDetail(Id paramQuoteId) {
            Id = paramQuoteId;
            Quote paramQuote = [
                SELECT OpportunityId, SAPNumber__c,
                    BillingName, BillingStreet, BillingPostalCode, BillingCity, BillingStateCode, BillingCountryCode, BillingAddress,
                    AdditionalName, AdditionalStreet, AdditionalPostalCode, AdditionalCity, AdditionalStateCode, AdditionalCountryCode, AdditionalAddress,
                    QuoteToName, QuoteToStreet, QuoteToPostalCode, QuoteToCity, QuoteToStateCode, QuoteToCountryCode, QuoteToAddress,
                    ShippingName, ShippingStreet, ShippingPostalCode, ShippingCity, ShippingStateCode, ShippingCountryCode, ShippingAddress,
                    (SELECT PPJSON__c, Product2.SAPNumber__c, Quantity, PPAdditionalInfoJSON__c, Configuration__c, CreatedById FROM QuoteLineItems)
                FROM Quote WHERE Id = :paramQuoteId];
            ProjectId = paramQuote.OpportunityId;
            SapQuoteNumber = paramQuote.SAPNumber__c;

            
            /* Bill to */
            if (paramQuote.BillingCountryCode != null) {
                Address billTo = new Address();
                billTo.AddressType = AddressTypeEnum.BILL_TO;
                billTo.Name = paramQuote.BillingName;
                billTo.Street = paramQuote.BillingStreet;
                billTo.PostalCode = paramQuote.BillingPostalCode;
                billTo.City = paramQuote.BillingCity;
                billTo.StateCode = paramQuote.BillingStateCode;
                billTo.CountryCode = paramQuote.BillingCountryCode;
                Addresses.add(billTo);
            }
            /* Additional To */
            if (paramQuote.AdditionalCountryCode != null) {
                Address additionalTo = new Address();
                additionalTo.AddressType = AddressTypeEnum.ADDITIONAL_TO;
                additionalTo.Name = paramQuote.AdditionalName;
                additionalTo.Street = paramQuote.AdditionalStreet;
                additionalTo.PostalCode = paramQuote.AdditionalPostalCode;
                additionalTo.City = paramQuote.AdditionalCity;
                additionalTo.StateCode = paramQuote.AdditionalStateCode;
                additionalTo.CountryCode = paramQuote.AdditionalCountryCode;
                Addresses.add(additionalTo);
            }
            /* Quote To */
            if (paramQuote.QuoteToCountryCode != null) {
                Address quoteTo = new Address();
                quoteTo.AddressType = AddressTypeEnum.QUOTE_TO;
                quoteTo.Name = paramQuote.QuoteToName;
                quoteTo.Street = paramQuote.QuoteToStreet;
                quoteTo.PostalCode = paramQuote.QuoteToPostalCode;
                quoteTo.City = paramQuote.QuoteToCity;
                quoteTo.StateCode = paramQuote.QuoteToStateCode;
                quoteTo.CountryCode = paramQuote.QuoteToCountryCode;
                Addresses.add(quoteTo);
            }
            /* Ship To */
            if (paramQuote.ShippingCountryCode != null) {
                Address shipTo = new Address();
                shipTo.AddressType = AddressTypeEnum.SHIP_TO;
                shipTo.Name = paramQuote.ShippingName;
                shipTo.Street = paramQuote.ShippingStreet;
                shipTo.PostalCode = paramQuote.ShippingPostalCode;
                shipTo.City = paramQuote.ShippingCity;
                shipTo.StateCode = paramQuote.ShippingStateCode;
                shipTo.CountryCode = paramQuote.ShippingCountryCode;
                Addresses.add(shipTo);
            }

            Products = new List<GenericProduct>();
            for (QuoteLineItem forQLI : paramQuote.QuoteLineItems) {
                Products.add(new GenericProduct(forQLI));
            }
            //CreatedDate =
            //ExpirationDate

        }
    }





    global enum ResponseStatusEnum {
        SUCCESS,
        REQUIRED_FIELDS_MISSING,
        NOT_FOUND,
        PRODUCT_NOT_AVAILABLE,
        NOT_EDITABLE,
        GENERAL_ERROR
    }

    global enum SortOrderEnum { CREATED_DATE_ASC, CREATED_DATE_DESC, MODIFIED_DATE_ASC, MODIFIED_DATE_DESC, PROJECT_NAME_ASC, PROJECT_NAME_DESC }
    global enum OwnerTypeEnum { PARTNER, DORMAKABA }
    global enum ProjectStatusEnum { OPEN, COMPLETED, CANCELLED }
    global enum ProductTypeEnum { STO, CPQ, MKS, IGS }
    global enum AddressTypeEnum { BILL_TO, ADDITIONAL_TO, QUOTE_TO, SHIP_TO }
}