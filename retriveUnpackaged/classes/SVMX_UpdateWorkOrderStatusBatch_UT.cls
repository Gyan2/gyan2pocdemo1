/************************************************************************************************************
Description: Test class for SVMX_UpdateWorkOrderStatusBatch class
 
Author: 
Date: 

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest
public class SVMX_UpdateWorkOrderStatusBatch_UT { 

       static testMethod void testUpdateWorkOrderStatusBatch()
       {
         Account acc = SVMX_TestUtility.CreateAccount('Test account', 'Norway', true);
         Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
         SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('test location', acc.Id, true);
         SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
         Profile prof = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
         User u = SVMX_TestUtility.CreateUser('testTech', prof.Id, false);
         u.UserCountry__c ='US';
         u.SalesOrg__c ='9999';
         insert u;
         SVMX_Custom_Feature_Enabler__c  cfe=SVMX_TestUtility.CreateCustomFeature('Germany',true);

         SVMX_Billing_Rules__c   br=SVMX_TestUtility.CreateBillingRule('Work Order','SVMXC__Order_Status__c','Work Complete','',false);
         br.SVMX_Country__c = 'Germany' ;
           insert br ;
         SVMX_Billing_Rules__c   bWdls=SVMX_TestUtility.CreateBillingRule('Work Details','SVMXC__Actual_Quantity2__c','1','Labor',false);
         bWdls.SVMX_Country__c = 'Germany' ;
           insert bWdls ;
         SVMXC__Service_Group__c servGp = SVMX_TestUtility.CreateServiceGroup('Test Service Group', true);
         //Create product
         Product2 p = SVMX_TestUtility.CreateProduct('Test Product', true);
         Case c = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', true);
         
         //Create technician
         SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Test Tech', u.Id, servGp.id, true);
        
         SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Service Contract', 'Global Std', con.Id, true);
         
         List<SVMXC__PM_Plan__c> pmPlanList = new List<SVMXC__PM_Plan__c>();
         SVMXC__PM_Plan__c pmPlan = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
         pmPlan.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
         pmPlanList.add(pmPlan);
         SVMXC__PM_Plan__c pmPlan1 = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
         pmPlan1.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
         pmPlan1.SVMX_Converted_Plan__c = true;
         pmPlanList.add(pmPlan1);
         insert pmPlanList;
         
         
         List<SVMXC__Service_Order_Line__c> sOrdList = new List<SVMXC__Service_Order_Line__c>();
         List<SVMXC__Service_Order__c> woList = new List<SVMXC__Service_Order__c>();
         SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Work Complete', 'Reactive', 'Global Std', sc.Id, tech.Id, c.Id, false);
         wo.SVMXC__Country__c = 'Germany' ;
         wo.SVMXC__PM_Plan__c = pmPlan.Id;
         woList.add(wo);
         SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id, Loc.Id, p.Id, 'Work Complete', 'Reactive', 'Global Std', sc.Id, tech.Id, c.Id, false);
         wo1.SVMXC__Country__c = 'Germany' ;
         wo1.SVMXC__PM_Plan__c = pmPlan1.Id;
         woList.add(wo1);
         insert woList;
         SVMXC__Service_Order_Line__c oli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, p.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, c.id,'Labor','Warranty',false);
         oli1.SVMXC__Actual_Quantity2__c = 1 ;
         SVMXC__Service_Order_Line__c oli2 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, p.Id, 'Usage/Consumption', wo1.Id, system.now(), sc.Id, tech.Id, c.id,'Labor','Goodwill',false);
         oli2.SVMXC__Actual_Quantity2__c = 1 ;
           sOrdList.add(oli1);  
         sOrdList.add(oli2);  
         insert sOrdList;
         
         Test.startTest();

            SVMX_UpdateWorkOrderStatusBatch obj = new SVMX_UpdateWorkOrderStatusBatch();
            DataBase.executeBatch(obj);

        Test.stopTest();
       }

}