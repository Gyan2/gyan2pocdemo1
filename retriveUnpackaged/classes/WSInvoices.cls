/**
 * Created by rebmangu on 20/04/2018.
 */

global class WSInvoices {

    public class WSInvoicesException extends Exception{}

    global class InvoiceResponse {
        webService Boolean success;
        webService List<InvoiceError> errors;
    }

    global class InvoiceError {
        webService String message;

        public InvoiceError(string message){
            this.message = message;
        }
    }

    global class InvoiceRequest {
        webService List<Invoice> invoices;
    }


    global class Invoice {
        webService String Status;
        webservice Decimal Amount;
        webService String CurrencyIsoCode;
        webService String Name;
        webService String Id;
        webService String CaseId;
        webService String SenderSystem;

        public Datetime CreatedDate;
        public Datetime LastModifiedDate;
        public String ExternalId; // SenderSystem - Id
    }


    webService static InvoiceResponse upsertInvoices(InvoiceRequest invoiceReq){

        Map<String,Map<String,invoice>> mapping = new Map<String,Map<String,invoice>>();
        InvoiceResponse response = new InvoiceResponse();
                        response.success = true;

        try{
            // MAP CASE => Invoice ID => Invoice to store the data temporary and update them.
            for(Invoice invoice : invoiceReq.invoices){
                if(!mapping.containsKey(invoice.CaseId)){
                    mapping.put(invoice.CaseId,new Map<String,Invoice>());
                }
                mapping.get(invoice.CaseId).put(invoice.Id,invoice);
            }

            Map<Id,Case> cases = new Map<Id,Case>([select id,InvoicesJSON__c from Case where Id in: mapping.keySet()]);

            for(Id key : cases.keySet()){
                List<Invoice> invoiceJson = String.isBlank(cases.get(key).InvoicesJSON__c) ? new List<Invoice>(): (List<Invoice>)JSON.deserialize(cases.get(key).InvoicesJSON__c,List<Invoice>.class);

                // Update invoice (remove if updated)
                for(Integer i = 0; i < invoiceJson.size(); i++){
                    String invoiceId = invoiceJson[i].Id;
                    if(mapping.get(key).containsKey(invoiceId)){
                        // Backup of the createdDate
                        Datetime createdDate = invoiceJson[i].CreatedDate;

                        invoiceJson[i] = mapping.get(key).get(invoiceId);
                        invoiceJson[i].LastModifiedDate = System.now();
                        invoiceJson[i].CreatedDate = createdDate;
                        mapping.get(key).remove(invoiceId);
                    }
                }
                // Insert invoice (Take the remaining invoices from the json)
                for(Invoice invoice : mapping.get(key).values()){
                    invoice.LastModifiedDate = System.now();
                    invoice.CreatedDate = System.now();
                    invoiceJson.add(invoice);
                }

                cases.get(key).InvoicesJSON__c = JSON.serialize(invoiceJson);

            }
            System.debug(cases.values());

            update cases.values();
        }catch(Exception e){
            response.success = false;
            response.errors = new List<InvoiceError>{
                    new InvoiceError(e.getLineNumber()+' - '+e.getMessage())
            };
        }





        return response;
    }

}