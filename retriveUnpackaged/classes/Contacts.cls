public with sharing class Contacts extends CoreSObjectDomain{

    public Contacts(List<Contact> sObjectList){
        super(sObjectList);
    }

    public override void onApplyDefaults() {
        ContactServices.applyInternationalPhonePrefixDefaults((List<Contact>)Records);
    }

    public override void onBeforeInsert(){
        ContactServices.splitInternationalPrefix(Records);
    }

    public override void onAfterInsert(){
        ContactServices.updateRelatedAccounts(Records,null);
    }

    public override void onValidate() {
        ContactServices.validateInternationalPhonePrefix((List<Contact>)Records);
    }

    public override void onBeforeUpdate(Map<Id, sObject> ExistingRecords) {
        ContactServices.splitInternationalPrefix(Records);
    }

    public override void onAfterUpdate(Map<Id,sObject> ExistingRecords){
        ContactServices.updateRelatedAccounts(Records, (Map<Id, Contact>)ExistingRecords);
    }

    public override void onAfterDelete(){
        ContactServices.updateRelatedAccounts(Records,null);
    }

    public override void onAfterUndelete(){
        ContactServices.updateRelatedAccounts(Records,null);
    }

    public class Constructor implements CoreSObjectDomain.IConstructable
    {
        public CoreSObjectDomain construct(List<SObject> sObjectList)
        {
            return new Contacts(sObjectList);
        }
    }
}