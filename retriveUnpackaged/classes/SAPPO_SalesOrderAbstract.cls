/**
 * Created by trakers on 12.07.18.
 */

public abstract class SAPPO_SalesOrderAbstract {

    // Constant
    public static Integer SUCCESS = 200;
    public static Integer STANDARD_ERROR = 400;
    public static Integer CURRENCY_ERROR = 401;
    public static Integer EMPTY_ERROR = 402;

    public SAPPO_SalesOrderAbstract.UTILITY Utility;
    public class SalesOrderException extends Exception{}


    public SAPPO_SalesOrderAbstract(){
        this.Utility = new SAPPO_SalesOrderAbstract.UTILITY();
    }


    /*************************************************************************************
     *
     *          WRAPPERS CLASSES (Subject to some modification in the future
     *
     *************************************************************************************/



    // todo: replace SalesOrderResultWrapper with response
    public class Response {
        @AuraEnabled public Integer status;
        @AuraEnabled public String message;
        @AuraEnabled public List<Map<String,String>> data;
    }


    /*************************************************************************************
    *
    *          METHODS to be overwritten
    *
    *************************************************************************************/

    public abstract void handleExceptions(Object paramResponse,Object paramResult,Exception e);
    public abstract List<String> validateMandatoryFields(Object obj);
    protected abstract Map<String,List<String>> getMandatoryFields();

    /*************************************************************************************
     *
     *          Utility CLASS (Subject to some modification in the future
     *
     *************************************************************************************/

    public class UTILITY {
        public Quote getQuotes(Id quoteId){
            return [
                    select id,Account.SAPNumber__c,ScheduledDate__c,Account.CurrencyIsoCode,ExternalQuoteNumber__c,GLN__c,SalesOrg__c,
                    (select id,Quantity,Product2.QuantityUnitOfMeasure,Product2.SAPNumber__c,Product2.ExternalId__c,CurrencyIsoCode from QuoteLineItems)
                    from Quote where Id =: quoteId
            ];
        }

        public Boolean getValue(Map<String,Object> obj, String fieldsToCheck){
            List<String> fields = new List<String>();

            if(fieldsToCheck.split('\\.').size() > 0){
                fields.addAll(fieldsToCheck.split('\\.'));
            }else{
                fields.add(fieldsToCheck);
            }

            String field = fields.remove(0);

            if(fields.size() == 0){
                return (obj.containsKey(field) && obj.get(field) != null);
            }else{
                if((obj.containsKey(field) && obj.get(field) != null)){
                    return getValue((Map<String,Object>)obj.get(field),String.join(fields,'.'));
                }else{
                    return false;
                }

            }

        }

        public Map<String,List<PricebookEntry>> handleUpsertPricebookEntries(List<PricebookEntry> pricebookEntries){

            Map<String,List<PricebookEntry>> result = new Map<String,List<PricebookEntry>>{
                    'insert' => new List<PricebookEntry>(),
                    'update' => new List<PricebookEntry>()
            };

            Map<String,PricebookEntry> pricebookEntryMap = new Map<String,PricebookEntry>();

            for(PricebookEntry pbe : pricebookEntries){
                pricebookEntryMap.put(pbe.ExternalId__c,pbe);
            }

            // UPDATE
            for(PricebookEntry pbe : [select id,ExternalId__c from PricebookEntry where ExternalId__c in : pricebookEntryMap.keySet()]){
                if(pricebookEntryMap.containsKey(pbe.ExternalId__c)){
                    PricebookEntry pbeOrigin = pricebookEntryMap.get(pbe.ExternalId__c);
                    pbeOrigin.put('Id',pbe.id);
                    result.get('update').add(pbeOrigin);

                    pricebookEntryMap.remove(pbe.ExternalId__c);
                }
            }

            // INSERT
            result.get('insert').addAll(pricebookEntryMap.values());

            return result;

        }
        public String getStandardPriceBookId(){
            if(Test.isRunningTest()){
                return Test.getStandardPricebookId();
            }else{
                return [select id from Pricebook2 where IsStandard = true limit 1].Id;
            }
        }
    }

}