@isTest
global class SAPPurchaseRequestUtilityMock implements WebServiceMock {
   global void doInvoke(
           Object stub,
           Object request,
           Map<String, Object> response,
           String endpoint,
           String soapAction,
           String requestName,
           String responseNS,
           String responseName,
           String responseType) {
        SVMX_soapSforceOutboundPurchase.notificationsResponse_element responseelement = 
            new SVMX_soapSforceOutboundPurchase.notificationsResponse_element();
        responseelement.Ack =true;
        response.put('response_x', responseelement); 
   }
}