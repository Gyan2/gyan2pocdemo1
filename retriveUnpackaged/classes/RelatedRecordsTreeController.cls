public class RelatedRecordsTreeController {
    
    @AuraEnabled
    public static Map<String,Object> getsObject(String paramId, Map<String,List<String>> paramFields, Map<String,Map<String,String>> paramRelated) {
        paramFields = (Map<String,List<String>>)JSON.deserialize(JSON.serialize(paramFields),Map<String,List<String>>.class);
        paramRelated = (Map<String,Map<String,String>>)JSON.deserialize(JSON.serialize(paramRelated),Map<String,Map<String,String>>.class);
        
        String sObjectName = String.valueOf(Id.valueOf(paramId).getSObjectType());
        
        
        List<String> queryFields = new List<String>{'Id','Name'};
        if (paramFields.get(sObjectName) != null) {
            queryFields.addAll(paramFields.get(sObjectName));
        }

        for(String forRelatedListName : paramRelated.keySet()){
            List<String> subQueryFields = new List<String>{'Id','Name'};
            String relatedFilter = paramRelated.get(forRelatedListName).get('filter');
            List<String> relatedFields = paramFields.get(paramRelated.get(forRelatedListName).get('sObjectName'));
            String whereCondition = '';
            if (String.isNotBlank(relatedFilter)) {
                whereCondition = ' WHERE ' + relatedFilter;
            }
            
            if (relatedFields != null) {
                subQueryFields.addAll(relatedFields);
            }
            
        	queryFields.add('(SELECT ' + String.join(subQueryFields, ',') + ' FROM ' + forRelatedListName + whereCondition + ')');        
        }
    	String query = String.format('SELECT {0} FROM {1} WHERE Id = \'\'' + paramId + '\'\'',new List<String>{String.join(queryFields,','),sObjectName});
        System.debug(query);
        sObject returnedSobject = database.query(query);
        return (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(returnedSObject));
    }
    
}