@isTest
public class SVMX_SerContractProductsTrigHandler_UT{

     static testMethod void invokeSerContrProductsTrigHandler() {

        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'ServiceContract';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
              
        SVMX_SAP_Integration_UserPass__c cs1 = new SVMX_SAP_Integration_UserPass__c();
              cs1.name ='SAP PO';
              cs1.WS_USER__c ='test';
              cs1.WS_PASS__c ='test';
        insert cs1;

       SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Service Contract Product';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Start_Date__c';
        insert trigconfig2;

      Account acc = SVMX_TestUtility.CreateAccount('Norway Account','Norway',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Norway Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
      
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
           
        Case cs = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', true);
       
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, false);
        sc.SVMXC__Start_Date__c = Date.newInstance(2018, 1, 1);
        sc.SVMXC__End_Date__c = Date.newInstance(2019, 12, 10);
        insert sc;

        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        SVMX_Rate_Tiers__c rt2 = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, true, true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Germany',true);
        
        //SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Reactive','Global Std', sc.Id, tech.Id, cs.id, true);
        Product2 exProd = new Product2(Name = 'Expense Prod', SAPNumber__c='1287236');
        Product2 LoProd = new Product2(Name = 'Labor Prod', SAPNumber__c='1287237');
        Product2 TravProd = new Product2(Name = 'Travel Prod', SAPNumber__c='1287238');
        
         List<product2> prods = new List<Product2>();
         prods.add(exProd);
         prods.add(LoProd);
         prods.add(TravProd);
         
         SVMX_Sales_Office__c so = SVMX_TestUtility.createSalesOffice('Norway', exProd.id, LoProd.id, TravProd.id, true );
         
        Date startDate=system.today();
        Date startDateSec;
        if(system.today().day() > 15){
         startDate=system.today().addDays(15);
        }else{
         startDateSec=Date.newInstance(2018, 12, 9);//system.today().addDays(-2);
        }
        SVMXC__Service_Contract_Products__c serContrProduct=SVMX_TestUtility.CreateServiceContractProduct(sc.Id,'Monthly on First of Month',startDate,true);
        serContrProduct.SVMXC__Start_Date__c=startDate;
        
        SVMXC__Service_Contract_Products__c serContrProductthree=SVMX_TestUtility.CreateServiceContractProduct(sc.Id,'Monthly on First of Month',system.today(),true);
       // serContrProductthree.SVMXC__Start_Date__c=startDateSec;
        
        SVMXC__Service_Contract_Products__c serContrProductSec=SVMX_TestUtility.CreateServiceContractProduct(sc.Id,'Yearly on Last of Start Month',startDateSec,true);
        serContrProductSec.SVMXC__Start_Date__c=startDateSec;
        //CreateServiceContract
        Test.startTest();

        Test.setMock(WebServiceMock.class, new SAPServiceContractUtilityMock());
        SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader messageheader = new SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract serviceContractUpdatereq = new SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract();
        SVMX_wwwDormakabaComContract.ServiceContractOutPort requstelemnt = new SVMX_wwwDormakabaComContract.ServiceContractOutPort();
        requstelemnt.UpdateSync(messageheader,serviceContractUpdatereq);
       
        update serContrProduct;
        update serContrProductSec;
        //update serContrProductthree;
    
        // System.assertEquals(,);
        Test.stopTest();
      }


}