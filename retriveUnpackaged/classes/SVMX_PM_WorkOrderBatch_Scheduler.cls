global class SVMX_PM_WorkOrderBatch_Scheduler implements Schedulable {
    // Execute at regular intervals
    global void execute(SchedulableContext sc){
      SVMX_PM_WorkOrderBatch pmwBatch = new SVMX_PM_WorkOrderBatch();
      Database.executebatch(pmwBatch, 20);
    }
}