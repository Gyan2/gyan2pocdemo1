// @description WSDL representation of SAP PO upload payload 
// @copyright Parx
// @author MHA
// @lastmodify 24.04.2017
// 
public without sharing class SapPoXML {
    
    public static String endpoint = 'callout:SAP_PO';
    public static DOM.Document document;
    
	public static HttpResponse sendRequest(Data data)
    {          
        if(document == null) createDocument(data);
        // Send the request
        HttpRequest req = new HttpRequest();
        req.setMethod('POST');
        req.setEndpoint(endpoint);
        req.setHeader('Content-Type', 'application/soap+xml');
        
        req.setBodyDocument(document);
        
        Http http = new Http();
        HttpResponse res;
        if(!Test.isRunningTest())
            {
                res = http.send(req);
            }
        return res;
    }
    
    public static DOM.Document createDocument(Data data)
    {
        // Create the request envelope
        DOM.Document doc = new DOM.Document();
        
        String soapNS = 'http://schemas.xmlsoap.org/soap/envelope/';
        String ajil = 'ajil';
        String serviceNS = 'http://www.dormakaba.com/e-connect/bc/salesforce/ajila';
        
        dom.XmlNode envelope
            = doc.createRootElement('Envelope', soapNS, 'soapenv');
        envelope.setNamespace('ajil', 'http://www.dormakaba.com/e-connect/bc/salesforce/ajila');
        
        dom.XmlNode header
            = envelope.addChildElement('Header', soapNS, 'soapenv');
        
        dom.XmlNode body
            = envelope.addChildElement('Body', soapNS, 'soapenv');
        
        dom.XmlNode form1 
            =  body.addChildElement('form1', serviceNS, ajil);
        
        dom.XmlNode titlePage 
            =  form1.addChildElement('TitlePage', serviceNS, ajil);
        
        createContent(titlePage,serviceNS,ajil,data); 
        
        System.debug(doc.toXmlString());
            
        if(document == null) document = doc;
        
        return doc;
    }
    
    //creates ordered xml elements from field_order_type_info and an object
    private static void createContent(Dom.XmlNode parent,String namespace, String prefix, 
                                      Object o)
    {        
        String oString = Json.serialize(o);
        Map<String, Object> nameValueMap = (Map<String, Object>) Json.deserializeUntyped(oString); 
        Dom.XmlNode tempNode;
        String key;
        List<Object> field_order_type_info = (List<Object>) nameValueMap.get('field_order_type_info');
            
        for(Object oKey : field_order_type_info)
        {
            key = String.valueOf(oKey);
            tempNode = parent.addChildElement(key, namespace, prefix);
            if(nameValueMap.get(key) != null)
            {                
            	tempNode.addTextNode(String.valueOf(nameValueMap.get(key)));
            }
        }
    }
     public class Data {
        public String objectName;
        public String projectName;
        public String offer_No;
        public String offer_Date;
        public String companyName;
        public String companyAdress;
        public String companyZip;
        public String companyCity;
        public String contactSalutation;
        public String contactPrename;
        public String contactName;
        public String contactPhone;
        public String contactFax;
        public String contactEmail;
        public String sapOrderNo;
        public String sapBillingNo;
        public String kabaContact1_Firstname;
        public String kabaContact1_Name;
        public String kabaContact1_Function;
        public String kabaContact1_Phone;
        public String kabaContact1_Fax;
        public String kabaContact1_Email;
        public String kabaContact2_Firstname;
        public String kabaContact2_Name;
        public String kabaContact2_Function;
        public String kabaContact2_Phone;
        public String kabaContact2_Fax;
        public String kabaContact2_Email;
        public String kabaContact3_Firstname;
        public String kabaContact3_Name;
        public String kabaContact3_Function;
        public String kabaContact3_Phone;
        public String kabaContact3_Fax;
        public String kabaContact3_Email;
        public String kabaContact5_Firstname;
        public String kabaContact5_Name;
        public String kabaContact5_Function;
        public String kabaContact5_Company;
        public String kabaContact5_PlzCity;
        public String kabaContact5_Street;
        public String kabaContact5_Phone;
        public String kabaContact5_Fax;
        public String kabaContact5_Email;
        public String kabaContact6_Firstname;
        public String kabaContact6_Name;
        public String kabaContact6_Function;
        public String kabaContact6_Company;
        public String kabaContact6_PlzCity;
        public String kabaContact6_Street;
        public String kabaContact6_Phone;
        public String kabaContact6_Fax;
        public String kabaContact6_Email;
        
        private String[] field_order_type_info = new String[]{'objectName','projectName','offer_No','offer_Date','companyName','companyAdress','companyZip','companyCity','contactSalutation','contactPrename','contactName','contactPhone','contactFax','contactEmail','sapOrderNo','sapBillingNo','kabaContact1_Firstname','kabaContact1_Name','kabaContact1_Function','kabaContact1_Phone','kabaContact1_Fax','kabaContact1_Email','kabaContact2_Firstname','kabaContact2_Name','kabaContact2_Function','kabaContact2_Phone','kabaContact2_Fax','kabaContact2_Email','kabaContact3_Firstname','kabaContact3_Name','kabaContact3_Function','kabaContact3_Phone','kabaContact3_Fax','kabaContact3_Email','kabaContact5_Firstname','kabaContact5_Name','kabaContact5_Function','kabaContact5_Company','kabaContact5_PlzCity','kabaContact5_Street','kabaContact5_Phone','kabaContact5_Fax','kabaContact5_Email','kabaContact6_Firstname','kabaContact6_Name','kabaContact6_Function','kabaContact6_Company','kabaContact6_PlzCity','kabaContact6_Street','kabaContact6_Phone','kabaContact6_Fax','kabaContact6_Email'};

     }
}