/*******************************************************************************************************
Description: Trigger handler for Work Order Trigger

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger: SVMX_WorkOrderTrggr.cls
    Service Manager: SVMX_WorkOrderServiceManager
    Test class: SVMX_WorkOrderTrggrHandler_UT.cls
    
 
Author: Ranjitha S
Date: 16-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
13 Jan 2018     Pooja Singh     Added setWorkOrderCurrencyFromAcc method. 
                                The logic inside sets the work order currency to account's currency.
19 Jan 2018     Pooja Singh     Added beforeUpdate method. The logic inside creates tasks for Pm type of work orders.
28 Jun 2018     Keshava Prasad  Added logic inside afterInsert to create entitlement history and Case.
*******************************************************************************************************/

public with sharing class SVMX_WorkOrderTrggrHandler extends SVMX_TriggerHandler {

    private list<SVMXC__Service_Order__c> newWOList;
    private list<SVMXC__Service_Order__c> oldWOList;
    private Map<Id, SVMXC__Service_Order__c> newWOMap;
    private Map<Id, SVMXC__Service_Order__c> oldWOMap;
     
    public SVMX_WorkOrderTrggrHandler() {
        this.newWOList = (list<SVMXC__Service_Order__c>) Trigger.new;
        this.oldWOList = (list<SVMXC__Service_Order__c>) Trigger.old;
        this.newWOMap = (Map<Id, SVMXC__Service_Order__c>) Trigger.newMap;
        this.oldWOMap = (Map<Id, SVMXC__Service_Order__c>) Trigger.oldMap;
    }

    public override void beforeInsert(){
        set<Id> caseIds = new set<Id>();
        List<SVMXC__Service_Order__c> woList = new List<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> createcaseList = new List<SVMXC__Service_Order__c>();
        Id GlobalstdRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Global_Std','SVMXC__Service_Order__c');
        for(SVMXC__Service_Order__c wo : newWOList ){
            if(wo.SVMXC__Case__c != null && wo.SVMXC__Order_Type__c == System.Label.Reactive && !wo.SVMX_Out_of_hours__c){
                caseIds.add(wo.SVMXC__Case__c);
                woList.add(wo);
            }
            
            wo.SVMX_PS_Contact_Email__c = wo.SVMX_Copy_Contact_Email__c;
            wo.SVMX_PS_Contact_Phone__c = wo.SVMX_Copy_Contact_Phone__c;


            if(wo.SVMXC__Order_Type__c == System.Label.Planned_Services_PPM && wo.RecordTypeId == GlobalstdRecordTypeId && wo.SVMXC__Purpose_of_Visit__c == 'Technician Onsite' && wo.SVMX_Create_PM_From_SFM__c == true)
                createcaseList.add(wo);

        }



        if( caseIds.size() >0 ){

            Map<Id,case> casesList = SVMX_WorkOrderDataManager.checkCase(caseIds);
            if(casesList.size() > 0)
                SVMX_WorkOrderServiceManager.updateActiveWarranty(woList,casesList);
         
        }
        
        /* 2018-04-20  Yan Anderson  Set the work order billing type to 'Free Of Charge' 
                    if work order type is installation, ppm, quoted works */
        SVMX_WorkOrderServiceManager.updateBillingType(newWOList);

        If(createcaseList.Size() > 0)
            SVMX_WorkOrderServiceManager.createcase(createcaseList);
    }
    
    public override void afterInsert(){
        list<SVMXC__Service_Order__c> woList = new list<SVMXC__Service_Order__c>();
        Set<String> countrySet = new Set<String>();
        list<Id> nonContractWOList = new list<Id>();
        map<id,SVMXC__Service_Order__c> UpList = new map<id,SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order_Line__c> wdInsList = new List<SVMXC__Service_Order_Line__c> ();
        List<SVMXC__Service_Order_Line__c> wdInsList1 = new List<SVMXC__Service_Order_Line__c> ();
        List<SVMXC__Service_Order_Line__c> wdInsList2 = new List<SVMXC__Service_Order_Line__c> ();
        list<SVMXC__Service_Order__c> newUpList = new list<SVMXC__Service_Order__c>();
        List<Case> caseList = new List<Case>() ;
        List<id> priorityWOList = new List<id>();
        List<Id> TravelWOList = new List<Id>();
        Id GlobalstdRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Global_Std','SVMXC__Service_Order__c');
        Boolean AfterInsertFlag = false;
        String countrytravel = null;
        String currecyWO = null;
        string language = null;

        /*//Active Warranty
        for(SVMXC__Service_Order__c wo : newWOList ){
            if(wo.SVMXC__Case__c != null && wo.SVMXC__Order_Type__c == System.Label.Reactive && !wo.SVMX_Out_of_hours__c){
                woList.add(wo);
                AfterInsertFlag = true;
            }
        }*/
        
       

        //Default Contract Check
        for(SVMXC__Service_Order__c wo : newWOList){
            if(wo.SVMXC__Auto_Entitlement_Status__c == 'Failed' && wo.SVMXC__Order_Status__c == 'Open' && wo.SVMXC__Country__c != null  && wo.SVMXC__Order_Type__c != System.Label.Planned_Services_PPM){
                nonContractWOList.add(wo.id);
                countrySet.add(wo.SVMXC__Country__c);
                AfterInsertFlag =true;
            }
        }
        
        //Priority Surcharge check
        for(SVMXC__Service_Order__c wo : newWOList){
            if(wo.RecordTypeId == GlobalstdRecordTypeId && !wo.SVMX_Out_of_hours__c && wo.SVMXC__Order_Type__c == System.Label.Reactive && (wo.SVMXC__Service_Contract__c != null || wo.SVMXC__Service_Contract__c != ' ')){
                priorityWOList.add(wo.id);
                AfterInsertFlag = true;
            }
        }
        
        //Travel Line for Norway
        list<SVMX_Custom_Feature_Enabler__c> customSett = SVMX_Custom_Feature_Enabler__c.getall().values();
        set<string> country = new set<string>();
        for(SVMX_Custom_Feature_Enabler__c cs: customSett){
            if(cs.SVMX_Travel_Activity_Types__c)
                country.add(cs.name);
        }
        
        for(SVMXC__Service_Order__c wo: newWOList){
            if(wo.RecordTypeId == GlobalstdRecordTypeId && country.contains(wo.SVMXC__Country__c) && wo.SVMXC__Order_Type__c == System.Label.Reactive){
                TravelWOList.add(wo.id);
                countrytravel = wo.SVMXC__Country__c;
                currecyWO = wo.CurrencyIsoCode;
                language = wo.SVMX_Language__c;
                AfterInsertFlag = true;
            }
        }

        if(AfterInsertFlag){
            //QUERY WORK ORDERS
            SVMX_WorkOrderServiceManager.getWorkOrders(newWOList);

            /*if(!woList.isEmpty())
                UpList = SVMX_WorkOrderServiceManager.updateActiveWarranty(woList);*/
                
            if(!nonContractWOList.isEmpty())
                SVMX_WorkOrderServiceManager.nonContractMethod(nonContractWOList,countrySet);
            
            if(!priorityWOList.isEmpty()){
                if(system.isFuture() || system.isBatch() || system.isScheduled())
                    SVMX_WorkOrderServiceManager.addPrioritySurcharge(priorityWOList);
                else
                    SVMX_WorkOrderServiceManager.addPrioritySurchargefuture(priorityWOList);
            }
               // wdInsList1 = SVMX_WorkOrderServiceManager.addPrioritySurcharge(priorityWOList);
               

            if(!TravelWOList.isEmpty()){
                if(system.isFuture() || system.isBatch() || system.isScheduled())
                    SVMX_WorkOrderServiceManager.addTravelLine(TravelWOList,countrytravel,currecyWO, language);
                else
                    SVMX_WorkOrderServiceManager.addTravelLinefuture(TravelWOList,countrytravel,currecyWO, language);
                 
            }
            
           // wdInsList.addAll(wdInsList1);
            //wdInsList.addAll(wdInsList2);
        }
        List<SVMXC__Service_Order__c> historyCreateList = new List<SVMXC__Service_Order__c>() ;
        for(SVMXC__Service_Order__c wo : newWOList) {
            if(wo.SVMXC__Order_Type__c == System.Label.Planned_Services_PPM && wo.RecordTypeId == GlobalstdRecordTypeId && wo.SVMX_Create_PM_From_SFM__c == true ) {
                    historyCreateList.add(wo) ;

            }        
        }

        if(!historyCreateList.isEmpty()) {
            SVMX_WorkOrderServiceManager.createEntitleHistory(historyCreateList) ;
        }

       
       
        
      /*  if(UpList.size() > 0)
            SVMX_WorkOrderServiceManager.UpdateWorkOrders(UpList.values()); 
        
        if(!wdInsList.isEmpty())
            SVMX_WorkDetailDataManager.InsWorkDetails(wdInsList); */
    } 
  
    public override void beforeUpdate(){
        Id GlobalstdRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Global_Std','SVMXC__Service_Order__c');
        List<SVMXC__Service_Order__c> woCurrList = new List<SVMXC__Service_Order__c>();
        
        for(SVMXC__Service_Order__c wo : newWOList){
             system.debug('active before update '+wo.SVMX_Active_Warranty__c);
            if(wo.RecordTypeId == GlobalstdRecordTypeId && wo.SVMXC__Company__c != null && wo.CurrencyIsoCode != wo.SVMX_Account_Currency__c){
                wo.CurrencyIsoCode = wo.SVMX_Account_Currency__c;
            }
        }     
    }
    
    public override void afterUpdate(){  
        
        List<case> caseList = new List<case>();
        List<SVMXC__Service_Order__c> woList = new list<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> ppmWOList = new List<SVMXC__Service_Order__c>();
       // List<SVMXC__Service_Order__c> workCompleteWOList = new List<SVMXC__Service_Order__c>();
        List<SVMXC__Service_Order__c> Caseclosure = new List<SVMXC__Service_Order__c>();
        Map<id,SVMXC__Service_Order__c> woIntegrationMap = new Map<id,SVMXC__Service_Order__c>();
        Map<id,SVMXC__Service_Order__c> woSalesOrderMap = new Map<id,SVMXC__Service_Order__c>();
        list<SVMXC__RMA_Shipment_Order__c> partsorderList = new List<SVMXC__RMA_Shipment_Order__c>();
        list<SVMXC__RMA_Shipment_Line__c> partsorderLineList = new List<SVMXC__RMA_Shipment_Line__c>();
        set<Id> workorderIdset= new set<Id>();
        set<Id> workorderIdsetForclosingpartsorderLines= new set<Id>();
        set<Id> partsorderLineIdset= new set<Id>();
        set<id> CanceledWOIdset = new set<id>();
        List<SVMXC__Service_Order_Line__c> wdupdateList = new list<SVMXC__Service_Order_Line__c>();
        
        for(SVMXC__Service_Order__c wo : newWOList){
            if((wo.SVMXC__Order_Status__c  == system.label.Ready_To_Bill || wo.SVMXC__Order_Status__c == system.label.Canceled) && !wo.SVMX_Revisit_Required__c && wo.SVMXC__Order_Status__c != oldWOMap.get(wo.id).SVMXC__Order_Status__c){
                woList.add(wo);
                //AfterUpdateFlag = true;
            }
            if(wo.SVMXC__Group_Member__c != null && oldWOMap.get(wo.id).SVMXC__Group_Member__c != wo.SVMXC__Group_Member__c && wo.SVMXC__Order_Type__c == System.Label.Planned_Services_PPM && wo.SVMX_Task_Present__c == true && wo.SVMX_PM_Schedule_Definition__c != null && SVMX_DefinitionClass.stopTaskRecursion == false){ 
                ppmWOList.add(wo);
                //AfterUpdateFlag = true;
            }

            if((wo.SVMXC__Order_Type__c == System.Label.Planned_Services_PPM || wo.SVMXC__Order_Type__c == System.Label.Reactive
                || wo.SVMXC__Order_Type__c == System.Label.Quoted_Works | wo.SVMXC__Order_Type__c == System.Label.Installation) && wo.SVMX_Count_Of_childRecords__c > 0) {

                woIntegrationMap.put(wo.Id,wo);
            }
            
            if(wo.SVMXC__Order_Status__c == system.label.Work_Complete 
                && wo.SVMXC__Order_Status__c != oldWOMap.get(wo.id).SVMXC__Order_Status__c) {
                
                //workCompleteWOList.add(wo);
                woSalesOrderMap.put(wo.Id, wo); 
            }

            if(wo.SVMXC__Order_Status__c == System.label.Closed && wo.SVMXC__Order_Status__c != oldWOMap.get(wo.id).SVMXC__Order_Status__c){
                Caseclosure.add(wo);
         
            }
            
            //service salesorder number check for service report generation to sap
            if(wo.SVMX_Service_Sales_Order_Number__c != null && wo.SVMX_Service_Sales_Order_Number__c != oldWOMap.get(wo.Id).SVMX_Service_Sales_Order_Number__c){

                workorderIdset.add(wo.Id);
            }

            if(wo.SVMXC__Order_Status__c  == system.label.Ready_To_Bill && wo.SVMXC__Order_Status__c != oldWOMap.get(wo.id).SVMXC__Order_Status__c){
                
                workorderIdsetForclosingpartsorderLines.add(wo.Id);          
            } 
            //work order status is closed of type spare parts Order and Quote Request
            if(wo.SVMXC__Order_Status__c == System.label.Closed && wo.SVMXC__Order_Status__c != oldWOMap.get(wo.id).SVMXC__Order_Status__c ){
                if(wo.SVMXC__Order_Type__c == 'Spare Parts Order' || wo.SVMXC__Order_Type__c == 'Quote Request'){

                    workorderIdsetForclosingpartsorderLines.add(wo.Id); 
                }
            }

            if(wo.SVMXC__Order_Status__c == System.label.Canceled && wo.SVMXC__Order_Status__c != oldWOMap.get(wo.id).SVMXC__Order_Status__c ){

                CanceledWOIdset.add(wo.Id);
            }  

        }
        
        if(!woList.isEmpty()) {
            caseList = SVMX_WorkOrderServiceManager.caseReadytoInvoice(woList);
        }
        if(!Caseclosure.isEmpty()) {
            caseList = SVMX_WorkOrderServiceManager.caseReadytoClose(Caseclosure);
        }
        if(!caseList.isEmpty()) {
            SVMX_CaseDataManager.UpdateCase(caseList);
        }
        if(!ppmWOList.isEmpty()) {
            SVMX_WorkOrderServiceManager.createTaskForPPMWorkOrdersOnAssignment(ppmWOList);
        }
        if(!workorderIdsetForclosingpartsorderLines.isEmpty()) {
            partsorderList = SVMX_PartsOrderDataManager.workorderparOrdQuery(workorderIdsetForclosingpartsorderLines);
        }
        if(partsorderList.size()>0){
            for(SVMXC__RMA_Shipment_Order__c partsOrder:partsorderList){
                    partsOrder.SVMXC__Order_Status__c ='Closed';
                    partsorderLineIdset.add(partsOrder.id);
            }
        }

        if(!partsorderLineIdset.isEmpty()) {
            partsorderLineList = SVMX_PartsOrderLineDataManager.PartOrdIdParOrdLineQuery(partsorderLineIdset);
        }
        if(partsorderLineList.size()>0){
            for(SVMXC__RMA_Shipment_Line__c partsOrderLine:partsorderLineList){
                    
                    partsOrderLine.SVMXC__Line_Status__c ='Completed';
            }
        }        
        
        if(!woIntegrationMap.isEmpty()) {
            SVMX_SAPSalesOrderUtility.handleSAPSalesOrderHeaderUpdate(woIntegrationMap,oldWOMap);
        }

        if(!woSalesOrderMap.isEmpty()) {
            SVMX_SAPSalesOrderUtility.handleWorkOrderCompleteForSalesOrderUpdate(woSalesOrderMap,oldWOMap);    
        }
        
      /*  if(!workCompleteWOList.isEmpty()) {
            SVMX_ProductStockServiceManager.updateProductStock(workCompleteWOList,null); 
        } */

        if(workorderIdset.size()>0) {
            SVMX_SAPServiceReportUtility.handleServiceReportCreationOnworkOrderupdate(workorderIdset);
        }
        if(partsorderList.size()>0) {
            SVMX_PartsOrderDataManager.updatepartsOrder(partsorderList);
        }

        if(partsorderLineList.size()>0) {
            SVMX_PartsOrderLineDataManager.updatepartsOrderline(partsorderLineList);
        }

        if(CanceledWOIdset.size()>0){
            wdupdateList = SVMX_WorkDetailDataManager.ToCanceledWorkDetailLines(CanceledWOIdset);
        }

        if(wdupdateList.size()>0){
            
            for(SVMXC__Service_Order_Line__c wd:wdupdateList){
                wd.SVMX_SAP_Status__c = 'Rejected';
            }
            SVMX_WorkDetailDataManager.UpdaWorkDetails(wdupdateList);
        }
    }
 
}