/* 
    Author : Keshava Prasad
    Date   : 16/04/2018 
*/   


@isTest
public class SVMX_LocationDataManager_UT {

  public static testMethod void insertAccId() {
    Set<Id> accIdSet = new Set<Id>() ;
    for(Integer i=0;i<3;i++) {
        Account account = SVMX_TestUtility.CreateAccount('Atom Account'+i,'India', true) ;
        accIdSet.add(account.id) ;
        }
        SVMX_LocationDataManager.accountsLocationQuery(accIdSet) ;
  }

  public static testMethod void insertLocationId() {
    Set<Id> locIdSet = new Set<Id>() ;
    for(Integer i=0; i<3 ;i++) {
      Account acc = SVMX_TestUtility.CreateAccount('Test'+i,'Germany',true) ;
      SVMXC__Site__c locations = SVMX_TestUtility.CreateLocation('TestLocation'+i,acc.id,true) ; 
      locIdSet.add(locations.id) ;
      }  
      SVMX_LocationDataManager.LocationQuery(locIdSet) ;
    }

   /* public static testMethod void insertRelatedAccount() {
        List<SVMX_Related_Account__c> relAccList = new List<SVMX_Related_Account__c>();
        SVMX_Related_Account__c relAcc = new SVMX_Related_Account__c() ;
        //relAcc.SVMX_Date_Ended__c = system.today() ;
        relAccList.add(relAcc) ;
        SVMX_LocationDataManager.inserRelAcc(relAccList) ;
    }

  public static testMethod void updateInstalledProducts() {

    Account account = SVMX_TestUtility.CreateAccount('Testing Account','India', true) ;
    SVMXC__Product__c product = new SVMXC__Product__c(Name='TestProd', CurrencyIsoCode =) ;
    SVMXC__Installed_Product__c installedProduct = SVMX_TestUtility.CreateInstalledProduct(account.id,) ;
    
  
  }*/
  
}