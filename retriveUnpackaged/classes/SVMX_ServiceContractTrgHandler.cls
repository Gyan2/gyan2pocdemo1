/************************************************************************************************************
Description: Trigger handler class for SVMX_ServiceContractTrg trigger

Dependency: SVMX_ServiceContractDataManager
 
Author: Pooja Singh
Date: 18-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
26-12-2017    Ranjitha        Added before update method to activate PM plans.
****************************************************************************************************************/

public with sharing class SVMX_ServiceContractTrgHandler extends SVMX_TriggerHandler{
    private list<SVMXC__Service_Contract__c > newSCList;
    private list<SVMXC__Service_Contract__c > oldSCList;
    private Map<Id, SVMXC__Service_Contract__c > newSCMap;
    private Map<Id, SVMXC__Service_Contract__c > oldSCMap;
    private Map<String ,String> countriesCode; 
    
    public SVMX_ServiceContractTrgHandler() {
        this.newSCList = (List<SVMXC__Service_Contract__c >) Trigger.new;
        this.oldSCList = (List<SVMXC__Service_Contract__c >) Trigger.old;
        this.newSCMap = (Map<Id, SVMXC__Service_Contract__c >) Trigger.newMap;
        this.oldSCMap = (Map<Id, SVMXC__Service_Contract__c >) Trigger.oldMap;
         
           countriesCode=new Map<String,String>();
           List<SVMX_Custom_Feature_Enabler__c> allCountryWithCode = SVMX_Custom_Feature_Enabler__c.getAll().values();
           for(SVMX_Custom_Feature_Enabler__c  countryCode:allCountryWithCode){
               
               countriesCode.put(countryCode.Name,countryCode.Country_Code__c);
               
           }
    }
    
    
    public override void beforeUpdate(){
        
        for(SVMXC__Service_Contract__c contract:newSCList){
            //Check if the contract is Actiavted and Renewed through Job is True
            if(oldSCMap!=null && oldSCMap.get(contract.Id).SVMXC__Active__c!=contract.SVMXC__Active__c && contract.SVMXC__Active__c && contract.SVMX_Renewed_Through_Job__c){
                
                contract.SVMXC__Start_Date__c = contract.SVMXC__Start_Date__c + 1;
                contract.SVMX_Activation_Date__c = system.now();
               // contract.SVMXC__End_Date__c = contract.SVMXC__Start_Date__c + 364;
            } 
            
           
            //Code for Contract Number Generation added on 18/01/2017.
            if(oldSCMap!=null && oldSCMap.get(contract.Id).SVMXC__Active__c!=contract.SVMXC__Active__c && contract.SVMXC__Active__c == true && contract.SVMXC__Renewed_From__c == null && contract.SVMX_Country__c!=null ){
                system.debug('in here');
                String randStr=SVMX_ServiceContractServiceManager.generateRandamNumber();
                String countrycode=countriesCode.get(contract.SVMX_Country__c);
                contract.SVMX_GlobalContract_Number__c =countrycode+'-'+randStr;
                
            }
        }
    }
    
    public override void afterUpdate(){
        List<Opportunity> oppUpList = new List<Opportunity>();
        List<SVMXC__Service_Contract__c> scList = new List<SVMXC__Service_Contract__c>();
        Map<Id,SVMXC__Service_Contract__c> scIntMap = new Map<Id,SVMXC__Service_Contract__c>();
        Map<Id,SVMXC__Service_Contract__c> scIntUpdateMap = new Map<Id,SVMXC__Service_Contract__c>();
        //set<Id> awaitingrespnsecontarctList = new set<Id>();
        
        for(SVMXC__Service_Contract__c sc : newSCList){
        
            if(oldSCMap!=null && oldSCMap.get(sc.Id).SVMXC__Active__c!=sc.SVMXC__Active__c && sc.SVMXC__Active__c && sc.SVMX_Renewed_Through_Job__c){
                
                system.debug('Start Method @@@');
                String contractEndDate = sc.SVMXC__End_Date__c.year()+'-'+sc.SVMXC__End_Date__c.month()+'-'+sc.SVMXC__End_Date__c.day();
                
                //Builing Request to invoke the managed package webservice 
                SVMXC.INTF_WebServicesDef.INTF_TargetRecord targetTemp = new SVMXC.INTF_WebServicesDef.INTF_TargetRecord();
                SVMXC.INTF_WebServicesDef.INTF_StringMap tempStringMap1 = new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMX_recordId',sc.Id);
                SVMXC.INTF_WebServicesDef.INTF_StringMap tempStringMap2 = new SVMXC.INTF_WebServicesDef.INTF_StringMap('Id',sc.Id);
                SVMXC.INTF_WebServicesDef.INTF_StringMap tempStringMap3 = new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__End_Date__c',contractEndDate);
                targetTemp.stringMap.add(tempStringMap1);
                
                system.debug('TempStrin Map @@@'+tempStringMap1);
                targetTemp.headerRecord.objName = 'SVMXC__Service_Contract__c';

                SVMXC.INTF_WebServicesDef.INTF_Record recordstemp = new SVMXC.INTF_WebServicesDef.INTF_Record();
                recordstemp.targetRecordAsKeyValue.add(tempStringMap2);
                recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
                recordstemp.targetRecordId=sc.Id;
                targetTemp.headerRecord.records.add(recordstemp);
                SVMXC.INTF_WebServicesDef.INTF_ActivateContract(targetTemp);
                
                system.debug('Activate Contract @@@');
            }
        
        
            if(sc.SVMX_Contract_Status__c != null && sc.SVMX_Contract_Status__c != oldSCMap.get(sc.Id).SVMX_Contract_Status__c && sc.SVMX_Opportunity__c != null){
               if(sc.SVMX_Contract_Status__c == 'Quote Accepted'){

                    Opportunity opp = new Opportunity(Id= sc.SVMX_Opportunity__c);
                    opp.StageName = 'Closed Won';
                    oppUpList.add(opp);

               }
               if(sc.SVMX_Contract_Status__c == 'Quote Rejected'){
                    Opportunity opp = new Opportunity(Id= sc.SVMX_Opportunity__c);
                    opp.StageName = 'Closed Lost';
                    oppUpList.add(opp);
               }
            }
            
            if((sc.SVMX_Dynamic_PM_Plan__c != oldSCMap.get(sc.Id).SVMX_Dynamic_PM_Plan__c && sc.SVMX_Dynamic_PM_Plan__c && sc.SVMX_Current_Renewal_Year__c != null) || (sc.SVMX_Current_Renewal_Year__c != oldSCMap.get(sc.Id).SVMX_Current_Renewal_Year__c && sc.SVMX_Dynamic_PM_Plan__c)){
                sclist.add(sc);
            }

           /* if(sc.SVMXC__Active__c == true && sc.SVMX_Awaiting_SAP_Response__c == true && sc.SVMX_Awaiting_Check__c == true && sc.SVMX_Awaiting_Check__c != oldSCMap.get(sc.Id).SVMX_Awaiting_Check__c){
                    awaitingrespnsecontarctList.add(sc.ID);
            }*/
        }
        
        if(!sclist.isEmpty())
            SVMX_ServiceContractServiceManager.dynamicPMPlan(sclist);

        if(oppUpList.size() > 0)
            SVMX_OpportunityDataManager.updateOpp(oppUpList);

        //To check Active ServiceContracts and Pricing methods for Timebased and both

        for(SVMXC__Service_Contract__c servcContract : newSCMap.values()){
        
            system.debug('servicecont-->'+servcContract);

            if((servcContract.SVMX_Pricing_Method__c == System.Label.Time_Based || servcContract.SVMX_Pricing_Method__c ==  System.Label.Both) && servcContract.SVMXC__Active__c == true && servcContract.SVMXC__Active__c != oldSCMap.get(servcContract.id).SVMXC__Active__c && servcContract.SVMX_No_of_IsBillable_Line_Items__c >0){
                    
                    system.debug('servicecont1-->'+servcContract);
                    scIntMap.put(servcContract.id,servcContract);
                    
            }else if((servcContract.SVMX_Pricing_Method__c == System.Label.Time_Based || servcContract.SVMX_Pricing_Method__c ==  System.Label.Both) && servcContract.SVMX_Pricing_Method__c == oldSCMap.get(servcContract.id).SVMX_Pricing_Method__c && servcContract.SVMXC__Active__c == true && servcContract.SVMXC__Active__c == oldSCMap.get(servcContract.id).SVMXC__Active__c && servcContract.SVMX_No_of_IsBillable_Line_Items__c >0){

                    system.debug('servicecont1-->'+servcContract);
                    scIntUpdateMap.put(servcContract.id,servcContract);
            }
        }

        //call to SVMX_SAPServiceContractUtility 

        if(!scIntMap.isEmpty()) {
          
          SVMX_SAPServiceContractUtility.handleSAPServiceContractCreation(scIntMap,oldSCMap);
        } 

         if(!scIntUpdateMap.isEmpty()) {
          
          SVMX_SAPServiceContractUtility.handleSAPServiceContractupdate(scIntUpdateMap,oldSCMap);
        }

        /*if(!awaitingrespnsecontarctList.isEmpty()){
           SVMX_SAPServiceContractUtility.contractLineItemsawaitingResponseUpdate(awaitingrespnsecontarctList);
        } */     
       
    }
}