/************************************************************************************************************
Description: Controller for SVMX_SendQuoteEmail VF page.

Dependancy: 
          SVMX_SendQuote_DataManager
 
Author: Pooja Singh
Date: 18-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

public  class SVMX_SendQuoteController{

 public List<SVMXC__Service_Contract__c> contractList {get; set;}
        public String contractQuotationNumber{get ; set;}
        public  String contactName { get ; set ;}
        public  String contactEmail { get ; set ;}
        public List<AttachmentWrapper> attachmentWrapperList {get;set;}
        public Boolean isCheckedAll {get ; set ; }  
        public List<String> address =new List<String>();
        public Contact primaryContact = new Contact();
                    
        public SVMX_SendQuoteController()
        {
            isCheckedAll = FALSE;
            contractList = new List<SVMXC__Service_Contract__c>();
            attachmentWrapperList = new List<AttachmentWrapper>();
            contractQuotationNumber = ApexPages.currentPage().getParameters().get('quoteNumber');
               system.debug('contractQuotationNumber '+contractQuotationNumber);
            if(contractQuotationNumber != null)
                fetchQuotes();
        }
        
        public PageReference fetchQuotes()
        {
            Id scRecTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Global_Std_Quote','SVMXC__Service_Contract__c');
            System.debug('contractQuotationNumber-->'+contractQuotationNumber);
            if(contractQuotationNumber != null && contractQuotationNumber != '')
            {
                contractList = SVMX_ServiceContractDataManager.servContractList(scRecTypeId, contractQuotationNumber);
                //system.debug('Contract List '+contractList[0].SVMXC__Contact__r.Email);             
                if(contractList != null && contractList.size() > 0)
                {

                    for(SVMXC__Service_Contract__c contract : contractList)
                    {
                        AttachmentWrapper wrapper = new  AttachmentWrapper(contract);


                        attachmentWrapperList.add(wrapper);
                    }

                    List<Contact> quotecontacts = SVMX_ContactDataManager.getContacts(contractList[0].SVMXC__Company__c);
                    if(quotecontacts.size() > 0)
                        primaryContact = quotecontacts[0];
                    for( Contact ct: quotecontacts){
                        system.debug('ct.SVMX_Roles__c-->'+ct.SVMX_Roles__c);
                        if(ct.SVMX_Roles__c != NULL && ct.SVMX_Roles__c.contains('Quotation Manager')){
                            //system.debug('email '+ct.Email);
                            address.add(ct.Email);
                        }

                    }
                    /*if(contractList[0].SVMXC__Contact__c != null && contractList[0].SVMXC__Contact__r.SVMX_Roles__c == 'Quotation Manager'){
                        contactName = contractList[0].SVMXC__Contact__r.Name;
                        contactEmail = contractList[0].SVMXC__Contact__r.Email;
                    }*/
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.No_Quote_Found + contractQuotationNumber));
                }
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Valid_Quote_Name));
            }

                 return null;
        }
        
        public PageReference sendQuotes()
        {
            if(attachmentWrapperList != null)
            {    
                Set<Id> attachmentIdSet = new Set<Id> ();         
                Id quoteOppId ;

                SVMXC__Service_Contract__c primaryQuote ;

                system.debug('attachmentWrapperList '+attachmentWrapperList);
                for(AttachmentWrapper wrapper : attachmentWrapperList)
                {
                   /* if(wrapper.contract.SVMX_Opportunity__c != null)
                    {
                        quoteOppId = wrapper.contract.SVMX_Opportunity__c;
                        wrapper.isOpportunityAttached = true;
                    }
                    else
                    {
                        wrapper.isOpportunityAttached = false;
                    } */              
                    
                    //if(wrapper.isPrimaryQuote)
                        primaryQuote = wrapper.contract;

                    if(wrapper.isSelected && wrapper.contract.Attachments != null && wrapper.contract.Attachments.size() > 0) 
                    {
                        attachmentIdSet.add(wrapper.contract.Attachments[0].id);
   
                    }
                   /* else if(wrapper.isSelected && wrapper.contract.Attachments != null && wrapper.contract.Attachments.size() == 0)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.No_Quote_Document + wrapper.contract.Name + Label.And_Plan + wrapper.contract.SVMXC__Service_Plan__r.Name));  
                        return null;
                    }*/
                }

                system.debug('primary quote '+primaryQuote);
                

                List<SVMXC__Service_Contract__c> updatedContractList = new List<SVMXC__Service_Contract__c>();
               /* system.debug('quoteOppId--->'+quoteOppId);
                if(quoteOppId != null && attachmentIdSet.size() > 0)
                {
                    for(AttachmentWrapper wrapper : attachmentWrapperList)
                    {
                        if(!wrapper.isOpportunityAttached && wrapper.isSelected) 
                        {
                            wrapper.contract.SVMX_Opportunity__c = quoteOppId; 
                            attachmentToOpp(attachmentIdSet,quoteOppId);                          
                        }
                        if(wrapper.isSelected)
                        {
                            updatedContractList.add(wrapper.contract);
                            attachmentToOpp(attachmentIdSet,quoteOppId);                          
                        }
                        
                    }

                    

                }
                else
                {*/
                    if(primaryQuote != null && attachmentIdSet.size() > 0)
                    {
                       Opportunity newOpp= insertOppContactRole(primaryQuote, attachmentIdSet);
                       System.debug('Opp Id -->'+newOpp);
                                               
                        for(AttachmentWrapper wrapper : attachmentWrapperList)
                        {
                            if(wrapper.isSelected) 
                            {
                                wrapper.contract.SVMX_Opportunity__c = newOpp.id;
                                updatedContractList.add(wrapper.contract);
                            }
                        }
                    }
                    else
                    {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.No_Quote_Attached));                        
                    }
                //}

                if(attachmentIdSet.size() > 0 && contractList.size() > 0)
                {
                    List<Attachment> attachmentList = attachmentListMethod(attachmentIdSet);      
                    

                    System.debug('....... Addresses ' +address);

                    Messaging.SingleEmailMessage QuoteMail = new Messaging.SingleEmailMessage ();
                    QuoteMail.setSaveAsActivity(true);
                    QuoteMail.setToAddresses(address);            
                    QuoteMail.setSenderDisplayName('DORMA');
                    QuoteMail.setSubject('Contract Quote Email');
                    QuoteMail.setPlainTextBody('Contract Quotes');
                    
                
                    Integer noOfAttachments = attachmentList.size();
                    Messaging.EmailFileAttachment[] fileAttachments = new Messaging.EmailFileAttachment[noOfAttachments];
                    Integer i = 0;
                    
                    for(Attachment attachment : attachmentList)
                    {
                        Messaging.EmailFileAttachment fileAttachment = new Messaging.EmailFileAttachment();
                        fileAttachment.setBody(attachment.Body);
                        fileAttachment.ContentType='application/x-pdf';
                        fileAttachment.Inline=true;     
                        fileAttachments[i++] = fileAttachment;
                    }
                    System.debug('....... fileAttachments ' +fileAttachments); 
                    QuoteMail.setFileAttachments(fileAttachments);
                    System.debug('....... QuoteMail ' +QuoteMail);    
                    try{
                         Messaging.sendEmail(new Messaging.SingleEmailMessage[] {QuoteMail});
                            
                            
                        for(SVMXC__Service_Contract__c  contract : updatedContractList)
                        {
                            if(contract.Attachments != null && contract.Attachments.size() > 0)
                            {
                                contract.SVMX_Contract_Status__c = 'Quote Sent';
                            }
                        }
                        System.debug('....... updatedContractList ' +updatedContractList);                        
                        update updatedContractList;
                        contractQuotationNumber = null;
                        contractList = new List<SVMXC__Service_Contract__c  >();      
                    }Catch(exception e)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Email_Error + e));
                    }
                                       
                 }
                 else
                 {
                      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.No_Quote_Document));               
                 }
                 
             }
             
             return null;
        }

        public PageReference checkAllValue()
        {            
            for(AttachmentWrapper wrapper: attachmentWrapperList)
            {
                if(isCheckedAll)
                {
                    if(wrapper.contract.SVMX_Contract_Status__c != 'Quote Sent')
                        wrapper.isSelected  = true;
                }
                else
                    wrapper.isSelected = false;
            }

            return null;
        }
        class AttachmentWrapper {

            public SVMXC__Service_Contract__c contract {get ;set;}
            public boolean isSelected {get ; set;}
            public boolean isOpportunityAttached {get;set;}
            //public boolean isPrimaryQuote {get;set;}

            public AttachmentWrapper (SVMXC__Service_Contract__c contract)
            {
                this.contract = contract;
                isSelected = false;
            }

        }


        public static List<Attachment> attachmentListMethod(Set<Id> attachmentIdSet){
            List<Attachment> attachmentList = [Select Id, Name, Body,Parent.Lastname, Parent.Firstname from Attachment 
                                                Where ID in :attachmentIdSet];

                return attachmentList;
        }


        public static Opportunity insertOppContactRole(SVMXC__Service_Contract__c primaryQuote, Set<Id> attachmentIdSet){

        //Id opprtunityRecTypeId = SVMX_SendQuote_DataManager.recordTypeId('Opportunity');
                        Set<Id> quoteIdSet = new Set<Id>();
                        Opportunity newOpportunity = new Opportunity();
                        
                        newOpportunity.StageName = 'Quotation';
                        newOpportunity.CloseDate = System.today();
                        newOpportunity.Name = primaryQuote.Name;
                        newOpportunity.Type = 'Service';
                        //newOpportunity.RecordTypeId = recordTypeId('Opportunity');
                        newOpportunity.Amount = primaryQuote.SVMXC__Contract_Price2__c;
                        newOpportunity.AccountId = primaryQuote.SVMXC__Company__c;
                        newOpportunity.Type = 'Service Contract';

                        insert newOpportunity;
                        System.debug('newOpportunity Id -->'+newOpportunity); 

                        quoteIdSet.add(primaryQuote.id);
                        attachmentToOpp(attachmentIdSet,primaryQuote.SVMX_Opportunity__c);    



                        /*OpportunityContactRole contactRole = new OpportunityContactRole(OpportunityId = newOpportunity.id,
                                                                                        Role = 'Decider',
                                                                                        ContactId = primaryContact.id,
                                                                                        IsPrimary = true );
                        
                        insert contactRole;*/

                        return newOpportunity;
    }


    public static void attachmentToOpp(Set<Id> attachmentIdSet, Id oppId){

        List<Attachment> insAttachList = new List<Attachment>();
        List<Attachment> attachmentList = attachmentListMethod(attachmentIdSet);
        List<Opportunity> newOpportunityQuery = [Select Id from Opportunity where Id =:oppId];

        for(Opportunity opp : newOpportunityQuery){
            for(Attachment attachment : attachmentList){
                Attachment att = new Attachment(name = attachment.name, body = attachment.body, parentid = oppId);
                    insAttachList.add(att);
            }

            if(insAttachList.size() > 0)
            {
                insert insAttachList;
            }
        }
    }
}