/**
 * Created by rebmangu on 27/04/2018.
 */

@IsTest
public with sharing class TestWSMaterials {

    @testSetup static void setup(){
        insert new ProductHierarchy__c(ExternalId__c='01',Name='Hello the world - 1');
        insert new ProductHierarchy__c(ExternalId__c='0120',Name='Hello the world - 2');
        insert new ProductHierarchy__c(ExternalId__c='0120002',Name='Hello the world - 3');
    }

    public static testMethod void insertProduct(){
        List<String> languages = new List<String>{
                'FR','DE','EN','ES'
        };


        WSMaterials.MaterialRequest request = new WSMaterials.MaterialRequest();
                                    request.materials       = new List<WSMaterials.Material>();
                                    request.SenderSystem    = 'PS8';

        for(Integer i = 0; i < 2; i++){
                WSMaterials.Material material = new WSMaterials.Material();
                    material.baseMeasureUnitCode = 'PCE';
                    material.deletionIndicator = false;
                    material.internalId = '00000000000006900'+i;
                    material.producthierarchy = '0120002';
                    material.salesSpecifications = new List<WSMaterials.SalesSpecifications>();
                    material.descriptions = new List<WSMaterials.Description>();


                for(Integer j = 0; j < 4; j++){
                    WSMaterials.Description description = new WSMaterials.Description();
                        description.languageCode = languages[j];
                        description.value = 'Bla bla in '+languages[j];
                    material.descriptions.add(description);

                    WSMaterials.SalesSpecifications specification = new WSMaterials.SalesSpecifications();
                        specification.salesOrganisationId           = '550'+j;
                        specification.distributenChannelCode        = '5500'+j;
                        specification.salesMeasureUnitCode          = 'PCE';
                        specification.blockedForSalesIndicator      = false;
                        specification.configurableIndicator         = false;
                        specification.makeToOrderIndicator          = false;
                        specification.previousMaterialInternalID    = '1123456479'+j;
                        specification.mrpProfile                    = 'PC16';
                    material.salesSpecifications.add(specification);
                }

            request.materials.add(material);
        }
        WSMaterials.MaterialResponse response = WSMaterials.upsertMaterial(request);
        System.debug(response);
        for(Product2 prod : [select Id,MRPProfile__c,SVMXC__Stockable__c,SAPNumber__c,SVMX_SAP_Product_Type__c,SalesSpecifications__c from Product2 ]){
            for(Object obj : (List<Object>)JSON.deserializeUntyped(prod.SalesSpecifications__c)){
                Map<String,Object> mapping = (Map<String,Object>)obj;
                System.assertEquals('Contract Product',(String)mapping.get('SVMX_SAP_Product_Type__c'));
                System.assertEquals(false,(Boolean)mapping.get('SVMXC__Stockable__c'));
            }
        }

        System.assertEquals(2,[select count() from PricebookEntry where Pricebook2Id =: Test.getStandardPricebookId()]);
        System.assertEquals(2,[select count() from Product2]);

        System.assertEquals(8,[select count() from ProductDetail__c]);
        System.assertEquals(2,[select count() from Product2 where ProductHierarchyRef__r.FamilyRef__r.ExternalId__c = '0120']);
        System.assertEquals('11234564790 11234564791 11234564792 11234564793',[select PreviousExternalId__c from Product2 limit 1].PreviousExternalId__c);
    }

    public static testMethod void upsertPricebooks(){
        List<String> languages = new List<String>{
                'FR','DE','EN','ES'
        };


        WSMaterials.MaterialRequest request = new WSMaterials.MaterialRequest();
        request.materials       = new List<WSMaterials.Material>();
        request.SenderSystem    = 'PS8';

        for(Integer i = 0; i < 2; i++){
            WSMaterials.Material material = new WSMaterials.Material();
            material.baseMeasureUnitCode = 'PCE';
            material.deletionIndicator = false;
            material.internalId = '10000000000000000'+i;
            material.producthierarchy = '0120002';
            material.salesSpecifications = new List<WSMaterials.SalesSpecifications>();
            material.descriptions = new List<WSMaterials.Description>();

            for(Integer j = 0; j < 4; j++){
                WSMaterials.Description description = new WSMaterials.Description();
                description.languageCode = languages[j];
                description.value = 'Bla bla in '+languages[j];
                material.descriptions.add(description);

                WSMaterials.SalesSpecifications specification = new WSMaterials.SalesSpecifications();
                specification.salesOrganisationId           = '550'+j;
                specification.distributenChannelCode        = '5500'+j;
                specification.salesMeasureUnitCode          = 'PCE';
                specification.blockedForSalesIndicator      = false;
                specification.configurableIndicator         = false;
                specification.makeToOrderIndicator          = false;
                specification.previousMaterialInternalID    = '1123456479'+j;
                material.salesSpecifications.add(specification);
            }

            request.materials.add(material);
        }
        WSMaterials.MaterialResponse response = WSMaterials.upsertMaterial(request);

        System.assertEquals(2,[select count() from PricebookEntry where Pricebook2Id =: Test.getStandardPricebookId()]);
        Map<String,PricebookEntry> pricebookEntries = new Map<String,PricebookEntry>();
        for(PricebookEntry pbe : [select id,name,UnitPrice,ExternalId__c from PricebookEntry]){
            pricebookEntries.put(pbe.ExternalId__c,pbe);
        }



        Util.UtilPricebook.add(pricebookEntries).upsertPricebooks();


    }
}