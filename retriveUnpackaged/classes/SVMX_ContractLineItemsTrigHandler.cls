/*******************************************************************************************************
Description: 

Dependancy: 
    Trigger Framework: 
    Trigger: 
    Service Manager: 
    Test class: 
    
 
Author: Tulsi B R
Date: 19-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
20-02-2018      Ranjitha        Added methods to sum the price of contract lines items and populate the total price for contract Line items on contract
*******************************************************************************************************/
public class SVMX_ContractLineItemsTrigHandler extends SVMX_TriggerHandler {
   private list<SVMX_Contract_Line_Items__c> newWOList;
   private list<SVMX_Contract_Line_Items__c> oldWOList;
   private map<Id, SVMX_Contract_Line_Items__c> newWOMap;
   private map<Id, SVMX_Contract_Line_Items__c> oldWOMap;
   public SVMX_ContractLineItemsTrigHandler(){
        this.newWOList = (list<SVMX_Contract_Line_Items__c>) Trigger.new;
        this.oldWOList = (list<SVMX_Contract_Line_Items__c>) Trigger.old;
        this.newWOMap = (Map<Id, SVMX_Contract_Line_Items__c>) Trigger.newMap;
        this.oldWOMap = (Map<Id, SVMX_Contract_Line_Items__c>) Trigger.oldMap;
   
   }
    
  public override void beforeinsert(){
    
       for(SVMX_Contract_Line_Items__c contraLineItem:newWOList){
        if(contraLineItem.SVMX_Billing_schedule__c == System.Label.Yearly_on_Last_of_Start_month || contraLineItem.SVMX_Billing_schedule__c == System.Label.Quarterly_Every_3_months  ){
          contraLineItem.SVMX_Billing_Start_Billing_End__c=contraLineItem.SVMX_Contract_Start__c;
          contraLineItem.SVMX_Billing_End_Date__c =contraLineItem.SVMX_Contract_End__c;
        }else if(contraLineItem.SVMX_Billing_schedule__c==System.Label.Monthly_on_First_of_Month){
           if(contraLineItem.SVMX_Contract_Start__c.day() >= 15){
            contraLineItem.SVMX_Billing_Start_Billing_End__c=contraLineItem.SVMX_Contract_Start__c.addMonths(1).toStartOfMonth(); //addDays(-1);
            contraLineItem.SVMX_Billing_End_Date__c =contraLineItem.SVMX_Contract_End__c;
           }
           if(contraLineItem.SVMX_Contract_Start__c.day() < 15){
             contraLineItem.SVMX_Billing_Start_Billing_End__c=contraLineItem.SVMX_Contract_Start__c.addMonths(0).toStartOfMonth();
            contraLineItem.SVMX_Billing_End_Date__c =contraLineItem.SVMX_Contract_End__c;
           }
        }

        
       
       }
   
   }
   
   public override void afterInsert(){
        
       set<id> scIds = new set<id>();
       List<SVMXC__Service_Contract__c> contractList = new List<SVMXC__Service_Contract__c>();
       List<SVMX_Contract_Line_Items__c> contractLineItemList = new List<SVMX_Contract_Line_Items__c>();
       
       for(SVMX_Contract_Line_Items__c cli : newWOList){
           if(cli.SVMX_Total_Line_Price__c != null){
               scIds.add(cli.SVMX_Service_Contract__c);
           }

           if(cli.SVMX_Is_Billable__c == true && cli.SVMX_Service_Contract__c != Null){
              contractLineItemList.add(cli);
            }
       }
       
       if(!scIds.isEmpty())
           contractList = SVMX_ContractLineItemsServiceManager.updateContractPrice(scIds);
       
       if(!contractList.isEmpty())
           SVMX_ServiceContractDataManager.updateContractLIPrice(contractList);
           
       
        list<SVMX_Contract_Line_Items__c > scLineItemsIntList = new  list<SVMX_Contract_Line_Items__c > ();
        for(SVMX_Contract_Line_Items__c scLine : newWOList){
          if(scLine.SVMX_Service_Contract__c != Null && scLine.SVMX_Is_Billable__c == true){
            scLineItemsIntList.add(scLine);
          }
        }

        if(!scLineItemsIntList.isEmpty()) {
            SVMX_SAPServiceContractUtility.handleSAPServiceContractUpdateofLineItem(scLineItemsIntList);
         }

         if(!contractLineItemList.isEmpty()) {
            SVMX_ServContractLIneItemRollupsummary.handleNoOfContractLineItemChildCreation(contractLineItemList);
         }         
    }

    public override void afterupdate(){
        
       set<id> scIds = new set<id>();
       List<SVMXC__Service_Contract__c> contractList = new List<SVMXC__Service_Contract__c>();
       List<SVMX_Contract_Line_Items__c> contractLineItemList = new List<SVMX_Contract_Line_Items__c>();
       
       for(SVMX_Contract_Line_Items__c cli : newWOList){
           if(cli.SVMX_Total_Line_Price__c != null){
               scIds.add(cli.SVMX_Service_Contract__c);
           }
       }
       
       if(!scIds.isEmpty())
           contractList = SVMX_ContractLineItemsServiceManager.updateContractPrice(scIds);
       
       if(!contractList.isEmpty())
           SVMX_ServiceContractDataManager.updateContractLIPrice(contractList);
        
        map<Id,SVMX_Contract_Line_Items__c > scLineItemsIntMap = new  map<Id,SVMX_Contract_Line_Items__c> ();
        for(SVMX_Contract_Line_Items__c scLine : newWOMap.values()){
             if(scLine.SVMX_Service_Contract__c != Null && scLine.SVMX_Is_Billable__c == true){
               scLineItemsIntMap.put(scLine.Id,scLine);
             }

             if(scLine.SVMX_Service_Contract__c != Null && scLine.SVMX_Is_Billable__c == true){
              contractLineItemList.add(scLine);
            }

        }

        if(scLineItemsIntMap.keyset().size() > 0){
            SVMX_SAPServiceContractUtility.handleSAPExistingServiceContractLineItemUpdate(scLineItemsIntMap,oldWOMap);
         }

         if(!contractLineItemList.isEmpty()) {
            SVMX_ServContractLIneItemRollupsummary.handleNoOfContractLineItemChildCreation(contractLineItemList);
         }    
        
    }
    
    public override void beforeDelete(){
        for(SVMX_Contract_Line_Items__c cli : oldWOList){
           if(cli.SVMX_SAP_Contract_Item_Number__c != null)
               cli.addError('Cannot delete the line as Contract Item number is already generated');
        }
    }
    
    public override void afterdelete(){
        
       Set<id> scIds = new set<id>();
       List<SVMXC__Service_Contract__c> contractList = new List<SVMXC__Service_Contract__c>();
       List<SVMX_Contract_Line_Items__c> contractLineItemList = new List<SVMX_Contract_Line_Items__c>();

       for(SVMX_Contract_Line_Items__c cli : oldWOList){
           if(cli.SVMX_Line_Price__c != null){
               scIds.add(cli.SVMX_Service_Contract__c);
           }

           if(cli.SVMX_Service_Contract__c != Null && cli.SVMX_Is_Billable__c == true){
              contractLineItemList.add(cli);
            }
       }
       
       if(!scIds.isEmpty())
           contractList = SVMX_ContractLineItemsServiceManager.updateContractPrice(scIds);
       
       if(!contractList.isEmpty())
           SVMX_ServiceContractDataManager.updateContractLIPrice(contractList);

       if(!contractLineItemList.isEmpty()) {
            SVMX_ServContractLIneItemRollupsummary.handleNoOfContractLineItemChildCreation(contractLineItemList);
        }   
    }

}