/************************************************************************************************************
Description: Data Manager for Case. All the SOQL queries and DML Operations are performed in this class.

Dependency: 
 
Author: Ranjitha S
Date: 15-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
21/06/2018    Keshava Prasad    Added insertCase method
*******************************************************************************************************/

public class SVMX_CaseDataManager {
    
    public static void UpdateCase(List<Case> caseList){
        update caseList;
    }   
    
    public static void insertCase(Map<id,Case>caseList){
    
        insert caseList.values() ;
        }
}