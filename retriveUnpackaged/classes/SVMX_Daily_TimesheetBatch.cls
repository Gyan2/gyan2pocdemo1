/*******************************************************************************************************
Description: Batch class for creating daily timesheets

Dependancy:    
	Class: SVMX_TimesheetBatch_Controller
 
Author: Pooja Singh
Date: 14-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

global class SVMX_Daily_TimesheetBatch implements Database.Batchable<sObject>, Database.Stateful {
    public String query ='';
	public Set<String> dailyCtrySet = new Set<String>();
    public Set<String> profSet = new Set<String>();
    global SVMX_Daily_TimesheetBatch(Set<String> passedIdSet, List<String> profileList){
        dailyCtrySet = passedIdSet;
        for(Profile p :[Select Id, Name from Profile where Name IN: profileList]){
            profSet.add(p.Id);
            system.debug('profSet'+profSet);
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
        
    	query = 'Select Id, UserCountry__c from User where UserCountry__c IN :dailyCtrySet and isActive = true and profileId IN:profSet';
       	 return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<User> userList) {
        List<Id> UserIdList = new List<Id>();
        Map<Id,Id> existTsUserMap = new Map<Id,Id>();
		Set<Date> startDatesSet = new Set<Date>();
   		//Set<Date> endDatesSet = new Set<Date>();
   		Map<Date,Date> stDtEdDtMap = new Map<Date,Date>();
        //Integer n;
        Date startDate;
        
        //date collection for next week
        for(Integer i=1; i<=7; i++){
        	startDate = system.today()+i;
            //endDate = startDate;
            startDatesSet.add(startDate);
            stDtEdDtMap.put(startDate,startDate);
            //endDatesSet.add(endDate);
        }
		//Query if a timesheet exists for the given date range
        List<SVMXC__Timesheet__c> timeSheetQuery = new List<SVMXC__Timesheet__c>([Select Id, Name, SVMX_Current_Week__c, SVMXC__Start_Date__c, SVMXC__End_Date__c,SVMXC__Period__c, SVMXC__User__c from SVMXC__Timesheet__c where SVMXC__User__c IN:userList and SVMXC__Start_Date__c IN :startDatesSet and SVMXC__End_Date__c IN: startDatesSet]);
        for(SVMXC__Timesheet__c ts : timeSheetQuery){
            ts.SVMX_Current_Week__c = true;
            existTsUserMap.put(ts.SVMXC__User__c,ts.id);
            system.debug('existing timesheets for daily user'+existTsUserMap);
        }
        if(timeSheetQuery.size() > 0)
        	update timeSheetQuery;
        
        for(User u : userList){
            if(!existTsUserMap.containsKey(u.id)){
                UserIdList.add(u.id);
            	system.debug('List of users for new daily timesheets'+UserIdList);
            }
        }
        if(UserIdList.size() > 0)
        	SVMX_TimesheetBatch_Controller.createTimesheet(startDatesSet, stDtEdDtMap, 'Daily', UserIdList);
        
        /* Date collection for previous daily timesheets*/
        Date prevStDt;	//previous start date
        Set<Date> prevStDtSet = new Set<Date>();
        //Date prevEdDt;	//previous end date
        Map<Date,Date> prevStDtEdDtMap = new Map<Date,Date>();
        
        for(Integer i=-6; i<=0; i++){
            prevStDt = system.today()+i;
            prevStDtSet.add(prevStDt);
        	//prevEdDt = startDate;
            prevStDtEdDtMap.put(prevStDt,prevStDt);
        }
        SVMX_TimesheetBatch_Controller.prevWeekTimeSheet(prevStDtSet,prevStDtEdDtMap);
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}