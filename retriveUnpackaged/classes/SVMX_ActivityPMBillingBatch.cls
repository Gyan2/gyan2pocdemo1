/************************************************************************************************************
Description:.

Dependency: 
 
Author: Tulsi B R
Date: 19-01-2018

Modification Log: 
Date            Author          Modification Comments

--------------------------------------------------------------
 
*******************************************************************************************************/

global class SVMX_ActivityPMBillingBatch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful  {
  
  
 public string gupdateWorkOrderQuery;

 global Database.QueryLocator start(Database.BatchableContext BC) {

  system.debug('**** NAVEED TEST *** ');
   
       //SVMX_Pricing_Method__c
       String workOrdreType=Label.Planned_Services_PPM;
       String Ready_To_Bill = Label.Ready_To_Bill;
       String allLocationsPerVisit=Label.All_Locations_P;
       String perLocationPerVisit=Label.Per_Location_Per_Visit;
       List<string> contractCond=new List<String>();
       contractCond.add(perLocationPerVisit);
       contractCond.add(allLocationsPerVisit);
       gupdateWorkOrderQuery='Select Id,Name,SVMXC__Site__c,SVMXC__Site__r.Name,SVMX_PM_Schedule_Definition__c,SVMXC__Case__c,SVMXC__Service_Contract__c,SVMXC__Service_Contract__r.SVMXC__Billing_Schedule__c,SVMXC__Case__r.Status';
       gupdateWorkOrderQuery+=' From SVMXC__Service_Order__c';
       gupdateWorkOrderQuery+=' Where  SVMXC__Order_Type__c=:workOrdreType and SVMXC__Case__r.SVMX_Invoice_Status__c = null and SVMXC__Order_Status__c =: Ready_To_Bill';
      // gupdateWorkOrderQuery+=' AND  SVMXC__Case__r.Status=:casestatus';

       gupdateWorkOrderQuery+=' AND SVMXC__Service_Contract__r.SVMXC__Billing_Schedule__c IN(\'' + string.join(New List<String>(contractCond),'\',\'') + '\')';
       system.debug('gupdateWorkOrderQuery ---->>>'+gupdateWorkOrderQuery);
       return Database.getQueryLocator(gupdateWorkOrderQuery);
  
 }
 global void execute(Database.BatchableContext BC, List<SVMXC__Service_Order__c> workOrders) {
       String allLocationsPerVisit=Label.All_Locations_P;
        String perLocationPerVisit=Label.Per_Location_Per_Visit;
        String Ready_To_Bill = Label.Ready_To_Bill;
        

        system.debug('**** NAVEED *** '+ workOrders);


       List<Id>  pmScheduleIds=new List<Id>();
       //pmScheduleIds.add(Id.Valueof('a0h0E000000P3vB'));
       for(SVMXC__Service_Order__c  serOrder:workOrders){
        if(serOrder.SVMX_PM_Schedule_Definition__c!=null && serOrder.SVMXC__Case__c!=null){
          pmScheduleIds.add(serOrder.SVMX_PM_Schedule_Definition__c);    
        }
       }
       system.debug('pmScheduleIds '+pmScheduleIds);
     
       List<Case>  updateCaseList=new List<Case>();
       List<SVMXC__Service_Order__c>  wkOderAllLoc=new  List<SVMXC__Service_Order__c> ();

       List<SVMXC__Service_Order__c>  wkOderPerLoc=new  List<SVMXC__Service_Order__c> ();
       List<SVMXC__Service_Order__c>  wkorder=[Select Id,Name,SVMX_PM_Schedule_Definition__c,SVMXC__Site__c,SVMXC__Case__c,SVMXC__Service_Contract__c,SVMXC__Service_Contract__r.SVMXC__Billing_Schedule__c, SVMXC__Case__r.Status, SVMXC__Case__r.SVMXC__Site__c From SVMXC__Service_Order__c where SVMX_PM_Schedule_Definition__c IN :pmScheduleIds];

           List<Id> caseAllLocIds=new List<Id>();
           Map<Id,String> caseAllLocStatus=new Map<Id,String>();
           List<Id> casePerLocIds=new List<Id>();
           Map<Id,String> locCaseIdsMap=new   Map<Id,String>() ;
            Map<Id,Id> locCaseMap=new   Map<Id,Id>() ;
           Map<Id,List<Id>> allloclistCaseIdsMap=new   Map<Id,List<Id>>() ;

           Map<Id,List<Id>> pmSites = new  Map<Id,List<Id>>();
  
           for(SVMXC__Service_Order__c wrkord:wkorder){
            //system.debug('pm sdef'+wrkord.SVMX_PM_Schedule_Definition__c);
              if(wrkord.SVMXC__Service_Contract__r.SVMXC__Billing_Schedule__c == allLocationsPerVisit){
               
                  wkOderAllLoc.add(wrkord);
                  if(wrkord.SVMXC__Site__c!=null && wrkord.SVMXC__Case__c!=null){
                       locCaseIdsMap.put(wrkord.SVMXC__Site__c, wrkord.SVMXC__Case__r.Status);
                       locCaseMap.put(wrkord.SVMXC__Site__c, wrkord.SVMXC__Case__c);
                       //caseAllLocStatus.put(wrkord.SVMXC__Case__c,wrkord.SVMXC__Case__r.Status);
                        //check if all locations for that site are complete
                       if(pmSites.containsKey(wrkord.SVMX_PM_Schedule_Definition__c)){
                         List<Id> sites = pmSites.get(wrkord.SVMX_PM_Schedule_Definition__c);
                         sites.add(wrkord.SVMXC__Site__c);
                         pmSites.put(wrkord.SVMX_PM_Schedule_Definition__c, sites);
                        }else{
                          pmSites.put(wrkord.SVMX_PM_Schedule_Definition__c, new List<Id>{wrkord.SVMXC__Site__c});
                       }
                      
                   //allloclistCaseIdsMap.put(wrkord.SVMXC__Case__c,wrkord.SVMXC__Site__c);
                  }
                 
              }else if(wrkord.SVMXC__Service_Contract__r.SVMXC__Billing_Schedule__c == perLocationPerVisit && wrkord.SVMXC__Case__r.Status == system.Label.Ready_To_Bill){
                  //casePerLocIds.add(wrkord.SVMXC__Case__c);
                  //wkOderPerLoc.add(wrkord);
                  case cs = new case(Id=wrkord.SVMXC__Case__c);
                  cs.SVMX_Invoice_Status__c=Label.Requested;
                  updateCaseList.add(cs);
              }

           }

           system.debug('PM Sites '+pmSites);
           //check Logic for All Locations Per Visit
           if(pmSites.size() > 0){
             for(Id pmId: pmScheduleIds){
                  if(pmSites.containskey(pmid)){
                     List<Id> allSites = pmSites.get(pmid);
                     Integer allsitescount =allSites.size();
                     Integer count =0;
                     set<Id> caseIdset = new set<Id>();
                     for(Id site: allSites){
                        if(locCaseIdsMap.containskey(site)){
                            if(locCaseIdsMap.get(site) == Ready_To_Bill ){
                              count++;
                              caseIdset.add(locCaseMap.get(site));
                            }

                        }else{
                          system.debug(' in break for PM ID '+pmId);
                          break;
                        }
                     }

                     system.debug(' allsitescount '+allsitescount);
                     system.debug('case lsit '+count);
                     //update all cases since locations are complete
                     if(count == allsitescount){
                       for(Id  allLocCases:caseIdset){
                          case cs = new case(Id=allLocCases);
                          cs.SVMX_Invoice_Status__c=Label.Requested;
                           updateCaseList.add(cs);
                      }
                     }

                  }
                 
             }
          }



          /* Map<Id,Case>    CaseIdsAndCaseMap=new Map<Id,Case>();
           Set<Id>   updateCaseIdsAllLoc=new set<Id>();
           for(Id   locIds:allloclistCaseIdsMap.keySet()){
             for(Id   caseIds:allloclistCaseIdsMap.get(locIds)){
               String   alllocCase=caseAllLocStatus.get(caseIds);
               if(alllocCase == system.Label.Ready_To_Bill){
                  updateCaseIdsAllLoc.add(caseIds);
               }
             }
           }
           
           for(Id  allLocCases:updateCaseIdsAllLoc){
                 case cs = new case(Id=allLocCases);
                 cs.SVMX_Invoice_Status__c=Label.Requested;
                 updateCaseList.add(cs);
           }
         
           for(Id  cases:casePerLocIds){
                case cs = new case(Id=cases);
                cs.SVMX_Invoice_Status__c=Label.Requested;
                updateCaseList.add(cs);
           }*/
          
           

/////////// Batch calls the soap utility class synchronously ///////////
        if(updateCaseList.size() >0 ){
            //Set<Id> caseIds = new Set<Id>();
            Set<Id> wdUpdateIds = new Set<Id>();
            Set<Id> wdCreateIds = new Set<Id>();
            List<SVMXC__Service_Order_Line__c> wdUsageList = new List<SVMXC__Service_Order_Line__c>();
            List<SVMXC__Service_Order_Line__c> wdProdServList = new List<SVMXC__Service_Order_Line__c>();

            id usageRecordTypeId = SVMX_RecordTypeDataManager.getRecordTypeID('UsageConsumption','SVMXC__Service_Order_Line__c');
            id prodServicedRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Products_Serviced','SVMXC__Service_Order_Line__c');
        
           /* for(Case cas : newCaseMap.values()) {

                if(oldCaseMap.get(cas.Id).SVMX_Invoice_Status__c != cas.SVMX_Invoice_Status__c
                    && cas.SVMX_Invoice_Status__c == System.Label.Requested) {

                    caseIds.add(cas.Id);
                }
            }*/

            wdUsageList = [SELECT Id, Name, SVMXC__Service_Order__r.SVMXC__Case__c, SVMXC__Service_Order__c,
                           SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c, SVMX_PM_Charge__c,
                           SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMX_Pricing_Method__c, SVMXC__Service_Order__r.SVMXC__Order_Type__c 
                           FROM SVMXC__Service_Order_Line__c 
                           WHERE SVMXC__Service_Order__r.SVMXC__Case__c IN : updateCaseList
                           AND RecordTypeId = : usageRecordTypeId
                           AND (SVMXC__Line_Type__c = 'Labor'
                                OR SVMXC__Line_Type__c = 'Travel'
                                OR SVMXC__Line_Type__c = 'Expenses')
                            AND (SVMXC__Service_Order__r.SVMXC__Order_Type__c = 'Reactive'
                                 OR SVMXC__Service_Order__r.SVMXC__Order_Type__c = 'Quoted Works'
                                 OR SVMXC__Service_Order__r.SVMXC__Order_Type__c = 'Installation'
                                 OR SVMXC__Service_Order__r.SVMXC__Order_Type__c = 'Planned Services (PPM)')
                            AND SVMX_Sales_Order_Item_Number__c = null];

            wdProdServList = [SELECT Id, Name, SVMXC__Service_Order__r.SVMXC__Case__c, SVMXC__Service_Order__c,
                              SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c, SVMX_PM_Charge__c,
                              SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMX_Pricing_Method__c, SVMXC__Service_Order__r.SVMXC__Order_Type__c 
                              FROM SVMXC__Service_Order_Line__c 
                              WHERE SVMXC__Service_Order__r.SVMXC__Case__c IN : updateCaseList
                              AND RecordTypeId = : prodServicedRecordTypeId
                              AND SVMXC__Service_Order__r.SVMXC__Order_Type__c = 'Planned Services (PPM)'];

            if(!wdUsageList.isEmpty()) {

                // Build 2 lists; 1 for exisiting Sale sOrder Numbers (i.e. Sales Order Number already exists, 
                // due to Parts Usage, and 1 where a Part did not exist, so a Sales Order needs to be created)
                for(SVMXC__Service_Order_Line__c wd : wdUsageList) {
                    
                    if(wd.SVMXC__Service_Order__r.SVMXC__Order_Type__c == System.Label.Planned_Services_PPM && wd.SVMX_PM_Charge__c == 'Yes'
                        || (wd.SVMXC__Service_Order__r.SVMXC__Order_Type__c == System.Label.Reactive || wd.SVMXC__Service_Order__r.SVMXC__Order_Type__c == System.Label.Installation)) {
                        
                        if(wd.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c != null) {
                            wdUpdateIds.add(wd.Id);  
                        }
                        else {

                            wdCreateIds.add(wd.Id);
                        } 
                    }
                }
            }

            if(!wdProdServList.isEmpty()) {

                for(SVMXC__Service_Order_Line__c wdp : wdProdServList) {

                    if(wdp.SVMXC__Service_Order__r.SVMXC__Service_Contract__r.SVMX_Pricing_Method__c == System.Label.Activity_Based) {

                        if(wdp.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c != null) {

                            wdUpdateIds.add(wdp.Id);  
                        }
                        else {

                            wdCreateIds.add(wdp.Id);
                        } 
                    }
                }
            }
                

            if(!wdUpdateIds.isEmpty()) {
                
                //isBatch needs to be checked, due to Activity Based Contracts. Batch will run that sets Case Invoice
                // Status to 'Requested', which will trigger this method to run
                if(System.isBatch()) {    
                  
                  
                  SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderItemUpdate(wdUpdateIds, true);
                }
                /*else {

                  asyncSalesOrderItemUpdateMethod(wdUpdateIds, true);              
                }*/
            }

            if(!wdCreateIds.isEmpty()) {

                if(System.isBatch()) {
                  
                  
                  SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderCreation(wdCreateIds); 
                }
                /*else {

                  asyncSalesOrderCreationMethod(wdCreateIds);
                }*/
            }
        } 
        
        system.debug('updateCaseList ---->>>'+updateCaseList);
        if(updateCaseList!=null && updateCaseList.size() >0){
                SVMX_CaseDataManager.UpdateCase(updateCaseList);
        }   
      
 }
 
 global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
 }
 
}