@IsTest
private class SVMX_dormaEsmEdt10SalesOrder_UT
{
    static testMethod void coverTypes(){

        new SVMX_dormaEsmEdt10SalesOrder.PropertyValueDateSpecification();
        new SVMX_dormaEsmEdt10SalesOrder.PersonName();
        new SVMX_dormaEsmEdt10SalesOrder.PriceComponent();
        new SVMX_dormaEsmEdt10SalesOrder.Log();
        new SVMX_dormaEsmEdt10SalesOrder.Rate();
        new SVMX_dormaEsmEdt10SalesOrder.LogItem();
        new SVMX_dormaEsmEdt10SalesOrder.PropertyValueNumericSpecification();
        new SVMX_dormaEsmEdt10SalesOrder.PropertyValue();
        new SVMX_dormaEsmEdt10SalesOrder.PropertyDefinitionClassReference();
        new SVMX_dormaEsmEdt10SalesOrder.PropertyValuationValueGroup();
        new SVMX_dormaEsmEdt10SalesOrder.UnloadingPoint();
        new SVMX_dormaEsmEdt10SalesOrder.PropertyValueTextSpecification();
        new SVMX_dormaEsmEdt10SalesOrder.CompanyName();
        new SVMX_dormaEsmEdt10SalesOrder.PropertyValueIndicatorSpecification();
        new SVMX_dormaEsmEdt10SalesOrder.Incoterms();
        new SVMX_dormaEsmEdt10SalesOrder.PropertyValueTimeSpecification();
        
        
    }
}