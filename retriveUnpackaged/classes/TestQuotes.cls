/**
 * Created by rebmangu on 07/03/2018.
 */

@IsTest
private class TestQuotes {

    @testSetup static void setup(){

        Product2 product = new Product2(
                Name='Test',
                SalesSpecifications__c='{"blabla":"blabla"}',
                ExternalId__c = 'PS8-00000001',
                SAPNumber__c = '00000001',
                QuantityUnitOfMeasure = 'PCE'
        );
        insert product;


        PricebookEntry pbE = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(),Product2Id=product.Id,UnitPrice=1000,IsActive=true);
        insert pbE;

        Account acc = new Account(SAPNumber__c='123456789',Name='Test Account');
        insert acc;

        Opportunity opp = new Opportunity(Name='Test opp',AccountId=acc.Id,StageName='Proposal/Price Quote',CloseDate=Date.valueOf(System.now()));
        insert opp;


    }


    static testMethod void testPricebook() {
        Opportunity opp = [select id from Opportunity limit 1];
        Quote quo = new Quote(OpportunityId=opp.Id,Name='Test Quote',SalesOrg__c='5500',ScheduledDate__c=Date.valueOf(System.now()));
        insert quo;
        
        System.assertEquals(Test.getStandardPricebookId(),[select id,Pricebook2Id from Quote where id =: quo.Id].Pricebook2Id);

    }
}