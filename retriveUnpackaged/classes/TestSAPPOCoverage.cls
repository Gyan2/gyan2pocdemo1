/**
 * Created by rebmangu on 14/02/2018.
 */

@IsTest
private class TestSAPPOCoverage {
    static testMethod void CoverageACK() {
        SAPPO_ACK.notificationsResponse_element element = new SAPPO_ACK.notificationsResponse_element();
        element.Ack = true;
    }

    static testMethod void CoverageCommonDataTypes(){
        SAPPO_CommonDataTypes.LocationStandardID locationStandardID = new SAPPO_CommonDataTypes.LocationStandardID();

        SAPPO_CommonDataTypes.PartyTaxID partyTaxID = new SAPPO_CommonDataTypes.PartyTaxID();

        SAPPO_CommonDataTypes.Description description = new SAPPO_CommonDataTypes.Description();

        SAPPO_CommonDataTypes.Email email = new SAPPO_CommonDataTypes.Email();

        SAPPO_CommonDataTypes.Telephone telephone = new SAPPO_CommonDataTypes.Telephone();

        SAPPO_CommonDataTypes.BusinessDocumentMessageHeader businessDocumentMessageHeader = new SAPPO_CommonDataTypes.BusinessDocumentMessageHeader();

        SAPPO_CommonDataTypes.BusinessDocumentMessageID businessDocumentMessageID = new SAPPO_CommonDataTypes.BusinessDocumentMessageID();

        SAPPO_CommonDataTypes.EmailURI emailURI = new SAPPO_CommonDataTypes.EmailURI();

        SAPPO_CommonDataTypes.BusinessScope businessScope = new SAPPO_CommonDataTypes.BusinessScope();

        SAPPO_CommonDataTypes.TransportationZoneID transportationZoneID = new SAPPO_CommonDataTypes.TransportationZoneID();

        SAPPO_CommonDataTypes.CustomerID customerID = new SAPPO_CommonDataTypes.CustomerID();

        SAPPO_CommonDataTypes.PartyStandardID partyStandardID = new SAPPO_CommonDataTypes.PartyStandardID();

        SAPPO_CommonDataTypes.SHORT_Note short_note = new SAPPO_CommonDataTypes.SHORT_Note();

        SAPPO_CommonDataTypes.Web web = new SAPPO_CommonDataTypes.Web();

        SAPPO_CommonDataTypes.PhoneNumber phoneNumber = new SAPPO_CommonDataTypes.PhoneNumber();

        SAPPO_CommonDataTypes.MEDIUM_Name medium_name = new SAPPO_CommonDataTypes.MEDIUM_Name();

        SAPPO_CommonDataTypes.SHORT_Description short_description = new SAPPO_CommonDataTypes.SHORT_Description();

        SAPPO_CommonDataTypes.Facsimile facsimile = new SAPPO_CommonDataTypes.Facsimile();

        SAPPO_CommonDataTypes.PartyInternalID partyInternalID = new SAPPO_CommonDataTypes.PartyInternalID();

        SAPPO_CommonDataTypes.BusinessScopeInstanceID businessScopeInstanceID = new SAPPO_CommonDataTypes.BusinessScopeInstanceID();

        SAPPO_CommonDataTypes.BusinessScopeID businessScopeID = new SAPPO_CommonDataTypes.BusinessScopeID();

        SAPPO_CommonDataTypes.Address address = new SAPPO_CommonDataTypes.Address();

        SAPPO_CommonDataTypes.ProductInternalID productInternalID = new SAPPO_CommonDataTypes.ProductInternalID();

        SAPPO_CommonDataTypes.ConfigurationPropertyValuation configurationPropertyValuation = new SAPPO_CommonDataTypes.ConfigurationPropertyValuation();

        SAPPO_CommonDataTypes.ContactPerson contactPerson = new SAPPO_CommonDataTypes.ContactPerson();

        SAPPO_CommonDataTypes.AttachmentID attachmentID = new SAPPO_CommonDataTypes.AttachmentID();

        SAPPO_CommonDataTypes.ProductStandardID productStandardID = new SAPPO_CommonDataTypes.ProductStandardID();

        SAPPO_CommonDataTypes.PropertyID propertyID = new SAPPO_CommonDataTypes.PropertyID();

        SAPPO_CommonDataTypes.Party party = new SAPPO_CommonDataTypes.Party();

        SAPPO_CommonDataTypes.OrderRejectionReasonCode orderRejectionReasonCode = new SAPPO_CommonDataTypes.OrderRejectionReasonCode();

        SAPPO_CommonDataTypes.BusinessDocumentTextCollectionText businessDocumentTextCollectionText = new SAPPO_CommonDataTypes.BusinessDocumentTextCollectionText();

        SAPPO_CommonDataTypes.Communication communication = new SAPPO_CommonDataTypes.Communication();

        SAPPO_CommonDataTypes.BusinessDocumentTextCollection businessDocumentTextCollection = new SAPPO_CommonDataTypes.BusinessDocumentTextCollection();

        SAPPO_CommonDataTypes.Amount amount = new SAPPO_CommonDataTypes.Amount();

        SAPPO_CommonDataTypes.BusinessTransactionDocumentID businessTransactionDocumentID = new SAPPO_CommonDataTypes.BusinessTransactionDocumentID();

        SAPPO_CommonDataTypes.Quantity quantity = new SAPPO_CommonDataTypes.Quantity();

        SAPPO_CommonDataTypes.ContactPersonInternalID contactPersonInternalID = new SAPPO_CommonDataTypes.ContactPersonInternalID();

        SAPPO_CommonDataTypes.Text txt = new SAPPO_CommonDataTypes.Text();

        SAPPO_CommonDataTypes.PhysicalAddress physicalAddress = new SAPPO_CommonDataTypes.PhysicalAddress();

        SAPPO_CommonDataTypes.PropertyReference propertyReference = new SAPPO_CommonDataTypes.PropertyReference();

        SAPPO_CommonDataTypes.PropertyValuation propertyValuation = new SAPPO_CommonDataTypes.PropertyValuation();

        SAPPO_CommonDataTypes.BasicMessageHeader basicMessageHeader = new SAPPO_CommonDataTypes.BasicMessageHeader();

        SAPPO_CommonDataTypes.LONG_Name long_name = new SAPPO_CommonDataTypes.LONG_Name();

        SAPPO_CommonDataTypes.BusinessDocumentAttachment businessDocumentAttachment = new SAPPO_CommonDataTypes.BusinessDocumentAttachment();

        SAPPO_CommonDataTypes.WorkplaceAddress workplaceAddress = new SAPPO_CommonDataTypes.WorkplaceAddress();

    }

    static testMethod void CoverageSalesOrderRequest(){
        SAPPO_SalesOrderRequest.SalesDocumentRequestGoodsRecipientDocument SalesDocumentRequestGoodsRecipientDocument = new SAPPO_SalesOrderRequest.SalesDocumentRequestGoodsRecipientDocument();
        SAPPO_SalesOrderRequest.SalesOrderCreateRequestSalesOrder SalesOrderCreateRequestSalesOrder = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestSalesOrder();
        SAPPO_SalesOrderRequest.SalesOrderCreateRequestInvoiceTerms SalesOrderCreateRequestInvoiceTerms = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestInvoiceTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestBusiTransDocRef SalesDocumentRequestBusiTransDocRef = new SAPPO_SalesOrderRequest.SalesDocumentRequestBusiTransDocRef();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseBuyerParty SalesOrderSimulateResponseBuyerParty = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseBuyerParty();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseTotalValues SalesOrderSimulateResponseTotalValues = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseTotalValues();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseSalesOrder SalesOrderSimulateResponseSalesOrder = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseSalesOrder();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemScheduleLine SalesOrderSimulateResponseItemScheduleLine = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemScheduleLine();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseParty SalesOrderSimulateResponseParty = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseParty();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponsePriceComponent SalesOrderSimulateResponsePriceComponent = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponsePriceComponent();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemTotalValues SalesOrderSimulateResponseItemTotalValues = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemTotalValues();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponse SalesOrderSimulateResponse = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponse();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseGoodsRecipientParty SalesOrderSimulateResponseGoodsRecipientParty = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseGoodsRecipientParty();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponsePartyName SalesOrderSimulateResponsePartyName = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponsePartyName();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponsePartyAddress SalesOrderSimulateResponsePartyAddress = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponsePartyAddress();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemProduct SalesOrderSimulateResponseItemProduct = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemProduct();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponsePriceComponentCalculationBasis SalesOrderSimulateResponsePriceComponentCalculationBasis = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponsePriceComponentCalculationBasis();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseDeliveryTerms SalesOrderSimulateResponseDeliveryTerms = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseDeliveryTerms();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem SalesOrderSimulateResponseItem = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent SalesOrderSimulateResponseItemPriceComponent = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemProduct SalesDocumentRequestItemProduct = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemProduct();
        SAPPO_SalesOrderRequest.SalesOrderUpdateRequest SalesOrderUpdateRequest = new SAPPO_SalesOrderRequest.SalesOrderUpdateRequest();
        SAPPO_SalesOrderRequest.SalesOrderUpdateRequestParty SalesOrderUpdateRequestParty = new SAPPO_SalesOrderRequest.SalesOrderUpdateRequestParty();
        SAPPO_SalesOrderRequest.SalesDocumentRequestStatusObject SalesDocumentRequestStatusObject = new SAPPO_SalesOrderRequest.SalesDocumentRequestStatusObject();
        SAPPO_SalesOrderRequest.SalesOrderUpdateRequestSalesOrder SalesOrderUpdateRequestSalesOrder = new SAPPO_SalesOrderRequest.SalesOrderUpdateRequestSalesOrder();
        SAPPO_SalesOrderRequest.SalesOrderRequestDateTerms SalesOrderRequestDateTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestDateTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemStatusObject SalesDocumentRequestItemStatusObject = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemStatusObject();
        SAPPO_SalesOrderRequest.SalesOrderUpdateRequestPriceComponent SalesOrderUpdateRequestPriceComponent = new SAPPO_SalesOrderRequest.SalesOrderUpdateRequestPriceComponent();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemStatus SalesDocumentRequestItemStatus = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemStatus();
        SAPPO_SalesOrderRequest.SalesOrderRequest SalesOrderRequest = new SAPPO_SalesOrderRequest.SalesOrderRequest();
        SAPPO_SalesOrderRequest.SalesOrderUpdateRequestItem SalesOrderUpdateRequestItem = new SAPPO_SalesOrderRequest.SalesOrderUpdateRequestItem();
        SAPPO_SalesOrderRequest.SalesOrderCreateRequestItem SalesOrderCreateRequestItem = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestItem();
        SAPPO_SalesOrderRequest.SalesOrderRequestItemTotalValues SalesOrderRequestItemTotalValues = new SAPPO_SalesOrderRequest.SalesOrderRequestItemTotalValues();
        SAPPO_SalesOrderRequest.SalesDocumentRequestGoodsRecipientParty SalesDocumentRequestGoodsRecipientParty = new SAPPO_SalesOrderRequest.SalesDocumentRequestGoodsRecipientParty();
        SAPPO_SalesOrderRequest.SalesOrderRequestDeliveryTerms SalesOrderRequestDeliveryTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestDeliveryTerms();
        SAPPO_SalesOrderRequest.SalesOrderUpdateRequestInvoiceTerms SalesOrderUpdateRequestInvoiceTerms = new SAPPO_SalesOrderRequest.SalesOrderUpdateRequestInvoiceTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestStatus SalesDocumentRequestStatus = new SAPPO_SalesOrderRequest.SalesDocumentRequestStatus();
        SAPPO_SalesOrderRequest.SalesOrderCreateRequest SalesOrderCreateRequest = new SAPPO_SalesOrderRequest.SalesOrderCreateRequest();
        SAPPO_SalesOrderRequest.SalesDocumentRequestShipToLocation SalesDocumentRequestShipToLocation = new SAPPO_SalesOrderRequest.SalesDocumentRequestShipToLocation();
        SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder SalesOrderRequestSalesOrder = new SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder();
        SAPPO_SalesOrderRequest.SalesOrderRequestSellerParty SalesOrderRequestSellerParty = new SAPPO_SalesOrderRequest.SalesOrderRequestSellerParty();
        SAPPO_SalesOrderRequest.SalesOrderRequestItemProduct SalesOrderRequestItemProduct = new SAPPO_SalesOrderRequest.SalesOrderRequestItemProduct();
        SAPPO_SalesOrderRequest.SalesDocumentRequestPricingTerms SalesDocumentRequestPricingTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestPricingTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestSellerParty SalesDocumentRequestSellerParty = new SAPPO_SalesOrderRequest.SalesDocumentRequestSellerParty();
        SAPPO_SalesOrderRequest.SalesOrderUpdateItemSalesTerms SalesOrderUpdateItemSalesTerms = new SAPPO_SalesOrderRequest.SalesOrderUpdateItemSalesTerms();
        SAPPO_SalesOrderRequest.SalesOrderUpdateRequestDeliveryTerms SalesOrderUpdateRequestDeliveryTerms = new SAPPO_SalesOrderRequest.SalesOrderUpdateRequestDeliveryTerms();
        SAPPO_SalesOrderRequest.SalesOrderRequestShipToLocation SalesOrderRequestShipToLocation = new SAPPO_SalesOrderRequest.SalesOrderRequestShipToLocation();
        SAPPO_SalesOrderRequest.SalesOrderRequestBuyerParty SalesOrderRequestBuyerParty = new SAPPO_SalesOrderRequest.SalesOrderRequestBuyerParty();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemStatusObjectUserStatus SalesDocumentRequestItemStatusObjectUserStatus = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemStatusObjectUserStatus();
        SAPPO_SalesOrderRequest.SalesOrderRequestTransportationTerms SalesOrderRequestTransportationTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestTransportationTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemInvoiceTerms SalesDocumentRequestItemInvoiceTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemInvoiceTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestDateTerms SalesDocumentRequestDateTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestDateTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestCashDiscountTerms SalesDocumentRequestCashDiscountTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestCashDiscountTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestBuyerDocument SalesDocumentRequestBuyerDocument = new SAPPO_SalesOrderRequest.SalesDocumentRequestBuyerDocument();
        SAPPO_SalesOrderRequest.SalesOrderCreateRequestTransportationTerms SalesOrderCreateRequestTransportationTerms = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestTransportationTerms();
        SAPPO_SalesOrderRequest.SalesOrderCreateRequestDeliveryTerms SalesOrderCreateRequestDeliveryTerms = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestDeliveryTerms();
        SAPPO_SalesOrderRequest.SalesOrderRequestItemDeliveryTerms SalesOrderRequestItemDeliveryTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestItemDeliveryTerms();
        SAPPO_SalesOrderRequest.SalesOrderCreateRequestItemBOMVariant SalesOrderCreateRequestItemBOMVariant = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestItemBOMVariant();
        SAPPO_SalesOrderRequest.SalesOrderRequestItem SalesOrderRequestItem = new SAPPO_SalesOrderRequest.SalesOrderRequestItem();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemDateTerms SalesDocumentRequestItemDateTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemDateTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemDeliveryTerms SalesDocumentRequestItemDeliveryTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemDeliveryTerms();
        SAPPO_SalesOrderRequest.SalesOrderRequestPricingTerms SalesOrderRequestPricingTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestPricingTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemBuyerDocument SalesDocumentRequestItemBuyerDocument = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemBuyerDocument();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemTotalValues SalesDocumentRequestItemTotalValues = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemTotalValues();
        SAPPO_SalesOrderRequest.SalesOrderCreateRequestItemBOM SalesOrderCreateRequestItemBOM = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestItemBOM();
        SAPPO_SalesOrderRequest.SalesOrderCreateRequestItemBOMVariantItem SalesOrderCreateRequestItemBOMVariantItem = new SAPPO_SalesOrderRequest.SalesOrderCreateRequestItemBOMVariantItem();
        SAPPO_SalesOrderRequest.SalesOrderRequestItemDateTerms SalesOrderRequestItemDateTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestItemDateTerms();
        SAPPO_SalesOrderRequest.SalesDocumentRequestSalesChannel SalesDocumentRequestSalesChannel = new SAPPO_SalesOrderRequest.SalesDocumentRequestSalesChannel();
        SAPPO_SalesOrderRequest.SalesDocumentRequestItemBusiTransDocRef SalesDocumentRequestItemBusiTransDocRef = new SAPPO_SalesOrderRequest.SalesDocumentRequestItemBusiTransDocRef();
        SAPPO_SalesOrderRequest.SalesDocumentRequestStatusObjectUserStatus SalesDocumentRequestStatusObjectUserStatus = new SAPPO_SalesOrderRequest.SalesDocumentRequestStatusObjectUserStatus();
        SAPPO_SalesOrderRequest.SalesDocumentRequestBuyerParty SalesDocumentRequestBuyerParty = new SAPPO_SalesOrderRequest.SalesDocumentRequestBuyerParty();
        SAPPO_SalesOrderRequest.SalesDocumentRequestSalesTerms SalesDocumentRequestSalesTerms = new SAPPO_SalesOrderRequest.SalesDocumentRequestSalesTerms();
        SAPPO_SalesOrderRequest.SalesOrderRequestGoodsRecipientParty SalesOrderRequestGoodsRecipientParty = new SAPPO_SalesOrderRequest.SalesOrderRequestGoodsRecipientParty();
        SAPPO_SalesOrderRequest.SalesOrderUpdateRequestTransportationTerms SalesOrderUpdateRequestTransportationTerms = new SAPPO_SalesOrderRequest.SalesOrderUpdateRequestTransportationTerms();



    }

    static testMethod void CoverageDocumentRequest(){

        /* REQUESTS */

        SAPPO_DocumentRequest.DocumentCreateMultipleRequest DocumentCreateMultipleRequest = new SAPPO_DocumentRequest.DocumentCreateMultipleRequest();
        SAPPO_DocumentRequest.DocumentCreateRequest DocumentCreateRequest = new SAPPO_DocumentRequest.DocumentCreateRequest();
        SAPPO_DocumentRequest.BusinessDocumentMessageHeader BusinessDocumentMessageHeader = new SAPPO_DocumentRequest.BusinessDocumentMessageHeader();
        SAPPO_DocumentRequest.BusinessDocumentMessageID BusinessDocumentMessageID = new SAPPO_DocumentRequest.BusinessDocumentMessageID();
        SAPPO_DocumentRequest.BusinessScope BusinessScope = new SAPPO_DocumentRequest.BusinessScope();
        SAPPO_DocumentRequest.BusinessTransactionDocumentID BusinessTransactionDocumentID = new SAPPO_DocumentRequest.BusinessTransactionDocumentID();
        SAPPO_DocumentRequest.DocumentCreateMultipleSyncResponse DocumentCreateMultipleSyncResponse = new SAPPO_DocumentRequest.DocumentCreateMultipleSyncResponse();
        SAPPO_DocumentRequest.BusinessTransactionDocumentReference_InternalID BusinessTransactionDocumentReference_InternalID = new SAPPO_DocumentRequest.BusinessTransactionDocumentReference_InternalID();
        SAPPO_DocumentRequest.AttachmentID AttachmentID = new SAPPO_DocumentRequest.AttachmentID();
        SAPPO_DocumentRequest.DocumentCreateRequestDocument DocumentCreateRequestDocument = new SAPPO_DocumentRequest.DocumentCreateRequestDocument();
        SAPPO_DocumentRequest.PropertyID PropertyID = new SAPPO_DocumentRequest.PropertyID();
        SAPPO_DocumentRequest.DocumentCreateResponseDocument DocumentCreateResponseDocument = new SAPPO_DocumentRequest.DocumentCreateResponseDocument();
        SAPPO_DocumentRequest.PropertyReference PropertyReference = new SAPPO_DocumentRequest.PropertyReference();
        SAPPO_DocumentRequest.PropertyValuation PropertyValuation = new SAPPO_DocumentRequest.PropertyValuation();
        SAPPO_DocumentRequest.PartyStandardID PartyStandardID = new SAPPO_DocumentRequest.PartyStandardID();
        SAPPO_DocumentRequest.UserAccountID UserAccountID = new SAPPO_DocumentRequest.UserAccountID();
        SAPPO_DocumentRequest.DocumentCreateRequestDocumentingParty DocumentCreateRequestDocumentingParty = new SAPPO_DocumentRequest.DocumentCreateRequestDocumentingParty();
        SAPPO_DocumentRequest.BusinessDocumentSystemAdministrativeData BusinessDocumentSystemAdministrativeData = new SAPPO_DocumentRequest.BusinessDocumentSystemAdministrativeData();
        SAPPO_DocumentRequest.DocumentCreateRequestContent DocumentCreateRequestContent = new SAPPO_DocumentRequest.DocumentCreateRequestContent();
        SAPPO_DocumentRequest.MEDIUM_Name MEDIUM_Name = new SAPPO_DocumentRequest.MEDIUM_Name();
        SAPPO_DocumentRequest.PartyInternalID PartyInternalID = new SAPPO_DocumentRequest.PartyInternalID();
        SAPPO_DocumentRequest.BusinessScopeInstanceID BusinessScopeInstanceID = new SAPPO_DocumentRequest.BusinessScopeInstanceID();
        SAPPO_DocumentRequest.BusinessScopeID BusinessScopeID = new SAPPO_DocumentRequest.BusinessScopeID();
        SAPPO_DocumentRequest.HTTPS_Port HTTPS_Port = new SAPPO_DocumentRequest.HTTPS_Port();

        /* DOCUMENTS */

        SAPPO_Document.DocumentReadRequest DocumentReadRequest = new SAPPO_Document.DocumentReadRequest();
        SAPPO_Document.ArchiveDocument ArchiveDocument = new SAPPO_Document.ArchiveDocument();
        SAPPO_Document.DocumentReadRequestDocumentingParty DocumentReadRequestDocumentingParty = new SAPPO_Document.DocumentReadRequestDocumentingParty();
        SAPPO_Document.ObjectReference ObjectReference = new SAPPO_Document.ObjectReference();
        SAPPO_Document.DocumentReadResponse DocumentReadResponse = new SAPPO_Document.DocumentReadResponse();
        SAPPO_Document.ArchiveDocuments ArchiveDocuments = new SAPPO_Document.ArchiveDocuments();
        SAPPO_Document.ObjectReferences ObjectReferences = new SAPPO_Document.ObjectReferences();
        SAPPO_Document.ArchivePartners ArchivePartners = new SAPPO_Document.ArchivePartners();
        SAPPO_Document.ArchivePartner ArchivePartner = new SAPPO_Document.ArchivePartner();
        SAPPO_Document.ArchiveObject ArchiveObject = new SAPPO_Document.ArchiveObject();


    }

    static testMethod void CoverageESDMT(){
        SAPPO_EsmEdt.PropertyValueDateSpecification PropertyValueDateSpecification = new SAPPO_EsmEdt.PropertyValueDateSpecification();
        SAPPO_EsmEdt.BusinessDocumentMessageHeader BusinessDocumentMessageHeader = new SAPPO_EsmEdt.BusinessDocumentMessageHeader();
        SAPPO_EsmEdt.BusinessDocumentMessageID BusinessDocumentMessageID = new SAPPO_EsmEdt.BusinessDocumentMessageID();
        SAPPO_EsmEdt.BusinessScopeInstanceID BusinessScopeInstanceID = new SAPPO_EsmEdt.BusinessScopeInstanceID();
        SAPPO_EsmEdt.BusinessScope BusinessScope = new SAPPO_EsmEdt.BusinessScope();
        SAPPO_EsmEdt.BusinessScopeID BusinessScopeID = new SAPPO_EsmEdt.BusinessScopeID();
        SAPPO_EsmEdt.Identifier Identifier = new SAPPO_EsmEdt.Identifier();
        SAPPO_EsmEdt.PartyInternalID PartyInternalID = new SAPPO_EsmEdt.PartyInternalID();
        SAPPO_EsmEdt.PartyStandardID PartyStandardID = new SAPPO_EsmEdt.PartyStandardID();
        SAPPO_EsmEdt.PersonName PersonName = new SAPPO_EsmEdt.PersonName();
        SAPPO_EsmEdt.PriceComponent PriceComponent = new SAPPO_EsmEdt.PriceComponent();
        SAPPO_EsmEdt.Log Log = new SAPPO_EsmEdt.Log();
        SAPPO_EsmEdt.Rate Rate = new SAPPO_EsmEdt.Rate();
        SAPPO_EsmEdt.LogItem LogItem = new SAPPO_EsmEdt.LogItem();
        SAPPO_EsmEdt.PropertyValueNumericSpecification PropertyValueNumericSpecification = new SAPPO_EsmEdt.PropertyValueNumericSpecification();
        SAPPO_EsmEdt.PropertyValue PropertyValue = new SAPPO_EsmEdt.PropertyValue();
        SAPPO_EsmEdt.PropertyDefinitionClassReference PropertyDefinitionClassReference = new SAPPO_EsmEdt.PropertyDefinitionClassReference();
        SAPPO_EsmEdt.PropertyValuationValueGroup PropertyValuationValueGroup = new SAPPO_EsmEdt.PropertyValuationValueGroup();
        SAPPO_EsmEdt.UnloadingPoint UnloadingPoint = new SAPPO_EsmEdt.UnloadingPoint();
        SAPPO_EsmEdt.PropertyValueTextSpecification PropertyValueTextSpecification = new SAPPO_EsmEdt.PropertyValueTextSpecification();
        SAPPO_EsmEdt.CompanyName CompanyName = new SAPPO_EsmEdt.CompanyName();
        SAPPO_EsmEdt.PropertyValueIndicatorSpecification PropertyValueIndicatorSpecification = new SAPPO_EsmEdt.PropertyValueIndicatorSpecification();
        SAPPO_EsmEdt.Incoterms Incoterms = new SAPPO_EsmEdt.Incoterms();
        SAPPO_EsmEdt.PropertyValueTimeSpecification PropertyValueTimeSpecification = new SAPPO_EsmEdt.PropertyValueTimeSpecification();

    }
}