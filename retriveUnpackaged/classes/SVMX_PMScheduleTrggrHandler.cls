/*******************************************************************************************************
Description: Trigger handler for PM schedule Trigger

Dependancy:

Author: Ranjitha S
Date: 19-03-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/
public class SVMX_PMScheduleTrggrHandler extends SVMX_TriggerHandler{

    private list<SVMXC__PM_Schedule__c > newPMScheList;
    private list<SVMXC__PM_Schedule__c > oldPMScheList;
    private Map<Id, SVMXC__PM_Schedule__c > newPMScheMap;
    private Map<Id, SVMXC__PM_Schedule__c > oldPMScheMap;
     
    public SVMX_PMScheduleTrggrHandler() {
        this.newPMScheList = (list<SVMXC__PM_Schedule__c >) Trigger.new;
        this.oldPMScheList = (list<SVMXC__PM_Schedule__c >) Trigger.old;
        this.newPMScheMap = (Map<Id, SVMXC__PM_Schedule__c >) Trigger.newMap;
        this.oldPMScheMap = (Map<Id, SVMXC__PM_Schedule__c >) Trigger.oldMap;
    }

    public override void afterInsert(){
        Map<Date,SVMXC__PM_Schedule__c> DateSchedule = new Map<Date,SVMXC__PM_Schedule__c>();
        List<SVMXC__PM_Schedule_Definition__c> pmList = new List<SVMXC__PM_Schedule_Definition__c>();
        for(SVMXC__PM_Schedule__c pm: newPMScheList){
            DateSchedule.put(pm.SVMXC__Scheduled_On__c, pm);

        }

        if(DateSchedule.size() > 0){
             Integer count =1;
             for(Date pm: DateSchedule.keyset()){
                SVMXC__PM_Schedule_Definition__c pmsch = new SVMXC__PM_Schedule_Definition__c();
                pmsch.SVMXC__SM_Sequence__c = count;
                pmsch.id = DateSchedule.get(pm).SVMXC__PM_Schedule_Definition__c;
                pmList.add(pmsch);
                count++;

            }
        }

        if(pmList.size() > 0)
            update pmList;
        
    }
}