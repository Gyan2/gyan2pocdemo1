@isTest
public class SVMX_WorkDetailTrggrHandler_UT {

    static testMethod void invokeWorkDetailtrigger() { 
    
     SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'WorkOrder';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
        
        SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
              intg.name= 'SAP PO';
              intg.WS_USER__c ='test';
              intg.WS_PASS__c ='test';
        insert intg;

        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='Germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='Germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;    
        
         SVMX_Interface_Trigger_Configuration__c trigconfig4=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig4.name='trig1';
        trigconfig4.SVMX_Country__c='germany';
        trigconfig4.SVMX_Object__c='Parts Order -Spare Parts';
        trigconfig4.SVMX_Trigger_on_Field__c='SVMXC__Order_Status__c';
        insert trigconfig4;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Parts Order Line-Spare Parts';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig2;     
        
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
      
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
           
        Case cs = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', true);
       
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, true);
        
        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        SVMX_Rate_Tiers__c rt2 = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, true, true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Germany',true);
        
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Planned Services (PPM)','Global Std', sc.Id, tech.Id, cs.id, true);
        //SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Planned Services (PPM)','Global Std', sc.Id, tech.Id, cs.id, true);
        
        SVMXC__Service_Order_Line__c oli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',false);
        SVMXC__Service_Order_Line__c oli2 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Expenses','Goodwill',false);
        SVMXC__Service_Order_Line__c oli3 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Travel','Non Warranty',false);
        SVMXC__Service_Order_Line__c oli4 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Estimate', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',false);
        SVMXC__Service_Order_Line__c oli5 = SVMX_TestUtility.CreateWorkOrderDetailEstimate(acc.Id, loc.Id, prod.Id, 'Estimate', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',2,true,false);

      
        
        
        list<SVMXC__Service_Order_Line__c> oliList = new list<SVMXC__Service_Order_Line__c>();
        oliList.add(oli1);
        oliList.add(oli2);
        oliList.add(oli3);
        oliList.add(oli4);
        oliList.add(oli5);
        //oliList.add(oli6);
        
        test.startTest();
        insert oliList;
        oli1.SVMX_Billing_Type__c = 'Goodwill';
        oli1.SVMXC__Start_Date_and_Time__c = system.now()+1;
        oli2.SVMX_Billing_Type__c = 'Warranty';
        oli3.SVMXC__Start_Date_and_Time__c = system.now()+1;
        oli3.SVMXC__End_Date_and_Time__c = system.now()+2;
        list<SVMXC__Service_Order_Line__c> UpdateOliList = new list<SVMXC__Service_Order_Line__c>();
        
        UpdateOliList.add(oli1);
        UpdateOliList.add(oli2);
        UpdateOliList.add(oli3);
        
        update UpdateOliList;
        
         delete oli4;
        test.stopTest();
    } 
@istest(seealldata=true)
    static  void invokeworkdetailservicemanager() {
    
    list<SVMX_Integration_SAP_Endpoint__c> customSett = SVMX_Integration_SAP_Endpoint__c.getall().values();

      if(customSett.size() == 0){
        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'WorkOrder';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
        
        SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
              intg.name= 'SAP PO';
              intg.WS_USER__c ='test';
              intg.WS_PASS__c ='test';
        insert intg;
      } 

        List<SVMX_Interface_Trigger_Configuration__c> trigcon1 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Order'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon2 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Details'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon3 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order Line-Spare Parts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon4 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order -Spare Parts'];

      if(trigcon1.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
       } 
      if(trigcon2.size() == 0){  
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;
       }
        
       if(trigcon3.size() == 0){ 
        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Parts Order Line-Spare Parts';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig2;
      }

      if(trigcon4.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig3=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig3.name='trig1';
        trigconfig3.SVMX_Country__c='germany';
        trigconfig3.SVMX_Object__c='Parts Order -Spare Parts';
        trigconfig3.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig3;
      }
          
        
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',false);
        acc.Language__c ='EN';
        insert acc;
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,false);
        loc.SVMXC__Location_Type__c = 'External';
        loc.SVMXC__Stocking_Location__c = true;
        insert loc;
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
      
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
           
        Case cs = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', true);
       
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, true);
        
        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        SVMX_Rate_Tiers__c rt2 = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, true, true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Germany',true);

        ProductDetail__c produtdetail = new ProductDetail__c();
        produtdetail.Name='test1';
        insert produtdetail;
        
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Planned Services (PPM)','Global Std', sc.Id, tech.Id, cs.id, false);
        wo.SVMXC__Order_Type__c='Spare Parts Order';
        wo.SVMX_From_SAP_Storage_Location__c = loc.id;
        wo.SVMXC__City__c ='test';
        wo.SVMXC__Country__c='Norway';
        wo.SVMXC__State__c='test';
        wo.SVMXC__Zip__c='test';
        wo.SVMXC__Street__c='test';

            

        insert wo;
        //SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Planned Services (PPM)','Global Std', sc.Id, tech.Id, cs.id, true);
        
        SVMXC__Service_Order_Line__c oli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',false);
        oli1.SVMX_Create_Spare_Parts_Order__c =true;
        oli1.SVMXC__Estimated_Quantity2__c =3;
        oli1.SVMX_ProductDetail__c= produtdetail.Id;
        oli1.SVMXC__Estimated_Price2__c=88;
        oli1.SVMXC__Discount__c=4;
       
        //SVMXC__Service_Order_Line__c oli2 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Expenses','Goodwill',false);
        //SVMXC__Service_Order_Line__c oli3 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Travel','Non Warranty',false);
        //SVMXC__Service_Order_Line__c oli4 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Estimate', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',false);
        //SVMXC__Service_Order_Line__c oli5 = SVMX_TestUtility.CreateWorkOrderDetailEstimate(acc.Id, loc.Id, prod.Id, 'Estimate', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',2,true,false);

      
        
       /* 
        list<SVMXC__Service_Order_Line__c> oliList = new list<SVMXC__Service_Order_Line__c>();
        oliList.add(oli1);
        oliList.add(oli2);
        oliList.add(oli3);
        oliList.add(oli4);
        oliList.add(oli5);
        //oliList.add(oli6);*/
        
        test.startTest();

        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder salesOrderreq=new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt.CreateSync(messageheader,salesOrderreq);
        insert oli1;
        
        /*oli1.SVMX_Billing_Type__c = 'Goodwill';
        oli1.SVMXC__Start_Date_and_Time__c = system.now()+1;
        oli2.SVMX_Billing_Type__c = 'Warranty';
        oli3.SVMXC__Start_Date_and_Time__c = system.now()+1;
        oli3.SVMXC__End_Date_and_Time__c = system.now()+2;
        list<SVMXC__Service_Order_Line__c> UpdateOliList = new list<SVMXC__Service_Order_Line__c>();
        
        UpdateOliList.add(oli1);
        UpdateOliList.add(oli2);
        UpdateOliList.add(oli3);
        
        update UpdateOliList;
        
         delete oli4;*/
        test.stopTest();
    } 
}