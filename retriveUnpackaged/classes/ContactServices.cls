public with sharing class ContactServices {

    public static void updateRelatedAccounts(List<Contact> Records, Map<Id,Contact> OtherRecords){
        Map<Id, Account> accountsToUpdate = new Map<Id, Account>();

        if (OtherRecords == null) {
            for (Contact forContact : Records) {
                if (forContact.AccountId != null && !accountsToUpdate.containsKey(forContact.AccountId)) {
                    accountsToUpdate.put(forContact.AccountId, new Account(Id = forContact.AccountId));
                }
            }
        } else {
            for (Contact forContact : Records) {
                Contact forOtherRecord = OtherRecords.get(forContact.Id);
                if (forContact.AccountId != forOtherRecord.AccountId) {
                    if (forContact.AccountId != null && !accountsToUpdate.containsKey(forContact.AccountId)) {
                        accountsToUpdate.put(forContact.AccountId, new Account(Id = forContact.AccountId));
                    }
                    if (forOtherRecord.AccountId != null && !accountsToUpdate.containsKey(forOtherRecord.AccountId)) {
                        accountsToUpdate.put(forOtherRecord.AccountId, new Account(Id = forOtherRecord.AccountId));
                    }
                }
            }
        }

        update accountsToUpdate.values();
    }

    public static void applyInternationalPhonePrefixDefaults(List<Contact> paramContacts) {
        for (Contact forContact : paramContacts) {
            if (String.isBlank(forContact.PhoneCountryCode__c)) {
                forContact.PhoneCountryCode__c = forContact.MailingCountryCode;
            }
            if (String.isBlank(forContact.MobilePhoneCountryCode__c)) {
                forContact.MobilePhoneCountryCode__c = forContact.MailingCountryCode;
            }
        }
    }

    public static void splitInternationalPrefix(List<Contact> paramContacts) {
        for (Contact forContact : paramContacts) {
            if (!String.isBlank(forContact.PhoneCountryCode__c) && !String.isBlank(forContact.Phone)) {
                forContact.PhoneWithoutInternationalPrefix__c = forContact.Phone.removeStart(Util.Countries.getByCode(forContact.PhoneCountryCode__c).InternationalPhonePrefix__c).trim();
            }

            if (!String.isBlank(forContact.MobilePhoneCountryCode__c) && !String.isBlank(forContact.MobilePhone)) {
                forContact.MobilePhoneWithoutInternationalPrefix__c = forContact.MobilePhone.removeStart(Util.Countries.getByCode(forContact.MobilePhoneCountryCode__c).InternationalPhonePrefix__c).trim();
            }
        }
    }

    public static void validateInternationalPhonePrefix(List<Contact> paramContacts) {

        Set<String> countryCodes = new Set<String>();
        for (Contact forContact : paramContacts) {
            countryCodes.add(forContact.PhoneCountryCode__c);
            countryCodes.add(forContact.MobilePhoneCountryCode__c);
            //forAccount.addError('testing');
        }

        Map<String,String> phonePrefixMapping = new Map<String, String>(); // Map<{Country Code} , {InternationalPhonePrefix} >
        for (Country__mdt forCountry : [SELECT DeveloperName, InternationalPhonePrefix__c FROM Country__mdt WHERE DeveloperName IN :countryCodes]) {
            phonePrefixMapping.put(forCountry.DeveloperName, forCountry.InternationalPhonePrefix__c);
        }

        for (Contact forContact : paramContacts) {
            String forPhonePrefix = phonePrefixMapping.get(forContact.PhoneCountryCode__c);
            if (forPhonePrefix != null && forContact.Phone != null) { // if there is a validation for that phone country code
                if (!forContact.Phone.startsWith(forPhonePrefix)) {
                    forContact.Phone.addError(String.format('Must start with {0}', new List<String>{forPhonePrefix}));
                }
            }

            String forMobilePhonePrefix = phonePrefixMapping.get(forContact.MobilePhoneCountryCode__c);
            if (forMobilePhonePrefix != null && forContact.MobilePhone != null) {
                if (!forContact.MobilePhone.startsWith(forMobilePhonePrefix)) {
                    forContact.MobilePhone.addError(String.format('Must start with {0}', new List<String>{forMobilePhonePrefix}));
                }
            }
        }
    }
}