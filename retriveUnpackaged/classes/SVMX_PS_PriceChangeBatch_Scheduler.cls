global class SVMX_PS_PriceChangeBatch_Scheduler implements Schedulable {

    global void execute(SchedulableContext sc) {

	    SVMX_PartsPriceChangeHandler_Batch ppcBatch = new SVMX_PartsPriceChangeHandler_Batch();
	    Database.executebatch(ppcBatch, 100);
    }

}