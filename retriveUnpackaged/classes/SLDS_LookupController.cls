/**
 * Created by Jai Chaturvedi.
 */

public class SLDS_LookupController {

    /* Method to query records using SOSL*/
    @AuraEnabled
    public static String search(String objectAPIName, String searchText,
            List<String> whereClause, List<String> extrafields, Integer queryLimit){

        objectAPIName = String.escapeSingleQuotes(objectAPIName);
        searchText = String.escapeSingleQuotes(searchText);
        String searchQuery = 'FIND \'' + searchText + '*\' IN ALL FIELDS RETURNING ' + objectAPIName + '(Id,Name' ;
        if(!extrafields.isEmpty()){
            searchQuery = searchQuery + ',' + String.join(extrafields, ',') ;
        }
        system.debug(whereClause);
        if(!whereClause.isEmpty()){
            searchQuery = searchQuery + ' WHERE ' ;
            searchQuery = searchQuery + String.join(whereClause, 'AND') ;
        }
        searchQuery = searchQuery + (queryLimit == null?' LIMIT 5':' LIMIT ' + Integer.valueOf(queryLimit)) + ' ) ';
        system.debug(searchQuery);
        return JSON.serializePretty(search.query(searchQuery)) ;
    }

    /* Method to query records using SOQL*/
    @AuraEnabled
    public static List<SObject> getRecentlyViewed(
            String objectAPIName,
            List<String> whereClause,
            List<String> extrafields,
    		Integer queryLimit){

        String searchQuery = 'SELECT Id, Name';
        if(!extrafields.isEmpty()){
            searchQuery = searchQuery + ',' + String.join(extrafields, ',') ;
        }
        searchQuery = searchQuery + ' FROM ' + objectAPIName + ' WHERE LastViewedDate != NULL ';
        if(!whereClause.isEmpty()){
            searchQuery = searchQuery + ' AND ' ;
            searchQuery = searchQuery + String.join(whereClause, 'AND') ;
            system.debug(searchQuery);
        }
        searchQuery = searchQuery + ' ORDER BY LastViewedDate DESC '  + (queryLimit == null?' LIMIT 5':'LIMIT ' + Integer.valueOf(queryLimit));
        List<SObject> objectList =  new List<SObject>();
        objectList = Database.query(searchQuery);
        return objectList;
    }

    /* Method to query records using SOQL*/
    @AuraEnabled
    public static List<SObject> getSpecific(
            String objectAPIName,
            String selectedId,
            List<String> extrafields,
    		Integer queryLimit){

        String searchQuery = 'SELECT Id, Name';
        if(!extrafields.isEmpty()){
            searchQuery = searchQuery + ',' + String.join(extrafields, ',') ;
        }
        searchQuery = searchQuery + ' FROM ' + objectAPIName + ' WHERE Id = \''+selectedId+'\'';
        searchQuery = searchQuery + ' ORDER BY LastViewedDate DESC ' + (queryLimit == null?' LIMIT 5':'LIMIT ' + Integer.valueOf(queryLimit)) ;
        List<SObject> objectList =  new List<SObject>();
        objectList = Database.query(searchQuery);
        return objectList;
    }
}