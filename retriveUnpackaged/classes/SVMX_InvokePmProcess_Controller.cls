/*******************************************************************************************************
Description:
  This class is used to invoke PM Process 
 
Author: Pooja Singh
Date: 08-01-2018

Modification Log: 
Date              Author          Modification Comments
--------------------------------------------------------------
16 Jan 2018       Pooja Singh    Added convertPMPlans method which converts the PM plans of coverage type product.
                                 Added createProductServicedLines method which creates product serviced lines on the PM work orders
*******************************************************************************************************/

public class SVMX_InvokePmProcess_Controller{
    public void invokePMProcess(){
        SVMXC__ServiceMax_Processes__c PMProcessRecId=[select id from SVMXC__ServiceMax_Processes__c where SVMXC__ProcessID__c=:'SVMX Location based preventive maintenance plan' limit 1];
        SVMXC.PREV_Engine_Batch EngObj = new SVMXC.PREV_Engine_Batch();
        EngObj.setProcessIds.add(PMProcessRecId.Id);
        ID Batch = database.executebatch(EngObj, 1);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM,'PM Process is successfully triggered.'));
    }
    
    public void convertPMPlans(){
        SVMX_ActivatedContractsPmPlanBatch b = new SVMX_ActivatedContractsPmPlanBatch();
        database.executebatch(b,10);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM,'Batch for converting PM plans of coverage type product is triggered.'));
    }
    
    public void createProductServicedLines(){
        SVMX_PM_WorkOrderBatch b = new SVMX_PM_WorkOrderBatch(); 
        database.executebatch(b,10);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM,'Batch for creating product service lines for the work orders is triggered.'));
    }
    
     public void ActivityBasedBilling(){
        SVMX_ActivityPMBillingBatch b = new SVMX_ActivityPMBillingBatch(); 
        database.executebatch(b,10);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM,'Activity based billing batch is triggered.'));
    }
    
      public void AutoVerificationBatch(){
        SVMX_UpdateWorkOrderStatusBatch b = new SVMX_UpdateWorkOrderStatusBatch(); 
        database.executebatch(b,10);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM,'Batch for updating work orders status is triggered.'));
    }

     public void processInstallationTypeWorkOrders(){
        SVMX_InstallationWorkOrderHandler_Batch b = new SVMX_InstallationWorkOrderHandler_Batch();
        database.executebatch(b,10);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM,'Batch for processing Installation Type Work Orders is triggered.'));
     }

     public void processLaborTravelExpenseLinesToSAP(){
        SVMX_LaborTravelExpenseHandler_Batch b = new SVMX_LaborTravelExpenseHandler_Batch();
        database.executebatch(b,10);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM,'Batch for processing Labor, Travel and Expense Lines to SAP is triggered.'));
     }

     public void processPartLinesToSAP(){
        SVMX_PartsPriceChangeHandler_Batch b = new SVMX_PartsPriceChangeHandler_Batch();
        database.executebatch(b,10);
        ApexPages.addMessage(new ApexPages.message(ApexPages.Severity.CONFIRM,'Batch for processing Parts Lines to SAP is triggered.'));
     }
    
}