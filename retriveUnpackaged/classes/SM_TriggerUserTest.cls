/*******************************************************************************************************
* Class Name     	: SM_TriggerUserTest
* Description		: Test class for Trigger Handler
* Author          	: Paul Carmuciano
* Created On      	: 2017-07-10
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-07-10         1000                 Initial version
******************************************************************************************************/
@isTest
public class SM_TriggerUserTest {

    @testSetup
    public static void setupData() {
        
    }

    /***
* Method name	: bulkTest
* Description	: Can test as admin user on User object
* Author		: Paul Carmuciano
* Return Type	: n/a
* Parameter		: n/a
*/
    @isTest
    public static void bulkTest() {
        // Arrange

        Map<Id,Profile> profiles = DM004_User.getCustomProfiles(); // don't use it, but could

        // get metadata
        Map<String,Management_Segmentation__mdt> ManagementSegmentationMapping = new Map<String,Management_Segmentation__mdt>();
        ManagementSegmentationMapping = MDM001_ManagementSegmentation.ManagementSegmentationMapping;

        // contextualise to a target so we can assert the result
        Management_Segmentation__mdt targetManagementSegmentationMapping = ManagementSegmentationMapping.values()[0];

        // make 200 users
        List<User> bulkUsers = new List<User>();
        for (Integer i=0; i<=200; i++) {
            User u = DM004_User.createRandomUser(null, new List<Id>(profiles.keySet())[0], 'firstName', 'lastName', false);
            u.UserCountry__c = targetManagementSegmentationMapping.DeveloperName;
            bulkUsers.add(u);
        }

        // Act
        Test.startTest();

        // cover insert
        insert bulkUsers;
        
        // cover update
        for (User u : bulkUsers ) {
            u.UserCountry__c = ManagementSegmentationMapping.values()[ManagementSegmentationMapping.size()-1].DeveloperName;
        }
        update bulkUsers;

        // can't cover delete / undelete on user

        Test.stopTest();
        

        // Assert true (we are not testing any logic here, only bulk support)
        system.assert(true);
        
    }
    

}