public without sharing class Util {
    public static RecordTypes RecordTypes {get {if (RecordTypes == null)RecordTypes = new RecordTypes(); return RecordTypes;} private set;}
    public static SalesOrganizationMetaData SalesOrganizationMetaData {get {if (SalesOrganizationMetaData == null) SalesOrganizationMetaData = new SalesOrganizationMetaData(); return SalesOrganizationMetaData;} private set;}
    public static Describe Describe {get {if (Describe == null) Describe = new Describe(); return Describe;} private set;}
    public static User CurrentUserRecord {get {if (CurrentUserRecord == null) CurrentUserRecord = [SELECT UserCountry__c, UserRegion__c, UserSegment__c, SalesOrg__c FROM User WHERE Id = :UserInfo.getUserId()]; return CurrentUserRecord;} private set;}
    public static Organization Org {get {if (Org == null) Org = [SELECT IsSandbox FROM Organization ]; return Org;} private set;}
    //public static UtilDeDuplication DeDuplication {get {if (DeDuplication == null) DeDuplication = new UtilDeDuplication(); return DeDuplication;} private set;}
    public static UtilPricebook UtilPricebook {get {if (UtilPricebook == null) UtilPricebook = new UtilPricebook(); return UtilPricebook;} private set;}
    public static UtilDependentPicklist UtilDependentPicklist {get {if (UtilDependentPicklist == null) UtilDependentPicklist = new UtilDependentPicklist(); return UtilDependentPicklist;} private set;}
    public static Countries Countries {get {if (Countries == null) Countries = new Countries(); return Countries;} private set;}
    public static String SandboxName {get {
        if (SandboxName == null) {
            if (!Org.IsSandbox) SandboxName = '';
            else if (Test.isRunningTest()) SandboxName = 'UNITTESTING';
            else {
                System.debug(URL.getSalesforceBaseUrl().toExternalForm());
                SandboxName = URL.getSalesforceBaseUrl().toExternalForm().substringBetween('https://dormakaba--','.');
                String alternativeForManaged = Url.getSalesforceBaseUrl().toExternalForm().substringBetween('https://dormakaba--','--');
                if (alternativeForManaged != null && alternativeForManaged.length() < SandboxName.length()) SandboxName = alternativeForManaged;
                SandboxName = SandboxName.toUpperCase();
            }
        }
        //sandboxName = 'DEV';
        System.debug(sandboxName);
        return sandboxName;
    } private set;}
    public static IntegrationEndpoints__c IntegrationEndpoints {get {
        if (IntegrationEndpoints == null) {
            String prefix = Util.Org.IsSandbox?(Util.SandboxName + '_'):'';
            IntegrationEndpoints = IntegrationEndpoints__c.getInstance();
            for (String forField : IntegrationEndpoints.getPopulatedFieldsAsMap().keySet()) {
                if (forField.endsWith('__c')) {
                    IntegrationEndpoints.put(forField, String.format((String)IntegrationEndpoints.get(forField),new List<String>{prefix}));
                }
            }
        }

        System.debug(IntegrationEndpoints);
        return IntegrationEndpoints;
    } private set;}

    public class RecordTypes {
        Map<String, RecordType> developerNameToRecordTypeMap = new Map<String, RecordType>();
        Map<Id, RecordType> allRecordTypes;
        public RecordTypes() {
            allRecordTypes = new Map<Id, RecordType>([SELECT Name, DeveloperName, NamespacePrefix, SobjectType FROM RecordType]);
            for (RecordType forRecordType : allRecordTypes.values()) {
                String prefix = forRecordType.SobjectType + ':';
                if (!String.isBlank(forRecordType.NamespacePrefix)) {
                    prefix += forRecordType.NamespacePrefix + '__';
                }
                developerNameToRecordTypeMap.put((prefix + forRecordType.DeveloperName).toUpperCase(), forRecordType);
            }
            System.debug(developerNameToRecordTypeMap);
        }

        public RecordType get(String paramDeveloperName) {
            return developerNameToRecordTypeMap.get(paramDeveloperName.toUpperCase());
        }

        public RecordType get(Id paramId) {
            return allRecordTypes.get(paramId);
        }

        public String getCombinedDeveloperName(Id paramId) {
            RecordType rt = allRecordTypes.get(paramId);
            if (rt != null) {
                return rt.SobjectType + ':' + (String.isBlank(rt.NamespacePrefix)?'':rt.NamespacePrefix + '__') + rt.DeveloperName;
            } else {
                return null;
            }
        }

        public String getDeveloperName(Id paramId) {
            RecordType rt = allRecordTypes.get(paramId);
            if (rt != null) {
                return (String.isBlank(rt.NamespacePrefix)?'':rt.NamespacePrefix + '__') + rt.DeveloperName;
            } else {
                return null;
            }
        }
    }

    public class SalesOrganizationMetaData {
        public String getCountryCodeBySalesOrgId(String paramSalesOrgId) {
            String returned;
            if (paramSalesOrgId != null) {
                // don't be scared, Metadata SOQL do not count against limits, as they are cached.
                List<SalesOrganizationData__mdt> salesOrgs = [SELECT CountryCode__c FROM SalesOrganizationData__mdt WHERE MasterLabel = :paramSalesOrgId];

                if (salesOrgs.size() == 1) {
                    returned = salesOrgs.get(0).CountryCode__c;
                } else if (salesOrgs.isEmpty()) {
                    throw new SalesOrganizationMetaDataException('SALES_ORGANIZATION_NOT_FOUND');
                } else {
                    throw new SalesOrganizationMetaDataException('MORE_THAN_ONE_SALES_ORGANIZATION_FOUND');
                }

            }
            return returned;
        }
    }

    public class SalesOrganizationMetaDataException extends Exception {}

    public without sharing class Describe {

        Map<String, Schema.sObjectType> globalDescribe = Schema.getGlobalDescribe();
        Map<String, Schema.DescribeSObjectResult> sObjectsDescribe = new Map<String, Schema.DescribeSObjectResult>();
        Map<String, Map<String, Schema.DescribeFieldResult>> fieldsDescribe = new Map<String, Map<String, Schema.DescribeFieldResult>>();

        public Schema.DescribeFieldResult getField(String paramSObjectName, String paramField) {

            if (sObjectsDescribe.get(paramSObjectName) == null) {
                sObjectsDescribe.put(paramSObjectName, globalDescribe.get(paramSObjectName).getDescribe());
                fieldsDescribe.put(paramSObjectName, new Map<String, Schema.DescribeFieldResult>());
            }

            Schema.DescribeFieldResult returned;
            if (paramField.contains('.')) {// if it has a relationship
                String[] fieldSplit = paramField.split('\\.',2);

                if (fieldSplit[0].endsWith('__r')) {
                    fieldSplit[0] = fieldSplit[0].replace('__r','__c');
                } else {
                    fieldSplit[0] += 'Id';
                }

                Schema.DescribeFieldResult relationshipDescribeResult = fieldsDescribe.get(paramSObjectName).get(fieldSplit[0]);

                if (relationshipDescribeResult == null) {
                    relationshipDescribeResult = sObjectsDescribe.get(paramSObjectName).fields.getMap().get(fieldSplit[0]).getDescribe();
                    fieldsDescribe.get(paramSObjectName).put(fieldSplit[0], relationshipDescribeResult);
                }

                returned = getField(String.valueOf(relationshipDescribeResult.getReferenceTo()[0]), fieldSplit[1]);

            } else {
                returned = fieldsDescribe.get(paramSObjectName).get(paramField);
                if (returned == null) {
                    returned = sObjectsDescribe.get(paramSObjectName).fields.getMap().get(paramField).getDescribe();
                    fieldsDescribe.get(paramSObjectName).put(paramField, returned);
                }
            }

            return returned;
        }
    }

    public static Object getValue(Map<String,Object> obj, String fieldsToCheck){
        List<String> fields = new List<String>();

        if(fieldsToCheck.split('\\.').size() > 0){
            fields.addAll(fieldsToCheck.split('\\.'));
        }else{
            fields.add(fieldsToCheck);
        }

        String field = fields.remove(0);

        if(fields.size() == 0){
            return obj.containsKey(field) ? obj.get(field) : null;
        }else{
            if((obj.containsKey(field) && obj.get(field) != null)){
                return getValue((Map<String,Object>)obj.get(field),String.join(fields,'.'));
            }else{
                return null;
            }

        }

    }

    public class Countries {

        public Country__mdt getByCode(String paramCode) {
            List<Country__mdt> countriesList = [SELECT DeveloperName, InternationalPhonePrefix__c FROM Country__mdt WHERE DeveloperName = :paramCode];
            if (countriesList.isEmpty()) {
                return null;
            } else {
                return countriesList.get(0);
            }
        }
    }


}