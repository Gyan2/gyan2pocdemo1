public with sharing class LCC001_Generic_Hierarchy_Controller {
    
    @AuraEnabled public List<sObject> hierarchyData;
    @AuraEnabled public List<String> columnLabels;
    
    @AuraEnabled 
    public static LCC001_Generic_Hierarchy_Controller fetchHierarchyData (String[] columns,String recordId, String ObjectAPIName, String LookupAPIName, String FormulaAPIName){ 
        LCC001_Generic_Hierarchy_Controller controllerData = new LCC001_Generic_Hierarchy_Controller();
        controllerData.columnLabels = LCC001_Generic_Hierarchy_Controller.getColumnLabels(ObjectAPIName, columns);
        String masterParentId = LCC001_Generic_Hierarchy_Controller.getMasterParentId(recordId,ObjectAPIName,FormulaAPIName);
        if(controllerData.columnLabels != null && masterParentId != null){
            controllerData.hierarchyData = Database.query(LCC001_Generic_Hierarchy_Controller.generateQueryString(masterParentId,LookupAPIName,FormulaAPIName,ObjectAPIName,columns));	
        }else{
            controllerData = null;
        }
        return controllerData;    
    }
    
    public static String getMasterParentId(String recordId, String ObjectAPIName, String FormulaAPIName){
        try{
            String query = 'SELECT Id, ' + FormulaAPIName;
            query += ' FROM ' + ObjectAPIName;
            query += ' WHERE Id=\'' + recordId + '\'';
            sObject currentObject = Database.query(query);
            return (String) currentObject.get(FormulaAPIName);
        }catch(Exception ex){
            System.debug(ex.getMessage());
            return null;
        }      
    }
    
    public static List<String> getColumnLabels(String ObjectAPIName, String[] columns){
        try{
            List<String> labels = new List<String>(); 
            Map<String,Schema.SObjectField> fieldsMap = Schema.getGlobalDescribe().get(ObjectAPIName).getDescribe().fields.getMap();
            for(String s : columns){
                String labelString = fieldsMap.get(s).getDescribe().getLabel();
                labels.add(labelString);
            }
            return labels;
        }catch(Exception ex){
            System.debug(ex.getMessage());
            return null;
        }
    }
    
    public static String generateQueryString(String masterParentId, String LookupAPIName, String FormulaAPIName,String ObjectAPIName, String[] columns){
        String toReturn = 'SELECT Id,' + LookupAPIName + ',' + FormulaAPIName;
        for(String s : columns){
            toReturn += ',' + s;
        } 
        toReturn += ' FROM ' + ObjectAPIName;
        toReturn += ' WHERE ' + FormulaAPIName;
        toReturn += '=\'' + masterParentId + '\'';
        return toReturn;
    }  
}