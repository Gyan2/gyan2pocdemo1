public abstract without sharing class MirrorService {
    protected String sourceType;
    protected String targetType;
    protected Map<String,String> mapping;
    protected Map<String,String> recordTypeMapping;
    protected MirrorService(String paramSourceType, String paramTargetType){
        /*sourceType = paramSourceType;
        targetType = paramTargetType;

        mapping = new Map<String,String>();
        recordTypeMapping = new Map<String,String>();
        for (MirrorEngineMapping__c forFieldMapping : [
            SELECT
                SObjectA__c, FieldA__c,
                SObjectB__c, FieldB__c,
                MappingType__c
            FROM MirrorEngineMapping__c
            WHERE
                ( SObjectA__c = :sourceType OR SObjectB__c = :sourceType ) AND
                ( SObjectA__c = :targetType OR SObjectB__c = :targetType )
        ]){
            Map<String,String> associatedMapping;
            if (forFieldMapping.MappingType__c == 'Field') {
                associatedMapping = mapping;
            } else if (forFieldMapping.MappingType__c == 'RecordType') {
                associatedMapping = recordTypeMapping;
            } else {
                associatedMapping = new Map<String,String>(); // Ignore Mapping
            }

            if (forFieldMapping.SObjectA__c == sourceType) {
                associatedMapping.put(forFieldMapping.FieldA__c, forFieldMapping.FieldB__c);
            } else {
                associatedMapping.put(forFieldMapping.FieldB__c, forFieldMapping.FieldA__c);
            }
        }*/

        sourceType = paramSourceType;
        targetType = paramTargetType;

        mapping = new Map<String, String>();
        List<MirrorEngineSObject__mdt> mirrorEngines = [
            SELECT
                SObjectA__c,
                SObjectB__c

            FROM MirrorEngineSObject__mdt
            WHERE
                SObjectA__c = :sourceType AND SObjectB__c = :targetType
        ];
        if (mirrorEngines.isEmpty()) {
            mirrorEngines = [
                SELECT
                    SObjectA__c,
                    SObjectB__c
                FROM MirrorEngineSObject__mdt
                WHERE
                    SObjectA__c = :targetType AND SObjectB__c = :sourceType
            ];
        }
        /* Yes I know, shitty work-around, but the where clause does NOT Support OR statement, it throws Disjunctions not supported */

        if (mirrorEngines.isEmpty()) throw new MirrorServiceMissingConfigurationException();

        MirrorEngineSObject__mdt mirrorEngine = mirrorEngines.get(0);


        for (MirrorEngineFieldMapping__mdt forFieldMapping : [  SELECT FieldA__c, FieldB__c, MappingType__c, Direction__c
                                                                FROM MirrorEngineFieldMapping__mdt
                                                                WHERE MirrorEngineSObjectRef__c = :mirrorEngine.Id]){
            Map<String,String> associatedMapping;
            if (forFieldMapping.MappingType__c == 'Field') {
                associatedMapping = mapping;
            } else if (forFieldMapping.MappingType__c == 'RecordType') {
                associatedMapping = recordTypeMapping;
            } else {
                associatedMapping = new Map<String,String>(); // Ignore Mapping
            }

            if (mirrorEngine.SObjectA__c == sourceType && (forFieldMapping.Direction__c == 'ALL' || forFieldMapping.Direction__c == 'A2B')) {
                associatedMapping.put(forFieldMapping.FieldA__c, forFieldMapping.FieldB__c);
            } else if (mirrorEngine.SObjectB__c == sourceType && (forFieldMapping.Direction__c == 'ALL' || forFieldMapping.Direction__c == 'B2A')){
                associatedMapping.put(forFieldMapping.FieldB__c, forFieldMapping.FieldA__c);
            }
        }


    }
    public void doMirroring(List<sObject> paramSource, List<sObject> paramTarget) {
        if (paramSource.size() != paramTarget.size()) {
            throw new MirrorServicePreconditionException('DirectMirror.doMirroring needs 2 lists of same length. ' + paramSource.size() + ' vs ' + paramTarget.size());
        }

        Integer i = 0;
        for (sObject forRecordSource : paramSource) {
            sObject forRecordTarget = paramTarget.get(i++);
            for (String forField : mapping.keySet()) {
                if (forField == 'RecordTypeId') {
                    try {
                        forRecordTarget.put('RecordTypeId', Util.RecordTypes.get(
                            targetType + 
                            ':' + 
                            recordTypeMapping.get(
                                Util.RecordTypes.getDeveloperName((Id)forRecordSource.get('RecordTypeId'))
                            )
                        ).Id);
                    } catch (NullPointerException e) {
                        throw new MirrorServiceWrongRecordTypeMappingException();
                    }
                } else {
                    forRecordTarget.put(mapping.get(forField),forRecordSource.get(forField));        
                }
            }
        }
    }

    /*public List<Boolean> checkChanges(List<sObject> paramNew, List<sObject> paramOld) { // It assumes both Lists to be a <source> sObjectType
        List<Boolean> returned = new List<Boolean>();
        Integer i = 0;
        for (sObject forRecord : paramNew) {
            Boolean diff = false;
            sObject forCompareTo = paramOld.get(i++);
            for (String forField : mapping.keySet()) {
                if (forRecord.get(forField) != forCompareTo.get(forField)) {
                    diff = true;
                    break;
                }
            }
            returned.add(diff);
        }
        return returned;
    }*/

    public abstract List<sObject> doMirroring(List<sObject> paramSource, Schema.SObjectField paramField);


    public without sharing class DirectMirror extends MirrorService{
        public DirectMirror(String paramSourceType, String paramTargetType) {
            super(paramSourceType, paramTargetType);
        }

        public override List<sObject> doMirroring(List<sObject> paramSource, Schema.SObjectField paramField) {
            //Set<Id> paramSourceIds = new Map<Id,sObject>(paramSource).keySet();
            List<sObject> targets = new List<sObject>();
            Map<Id, sObject> targetMap = new Map<Id, sObject>();

            Schema.sObjectType targetSObjectType = Schema.getGlobalDescribe().get(targetType);
            for (sObject forSourceRecord : paramSource) {
                sObject target = targetSObjectType.newSObject();
                target.put(paramField, forSourceRecord.Id);
                targets.add(target);
            }

            doMirroring(paramSource, targets);
            return targets;
        }
    }

    /*public without sharing class IndirectMirror extends MirrorService {
        public IndirectMirror(String paramSourceType, String paramTargetType) {
            super(paramSourceType, paramTargetType);
        }

        public override List<sObject> doMirroring(List<sObject> paramSource, Schema.SObjectField paramField) {
            List<sObject> targets = new List<sObject>();
            Schema.sObjectType targetSObjectType = Schema.getGlobalDescribe().get(targetType);
            for (sObject forSourceRecord : paramSource) {
                sObject target = targetSObjectType.newSObject();
                target.put('Id',forSourceRecord.get(paramField));
                targets.add(target);
            }

            doMirroring(paramSource, targets);
            return targets;
        }
    }*/

    public abstract class MirrorServiceException extends Exception {}
    public class MirrorServiceMissingConfigurationException extends MirrorServiceException {}
    public class MirrorServicePreconditionException extends MirrorServiceException {}
    public class MirrorServiceWrongFieldMappingException extends MirrorServiceException{}
    public class MirrorServiceWrongRecordTypeMappingException extends MirrorServiceException{}
}


/**
/ *MirrorService ms = new MirrorService.DirectMirror('SVMXC__Site__c', 'Location__c');

SVMXC__Site__c source = [SELECT Name FROM SVMXC__Site__c LIMIT 1];

List<sObject> target = ms.doMirroring(new List<sObject>{source}, Location__c.fields.SVMXCSite__c);

System.debug(target); * /

Location__c location = new Location__c(Name='Testing 1 2 3');

insert location;

MirrorService ms = new MirrorService.IndirectMirror('Location__c', 'SVMXC__Site__c');

List<SVMXC__Site__c> toUpsert = new List<SVMXC__Site__c>();
toUpsert.addAll((List<SVMXC__Site__c>)ms.doMirroring(new List<sObject>{location}, Location__c.fields.SVMXCSite__c));
upsert toUpsert;


**/