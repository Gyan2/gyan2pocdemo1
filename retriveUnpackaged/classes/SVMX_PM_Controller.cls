/*******************************************************************************************************
Description: Controller class for SVMX_PM_WorkOrderBatch to create work detail lines of type products serviced
 
Author: Pooja Singh
Date: 20-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
22/12/2017    Pooja Singh    Added createPmCoverage method to create PM Coverages for the PM plans

*******************************************************************************************************/

public class SVMX_PM_Controller {
    public static void createProdServicedLines(Map<Id, Map<Id,List<Id>>> woIdLocIdIpIdListMap, Map<Id,Id> ipIdProdIdMap,Map<Id,Decimal> IPLocPriceMap, Map<Id,Decimal> IPLocDiscountMap,Map<id,Map<Id,SVMXC__Service_Contract_Products__c>> servicecontractcoverdProductMap){
        List<SVMXC__Service_Order_Line__c> wdInsertList = new List<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order__c> woUpdateList = new List<SVMXC__Service_Order__c>();
        //Record type query
        Id wdRecTypeId = [select Id from RecordType where SObjectType = 'SVMXC__Service_Order_Line__c' and DeveloperName = 'Products_Serviced'].Id;
        Map<id,SVMXC__Service_Order__c> workorderMap = new Map<id,SVMXC__Service_Order__c>([select id,name,SVMXC__Service_Contract__c from SVMXC__Service_Order__c where id in:woIdLocIdIpIdListMap.keySet()]);
        system.debug('workorderMap***'+workorderMap);
        for(Id woId : woIdLocIdIpIdListMap.keySet()){
            system.debug('woId'+woId);
            Map<Id,List<Id>> locIdIpIdListMap = woIdLocIdIpIdListMap.get(woId);
            Id locId = new List<Id>(locIdIpIdListMap.keySet())[0];
            List<Id> ipIdList = locIdIpIdListMap.get(locId);
            for(Id ipId : ipIdList){
                //Create work detail lines
                system.debug('ipId'+ipId);
                SVMXC__Service_Order_Line__c prodSerLine = new SVMXC__Service_Order_Line__c();
                prodSerLine.RecordTypeId = wdRecTypeId;
                prodSerLine.SVMXC__Service_Order__c = woId;
                prodSerLine.SVMXC__Serial_Number__c = ipId;
                /*CGTP-637 30 June 2018 Yan Anderson - defect raised by Sam requested that PM Work orders do 
                 *                                      not populate the product on work detail lines
                prodSerLine.SVMXC__Product__c = ipIdProdIdMap.get(ipId);*/
                prodSerLine.SVMXC__Billable_Quantity__c= 1;
                prodSerLine.SVMX_Chargeable_Qty__c =1;
                prodSerLine.SVMXC__Is_Billable__c =true;
                if(IPLocPriceMap.containsKey(ipid)){
                    prodSerLine.SVMXC__Billable_Line_Price__c = IPLocPriceMap.get(ipid);
                    prodSerLine.SVMXC__Actual_Price2__c = IPLocPriceMap.get(ipid);
                    if(IPLocDiscountMap.containsKey(ipid)){
                      prodSerLine.SVMX_Additional_Discount__c = IPLocDiscountMap.get(ipid);
                    }
                }
                system.debug('workorderMap.get(woId).SVMXC__Service_Contract__c'+workorderMap.get(woId).SVMXC__Service_Contract__c);
                if(servicecontractcoverdProductMap.containsKey(workorderMap.get(woId).SVMXC__Service_Contract__c)){
                    map<id,SVMXC__Service_Contract_Products__c> tempipcoverdProductmap = servicecontractcoverdProductMap.get(workorderMap.get(woId).SVMXC__Service_Contract__c);
                    prodSerLine.SVMX_Covered_Product__c = tempipcoverdProductmap.get(ipId).Id;
                }    
                wdInsertList.add(prodSerLine);
            }
            SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(Id=woId);
            wo.SVMX_Work_Detail_Created__c = true;
            woUpdateList.add(wo);
        }
        system.debug('Work detail insert list'+wdInsertList);
        if(wdInsertList.size() > 0)
            insert wdInsertList;   
        if(woUpdateList.size() > 0)
            update woUpdateList;
    }
    
    public static void createPmCoverage(Map<Id,Map<Id,List<Id>>> pmIdLocIdIpIdListMap){
        List<SVMXC__PM_Coverage__c> pmCovInsertList = new List<SVMXC__PM_Coverage__c>();
        for(Id pmId : pmIdLocIdIpIdListMap.keySet()){
            Map<Id,List<Id>> locIdIpIdListMap = pmIdLocIdIpIdListMap.get(pmId);
            //List<Id> locIdList = pmIdLocIdListMap.get(pmId);  //List of IP Id's
            for(Id locId : locIdIpIdListMap.keySet()){
                //Create Pm Coverage for the PM Plans
                SVMXC__PM_Coverage__c pmCov = new SVMXC__PM_Coverage__c();
                pmCov.SVMXC__PM_Plan__c = pmId;
                //pmCov.SVMXC__Product_Name__c = ipId;
                pmCov.SVMXC__Location_Name__c = locId;
                pmCovInsertList.add(pmCov);
            }
        }
        if(pmCovInsertList.size() > 0)
          insert pmCovInsertList;
    }
}