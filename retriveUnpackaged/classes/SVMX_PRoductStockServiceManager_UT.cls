/************************************************************************************************************
Description: Test Class for SVMX_ProductStockServiceManager class.

Dependancy: 
 
Author: Keshava Prasad
Date: 15-05-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

**************************************************************************************************/
@isTest
public class SVMX_PRoductStockServiceManager_UT {
    
       
    public static testMethod void checkUpdateProductStock() {
      /*   Account account          				= SVMX_TestUtility.CreateAccount('Test Account', 'Norway', true) ;
      Contact contact         				= SVMX_TestUtility.CreateContact(account.id, true) ;
      SVMXC__Site__c location  				= SVMX_TestUtility.CreateLocation('Test Location', account.id, true) ;
      Product2 product		   				= SVMX_TestUtility.CreateProduct('Test Product', true) ;

        
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;
        
        SVMXC__Product_Stock__c productStock = new SVMXC__Product_Stock__c() ;
        productStock.SVMXC__Product__c = product.id ;
        productStock.SVMXC__Location__c = location.id ;
        productStock.SVMXC__Quantity2__c = 1.0 ;
        productStock.SVMXC__Status__c = 'Available' ;
        insert productStock ;
        
                
        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
        
        
            
        Case cs = SVMX_TestUtility.CreateCase(account.Id,contact.Id, 'New', false);
        cs.SVMX_Service_Sales_Order_Number__c ='5788575648';
        cs.SVMX_Awaiting_SAP_Response__c = true;
        cs.SVMX_Invoice_Status__c='Requested on all cases';
        insert cs;
         
        Case cs1 = SVMX_TestUtility.CreateCase(account.Id, contact.Id, 'New', False);
        cs1.Type ='Quoted Works';
        cs1.SVMX_Service_Sales_Order_Number__c = '5788575649';
        cs1.SVMX_Awaiting_SAP_Response__c = True;
        cs1.SVMX_Invoice_Status__c='Requested on all cases';
        insert cs1;
        
          SVMXC__Service_Group__c serviceTeam = new SVMXC__Service_Group__c() ;
    serviceTeam.Name = 'Test team' ;
    serviceTeam.SVMXC__Country__c = 'Norway' ;
    	insert serviceTeam ;
    
      SVMXC__Service_Group_Members__c technician = new SVMXC__Service_Group_Members__c() ;
    technician.SVMXC__Service_Group__c = serviceTeam.id ;
    technician.Name = 'Test technician' ;
    technician.SVMXC__Active__c = true ;
    technician.SVMXC__Inventory_Location__c = location.id ;
    	insert technician ;
    
        List<SVMXC__Service_Order__c> workCompleteList = new List<SVMXC__Service_Order__c>() ;
       List<SVMXC__Service_Order_Line__c> rejectedWDList = new List<SVMXC__Service_Order_Line__c>() ;

        
      SVMXC__Service_Order__c workOrder = SVMX_TestUtility.CreateWorkOrder(account.id, location.id, product.id, 'Open', 'Reactive', 'Global Std', null, technician.id, cs.id, false) ;
        workOrder.SVMX_Service_Sales_Order_Number__c = '6846957' ;
     SVMXC__Service_Order_Line__c workDetail = SVMX_TestUtility.CreateWorkOrderDetail(account.id, location.id, product.id, 'Usage/Consumption', workOrder.id, DateTime.newInstance(2018, 5, 20), null, technician.id, null, 'Parts', null, false) ;
        workDetail.SVMXC__Actual_Quantity2__c = 1.0 ;
         workCompleteList.add(workOrder) ;
       
        
        SVMXC__Service_Order__c rejectedWorkOrder = SVMX_TestUtility.CreateWorkOrder(account.id, location.id, product.id, 'Open', null, null, null, technician.id, cs1.id, false) ;
     SVMXC__Service_Order_Line__c rejectedWorkDetail = SVMX_TestUtility.CreateWorkOrderDetail(account.id, location.id, product.id, 'Usage/Consumption', rejectedWorkOrder.id, DateTime.newInstance(2018, 5, 21), null, technician.id, null, 'Parts', null, false) ;
        rejectedWorkDetail.SVMXC__Actual_Quantity2__c = 1.0 ;
        workCompleteList.add(rejectedWorkOrder) ;
         rejectedWDList.add(rejectedWorkDetail);
        
        insert workOrder ;
         insert workDetail ;
        insert rejectedWorkDetail ;
*/
        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;
        
        
        
        
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers',false);
        prod.SAPNumber__c= '1234567';
        insert prod;
        
        Product2 prod1 = SVMX_TestUtility.CreateProduct('Door', false);
        prod1.SAPNumber__c= '1234567';
        insert prod1;
        
        SVMXC__Product_Stock__c productStock = new SVMXC__Product_Stock__c() ;
        productStock.SVMXC__Product__c = prod.id ;
        productStock.SVMXC__Location__c = loc.id ;
        productStock.SVMXC__Quantity2__c = 2 ;
        productStock.SVMXC__Status__c = Label.Available ;
        insert productStock ;
        
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
        
        Case cs = SVMX_TestUtility.CreateCase(acc.Id,con.Id, 'New', false);
        cs.SVMX_Service_Sales_Order_Number__c ='';
        cs.SVMX_Awaiting_SAP_Response__c = false;
        cs.SVMX_Invoice_Status__c='Requested on all cases';
        insert cs;
         
        Case cs1 = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', False);
        cs1.Type ='Quoted Works';
        cs1.SVMX_Service_Sales_Order_Number__c = '5788575648';
        cs1.SVMX_Awaiting_SAP_Response__c = True;
        cs1.SVMX_Invoice_Status__c='Requested on all cases';
        insert cs1;
       
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id,true);
        
        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        SVMX_Rate_Tiers__c rt2 = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, true, true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, false);
        tech.SVMXC__Inventory_Location__c = loc.id ;
        insert tech ;
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Germany',true);
        
        List<SVMXC__Service_Order__c> workCompleteList = new List<SVMXC__Service_Order__c>() ;
        
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Reactive','Global Std',sc.Id,tech.Id,cs.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.today();
        wo.SVMX_Service_Sales_Order_Number__c = '6846957';
        workCompleteList.add(wo) ;
        
        
        SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Planned Services (PPM)','Global Std',sc.Id,tech.Id,cs1.id,false);
        wo1.SVMXC__Scheduled_Date_Time__c = system.today();
        workCompleteList.add(wo1) ;
        insert workCompleteList ;
        
        list<SVMXC__Service_Order_Line__c> wol = new list<SVMXC__Service_Order_Line__c>();
        
        SVMXC__Service_Order_Line__c oli = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id,prod.Id,'Usage/Consumption',wo.Id,system.now(), sc.Id, tech.Id, cs.id,'Parts','Warranty',false);
        oli.SVMXC__Actual_Quantity2__c = 2;
        oli.SVMXC__Billable_Quantity__c = 1;
        wol.add(oli);
        
       /* SVMXC__Service_Order_Line__c oli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id,prod.Id,'Usage/Consumption',wo1.Id,system.now(), sc.Id, tech.Id, cs1.id,'parts','Warranty',false);
        oli1.SVMXC__Actual_Quantity2__c = 2;
        oli1.SVMX_Sales_Order_Item_Number__c='123';
        oli1.SVMXC__Billable_Quantity__c = 7;
        oli1.SVMX_PM_Charge__c = 'Yes';
        wol.add(oli1);
        
        SVMXC__Service_Order_Line__c oli2 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id,prod.Id,'Usage/Consumption',wo.Id,system.now(),sc.Id, tech.Id, cs1.id,'Labor','Warranty',false);
        oli2.SVMXC__Actual_Quantity2__c = 2;
        oli2.SVMX_Sales_Order_Item_Number__c='123';
        oli2.SVMXC__Billable_Quantity__c = 7;
        wol.add(oli2); */
        
        insert wol;

        
        SVMX_ProductStockServiceManager.updateProductStock(workCompleteList,wol);
        
        
    }
}