/************************************************************************************************************
Description: Data Manager for Entitlement History. All the SQL queries and DML Operations are performed in this class.

Dependancy: 
 
Author: Ranjitha S
Date: 17-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

public class SVMX_EntitlementHistoryDataManager {

    public static void createEntitleHistory(list<SVMXC__Entitlement_History__c> entitleHistoryList){

        if(!entitleHistoryList.isEmpty())
            insert entitleHistoryList;
    }
}