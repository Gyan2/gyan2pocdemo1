/************************************************************************************************************
Description: Data Manager for Service Contract. All the SOQL queries and DML Operations are performed in this class.

Dependency: 
 
Author: Tulsi B R
Date: 25-10-2017

Modification Log: 
Date            Author          Modification Comments

--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_ContactDataManager{

    public static List<Contact> getContacts(Id lstAccounts){
    
        List<Contact> lstCts = [select id,Email,SVMX_Roles__c,AccountId from Contact where AccountId =: lstAccounts];
        
        return lstCts;
    
    }

}