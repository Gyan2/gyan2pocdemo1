public class InvoiceTableController {


    @AuraEnabled
    public static String getFilesFromCase(String recordId){
            //        return '[{"Name":"Invoice n°1","Date":"2018-04-25","Id":"01234","Amount":"150","CurrencyIsoCode":"chf","Status":"Closed"},{"Name":"Invoice n°2","Date":"2018-04-21","Id":"01235","Amount":"150","CurrencyIsoCode":"chf","Status":"Closed"}]';
            //

        Case caseInstance = [select id,InvoicesJSON__c from Case where Id =: recordId];

        return String.isNotBlank(caseInstance.InvoicesJSON__c) ? caseInstance.InvoicesJSON__c : '[]';

    }

    @AuraEnabled
    public static SAPPO_DocumentService.InvoiceResult downloadFileFromSAPPO(String fileId){

        return new SAPPO_DocumentService().getInvoicePayLoadFromSAP(fileId);
    }
    
}