@isTest
public class SVMX_ServiceContractSiteDataManager_UT{
    
    public static testMethod void checkTimeBasedscsiteBlankcheck(){
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
           
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);

        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, false);
        sc.SVMXC__Active__c = False;
        sc.SVMX_Pricing_Method__c = 'Both';
        sc.SVMXC__Start_Date__c = Date.newInstance(2018, 1, 1);
        sc.SVMXC__End_Date__c = Date.newInstance(2019, 12, 10);
        insert sc;

        set<id> idset = new set<id>();
        idset.add(sc.id);

        Date startDate=system.today();

        SVMXC__Service_Contract_Sites__c  serContrSite = SVMX_TestUtility.CreateServicecontractSite(sc.Id,'Yearly on Last of Start month',startDate,false);
        serContrSite.SVMXC__Start_Date__c = system.today();
        serContrSite.SVMXC__End_Date__c = Date.newInstance(2018, 12, 9);
        serContrSite.SVMX_Pricing_Methos__c = 'Time Based';
        serContrSite.SVMX_SAP_Contract_Item_Number__c ='78238748';
        serContrSite.SVMX_SAP_Contract_Item_Num_Blank_Check__c = True;
        insert serContrSite;

        SVMX_ServiceContractSiteDataManager.timeBasedSCSiteBlankcheckQuery(idset);
    }    
}