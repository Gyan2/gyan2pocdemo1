/************************************************************************************************************
Description: UT for SVMX_PR_SAPAvailability_Controller class.
 
Author: Keshava Prasad
Date: 19-04-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest(SeeAllData=true)
public class SVMX_WO_SAPAvailability_Controller_UT {
    
    public static testMethod void checkSapAvailability() {



    Account account = SVMX_TestUtility.CreateAccount('Testing Account', 'Norway',true) ;
            account.SAPNumber__c = 'P1234' ;
        update account ;
            
    Contact contact = SVMX_TestUtility.CreateContact(account.id,true) ;



     SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'PurchaseRequisition';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
                 try{
        insert mycs;
      }catch(Exception e){}

        
        SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
              intg.name= 'SAP PO';
              intg.WS_USER__c ='test';
              intg.WS_PASS__c ='test';
              try{
        insert intg;
      }catch(Exception e){}
    
        
         SVMXC__Site__c  location = SVMX_TestUtility.CreateLocation('Service Location',account.id,true);
        location.SVMXC_SAP_Storage_Location__c = '1234667';
        location.SVMXC_Plant_Number__c = 'P122';
        location.SVMXC__Stocking_Location__c = true;
        location.SVMXC__Location_Type__c = 'External';
        location.SVMXC__Country__c = 'Norway' ;
        //location.id = partsReq.SVMX_From_SAP_Storage_Location__c ;
        update location;
        
         SVMXC__Site__c  stockLocation = SVMX_TestUtility.CreateLocation('From Location',account.id,true);
        stockLocation.SVMXC_SAP_Storage_Location__c = '112345644';
        stockLocation.SVMXC_Plant_Number__c = 'T677';
        stockLocation.SVMXC__Stocking_Location__c = true;
        stockLocation.SVMXC__Country__c = 'Norway' ;
        stockLocation.SVMXC__Location_Type__c = 'External';
       update stockLocation;
        
        
        
        SVMXC__Parts_Request__c  partsReq = new SVMXC__Parts_Request__c () ;
        partsReq.SVMXC__Requested_From__c = location.id ;
        partsReq.SVMXC__Required_At_Location__c = stockLocation.id ;
        partsReq.SVMX_From_SAP_Storage_Location__c = location.id ;
        insert partsReq ;
        
        SVMX_Sales_Office__c salesOffice = new SVMX_Sales_Office__c ();
        salesOffice.SVMX_Price_Order_Account__c = account.id ;
        salesOffice.SVMX_Country__c = 'Germany' ;
        insert salesOffice ;
    
    
    
    SVMXC__Installed_Product__c installedProduct = SVMX_TestUtility.CreateInstalledProduct(account.id,null,location.id,true);
        Product2 product = SVMX_TestUtility.CreateProduct('Test prod',true);
        product.SVMX_SAP_Product_Type__c = 'Stock Part';
        Update product;

          Product2 product22 = SVMX_TestUtility.CreateProduct('Test prod 2',true);
        product22.SVMX_SAP_Product_Type__c = 'Stock Part';
        Update product22;
    
    Profile profile = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
      User user = SVMX_TestUtility.CreateUser('Testtech', profile.id, true);
      SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Svmx Tech2', user.id, null, true);
        tech.SVMXC__Inventory_Location__c = location.id;
        update tech;


        Case cs = SVMX_TestUtility.CreateCase(account.Id, contact.Id, 'New', true);

         SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(account.Id, null, product.Id, 'open', 'Reactive','Global Std', null, tech.Id, cs.id, false);
       wo.SVMX_From_SAP_Storage_Location__c = location.id;
       wo.SVMX_To_Stock_Location__c = location.id;
       wo.SVMX_Default_Stock_Location__c =location.id;
       insert wo;

       SVMXC__Service_Order_Line__c oli1 = SVMX_TestUtility.CreateWorkOrderDetail(account.Id, null, product.Id, 'Request/Receipt', wo.Id, system.now(), null, tech.Id, cs.id,'Parts',null,false);

       insert oli1;
        SVMXC__Service_Order_Line__c oli2 = SVMX_TestUtility.CreateWorkOrderDetail(account.Id, null, product22.Id, 'Estimate', wo.Id, system.now(), null, tech.Id, cs.id,'Parts',null,false);
        insert oli2;

       List<SVMXC__Product_Stock__c> psList = new List<SVMXC__Product_Stock__c>();
       SVMXC__Product_Stock__c ps = new SVMXC__Product_Stock__c();
       ps.SVMXC__Product__c = product.id;
       ps.SVMXC__Status__c = 'Available';
       ps.SVMXC__Quantity2__c = 2;
       ps.SVMXC__Location__c = stockLocation.id;
       insert ps;
        
      
       PageReference pageRef = Page.SVMX_WO_SAPAvailability ;
         pageRef.getParameters().put('workOrderId', wo.id) ;
         pageRef.getParameters().put('woOrderType', 'Reactive') ;
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
         SVMX_WO_SAPAvailability_Controller sapAvailability = new SVMX_WO_SAPAvailability_Controller() ;
        sapAvailability.workdetailWrap.get(0).availableFrmLocName = location.Name ;
       // sapAvailability.partReqLineWrap.get(0).avlQty = '1';
        sapAvailability.workdetailWrap.get(0).availableFrmLoc = location.id;
        
        Test.setMock(WebServiceMock.class, new SAPPurchaseRequestUtilityMock());
            SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader();
            SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest purchaseRequest = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest();
            SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort requstelemnt=new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort();
            requstelemnt.CreateSync(messageheader,purchaseRequest);
        sapAvailability.wo = wo;
                 
        sapAvailability.chkbox();
        sapAvailability.ch();
        sapAvailability.change();
        sapAvailability.ChkAvailability(); 
        sapAvailability.toLocChange();
        sapAvailability.Back();
        sapAvailability.workdetailWrap.get(0).availableFrmLocName = location.Name ;
        sapAvailability.workdetailWrap.get(0).avlQty = '1';
        sapAvailability.workdetailWrap.get(0).availableFrmLoc = location.id;
        sapAvailability.workdetailWrap.get(0).avlDate = String.valueOf(system.today().addDays(1));
        sapAvailability.PartOrder();
        
        Test.stopTest();
    
    
    }
}