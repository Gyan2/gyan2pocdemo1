/************************************************************************************************************
Description: Service Manager for Service Contract. All the methods and the logic for Work Order object resides here.

Dependancy: 
 
Author: Ranjitha S
Date: 18-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/
public class SVMX_ServiceContractServiceManager {

    // Randam code generation method.
    public Static String generateRandamNumber(){
        Integer len=7;
        final String chars='0123456789';
        String randStr='';
        while(randStr.length() < len){
            Integer idx=Math.mod(Math.abs(Crypto.getRandomInteger()),chars.length());
            randStr+=chars.substring(idx,idx+1);
        }
        return randStr;
    }
    
    public static void dynamicPMPlan(List<SVMXC__Service_Contract__c> sclist){
        List<SVMXC__Service_Contract__c> scPMList = new List<SVMXC__Service_Contract__c>();
        List<SVMXC__PM_Offering__c> newPMOfList = new List<SVMXC__PM_Offering__c>();
        List<SVMXC__PM_Offering__c> delPMoff = new List<SVMXC__PM_Offering__c>();
        SVMX_DefinitionClass.runOnceDynamicPM = false;
        
        scPMList = SVMX_ServiceContractDataManager.pmOfferingQuery(scList);
        system.debug(' ## sclist' +sclist);
        for(SVMXC__Service_Contract__c sc: scPMList){
            for(SVMXC__PM_Offering__c pm: sc.SVMXC__PM_Offerings__r){
                delPMoff.add(pm);
            }
        }
        
        if(!delPMoff.isEmpty())
            SVMX_ServiceContractDataManager.delPMOffering(delPMoff);
        
        List<SVMX_Dynamic_PM_Configuration__c> PMconfList = [select id, name, SVMX_PM_Plan_Template__c, SVMX_Service_Maintenance_Contract__c, SVMX_Service_Maintenance_Contract__r.SVMX_Current_Renewal_Year__c, SVMX_PM_Plan_Template__r.SVMXC__Coverage_Type__c, SVMX_Year__c from SVMX_Dynamic_PM_Configuration__c where SVMX_Service_Maintenance_Contract__c in :scList ]; 
        for(SVMX_Dynamic_PM_Configuration__c pm: PMconfList){
                if(pm.SVMX_Year__c.contains(string.valueof(pm.SVMX_Service_Maintenance_Contract__r.SVMX_Current_Renewal_Year__c))){
                    SVMXC__PM_Offering__c pmOff = new SVMXC__PM_Offering__c();
                    pmoff.SVMXC__Service_Contract__c = pm.SVMX_Service_Maintenance_Contract__c;
                    pmoff.SVMXC__PM_Plan_Template__c = pm.SVMX_PM_Plan_Template__c;
                    pmoff.SVMX_Coverage_Type__c = pm.SVMX_PM_Plan_Template__r.SVMXC__Coverage_Type__c;
                    newPMOfList.add(pmOff);
                }
        }
        if(!newPMOfList.isEmpty())
            SVMX_ServiceContractDataManager.insertPMoffering(newPMOfList);
    }   
}