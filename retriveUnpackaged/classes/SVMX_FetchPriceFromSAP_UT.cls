@isTest
public class SVMX_FetchPriceFromSAP_UT {
   
    public static testMethod void createTestData(){
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
      
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
           
        Case cs = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', true);
       
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, true);
        
        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        SVMX_Rate_Tiers__c rt2 = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, true, true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Norway',true);
        
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Planned Services (PPM)','Global Std', sc.Id, tech.Id, cs.id, true);
        //SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Planned Services (PPM)','Global Std', sc.Id, tech.Id, cs.id, true);
        
        SVMXC__Service_Order_Line__c oli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Parts','Warranty',false);
        oli1.SVMX_Country__c='Norway';
        oli1.SVMXC__Actual_Quantity2__c=1;
        SVMXC__Service_Order_Line__c oli2 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Estimate', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'Parts','Goodwill',false);
        oli2.SVMX_Country__c='Norway';
        oli2.SVMXC__Estimated_Quantity2__c =1;
        
        
        list<SVMXC__Service_Order_Line__c> oliList = new list<SVMXC__Service_Order_Line__c>();
        oliList.add(oli1);
        oliList.add(oli2);
    
        
        test.startTest();
        insert oliList;
        
        List<SVMXC__Service_Order_Line__c> lstWOItem = [SELECT Id FROM SVMXC__Service_Order_Line__c];
        SVMXC.SFM_WrapperDef.SFM_TargetRecord targetTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecord();
        List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject> detailRecordsList = new List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject>();


        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap1 = new SVMXC.SFM_WrapperDef.SFM_StringMap('SVMX_recordId',wo.Id);       

        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap2 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',wo.Id);
        //SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',workDetails[0].Id);

        //SVMXC.INTF_WebServicesDef.INTF_StringMap tempStringMap3 = new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__End_Date__c','2019-05-02');
        targetTemp.stringMap.add(tempStringMap1);
        
        targetTemp.headerRecord.objName = 'SVMXC__Service_Order__c';
        //targetTemp.headerRecord.pageLayoutId='a357A0000005qBtQAI';
        
        //targetTemp.detailRecords.objName = 'SVMXC__Service_Order_Line__c';
        
        SVMXC.SFM_WrapperDef.SFM_Record recordstemp = new SVMXC.SFM_WrapperDef.SFM_Record();
        //SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();

        recordstemp.targetRecordAsKeyValue.add(tempStringMap2);
        //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
        recordstemp.targetRecordId=wo.Id;

        
        for(SVMXC__Service_Order_Line__c wdl:lstWOItem){
            SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();
            SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',wdl.Id);
            SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
            recordstemp1.targetRecordAsKeyValue.add(tempStringMap3);
            //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
            recordstemp1.targetRecordId=wdl.Id;
            detailRecordTemp.records.add(recordstemp1);
            detailRecordTemp.objName = 'SVMXC__Service_Order_Line__c';
            detailRecordsList.add(detailRecordTemp);
        }
        SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp1 = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
        detailRecordTemp1.aliasName='';
        //detailRecordTemp1.pageLayoutId='pageLayoutI';
        detailRecordTemp1.objName = 'SVMXC__Service_Order_Line__c';
        detailRecordTemp1.parentColumnName = 'SVMXC__Service_Order__c';

        detailRecordsList.add(detailRecordTemp1);
        targetTemp.headerRecord.records.add(recordstemp);
        targetTemp.detailRecords = detailRecordsList;

        system.debug('calling web service pre');
        system.debug('targetTemp '+targetTemp);
        
        SVMXC.SFM_WrapperDef.SFM_PageData response = SVMX_FetchPriceFromSAP.getPartsPrice(targetTemp);
        system.debug('calling web service post');
      
        Test.stopTest();
        
        
    } 
}

        
    /*
    @isTest
    private static void getRealTimePriceTest(){
      Test.startTest();
        
     
        
        
        // query parent record created in test setup
        //List<SVMXC__Service_Order__c> lstWO = [SELECT Id FROM SVMXC__Service_Order__c];
        List<Case> caseList = [SELECT id FROM Case LIMIT 1];
        Account acc =[SELECT id FROM Account LIMIT 1];
        
        //insert child WO
        Id rtWO1 = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Quotes').getRecordTypeId();
        
        SVMXC__Service_Order__c quote = new SVMXC__Service_Order__c(RecordTypeId = rtWO1);
        quote.SVMXC__Order_Status__c = 'Open';
        quote.SVMXC__Company__c = acc.Id;
        //quote.SVMXC__Site__c = location2.Id;
        insert quote; 
        
        //standard pricebook
        Id pricebookId = Test.getStandardPricebookId();
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'Medical';
        insert priceBk;

        Product2 prod1 = new Product2();
        prod1.Name = 'Test Medical Product';
        prod1.IsActive= True;
        prod1.ProductCode='TEST123';
        insert prod1;
        
        PricebookEntry pbEntry = new PricebookEntry();
        pbEntry.Product2Id = prod1.Id;
        pbEntry.UnitPrice = 2;
        pbEntry.Pricebook2Id = pricebookId;
        pbEntry.IsActive = true;
        insert pbEntry;
        
        PricebookEntry pbEntry1 = new PricebookEntry();
        pbEntry1.Product2Id = prod1.Id;
        pbEntry1.UnitPrice = 2;
        pbEntry1.Pricebook2Id = priceBk.Id;
        pbEntry1.IsActive = true;
        insert pbEntry1;
        
        List<SVMXC__Service_Order_Line__c> workDetails = new List<SVMXC__Service_Order_Line__c>();
        Id recordTypeId = Schema.sObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        for(Integer i = 0; i < 10; i++) {
            SVMXC__Service_Order_Line__c soRecord = new SVMXC__Service_Order_Line__c(RecordTypeId = recordTypeId);
            
               soRecord.SVMXC__Line_Type__c = 'Parts';
                soRecord.SVMXC__Actual_Quantity2__c = 2;
                soRecord.SVMXC__Billable_Line_Price__c = 60;
            soRecord.SVMXC__Service_Order__c = quote.Id;
            //soRecord.SVMX_Consumed_Part__c = stockProduct.Id;
            soRecord.SVMXC__Billable_Quantity__c = 20;
            soRecord.SVMXC__Product__c = prod1.Id;
            soRecord.SVMXC__Billable_Line_Price__c = 60;
            //soRecord.SVMX_Consumed_Serial_Number__c = serail.Id;
            //soRecord.SVMXC__Product__c = prd1.Id;
            workDetails.add(soRecord);
        }
        insert workDetails;
        
        
        
        List<SVMXC__Service_Order_Line__c> lstWOItem = [SELECT Id FROM SVMXC__Service_Order_Line__c];
        
        SVMXC.SFM_WrapperDef.SFM_TargetRecord targetTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecord();
        List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject> detailRecordsList = new List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject>();


        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap1 = new SVMXC.SFM_WrapperDef.SFM_StringMap('SVMX_recordId',quote.Id);
        

        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap2 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',quote.Id);
        //SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',workDetails[0].Id);

        //SVMXC.INTF_WebServicesDef.INTF_StringMap tempStringMap3 = new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__End_Date__c','2019-05-02');
        targetTemp.stringMap.add(tempStringMap1);
        
        targetTemp.headerRecord.objName = 'SVMXC__Service_Order__c';
        //targetTemp.headerRecord.pageLayoutId='a357A0000005qBtQAI';
        
        //targetTemp.detailRecords.objName = 'SVMXC__Service_Order_Line__c';
        
        SVMXC.SFM_WrapperDef.SFM_Record recordstemp = new SVMXC.SFM_WrapperDef.SFM_Record();
        //SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();

        recordstemp.targetRecordAsKeyValue.add(tempStringMap2);
        //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
        recordstemp.targetRecordId=quote.Id;

        
        for(SVMXC__Service_Order_Line__c wdl:lstWOItem){
            SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();
            SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',wdl.Id);
            SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
            recordstemp1.targetRecordAsKeyValue.add(tempStringMap3);
            //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
            recordstemp1.targetRecordId=workDetails[0].Id;
            detailRecordTemp.records.add(recordstemp1);
            detailRecordTemp.objName = 'SVMXC__Service_Order_Line__c';
            detailRecordsList.add(detailRecordTemp);
        }
        SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp1 = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
        detailRecordTemp1.aliasName='';
        //detailRecordTemp1.pageLayoutId='pageLayoutI';
        detailRecordTemp1.objName = 'SVMXC__Service_Order_Line__c';
        detailRecordTemp1.parentColumnName = 'SVMXC__Service_Order__c';

        detailRecordsList.add(detailRecordTemp1);
        targetTemp.headerRecord.records.add(recordstemp);
        targetTemp.detailRecords = detailRecordsList;
        
        SVMX_FetchPriceFromSAP.getPartsPrice(targetTemp);
       // Medical_CallExternalWebServiceFromSFM.getRealTimePrice(targetTemp);
        Test.stopTest();
        System.assertEquals(60,[SELECT id,SVMXC__Billable_Line_Price__c FROM SVMXC__Service_Order_Line__c WHERE SVMXC__Line_Type__c = 'Parts' LIMIT 1].SVMXC__Billable_Line_Price__c);
    }*/
    
 /*    @isTest
    private static void getRealTimePriceTest1(){
      Test.startTest();
        Medical_CustomerPricing_REST.mockSuccess = false;
        Test.setMock(HttpCalloutMock.class, new IntegerationStrykerRestServiceMock());
        
        
        // query parent record created in test setup
        //List<SVMXC__Service_Order__c> lstWO = [SELECT Id FROM SVMXC__Service_Order__c];
        List<Case> caseList = [SELECT id FROM Case LIMIT 1];
        Account acc = [SELECT id FROM Account LIMIT 1];
        
        //insert child WO
        Id rtWO1 = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Quotes').getRecordTypeId();
        
        SVMXC__Service_Order__c quote = new SVMXC__Service_Order__c(RecordTypeId = rtWO1);
        quote.SVMXC__Order_Status__c = 'Open';
        quote.SVMXC__Case__c = caseList.get(0).Id;
        quote.SVMXC__Work_Order_Scheduling_Status__c = 'Open';
        quote.Oracle_Repair_Number__c = '1111';
        //quote.Work_Order_Quote__c = lstWO.get(0).ID;
        quote.Get_Customer_Price__c = true;
        QUOTE.SVMXC__Company__c = acc.id;
        //quote.SVMXC__Site__c = location2.Id;
        insert quote;  
        
        PriceBook2 priceBk = new PriceBook2();
        priceBk.Name = 'Europe';
        insert priceBk;

        Product2 prod1 = new Product2();
        prod1.Name = 'Test Medical Product';
        prod1.IsActive= True;
        prod1.ProductCode='TEST123';
        insert prod1;

        List<SVMXC__Service_Order_Line__c> workDetails = new List<SVMXC__Service_Order_Line__c>();
        Id recordTypeId = Schema.sObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        for(Integer i = 0; i < 10; i++) {
            SVMXC__Service_Order_Line__c soRecord = new SVMXC__Service_Order_Line__c(RecordTypeId = recordTypeId);
            if(i < 4){
               soRecord.SVMXC__Line_Type__c = 'Parts';
                soRecord.SVMXC__Actual_Quantity2__c = 2;
                soRecord.SVMXC__Billable_Line_Price__c = 60;
            } else if (i >=4 && i < 7) {
               soRecord.SVMXC__Line_Type__c = 'Labor';
               soRecord.Quarter_Hour_Unit__c = 2;
                soRecord.SVMXC__Billable_Line_Price__c = 70;
            }else {
                if(i == 7) {
                    soRecord.Quarter_Hour_Unit__c = 18;
                }else {
                    soRecord.Quarter_Hour_Unit__c = 2;
                }
                soRecord.SVMXC__Line_Type__c = 'Travel';
                soRecord.SVMXC__Billable_Line_Price__c = 50;
            }
            soRecord.SVMXC__Service_Order__c = quote.Id;
            //soRecord.SVMX_Consumed_Part__c = stockProduct.Id;
            soRecord.SVMXC__Billable_Quantity__c = 20;
            soRecord.SVMXC__Product__c = prod1.Id;
            //soRecord.SVMX_Consumed_Serial_Number__c = serail.Id;
            //soRecord.SVMXC__Product__c = prd1.Id;
            workDetails.add(soRecord);
        }
        insert workDetails;
        
        
        
        List<SVMXC__Service_Order_Line__c> lstWOItem = [SELECT Id FROM SVMXC__Service_Order_Line__c];
        
        SVMXC.SFM_WrapperDef.SFM_TargetRecord targetTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecord();
        List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject> detailRecordsList = new List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject>();


        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap1 = new SVMXC.SFM_WrapperDef.SFM_StringMap('SVMX_recordId',quote.Id);
        

        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap2 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',quote.Id);
        //SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',workDetails[0].Id);

        //SVMXC.INTF_WebServicesDef.INTF_StringMap tempStringMap3 = new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__End_Date__c','2019-05-02');
        targetTemp.stringMap.add(tempStringMap1);
        
        targetTemp.headerRecord.objName = 'SVMXC__Service_Order__c';
        //targetTemp.headerRecord.pageLayoutId='a357A0000005qBtQAI';
        
        //targetTemp.detailRecords.objName = 'SVMXC__Service_Order_Line__c';
        
        SVMXC.SFM_WrapperDef.SFM_Record recordstemp = new SVMXC.SFM_WrapperDef.SFM_Record();
        //SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();

        recordstemp.targetRecordAsKeyValue.add(tempStringMap2);
        //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
        recordstemp.targetRecordId=quote.Id;

        
        for(SVMXC__Service_Order_Line__c wdl:lstWOItem){
            SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();
            SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',wdl.Id);
            SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
            recordstemp1.targetRecordAsKeyValue.add(tempStringMap3);
            //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
            recordstemp1.targetRecordId=workDetails[0].Id;
            detailRecordTemp.records.add(recordstemp1);
            detailRecordTemp.objName = 'SVMXC__Service_Order_Line__c';
            detailRecordsList.add(detailRecordTemp);
        }
        SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp1 = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
        detailRecordTemp1.aliasName='';
        //detailRecordTemp1.pageLayoutId='pageLayoutI';
        detailRecordTemp1.objName = 'SVMXC__Service_Order_Line__c';
        detailRecordTemp1.parentColumnName = 'SVMXC__Service_Order__c';

        detailRecordsList.add(detailRecordTemp1);
        targetTemp.headerRecord.records.add(recordstemp);
        targetTemp.detailRecords = detailRecordsList;
        
        Medical_CustomerPricingWSCallout.getRealTimePrice(targetTemp);
       // Medical_CallExternalWebServiceFromSFM.getRealTimePrice(targetTemp);
        Test.stopTest();
        System.assertEquals(60,[SELECT id,SVMXC__Billable_Line_Price__c FROM SVMXC__Service_Order_Line__c WHERE SVMXC__Line_Type__c = 'Parts' LIMIT 1].SVMXC__Billable_Line_Price__c);
    }
    @isTest
    private static void getRealTimePriceTest2(){
        Test.startTest();
        Medical_CustomerPricingWSCallout.testClassVar = true;
        Medical_CustomerPricing_REST.mockSuccess = true;
        Test.setMock(HttpCalloutMock.class, new IntegerationStrykerRestServiceMock());
        
        
        // query parent record created in test setup
        //List<SVMXC__Service_Order__c> lstWO = [SELECT Id FROM SVMXC__Service_Order__c];
        List<Case> caseList = [SELECT id FROM Case LIMIT 1];
        Account acc =[SELECT id FROM Account LIMIT 1];
        
        //insert child WO
        Id rtWO1 = Schema.sObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Quotes').getRecordTypeId();
        
        SVMXC__Service_Order__c quote = new SVMXC__Service_Order__c(RecordTypeId = rtWO1);
        quote.SVMXC__Order_Status__c = 'Open';
        quote.SVMXC__Case__c = caseList.get(0).Id;
        quote.SVMXC__Work_Order_Scheduling_Status__c = 'Open';
        quote.Oracle_Repair_Number__c = '1111';
        //quote.Work_Order_Quote__c = lstWO.get(0).ID;
        quote.Get_Customer_Price__c = true;
        quote.SVMXC__Company__c = acc.Id;
        //quote.SVMXC__Site__c = location2.Id;
        insert quote; 
        
        List<SVMXC__Service_Order_Line__c> workDetails = new List<SVMXC__Service_Order_Line__c>();
        Id recordTypeId = Schema.sObjectType.SVMXC__Service_Order_Line__c.getRecordTypeInfosByName().get('Usage/Consumption').getRecordTypeId();
        for(Integer i = 0; i < 3; i++) {
            SVMXC__Service_Order_Line__c soRecord = new SVMXC__Service_Order_Line__c(RecordTypeId = recordTypeId);
            if(i == 0){
               soRecord.SVMXC__Line_Type__c = 'Parts';
                soRecord.SVMXC__Actual_Quantity2__c = 2;
                soRecord.SVMXC__Billable_Line_Price__c = 60;
            } else if (i == 1) {
               soRecord.SVMXC__Line_Type__c = 'Labor';
               soRecord.Quarter_Hour_Unit__c = 2;
                soRecord.SVMXC__Billable_Line_Price__c = 70;
            }else {
                soRecord.SVMXC__Line_Type__c = 'Travel';
                soRecord.Quarter_Hour_Unit__c = 2;
                soRecord.SVMXC__Billable_Line_Price__c = 50;
            }
            soRecord.SVMXC__Service_Order__c = quote.Id;
            //soRecord.SVMX_Consumed_Part__c = stockProduct.Id;
            soRecord.SVMXC__Billable_Quantity__c = 20;
            //soRecord.SVMXC__Product__c = prod1.Id;
            soRecord.SVMXC__Billable_Line_Price__c = 60;
            //soRecord.SVMX_Consumed_Serial_Number__c = serail.Id;
            //soRecord.SVMXC__Product__c = prd1.Id;
            workDetails.add(soRecord);
        }
        insert workDetails;
        Test.stopTest();
        
        
        List<SVMXC__Service_Order_Line__c> lstWOItem = [SELECT Id FROM SVMXC__Service_Order_Line__c];
        
        SVMXC.SFM_WrapperDef.SFM_TargetRecord targetTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecord();
        List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject> detailRecordsList = new List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject>();


        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap1 = new SVMXC.SFM_WrapperDef.SFM_StringMap('SVMX_recordId',quote.Id);
        

        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap2 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',quote.Id);
        //SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',workDetails[0].Id);

        //SVMXC.INTF_WebServicesDef.INTF_StringMap tempStringMap3 = new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__End_Date__c','2019-05-02');
        targetTemp.stringMap.add(tempStringMap1);
        
        targetTemp.headerRecord.objName = 'SVMXC__Service_Order__c';
        //targetTemp.headerRecord.pageLayoutId='a357A0000005qBtQAI';
        
        //targetTemp.detailRecords.objName = 'SVMXC__Service_Order_Line__c';
        
        SVMXC.SFM_WrapperDef.SFM_Record recordstemp = new SVMXC.SFM_WrapperDef.SFM_Record();
        //SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();

        recordstemp.targetRecordAsKeyValue.add(tempStringMap2);
        //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
        recordstemp.targetRecordId=quote.Id;

        
        for(SVMXC__Service_Order_Line__c wdl:lstWOItem){
            SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();
            SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',wdl.Id);
            SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
            recordstemp1.targetRecordAsKeyValue.add(tempStringMap3);
            //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
            recordstemp1.targetRecordId=workDetails[0].Id;
            detailRecordTemp.records.add(recordstemp1);
            detailRecordTemp.objName = 'SVMXC__Service_Order_Line__c';
            detailRecordsList.add(detailRecordTemp);
        }
        SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp1 = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
        detailRecordTemp1.aliasName='';
        //detailRecordTemp1.pageLayoutId='pageLayoutI';
        detailRecordTemp1.objName = 'SVMXC__Service_Order_Line__c';
        detailRecordTemp1.parentColumnName = 'SVMXC__Service_Order__c';

        detailRecordsList.add(detailRecordTemp1);
        targetTemp.headerRecord.records.add(recordstemp);
        targetTemp.detailRecords = detailRecordsList;
        
        Medical_CustomerPricingWSCallout.getRealTimePrice(targetTemp);
       // Medical_CallExternalWebServiceFromSFM.getRealTimePrice(targetTemp);
        
       System.assertEquals(60,[SELECT id,SVMXC__Billable_Line_Price__c FROM SVMXC__Service_Order_Line__c WHERE SVMXC__Line_Type__c = 'Parts' LIMIT 1].SVMXC__Billable_Line_Price__c);
    }
    /*@isTest static void testwebservice(){
        EU_TestUtils.createEUOracleIntegrationSettings();
        // SVMX_CallExternalWebServiceFromSFM extWeb = new SVMX_CallExternalWebServiceFromSFM(null);
        EU_ModelnRtpm.RealTimePriceMultiResponseType modelObj1 = new EU_ModelnRtpm.RealTimePriceMultiResponseType();
        EU_ModelnRtpm.RealTimePriceMultiRequestType modelObj2 = new EU_ModelnRtpm.RealTimePriceMultiRequestType();
        EU_ModelnRtpm.ResolvedPrices_element modelObj3 = new EU_ModelnRtpm.ResolvedPrices_element();
        EU_ModelnRtpm.ResolvedPriceType modelObj4 = new EU_ModelnRtpm.ResolvedPriceType();
        EU_ModelnRtpm.ProductIDs_element modelObj5 = new EU_ModelnRtpm.ProductIDs_element();
        EU_OracleEmeaRealtimePriceCalc.EMEARealTImePriceTargetBPELProcess_pt priceObj1 = new EU_OracleEmeaRealtimePriceCalc.EMEARealTImePriceTargetBPELProcess_pt();
    }*/