@IsTest
public with sharing class TestSAPPOCustomerService {

    @testSetup static void setup(){

        Account acc = new Account(
            Name = 'Test',
            AccountName2__c = 'Test',
            AccountName3__c = 'Test',
            AccountName4__c = 'Test',
            BillingPostalCode = '8000',
            BillingCountryCode = 'CH',
            BillingCity = 'Zurich',
            BillingStreet = '80 blablabla',
            Language__c = 'DE'
        );

        insert acc;

        Location__c loc = new Location__c(
            Name                        = 'Test',
            SVMX_LocationName2__c       = 'Test',
            SVMX_LocationName3__c       = 'Test',
            SVMX_LocationName4__c       = 'Test',
            SVMXC_Zip__c                = '8000',
            SVMXC_Country__c            = 'CH',
            SVMXC_City__c               = 'Zurich',
            SVMXC_Street__c             = '80 blablabla'
        );

        insert loc;

    }


    static testMethod void testAccount() {

        Test.setMock(WebServiceMock.class,new CustomerRequestMockImpl());

        Test.startTest();
            SAPPO_CustomerService.futureSyncAccount([select id from Account limit 1].Id);
        Test.stopTest();
    }

    static testMethod void testLocation() {

        Test.setMock(WebServiceMock.class,new CustomerRequestMockImpl());

        Test.startTest();
            SAPPO_CustomerService.futureSyncLocation([select id from Location__c limit 1].Id);
            coverWrappers();
        Test.stopTest();
    }


    static void coverWrappers(){
        new SAPPO_CustomerCreateRequest.SalesArrangementCreateRequestTransportationTerms();
        new SAPPO_CustomerCreateRequest.SalesArrangementCreateRequestDeliveryTerms();
        new SAPPO_CustomerCreateRequest.SalesArrangementCreateRequestSalesArrangement();
        new SAPPO_CustomerCreateRequest.SalesArrangementCreateRequestPricingTerms();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestBankDetails();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestIndustrySector();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestOffice();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestRelationship();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestPersonName();
        new SAPPO_CustomerCreateRequest.SalesArrangementCreateRequestSalesOfficeParty();
        new SAPPO_CustomerCreateRequest.SalesArrangementCreateRequestPaymentTerms();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestRelshpContactPersonCommunication();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestRelshpContactPersonAddress();
        new SAPPO_CustomerCreateRequest.SalesArrangementCreateRequestSalesGroupParty();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestMarketingAttributes();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestRelshpContactPerson();
        new SAPPO_CustomerCreateRequest.SalesArrangementCreateRequestParty();
        new SAPPO_CustomerCreateRequest.CustomerCreateRequestBankDetailsBank();
    }
    /*
            Mock
     */

    public class CustomerRequestMockImpl implements WebServiceMock  {

        public void doInvoke(
                Object stub,
                Object request,
                Map<String, Object> response,
                String endpoint,
                String soapAction,
                String requestName,
                String responseNS,
                String responseName,
                String responseType) {

            SAPPO_ACK.notificationsResponse_element element = new SAPPO_ACK.notificationsResponse_element();
                                                    element.Ack = true;

            response.put('response_x', element);
        }

    }

}