/**
 * Created by rebmangu on 07/05/2018.
 */

@IsTest
public with sharing class TestWSInvoices {

    @testSetup static void setup(){
        insert new Case(Description='Test Case Invoice');
    }

    public static testMethod void upsertInvoice() {
        Case case1 = [select id from Case limit 1];

        WSInvoices.InvoiceRequest request = new WSInvoices.InvoiceRequest();
        WSInvoices.Invoice invoice = new WSInvoices.Invoice();
            invoice.Status = 'Opened';
            invoice.Amount = 255.55;
            invoice.CurrencyIsoCode = 'CHF';
            invoice.Name = 'Invoice n1';
            invoice.SenderSystem = 'PS8';
            invoice.CaseId = case1.id;
            invoice.Id = 'Invoice-1';

        request.invoices = new List<WSInvoices.Invoice>{
                invoice
        };

        // Invoice
        WSInvoices.upsertInvoices(request);
        case1 = [select id,InvoicesJSON__c from Case limit 1];
        System.assertNotEquals(null,case.InvoicesJSON__c);

        List<WSInvoices.Invoice> invoices = (List<WSInvoices.Invoice>)JSON.deserialize(case1.InvoicesJSON__c,List<WSInvoices.Invoice>.class);
        System.assertEquals(true,invoices[0].CreatedDate == invoices[0].LastModifiedDate);
        System.assertEquals('PS8',invoices[0].SenderSystem);
        System.assertEquals('Invoice-1',invoices[0].Id);

        //  Update
        WSInvoices.upsertInvoices(request);

        case1 = [select id,InvoicesJSON__c from Case limit 1];
        invoices = (List<WSInvoices.Invoice>)JSON.deserialize(case1.InvoicesJSON__c,List<WSInvoices.Invoice>.class);
        System.assertEquals(true,invoices[0].CreatedDate < invoices[0].LastModifiedDate);

    }

}