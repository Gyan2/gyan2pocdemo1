/************************************************************************************************************
Description: Controller for SVMX_SendQuoteEmail VF page.

Dependancy: 
          SVMX_SendQuote_DataManager
 
Author: Ranjitha Shenoy
Date: 15-11-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

public  class SVMX_SendServiceQuoteController{

 public List<SVMXC__Quote__c> quoteList {get; set;}
        public String quoteNumber{get ; set;}
        public  String contactName { get ; set ;}
        public  String contactEmail { get ; set ;}
        public List<AttachmentWrapper> attachmentWrapperList {get;set;}
        public Boolean isCheckedAll {get ; set ; }  
        public List<String> address =new List<String>();
        public Contact primaryContact = new Contact();
                    
        public SVMX_SendServiceQuoteController()
        {
            isCheckedAll = FALSE;
            quoteList = new List<SVMXC__Quote__c>();
            attachmentWrapperList = new List<AttachmentWrapper>();
            quoteNumber = ApexPages.currentPage().getParameters().get('quoteNumber');
               system.debug('quoteNumber '+quoteNumber);
            if(quoteNumber != null)
                fetchQuotes();
        }
        
        public PageReference fetchQuotes()
        {
             System.debug('quoteNumber-->'+quoteNumber);
            if(quoteNumber != null && quoteNumber != '')
            {
                quoteList = SVMX_ServiceQuoteDataManager.servQuoteList(quoteNumber);
                //system.debug('Contract List '+quoteList[0].SVMXC__Contact__r.Email);             
                if(quoteList != null && quoteList.size() > 0)
                {

                    for(SVMXC__Quote__c contract : quoteList)
                    {
                        AttachmentWrapper wrapper = new  AttachmentWrapper(contract);


                        attachmentWrapperList.add(wrapper);
                    }

                    List<Contact> quotecontacts = SVMX_ContactDataManager.getContacts(quoteList[0].SVMXC__Company__c);
                    if(quotecontacts.size() > 0)
                        primaryContact = quotecontacts[0];
                    for( Contact ct: quotecontacts){
                        system.debug('ct.SVMX_Roles__c-->'+ct.SVMX_Roles__c);
                        if(ct.SVMX_Roles__c != NULL && ct.SVMX_Roles__c.contains('Quotation Manager')){
                            //system.debug('email '+ct.Email);
                            address.add(ct.Email);
                        }

                    }
                }
                else
                {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.No_Quote_Found + quoteNumber));
                }
            }
            else
            {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Valid_Quote_Name));
            }

                 return null;
        }
        
        public PageReference sendQuotes()
        {
            if(attachmentWrapperList != null)
            {    
                Set<Id> attachmentIdSet = new Set<Id> ();         
                Id quoteOppId ;

                SVMXC__Quote__c primaryQuote ;

                system.debug('attachmentWrapperList '+attachmentWrapperList);
                for(AttachmentWrapper wrapper : attachmentWrapperList)
                {
                        primaryQuote = wrapper.contract;

                    if(wrapper.isSelected && wrapper.contract.Attachments != null && wrapper.contract.Attachments.size() > 0) 
                    {
                        attachmentIdSet.add(wrapper.contract.Attachments[0].id);
   
                    }

                }

                system.debug('primary quote '+primaryQuote);
                

                List<SVMXC__Quote__c> updatedquoteList = new List<SVMXC__Quote__c>();
               
                    if(primaryQuote != null && attachmentIdSet.size() > 0)
                    {
                       Opportunity newOpp= insertOppContactRole(primaryQuote, attachmentIdSet);
                       System.debug('Opp Id -->'+newOpp);
                                               
                        for(AttachmentWrapper wrapper : attachmentWrapperList)
                        {
                            if(wrapper.isSelected) 
                            {
                                wrapper.contract.SVMX_Opportunity__c = newOpp.id;
                                updatedquoteList.add(wrapper.contract);
                            }
                        }
                    }
                    else
                    {
                    ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.No_Quote_Attached));                        
                    }
                //}

                if(attachmentIdSet.size() > 0 && quoteList.size() > 0)
                {
                    List<Attachment> attachmentList = attachmentListMethod(attachmentIdSet);      
                    

                    System.debug('....... Addresses ' +address);

                    Messaging.SingleEmailMessage QuoteMail = new Messaging.SingleEmailMessage ();
                    QuoteMail.setSaveAsActivity(true);
                    QuoteMail.setToAddresses(address);            
                    QuoteMail.setSenderDisplayName('DORMA');
                    QuoteMail.setSubject('Contract Quote Email');
                    QuoteMail.setPlainTextBody('Contract Quotes');
                    
                
                    Integer noOfAttachments = attachmentList.size();
                    Messaging.EmailFileAttachment[] fileAttachments = new Messaging.EmailFileAttachment[noOfAttachments];
                    Integer i = 0;
                    
                    for(Attachment attachment : attachmentList)
                    {
                        Messaging.EmailFileAttachment fileAttachment = new Messaging.EmailFileAttachment();
                        fileAttachment.setBody(attachment.Body);
                        fileAttachment.ContentType='application/x-pdf';
                        fileAttachment.Inline=true;     
                        fileAttachments[i++] = fileAttachment;
                    }
                    System.debug('....... fileAttachments ' +fileAttachments); 
                    QuoteMail.setFileAttachments(fileAttachments);
                    System.debug('....... QuoteMail ' +QuoteMail);    
                    try{
                         Messaging.sendEmail(new Messaging.SingleEmailMessage[] {QuoteMail});
                            
                            
                        for(SVMXC__Quote__c  quote : updatedquoteList)
                        {
                            if(quote.Attachments != null && quote.Attachments.size() > 0)
                            {
                                quote.SVMXC__Status__c = 'Quote Sent';
                            }
                        }
                        System.debug('....... updatedquoteList ' +updatedquoteList);                        
                        update updatedquoteList;
                        quoteNumber = null;
                        quoteList = new List<SVMXC__Quote__c  >();      
                    }Catch(exception e)
                    {
                        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.Email_Error + e));
                    }
                                       
                 }
                 else
                 {
                      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR,Label.No_Quote_Document));               
                 }
                 
             }
             
             return null;
        }

        public PageReference checkAllValue()
        {            
            for(AttachmentWrapper wrapper: attachmentWrapperList)
            {
                if(isCheckedAll)
                {
                   // if(wrapper.quote.SVMXC__Status__c != 'Quote Sent')
                        wrapper.isSelected  = true;
                }
                else
                    wrapper.isSelected = false;
            }

            return null;
        }
        class AttachmentWrapper {

            public SVMXC__Quote__c contract {get ;set;}
            public boolean isSelected {get ; set;}
            public boolean isOpportunityAttached {get;set;}
            //public boolean isPrimaryQuote {get;set;}

            public AttachmentWrapper (SVMXC__Quote__c contract)
            {
                this.contract = contract;
                isSelected = false;
            }

        }


        public static List<Attachment> attachmentListMethod(Set<Id> attachmentIdSet){
            List<Attachment> attachmentList = [Select Id, Name, Body,Parent.Lastname, Parent.Firstname from Attachment 
                                                Where ID in :attachmentIdSet];

                return attachmentList;
        }


        public static Opportunity insertOppContactRole(SVMXC__Quote__c primaryQuote, Set<Id> attachmentIdSet){


                        Set<Id> quoteIdSet = new Set<Id>();
                        Opportunity newOpportunity = new Opportunity();
                        
                        newOpportunity.StageName = 'Quotation';
                        newOpportunity.CloseDate = System.today();
                        newOpportunity.Name = primaryQuote.Name;
                        newOpportunity.Type = 'Service';
                        newOpportunity.Amount = primaryQuote.SVMXC__Quote_Amount2__c;
                        newOpportunity.AccountId = primaryQuote.SVMXC__Company__c;
                        newOpportunity.Type = 'Service Quote';

                        insert newOpportunity;
                        System.debug('newOpportunity Id -->'+newOpportunity); 

                        quoteIdSet.add(primaryQuote.id);
                        attachmentToOpp(attachmentIdSet,newOpportunity.id);    

                        return newOpportunity;
    }


    public static void attachmentToOpp(Set<Id> attachmentIdSet, Id oppId){

        List<Attachment> insAttachList = new List<Attachment>();
        List<Attachment> attachmentList = attachmentListMethod(attachmentIdSet);
        List<Opportunity> newOpportunityQuery = [Select Id from Opportunity where Id =:oppId];

        for(Opportunity opp : newOpportunityQuery){
            for(Attachment attachment : attachmentList){
                Attachment att = new Attachment(name = attachment.name, body = attachment.body, parentid = oppId);
                    insAttachList.add(att);
            }

            if(insAttachList.size() > 0)
            {
                insert insAttachList;
            }
        }
    }
}