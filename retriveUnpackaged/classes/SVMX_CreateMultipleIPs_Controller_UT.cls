/************************************************************************************************************
Description: UT for SVMX_CreateMultipleIPs_Controller class.
 
Author: Keshava Prasad
Date: 19-04-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest(SeeAllData = false)
public class SVMX_CreateMultipleIPs_Controller_UT {
    
    public static testMethod void multipleIPS() {
      Account account = SVMX_TestUtility.CreateAccount('Testing Account', 'Norway',true) ;
      account.SAPNumber__c = 'P1234' ;
        update account ;
            
      Contact contact = SVMX_TestUtility.CreateContact(account.id,true) ;
    
        
         SVMXC__Site__c  location = SVMX_TestUtility.CreateLocation('Service Location',account.id,true);
        location.SVMXC_SAP_Storage_Location__c = '1234667';
        location.SVMXC_Plant_Number__c = 'P122';
        location.SVMXC__Stocking_Location__c = true;
        location.SVMXC__Location_Type__c = 'External';
        location.SVMXC__Country__c = 'Norway' ;
        //location.id = partsReq.SVMX_From_SAP_Storage_Location__c ;
        update location;
        
     
    
        

        Product2 product = SVMX_TestUtility.CreateProduct('Test prod',false);
        product.SVMX_SAP_Product_Type__c = 'Stock Part';
        product.ClassificationMake__c='A1';
        product.ClassificationModel__c = 'test';
        product.ClassificationServiceFamily__c ='Accordion Shutter';
        insert product;

        ProductDetail__c prd = new ProductDetail__c();
        prd.ProductRef__c = product.id;
        prd.Language__c ='NO';
        insert prd;

        SVMXC__Installed_Product__c installedProduct = SVMX_TestUtility.CreateInstalledProduct(account.id,null,location.id,false);
        installedProduct.SVMXC__Product__c = product.id;
        installedProduct.SVMX_ProductDetail__c = prd.id;
        insert installedProduct;
  
      

       PageReference pageRef = Page.SVMX_CreateMultipleIPs ;
       pageRef.getParameters().put('Id', location.id) ;
          Test.setCurrentPage(pageRef);
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(location);
         SVMX_CreateMultipleIPs_Controller sapAvailability = new SVMX_CreateMultipleIPs_Controller(sc) ;
          SVMX_CreateMultipleIPs_Controller.IPWrapper ip = new SVMX_CreateMultipleIPs_Controller.IPWrapper();
          ip.ipToInstall = installedProduct;
          ip.id= 1;
          ip.quantity =1;

          sapAvailability.ipWrapperList.add(ip);
         sapAvailability.location = location;
         sapAvailability.loc = location;
         sapAvailability.account= account;
   
                 
        sapAvailability.addRow();
        sapAvailability.createIP();
        //sapAvailability.change();
        sapAvailability.cancel(); 
        pageRef.getParameters().put('rowId', '1') ;
        sapAvailability.deleterow();
        
        Test.stopTest();
    
    
    }
}