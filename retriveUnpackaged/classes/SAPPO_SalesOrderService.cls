/**
 * Created by rebmangu on 12/02/2018.
 */

public class SAPPO_SalesOrderService extends SAPPO_SalesOrderAbstract{

    private SAPPO_SalesOrderRequest.SalesOrderSimulateResponse response;
    private SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder request;
    private SalesOrderSimulationWrapper wrapper;

    public SAPPO_SalesOrderService(){
        this.Utility = new SAPPO_SalesOrderService.UTILITY();
    }

    /**********************************************************************
     *
     *          ORDER Simulation
     *
     ***********************************************************************/

    public SalesOrderResultWrapper syncSimulatedSalesOrder(SalesOrderSimulationWrapper wrapper,Boolean updateEnabled){
        List<String> missingFields = this.validateMandatoryFields(wrapper);
        if(missingFields.size() > 0){
            SalesOrderResultWrapper result = new SalesOrderResultWrapper();
            result.status = STANDARD_ERROR; //400
            result.message = 'Missing fields: '+String.join(missingFields,' | ');
            return result;
        }
        this.wrapper = wrapper;

        SAPPO_SalesOrderRequest.SalesOrderRequest request = new SAPPO_SalesOrderRequest.SalesOrderRequest();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponse sapResponse = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponse();

        // Header
        request.MessageHeader   = new SAPPO_CommonDataTypes.BasicMessageHeader();
        request.MessageHeader.ID = new SAPPO_CommonDataTypes.BusinessDocumentMessageID();
        request.MessageHeader.ID.Content = (Id)wrapper.quote.get('Id'); //wrapper.quote.Id;

        request.MessageHeader.BusinessScope = new List<SAPPO_CommonDataTypes.BusinessScope>();
        request.MessageHeader.BusinessScope.add(new SAPPO_CommonDataTypes.BusinessScope('SFG',wrapper.quote.ExternalQuoteNumber__c,'Order'));

		
        try{
            request.SalesOrder = this.createSimulatedSalesOrder(wrapper);
            sapResponse = new SAPPO_SalesOrderRequest.SalesOrderOutPort().SimulateSync(request.MessageHeader,request.SalesOrder);
        }catch(Exception e){
              SalesOrderResultWrapper result = new SalesOrderResultWrapper();
        							  result.data = new List<Map<String,String>>();
        							  result.status = 400;
            						  result.message = 'Line '+e.getLineNumber()+' | '+e.getMessage();
            return result;
        }
        
        //System.debug(JSON.serialize(this.response));
        return this.handleSimulationResponse(sapResponse,updateEnabled);
    }

    private SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder createSimulatedSalesOrder(SalesOrderSimulationWrapper wrapper){
        SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder SalesOrder  = new SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder();

        SalesOrder.ProcessingTypeCode = 'SV';
        SalesOrder.BusinessProcessVariantTypeCode = 'SV08';



        //TODO Check if we need the "Business Transaction Document Reference & Buyer Documents"
        //        SalesOrder.BuyerDocument = new SAPPO_SalesOrderRequest.SalesDocumentRequestBuyerDocument();
        //        SalesOrder.BuyerDocument.ID = new SAPPO_CommonDataTypes.BusinessTransactionDocumentID();
        //        SalesOrder.BuyerDocument.ID.Content = Order.Account.SAPNumber__c;

        //SalesOrder.BusinessTransactionDocumentReference = new List<SAPPO_SalesOrderRequest.SalesDocumentRequestBusiTransDocRef>();


        // Buyer Party
        SalesOrder.BuyerParty = new SAPPO_SalesOrderRequest.SalesOrderRequestBuyerParty();
        SalesOrder.BuyerParty.InternalID = new SAPPO_CommonDataTypes.PartyInternalID();
        SalesOrder.BuyerParty.InternalID.Content = wrapper.account.SAPNumber__c;

        // Seller Party
        SalesOrder.SellerParty = new SAPPO_SalesOrderRequest.SalesOrderRequestSellerParty();
        SalesOrder.SellerParty.StandardID = new SAPPO_CommonDataTypes.PartyStandardID();
        SalesOrder.SellerParty.StandardID.Content = '9921226005491';//wrapper.quote.GLN__c; // GLN of the seller
        SalesOrder.SellerParty.StandardID.SchemeAgencyID = '009';

        // Sales Channel
//        SalesOrder.SalesChannel = new SAPPO_SalesOrderRequest.SalesDocumentRequestSalesChannel();
//        SalesOrder.SalesChannel.SalesChannelCode = '';

        // Date Terms
        SalesOrder.DateTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestDateTerms();
        SalesOrder.DateTerms.RequestDate = String.ValueOf(wrapper.quote.ScheduledDate__c);

        // Pricing Terms
        SalesOrder.PricingTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestPricingTerms();
        SalesOrder.PricingTerms.CurrencyCode = wrapper.account.CurrencyIsoCode;


        // Create List to store the items (Work Details). This must be done outside the loop below, otherwise the list will be overwritten
        salesOrder.Item = new List<SAPPO_SalesOrderRequest.SalesOrderRequestItem>();
        Integer index = 1;
        for(QuoteLineItem item : wrapper.quoteLineItems){
            SAPPO_SalesOrderRequest.SalesOrderRequestItem SalesOrderItem = new SAPPO_SalesOrderRequest.SalesOrderRequestItem();
            SalesOrderItem.ID = String.valueOf(index); // We can't use the position because the serviceMax team is using it too: max Char = 6 digits

            // Product
            SalesOrderItem.Product = new SAPPO_SalesOrderRequest.SalesOrderRequestItemProduct();
            SalesOrderItem.Product.InternalID = new SAPPO_CommonDataTypes.ProductInternalID();
            SalesOrderItem.Product.InternalID.Content = item.Product2.SAPNumber__c; // Check if can remove the trim later
            System.debug('a -> '+SalesOrderItem.Product.InternalID.Content);
            //DateTerms
            SalesOrderItem.DateTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestItemDateTerms();
            SalesOrderItem.DateTerms.RequestDate = String.valueOf(wrapper.quote.ScheduledDate__c);

            // PRICING is null, as it's a simulation


            // QUANTITY
            SalesOrderItem.TotalValues = new SAPPO_SalesOrderRequest.SalesOrderRequestItemTotalValues();
            SalesOrderItem.TotalValues.RequestedQuantity = new SAPPO_CommonDataTypes.Quantity();
            SalesOrderItem.TotalValues.RequestedQuantity.Content    = String.valueOf(item.Quantity);
            SalesOrderItem.TotalValues.RequestedQuantity.UnitCode   = item.Product2.QuantityUnitOfMeasure;

            SalesOrder.Item.add(SalesOrderItem);

            index++;
        }

        return SalesOrder;
    }

    private SalesOrderResultWrapper handleSimulationResponse(SAPPO_SalesOrderRequest.SalesOrderSimulateResponse response,Boolean updateEnabled){

        //        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseBuyerParty buyerParty                         = response.SalesOrder.BuyerParty;
        //        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseGoodsRecipientParty goodsRecipientParty       = response.SalesOrder.GoodsRecipientParty;
        //        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseParty[] party                                 = response.SalesOrder.Party;
        //        SAPPO_SalesOrderRequest.SalesOrderSimulateResponsePriceComponent[] priceComponent               = response.SalesOrder.PriceComponent;
        //        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseDeliveryTerms deliveryTerms                   = response.SalesOrder.DeliveryTerms;
        //        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseTotalValues totalValues                       = response.SalesOrder.TotalValues;
        //        SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem[] items                                   = response.SalesOrder.Item;

        //System.debug(JSON.serialize(response));

        SalesOrderResultWrapper result = new SalesOrderResultWrapper();
                                result.data = new List<Map<String,String>>();
                                result.status = 200;


        // manage prices
        try{
            List<PricebookEntry> pricebookEntriesToUpdate   = new List<PricebookEntry>();

            String standardPricebookId = Utility.getStandardPriceBookId();



            for(SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem item : response.SalesOrder.Item){
                //String internalId = item.Product.InternalID.Content.replaceAll('[0]+(.*)','$1');

                // Initialization of the priceMapping
                Map<String,String> priceMapping = new Map<String,String>{
                        'MWST' => '', // Taxe
                        'ADDD' => '', // AdditionalDiscount
                        'BASD' => '', // BaseDiscount
                        'SPED' => '', // SpecialDiscount
                        'GRSP' => '', // UnitPrice
                        'ZD04' => '', // Discount ?
                        'SKTO' => '', // Total price with taxe
                        'INTP' => ''  // Internal price
                };

                for(SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent priceComponent :item.priceComponent){
                    if(priceMapping.containsKey(priceComponent.PriceSpecificationElementTypeCode)){
                        priceMapping.put(priceComponent.PriceSpecificationElementTypeCode,priceComponent.Rate.DecimalValue);
                    }
                }


                String availableOnDate = (item.ScheduleLine == null || item.ScheduleLine[0] == null || String.isBlank(item.ScheduleLine[0].Date_x))?null:item.ScheduleLine[0].Date_x;
                Decimal totalPrice      = Decimal.valueOf(String.isBlank(item.TotalValues.NetAmount.Content)?'0':item.TotalValues.NetAmount.Content);
                Integer quantity        = Integer.valueOf(String.isBlank(item.TotalValues.RequestedQuantity.Content)?'0':item.TotalValues.RequestedQuantity.Content);
                System.debug('totalPrice --> '+totalPrice);


                for(SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemScheduleLine scheduleLine:item.ScheduleLine){
                    if(scheduleLine.ConfirmedQuantity.Content == String.valueof(quantity)){
                        availableOnDate = scheduleLine.Date_x;
                    }
                }

                Decimal netAmount       = String.isBlank(priceMapping.get('MWST'))? totalPrice :(totalPrice + totalPrice*Integer.valueOf(priceMapping.get('MWST'))/100) ;



                Map<String,String>  resultItem = new Map<String,String>{
                        'MaterialId'            =>  item.Product.InternalID.Content,
                        'CurrencyIsoCode'       =>  item.TotalValues.NetAmount.CurrencyCode,
                        'RequestedQuantity'     =>  String.valueOf(quantity),
                        'ConfirmedQuantity'     =>  item.TotalValues.ConfirmedQuantity.Content,
                        'BaseDiscount'          =>  String.isBlank(priceMapping.get('BASD'))?'0':priceMapping.get('BASD'),
                        'AdditionalDiscount'    =>  String.isBlank(priceMapping.get('ADDD'))?'0':priceMapping.get('ADDD'),
                        'SpecialDiscount'       =>  String.isBlank(priceMapping.get('SPED'))?'0':priceMapping.get('SPED'),
                        'TotalDiscount'         =>  String.isBlank(priceMapping.get('ZD04'))?'0':priceMapping.get('ZD04'),
                        'UnitPrice'             =>  String.isBlank(priceMapping.get('GRSP'))?'0':priceMapping.get('GRSP'),
                        'NetAmount'             =>  String.valueOf(netAmount), // This is the total price
                        'InternalPrice'         =>  String.isBlank(priceMapping.get('INTP'))?'0':priceMapping.get('INTP'),
                        'Tax'                   =>  String.isBlank(priceMapping.get('MWST'))?'0':priceMapping.get('MWST'),
                        'AvailableOnDate'       =>  availableOnDate
                };
                result.data.add(resultItem);


                if(updateEnabled && String.isNotBlank(resultItem.get('UnitPrice'))){

                    // Loop here over the list of QuoteLineItem for each products
                    QuoteLineItem quoteLineItem = this.wrapper.QuoteLineItems[Integer.valueOf(item.ID) - 1];

                    if(quoteLineItem.CurrencyIsoCode != item.TotalValues.NetAmount.CurrencyCode){
                        throw new SalesOrderException('The currency from SAP is : '+item.TotalValues.NetAmount.CurrencyCode);
                    }
                    quoteLineItem.UnitPrice         = Decimal.valueOf(resultItem.get('UnitPrice'));
                    quoteLineItem.SAPPrice__c       = Decimal.valueOf(resultItem.get('UnitPrice'));
                    quoteLineItem.Discount          = Math.abs(Decimal.valueOf(resultItem.get('TotalDiscount')));
                    quoteLineItem.Tax__c            = Decimal.valueOf(resultItem.get('Tax'));
                    quoteLineItem.ConfirmedDate__c  = (resultItem.get('AvailableOnDate')!= null?Date.valueOf(resultItem.get('AvailableOnDate')):null);

                    // SAP Informations:

                    Map<String,List<Object>> SAPInformationJSON = new Map<String,List<Object>>();

//                    SAPInformationJSON.put('ZP00',new List<Object>{
//                            new Map<String,Object>{
//                                    'key' => 'Gross price',
//                                    'amount' => resultItem.get('UnitPrice'),
//                                    'currency' => resultItem.get('CurrencyIsoCode')
//                            },
//                            new Map<String,Object>{
//                                    'key' => 'Subtotal',
//                                    'amount' => null,
//                                    'currency' => resultItem.get('CurrencyIsoCode')
//                            },
//                            new Map<String,Object>{
//                                    'key' => 'Net price',
//                                    'amount' => resultItem.get('UnitPrice'),
//                                    'currency' => resultItem.get('CurrencyIsoCode')
//                            },
//                            new Map<String,Object>{
//                                    'key' => 'Total excl. Taxe',
//                                    'amount' => resultItem.get('UnitPrice'),
//                                    'currency' => resultItem.get('CurrencyIsoCode')
//                            },
//                            new Map<String,Object>{
//                                    'key' => 'Total',
//                                    'amount' => resultItem.get('UnitPrice'),
//                                    'currency' => resultItem.get('CurrencyIsoCode')
//                            },
//                            new Map<String,Object>{
//                                    'key' => 'Total incl. Freight',
//                                    'amount' => resultItem.get('UnitPrice'),
//                                    'currency' => resultItem.get('CurrencyIsoCode')
//                            }
//                    });
//                    SAPInformationJSON.put('VAT',new List<Object>());
//                    SAPInformationJSON.put('VPRS',new List<Object>());

                    quoteLineItem.SAPInformationsJSON__c = JSON.serialize(item);


                    //quoteLineItemsToUpdate.add(quoteLineItem);
                    /*   Pricebook update removed, doesn't make sense since we gonna have a pricebook integration
                        pricebookEntriesToUpdate.add(new PricebookEntry(
                                ExternalId__c   = 'standard-'+quoteLineItem.Product2.ExternalId__c+'-'+quoteLineItem.CurrencyIsoCode,
                                UnitPrice       = quoteLineItem.UnitPrice,
                                Pricebook2Id    = standardPricebookId,
                                Product2Id      = quoteLineItem.Product2Id,
                                CurrencyIsoCode = quoteLineItem.CurrencyIsoCode,
                                IsActive        = true
                        ));
                    */
                }
            }

            if(updateEnabled){
                update this.wrapper.QuoteLineItems;

                /*
                Map<String,List<PricebookEntry>> pricebookEntryMap = Utility.handleUpsertPricebookEntries(pricebookEntriesToUpdate);
                System.debug(pricebookEntryMap);
                insert pricebookEntryMap.get('insert');
                update pricebookEntryMap.get('update');
                */
            }


        }catch(Exception e){
            this.handleExceptions(response,result,e);
        }
        System.debug('##### SalesOrder Simulation #####');
        system.debug(result);
        return result;


    }



    /**********************************************************************
     *
     *          ATP Check
     *
     ***********************************************************************/

    public SalesOrderResultWrapper getATPCheck(String buyerSAPNumber,Date requestedDate,String sellerSAPNumber,Map<String,ATPCheckMapping> materialMapping){

        SAPPO_SalesOrderRequest.SalesOrderRequest request = new SAPPO_SalesOrderRequest.SalesOrderRequest();
        SAPPO_SalesOrderRequest.SalesOrderSimulateResponse sapResponse = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponse();

        // Header
        request.MessageHeader   = new SAPPO_CommonDataTypes.BasicMessageHeader();
        request.MessageHeader.ID = new SAPPO_CommonDataTypes.BusinessDocumentMessageID();
        request.MessageHeader.ID.Content = buyerSAPNumber+'-'+sellerSAPNumber+'-'+String.valueOf(requestedDate); //wrapper.quote.Id;

        request.MessageHeader.BusinessScope = new List<SAPPO_CommonDataTypes.BusinessScope>();
        request.MessageHeader.BusinessScope.add(new SAPPO_CommonDataTypes.BusinessScope('SFG',buyerSAPNumber+'-'+sellerSAPNumber+'-'+String.valueOf(requestedDate),'Order'));

        // Trim the leading 0 to be sure that we match the ids on both side
        Map<String,ATPCheckMapping> materialMappingTrim = new Map<String,ATPCheckMapping>();
        for(String key : materialMapping.keySet()){
            String internalId = key.replaceAll('^0*','');
            materialMappingTrim.put(internalId,materialMapping.get(key));
        }

        try{
            request.SalesOrder = this.createSimulatedSalesOrderForATPCheck(buyerSAPNumber,requestedDate,sellerSAPNumber,materialMapping);
            sapResponse = new SAPPO_SalesOrderRequest.SalesOrderOutPort().SimulateSync(request.MessageHeader,request.SalesOrder);
        }catch(Exception e){
            SalesOrderResultWrapper result = new SalesOrderResultWrapper();
                                    result.data = new List<Map<String,String>>();
                                    result.status = 400;
                                    result.message = 'Line '+e.getLineNumber()+' | '+e.getMessage();
            return result;
        }

        return this.handleSimulationResponseForATPCheck(sapResponse,materialMappingTrim);
    }

    private SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder createSimulatedSalesOrderForATPCheck(String buyerSAPNumber,Date requestedDate,String sellerSAPNumber,Map<String,ATPCheckMapping> materialMapping){

        System.debug(new Map<String,String>{
                'buyerSAPNumber'    =>  buyerSAPNumber,
                'requestedDate'     =>  String.valueOf(requestedDate),
                'sellerSAPNumber'   =>  String.valueOf(sellerSAPNumber)
        });
        System.debug(materialMapping);

        SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder SalesOrder  = new SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder();

        SalesOrder.ProcessingTypeCode = 'SV';
        SalesOrder.BusinessProcessVariantTypeCode = 'SV08';

        // Buyer Party
        SalesOrder.BuyerParty = new SAPPO_SalesOrderRequest.SalesOrderRequestBuyerParty();
        SalesOrder.BuyerParty.InternalID = new SAPPO_CommonDataTypes.PartyInternalID();
        SalesOrder.BuyerParty.InternalID.Content = buyerSAPNumber;

        // Seller Party
        SalesOrder.SellerParty = new SAPPO_SalesOrderRequest.SalesOrderRequestSellerParty();
        SalesOrder.SellerParty.StandardID = new SAPPO_CommonDataTypes.PartyStandardID();
        SalesOrder.SellerParty.StandardID.Content = sellerSAPNumber;
        SalesOrder.SellerParty.StandardID.SchemeAgencyID = '009';

        // Date Terms
        SalesOrder.DateTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestDateTerms();
        SalesOrder.DateTerms.RequestDate = String.ValueOf(requestedDate);

        // Create List to store the items (Work Details). This must be done outside the loop below, otherwise the list will be overwritten
        salesOrder.Item = new List<SAPPO_SalesOrderRequest.SalesOrderRequestItem>();

        for(String key : materialMapping.keySet()){
            SAPPO_SalesOrderRequest.SalesOrderRequestItem SalesOrderItem = new SAPPO_SalesOrderRequest.SalesOrderRequestItem();

            // Product
            SalesOrderItem.Product = new SAPPO_SalesOrderRequest.SalesOrderRequestItemProduct();
            SalesOrderItem.Product.InternalID = new SAPPO_CommonDataTypes.ProductInternalID();
            SalesOrderItem.Product.InternalID.Content = key;

            //DateTerms
            SalesOrderItem.DateTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestItemDateTerms();
            SalesOrderItem.DateTerms.RequestDate = String.valueOf(requestedDate);

            // QUANTITY
            SalesOrderItem.TotalValues = new SAPPO_SalesOrderRequest.SalesOrderRequestItemTotalValues();
            SalesOrderItem.TotalValues.RequestedQuantity = new SAPPO_CommonDataTypes.Quantity();
            SalesOrderItem.TotalValues.RequestedQuantity.Content    = String.valueOf(materialMapping.get(key).quantity);

            // PLANT & LOCATION

            SalesOrderItem.DeliveryTerms = new SAPPO_SalesOrderRequest.SalesOrderRequestItemDeliveryTerms();
            SalesOrderItem.DeliveryTerms.DeliveryPlantID    = materialMapping.get(key).deliveryPlantID;
            SalesOrderItem.DeliveryTerms.StorageLocationID  = materialMapping.get(key).storageLocationID;

            SalesOrder.Item.add(SalesOrderItem);
        }

        return SalesOrder;
    }

    private SalesOrderResultWrapper handleSimulationResponseForATPCheck(SAPPO_SalesOrderRequest.SalesOrderSimulateResponse response,Map<String,ATPCheckMapping> materialMapping){
        SalesOrderResultWrapper result = new SalesOrderResultWrapper();
        result.data = new List<Map<String,String>>();
        result.status = 200;
        try{
            for(SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem item : response.SalesOrder.Item){

                String internalId = item.Product.InternalID.Content.replaceAll('^0*','');
                Integer quantity = materialMapping.get(internalId).quantity;
                String availableOnDate;
                for(SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemScheduleLine scheduleLine:item.ScheduleLine){

                    if(Integer.valueOf(scheduleLine.ConfirmedQuantity.Content) == quantity){
                        availableOnDate = scheduleLine.Date_x;

                    }
                }
                //String internalId = item.Product.InternalID.Content.replaceAll('[0]+(.*)','$1');
                Map<String,String>  resultItem = new Map<String,String>();
                resultItem.put('MaterialId',item.Product.InternalID.Content);
                resultItem.put('RequestedQuantity',String.valueOf(quantity));
                resultItem.put('ConfirmedQuantity',item.TotalValues.ConfirmedQuantity.Content);
                resultItem.put('NetAmount',item.TotalValues.NetAmount.Content);
                resultItem.put('AvailableOnDate',availableOnDate);
                // if on requestedDate => reQuantity == coQuantity => AvailableOnDate = requestedDate

                result.data.add(resultItem);
            }
        }catch(Exception e){
            this.handleExceptions(response,result,e);
        }
        System.debug('##### ATP Check Response #####');
        system.debug(result);

        return result;


    }

    /*************************************************************************************
    *
    *          METHODS
    *
    *************************************************************************************/

    public override void handleExceptions(Object paramResponse,Object paramResult,Exception e){

        SAPPO_SalesOrderRequest.SalesOrderSimulateResponse sapResponse = (SAPPO_SalesOrderRequest.SalesOrderSimulateResponse)paramResponse;
        SalesOrderResultWrapper result = (SalesOrderResultWrapper)paramResult;

        result.status = STANDARD_ERROR; //400
        if(sapResponse.Log != null){
            List<String> errors = new List<String>();
            for(SAPPO_EsmEdt.LogItem item : sapResponse.Log.Item){
                errors.add(item.Note);
            }
            result.message = String.join(errors,' | ');
        }else{
            result.message = 'Line '+e.getLineNumber()+' | '+e.getMessage();
        }
    }

    public override List<String> validateMandatoryFields(Object obj){

        SalesOrderSimulationWrapper wrapper = (SalesOrderSimulationWrapper)obj;

        Boolean result = true;
        Set<String> errors = new Set<String>();

        Map<String,List<String>> fieldsMapping = this.getMandatoryFields();

        // Account
        List<String> fields = fieldsMapping.get('account');
        Map<String,Object> accountMap = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(wrapper.account));
        for(String field : fields){
            //System.debug(this.getValue(wrapper.account,field));
            if(!Utility.getValue(accountMap,field)){
                result = false;
                errors.add('Account.'+field);
            }
        }
        // QuoteLineItems
        if(result == true){
            fields = fieldsMapping.get('quoteLineItems');

            for(QuoteLineItem item : wrapper.quoteLineItems){
                Map<String,Object> quoteLineItemMap = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(item));
                for(String field : fields){
                    if(!Utility.getValue(quoteLineItemMap,field)){
                        result = false;
                        errors.add('QuoteLineItem.'+field);
                    }
                }
            }
        }
        // Quote
        if(result == true){
            fields = fieldsMapping.get('quote');
            Map<String,Object> quoteMap = (Map<String,Object>)JSON.deserializeUntyped(JSON.serialize(wrapper.quote));
            for(String field : fields){
                if(!Utility.getValue(quoteMap,field)){
                    result = false;
                    errors.add('Quote.'+field);
                }
            }
        }


        //System.debug(errors);
        return new List<String>(errors);
    }

    protected override Map<String,List<String>> getMandatoryFields(){
        return new Map<String,List<String>>{
                'account'           =>new List<String>{'SAPNumber__c','CurrencyIsoCode'},
                'quote'             =>new List<String>{'ScheduledDate__c'},
                'quoteLineItems'    =>new List<String>{'Product2.SAPNumber__c','Quantity','Product2.ExternalId__c'}
        };
    }

    /*************************************************************************************
    *
    *          CLASSES
    *
    *************************************************************************************/

    public class SalesOrderSimulationWrapper {
        public Account account; // for Informations
        public Quote quote; // for Informations
        public List<QuoteLineItem> quoteLineItems; // Updatable
    }

    //TODO: deprecated, it's only for servicemax. We should use SAPPO_SalesOrderAbstract.Response
    public class SalesOrderResultWrapper {
        @AuraEnabled public Integer status;
        @AuraEnabled public String message;
        @AuraEnabled public List<Map<String,String>> data;
    }

    public class ATPCheckMapping {
        public Integer quantity;
        public String storageLocationID;
        public String deliveryPlantID;

        public ATPCheckMapping(Integer quantity,String storageLocationID,String deliveryPlantID){
            this.quantity = quantity;
            this.storageLocationID = storageLocationID;
            this.deliveryPlantID = deliveryPlantID;
        }
    }
}