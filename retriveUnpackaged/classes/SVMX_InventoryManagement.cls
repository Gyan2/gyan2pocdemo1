public class SVMX_InventoryManagement{
    
    //updateInventoryToInTransit
    public void updateInventoryToInTransit(List<SVMXC__Stock_Transfer_Line__c> newstocktransferLines) {
    
        List<SVMXC__Product_Stock__c> updateProdutStockSourcLoc = new List<SVMXC__Product_Stock__c>();
        List<SVMXC__Product_Stock__c> updateTransitProdutStockSourcLoc = new List<SVMXC__Product_Stock__c>();
        List<SVMXC__Product_Stock__c> createTrasitProdutstockLoc = new List<SVMXC__Product_Stock__c>();
        List<SVMXC__Stock_History__c> stkHstList = new List<SVMXC__Stock_History__c>();
        List<SVMXC__Product_Stock__c> tempValidProdStockListSourceLoc = new List<SVMXC__Product_Stock__c>();
        Set<id> tempProductIdset=new Set<id>();
        Set<id> tempsitesourceIdset=new Set<id>();
        Map<Id,List<SVMXC__Product_Stock__c>> sourceLocationAVAILProdStockMap = new Map<Id,List<SVMXC__Product_Stock__c>>();
        Map<Id,List<SVMXC__Product_Stock__c>> sourceLocationTRANSProdStockMap = new Map<Id,List<SVMXC__Product_Stock__c>>();
                

        for(SVMXC__Stock_Transfer_Line__c tempSTL : newstocktransferLines){
            
            tempProductIdset.add(tempSTL.SVMXC__Product__c);
            tempsitesourceIdset.add(tempSTL.SVMXC_Sender_Stocking_Location__c);
        }


        Map<Id,product2> productMap = new Map<Id,product2>([SELECT Id,Name,SVMXC__Stockable__c FROM product2 
                                                            WHERE id IN : tempProductIdset]);

        
        //Get the Product Stock records for all Products and their respective Locations, add them to List           
        tempValidProdStockListSourceLoc = QueryStock(tempProductIdset,tempsitesourceIdset);

        system.debug('tempValidProdStockListSourceLoc-->'+tempValidProdStockListSourceLoc);
        system.debug('newstocktransferLines-->'+newstocktransferLines);

        // Yogesh - Check here to ensure Product Stock records are returned in tempValidProdStockListSourceLoc, otherwise throw error. 
        //Maybe the Source Location has no Product Stock records at all
        
            for(SVMXC__Product_Stock__c ps : tempValidProdStockListSourceLoc) {

                    List<SVMXC__Product_Stock__c> tempProductStockList = new List<SVMXC__Product_Stock__c>(); 

                    if(ps.SVMXC__Status__c == 'Available') {

                      if(sourceLocationAVAILProdStockMap.containsKey(ps.SVMXC__Location__c)) {

                        tempProductStockList = sourceLocationAVAILProdStockMap.get(ps.SVMXC__Location__c);
                      }

                      tempProductStockList.add(ps);
                      sourceLocationAVAILProdStockMap.put(ps.SVMXC__Location__c, tempProductStockList);
                    }
                    else if (ps.SVMXC__Status__c == 'In Transit') {

                      if(sourceLocationTRANSProdStockMap.containsKey(ps.SVMXC__Location__c)) {

                        tempProductStockList = sourceLocationTRANSProdStockMap.get(ps.SVMXC__Location__c);
                      }

                      tempProductStockList.add(ps) ;
                      sourceLocationTRANSProdStockMap.put(ps.SVMXC__Location__c, tempProductStockList);
                    }

            }
        
      //Iterate through the Stock Transfer Lines and make the Stock Adjustement
      for(SVMXC__Stock_Transfer_Line__c stockLines : newstocktransferLines) {

           if(productMap.get(stockLines.SVMXC__Product__c).SVMXC__Stockable__c == true) {

                 List<SVMXC__Product_Stock__c> productstockAVAILList = new List<SVMXC__Product_Stock__c> ();
                 List<SVMXC__Product_Stock__c> productstockTRANSList = new List<SVMXC__Product_Stock__c> ();
                 List<SVMXC__Product_Stock__c> matchingProductStockAVAILList = new List<SVMXC__Product_Stock__c> ();
                 List<SVMXC__Product_Stock__c> matchingProductStockTRANSList = new List<SVMXC__Product_Stock__c> (); 

                 
                 productstockAVAILList = sourceLocationAVAILProdStockMap.get(stockLines.SVMXC_Sender_Stocking_Location__c);
                 productstockTRANSList = sourceLocationTRANSProdStockMap.get(stockLines.SVMXC_Sender_Stocking_Location__c);

                 // Yogesh - Check here to ensure there are Prodcut Stocks available for this Locatuion, otherwise throw error.
                 //Maybe the Source Location has no Product Stock at all

                 if(productstockAVAILList != null) {
                   
                   for(SVMXC__Product_Stock__c ps : productstockAVAILList) {

                    if(ps.SVMXC__Product__c == stockLines.SVMXC__Product__c) {

                      matchingProductStockAVAILList.add(ps);
                    } 
                   }
                 }

                 if(productstockTRANSList != null) {
                   
                   for(SVMXC__Product_Stock__c ps : productstockTRANSList) {

                    if(ps.SVMXC__Product__c == stockLines.SVMXC__Product__c) {

                      matchingProductStockTRANSList.add(ps);
                    } 
                   }
                 }
                 
                 // Yogesh - Check here to ensure there are Prodcut Stocks available for this Product, otherwise throw error.
                 //Maybe the Source Location has no Product Stock for this Product

                 if(!matchingProductStockAVAILList.isEmpty()) {
                   
                   for(SVMXC__Product_Stock__c productstck : matchingProductStockAVAILList) {
                      
                      //Adjust Prodcut Stock available record
                      SVMXC__Product_Stock__c stk =new SVMXC__Product_Stock__c();
                      stk.id = productstck.id;
                      stk.SVMXC__Quantity2__c = productstck.SVMXC__Quantity2__c - stockLines.SVMXC__Quantity_Transferred2__c;

                      updateProdutStockSourcLoc.add(stk);

                      //Create Stock History record
                      SVMXC__Stock_History__c StkHst = new SVMXC__Stock_History__c();
                      StkHst.SVMXC__Product_Stock__c = productstck.id;
                      StkHst.SVMXC__Product__c = stockLines.SVMXC__Product__c;
                      StkHst.SVMXC__Location__c = stockLines.SVMXC_Sender_Stocking_Location__c;
                      StkHst.SVMXC__Status__c = 'Available';    
                      StkHst.SVMXC__Transaction_Quantity2__c = stockLines.SVMXC__Quantity_Transferred2__c;
                      StkHst.SVMXC__Quantity_after_change2__c = productstck.SVMXC__Available_Qty__c-stockLines.SVMXC__Quantity_Transferred2__c;
                      StkHst.SVMXC__Quantity_before_change2__c =productstck.SVMXC__Available_Qty__c;
                      StkHst.SVMXC__Date_Changed__c = System.Now();
                      StkHst.SVMXC__Transaction_Type__c = 'Stock Transfer';  
                      StkHst.SVMXC__Change_Type__c = 'Decrease';
                      StkHst.SVMXC__Changed_By__c = UserInfo.GetUserId(); 
                      StkHst.recordtypeid = FetchRecordTypeID('SVMXC__Stock_History__c','Stock History');              
                      
                      stkHstList.add(StkHst);
                    }
                  }

                  if(!matchingProductStockTRANSList.isEmpty()) {
                    
                    for(SVMXC__Product_Stock__c productstck : matchingProductStockTRANSList) {

                      SVMXC__Product_Stock__c postk = new SVMXC__Product_Stock__c();
                      postk.Id = productstck.Id;
                      postk.SVMXC__Quantity2__c = productstck.SVMXC__Quantity2__c + stockLines.SVMXC__Quantity_Transferred2__c;

                      updateTransitProdutStockSourcLoc.add(postk);

                      //Create Stock History record
                      SVMXC__Stock_History__c StkHstTrans = new SVMXC__Stock_History__c();
                      StkHstTrans.SVMXC__Product_Stock__c = productstck.id;
                      StkHstTrans.SVMXC__Product__c = stockLines.SVMXC__Product__c;
                      StkHstTrans.SVMXC__Location__c = stockLines.SVMXC_Sender_Stocking_Location__c;
                      StkHstTrans.SVMXC__Status__c = 'Available';    
                      StkHstTrans.SVMXC__Transaction_Quantity2__c = stockLines.SVMXC__Quantity_Transferred2__c;
                      StkHstTrans.SVMXC__Quantity_after_change2__c = productstck.SVMXC__Available_Qty__c + stockLines.SVMXC__Quantity_Transferred2__c;
                      StkHstTrans.SVMXC__Quantity_before_change2__c =productstck.SVMXC__Available_Qty__c;
                      StkHstTrans.SVMXC__Date_Changed__c = System.Now();
                      StkHstTrans.SVMXC__Transaction_Type__c = 'Stock Transfer';  
                      StkHstTrans.SVMXC__Change_Type__c = 'Increase';
                      StkHstTrans.SVMXC__Changed_By__c = UserInfo.GetUserId(); 
                      StkHstTrans.recordtypeid = FetchRecordTypeID('SVMXC__Stock_History__c','Stock History');              
                      
                      stkHstList.add(StkHstTrans);
                    }
                  }
                  else {

                    SVMXC__Product_Stock__c stk1 = new SVMXC__Product_Stock__c();
                    stk1.SVMXC__Product__c = stockLines.SVMXC__Product__c;
                    stk1.SVMXC__Location__c = stockLines.SVMXC_Sender_Stocking_Location__c;
                    stk1.SVMXC__Status__c = 'In Transit';
                    stk1.SVMXC__Quantity2__c = stockLines.SVMXC__Quantity_Transferred2__c;
                    
                    createTrasitProdutstockLoc.add(stk1);

                  }
        }
                                   
      }
         

      if(!updateProdutStockSourcLoc.isEmpty()){
              update updateProdutStockSourcLoc;
       }
      if(!updateTransitProdutStockSourcLoc.isEmpty()){
              update updateTransitProdutStockSourcLoc;
       }
       if(!createTrasitProdutstockLoc.isEmpty()){
             insert createTrasitProdutstockLoc;
       }
       if(!stkHstList.isEmpty()){
             insert stkHstList;
       }
         
    }

    //====================================

 //postToInventoryFromStockTransferLine
 public void postToInventoryFromStockTransferLine(List<SVMXC__Stock_Transfer_Line__c> newstocktransferLines){

     list<SVMXC__Product_Stock__c> UpdateProdutStockSourcLoc = new list<SVMXC__Product_Stock__c>();
    list<SVMXC__Product_Stock__c> UpdateProdutsDestinationLoc = new list<SVMXC__Product_Stock__c>();
    list<SVMXC__Product_Stock__c> createProdutsDestinationLoc = new list<SVMXC__Product_Stock__c>();
    list<SVMXC__Stock_History__c> stkHstList = new list<SVMXC__Stock_History__c>();

    list<SVMXC__Product_Stock__c> tempValidProdStockListsourcLoc = new list<SVMXC__Product_Stock__c>();  
    list<SVMXC__Product_Stock__c> tempValidProdStockListDestinaLoc = new list<SVMXC__Product_Stock__c>();  

    Set<id> tempProductIdset = new Set<id>();
    Set<id> tempsitesourceIdset = new Set<id>();
    Set<id> tempsiteDestinationIdset = new Set<id>();

    Map<Id,List<SVMXC__Product_Stock__c>> destinationLocationAVAILProdStockMap = new Map<Id,List<SVMXC__Product_Stock__c>>();
    Map<Id,List<SVMXC__Product_Stock__c>> sourceLocationTRANSProdStockMap = new Map<Id,List<SVMXC__Product_Stock__c>>();
    //Map<Id,SVMXC__Product_Stock__c> createProdutsDestinationLocMap = new Map<Id,SVMXC__Product_Stock__c>();

    for(SVMXC__Stock_Transfer_Line__c tempSTL:newstocktransferLines){
            
        tempProductIdset.add(tempSTL.SVMXC__Product__c);
        tempsitesourceIdset.add(tempSTL.SVMXC_Sender_Stocking_Location__c);
        tempsiteDestinationIdset.add(tempSTL.SVMXC_Receiver_Stocking_Location__c);
    }

    Map<Id,product2> productMap = new Map<Id,product2>([SELECT Id,Name,SVMXC__Stockable__c FROM product2 
                                                            WHERE id IN : tempProductIdset]);



    tempValidProdStockListsourcLoc=QueryStock(tempProductIdset, tempsitesourceIdset);                
    tempValidProdStockListDestinaLoc=QueryStock(tempProductIdset,tempsiteDestinationIdset);


    for(SVMXC__Product_Stock__c ps : tempValidProdStockListsourcLoc) {

        List<SVMXC__Product_Stock__c> tempProductStockList = new List<SVMXC__Product_Stock__c>(); 

        
        if (ps.SVMXC__Status__c == 'In Transit') {

          if(sourceLocationTRANSProdStockMap.containsKey(ps.SVMXC__Location__c)) {

            tempProductStockList = sourceLocationTRANSProdStockMap.get(ps.SVMXC__Location__c);
          }

          tempProductStockList.add(ps) ;
          sourceLocationTRANSProdStockMap.put(ps.SVMXC__Location__c, tempProductStockList);
        }

    }

    for(SVMXC__Product_Stock__c ps : tempValidProdStockListDestinaLoc) {

        List<SVMXC__Product_Stock__c> tempProductStockList = new List<SVMXC__Product_Stock__c>(); 

        
        if (ps.SVMXC__Status__c == 'Available') {

          if(destinationLocationAVAILProdStockMap.containsKey(ps.SVMXC__Location__c)) {

            tempProductStockList = sourceLocationTRANSProdStockMap.get(ps.SVMXC__Location__c);
          }

          tempProductStockList.add(ps) ;
          destinationLocationAVAILProdStockMap.put(ps.SVMXC__Location__c, tempProductStockList);
        }

    }

    for(SVMXC__Stock_Transfer_Line__c stockLines : newstocktransferLines){

          if(productMap.get(stockLines.SVMXC__Product__c).SVMXC__Stockable__c==true){

            List<SVMXC__Product_Stock__c> productstockDestinationAVAILList = new List<SVMXC__Product_Stock__c> ();
            List<SVMXC__Product_Stock__c> productstockSourceTRANSList = new List<SVMXC__Product_Stock__c> ();

            List<SVMXC__Product_Stock__c> matchingProductStockAVAILList = new List<SVMXC__Product_Stock__c> ();
            List<SVMXC__Product_Stock__c> matchingProductStockTRANSList = new List<SVMXC__Product_Stock__c> ();

            productstockDestinationAVAILList = destinationLocationAVAILProdStockMap.get(stockLines.SVMXC_Receiver_Stocking_Location__c);
            productstockSourceTRANSList = sourceLocationTRANSProdStockMap.get(stockLines.SVMXC_Sender_Stocking_Location__c);

            if(productstockDestinationAVAILList != null) {
                   
               for(SVMXC__Product_Stock__c ps : productstockDestinationAVAILList) {

                if(ps.SVMXC__Product__c == stockLines.SVMXC__Product__c) {

                  matchingProductStockAVAILList.add(ps);
                } 
               }
            }

           if(productstockSourceTRANSList != null) {
             
             for(SVMXC__Product_Stock__c ps : productstockSourceTRANSList) {

              if(ps.SVMXC__Product__c == stockLines.SVMXC__Product__c) {

                matchingProductStockTRANSList.add(ps);
              } 
             }
           }


           if(!matchingProductStockTRANSList.isEmpty()) {
                   
                   for(SVMXC__Product_Stock__c productstck : matchingProductStockTRANSList) {
                      
                      //Adjust Prodcut Stock available record
                      SVMXC__Product_Stock__c stk =new SVMXC__Product_Stock__c();
                      stk.id = productstck.id;
                      stk.SVMXC__Quantity2__c = productstck.SVMXC__Available_Qty__c - stockLines.SVMXC__Quantity_Transferred2__c;

                      UpdateProdutStockSourcLoc.add(stk);

                      //Create Stock History record
                      SVMXC__Stock_History__c StkHst = new SVMXC__Stock_History__c();
                      StkHst.SVMXC__Product_Stock__c = productstck.id;
                      StkHst.SVMXC__Product__c = stockLines.SVMXC__Product__c;
                      StkHst.SVMXC__Location__c = stockLines.SVMXC_Sender_Stocking_Location__c;
                      StkHst.SVMXC__Status__c = 'In Transit';    
                      StkHst.SVMXC__Transaction_Quantity2__c = stockLines.SVMXC__Quantity_Transferred2__c;
                      StkHst.SVMXC__Quantity_after_change2__c = productstck.SVMXC__Available_Qty__c-stockLines.SVMXC__Quantity_Transferred2__c;
                      StkHst.SVMXC__Quantity_before_change2__c =productstck.SVMXC__Available_Qty__c;
                      StkHst.SVMXC__Date_Changed__c = System.Now();
                      StkHst.SVMXC__Transaction_Type__c = 'Stock Transfer';  
                      StkHst.SVMXC__Change_Type__c = 'Decrease';
                      StkHst.SVMXC__Changed_By__c = UserInfo.GetUserId(); 
                      StkHst.recordtypeid = FetchRecordTypeID('SVMXC__Stock_History__c','Stock History');              
                      
                      stkHstList.add(StkHst);
                    }
              }

            if(!matchingProductStockAVAILList.isEmpty()) {
                  
                  for(SVMXC__Product_Stock__c productstck : matchingProductStockAVAILList) {

                    SVMXC__Product_Stock__c postk = new SVMXC__Product_Stock__c();
                    postk.Id = productstck.Id;
                    postk.SVMXC__Quantity2__c = productstck.SVMXC__Quantity2__c + stockLines.SVMXC__Quantity_Transferred2__c;

                    UpdateProdutsDestinationLoc.add(postk);

                    //Create Stock History record
                    SVMXC__Stock_History__c StkHstTrans = new SVMXC__Stock_History__c();
                    StkHstTrans.SVMXC__Product_Stock__c = productstck.id;
                    StkHstTrans.SVMXC__Product__c = stockLines.SVMXC__Product__c;
                    StkHstTrans.SVMXC__Location__c = stockLines.SVMXC_Receiver_Stocking_Location__c;
                    StkHstTrans.SVMXC__Status__c = 'Available';    
                    StkHstTrans.SVMXC__Transaction_Quantity2__c = stockLines.SVMXC__Quantity_Transferred2__c;
                    StkHstTrans.SVMXC__Quantity_after_change2__c = productstck.SVMXC__Available_Qty__c + stockLines.SVMXC__Quantity_Transferred2__c;
                    StkHstTrans.SVMXC__Quantity_before_change2__c =productstck.SVMXC__Available_Qty__c;
                    StkHstTrans.SVMXC__Date_Changed__c = System.Now();
                    StkHstTrans.SVMXC__Transaction_Type__c = 'Stock Transfer';  
                    StkHstTrans.SVMXC__Change_Type__c = 'Increase';
                    StkHstTrans.SVMXC__Changed_By__c = UserInfo.GetUserId(); 
                    StkHstTrans.recordtypeid = FetchRecordTypeID('SVMXC__Stock_History__c','Stock History');              
                    
                    stkHstList.add(StkHstTrans);
                  }
                }
                else {

                  SVMXC__Product_Stock__c stk1 = new SVMXC__Product_Stock__c();
                  stk1.SVMXC__Product__c = stockLines.SVMXC__Product__c;
                  stk1.SVMXC__Location__c = stockLines.SVMXC_Receiver_Stocking_Location__c;
                  stk1.SVMXC__Status__c = 'Available';
                  stk1.SVMXC__Quantity2__c = stockLines.SVMXC__Quantity_Transferred2__c;
                  
                  createProdutsDestinationLoc.add(stk1);
                  //createProdutsDestinationLocMap.put(stk1.Id,stk1);

                }
              
          }
      }              
              


      if(!UpdateProdutStockSourcLoc.isEmpty()){
              update UpdateProdutStockSourcLoc;
       }
      if(!UpdateProdutsDestinationLoc.isEmpty()){
              update UpdateProdutsDestinationLoc;
       }
       if(!createProdutsDestinationLoc.isEmpty()){
             insert createProdutsDestinationLoc;
       }

       for(SVMXC__Product_Stock__c productStock : createProdutsDestinationLoc){

            SVMXC__Stock_History__c StkHstTrans = new SVMXC__Stock_History__c();
            StkHstTrans.SVMXC__Product_Stock__c = productStock.id;
            StkHstTrans.SVMXC__Product__c = productStock.SVMXC__Product__c;
            StkHstTrans.SVMXC__Location__c = productStock.SVMXC__Location__c;
            StkHstTrans.SVMXC__Status__c = 'Available';    
            StkHstTrans.SVMXC__Transaction_Quantity2__c = productStock.SVMXC__Quantity2__c;
            StkHstTrans.SVMXC__Quantity_before_change2__c = 0;
            StkHstTrans.SVMXC__Quantity_after_change2__c = productStock.SVMXC__Quantity2__c;
            StkHstTrans.SVMXC__Date_Changed__c = System.Now();
            StkHstTrans.SVMXC__Transaction_Type__c = 'Stock Transfer';  
            StkHstTrans.SVMXC__Change_Type__c = 'Increase';
            StkHstTrans.SVMXC__Changed_By__c = UserInfo.GetUserId(); 
            StkHstTrans.recordtypeid = FetchRecordTypeID('SVMXC__Stock_History__c','Stock History');

            stkHstList.add(StkHstTrans);

       }
       if(!stkHstList.isEmpty()){
             insert stkHstList;
       }      
           
  }

    //==================
    public List<SVMXC__Product_Stock__c> QueryStock(set<id> tempProdIds,set<id> tempSiteIds){

        List<SVMXC__Product_Stock__c> tempProdStockList = [SELECT id, SVMXC__Product__c,SVMXC__Available_Qty__c,
                                                           SVMXC__Quantity2__c,SVMXC__Location__c,SVMXC__Status__c 
                                                           FROM SVMXC__Product_Stock__c 
                                                           WHERE SVMXC__Location__c =:tempSiteIds 
                                                           AND SVMXC__Product__c =:tempProdIds];

        //System.debug('Query All Product Stock related to Parts in Parts Order Line & Destination Location in Parts Order :: Site IDs ::'+tempSiteIds.size()+' :: Prod Ids ::'+tempProdIds.size()+' :: Product Stock ::'+tempProdStockList.size());
        return tempProdStockList;
    }

    public Id FetchRecordTypeID(String ObjectName,String RecordTypeName){

        Recordtype tempRecordTypeId=[select id from Recordtype where Sobjecttype=:ObjectName and Name=:RecordTypeName limit 1];
        return tempRecordTypeId.Id;
    }   

}