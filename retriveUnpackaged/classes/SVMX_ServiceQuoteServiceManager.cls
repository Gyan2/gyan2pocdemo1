/************************************************************************************************************
Description: Service Manager for Service Quote. All the methods and the logic for Service Quote object resides here.

Dependancy: 
    Data Manager:SVMX_ServiceQuoteDataManager.cls
 
Author: Ranjitha S
Date: 16-11-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

public class SVMX_ServiceQuoteServiceManager {

    public static void updateOpp(list<SVMXC__Quote__c> quoteList){
        list<SVMXC__Quote__c> quotes = new list<SVMXC__Quote__c>();
        quotes = SVMX_ServiceQuoteDataManager.oppQuery(quoteList);
        list<opportunity> opportunitesToUpdate = new List<opportunity>();
        
        for(SVMXC__Quote__c sq: quotes){ 
            Opportunity opp = new Opportunity(id = sq.SVMX_Opportunity__c);
            if(sq.SVMXC__Status__c == 'Accepted')
              opp.StageName = 'Closed Won';
            else if (sq.SVMXC__Status__c == 'Rejected')
              opp.StageName = 'Closed Lost';

            opportunitesToUpdate.add(opp);
          }
        
        if(!opportunitesToUpdate.isEmpty())
            SVMX_OpportunityDataManager.updateOpp(opportunitesToUpdate);
    }
    
    public static void updateQuoteFieldsQC(List<SVMXC__Quote__c> newSQList) {
        //List<SVMXC__Quote__c> quoteList = new List<SVMXC__Quote__c>();
        List<SVMXC_Quote_Configuration__c> quoteConfigList = new List<SVMXC_Quote_Configuration__c>() ;
        Set<String> countries = new Set<String>() ;
        for(SVMXC__Quote__c quote : newSQList){
            countries.add(quote.SVMX_PS_Country__c) ;
        }
        
        if(countries.size() > 0){
            quoteConfigList = [SELECT SVMX_Auto_Approval_Threshold__c,SVMX_Country__c,SVMX_No_Of_Approvals__c,SVMX_Special_Approval__c FROM SVMXC_Quote_Configuration__c WHERE SVMX_Country__c IN :countries] ;
            
            if(!quoteConfigList.isEmpty()) {
                for(SVMXC_Quote_Configuration__c quoteConfig : quoteConfigList){
                    for(SVMXC__Quote__c quote : newSQList) {
                        quote.SVMX_Auto_approved_threshold__c = quoteConfig.SVMX_Auto_Approval_Threshold__c ;
                        quote.SVMX_Special_Approval__c = quoteConfig.SVMX_Special_Approval__c ;
                        
                        
                    }
                }
                
            }
           updateQuoteAmountLevel(newSQList);
            
        }
        //SVMX_ServiceQuoteDataManager.updateQuote(quoteList) ;
        
    }

    public static void updateQuoteForOwnerChange(List<SVMXC__Quote__c> quoteList) {
      List<SVMXC__Quote__c> quotes = new List<SVMXC__Quote__c>() ;
      List<SVMXC__Quote__c> sqList = new List<SVMXC__Quote__c>();  
        quotes = SVMX_ServiceQuoteDataManager.quoteListOwnerChange(quoteList) ;
        
        for(SVMXC__Quote__c sq : quotes) {
            sq.OwnerId = UserInfo.getUserId() ;
            //sqList.add(sq) ;
        }
        /*if(!sqList.isEmpty()) {
            SVMX_ServiceQuoteDataManager.updateQuote(sqList) ;
        }*/
    }
    
    public static void quoteApproval(List<SVMXC__Quote__c> quoteList) {
        List<SVMXC_Quote_Approvers__c> quoteApprovers = new List<SVMXC_Quote_Approvers__c>() ;
        List<SVMXC_Quote_Configuration__c> quoteConfigList = new List<SVMXC_Quote_Configuration__c>() ;
        Set<String> countries = new Set<String>() ;
        for(SVMXC__Quote__c quote : quoteList){
            countries.add(quote.SVMX_PS_Country__c) ;
        }
        quoteConfigList = [SELECT id,SVMX_Auto_Approval_Threshold__c,SVMX_Country__c,SVMX_No_Of_Approvals__c,SVMX_Special_Approval__c,(SELECT id,SVMX_Level__c ,SVMX_Approver__c FROM Quote_Approvers__r ) FROM SVMXC_Quote_Configuration__c WHERE SVMX_Country__c IN :countries] ; 
            
            if(!quoteConfigList.isEmpty()) {
            for(SVMXC_Quote_Configuration__c quoteConfig : quoteConfigList){
                for(SVMXC_Quote_Approvers__c quoteApp : quoteConfig.Quote_Approvers__r) {
                    for(SVMXC__Quote__c quote : quoteList) {
                        quote.SVMX_Approval_Levels__c = Integer.valueOf(quoteApp.SVMX_Level__c) ;
                        if(quoteApp.SVMX_Level__c == '1') {
                            quote.SVMX_Approver_1__c = quoteApp.SVMX_Approver__c ;
                        }
                         
                    }
                }
            }
        }       
    }
    /*public static void specialApproval(List<SVMXC__Quote__c>quoteList) {
        List<SVMXC__Quote__c> sqList = new List<SVMXC__Quote__c>() ;
        List<SVMXC_Quote_Approvers__c> quoteApprovers = new List<SVMXC_Quote_Approvers__c>() ;
        List<SVMXC_Quote_Configuration__c> quoteConfigList = new List<SVMXC_Quote_Configuration__c>() ;
        Set<String> countries = new Set<String>() ;
        for(SVMXC__Quote__c quote : quoteList){
            countries.add(quote.SVMX_PS_Country__c) ;
        }

        if(countries.size() > 0){
        quoteConfigList = [SELECT id,SVMX_Auto_Approval_Threshold__c,SVMX_Country__c,SVMX_No_Of_Approvals__c,SVMX_Special_Approval__c,(SELECT id,SVMX_Level__c ,SVMX_Approver__c,SVMX_Approvers_Category__c,SVMX_Special_Approver__c FROM Quote_Approvers__r  WHERE SVMX_Special_Approver__c == true) FROM SVMXC_Quote_Configuration__c WHERE SVMX_Country__c IN :countries] ;
        

        if(!quoteConfigList.isEmpty()) {
            for(SVMXC_Quote_Configuration__c quoteConfig : quoteConfigList){
              for(SVMXC_Quote_Approvers__c quoteApp : quoteConfig.Quote_Approvers__r) {
                for(SVMXC__Quote__c quote : quoteList) {
                    if(quote.SVMX_Special_Approval__c == true) {
                        quote.SVMX_Special_Approver__c = quoteApp.SVMX_Approver__c ;
                    }
                }
              }  
            }
        }

    } */
    
    public static void updateApproverLevel(List<SVMXC__Quote__c> quoteList) {
        List<SVMXC__Quote__c> sqList = new List<SVMXC__Quote__c>() ;
        List<SVMXC_Quote_Approvers__c> quoteApprovers = new List<SVMXC_Quote_Approvers__c>() ;
        List<SVMXC_Quote_Configuration__c> quoteConfigList = new List<SVMXC_Quote_Configuration__c>() ;
        Set<String> countries = new Set<String>() ;
        Map<Decimal, List<SVMXC_Quote_Approvers__c>> LevelApproversMap = new Map<Decimal, List<SVMXC_Quote_Approvers__c>>();
        Map<Boolean,List<SVMXC_Quote_Approvers__c>> specialApproverMap = new Map<Boolean,List<SVMXC_Quote_Approvers__c>> ();
        for(SVMXC__Quote__c quote : quoteList){

            countries.add(quote.SVMX_PS_Country__c) ;
        }

        if(countries.size() > 0){
        quoteConfigList = [SELECT id,SVMX_Auto_Approval_Threshold__c,SVMX_Country__c,SVMX_No_Of_Approvals__c,SVMX_Special_Approval__c,(SELECT id,SVMX_Level__c ,SVMX_Approver__c,SVMX_Approvers_Category__c,SVMX_Special_Approver__c FROM Quote_Approvers__r ) FROM SVMXC_Quote_Configuration__c WHERE SVMX_Country__c IN :countries] ; 
         }   
            if(!quoteConfigList.isEmpty()) {
                for(SVMXC_Quote_Configuration__c quoteConfig : quoteConfigList){
                    for(SVMXC_Quote_Approvers__c quoteApp : quoteConfig.Quote_Approvers__r) {

                        if(quoteApp.SVMX_Special_Approver__c ==true)
                             specialApproverMap.put(true,new List<SVMXC_Quote_Approvers__c> {quoteApp}) ;
                         else{

                            if(LevelApproversMap.containsKey(Decimal.valueof(quoteApp.SVMX_Level__c))){
                                List<SVMXC_Quote_Approvers__c> tempset= LevelApproversMap.get(Decimal.valueOf(quoteApp.SVMX_Level__c));
                                tempset.add(quoteApp);
                                LevelApproversMap.put(Decimal.valueof(quoteApp.SVMX_Level__c), tempset);
                            }
                            
                            else
                                LevelApproversMap.put(Decimal.valueof(quoteApp.SVMX_Level__c), new List<SVMXC_Quote_Approvers__c> {quoteApp});

                        }
                    } 
                }
                }
              
                system.debug('LevelApproversMap '+LevelApproversMap) ;
                for(SVMXC__Quote__c quote : quoteList) {

                      quote.SVMX_Approver_1__c = null;
                      quote.SVMX_Approver_2__c = null;
                      quote.SVMX_Approver_3__c = null;
                      quote.SVMX_Approver_4__c = null;
                      quote.SVMX_Approver_5__c = null;


                    //SVMXC__Quote__c quoteClone = new SVMXC__Quote__c(Id=quote.id);

                     List<Id> approverList =new List<Id>();
                     List<Id> SpecialApproversList = new List<Id>() ;
                            if(quote.SVMXC__Status__c == 'Submitted For Approval'  ){
                                system.debug('LevelApproversMap first'+LevelApproversMap.get(1)) ;
                                 approverList = CheckCategory(quote, LevelApproversMap.get(1));

                                 if(quote.SVMX_Special_Approval__c==true) {
                                 SpecialApproversList = CheckCategoryForSpecialApprovers(quote,specialApproverMap.get(true)) ;
                                 }

                            }
                            else if(quote.SVMXC__Status__c == 'Level 1 Approved') {
                                approverList= CheckCategory(quote, LevelApproversMap.get(2));
                            }
                            else if(quote.SVMXC__Status__c == 'Level 2 Approved') {
                                approverList= CheckCategory(quote, LevelApproversMap.get(3));
                            }
                            else if(quote.SVMXC__Status__c == 'Level 3 Approved') {
                                approverList= CheckCategory(quote, LevelApproversMap.get(4));
                            }
                            else if(quote.SVMXC__Status__c == 'Quote Rejected') {
                                approverList = CheckCategory(quote, LevelApproversMap.get(1));
                            }

                            if(SpecialApproversList.size() >0 ) {
                            system.debug('Special Approver '+SpecialApproversList);
                                quote.SVMX_Special_Approver__c = SpecialApproversList.get(0) ;
                             }

                         if(approverList.size() > 0 ){
                            Integer countSize = approverList.size();
                           // system.debug('Approver 1' +approverList.get(0) + 'Approver 2'+approverList.get(1) ) ;

                            if(countSize > 0)
                                quote.SVMX_Approver_1__c = approverList.get(0);

                            if(countSize > 1)
                                quote.SVMX_Approver_2__c = approverList.get(1);

                            if(countSize > 2)
                               quote.SVMX_Approver_3__c = approverList.get(2);

                            if(countSize > 3)
                               quote.SVMX_Approver_4__c = approverList.get(3);
                            
                            if(countSize > 4)
                               quote.SVMX_Approver_5__c = approverList.get(4);
                        }

                       
                        system.debug('quote after approvers '+quote);
                        system.debug('approver List : '+approverList ) ;

                        if(quote.SVMX_Approver_1__c != null) {

                        if(quote.SVMX_Approver_1__c == null) {
                            quote.addError('Quote Must have atleast 1 Approver') ;
                            }
                             if(quote.SVMX_Approver_2__c == null) {
                                quote.SVMX_Approver_2__c = quote.SVMX_Approver_1__c ;
                            }
                             if(quote.SVMX_Approver_3__c == null) {
                                quote.SVMX_Approver_3__c = quote.SVMX_Approver_1__c ;
                             }
                             if(quote.SVMX_Approver_4__c == null) {
                                quote.SVMX_Approver_4__c = quote.SVMX_Approver_1__c ;
                             }
                             if(quote.SVMX_Approver_5__c == null) {
                                quote.SVMX_Approver_5__c = quote.SVMX_Approver_1__c ;
                             }
                     }


                    //sqList.add(quoteClone);
                }


            }

             //if(quoteList.size() > 0)
               // SVMX_ServiceQuoteDataManager.updateQuote(quoteList) ; 
       
//}
      
  //  }


    public static List<Id> CheckCategory(SVMXC__Quote__c qt, List<SVMXC_Quote_Approvers__c> approversList){
        List<Id> userslist = new List<Id>();
        List<Id> specialApprovers = new List<Id>() ;
        if(qt.SVMX_Submitters_Category__c != null){
            for(SVMXC_Quote_Approvers__c qa: approversList){
                Set<String> submittersCategory = new Set<String>() ;
                String sub =qt.SVMX_Submitters_Category__c;
                
                for(String subCat : sub.split(',')) {
                    submittersCategory.add(subCat) ;

                }
               

                system.debug('submittersCategory ' +submittersCategory ) ;
                system.debug('Approvers category '+qa.SVMX_Approvers_Category__c) ;
                if(submittersCategory.contains(qa.SVMX_Approvers_Category__c)) {
                    userslist.add(qa.SVMX_Approver__c);
                    System.debug('Approvers List with Category '+usersList) ;
                }
            }

        }

        else if(userslist.isEmpty() || qt.SVMX_Submitters_Category__c == null){
            for(SVMXC_Quote_Approvers__c qa: approversList){
                    userslist.add(qa.SVMX_Approver__c);
                    system.debug('UsersList' + userslist) ;
            }
        }

        
        return userslist;
    }
        
       public static List<Id> CheckCategoryForSpecialApprovers(SVMXC__Quote__c qt, List<SVMXC_Quote_Approvers__c> approversList){
        List<Id> userslist = new List<Id>();
                if((userslist.isEmpty() && (qt.SVMX_Special_Approval__c ==true))) {
                    for(SVMXC_Quote_Approvers__c qa : approversList) {
                        userslist.add(qa.SVMX_Approver__c) ;
                        System.debug('Special Approvers '+userslist) ;
                }
            }

            return userslist ;
       }
    
    
    public static void updateQuoteAmountLevel(List<SVMXC__Quote__c> quoteList) {
        //List<SVMXC__Quote__c> quotes = new List<SVMXC__Quote__c>() ;
        //quotes = [SELECT Id,SVMXC__Quote_Amount2__c FROM SVMXC__Quote__c WHERE id IN:quoteList] ;
        List<SVMXC_Quote_Configuration__c> quoteConfigList = new List<SVMXC_Quote_Configuration__c>() ;
        Set<String> countries = new Set<String>() ;
        for(SVMXC__Quote__c quote : quoteList){
             system.debug('Quote amount ' + quote.SVMXC__Quote_Amount2__c) ;
            if(quote.SVMXC__Quote_Amount2__c != null)
            countries.add(quote.SVMX_PS_Country__c) ;
        }

         Map<String,List<SVMXC_Quote_Amount__c>> quoteAmtMap = new Map<String,List<SVMXC_Quote_Amount__c>>() ;
        
        if(countries.size() > 0){
            quoteConfigList = [SELECT id,SVMX_Auto_Approval_Threshold__c,SVMX_Country__c,SVMX_No_Of_Approvals__c,SVMX_Special_Approval__c,(SELECT id, SVMX_Level__c , SVMX_Maximum_Amount__c, SVMX_Minimum_Amount__c , SVMX_Quote_Configuration__c FROM Quote_Amounts__r ) FROM SVMXC_Quote_Configuration__c WHERE SVMX_Country__c IN :countries] ; 


            for(SVMXC_Quote_Configuration__c quoteConfig : quoteConfigList) {
                for(SVMXC_Quote_Amount__c amt : quoteConfig.Quote_Amounts__r ){
                    List<SVMXC_Quote_Amount__c> tempAmount = new List<SVMXC_Quote_Amount__c>() ;
                    if(quoteAmtMap.containsKey(quoteConfig.SVMX_Country__c)) {
                        tempAmount = quoteAmtMap.get(quoteConfig.SVMX_Country__c) ;
                    }
                    tempAmount.add(amt) ;
                    quoteAmtMap.put(quoteConfig.SVMX_Country__c,tempAmount) ;
                }
            }
            
            if(!quoteAmtMap.isEmpty()){
                for(SVMXC__Quote__c sq : quoteList ) { 
                    List<SVMXC_Quote_Amount__c> amount = quoteAmtMap.get(sq.SVMX_PS_Country__c) ;
                    system.debug('Amount' + amount) ;
                    
                    for(SVMXC_Quote_Amount__c amtTemp :amount ){
                        if(sq.SVMXC__Quote_Amount2__c <= amtTemp.SVMX_Maximum_Amount__c && sq.SVMXC__Quote_Amount2__c >= amtTemp.SVMX_Minimum_Amount__c ) {
                            sq.SVMX_Approval_Levels__c = amtTemp.SVMX_Level__c ;
                        }
                        //else if(sq.SVMXC__Quote_Amount2__c >= amtTemp.SVMX_Maximum_Amount__c) {
                        //        sq.addError('Amount Greater than maximum amount') ;
                       // }
                    }
                    
                }

            }
        }
        
    }
}