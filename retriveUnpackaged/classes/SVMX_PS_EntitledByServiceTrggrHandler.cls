/*Description: Trigger handler for Update Entitled by Service on Entitlement History

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger: SVMX_PS_UpdateEntitledByService.cls
    class:SVMXC__Entitlement_History__c 
    
    
 
Author: Keshava Prasad
Date: 25-04-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_PS_EntitledByServiceTrggrHandler extends SVMX_TriggerHandler  {
    private static final Set<String> picklistValues = new Set<String>{system.label.Installation,system.label.Quoted_Works,system.label.Planned_Services_PPM} ;
       private list <SVMXC__Entitlement_History__c> newEntitlementHistoryList ;
        public SVMX_PS_EntitledByServiceTrggrHandler() {
            this.newEntitlementHistoryList = (list <SVMXC__Entitlement_History__c>) Trigger.new ;
        }
        
        public override void beforeInsert() { 
            Set<id> contractIDSet = new Set<id>() ;
            Set<String> workOrderTypeSet = new Set<String>();
        for(SVMXC__Entitlement_History__c entitlementHistory : newEntitlementHistoryList) {
            
            if(picklistValues.contains(entitlementHistory.Work_Order_Type__c)) {
                contractIDSet.add(entitlementHistory.SVMXC__Service_Contract__c);
                workOrderTypeSet.add(entitlementHistory.Work_Order_Type__c) ;
                
            }
            System.debug('contractID : '+contractIDSet);
            System.debug('workOrderTypeSet = '+workOrderTypeSet);
        }
            
            if(contractIDSet.size() > 0) {
              List<SVMXC__Service_Contract_Services__c> includedservicesList =  new List<SVMXC__Service_Contract_Services__c>([SELECT Id,SVMX_Type__c,SVMXC__Service_Contract__c FROM SVMXC__Service_Contract_Services__c WHERE SVMXC__Service_Contract__c IN : contractIDSet AND SVMX_Type__c IN : workOrderTypeSet]);
                for(SVMXC__Entitlement_History__c entHistory : newEntitlementHistoryList){
                    for(SVMXC__Service_Contract_Services__c inc:includedservicesList){
                        if(inc.SVMXC__Service_Contract__c == entHistory.SVMXC__Service_Contract__c && inc.SVMX_Type__c == entHistory.Work_Order_Type__c){
                            entHistory.SVMXC__Entitled_By_Service__c  = inc.id ;
                            entHistory.SVMXC__Entitled_Within_Threshold__c =true;
                            System.debug('entHistory.SVMXC__Entitled_By_Service__c = '+entHistory.SVMXC__Entitled_By_Service__c );
                            
                            }
                        }
                    }
                }
            }
            
        }