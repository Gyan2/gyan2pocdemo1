/************************************************************************************************************
Description: Test class for the Controller Class (SVMX_SendQuotedWorksQuote) 
 
Author: Ranjitha S
Date: 04-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest
public class SVMX_SendServiceQuoteController_UT { 
 
    static testMethod void testsendQuote()
    {
       
        Account acc = SVMX_TestUtility.CreateAccount('Test account', 'Germany', true);
        Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
        Opportunity opp = SVMX_TestUtility.CreateOpportunity(acc.Id, 'name', 'New', 50, 100, true);
        SVMXC__Quote__c quote = SVMX_TestUtility.CreateServiceQuote(acc.id,null,opp.id,'Draft',true);
    
        Attachment attachmentRec = new Attachment(Name = 'Test', ParentId = quote.Id, Body = Blob.valueOf('Test') );
        Insert attachmentRec;
        set<id> att = new set<id>();
        att.add(attachmentRec.id);
        
        quote = [select Id, name from SVMXC__Quote__c WHERE Id =: quote.Id ];

        PageReference pageRef = Page.SVMX_SendQuotedWorksQuote;

        pageRef.getParameters().put('quoteNumber', quote.name);
        Test.setCurrentPage(pageRef);

        SVMX_SendServiceQuoteController obj01 = new SVMX_SendServiceQuoteController();
        
        obj01.isCheckedAll = TRUE;
        obj01.checkAllValue();
        obj01.sendQuotes();
        SVMX_SendServiceQuoteController.attachmentToOpp(att, opp.Id);
       
    }
}