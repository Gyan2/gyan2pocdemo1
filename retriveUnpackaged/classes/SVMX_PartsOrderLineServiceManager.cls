public class SVMX_PartsOrderLineServiceManager{
    
    public static void checkValidation(set<Id> vendorids,List<SVMXC__RMA_Shipment_Line__c> partsOrderLine){
              Map<Id, set<id>> mapAccidProductId = new Map<Id, set<id>>();
             String vendorName= null;

             List<VendorMaterial__c> venList = SVMX_VendorMaterialDataManager.vendorMaterialQuery(vendorids);
          system.debug('VendorList Size '+venList.size());
             if(venList.size() > 0){
                     for(VendorMaterial__c venmaterl:venList){
                        set<Id> tempproductset = new set<Id>();
                            if(mapAccidProductId.containsKey(venmaterl.Account__c)) {

                                tempproductset = mapAccidProductId.get(venmaterl.Account__c);
                        } 
                        vendorName = venmaterl.Account__r.Name;
                        tempproductset.add(venmaterl.Product__c);
                        mapAccidProductId.put(venmaterl.Account__c,tempproductset);
                        system.debug('mapAccidProductId-->'+mapAccidProductId);
                    }


                 for( SVMXC__RMA_Shipment_Line__c pol:partsOrderLine){
                     system.debug('Parts Order Line size' +partsOrderLine.size() ) ;
                     system.debug(' vendor '+pol.SVMX_Vendor__c);
                      set<id> prodIdset = new  set<id>();
                     if(pol.SVMX_Vendor__c != null)
                     prodIdset = mapAccidProductId.get(pol.SVMX_Vendor__c);
                   system.debug('prodIdset'+prodIdset) ;
                     if(prodIdset.size() > 0 && !prodIdset.contains(pol.SVMXC__Product__c)) {  
                        String fullFileURL = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/SVMX_VendorMaterialRelatedList?Id='+pol.SVMX_Vendor__c;
                        fullFileURL= fullFileURL.substring(0,fullFileURL.indexof('svmxc'))+fullFileURL.substring(fullFileURL.indexof('svmxc')+4);
                        //String fullFileURL = URL.getSalesforceBaseUrl().toExternalForm()+'/'+pol.SVMX_Vendor__c;
                        String stringURL  = '<a href='+fullFileURL+'  target="click here">click here.</a>';  
                        pol.addError(vendorName+' Does not deal with this Part Number. To ge the list of Vendor Materials from this vendor '+stringURL);
                 }
                     
               } 

            

    }

} 
}