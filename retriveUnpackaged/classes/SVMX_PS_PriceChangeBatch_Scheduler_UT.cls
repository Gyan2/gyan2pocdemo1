@istest
public with sharing class SVMX_PS_PriceChangeBatch_Scheduler_UT {
    static testmethod void testPriceChange() {
        Test.startTest();
        SVMX_PS_PriceChangeBatch_Scheduler  obj = new SVMX_PS_PriceChangeBatch_Scheduler();
        obj.execute(null);
        Test.stopTest();
    }

}