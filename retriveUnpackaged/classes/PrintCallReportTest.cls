@isTest 
public class PrintCallReportTest 
{
    static testMethod void TEST_PrintCallReport() 
    {
        Account testAccount = new Account();
        testAccount.Name='acme' ;
        insert testAccount;
        
        Event eve = new Event( Subject='test event' ,  WhatId= testAccount.Id, StartDateTime=System.Now(), EndDateTime=System.Now().addMinutes(30), DurationInMinutes=30);
        insert eve;

        Test.StartTest(); 

            PageReference myVfPage = Page.PrintCallReport;
            Test.setCurrentPage(myVfPage);
            ApexPages.currentPage().getParameters().put('id', eve.Id);
            PrintCallReport cc = new PrintCallReport();
            Event testEvent = cc.getEvent();

        Test.StopTest();
    }
}