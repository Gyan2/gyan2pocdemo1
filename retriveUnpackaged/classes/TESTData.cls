// @description Create data for Test classes
// @copyright Parx
// @author MHA
// @lastmodify 24.04.2017
// 
public class TESTData {
    
    public static Opportunity opportunity;
    public static Account account;
    public static Contact contact;
    public static Date today = Date.today();
    
    public static void setupOpportunityAccountContact()
    {
        setupOpportunity();
        contact = createContact('TestParxContact', 'EN', account.Id, true);
    }
    public static void setupOpportunity()
    {
        account = createAccount('TestParxAccount',true);
        opportunity = createOpportunity('TestParxOpportunity',account.Id, 'Proposal', today, true);
    }
    
    public static Account createAccount(String name, Boolean persist)
    {
       Account a = new Account(Name = name);
       if(persist) insert a;
       return a;
    }
    
    public static Contact createContact(String lastName, String language, Id accountId, Boolean persist)
    {
        Contact c = new Contact(LastName = lastName, Language__c = language,
                           AccountId = accountId);
        if(persist) insert c;       
        return c;
    }
    public static Opportunity createOpportunity(String name, Id accountId, String stageName, Date closeDate, Boolean persist)
    {              
        Opportunity op = new Opportunity(AccountId = accountId, Name = name,
                                 StageName = stageName, CloseDate = closeDate);
        if(persist) insert op;
        return op;
    }
}