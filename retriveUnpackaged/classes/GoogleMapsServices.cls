public with sharing class GoogleMapsServices {

    private static String API_KEY = 'AIzaSyBqPwagPRtoMxnaf_vnuEgWq4HnNtlz1WY';

    public static GenericResponse addressAutoComplete(String searchKey) {
        return (GenericResponse)JSON.deserialize(call('place','autocomplete',new Map<String,String>{'input'=>searchKey,'types'=>'address'}), GenericResponse.class);
    }

    public static GenericResponse addressDetails(String paramPlaceId){
        return (GenericResponse)JSON.deserialize(call('place','details',new Map<String,String>{'placeid'=>paramPlaceId}), GenericResponse.class);
    }

    public static GenericResponse addressGeocode(String paramSearchKey) {
        return (GenericResponse)JSON.deserialize(call('geocode', null, new Map<String,String>{'address'=>paramSearchKey}), GenericResponse.class);
    }

    private static String call(String paramApiType, String paramApiSubtype, Map<String,String> paramInputs) {
        HttpRequest req = new HttpRequest();

        req.setMethod('GET');

        List<String> queryString = new List<String>{'key=' + API_KEY};
        for (String forKey : paramInputs.keySet()) {
            queryString.add(forKey + '=' + EncodingUtil.urlEncode(paramInputs.get(forKey), 'UTF-8'));
        }
        if (paramApiSubtype == null)
            req.setEndpoint(String.format('https://maps.googleapis.com/maps/api/{0}/json?{1}', new List<String>{paramApiType, String.join(queryString, '&')}));
        else
            req.setEndpoint(String.format('https://maps.googleapis.com/maps/api/{0}/{1}/json?{2}', new List<String>{paramApiType, paramApiSubtype, String.join(queryString, '&')}));

        if (req.getEndpoint().length() > 8192) throw new URLTooLongException();

        HttpResponse response = new Http().send(req);
        String returned = response.getBody();
        System.debug(returned);
        return returned;
    }




    public class GenericResponse {
        public GenericResult result;
        public List<GenericResult> results;
        public List<String> html_attributions;
        public List<Prediction> predictions;
        public String status;
    }

    public class GenericResult {
        public List<AddressComponent> address_components;
        public String adr_address;
        public String formatted_address;
        public Geometry geometry;
        public String icon;
        public String id;
        public String name;
        public String place_id;
        public String reference;
        public String scope;
        public List<String> types;
        public String url;
        public Integer utc_offset;
        public String vicinity;
    }

    public class Prediction {
        public String description;
        public String id;
        public String place_id;
        public String reference;
        public List<MatchedSubstring> matched_substrings;
    }

    public class MatchedSubstring {
        public Integer length;
        public Integer offset;
    }

    public class AddressComponent {
        public String long_name;
        public String short_name;
        public List<String> types;
    }

    public class Geometry {
        public GeoCoordinate location;
        public String location_type;
        public Viewport viewport;
    }

    public class Viewport {
        public GeoCoordinate northeast;
        public GeoCoordinate southwest;
    }

    public class GeoCoordinate {
        public Decimal lat;
        public Decimal lng;
    }

    public class URLTooLongException extends Exception {}
}