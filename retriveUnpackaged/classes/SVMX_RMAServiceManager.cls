public class SVMX_RMAServiceManager {

    // Sales Order RMA Create Method
    //
    public static void handleSAPSalesOrderRMACreation(list<SVMXC__RMA_Shipment_Line__c> partsOrderLineList) {
        
        Set<Id> validPartsOrderLineIdList = new Set<Id>();
        Set<Id> existingPartsOrderLineIdList = new Set<Id>();
        Set<Id> pOrIds = new Set<Id>();
        List<SVMXC__RMA_Shipment_Line__c> blankCheckUpdates = new List<SVMXC__RMA_Shipment_Line__c>();
        List<SVMXC__RMA_Shipment_Line__c> poLineList = new List<SVMXC__RMA_Shipment_Line__c>();

        if(partsOrderLineList != null && !partsOrderLineList.isEmpty()){
              
            System.debug('SVMX_SAPSalesOrderUtility : Handle RMA Sales Order Creation');

            for(SVMXC__RMA_Shipment_Line__c partOrderLine : partsOrderLineList) {

                pOrIds.add(partOrderLine.SVMXC__RMA_Shipment_Order__c);
            }

            Map<Id,SVMXC__RMA_Shipment_Order__c> parOrderMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>([SELECT Id, Name, SVMX_Service_Sales_Order_Number__c,
                                                                            SVMXC__Case__c, SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c,
                                                                            SVMX_Awaiting_SAP_Response__c
                                                                            FROM SVMXC__RMA_Shipment_Order__c
                                                                            WHERE Id IN : pOrIds]);
            
            for(SVMXC__RMA_Shipment_Line__c partsordline : partsOrderLineList){
               
               //Scenario 1 - New RMA Order Line created. Sales Order request not yet made to SAP 
               if(parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Service_Sales_Order_Number__c == null
                && parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Awaiting_SAP_Response__c == false) {

                    System.debug('SVMX_RMAServiceManager Parts Order : ' + partsordline.Name + ' is ready for SAP Sales Order Creation');
                    validPartsOrderLineIdList.add(partsordline.Id);
                }
                
                //Scenraio 2 - New RMA Line created. Sales Order Create request already made, but no Sales Order Number yet returned
                else if(parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Service_Sales_Order_Number__c == null
                && parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Awaiting_SAP_Response__c == true) {

                    System.debug('SVMX_RMAServiceManager Parts Order :' + partsordline.Name + ' is waiting for SAP Sales Order Number. Setting SVMX_Item_Number_Blank_Check__c to True');
                        
                    poLineList.add(partsordline);    
                }

                //Scenario 3 - New RMA Line created. Sales Order Number already exists on Parts Order
                else if(parOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Service_Sales_Order_Number__c != null) {
                    // These PartsOrderLine Details need to sent as Update, although they are new. This is because SAP considers
                    // these as updates to an existing Sales Order, and not a create
                    System.debug('SVMX_RMAServiceManager : Parts Order ' + partsordline.Name + ' is ready to be added to existing Sales Order - Update request will be sent to SAP, not Create');
                    existingPartsOrderLineIdList.add(partsordline.Id);       
                }
            }
            
            List<SVMXC__RMA_Shipment_Line__c> poLineList1=[SELECT Id, Name, SVMX_Item_Number_Blank_Check__c, 
                                   SVMX_Awaiting_SAP_Response__c, SVMX_Sales_Order_Item_Number__c
                                   FROM SVMXC__RMA_Shipment_Line__c
                                   WHERE Id IN : poLineList
                                   ];

            if(!poLineList.isEmpty()) {
                
                for(SVMXC__RMA_Shipment_Line__c pol : poLineList1) {

                    pol.SVMX_Item_Number_Blank_Check__c = true;
                    blankCheckUpdates.add(pol);
                }
            }                     
            
            if(!validPartsOrderLineIdList.isEmpty()) {
                
               SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderRMAcreation(validPartsOrderLineIdList);    
            }

            if(!existingPartsOrderLineIdList.isEmpty()) {

                SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderRMAUpdate(existingPartsOrderLineIdList, true);
            }

            if(!blankCheckUpdates.isEmpty()) {
                update blankCheckUpdates;
            }
        }

        else {

            System.debug('SVMX_SAPSalesOrderUtil : No Parts Detail Line for Sales Order Creation');
        }
    }

    //sales Order RMA Header update
    public static void handleSAPSalesOrderUpdateRMAHeader(Map<Id, SVMXC__RMA_Shipment_Order__c> newPartsOrderMap, Map<Id, SVMXC__RMA_Shipment_Order__c> oldPartsOrderMap) {
    
    List<SVMXC__RMA_Shipment_Line__c> awaitingSONumPOLineList = new List<SVMXC__RMA_Shipment_Line__c>();
    List<SVMXC__RMA_Shipment_Line__c> partsOrderLineUpdates = new List<SVMXC__RMA_Shipment_Line__c>();
    List<SVMXC__RMA_Shipment_Order__c> poRMAUpdates = new List<SVMXC__RMA_Shipment_Order__c>();
    Set<Id> poIds = new Set<Id>();
    Set<Id> polIds = new Set<Id>();
    Set<Id> polNoSOIds = new Set<Id>();


        System.debug('SVMX_SAPSalesOrderUtility : Handle Sales Order RMA Header Update');

        for(SVMXC__RMA_Shipment_Order__c po : newPartsOrderMap.values()) {

            //Scenario 1 - Parts Order waiting for Sales Order Number. Now the SO Number has arrived
            //Need to check Parts Order RMA Lines for Updates

            if(oldPartsOrderMap.get(po.Id).SVMX_Service_Sales_Order_Number__c == null
                && po.SVMX_Service_Sales_Order_Number__c != null
                && po.SVMX_Awaiting_SAP_Response__c == true) {

                poIds.add(po.Id);

                SVMXC__RMA_Shipment_Order__c pord = new SVMXC__RMA_Shipment_Order__c(id = po.Id, SVMX_Awaiting_SAP_Response__c = false);

                //po.SVMX_Awaiting_SAP_Response__c = false;
                poRMAUpdates.add(pord);
            }
        }

        Update poRMAUpdates;

        awaitingSONumPOLineList = [SELECT Id, Name, SVMX_Item_Number_Blank_Check__c, 
                                   SVMX_Awaiting_SAP_Response__c, SVMX_Sales_Order_Item_Number__c
                                   FROM SVMXC__RMA_Shipment_Line__c
                                   WHERE SVMXC__RMA_Shipment_Order__c IN : poIds
                                   AND SVMX_Item_Number_Blank_Check__c = true];

        if(!awaitingSONumPOLineList.isEmpty()) {
            
            for(SVMXC__RMA_Shipment_Line__c partsOrderLine : awaitingSONumPOLineList) {
                //Could also check for 'Awaiting SAP Response' Flag instead on SO Number
                if(partsOrderLine.SVMX_Sales_Order_Item_Number__c != null) {

                    polIds.add(partsOrderLine.Id);
                }
                else {
                    
                    polNoSOIds.add(partsOrderLine.Id);
                }

                partsOrderLine.SVMX_Item_Number_Blank_Check__c = false;
                partsOrderLineUpdates.add(partsOrderLine);
            }

            if(polIds.size() > 0) {
                
                SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderRMAUpdate(polIds, false);
            }

            if(polNoSOIds.size() > 0) {

                SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderRMAUpdate(polNoSOIds, true); 
            }

            Update partsOrderLineUpdates;
        }

    }

    //sales Order RMA Line Update
    public static void handleSAPSalesOrderUpdateRMA(Map<Id, SVMXC__RMA_Shipment_Line__c> newPartsOrderLineMap,Map<Id, SVMXC__RMA_Shipment_Line__c> oldPartsOrderLineMap) {
          
        if(newPartsOrderLineMap.keyset() != null) {
                    
            System.debug('SVMX_SAPSalesOrderUtility : Handle Sales Order RMA Update');
            
            String tiggerFieldSplit;
            List<SVMXC__RMA_Shipment_Line__c> updatepartsOrderLineList = new List<SVMXC__RMA_Shipment_Line__c>();
            Set<Id> sapPartsOrderLineUpdateSet = new Set<id>();
            Set<Id> partsOrderIds = new Set<Id>();

            SVMX_Interface_Trigger_Configuration__c wdtriggerFildQuery = [SELECT id, Name, SVMX_Country__c,SVMX_Object__c,
                                                                          SVMX_Trigger_on_Field__c 
                                                                          FROM SVMX_Interface_Trigger_Configuration__c 
                                                                          WHERE SVMX_Object__c = 'Parts Order Line'];
            
            tiggerFieldSplit = wdtriggerFildQuery.SVMX_Trigger_on_Field__c;
            List<String> triggerFieldTrmList = tiggerFieldSplit.split(';');
            List<String> triggerFieldList = new List<String>();
            
            for(String TRFld:triggerFieldTrmList){
                triggerFieldList.add(TRFld.Trim());
            }
            
            System.debug(triggerFieldList);

            for(SVMXC__RMA_Shipment_Line__c partsOrderLine : newPartsOrderLineMap.values()) {

                partsOrderIds.add(partsOrderLine.SVMXC__RMA_Shipment_Order__c);
            }

            for(SVMXC__RMA_Shipment_Line__c newPartsOrdLine : newPartsOrderLineMap.values()) {
                 
                System.debug('newPODetail.Id' + newPartsOrdLine.Id);
                
                //Scenario 1 - Sales Order Item Number already populated on Parts Order Line
                //A change is made to the Parts Order Line Trigger field, which already has an SO Item Number
                if(oldPartsOrderLineMap.get(newPartsOrdLine.Id).SVMX_Sales_Order_Item_Number__c != null 
                                         && newPartsOrdLine.SVMX_Sales_Order_Item_Number__c != null) {

                    for(String trigField : triggerFieldList) {

                        if(newPartsOrderLineMap.get(newPartsOrdLine.Id).get(trigField) != oldPartsOrderLineMap.get(newPartsOrdLine.Id).get(trigField)) {
                            
                            system.debug('trigFieldcheck1-->' + trigField);  
                            sapPartsOrderLineUpdateSet.add(newPartsOrdLine.Id);
                            break;  
                        }   
                    }
                }

                //Scenario 2 - Sales Order Item Number is blank on the Parts Order Line
                //A Creation request has already been sent to SAP, but we are waiting for the SO Item Number to return
                //A change is made to a trigger field on the Parts Order Line
                else if(newPartsOrdLine.SVMX_Sales_Order_Item_Number__c == null
                    && newPartsOrdLine.SVMX_Awaiting_SAP_Response__c == true) {

                    for(String trigField : triggerFieldList) {
                        
                        if(newPartsOrderLineMap.get(newPartsOrdLine.Id).get(trigField) != oldPartsOrderLineMap.get(newPartsOrdLine.Id).get(trigField)) {

                            SVMXC__RMA_Shipment_Line__c parOrdLine = new SVMXC__RMA_Shipment_Line__c(id = newPartsOrdLine.Id, SVMX_Item_Number_Blank_Check__c = true);
                            updatepartsOrderLineList.add(parOrdLine);
                            break;  
                        }
                    }   
                }
                
                //Scenario 3 - PO Line was waiting for Sales Order Item Number. Now the SO Item Number has arrived
                //Before the SO Item Number arrived, changes were made to the PO Line, causing the Blank Check Flag to be True
                //Need to send the PO Line for Update to SAP 
                if(oldPartsOrderLineMap.get(newPartsOrdLine.Id).SVMX_Sales_Order_Item_Number__c == null 
                                            && newPartsOrdLine.SVMX_Sales_Order_Item_Number__c != null 
                                            && newPartsOrdLine.SVMX_Item_Number_Blank_Check__c == true
                                            && newPartsOrdLine.SVMX_Awaiting_SAP_Response__c == true) {
                                                        
                    sapPartsOrderLineUpdateSet.add(newPartsOrdLine.Id);
                    
                    SVMXC__RMA_Shipment_Line__c parOrdLine1 = new SVMXC__RMA_Shipment_Line__c(id = newPartsOrdLine.Id, SVMX_Item_Number_Blank_Check__c = false, SVMX_Awaiting_SAP_Response__c = false);                            
                    updatepartsOrderLineList.add(parOrdLine1);
                }

                //Scenario 4 - PO Line was waiting for Sales Order Item Number. Now the SO Item Number has arrived
                //No changes were made to the PO Line, therefore, no need to send Update to SAP
                if(oldPartsOrderLineMap.get(newPartsOrdLine.Id).SVMX_Sales_Order_Item_Number__c == null 
                                            && newPartsOrdLine.SVMX_Sales_Order_Item_Number__c != null 
                                            && newPartsOrdLine.SVMX_Item_Number_Blank_Check__c == false
                                            && newPartsOrdLine.SVMX_Awaiting_SAP_Response__c == true) {
                                                        
                    SVMXC__RMA_Shipment_Line__c parOrdLine1 = new SVMXC__RMA_Shipment_Line__c(id = newPartsOrdLine.Id, SVMX_Awaiting_SAP_Response__c = false);                            
                    updatepartsOrderLineList.add(parOrdLine1);
                }
          
            }    
            
            if(!updatepartsOrderLineList.isEmpty()) {
                
                update updatepartsOrderLineList;                
            }
            
            if(!sapPartsOrderLineUpdateSet.isEmpty()) {
               
               SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderRMAUpdate(sapPartsOrderLineUpdateSet, false);
            }  
  
        }
        else{

            System.debug('SVMX_SAPSalesOrderUtility : No Work Detail Line for Sales Order RMA update');
        }
    }
}