/*******************************************************************************************************
* Class Name     	: SM_TriggerAccountTest
* Description		: Test class for Trigger Handler
* Author          	: Paul Carmuciano
* Created On      	: 2017-07-10
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-07-10         1000                 Initial version
******************************************************************************************************/
@isTest
public class SM_TriggerAccountTest {

    @testSetup
    public static void setupData() {
        
    }

    /***
* Method name	: bulkTest
* Description	: Can test as admin user on User object
* Author		: Paul Carmuciano
* Return Type	: n/a
* Parameter		: n/a
*/
    @isTest
    public static void bulkTest() {
        // Arrange

        Map<Id,Profile> profiles = DM004_User.getCustomProfiles(); // don't use it, but could

        // get metadata
        Map<String,Management_Segmentation__mdt> ManagementSegmentationMapping = new Map<String,Management_Segmentation__mdt>();
        ManagementSegmentationMapping = MDM001_ManagementSegmentation.ManagementSegmentationMapping;

        // contextualise to a target so we can assert the result
        Management_Segmentation__mdt targetManagementSegmentationMapping = ManagementSegmentationMapping.values()[0];

        // get a user to run
        Map<Id,User> users = DM004_User.getActiveUsersForProfile(new List<Id>(profiles.keySet()) );
        User u = new List<User>(users.values())[0];

        // Act
        Test.startTest();
        
        system.runAs(u) { 

            // make 200 users
            List<Account> bulkAccounts = new List<Account>();
            for (Integer i=0; i<=200; i++) {
                Account a = TESTData.createAccount('name', false);
                a.ManagementCountry__c = targetManagementSegmentationMapping.DeveloperName;
                bulkAccounts.add(a);
            }

            // cover insert
            insert bulkAccounts;
        
            // cover update
            for (Account a : bulkAccounts ) {
                a.ManagementCountry__c = ManagementSegmentationMapping.values()[ManagementSegmentationMapping.size()-1].DeveloperName;
            }
            update bulkAccounts;

            // delete / undelete on user        
            delete bulkAccounts;
            undelete bulkAccounts;
        }

        Test.stopTest();

        // Assert true (we are not testing any logic here, only bulk support)
        system.assert(true);
        
    }
    

}