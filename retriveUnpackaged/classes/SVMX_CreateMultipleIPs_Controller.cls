/**==================================================================================================================================
 * DormaKaba
 * Name: SVMX_CreateMultipleIPs_Controller
 * Description: Controller to create multiple IPs from Location from Vf page SVMX_CreateMultipleIPs
 * Created Date: 05-04-2018
 * Created By: Ranjitha S
 *
 
 ===================================================================================================================================*/   
public class SVMX_CreateMultipleIPs_Controller {
    
   public List<IPWrapper> ipWrapperList { get ; set ; }
   public Account account { get ; set ; }
   public SVMXC__Site__c location { get ; set ; }
   public SVMXC__Site__c loc;
   public SVMX_CreateMultipleIPs_Controller( ApexPages.StandardController controller)
   {
         location = (SVMXC__Site__c)controller.getRecord();
         Id locationId = ApexPages.currentPage().getParameters().get('Id');               
         loc = [SELECT id,SVMXC__Account__c,SVMXC__Account__r.Name,SVMXC__Preferred_Business_Hours__c ,SVMXC__City__c ,SVMX_PS_Contact__c ,SVMXC__Country__c ,CurrencyIsoCode,SVMX_Language__c ,SVMX_PS_Preferred_Technician__c , SVMXC__State__c , SVMXC__Street__c , SVMXC__Zip__c  FROM SVMXC__Site__c WHERE id = :locationId];
         
         ipWrapperList = new List<IPWrapper>();
         IPWrapper wrapper = new IPWrapper();
         wrapper.ipToInstall = new SVMXC__Installed_Product__c();
         wrapper.id = 0;
         ipWrapperList.add(wrapper);
                
   }       
        
   public PageReference addRow()
   {
         IPWrapper wrapper = new IPWrapper();
         wrapper.ipToInstall = new SVMXC__Installed_Product__c();
         wrapper.id = ipWrapperList.size();
          //wrapper.quantity = 0;
         ipWrapperList.add(wrapper);
                                
         return null;
   }
        
   public PageReference deleteRow(){
                
         Integer rowId = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowId'));
         ipWrapperList.remove(rowId);
         for (Integer i = rowId ; i < ipWrapperList.size() ; i++){
               ipWrapperList[i].id = rowId++ ;
         }
         return null;
   }
        
        
   public PageReference createIP()
   {                          
         List<SVMXC__Installed_Product__c> ipList = new List<SVMXC__Installed_Product__c>();
        if(ipWrapperList != null && ipWrapperList.size() > 0 )
        {   
            for(IPWrapper ipWrapper : ipWrapperList)
            {       
             if(ipWrapperList != null && ipWrapperList.size() > 0 && (ipWrapper.Quantity != null || ipWrapper.Quantity <=0 ))
             {
                   for(Integer i  = 0 ; i < ipWrapper.Quantity ; i++)
                     {
                     SVMXC__Installed_Product__c ip = new SVMXC__Installed_Product__c(SVMX_ProductDetail__c = ipWrapper.ipToInstall.SVMX_ProductDetail__c,
                                                                                        SVMX_PS_Product_Name__c = ipWrapper.ipToInstall.SVMX_PS_Product_Name__c,
                                                                                         SVMXC__Serial_Lot_Number__c = ipWrapper.ipToInstall.SVMXC__Serial_Lot_Number__c,
                                                                                         SVMXC__Asset_Tag__c  = ipWrapper.ipToInstall.SVMXC__Asset_Tag__c,
                                                                                         SVMXC__Date_Installed__c = ipWrapper.ipToInstall.SVMXC__Date_Installed__c,   
                                                                                         SVMXC__Product__c = ipWrapper.ipToInstall.SVMXC__Product__c ,
                                                                                         SVMXC__Site__c = loc.id,
                                                                                         SVMXC__Company__c = loc.SVMXC__Account__c,
                                                                                         SVMXC__Access_Hours__c = loc.SVMXC__Preferred_Business_Hours__c,
                                                                                         SVMXC__City__c = loc.SVMXC__City__c,
                                                                                         SVMXC__Contact__c = loc.SVMX_PS_Contact__c,
                                                                                         SVMXC__Country__c = loc.SVMXC__Country__c,
                                                                                         SVMX_Language__c = loc.SVMX_Language__c,
                                                                                         CurrencyIsoCode = loc.CurrencyIsoCode,
                                                                                         SVMXC__Preferred_Technician__c = loc.SVMX_PS_Preferred_Technician__c,
                                                                                         SVMXC__State__c = loc.SVMXC__State__c,
                                                                                         SVMXC__Street__c = loc.SVMXC__Street__c,
                                                                                         SVMXC__Zip__c = loc.SVMXC__Zip__c,
                                                                                         SVMX_Make__c = ipWrapper.ipToInstall.SVMX_Make__c,
                                                                                         SVMX_Model__c = ipWrapper.ipToInstall.SVMX_Model__c ,
                                                                                         SVMX_Service_Family__c = ipWrapper.ipToInstall.SVMX_Service_Family__c,
                                                                                         SVMXC__Status__c = 'Installed' );
                                                
                        ipList.add(ip);                                     
                      }           
               }
                        
                  
             }
             if(ipList.size() > 0 )
                       insert ipList;
        }
                
         PageReference p = new PageReference('/' + location.id);
         p.setRedirect(true);
         return p;
   }
        
   public PageReference cancel()
   {
         return new PageReference('/'+ location.id);
   }
        
   public class IPWrapper
   {
         public Integer id { get ; set ; }
         public SVMXC__Installed_Product__c ipToInstall { get ; set ; }
        public Integer quantity { get ; set ;}
       public IPWrapper(){
           quantity = 1;
       }
   }
    
   public void change()
   {
         
        set<id> prodDetailset = new set<id>();
       /* Integer rowId = Integer.valueOf(ApexPages.currentPage().getParameters().get('rowId'));
        id prodDetail = ipWrapperList[rowId].ipToInstall.SVMX_ProductDetail__c;
        ProductDetail__c prod = new ProductDetail__c();
           prod= [select ProductRef__c, ProductRef__r.ClassificationMake__c, ProductRef__r.ClassificationModel__c, ProductRef__r.ClassificationServiceFamily__c  from ProductDetail__c where id = : prodDetail];
          ipWrapperList[rowId].ipToInstall.SVMXC__Product__c = prod.ProductRef__c;
         ipWrapperList[rowId].ipToInstall.SVMX_Make__c = prod.ProductRef__r.ClassificationMake__c;
         ipWrapperList[rowId].ipToInstall.SVMX_Model__c = prod.ProductRef__r.ClassificationModel__c;
         ipWrapperList[rowId].ipToInstall.SVMX_Service_Family__c = prod.ProductRef__r.ClassificationServiceFamily__c; */

       
       
        for(IPWrapper ipWrapper : ipWrapperList){
            id prodDetail = ipWrapper.ipToInstall.SVMX_ProductDetail__c;
            prodDetailset.add(prodDetail);
        }
        Map<id,ProductDetail__c> prodMap = new Map<id,ProductDetail__c>([select ProductRef__c, ProductRef__r.name, SVMX_Make__c, SVMX_Model__c, SVMX_Service_Family__c from ProductDetail__c where id in : prodDetailset]);
         for(IPWrapper ipWrapper : ipWrapperList){
             ipWrapper.ipToInstall.SVMXC__Product__c = prodMap.get(ipWrapper.ipToInstall.SVMX_ProductDetail__c).ProductRef__c;
             if(ipWrapper.ipToInstall.SVMX_Make__c == null|| ipWrapper.ipToInstall.SVMX_Make__c == '')
             	ipWrapper.ipToInstall.SVMX_Make__c = prodMap.get(ipWrapper.ipToInstall.SVMX_ProductDetail__c).SVMX_Make__c;
             if(ipWrapper.ipToInstall.SVMX_Model__c == null|| ipWrapper.ipToInstall.SVMX_Model__c == '')
             	ipWrapper.ipToInstall.SVMX_Model__c = prodMap.get(ipWrapper.ipToInstall.SVMX_ProductDetail__c).SVMX_Model__c;
             if(ipWrapper.ipToInstall.SVMX_Service_Family__c == null|| ipWrapper.ipToInstall.SVMX_Service_Family__c == '')
             	ipWrapper.ipToInstall.SVMX_Service_Family__c = prodMap.get(ipWrapper.ipToInstall.SVMX_ProductDetail__c).SVMX_Service_Family__c;
             ipWrapper.ipToInstall.SVMX_PS_Product_Name__c = prodMap.get(ipWrapper.ipToInstall.SVMX_ProductDetail__c).ProductRef__r.name;
        }  
     }
}