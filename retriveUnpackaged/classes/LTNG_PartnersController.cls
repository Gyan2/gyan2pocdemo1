public without sharing class LTNG_PartnersController {


    @AuraEnabled
    public static PartnersWrapper auraGetPartners(List<String> fields, Id recordId, String queryOrder, Integer queryLimit){
        PartnersWrapper returned = new PartnersWrapper();
        queryLimit = Integer.valueOf(queryLimit); // Absurd? agreed, but it fails otherwise
        Id accountId, opportunityId;

        if (recordId.getSobjectType() == Opportunity.getSObjectType()) {
            accountId = [SELECT AccountId FROM Opportunity WHERE Id = :recordId].AccountId;
            opportunityId = recordId;
        } else {
            accountId = recordId;
        }

        returned.hasEditAccess = [SELECT RecordId, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId = :recordId].HasEditAccess;

        List<String> fieldsToQuery = new List<String>();
        for(String forField : fields) {
            Schema.DescribeFieldResult forFieldDescribe = Util.Describe.getField('Partner',forField);
            if (forFieldDescribe.isAccessible() || forField == 'AccountToId' || forField == 'Role' || forField == 'IsPrimary' || forField == 'OpportunityId') {
                String forFieldDescribeType = String.valueOf(forFieldDescribe.getType());
                String forFieldDescribeLabel = forFieldDescribe.getLabel();

                if (forFieldDescribeType == 'REFERENCE') { // Lookup or Roll-up
                    fieldsToQuery.add(forField);
                    returned.fields.put(forField, new FieldDefinition(forField, forFieldDescribeLabel, forFieldDescribeType));
                    if (forField.endsWith('__c')) {
                        fieldsToQuery.add(forField.left(forField.length() - 1) + 'r.Name');
                    } else if (forField.endsWith('Id')) {
                        fieldsToQuery.add(forField.left(forField.length() - 2) + '.Name');
                    }
                } else if (forFieldDescribeType == 'PICKLIST') { // Picklist
                    String endField = forField.replaceAll('.*\\.', '');
                    fieldsToQuery.add('toLabel(' + forField + ') ' + endField);
                    returned.fields.put(endField, new FieldDefinition(endField, forFieldDescribeLabel, forFieldDescribeType));
                } else {
                    fieldsToQuery.add(forField);
                    returned.fields.put(forField, new FieldDefinition(forField, forFieldDescribeLabel, forFieldDescribeType));
                }

                System.debug(forFieldDescribe.getType() + ':' + forFieldDescribe.getLabel());
            }
        }


        returned.records = getPartnersByQuery(fieldsToQuery, accountId, opportunityId, queryOrder, queryLimit);
            // new PartnerQuery().getFromQuery('SELECT ' + String.join(fieldsToQuery, ',') + ' FROM Partner ' + whereString + (!String.isBlank(queryOrder)?' ORDER BY ' + queryOrder:'') + (queryLimit>0?' LIMIT :queryLimit':''), recordId, queryLimit);
            //database.query('SELECT ' + String.join(fieldsToQuery, ',') + ' FROM Partner ' + whereString + (!String.isBlank(queryOrder)?' ORDER BY ' + queryOrder:'') + (queryLimit>0?' LIMIT :queryLimit':''));

        // Creation of the different roles picklist
        for (Schema.PicklistEntry forPickVal : Partner.Role.getDescribe().getPicklistValues()) {
            if (forPickVal.isActive()) {
                returned.roles.add(new RoleDescribe(forPickVal.getLabel(), forPickVal.getValue()));
            }
        }

        return returned;
    }


    public static List<Partner> getPartnersByQuery(List<String> fields, Id accountId, Id opportunityId, String queryOrder, Integer queryLimit) {
        List<Partner> returned;

        List<String> accountToFields = new List<String>{'Name'};
        List<String> opportunityFields = new List<String>{'Name'};


        for(String forField : fields) {
            if ((forField.startsWithIgnoreCase('AccountTo.') && forField != 'AccountTo.Name' ) || forField.startsWithIgnoreCase('toLabel(AccountTo.')) {
                accountToFields.add(forField.replaceAll('^(?i)AccountTo.','').replaceAll('^(?i)toLabel\\(AccountTo.','toLabel('));
            } else if ((forField.startsWithIgnoreCase('Opportunity.') && forField != 'Opportunity.Name') || forField.startsWithIgnoreCase('toLabel(Opportunity.')){
                opportunityFields.add(forField.replaceAll('^(?i)Opportunity.','').replaceAll('^(?i)toLabel\\(Opportunity.','toLabel('));
            }
        }

        if (opportunityId == null) {
            returned = database.query('SELECT AccountToId, AccountTo.Name, OpportunityId, Opportunity.Name, IsPrimary, toLabel(Role) Role FROM Partner WHERE AccountFromId = :accountId' + (String.isBlank(queryOrder)?'':' ORDER BY ' + queryOrder) + (queryLimit==null || queryLimit <= 0?'':' LIMIT :queryLimit'));
        } else {
            returned = database.query('SELECT AccountToId, AccountTo.Name, OpportunityId, Opportunity.Name, IsPrimary, toLabel(Role) Role FROM Partner WHERE AccountFromId = :accountId AND OpportunityId = :opportunityId' + (String.isBlank(queryOrder)?'':' ORDER BY ' + queryOrder) + (queryLimit==null || queryLimit <= 0?'':' LIMIT :queryLimit'));
        }

        Map<Id, Account> withSharingAccounts = new Map<Id, Account>();
        Map<Id, Opportunity> withSharingOpportunities = new Map<Id, Opportunity>();

        for (Partner forPartner : returned) {
            withSharingAccounts.put(forPartner.AccountToId, null);
            if (forPartner.OpportunityId != null) {
                withSharingOpportunities.put(forPartner.OpportunityId, null);
            }
        }
        WithSharing ws = new WithSharing();
        withSharingAccounts = new Map<Id, Account>(ws.getAccountsFromQuery(accountToFields, withSharingAccounts.keySet()));
        withSharingOpportunities = new Map<Id, Opportunity>(ws.getOpportunitiesFromQuery(opportunityFields, withSharingOpportunities.keySet()));

        System.debug(withSharingAccounts);
        System.debug(withSharingOpportunities);

        for (Partner forPartner : returned) {
            Account forWithSharingAccount = withSharingAccounts.get(forPartner.AccountToId);
            Opportunity forWithSharingOpportunity = withSharingOpportunities.get(forPartner.OpportunityId);

            if (forWithSharingAccount != null) {
                forPartner.AccountTo = forWithSharingAccount;
            }
            if (forWithSharingOpportunity != null) {
                forPartner.Opportunity = forWithSharingOpportunity;
            }
        }


        return returned;
    }

    public with sharing class WithSharing {
        public List<Account> getAccountsFromQuery(List<String> fields, Set<Id> accountIds) {
            return (List<Account>)database.query('SELECT ' + String.join(fields, ',') + ' FROM Account WHERE Id IN :accountIds');
        }
        public List<Opportunity> getOpportunitiesFromQuery(List<String> fields, Set<Id> opportunityIds) {
            return (List<Opportunity>)database.query('SELECT ' + String.join(fields, ',') + ' FROM Opportunity WHERE Id IN :opportunityIds');
        }
        public void doDelete(Partner partner) {
            delete partner;
        }
        public void doInsert(List<Partner> partners) {
            insert partners;
        }
    }


    @AuraEnabled public static void auraDeletePartner(Id recordId) {
        new WithSharing().doDelete(new Partner(Id = recordId));
    }

    @AuraEnabled public static void auraSavePartners(String partnersData){
        List<Partner> partners = new List<Partner>();

        Set<Id> needsEditAccess = new Set<Id>();
        Set<Id> needsReadAccess = new Set<Id>();

        for (Map<String, Object> forPartner : (List<Map<String,Object>>)JSON.deserialize(partnersData,List<Map<String,String>>.class)) {
            Partner thePartner = new Partner();
            System.debug(forPartner);
            if (forPartner.containsKey('AccountFromId')){
                thePartner.AccountFromId = String.valueOf(forPartner.get('AccountFromId'));
                needsEditAccess.add(thePartner.AccountFromId);
            }
            if (forPartner.containsKey('AccountToId')){
                thePartner.AccountToId = String.valueOf(forPartner.get('AccountToId'));
                needsReadAccess.add(thePartner.AccountToId);
            }
            if (forPartner.containsKey('OpportunityId')){
                thePartner.OpportunityId = String.valueOf(forPartner.get('OpportunityId'));
                needsEditAccess.add(thePartner.OpportunityId);
            }
            if (forPartner.containsKey('Role')) thePartner.Role = String.valueOf(forPartner.get('Role'));
            if (forPartner.containsKey('IsPrimary')) thePartner.IsPrimary = Boolean.valueOf(forPartner.get('IsPrimary'));

            partners.add(thePartner);
        }

        Set<Id> mergeAccessIds = new Set<Id>();
        mergeAccessIds.addAll(needsEditAccess);
        mergeAccessIds.addAll(needsReadAccess);

        List<UserRecordAccess> notEnoughAccesses = [SELECT RecordId, HasReadAccess, HasEditAccess FROM UserRecordAccess WHERE UserId = :UserInfo.getUserId() AND RecordId IN :mergeAccessIds];

        for (UserRecordAccess forURA : notEnoughAccesses) {
            if (forURA.HasReadAccess) needsReadAccess.remove(forURA.RecordId);
            if (forURA.HasEditAccess) needsEditAccess.remove(forURA.RecordId);
        }


        if (needsReadAccess.isEmpty() && needsEditAccess.isEmpty()) {
            insert partners;
        } else {

            mergeAccessIds = new Set<Id>();
            mergeAccessIds.addAll(needsEditAccess);
            mergeAccessIds.addAll(needsReadAccess);

            throw new AuraHandledException('You don\'t have the required permissions for the following records: ' + String.join(new List<Id>(mergeAccessIds), ','));
        }
    }

    public without sharing class PartnersWrapper {
        @AuraEnabled public Boolean hasEditAccess;
        @AuraEnabled public Map<String,Map<String,String>> labels = new Map<String,Map<String,String>>{
            'Partner' => new Map<String,String> {
                'LabelPlural' => Partner.getSObjectType().getDescribe().getLabelPlural(),
                'AccountTo' => /*'AccountToLabel',*/Util.Describe.getField('Partner','AccountTo.Name').getLabel(),
                'Role' => /*'Role Label',*/Util.Describe.getField('Partner','Role').getLabel(),
                'IsPrimary' => /*'IsPrimary Label'/*/Util.Describe.getField('Partner','IsPrimary').getLabel()
            }};
        @AuraEnabled public Map<String,FieldDefinition> fields = new Map<String,FieldDefinition>();
        @AuraEnabled public List<Partner> records = new List<Partner>();
        @AuraEnabled public List<RoleDescribe> roles = new List<RoleDescribe>();
    }

    public class FieldDefinition {
        @AuraEnabled public String apiName;
        @AuraEnabled public String label;
        @AuraEnabled public String type;

        public FieldDefinition(String apiName, String label, String type) {
            this.apiName = apiName;
            this.label = label;
            this.type = type;
        }
    }

    public class RoleDescribe {
        @AuraEnabled public String label;
        @AuraEnabled public String value;

        public RoleDescribe (String paramLabel, String paramValue) {
            label = paramLabel;
            value = paramValue;
        }
    }

}