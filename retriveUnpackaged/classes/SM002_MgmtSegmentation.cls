/*******************************************************************************************************
* Class Name      	: SM002_MgmtSegmentation
* Description		: Service Manager for determining and auto-applying the dormakaba management 
						segmentation model across core objects.
* Author          	: Paul Carmuciano
* Created On      	: 2017-07-10
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-07-10         1000                 Initial version
******************************************************************************************************/
public class SM002_MgmtSegmentation {
    
    /***
* Method name	: applyManagementSegmentation
* Description	: This method applies the Management_Segmentation__mdt to the generic sobject and 
					returns the sobject to the trigger handler
* Author		: Paul Carmuciano
* Return Type	: Sobject
* Parameter		: Sobject so = the single object from the running trigger context
				, Management_Segmentation__mdt msmdt = the metadata applicalbe to this record
				, Schema.DescribeFieldResult regionField = strictly typed field key to update
				, Schema.DescribeFieldResult segmentField = strictly typed field key to update
*/
    public static Sobject applyManagementSegmentation(
        Sobject so, Management_Segmentation__mdt msmdt, 
        Schema.DescribeFieldResult regionField, Schema.DescribeFieldResult segmentField) 
    {
        try {
            so.put(regionField.name, (msmdt==null ? null : msmdt.Region__c) );
            so.put(segmentField.name, (msmdt==null ? null : msmdt.Segment__c) );
            
        } catch (Exception e) {
            // such as sobject gone
            system.debug('applyManagementSegmentation Exception\n'+e.getMessage()+'\n'+e.getStackTraceString());
        }
        return so;
    }
    /***
* Method name	: applyManagementSegmentation (Overload)
* Description	: This method overloads the applyManagementSegmentation method allowing for old vs. new 
					sobject comparison and returns the sobject to the trigger handler
* Author		: Paul Carmuciano
* Return Type	: Sobject
* Parameter		: Sobject oldSo = the single object.old from the running trigger context
				, Sobject so = the single object from the running trigger context
				, Management_Segmentation__mdt msmdt = the metadata applicalbe to this record
				, Schema.DescribeFieldResult sourceField = strictly typed source field key to compare
				, Schema.DescribeFieldResult regionField = strictly typed field key to update
				, Schema.DescribeFieldResult segmentField = strictly typed field key to update
*/
    public static Sobject applyManagementSegmentation(
        Sobject oldSo, Sobject so, Management_Segmentation__mdt msmdt, 
        Schema.DescribeFieldResult sourceField, Schema.DescribeFieldResult regionField, Schema.DescribeFieldResult segmentField) 
    {
        // Apply the Management Segmentation
        if (so.get(sourceField.name) != oldSo.get(sourceField.name) ) {
            return applyManagementSegmentation(so, msmdt, regionField, segmentField);
        } else {
            return so;
        }
    }   
    
    
    
    
}