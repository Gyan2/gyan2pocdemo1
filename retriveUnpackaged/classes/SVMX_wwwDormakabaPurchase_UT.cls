@IsTest
private class SVMX_wwwDormakabaPurchase_UT
{
    static testMethod void coverTypes(){

        new SVMX_wwwDormakabaPurchase.BusinessTransactionDocumentReference();
        new SVMX_wwwDormakabaPurchase.Description();
        new SVMX_wwwDormakabaPurchase.Email();
        new SVMX_wwwDormakabaPurchase.Address();
        new SVMX_wwwDormakabaPurchase.Telephone();
        new SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader();
        new SVMX_wwwDormakabaPurchase.ProductInternalID();
        new SVMX_wwwDormakabaPurchase.BusinessDocumentMessageID();
        new SVMX_wwwDormakabaPurchase.MEDIUM_Name();
        new SVMX_wwwDormakabaPurchase.Facsimile();
        new SVMX_wwwDormakabaPurchase.BusinessDocumentTextCollectionText();
        new SVMX_wwwDormakabaPurchase.BusinessScopeID();
        new SVMX_wwwDormakabaPurchase.Communication();
        new SVMX_wwwDormakabaPurchase.BusinessDocumentTextCollection();
        new SVMX_wwwDormakabaPurchase.EmailURI();
        new SVMX_wwwDormakabaPurchase.BusinessScope();
        new SVMX_wwwDormakabaPurchase.BusinessTransactionDocumentID();
        new SVMX_wwwDormakabaPurchase.Quantity();
        new SVMX_wwwDormakabaPurchase.Text();
        new SVMX_wwwDormakabaPurchase.PhysicalAddress();
        new SVMX_wwwDormakabaPurchase.PartyStandardID();
        new SVMX_wwwDormakabaPurchase.Web();
        new SVMX_wwwDormakabaPurchase.PhoneNumber();
        new SVMX_wwwDormakabaPurchase.PartyInternalID();
        new SVMX_wwwDormakabaPurchase.BusinessScopeInstanceID();
        
    }
}