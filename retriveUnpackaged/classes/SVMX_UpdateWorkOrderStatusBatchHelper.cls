public class SVMX_UpdateWorkOrderStatusBatchHelper{

  public static Map<String,String> allfieldstoQuery(List<String> enabledCountries)
   {
       Map<String,String>  fieldsToQuery=new Map<String,String>();
       String fieldstoWorkOrderQuery='';
       String fieldstoWorkDetailQuery='';
       String fieldstoWorkDetailWhereQuery='';
       
       
       List<SVMX_Billing_Rules__c> billingRules;
       set<String>   billingRuleWorkDetailWherefields=new set<String>();
       
       set<String>   billingRuleWorkOrderfields=new set<String>();
       set<String>   billingRuleWorkDetailfields=new set<String>();
       
       String queryBillingRule='Select Id,SVMX_Country__c,SVMX_Field__c,SVMX_Object__c,SVMX_Value__c,SVMX_Line_Type__c from SVMX_Billing_Rules__c';
       queryBillingRule+=' where SVMX_Country__c IN(\'' + string.join(New List<String>(enabledCountries),'\',\'') + '\')';
       billingRules=Database.query(queryBillingRule);     
       for(SVMX_Billing_Rules__c   billingRule:billingRules){
           if(billingRule.SVMX_Object__c =='Work Order'){
             billingRuleWorkOrderfields.add(billingRule.SVMX_Field__c);
           }else if(billingRule.SVMX_Object__c =='Work Details'){
             
               system.debug(' not Line Type ====>>'+billingRules);
               billingRuleWorkDetailfields.add(billingRule.SVMX_Field__c);
             
             if(billingRule.SVMX_Line_Type__c != ''){
                  system.debug(' equal ---- Line Type ====>>'+billingRules);
                 system.debug(' equal Line Type ====>>'+billingRule.SVMX_Line_Type__c);
                 billingRuleWorkDetailWherefields.add(billingRule.SVMX_Line_Type__c);
             }
           }
       }
       for(String str:billingRuleWorkOrderfields){
            fieldstoWorkOrderQuery=fieldstoWorkOrderQuery + ', ' +str;
       }
        for(String str:billingRuleWorkDetailfields){
            fieldstoWorkDetailQuery=fieldstoWorkDetailQuery + ', ' +str;
       }
       for(String str:billingRuleWorkDetailWherefields){
            fieldstoWorkDetailWhereQuery=fieldstoWorkDetailWhereQuery + ', ' +str;
       }
       fieldsToQuery.put('Work Order',fieldstoWorkOrderQuery);
       fieldsToQuery.put('Work Details',fieldstoWorkDetailQuery);
       fieldsToQuery.put('Line Type',fieldstoWorkDetailWhereQuery);
       return fieldsToQuery;
    }
   public static Boolean billingRuleValidation(SObject  WorkOrder,List<SVMX_Billing_Rules__c> billingrules)
   {
       Boolean billingRuleFalg=true;
     
       for(SVMX_Billing_Rules__c billRule:billingrules){
          
          if(WorkOrder.get('SVMXC__Country__c') == billRule.SVMX_Country__c){
             if(billRule.SVMX_Object__c == 'Work Order'){
              //if(WorkOrder.get(billRule.SVMX_Field__c) !=null){
            
                  if(WorkOrder.get(billRule.SVMX_Field__c) == billRule.SVMX_Value__c){
                       
                      // billingRuleFalg=true;
                  }else if(WorkOrder.get(billRule.SVMX_Field__c) == true || WorkOrder.get(billRule.SVMX_Field__c) == false){
                        if(WorkOrder.get(billRule.SVMX_Field__c) == Boolean.ValueOf(billRule.SVMX_Value__c)){
                         }else{
                         billingRuleFalg=false;
                         }
                  
                  }else  if((WorkOrder.get(billRule.SVMX_Field__c) != true || WorkOrder.get(billRule.SVMX_Field__c) != false)){
                      billingRuleFalg=true;
                  }else{
                    billingRuleFalg=false;
                  
                  }
             // }else{
                   // billingRuleFalg=false;
             // }
            }
       
         } 
       }
 
      return billingRuleFalg;
    }
   public static Boolean billingRuleWorkDetails(SObject  WorkDetail,List<SVMX_Billing_Rules__c> billingrules)
   {
       Boolean billingRuleFalg=true;
     
       for(SVMX_Billing_Rules__c billRule:billingrules){
            if(billRule.SVMX_Object__c == 'Work Details'){
              //if(WorkDetail.get(billRule.SVMX_Field__c) !=null){
                system.debug('billRule.SVMX_Line_Type__c  333'+billRule.SVMX_Line_Type__c);
                system.debug('WorkDetail.999'+WorkDetail.get('SVMXC__Line_Type__c'));
                //system.debug('field type ===>>> '+ WorkDetail.get(billRule.SVMX_Field__c).getDescribe().getType());
                  if(WorkDetail.get(billRule.SVMX_Field__c) == billRule.SVMX_Value__c &&  billRule.SVMX_Line_Type__c == WorkDetail.get('SVMXC__Line_Type__c') ){
                      // billingRuleFalg=true;
                  }else if((WorkDetail.get(billRule.SVMX_Field__c) == true || WorkDetail.get(billRule.SVMX_Field__c) == false) ){
                         if(WorkDetail.get(billRule.SVMX_Field__c) == Boolean.ValueOf(billRule.SVMX_Value__c)){
                         }else{
                         billingRuleFalg=false;
                         system.debug('billingRuleFalg 111'+billingRuleFalg);
                         }
                  }else if((WorkDetail.get(billRule.SVMX_Field__c) != true ||  WorkDetail.get(billRule.SVMX_Field__c) != false)){
                      //billingRuleFalg=true;
                  }else{
                    billingRuleFalg=false;
                     system.debug('billingRuleFalg 222'+billingRuleFalg);
                  }
              //}else{
              //   billingRuleFalg=false;
              //}
          
             }
        }
         system.debug('billingRuleFalg --->>> END'+billingRuleFalg);
        return billingRuleFalg;
   }
}