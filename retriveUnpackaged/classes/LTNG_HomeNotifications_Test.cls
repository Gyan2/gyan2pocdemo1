@isTest
public class LTNG_HomeNotifications_Test {

    public static testMethod void testAll() {
        LTNG_HomeNotifications.NotificationsWrapper notificationsWrapper = LTNG_HomeNotifications.auraGetNotifications();

        System.assertEquals(0, notificationsWrapper.notifications.size());

        List<User> users = LTNG_HomeNotifications.auraSearchUsers('"System Administrator"', new Map<String,Boolean>{
            'Name' => true,
            'Email' => true,
            'Alias' => true,
            'Username' => true,
            'Profile' => true,
            'UserRole' => true,
            'PermissionSet' => false
        });

        System.assertEquals([
                SELECT Id
                FROM User
                WHERE IsActive = true AND

                    (
                        Name = 'System Administrator' OR
                        Email = 'System Administrator' OR
                        Alias = 'System Administrator' OR
                        Username = 'System Administrator' OR
                        Profile.Name = 'System Administrator' OR
                        UserRole.Name = 'System Administrator'
                    )].size(), users.size());

        Notification__c notification = new Notification__c(
                Name = 'Test Notification',
                DefaultContent__c = '<b>Test Notification</b>',
                StartAt__c = System.now()
        );

        insert notification;

        LTNG_HomeNotifications.AddUsersInitialDataWrapper initialDataWrapper = LTNG_HomeNotifications.auraGetInitialAddUsersInformation(notification.Id);

        System.assertEquals(0, initialDataWrapper.userNotifications.size());

        LTNG_HomeNotifications.auraAddUsers(notification.Id, new List<User>{new User(Id = UserInfo.getUserId())});

        UserNotification__c userNotification = [select id from UserNotification__c];


        notificationsWrapper = LTNG_HomeNotifications.auraGetNotifications();

        System.assertEquals(1, notificationsWrapper.notifications.size());

        initialDataWrapper = LTNG_HomeNotifications.auraGetInitialAddUsersInformation(notification.Id);

        System.assertEquals(1, initialDataWrapper.userNotifications.size());


        LTNG_HomeNotifications.auraMarkAsRead(notification.Id);


        notificationsWrapper = LTNG_HomeNotifications.auraGetNotifications();

        System.assertEquals(0, notificationsWrapper.notifications.size());

        initialDataWrapper = LTNG_HomeNotifications.auraGetInitialAddUsersInformation(notification.Id);

        System.assertEquals(1, initialDataWrapper.userNotifications.size());


    }
}