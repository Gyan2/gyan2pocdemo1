/************************************************************************************************************
Description: UT for SVMX_CreateMultipleChildIPs_UT class.
 
Author: Ranjitha
Date: 17-05-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest(SeeAllData = false)
public class SVMX_CreateMultipleChildIPs_UT {
    
    public static testMethod void multipleIPS() {
      Account account = SVMX_TestUtility.CreateAccount('Testing Account', 'Norway',true) ;
      account.SAPNumber__c = 'P1234' ;
        update account ;
            
      Contact contact = SVMX_TestUtility.CreateContact(account.id,true) ;
    
        SVMXC__Site__c  location = SVMX_TestUtility.CreateLocation('Service Location',account.id,true);

        Product2 product = SVMX_TestUtility.CreateProduct('Test prod',false);
        product.SVMX_SAP_Product_Type__c = 'Stock Part';
        product.ClassificationMake__c='A1';
        product.ClassificationModel__c = 'test';
        product.ClassificationServiceFamily__c ='Accordion Shutter';
        insert product;

        ProductDetail__c prd = new ProductDetail__c();
        prd.ProductRef__c = product.id;
        prd.Language__c ='NO';
        insert prd;

        SVMXC__Installed_Product__c installedProduct = SVMX_TestUtility.CreateInstalledProduct(account.id,null,location.id,false);
        installedProduct.SVMXC__Product__c = product.id;
        installedProduct.SVMX_ProductDetail__c = prd.id;
        insert installedProduct;
  
        SVMXC__Installed_Product__c installedProduct2 = SVMX_TestUtility.CreateInstalledProduct(account.id,null,location.id,false);
        installedProduct2.SVMXC__Product__c = product.id;
        installedProduct2.SVMX_ProductDetail__c = prd.id;
        insert installedProduct2;
      

       PageReference pageRef = Page.SVMX_CreateMultipleIPs ;
       pageRef.getParameters().put('Id', installedProduct.id) ;
          Test.setCurrentPage(pageRef);
        
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.StandardController(installedProduct);
         SVMX_CreateMultipleChildIPs_Controller sapAvailability = new SVMX_CreateMultipleChildIPs_Controller(sc) ;
          SVMX_CreateMultipleChildIPs_Controller.IPWrapper ip = new SVMX_CreateMultipleChildIPs_Controller.IPWrapper();
          ip.ipToInstall = installedProduct;
          ip.id= 1;
          ip.quantity =1;

          sapAvailability.ipWrapperList.add(ip);
         sapAvailability.insProd = installedProduct;
         sapAvailability.ip = installedProduct;
         sapAvailability.account= account;
   
                 
        sapAvailability.addRow();
        sapAvailability.createIP();
        sapAvailability.change();
        sapAvailability.cancel(); 
        pageRef.getParameters().put('rowId', '1') ;
        sapAvailability.deleterow();
        
        Test.stopTest();
    
    
    }
}