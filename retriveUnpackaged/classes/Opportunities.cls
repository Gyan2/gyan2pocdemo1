public with sharing class Opportunities extends CoreSObjectDomain {

    public Opportunities(List<Opportunity>paramRecords){
        super(paramRecords);
    }

    public override void onApplyDefaults() {
        applyManagementFieldsDefaults((List<Opportunity>)Records);
    }

    private void applyManagementFieldsDefaults(List<Opportunity> paramRecords){
        /* TODO: Check. Logic copied from the Process Builder, but it makes no sense to "freeze" from the createdBy of the Opportunity, we might want to change it to owner since once we have an integration it will be TECH PI*/
        User currentUser = Util.CurrentUserRecord;
        for (Opportunity forOpp : paramRecords) {
            forOpp.ManagementCountry__c = currentUser.UserCountry__c;
            forOpp.ManagementRegion__c = currentUser.UserRegion__c;
            forOpp.ManagementSegment__c = currentUser.UserSegment__c;
        }
    }

    private void copyProjectName(List<Opportunity> paramRecords){
        for (Opportunity forOpp : paramRecords) {
            forOpp.ProjectSearch__c  = forOpp.ProjectNameAuto__c;
        }
    }

    public override void onBeforeInsert(){
        copyProjectName((List<Opportunity>)Records);
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        copyProjectName((List<Opportunity>)Records);
    }

    public class Constructor implements CoreSObjectDomain.IConstructable{
        public CoreSObjectDomain construct(List<sObject> sObjectList) {
            return new Opportunities(sObjectList);
        }
    }
}