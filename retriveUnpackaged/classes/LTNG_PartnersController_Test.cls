@IsTest
private class LTNG_PartnersController_Test {

    @isTest
    public static void auraGetPartners(){
        Account acc1 = new Account(Name = 'Test Account 1', BillingCountryCode = 'ES');
        Account acc2 = new Account(Name = 'Test Account 2', BillingCountryCode = 'DE');
        Account acc3 = new Account(Name = 'Test Account 3', BillingCountryCode = 'CH');
        insert new List<Account>{acc1,acc2,acc3};

        Opportunity opp31 = new Opportunity(Name = 'Test Opportunity 3.1', AccountId = acc3.Id, StageName = 'Prospecting', CloseDate = System.today());
        insert opp31;

        LTNG_PartnersController.auraSavePartners('[' +
                '{"AccountFromId" : "' + acc1.Id + '", "AccountToId" : "' + acc2.Id + '"},' +
                '{"AccountFromId" : "' + acc2.Id + '", "AccountToId" : "' + acc1.Id + '"},' +
                '{"OpportunityId" : "' + opp31.Id + '", "AccountToId" : "' + acc1.Id + '", "Role" : "Advertiser", "IsPrimary" : true}' +
                ']');
        // no clue why it does not create the mirror automatically ...
        //Test.startTest();
            System.assertEquals(3, [SELECT AccountFromId FROM Partner ].size());

            LTNG_PartnersController.PartnersWrapper response = LTNG_PartnersController.auraGetPartners(new List<String>{'Accounttoid', 'accountto.billingstreet', 'opportunityid','opportunity.stagename','isPrimary'}, acc1.Id, null, null);
            System.assertEquals(1,response.records.size());
            response = LTNG_PartnersController.auraGetPartners(new List<String>{'Accounttoid', 'accountto.billingstreet', 'opportunityid','opportunity.stagename','isPrimary'}, opp31.Id, null, null);
            System.assertEquals(1, response.records.size());

            LTNG_PartnersController.auraDeletePartner([SELECT Id FROM Partner LIMIT 1].Id);
            LTNG_PartnersController.auraDeletePartner([SELECT Id FROM Partner LIMIT 1].Id);
            LTNG_PartnersController.auraDeletePartner([SELECT Id FROM Partner LIMIT 1].Id);

        //Test.stopTest();
        System.assertEquals(0, [SELECT AccountFromId FROM Partner ].size());

    }
}