/**
 * Created by rebmangu on 12/01/2018.
 */

public with sharing class Products extends CoreSObjectDomain{

    public Products(List<Project__c> sObjectList){
        super(sObjectList);
    }

    public override void onApplyDefaults() {
        // Apply defaults to Products

    }

    public override void onValidate() {
        // Validate Products
    }

    public override void onValidate(Map<Id,SObject> existingRecords) {
        // Validate changes to Products

    }

    public override void onBeforeInsert(){
        ProductServices.copySAPPrettyNumber((List<Product2>)records);
        ProductServices.mapProductHierarchyForServiceMax((List<Product2>)records);
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        // Related Products
        ProductServices.copySAPPrettyNumber((List<Product2>)records);
        ProductServices.mapProductHierarchyForServiceMax((List<Product2>)records);
    }

    public override void onBeforeDelete(){

    }

    public override void onAfterInsert(){

    }


    public override void onAfterUpdate(Map<Id,SObject> existingRecords){

    }

    public override void onAfterDelete(){

    }


    public override void onAfterUndelete(){

    }


    public class Constructor implements CoreSObjectDomain.IConstructable
    {
        public CoreSObjectDomain construct(List<SObject> sObjectList)
        {
            return new Products(sObjectList);
        }
    }
}