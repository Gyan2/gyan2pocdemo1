/**
 * Created by trakers on 03/04/2018.
 */

public with sharing class ProjectController {

    @AuraEnabled
    public static List<Object> checkForProjectsAround(String projectJson,Integer distance){
        Map<String,String> projectMapping = (Map<String,String>)JSON.deserialize(projectJson,Map<String,String>.class);
        List<Object> result = new List<Object>();
        Decimal latitude = Decimal.valueOf('47.4747738');//projectMapping.get('Geolocation__Latitude__s');
        Decimal longitude = Decimal.valueOf('8.5231009');//projectMapping.get('Geolocation__Longitude__s');
        String query = 'select id, DISTANCE(BuildingRef__r.Geolocation__c,GEOLOCATION('+latitude+','+longitude+'),\'km\') dist, CreatedDate,Name,AccountRef__r.Name,Geolocation__Latitude__s,Geolocation__Longitude__s,StreetAuto__c,CityAuto__c,ZipAuto__c,CountryAuto__c,' +
                'BuildingRef__c,BuildingRef__r.Geolocation__Latitude__s,BuildingRef__r.Name,BuildingRef__r.Geolocation__Longitude__s,BuildingRef__r.Street__c,BuildingRef__r.City__c,BuildingRef__r.Zip__c,BuildingRef__r.Country__c' +
                ' from Project__c where ' +
                'BuildingRef__c <> null AND DISTANCE(BuildingRef__r.Geolocation__c,GEOLOCATION('+latitude+','+longitude+'),\'km\') <: distance order by DISTANCE(BuildingRef__r.Geolocation__c,GEOLOCATION('+latitude+','+longitude+'),\'km\')';

        System.debug(query);
        for(Project__c project : (List<Project__c>)Database.query(query)){
            Map<String, Object> fieldsToValue = project.getPopulatedFieldsAsMap().clone();
            Location projectLocation = Location.newInstance(latitude,longitude);
            Location otherLocation = Location.newInstance(project.BuildingRef__r.Geolocation__Latitude__s,project.BuildingRef__r.Geolocation__Longitude__s);
            // set distance
            Decimal dist = Decimal.valueOf(Location.getDistance(projectLocation, otherLocation, 'km')).setScale(1);
            fieldsToValue.put('distance',dist);

            result.add(fieldsToValue);
        }

        return result;
    }

    @AuraEnabled
    public static Project__c getProject(String recordId,List<String> fields){
        return (Project__c)Database.query('select '+String.join(fields,',')+' FROM Project__c where Id =: recordId limit 1');
    }
    @AuraEnabled
    public static Building__c getBuilding(String recordId){
        return [select id,Country__c,City__c,Street__c,Zip__c,CountryIsoCode__c from Building__c where Id =: recordId];
    }

    @AuraEnabled
    public static Map<String,Object> updateProject(Project__c project){
            Map<String,Object> result = new Map<String,Object>{
                    'status'=>200,
                    'message'=> null,
                    'messageToDisplay'=>null
            };

            try{
                update project;
                result.put('message',project.Id);
            }catch (Exception e){
                result.put('status',400);
                result.put('message',e.getLineNumber()+' - '+e.getMessage());
                result.put('messageToDisplay',e.getMessage());
            }

        return result;

    }

    @AuraEnabled
    public static Map<String,Object> insertProject(Project__c project){
        Map<String,Object> result = new Map<String,Object>{
                'status'=>200,
                'message'=> null,
                'messageToDisplay'=>null
        };


        try{
            // In case the building field is empty, we create a new building
            if(String.isBlank((String)project.get('BuildingRef__c'))){
                Building__c building;
                List<Building__c> buildings = [select id,Street__c,City__c,Country__c,Zip__c,CountryIsoCode__c,Geolocation__Latitude__s,Geolocation__Longitude__s from Building__c where Geolocation__Longitude__s = :project.Geolocation__Longitude__s AND Geolocation__Latitude__s =:project.Geolocation__Latitude__s limit 1];
                if(buildings.size() > 0){
                    building = buildings[0];

                    // We clone the address value of the building in the project
                    project.Street__c                  = building.Street__c;
                    project.City__c                    = building.City__c;
                    project.Zip__c                     = building.Zip__c;
                    project.Country__c                 = building.Country__c;
                    project.CountryIsoCode__c          = building.CountryIsoCode__c;
                    project.Geolocation__Latitude__s   = building.Geolocation__Latitude__s;
                    project.Geolocation__Longitude__s  = building.Geolocation__Longitude__s;

                }else{
                    building = new Building__c();
                    building.Street__c                  = project.Street__c;
                    building.City__c                    = project.City__c;
                    building.Zip__c                     = project.Zip__c;
                    building.Country__c                 = project.Country__c;
                    building.CountryIsoCode__c          = project.CountryIsoCode__c;
                    building.Geolocation__Latitude__s   = project.Geolocation__Latitude__s;
                    building.Geolocation__Longitude__s  = project.Geolocation__Longitude__s;
                    building.Name                       = 'Building ('+building.Street__c+', '+building.City__c+' '+building.Country__c+')';
                    insert building;
                }
                project.BuildingRef__c = building.Id;
            }


            insert project;
            result.put('message',project.Id);
        }catch (Exception e){
            result.put('status',400);
            result.put('message',e.getLineNumber()+' - '+e.getMessage());
            result.put('messageToDisplay',e.getMessage());
        }

        return result;

    }


}