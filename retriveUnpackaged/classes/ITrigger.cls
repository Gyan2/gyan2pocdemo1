/*******************************************************************************************************
* Class Name     	: ITrigger interface
* Description		: Adaptation of the Tony Scott Trigger Factory Pattern. While Hari Krishnan's variant 
*						may be more comprehensive eg; exception implementation, the pattern requires a 
*						separate Handler Class for each Trigger event which is superflous, carries
*						overhead, and also reduces overall contextual oversight. 
*
* Author          	: Paul Carmuciano
* Created On      	: 2017-07-10
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-07-10         1000                 Initial version
******************************************************************************************************/

/**
 * Interface containing methods Trigger Handlers must implement to enforce best practice
 * and bulkification of triggers.
 */
public interface ITrigger
{
    /**
     * bulkBefore
     *
     * This method is called prior to execution of a BEFORE trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    void bulkBefore();
 
    /**
     * bulkAfter
     *
     * This method is called prior to execution of an AFTER trigger. Use this to cache
     * any data required into maps prior execution of the trigger.
     */
    void bulkAfter();
 
    /**
     * beforeInsert
     *
     * This method is called iteratively for each record to be inserted during a BEFORE
     * trigger. Never execute any SOQL/SOSL etc in this and other iterative methods.
     */
    void beforeInsert(SObject so);
 
    /**
     * beforeUpdate
     *
     * This method is called iteratively for each record to be updated during a BEFORE
     * trigger.
     */
    void beforeUpdate(SObject oldSo, SObject so);
 
    /**
     * beforeDelete
     *
     * This method is called iteratively for each record to be deleted during a BEFORE
     * trigger.
     */
    void beforeDelete(SObject so);
 
    /**
     * afterInsert
     *
     * This method is called iteratively for each record inserted during an AFTER
     * trigger. Always put field validation in the 'After' methods in case another trigger
     * has modified any values. The record is 'read only' by this point.
     */
    void afterInsert(SObject so);
 
    /**
     * afterUpdate
     *
     * This method is called iteratively for each record updated during an AFTER
     * trigger.
     */
    void afterUpdate(SObject oldSo, SObject so);
 
    /**
     * afterDelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    void afterDelete(SObject so);
 
    /**
     * afterUndelete
     *
     * This method is called iteratively for each record deleted during an AFTER
     * trigger.
     */
    void afterUndelete(SObject so);
 
    /**
     * andFinally
     *
     * This method is called once all records have been processed by the trigger. Use this
     * method to accomplish any final operations such as creation or updates of other records.
     */
    void andFinally();
}