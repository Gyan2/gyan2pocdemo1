public without sharing class SVMX_ServContractLIneItemRollupsummary {

    public static void handleNoOfContractLineItemChildCreation(List<SVMX_Contract_Line_Items__c> contractLineItemList){
       List<SVMXC__Service_Contract__c> ct = new List<SVMXC__Service_Contract__c>();
       
       Set<Id> serviceContarctId = new Set<Id>();
       
       
       for(SVMX_Contract_Line_Items__c contractLineItem:contractLineItemList) {
        
          serviceContarctId.add(contractLineItem.SVMX_Service_Contract__c);   
      
       }   
       
       AggregateResult[] groupedResults = [SELECT COUNT(Id), SVMX_Service_Contract__c FROM SVMX_Contract_Line_Items__c where SVMX_Service_Contract__c IN :serviceContarctId AND SVMX_Is_Billable__c = true GROUP BY SVMX_Service_Contract__c ];
       
       for(AggregateResult ar:groupedResults) {
         
         Id parentid = (ID)ar.get('SVMX_Service_Contract__c');
         
         Integer count = (INTEGER)ar.get('expr0');
         
         SVMXC__Service_Contract__c servicecontarct = new SVMXC__Service_Contract__c(Id=parentid);
         
         servicecontarct.SVMX_No_of_IsBillable_Contract_Line_Item__c = count;
         
         ct.add(servicecontarct);
          
       } 
       update ct;
    }
}