/************************************************************************************************************
Description: Test class for the Controller Class (SVMX_PR_ServicStkAvailability_Controller) 
 
Author: Keshava Prasad 
Date: 18-04-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/
@isTest
public class SVMX_PR_ServicStkAvailability_UT {
    
    public static testMethod void checkIsStockAvailable() {
    
    Account account = SVMX_TestUtility.CreateAccount('Testing Account', 'Norway',true) ;
      Contact contact = SVMX_TestUtility.CreateContact(account.id,true) ;

      SVMXC__Site__c  location = SVMX_TestUtility.CreateLocation('Service Location',account.id,true);
        location.SVMXC_SAP_Storage_Location__c = '1234567';
        location.SVMXC_Plant_Number__c = 'P123';
        location.SVMXC__Stocking_Location__c = true;
        update location;
    
    SVMXC__Site__c  stockLocation = SVMX_TestUtility.CreateLocation('From Location',account.id,true);
        stockLocation.SVMXC_SAP_Storage_Location__c = '112345644';
        stockLocation.SVMXC_Plant_Number__c = 'T677';
        stockLocation.SVMXC__Stocking_Location__c = true;
        stockLocation.SVMXC__Location_Type__c = 'Internal';
        update stockLocation;

      SVMXC__Installed_Product__c installedProduct = SVMX_TestUtility.CreateInstalledProduct(account.id,null,location.id,true);
        Product2 product = SVMX_TestUtility.CreateProduct('Test prod',true);
        product.SVMX_SAP_Product_Type__c = 'Stock Part';
        Update product;


      Profile profile = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
      User user = SVMX_TestUtility.CreateUser('Testtech', profile.id, true);
      SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Svmx Tech2', user.id, null, true);
        tech.SVMXC__Inventory_Location__c = location.id;
        update tech;

		SVMXC__Parts_Request__c  partsReq = new SVMXC__Parts_Request__c () ;
        partsReq.SVMXC__Required_At_Location__c = location.id ;
        insert partsReq ;


        SVMXC__Parts_Request_Line__c partsReqLine = new SVMXC__Parts_Request_Line__c() ;
        partsReqLine.SVMXC__Product__c = product.id ;
        partsReqLine.SVMXC__Quantity_Required2__c = 1 ;
        partsReqLine.SVMX_Request_Lines_Converted__c = false ;
        partsReqLine.SVMXC__Line_Status__c = 'Open' ;
        partsReqLine.SVMXC__Parts_Request__c = partsReq.id ;
        insert partsReqLine;



       List<SVMXC__Product_Stock__c> psList = new List<SVMXC__Product_Stock__c>();
       SVMXC__Product_Stock__c ps = new SVMXC__Product_Stock__c();
       ps.SVMXC__Product__c = product.id;
       ps.SVMXC__Status__c = 'Available';
       ps.SVMXC__Quantity2__c = 2;
       ps.SVMXC__Location__c = stockLocation.id;
       insert ps;


    List<SVMX_PR_ServicStkAvailability_Controller.WrapPR> wrap = new List<SVMX_PR_ServicStkAvailability_Controller.WrapPR>() ;
    PageReference pageRef = Page.SVMX_PR_ServicStkAvailability ;
    pageRef.getParameters().put('partReqId', partsReq.id) ;
    Test.setCurrentPage(pageRef);
    
    Test.startTest() ;
    SVMX_PR_ServicStkAvailability_Controller serviceController = new SVMX_PR_ServicStkAvailability_Controller() ;
    serviceController.partReqLineWrap.get(0).sel = true ;
    serviceController.partReqLineWrap.get(0).selectedLoc = serviceController.partReqLineWrap.get(0).availableFrmLoc[0].getValue();
    serviceController.chkbox() ;
    serviceController.toLoc()  ;
    serviceController.change() ;
    serviceController.Back() ;
    serviceController.ChkAvailability() ;
    serviceController.partReqLineWrap.get(0).sel = true ;
    serviceController.partReqLineWrap.get(0).selectedLoc = serviceController.partReqLineWrap.get(0).availableFrmLoc[0].getValue();
    serviceController.StkTransfer() ;
    Test.stopTest() ;

    }

}