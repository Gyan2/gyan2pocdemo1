//Generated by wsdl2apex

public class AsyncDormaEsmEsiDocument10 {
    public class DocumentReadResponseFuture extends System.WebServiceCalloutFuture {
        public dormaEsmEsiDocument10.DocumentReadResponse getValue() {
            dormaEsmEsiDocument10.DocumentReadResponse response = (dormaEsmEsiDocument10.DocumentReadResponse)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
}