@isTest(SeeAllData=true)
public class SVMX_PartsOrderLineServiceManager_UT {

    public static testMethod void checkValidation() {
         List<SVMXC__RMA_Shipment_Line__c> partsLineList = new List<SVMXC__RMA_Shipment_Line__c>();

         list<SVMX_Integration_SAP_Endpoint__c> customSett = SVMX_Integration_SAP_Endpoint__c.getall().values();
        
        if(customSett.size() == 0){
          SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                   mycs.Name = 'WorkOrder';
                   mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
              insert mycs;
              
               SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
                intg.name= 'SAP PO';
                intg.WS_USER__c ='test';
                intg.WS_PASS__c ='test';
          insert intg;
       }
       // SVMX_TestUtility.CreateAccount('Test Vendor','Norway',false) ;
        Account vendor = new Account() ;
        RecordType rt= [select Id from RecordType where DeveloperName='VENDOR_ERP' and SobjectType='Account' ];
        //Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('VENDOR_ERP').getRecordTypeId();
        vendor.RecordTypeId = rt.id  ;
        
        vendor.Name = 'Test vendor' ;
        vendor.BillingCountry = 'Norway' ;
        vendor.SVMX_Automatic_billing__c='Automatic' ;
        vendor.CurrencyIsoCode = 'EUR' ;
        vendor.Status__c = 'Active' ;
        //system.debug('RecordType '+Schema.SObjectType.Account.getRecordTypeInfosByName().get('VENDOR_ERP').getRecordTypeId()) ;
        
        insert vendor;



      
       
        Product2 product = SVMX_TestUtility.CreateProduct('Test Product',true);
        Contact contact = SVMX_TestUtility.CreateContact(vendor.id,true); 
        
        VendorMaterial__c vendorMaterial = new VendorMaterial__c() ;
        vendorMaterial.Account__c = vendor.id ;
        vendorMaterial.ExternalId__c = 'Test Door' ;
        vendorMaterial.Product__c = product.id ;
        insert vendorMaterial ;
        
        SVMXC__Site__c location = SVMX_TestUtility.CreateLocation('Product Location',vendor.id,true) ;
        SVMXC__Service_Order__c  workOrder = SVMX_TestUtility.CreateWorkOrder(vendor.id,location.id,product.id,null,null,null,null,null,null,true);
        workOrder.SVMXC__Scheduled_Date_Time__c = system.today();
        update workOrder ;
         RecordType rt2= [select Id from RecordType where DeveloperName='Shipment' and SobjectType='SVMXC__RMA_Shipment_Order__c' ];
        SVMXC__RMA_Shipment_Order__c partsOrder = new SVMXC__RMA_Shipment_Order__c();
        partsOrder.SVMXC__Company__c = vendor.id ;
        partsOrder.SVMX_Vendor__c = vendor.id ;
        partsOrder.SVMXC__Contact__c = contact.id ;
        partsOrder.SVMXC__Order_Status__c = 'Open' ;
        partsOrder.SVMXC__Expected_Receive_Date__c = Date.today();
        partsOrder.SVMXC__Service_Order__c = workOrder.id ;
        partsorder.recordtypeid= rt2.id;
        insert partsOrder;
        
        SVMXC__RMA_Shipment_Line__c partsRequestLine = new SVMXC__RMA_Shipment_Line__c();
        partsRequestLine.SVMXC__Product__c = product.id ;
        partsRequestLine.SVMXC__RMA_Shipment_Order__c = partsOrder.id ;
        partsRequestLine.SVMXC__Expected_Quantity2__c = 1.0 ;
        partsRequestLine.SVMXC__Line_Status__c = 'Open' ;
        partsRequestLine.SVMXC__Expected_Condition__c = 'Good/Working' ;
        partsRequestLine.SVMXC__Disposition__c = 'Repair' ;
        partsRequestLine.SVMXC__Expected_Receipt_Date__c = Date.today();
        partsRequestLine.SVMX_Item_Number_Blank_Check__c = true ;
        partsRequestLine.SVMX_Awaiting_SAP_Response__c = false ;
        
        partsLineList.add(partsRequestLine);
        
        Set<id>vendorIds = new Set<id>();
        vendorIds.add(vendor.id);
       
        
        test.startTest();
        
       
        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder salesOrderreq=new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt.CreateSync(messageheader,salesOrderreq);
        
        
        
        insert partsLineList ;
              
        
        SVMX_PartsOrderLineServiceManager.checkValidation(vendorIds,partsLineList) ;
        
        test.stopTest() ;
    }
}