/**
 * Created by rebmangu on 09/04/2018.
 */

public with sharing class BuildingController {

    @AuraEnabled
    public static Map<String,Object> updateBuilding(Building__c building){
        Map<String,Object> result = new Map<String,Object>{
                'status'=>200,
                'message'=> null,
                'messageToDisplay'=>null
        };

        try{
            update building;
            result.put('message',building.Id);
        }catch (Exception e){
            result.put('status',400);
            result.put('message',e.getLineNumber()+' - '+e.getMessage());
            result.put('messageToDisplay',e.getMessage());
        }

        return result;

    }

    @AuraEnabled
    public static Map<String,Object> insertBuilding(Building__c building){
        Map<String,Object> result = new Map<String,Object>{
                'status'=>200,
                'message'=> null,
                'messageToDisplay'=>null
        };


        try{
            // In case the building field is empty, we create a new building
            List<Building__c> buildings = [select id,Name from Building__c where Geolocation__Longitude__s = :building.Geolocation__Longitude__s AND Geolocation__Latitude__s =:building.Geolocation__Latitude__s limit 1];
            if(buildings.size() > 0){
                result.put('status',400);
                result.put('message','There is already a building on this location: '+buildings[0].Id);
                result.put('messageToDisplay','There is already a building on this location: '+buildings[0].Name);
            }else{
                insert building;
                result.put('message',building.Id);
            }



        }catch (Exception e){
            result.put('status',400);
            result.put('message',e.getLineNumber()+' - '+e.getMessage());
            result.put('messageToDisplay',e.getMessage());
        }

        return result;

    }
}