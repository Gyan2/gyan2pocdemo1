@IsTest
private class SVMX_wwwDormakabaSalesOrder_UT
{
    static testMethod void coverTypes(){

        new SVMX_wwwDormakabaSalesOrder.Description();
        new SVMX_wwwDormakabaSalesOrder.Email();
        new SVMX_wwwDormakabaSalesOrder.Address();
        new SVMX_wwwDormakabaSalesOrder.Telephone();
        new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        new SVMX_wwwDormakabaSalesOrder.ProductInternalID();
        new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageID();
        new SVMX_wwwDormakabaSalesOrder.ConfigurationPropertyValuation();
        new SVMX_wwwDormakabaSalesOrder.ContactPerson();
        new SVMX_wwwDormakabaSalesOrder.AttachmentID();
        new SVMX_wwwDormakabaSalesOrder.ProductStandardID();
        new SVMX_wwwDormakabaSalesOrder.PropertyID();
        new SVMX_wwwDormakabaSalesOrder.Party();
        new SVMX_wwwDormakabaSalesOrder.OrderRejectionReasonCode();
        new SVMX_wwwDormakabaSalesOrder.MEDIUM_Name();
        new SVMX_wwwDormakabaSalesOrder.SHORT_Description();
        new SVMX_wwwDormakabaSalesOrder.Facsimile();
        new SVMX_wwwDormakabaSalesOrder.BusinessDocumentTextCollectionText();
        new SVMX_wwwDormakabaSalesOrder.Communication();
        new SVMX_wwwDormakabaSalesOrder.BusinessDocumentTextCollection();

        new SVMX_wwwDormakabaSalesOrder.Amount();
        new SVMX_wwwDormakabaSalesOrder.EmailURI();
        new SVMX_wwwDormakabaSalesOrder.BusinessScope();
        new SVMX_wwwDormakabaSalesOrder.BusinessTransactionDocumentID();
        new SVMX_wwwDormakabaSalesOrder.Quantity();
        new SVMX_wwwDormakabaSalesOrder.ContactPersonInternalID();
        new SVMX_wwwDormakabaSalesOrder.Text();
        new SVMX_wwwDormakabaSalesOrder.PhysicalAddress();
        new SVMX_wwwDormakabaSalesOrder.PropertyReference();
        new SVMX_wwwDormakabaSalesOrder.PropertyValuation();

        new SVMX_wwwDormakabaSalesOrder.PartyStandardID();
        new SVMX_wwwDormakabaSalesOrder.BasicMessageHeader();
        new SVMX_wwwDormakabaSalesOrder.LONG_Name();
        new SVMX_wwwDormakabaSalesOrder.Web();
        new SVMX_wwwDormakabaSalesOrder.BusinessDocumentAttachment();
        new SVMX_wwwDormakabaSalesOrder.PhoneNumber();
        new SVMX_wwwDormakabaSalesOrder.PartyInternalID();
        new SVMX_wwwDormakabaSalesOrder.WorkplaceAddress();
        new SVMX_wwwDormakabaSalesOrder.BusinessScopeInstanceID();
        
        
    }
}