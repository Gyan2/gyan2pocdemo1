/**
 * Created by rebmangu on 22/03/2018.
 */

@IsTest
private class TestProducts {
    static testMethod void testSAPPrettyNumber() {

        Product2 product = new Product2(
                Name='Test',
                SalesSpecifications__c='{"blabla":"blabla"}',
                ExternalId__c = 'PS8-00000001',
                SAPNumber__c = '00000001',
                QuantityUnitOfMeasure = 'PCE'
        );
        insert product;
        System.assertEquals('1',[select id,SAPPrettyNumber__c from Product2 limit 1].SAPPrettyNumber__c);


        product.SAPNumber__c = '00000000002';
        update product;
        System.assertEquals('2',[select id,SAPPrettyNumber__c from Product2 limit 1].SAPPrettyNumber__c);
    }
}