@isTest
public class SVMX_LocationTrggrHandler_UT {
    
    
    static testMethod void invokeLoctrigger() {  

        
        Account acct = SVMX_TestUtility.CreateAccount('US account','United States', true);

        Product2 prod = SVMX_TestUtility.CreateProduct('test123',True);
        
       // SVMXC__Site__c loct=SVMX_TestUtility.CreateLocation('test location',acct.id,true);
        
        SVMXC__Site__c loct=new SVMXC__Site__c(Name='test location',SVMXC__Account__c=acct.id);
        
        insert loct;

        SVMXC__Installed_Product__c IP=SVMX_TestUtility.CreateInstalledProduct(acct.id,prod.id,loct.id,true);
  
        Account acct2 = SVMX_TestUtility.CreateAccount('NA Account','United States', true);

        loct.SVMXC__Account__c=acct2.id;
        loct.SVMX_Update_Account__c =true;
        loct.SVMX_Old_Account_Role__c = 'Original Installer';
        
        SVMXC__Site__c loct3=new SVMXC__Site__c(SVMXC__Parent__c=loct.id,SVMXC__Account__c=acct.id);
        insert loct3;
       
        test.startTest();
         update loct; 
        test.stopTest();
        
    }     
    
}