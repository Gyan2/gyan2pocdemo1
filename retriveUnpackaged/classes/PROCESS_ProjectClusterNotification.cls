/**
 * Created by alcocese on 10-Apr-18.
 */

public with sharing class PROCESS_ProjectClusterNotification {

    @InvocableMethod public static void ProjectClusterNotification (List<ProjectClusterNotification> projectClusterNotifications) {
        Map<Id, ProjectClusterNotification> projectClusterMapping = new Map<Id,ProjectClusterNotification>();
        for (ProjectClusterNotification forPCN : projectClusterNotifications) {
            projectClusterMapping.put(forPCN.projectId, forPCN);
        }

        Map<Id, Project__c> projects = new Map<Id, Project__c>([SELECT CountryIsoCode__c, Zip__c FROM Project__c WHERE Id IN :projectClusterMapping.keySet() AND CountryIsoCode__c != null AND Zip__c != null]);

        if (!projects.isEmpty()) {
            Map<String, Set<String>> postalCodesPerCountry = new Map<String, Set<String>>();

            for (Project__c forProject : projects.values()) {
                if (!postalCodesPerCountry.containsKey(forProject.CountryIsoCode__c)) {
                    postalCodesPerCountry.put(forProject.CountryIsoCode__c, new Set<String>());
                }
                postalCodesPerCountry.get(forProject.CountryIsoCode__c).add(forProject.Zip__c);
            }

            List<String> whereClause = new List<String>();
            for (String forCountry : postalCodesPerCountry.keySet()) {
                List<String> whereZipClause = new List<String>();
                for (String forPostalCode : postalCodesPerCountry.get(forCountry)) {
                    whereZipClause.add('(PostalCodeStart__c >= \'' + forPostalCode + '\' AND (PostalCodeEnd__c < \'' + forPostalCode + '\' OR PostalCodeEnd__c = null))');
                }

                whereClause.add('(CountryCode__c = \'' + forCountry + '\' AND (' + String.join(whereZipClause, ' OR ') + '))');
            }

            String query = 'SELECT UserRef__c, Criteria__c, CountryCode__c, PostalCodeStart__c, PostalCodeEnd__c FROM PostalCodeAssignment__c WHERE Type__c = \'Project Notification\' AND ('+String.join(whereClause,' OR ')+')';
            System.debug(query);
            List<PostalCodeAssignment__c> postalCodeAssignments = database.query(query);

            for(Project__c forProject : projects.values()) {
                for (PostalCodeAssignment__c forPostalCodeAssignment : postalCodeAssignments) {
                    if (forProject.CountryIsoCode__c == forPostalCodeAssignment.CountryCode__c && forProject.Zip__c >= forPostalCodeAssignment.PostalCodeStart__c && (forProject.Zip__c < forPostalCodeAssignment.PostalCodeEnd__c || forPostalCodeAssignment.PostalCodeEnd__c == null)) {
                        // check product cluster TODO: merge with previous "if"
                        ProjectClusterNotification PCN = projectClusterMapping.get(forProject.Id);
                        if (PCN.notifyMKS == true && forPostalCodeAssignment.Criteria__c == 'MKS') {
                            postChatter(forProject.Id, forPostalCodeAssignment.UserRef__c, 'MKS');
                        }
                        if (PCN.notifyDHW == true && forPostalCodeAssignment.Criteria__c == 'DHW') {
                            postChatter(forProject.Id, forPostalCodeAssignment.UserRef__c, 'DHW');
                        }
                        if (PCN.notifyIGS == true && forPostalCodeAssignment.Criteria__c == 'IGS') {
                            postChatter(forProject.Id, forPostalCodeAssignment.UserRef__c, 'IGS');
                        }
                        if (PCN.notifyENS == true && forPostalCodeAssignment.Criteria__c == 'ENS') {
                            postChatter(forProject.Id, forPostalCodeAssignment.UserRef__c, 'ENS');
                        }

                        if (PCN.notifyLGS == true && forPostalCodeAssignment.Criteria__c == 'LGS') {
                            postChatter(forProject.Id, forPostalCodeAssignment.UserRef__c, 'LGS');
                        }
                        if (PCN.notifySAL == true && forPostalCodeAssignment.Criteria__c == 'SAL') {
                            postChatter(forProject.Id, forPostalCodeAssignment.UserRef__c, 'SAL');
                        }
                        if (PCN.notifyEAD == true && forPostalCodeAssignment.Criteria__c == 'EAD') {
                            postChatter(forProject.Id, forPostalCodeAssignment.UserRef__c, 'EAD');
                        }
                        if (PCN.notifySVC == true && forPostalCodeAssignment.Criteria__c == 'SVC') {
                            postChatter(forProject.Id, forPostalCodeAssignment.UserRef__c, 'SVC');
                        }

                    }
                }
            }
        }
    }

    private static void postChatter(Id projectId, Id userId, String productCluster) {

        ConnectApi.FeedItemInput feedItemInput = new ConnectApi.FeedItemInput();
        ConnectApi.MentionSegmentInput mentionSegmentInput = new ConnectApi.MentionSegmentInput();
        ConnectApi.MessageBodyInput messageBodyInput = new ConnectApi.MessageBodyInput();
        ConnectApi.TextSegmentInput textSegmentInput = new ConnectApi.TextSegmentInput();

        messageBodyInput.messageSegments = new List<ConnectApi.MessageSegmentInput>();

        mentionSegmentInput.id = userId;
        messageBodyInput.messageSegments.add(mentionSegmentInput);

        textSegmentInput.text = '\nThis Project might be interesting for your Product Cluster ('+productCluster+'), \nCould you take a look?';
        messageBodyInput.messageSegments.add(textSegmentInput);

        feedItemInput.body = messageBodyInput;
        feedItemInput.feedElementType = ConnectApi.FeedElementType.FeedItem;
        feedItemInput.subjectId = projectId;

        ConnectApi.FeedElement feedElement = ConnectApi.ChatterFeeds.postFeedElement(Network.getNetworkId(), feedItemInput);
    }

    public class ProjectClusterNotification {
        @InvocableVariable(required=true label='Project Id') public Id projectId;
        @InvocableVariable(label='Notify MKS User?') public Boolean notifyMKS;
        @InvocableVariable(label='Notify DHW User?') public Boolean notifyDHW;
        @InvocableVariable(label='Notify IGS User?') public Boolean notifyIGS;
        @InvocableVariable(label='Notify ENS User?') public Boolean notifyENS;

        @InvocableVariable(label='Notify LGS User?') public Boolean notifyLGS;
        @InvocableVariable(label='Notify SAL User?') public Boolean notifySAL;
        @InvocableVariable(label='Notify EAD User?') public Boolean notifyEAD;
        @InvocableVariable(label='Notify SVC User?') public Boolean notifySVC;

    }
}