/*******************************************************************************************************
Description:
  This test class is used to test the SVMX_TimesheetBatch class.
 
Author: Nivedita S
Date: 22-12-2017

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

@isTest

public class SVMX_TimesheetBatch_UT

{

 static testMethod void TimesheetBatch()

   {

   Profile profileId = [SELECT Id FROM Profile WHERE Name = 'SVMX - Global Technician' LIMIT 1];
      User usr = new User();
          
     usr.LastName = 'LIVESTON';

    usr.FirstName='JASON';

    usr.Alias = 'jliv';

    usr.Email = 'jason.liveston@asdf.com';

   usr.Username = 'test12300000@gmail.com';

   usr.ProfileId = profileId.id;

   usr.TimeZoneSidKey = 'GMT';

   usr.LanguageLocaleKey = 'en_US';

   usr.EmailEncodingKey = 'UTF-8';

   usr.LocaleSidKey = 'en_US';
    usr.UserCountry__c ='US';
       usr.SalesOrg__c ='9999';
    insert usr;

  SVMX_Timesheet_Configuration__c tc= new SVMX_Timesheet_Configuration__c();

             tc.Name='US';
             tc.SVMX_Frequency__c='Daily';
             tc.SVMX_NonProductive_Events__c='Training';
             tc.SVMX_Profiles__c ='SVMX - Global Technician';
              insert tc;
       SVMX_Timesheet_Configuration__c tcc= new SVMX_Timesheet_Configuration__c();
            tcc.Name='US';
             tcc.SVMX_Frequency__c='Weekly';
             tcc.SVMX_Profiles__c='SVMX - Global Technician';
             insert tcc;
        SVMX_Timesheet_Configuration__c tccc= new SVMX_Timesheet_Configuration__c();
            tccc.Name='US';
             tccc.SVMX_Frequency__c='Monthly';
             tccc.SVMX_Profiles__c='SVMX - Global Technician';
            insert tccc;
        Test.startTest();

         SVMX_TimesheetBatch obj = new SVMX_TimesheetBatch();
             DataBase.executeBatch(obj);

           Test.stopTest();

  }


}