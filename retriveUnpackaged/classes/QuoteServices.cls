/**
 * Created by rebmangu on 07/03/2018.
 */

public with sharing class QuoteServices {

    public static void handleStandardQuotes(List<Quote> Records){
        Id standardPricebookId;
        if(Test.isRunningTest()){
            standardPricebookId = Test.getStandardPricebookId();
        }else{
            standardPricebookId = [select id from Pricebook2 where IsStandard = true limit 1].Id;
        }

        for(Quote quote : records){
            if(quote.Pricebook2Id == null)
                quote.Pricebook2Id = standardPricebookId;        }

    }

    // after for the insert, before for the update
    public static void handleAccountLanguageAfterInsert(List<Quote> Records){
        Set<Id> accountIds = new Set<Id>();
        Map<Id,Quote> quotes = new Map<Id,Quote>([select id,AccountId,Languages__c from Quote where Id in: records]);

        for(Quote quote : records){
            String accountId = quotes.get(quote.Id).AccountId;
            accountIds.add(accountId);
        }

        Map<Id,Account> accounts = new Map<Id,Account>([select id,Language__c from Account where Id in: accountIds]);
        List<Quote> quoteToUpdate = new List<Quote>();

        for(Quote quote : records){
            String accountId = quotes.get(quote.Id).AccountId;
            if(accounts.containsKey(accountId)){
                quoteToUpdate.add(new Quote(Id=quote.Id,Languages__c=accounts.get(accountId).Language__c));
            }
        }

        update quoteToUpdate;
    }

    public static void deleteText(List<Quote> Records){
        delete [select id from Text__c where QuoteRef__c in :Records];
    }
}