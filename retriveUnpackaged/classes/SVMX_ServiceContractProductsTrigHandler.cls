/*******************************************************************************************************
Description: 

Dependancy: 
    Trigger Framework: 
    Trigger: 
    Service Manager: 
    Test class: 
    
 
Author: Tulsi B R
Date: 19-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/
public class SVMX_ServiceContractProductsTrigHandler extends SVMX_TriggerHandler {
   private list<SVMXC__Service_Contract_Products__c> newWOList;
   private list<SVMXC__Service_Contract_Products__c> oldWOList;
   private map<Id, SVMXC__Service_Contract_Products__c> newWOMap;
   private map<Id, SVMXC__Service_Contract_Products__c> oldWOMap;
   public SVMX_ServiceContractProductsTrigHandler(){
        this.newWOList = (list<SVMXC__Service_Contract_Products__c>) Trigger.new;
        this.oldWOList = (list<SVMXC__Service_Contract_Products__c>) Trigger.old;
        this.newWOMap = (Map<Id, SVMXC__Service_Contract_Products__c>) Trigger.newMap;
        this.oldWOMap = (Map<Id, SVMXC__Service_Contract_Products__c>) Trigger.oldMap;
   
   }
   public override void beforeinsert(){
        
        map<string,SVMX_Sales_Office__c> countrySalesOffMap = new map<string,SVMX_Sales_Office__c>();
          list<String> countryList = new List<String>();
          list<SVMX_Sales_Office__c> salesOfficeList = new List<SVMX_Sales_Office__c>();

       for(SVMXC__Service_Contract_Products__c serviceContrPro:newWOList){
        if(serviceContrPro.SVMX_Billing_schedule__c == System.Label.Yearly_on_Last_of_Start_month || serviceContrPro.SVMX_Billing_schedule__c == System.Label.Quarterly_Every_3_months ){
          serviceContrPro.SVMX_Billing_Start_Date__c=serviceContrPro.SVMXC__Start_Date__c;
          serviceContrPro.SVMX_Billing_End_Date__c=serviceContrPro.SVMXC__End_Date__c;
        }else if(serviceContrPro.SVMX_Billing_schedule__c==System.Label.Monthly_on_First_of_Month){
           if(serviceContrPro.SVMXC__Start_Date__c.day() >= 15){
           serviceContrPro.SVMX_Billing_Start_Date__c=serviceContrPro.SVMXC__Start_Date__c.addMonths(1).toStartOfMonth(); //addDays(-1);
           serviceContrPro.SVMX_Billing_End_Date__c=serviceContrPro.SVMXC__End_Date__c;
           }
           if(serviceContrPro.SVMXC__Start_Date__c.day() < 15){
           serviceContrPro.SVMX_Billing_Start_Date__c=serviceContrPro.SVMXC__Start_Date__c.addMonths(0).toStartOfMonth();
           serviceContrPro.SVMX_Billing_End_Date__c=serviceContrPro.SVMXC__End_Date__c;
           }
        }
       
       }

       for(SVMXC__Service_Contract_Products__c scProduct: newWOList){
            if(scProduct.SVMX_Maintenance_product__c == null){
                countryList.add(scProduct.SVMX_Country__c);
            }
       }
      if(countryList.size() > 0){
         salesOfficeList=SVMX_SalesOfficeDataManager.countrySalesOfficeQuery(countryList);
       }

      for(SVMX_Sales_Office__c sof: salesOfficeList){
            if(!countrySalesOffMap.containskey(sof.SVMX_Country__c)){
                 countrySalesOffMap.put(sof.SVMX_Country__c,sof);
            }
      }
      
      if(countrySalesOffMap.size() > 0){
      for(SVMXC__Service_Contract_Products__c scProd:newWOList){
            if(scProd.SVMX_Maintenance_product__c == null && countrySalesOffMap.size() > 0 && countrySalesOffMap.containsKey(scProd.SVMX_Country__c)){
                scProd.SVMX_Maintenance_product__c =countrySalesOffMap.get(scProd.SVMX_Country__c).SVMX_Maintenance_Product__c;
                scProd.SVMX_SAP_Material_Number__c = countrySalesOffMap.get(scProd.SVMX_Country__c).SVMX_Maintenance_Product__r.SAPNumber__c;
            }
      }
      }
   
   }
   
   public override void afterInsert(){
        
        list<SVMXC__Service_Contract_Products__c > scProdIntList = new  list<SVMXC__Service_Contract_Products__c > ();

        for(SVMXC__Service_Contract_Products__c scprod : newWOList){
                if(scprod.SVMX_Pricing_Method__c == System.Label.Time_Based && scprod.SVMXC__Is_Billable__c == true){

                    scProdIntList.add(scprod);
                } 

        }

        if(scProdIntList.size() > 0){
          
           SVMX_SAPServiceContractUtility.handleSAPServiceContractUpdateofProduct(scProdIntList);
         } 
        
        
    }

    public override void afterupdate(){
        
        map<Id,SVMXC__Service_Contract_Products__c > scProdIntMap = new  map<Id,SVMXC__Service_Contract_Products__c> ();

        for(SVMXC__Service_Contract_Products__c scprod : newWOMap.values()){
                if(scprod.SVMX_Pricing_Method__c == System.Label.Time_Based && scprod.SVMXC__Is_Billable__c == true){

                    scProdIntMap.put(scprod.Id,scprod);
                }
               
            

        }

        if(scProdIntMap.keyset().size() > 0){
          
           SVMX_SAPServiceContractUtility.handleSAPExistingServiceContractProductUpdate(scProdIntMap,oldWOMap);
         } 
        
        system.debug('$$$$$$$$$');
    }
    
    public override void beforeDelete(){
        for(SVMXC__Service_Contract_Products__c scprod : oldWOList){
           if(scprod.SVMX_SAP_Contract_Item_Number__c != null)
               scprod.addError('Cannot delete the line as Contract Item number is already generated');
        }
    }

}