/*******************************************************************************************************
* Class Name     	: SM_TriggerUser
* Description		: Trigger Handler for the noted SObject. This class implements the ITrigger
* 						interface to help ensure the trigger code is bulkified and all in one place.
*						Adaptation of the Tony Scott Trigger Factory Pattern. Refer to ITrigger.cls).
* Author          	: Paul Carmuciano
* Created On      	: 2017-07-10
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-07-10         1000                 Initial version
******************************************************************************************************/
public without sharing class SM_TriggerUser implements ITrigger
{
    // Member variables
    //  to hold the Orgs cfg from Utility classes
    private Map<String,Management_Segmentation__mdt> ManagementSegmentationMapping = new Map<String,Management_Segmentation__mdt>();
    private Schema.DescribeFieldResult mgmtSegSourceCountry = Schema.SObjectType.User.Fields.UserCountry__c;
    private Schema.DescribeFieldResult mgmtSegTargetRegionField = Schema.SObjectType.User.Fields.UserRegion__c;
    private Schema.DescribeFieldResult mgmtSegTargetSegmentField = Schema.SObjectType.User.Fields.UserSegment__c;


    //  to hold the Id's of Sobjects 'in use'
    private Set<Id> m_inUseIds = new Set<Id>();

    
    
    // Constructor
    public SM_TriggerUser()
    {
    }

    /**
* Bulk Operations
* @description Instantiate collections here and pass them in to Iterative trigger methods to preserve performance 
* by avoiding iterations within iterations & deep dependenies 
*/    
    public void bulkBefore()
    {
        // get Util
        ManagementSegmentationMapping = MDM001_ManagementSegmentation.ManagementSegmentationMapping;
    }

    public void bulkAfter()
    {
    }
    
    /**
* Iterative Operations
* @description When authoring methods consider  performance impacts, ie;
*	- Paramatise contextual data from variables intantiated during bulkBefore 
*	- Avoid relying on a dependant method to do further processing 
*	- Consider moving the heavy lifting to @future methods as to not impede syncronous transations
*/    
    public void beforeInsert(SObject so)
    {
        // Apply the Management Segmentation
        Management_Segmentation__mdt msmdt = ManagementSegmentationMapping.get( (String)so.get(mgmtSegSourceCountry.name));
        SM002_MgmtSegmentation.applyManagementSegmentation(so, msmdt, mgmtSegTargetRegionField, mgmtSegTargetSegmentField);
        
    }
    
    public void beforeUpdate(SObject oldSo, SObject so)
    {
        // Apply the Management Segmentation
        Management_Segmentation__mdt msmdt = ManagementSegmentationMapping.get( (String)so.get(mgmtSegSourceCountry.name));
        SM002_MgmtSegmentation.applyManagementSegmentation(oldSo, so, msmdt, mgmtSegSourceCountry, mgmtSegTargetRegionField, mgmtSegTargetSegmentField);
    }
    
    /*
* Would not be used as User object is not Deleteable / Undeleteable.
* Though as we implementing an Interface, then all methods must be declared.
*/
    public void beforeDelete(SObject so)
    {
        // Will not fire on User Object. DO NOT USE
    }
    
    public void afterInsert(SObject so)
    {
    }
    
    public void afterUpdate(SObject oldSo, SObject so)
    {
    }
    
    /*
* Would not be used as User object is not Deleteable / Undeleteable.
* Though as we implementing an Interface, then all methods must be declared.
*/
    public void afterDelete(SObject so)
    {
        // Will not fire on User Object. DO NOT USE
    }

    public void afterUndelete(SObject so)
    {
        // Will not fire on User Object. DO NOT USE
    }

    /**
* andFinally  Operations
* @description This method is called once all records have been processed by the trigger. Use this method to accomplish 
* any final operations such as creation or updates of other records including @future Methods & Callouts
*/
    public void andFinally()
    {
    }
    
}