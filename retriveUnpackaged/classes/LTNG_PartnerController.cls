public with sharing class LTNG_PartnerController {

    @AuraEnabled
    public static PartnersWrapper auraGetPartners(List<String> fields, Id recordId, Integer queryLimit) {
        PartnersWrapper returned = new PartnersWrapper();
        Boolean isOpportunity = recordId.getSobjectType() == Opportunity.getSObjectType();
        List<String> allFields = new List<String> {'Id'};

        if (fields != null) allFields.addAll(fields);
        //if (isOpportunity) allFields.add('IsPrimary');
        //allFields.add('');
        queryLimit = Integer.valueOf(queryLimit); // Absurd? yes, but it crashes otherwise...
        returned.partners = database.query('SELECT ' + String.join(allFields, ',') + ', toLabel(Role)' + (isOpportunity?', IsPrimary':'') + ' FROM Partner WHERE AccountFromId = :recordId OR OpportunityId = :recordId ORDER BY LastModifiedDate DESC ' + (queryLimit>0?'LIMIT :queryLimit':''));
        returned.fields = allFields;
        returned.labels = new List<String>();
        returned.types = new List<String>();

        for (String forField : allFields) {
            returned.labels.add(Util.Describe.getField('Partner', forField).getLabel());
            returned.types.add(String.valueOf(Util.Describe.getField('Partner', forField).getType()));
        }

        returned.roles = new List<Role>();

        for (Schema.PicklistEntry forPickVal : Partner.Role.getDescribe().getPicklistValues()) {
            if (forPickVal.isActive()) {
                returned.roles.add(new Role(forPickVal.getLabel(), forPickVal.getValue()));
            }
        }

        returned.labels.add(Util.Describe.getField('Partner','Role').getLabel());
        returned.types.add(String.valueOf(Util.Describe.getField('Partner','Role').getType()));

        returned.fields.add('Role');
        if (isOpportunity) {
            returned.labels.add(Util.Describe.getField('Partner', 'IsPrimary').getLabel());
            returned.types.add(String.valueOf(Util.Describe.getField('Partner', 'IsPrimary').getType()));
            returned.fields.add('IsPrimary');
        }

        return returned;
    }

    @AuraEnabled
    public static void auraSavePartners(Id accountId, List<String> partners) {
        List<Partner> partnersToInsert = new List<Partner>();
        for (String forPartner : partners) {
            List<String> forPartnerSplit = forPartner.split('::',2);
            partnersToInsert.add(new Partner(
                AccountFromId = accountId,
                AccountToId = forPartnerSplit[0],
                Role = forPartnerSplit[1]
            ));
        }
        System.debug(partnersToInsert);
        insert partnersToInsert;
    }

    @AuraEnabled
    public static void auraDeletePartner(Id partnerId) {
        delete new Partner(Id = partnerId);
    }


    public class PartnersWrapper {
        @AuraEnabled public List<String> labels;
        @AuraEnabled public List<String> fields;
        @AuraEnabled public List<String> types;
        @AuraEnabled public List<Partner> partners;
        @AuraEnabled public List<Role> roles;
    }


    public class Role {
        @AuraEnabled public String label;
        @AuraEnabled public String value;

        public Role (String paramLabel, String paramValue) {
            label = paramLabel;
            value = paramValue;
        }
    }
}