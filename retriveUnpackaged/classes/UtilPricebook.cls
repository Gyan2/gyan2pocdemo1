/**
 * Created by guillaumerebmann on 30/04/2018.
 */

public with sharing class UtilPricebook {

    public class UtilPricebookException extends Exception {}

    private Map<String,PricebookEntry> pricebookMapping;
    private UtilPricebook.Error error = new UtilPricebook.Error();
    private List<String> fieldForUpdate = new List<String>();

    public UtilPricebook(){
        // Get modifiable fields
        Map<String, Schema.SObjectField> mappingFieldsByStrings = Schema.SObjectType.PricebookEntry.fields.getMap();
        for(String fieldName : Schema.SObjectType.PricebookEntry.fields.getMap().keySet()) {
            if(mappingFieldsByStrings.get(fieldName).getDescribe().isUpdateable()) {
                fieldForUpdate.add(fieldName);
            }
        }
    }

    public UtilPricebook add(Map<String,PricebookEntry> pricebookMapping){
        this.pricebookMapping = pricebookMapping;
        return this;
    }


    public UtilPricebook upsertPricebooks(){
        Map<String,Map<String,PricebookEntry>> pricebookEntries;

        // Separate data in Standard Insert, Custom Insert and Update
        pricebookEntries = this.separateInsertAndUpdate();

        // Remove the fields that can't be updated;
        this.removeSpecificFieldsForUpdate(pricebookEntries);


        // Validate before upserting
        if(!error.validate()){
            throw new UtilPricebookException(error.toString());
        }
        try{
            System.debug(pricebookEntries);
            insert pricebookEntries.get('insertStandard').values();
            insert pricebookEntries.get('insert').values();
            update pricebookEntries.get('update').values();
        }catch(Exception e){
            error.addError('[UPSERT] Line '+e.getLineNumber()+' - '+e.getMessage());
        }

        // Validate
        if(!error.validate()){
            throw new UtilPricebookException(error.toString());
        }

        return this;

    }

    private void removeSpecificFieldsForUpdate(Map<String,Map<String,PricebookEntry>> pricebookEntries){
        for(PricebookEntry pbe : pricebookEntries.get('update').values()){
            Map<String,Object> mapping = pbe.getPopulatedFieldsAsMap();
            PricebookEntry newPbe = new PricebookEntry();
            for(String key : mapping.keySet()){
                // to LowerCase
                key = key.toLowerCase();
                if(fieldForUpdate.contains(key)){
                    newPbe.put(key,mapping.get(key));
                }
            }

            pbe =  newPbe;
        }

    }


    private Map<String,Map<String,PricebookEntry>> separateInsertAndUpdate(){
        Map<String,Map<String,PricebookEntry>> pricebookEntries = new Map<String,Map<String,PricebookEntry>>{
                'insert'  => new Map<String,PricebookEntry>(),
                'insertStandard'  => new Map<String,PricebookEntry>(), // Standard need to be inserted first
                'update'  => new Map<String,PricebookEntry>()
        };

        for(PricebookEntry line : [select id,Product2.ExternalId__c,ExternalId__c,Pricebook2.IsStandard from PricebookEntry where ExternalId__c IN : pricebookMapping.keySet()]){
            // Already exist, so we move it to update and we add the ID to the PriceBookEntry for the update
            PricebookEntry pbe = pricebookMapping.get(line.ExternalId__c);
                           pbe.Id = line.id;
            pricebookEntries.get('update').put(line.ExternalId__c,pbe);
            pricebookMapping.remove(line.ExternalId__c);
        }

        for(String key : pricebookMapping.keySet()){
            if(key.contains('standard-')){
                // It's a standard, so it has to be inserted first
                pricebookEntries.get('insertStandard').put(key,pricebookMapping.get(key));
                pricebookMapping.remove(key);
            }else{
                pricebookEntries.get('insert').put(key,pricebookMapping.get(key));
                pricebookMapping.remove(key);
            }
        }

        if(pricebookMapping.size() > 0){
            error.addError('[separateInsertAndUpdate] The pricebookEntries : '+String.join(new List<String>(pricebookMapping.keySet()),',')+ 'are not handled !!!');
        }


        return pricebookEntries;

    }





    /*
            ERROR CLASS
     */

    public class Error{
        public List<String> messages = new List<String>();

        public Error(){

        }

        public void addError(String message){
            messages.add(message);
        }

        public Boolean validate(){
            return messages.size() == 0;
        }


        public override String toString(){
            return String.join(messages,' \n ');
        }

    }


}