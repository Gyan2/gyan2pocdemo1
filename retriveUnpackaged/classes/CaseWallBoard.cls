//poc testing
//We are changing this for GIT demo
//This is second line change
//This is for demo on 16th
//The second line of testing
public class CaseWallBoard {
    
     @AuraEnabled
     public static List<Case> getCase() {
          return [SELECT Id,Next_SLA_Milestone_Due_Date__c,Real_Owner__c,Contract_Type__c,Client_Location__c,Type,New_Service_Area__c,Service_Area__c,CaseSubject__c,SVMXC__Service_Contract__r.SVMX_Contract_Types__c,SVMXC__Site__r.Name,No_Email__c,Subject,CaseNumber,Contact.Name,Account.Name,Priority,Status,Owner.Name,CreatedDate,Time__c from Case where SVMX_Internal_Country__c = 'France' and IsClosed = False and Status != 'Escalated' and Type = 'Reactive' order by Next_SLA_Milestone_Due_Date__c ASC LIMIT 25];
     }
   
    
    @AuraEnabled
	public static string getUserSession() 
    {
    	return userInfo.getSessionId();
	}
    
    //Dynamic picklist value-Type and Service Area    
    @AuraEnabled   
    public static List < String > getselectOptions1(sObject objObject, string fld) {
        system.debug('objObject --->' + objObject);
        system.debug('fld --->' + fld);
        List < String > allProd = new list < String > ();
        // Get the object type of the SObject.
        Schema.sObjectType objType = objObject.getSObjectType();
        
        // Describe the SObject using its object type.
        Schema.DescribeSObjectResult objDescribe = objType.getDescribe();
        
        // Get a map of fields for the SObject
        map < String, Schema.SObjectField > fieldMap = objDescribe.fields.getMap();
        
        // Get the list of picklist values for this field.
        list < Schema.PicklistEntry > values =
            fieldMap.get(fld).getDescribe().getPickListValues();
        
        // Add these values to the selectoption list.
        for (Schema.PicklistEntry a: values) {
            allProd.add(a.getValue());
        }
        system.debug('allProd---->' + allProd);
        allProd.sort();
        return allProd;
    }
    
    
   //Lightning Filter functionality
    @AuraEnabled   
    public static list<Case> filterCaseList(List<string> SearchType, string ServiceArea){
       String serviceType = '';
        for(String serType : SearchType) {
            serviceType = serviceType + ' Type = \'' + serType + '\' or';
        }
        //System.debug('SearchType ' + string.isNotEmpty(SearchType) + 'qqq' + ServiceArea);
     	List<Case> returnList = New List<Case>();
        String querry = 'SELECT Id,Next_SLA_Milestone_Due_Date__c,Real_Owner__c,Contract_Type__c,Client_Location__c,Type,New_Service_Area__c,Service_Area__c,CaseSubject__c,SVMXC__Service_Contract__r.SVMX_Contract_Types__c,SVMXC__Site__r.Name,No_Email__c,Subject,CaseNumber,Contact.Name,Account.Name,Priority,Status,Owner.Name,CreatedDate,Time__c from Case where ';
        if(SearchType.size() != 0){
            querry = querry +'('+ serviceType.substring(0,serviceType.length()-2) + ') and ';
        }
        /*else {
            Type = 'Reactive'
        }*/
        if(ServiceArea != ''){
            querry = querry + ' Service_Area__c =:ServiceArea and ';
        }
        querry = querry +  ' SVMX_Internal_Country__c = \'France\' and IsClosed = False and Status != \'Escalated\' order by Next_SLA_Milestone_Due_Date__c ASC ';
        System.debug('Check Query: ' + querry);
        returnList = Database.query(querry);
        System.debug('Return value: ' + returnList);
         return returnList ;
    }
    
   
}
