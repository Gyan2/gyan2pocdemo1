/*******************************************************************************************************
Description: Batch class for creating monthly timesheets

Dependancy:    
	Class: SVMX_TimesheetBatch_Controller
 
Author: Pooja Singh
Date: 14-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

global class SVMX_Monthly_TimesheetBatch implements Database.Batchable<sObject> {
	public String query ='';
	public Set<String> monthlyCtrySet = new Set<String>();
    public Set<String> profSet = new Set<String>();
    global SVMX_Monthly_TimesheetBatch(Set<String> passedIdSet, List<String> profileList){
        monthlyCtrySet = passedIdSet;
        for(Profile p :[Select Id, Name from Profile where Name IN: profileList]){
            profSet.add(p.Id);
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
    	query = 'Select Id, UserCountry__c from User where UserCountry__c IN :monthlyCtrySet and isActive = true and profileId IN:profSet';
        system.debug('Query '+query);
            return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<User> userList) {
        List<Id> UserIdList = new List<Id>();	//New user id list
        Map<Id,Id> existTsUserMap = new Map<Id,Id>(); //existing user id and timesheet id map
		Set<Date> startDatesSet = new Set<Date>();	//Set of start dates
   		Map<Date,Date> stDtEdDtMap = new Map<Date,Date>();	//Map of start date and corresponding end date
        Date startDate = System.Date.today().toStartOfMonth().addMonths(1);	//Start date of the month
        Date endDate = startDate.addMonths(1).addDays(-1);	//end date of the month
        startDatesSet.add(startDate);
        stDtEdDtMap.put(startDate,endDate);
        //Query if a timesheet existes for the given month range
        List<SVMXC__Timesheet__c> timeSheetQuery = new List<SVMXC__Timesheet__c>([Select Id, Name, SVMX_Current_Week__c, SVMXC__Start_Date__c, SVMXC__End_Date__c,SVMXC__Period__c, SVMXC__User__c from SVMXC__Timesheet__c where SVMXC__User__c IN:userList and SVMXC__Start_Date__c IN :startDatesSet and SVMXC__End_Date__c IN: stDtEdDtMap.values()]);
        for(SVMXC__Timesheet__c ts : timeSheetQuery){
            ts.SVMX_Current_Week__c = true;
            existTsUserMap.put(ts.SVMXC__User__c,ts.id);
            system.debug('existing timesheets for the user for that Month'+existTsUserMap);
        }
        
        if(timeSheetQuery.size() > 0)
        	update timeSheetQuery;
        
        for(User u : userList){
            if(!existTsUserMap.containsKey(u.id)){
                UserIdList.add(u.id);
            	system.debug('List of users for new timesheets for that Month'+UserIdList);
            }
        }
        if(UserIdList.size() > 0)
        	SVMX_TimesheetBatch_Controller.createTimesheet(startDatesSet, stDtEdDtMap, 'Monthly', UserIdList);
        
        /* Date collection for previous month timesheet */
        Set<Date> prevStDtSet = new Set<Date>();
        Map<Date,Date> prevStDtEdDtMap = new Map<Date,Date>();
        Date prevMoStartDate = System.Date.today().toStartOfMonth();	//Start date of the current month
        Date prevMoEndDate = prevMoStartDate.addMonths(1).addDays(-1);	//end date of the current month
        prevStDtSet.add(prevMoStartDate);
        prevStDtEdDtMap.put(prevMoStartDate,prevMoEndDate);
        system.debug('Set of previous start dates'+prevStDtSet);
        system.debug('Map of previous End dates'+prevStDtEdDtMap);
        
        SVMX_TimesheetBatch_Controller.prevWeekTimeSheet(prevStDtSet,prevStDtEdDtMap);
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}