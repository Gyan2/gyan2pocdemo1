public class SVMX_SalesOfficeDataManager {
    
      
    public static list<SVMX_Sales_Office__c> countrySalesOfficeQuery(List<string> countryList){
        list<SVMX_Sales_Office__c> salesOfficeLst = new list<SVMX_Sales_Office__c> ([select id,name,SVMX_Country__c,SVMX_Expense_Product__c,
                                                  SVMX_Labor_Product__c,SVMX_Travel_Labor_Product__c,SVMX_Expense_Product__r.SAPNumber__c, SVMX_Travel_Distance_Product__c, SVMX_Travel_Distance_Product__r.SAPNumber__c,
                                                  SVMX_Labor_Product__r.SAPNumber__c,SVMX_Travel_Labor_Product__r.SAPNumber__c,SVMX_Maintenance_Product__c ,SVMX_Maintenance_Product__r.SAPNumber__c 
                                                  from SVMX_Sales_Office__c where SVMX_Country__c in : countryList]);
        return salesOfficeLst;
    }
            
}