public class InstalledObjectApex {
    
  @AuraEnabled
    public static list<sobject> accountLookup(String objectName)
    {
        list<sobject> accList=new list<sobject>();
        if(objectName == 'Account')
        {
            accList=[SELECT name, id FROM Account];
            return accList;
        }
        
        else if(objectName == 'Location')
        {
            accList=[SELECT name, id FROM Location__c];
            return accList;
        }
        
        else
        {
            accList=[SELECT name, id FROM product2];
            return accList;

        }
        
    }
    
     @AuraEnabled
    public static list <sobject>search_Results(String accountValue,String searchLocation, String searchProduct, String searchcriteria)
    {
        list<sobject>searchList=new list<sobject>();
        system.debug(accountValue);
        system.debug(searchLocation);
        system.debug(searchProduct);
        system.debug(searchcriteria);
        String query = 'select Id,SVMX_PS_Product_Name__c,SVMXC__Company__r.Name,SVMXC__Site__r.Name,SVMXC__Serial_Lot_Number__c,SVMXC__Status__c  FROM SVMXC__Installed_Product__c where ';
        
        if(accountValue!= null && accountValue !='' )
        {
			query=query+' SVMXC__Company__r.Name =:accountValue';
        }
         
        if(searchLocation !=null && searchLocation !='')
        {
            if((accountValue !=null && accountValue !=''))
            {
               query=query+' And SVMXC__Site__r.Name =: searchLocation'; 
                system.debug(query);
            }
            else
            {
                query=query+' SVMXC__Site__r.Name =: searchLocation';
                 system.debug(query);
            }
            
        }
        if(searchProduct !=null && searchProduct !='')
        {
            if((searchLocation !=null && searchLocation !='')|| (accountValue !=null && accountValue !=''))
            {
                query=query+' And SVMXC__Product__r.Name =: searchProduct';
                 system.debug(query);
            }
            else
            {
                query=query+' SVMXC__Product__r.Name =: searchProduct';
                system.debug(query);
            }
            
        }
        if(searchcriteria !=null && searchcriteria !='')
        {
            //criteria=searchcriteria.toLowerCase();
           if(accountValue!= null && accountValue !='' || searchLocation !=null && searchLocation !='' || searchProduct!= null && searchProduct !='')
          {
              			//String var='%'+searchcriteria+'%';
                        query=query+ ' And SVMX_PS_Product_Name__c like \'%' + searchcriteria + '%\'';
                        system.debug(query);
              
          }
          else
          {
              //String var='%'+searchcriteria+'%';
               query=query+ ' SVMX_PS_Product_Name__c like \'%' + searchcriteria + '%\'';
              system.debug(query);
              
          }
        }
        
        searchList = Database.query(query);
        system.debug(searchList);
        return searchList;
        
        
    }
}