/**
 * Created by rebmangu on 22/03/2018.
 */

public with sharing class ProductServices {


    public static void copySAPPrettyNumber(List<Product2> records){
        for(Product2 product : records){

            if(!String.isBlank(product.SAPNumber__c)){
                product.SAPPrettyNumber__c = product.SAPNumber__c.replaceAll('^0*','');
            }


        }
    }

    public static void mapProductHierarchyForServiceMax(List<Product2> records){
        for(Product2 product : records){

            if(!String.isBlank(product.ProductHierarchy__c)){
                product.Family = product.ProductHierarchy__c.left(2);
                product.SVMXC__Product_Line__c = product.ProductHierarchy__c.left(4);
            }


        }
    }


}