/************************************************************************************************************
Description: Data Manager for Service Quote. All the SOQL queries and DML Operations are performed in this class.

Dependency: 
 
Author: Ranjitha S
Date: 16-11-2017

--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_ServiceQuoteDataManager {

    public static List<SVMXC__Quote__c> servQuoteList(String quoteNumber){
        List<SVMXC__Quote__c> scList = [SELECT id,Name,SVMXC__Contact__r.Email, SVMXC__Quote_Amount2__c, SVMXC__Status__c, SVMXC__Contact__r.Name, SVMXC__Contact__r.SVMX_Roles__c,
                                        SVMXC__Company__c, SVMXC__Contact__c, SVMX_Opportunity__c, 
                                (Select Id,Name From Attachments ORDER BY CreatedDate DESC) 
                                FROM SVMXC__Quote__c WHERE name = :quoteNumber];
        
        for(SVMXC__Quote__c sc: scList ){
            system.debug('Contact email '+sc.SVMXC__Contact__r.Email);
            System.debug('SVMXC__Contact__r.SVMX_Roles__c-->'+sc.SVMXC__Contact__r.SVMX_Roles__c);
        }
            System.debug('scList-->'+scList); 
            return scList; 
    }
    
    public static List<SVMXC__Quote__c> oppQuery(list<SVMXC__Quote__c> quoteList){
        
        List<SVMXC__Quote__c> oppList = [select id, SVMX_Opportunity__c, SVMXC__Status__c, SVMX_Opportunity__r.StageName from SVMXC__Quote__c where id in : quoteList ];
        
        return oppList;
    }
    
    public static List<SVMXC__Quote__c> quoteListOwnerChange(List<SVMXC__Quote__c> quoteList) {
        List<SVMXC__Quote__c> sqList = [SELECT id,SVMXC__Status__c, Owner.id FROM SVMXC__Quote__c WHERE id IN :quoteList] ;
        for(SVMXC__Quote__c sc:sqList){
        	system.debug('Status : '+sc.SVMXC__Status__c) ;
            system.debug('Owner :'+sc.OwnerId) ;
            }
        return sqList ;
    }
    
    public static void updateQuote(List<SVMXC__Quote__c> quoteList){
        update quoteList ;
    }
    
}