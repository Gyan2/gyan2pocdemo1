public class SalesOrderSimulateRequestMockImpl implements WebServiceMock  {
            private List<Map<String,String>> mapping;
            public SalesOrderSimulateRequestMockImpl(List<Map<String,String>> mapping){
                this.mapping = mapping;
            }

            public void doInvoke(
                    Object stub,
                    Object request,
                    Map<String, Object> response,
                    String endpoint,
                    String soapAction,
                    String requestName,
                    String responseNS,
                    String responseName,
                    String responseType) {
    
                SAPPO_SalesOrderRequest.SalesOrderSimulateResponse response_x = this.generateResponse(this.mapping);
    
                response.put('response_x', response_x);
            }

        public SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem generateItem(Map<String,String> mapping){
            SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem item = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem();
                item.Product = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemProduct();
                item.Product.InternalId = new SAPPO_CommonDataTypes.ProductInternalID();
                item.Product.InternalId.Content = mapping.get('internalId');
                item.TotalValues = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemTotalValues();
                item.TotalValues.RequestedQuantity = new SAPPO_CommonDataTypes.Quantity();
                item.TotalValues.RequestedQuantity.Content = mapping.get('quantity');
                item.TotalValues.ConfirmedQuantity = new SAPPO_CommonDataTypes.Quantity();
                item.TotalValues.ConfirmedQuantity.Content = mapping.get('quantity');
                item.TotalValues.NetAmount = new SAPPO_CommonDataTypes.Amount();
                item.TotalValues.NetAmount.Content = mapping.get('netAmount');
                item.TotalValues.NetAmount.CurrencyCode = 'CHF';
                item.PriceComponent = new List<SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent>{
                        new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent(),
                        new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent(),
                        new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent(),
                        new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent()
                };
                item.PriceComponent[0].PriceSpecificationElementTypeCode = 'MWST';
                item.PriceComponent[0].Rate = new SAPPO_EsmEdt.Rate();
                item.PriceComponent[0].Rate.BaseDecimalValue = '25';

                item.PriceComponent[1].PriceSpecificationElementTypeCode = 'SPED';
                item.PriceComponent[1].Rate = new SAPPO_EsmEdt.Rate();
                item.PriceComponent[1].Rate.BaseDecimalValue = '10';

                item.PriceComponent[2].PriceSpecificationElementTypeCode = 'BASD';
                item.PriceComponent[2].Rate = new SAPPO_EsmEdt.Rate();
                item.PriceComponent[2].Rate.BaseDecimalValue = '10';

                item.PriceComponent[3].PriceSpecificationElementTypeCode = 'ADDD';
                item.PriceComponent[3].Rate = new SAPPO_EsmEdt.Rate();
                item.PriceComponent[3].Rate.BaseDecimalValue = '10';

                item.ScheduleLine = new List<SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemScheduleLine>{
                        new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemScheduleLine()
                };

                item.ScheduleLine[0].Date_x = String.valueOf(System.today());
                item.ScheduleLine[0].RequestedQuantity = new SAPPO_CommonDataTypes.Quantity();
                item.ScheduleLine[0].RequestedQuantity.Content = mapping.get('quantity');
                item.ScheduleLine[0].ConfirmedQuantity = new SAPPO_CommonDataTypes.Quantity();
                item.ScheduleLine[0].ConfirmedQuantity.Content = mapping.get('quantity');

            return item;
        }

        public SAPPO_SalesOrderRequest.SalesOrderSimulateResponse generateResponse(List<Map<String,String>> data){
            SAPPO_SalesOrderRequest.SalesOrderSimulateResponse response_x = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponse();
            response_x.SalesOrder = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseSalesOrder();
            response_x.SalesOrder.Item = new List<SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem>();
            for(Map<String,String> mapping : data){
                response_x.SalesOrder.Item.add(this.generateItem(mapping));
            }

            response_x.SalesOrder.TotalValues = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseTotalValues();

            return response_x;
        }
    }