/*******************************************************************************************************
Description:
  This test class is used to test the SVMX_ServiceQuoteTrggrHandler class.
 
Author: Ranjitha S
Date: 23-11-2017

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

@isTest
public class SVMX_ServiceQuoteTrggrHandler_UT {

    static testMethod void invokeServiceQuotetrigger() {    
    
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, null, null, 'open', 'Reactive','Global Std', null, null, null, true);
         Opportunity op = SVMX_TestUtility.CreateOpportunity(acc.id, 'Test', 'stage', 50, 121, true);
        SVMXC__Quote__c sq = SVMX_TestUtility.CreateServiceQuote(acc.Id, wo.Id, op.id, 'Draft', true);
      
        test.startTest();
        try{
        //    sq.SVMX_Approval_Flag__c = true;
        sq.SVMXC__Status__c = 'Accepted';
        update sq;
        sq.SVMXC__Status__c = 'Rejected';
        update sq;
        }catch(Exception e){
            
        }
        
        test.stopTest();
    }

    public static testMethod void beforeUpdateServiceQuote() {

        Account account = SVMX_TestUtility.CreateAccount('Test Account','Norway',true) ;
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(account.Id, null, null, 'open', 'Reactive','Global Std', null, null, null, true);

        SVMXC_Quote_Configuration__c quoteConfiguration = new SVMXC_Quote_Configuration__c() ;
        quoteConfiguration.SVMX_Auto_Approval_Threshold__c = 200 ;
        quoteConfiguration.SVMX_Country__c = 'Norway' ;
        quoteConfiguration.SVMX_No_Of_Approvals__c = 1 ;
        quoteConfiguration.SVMX_Special_Approval__c = false ;

        insert quoteConfiguration ;

        SVMXC_Quote_Amount__c quoteAmount = new SVMXC_Quote_Amount__c() ;
        quoteAmount.SVMX_Level__c = 1 ;
        quoteAmount.SVMX_Maximum_Amount__c = 500 ;
        quoteAmount.SVMX_Minimum_Amount__c = 1 ;
        quoteAmount.SVMX_Quote_Configuration__c = quoteConfiguration.id ;

        insert quoteAmount ;


        SVMXC__Quote__c sq = SVMX_TestUtility.CreateServiceQuote(account.Id, wo.Id, null, 'Draft', false);
        sq.SVMX_PS_Country__c   = 'Norway' ;
        sq.SVMXC__Quote_Amount2__c = 100 ; 
        insert sq ;

        

    }

    public static testMethod void updateServiceQuote() {
Account account = SVMX_TestUtility.CreateAccount('Test Account','Norway',true) ;
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(account.Id, null, null, 'open', 'Reactive','Global Std', null, null, null, true);

        SVMXC_Quote_Configuration__c quoteConfiguration = new SVMXC_Quote_Configuration__c() ;
        quoteConfiguration.SVMX_Auto_Approval_Threshold__c = 200 ;
        quoteConfiguration.SVMX_Country__c = 'Norway' ;
        quoteConfiguration.SVMX_No_Of_Approvals__c = 1 ;
        quoteConfiguration.SVMX_Special_Approval__c = false ;


        insert quoteConfiguration ;

        SVMXC_Quote_Amount__c quoteAmount = new SVMXC_Quote_Amount__c() ;
        quoteAmount.SVMX_Level__c = 1 ;
        quoteAmount.SVMX_Maximum_Amount__c = 500 ;
        quoteAmount.SVMX_Minimum_Amount__c = 1 ;
        quoteAmount.SVMX_Quote_Configuration__c = quoteConfiguration.id ;

        insert quoteAmount ;

        Profile prof = SVMX_TestUtility.SelectProfile('System Administrator');
        User approver = SVMX_TestUtility.CreateUser('TestApp', prof.id, true);

        SVMXC_Quote_Approvers__c quoteApprovers = new SVMXC_Quote_Approvers__c() ;
        quoteApprovers.SVMX_Level__c = '1' ;
        quoteApprovers.SVMX_Quote_Configuration__c = quoteConfiguration.id ;
        quoteApprovers.SVMX_Special_Approver__c = false ;
        quoteApprovers.SVMX_Approver__c = approver.id ;

        insert quoteApprovers ;



        SVMXC__Quote__c sq = SVMX_TestUtility.CreateServiceQuote(account.Id, wo.Id, null, 'Draft', false);
        sq.SVMX_PS_Country__c   = 'Norway' ;
        sq.SVMXC__Quote_Amount2__c = 300 ; 
        insert sq ;

        sq.SVMXC__Status__c = 'Submit For Approval' ;
        update sq ;

        

    }
}