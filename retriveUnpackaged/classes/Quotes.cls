/**
 * Created by rebmangu on 07/03/2018.
 */

public with sharing class Quotes extends CoreSObjectDomain{

    public Quotes(List<Quote> sObjectList){
        super(sObjectList);
    }

    public override void onApplyDefaults() {
        // Apply defaults to Products

    }

    public override void onValidate() {
        // Validate Products
    }

    public override void onValidate(Map<Id,SObject> existingRecords) {
        // Validate changes to Products

    }

    public override void onBeforeInsert(){
        QuoteServices.handleStandardQuotes((List<Quote>)Records);

    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        // Related Products

    }

    public override void onBeforeDelete(){
        QuoteServices.deleteText((List<Quote>)Records);
    }

    public override void onAfterInsert(){
        QuoteServices.handleAccountLanguageAfterInsert((List<Quote>)Records);
    }


    public override void onAfterUpdate(Map<Id,SObject> existingRecords){

    }

    public override void onAfterDelete(){

    }


    public override void onAfterUndelete(){

    }


    public class Constructor implements CoreSObjectDomain.IConstructable
    {
        public CoreSObjectDomain construct(List<SObject> sObjectList)
        {
            return new Quotes(sObjectList);
        }
    }
}