/*******************************************************************************************************
Description: Trigger handler for Attachment Trigger

Dependancy: 
    Trigger Framework: 
    Trigger: 
    Service Manager: 
    Test class: 
    
 
Author: Yogesh 
Date: 

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/
public class SVMX_AttachmentTriggerHandler extends SVMX_TriggerHandler {

    private List<Attachment> newAttachmentList;
    private List<Attachment> oldAttachmentList;
    private Map<Id, Attachment> newAttachmentMap;
    private Map<Id, Attachment> oldAttachmentMap;
    
    public SVMX_AttachmentTriggerHandler() {

        this.newAttachmentList = (list<Attachment>) Trigger.new;
        this.oldAttachmentList = (list<Attachment>) Trigger.old;
        this.newAttachmentMap = (Map<Id, Attachment>) Trigger.newMap;
        this.oldAttachmentMap = (Map<Id, Attachment>) Trigger.oldMap;
    }

    public override void afterInsert(){

        Map<Id,Attachment> parentIdAttachmentMap = new Map<Id,Attachment>();
        Set<Id> AttcmntIdSet = new Set<Id>();
        Set<Id> WoIdSet = new Set<Id>();
        string name = '%'+System.Label.Service_Report+'%';

        for(Attachment Attachmnt:newAttachmentList){

            parentIdAttachmentMap.put(Attachmnt.ParentId,Attachmnt);
        }

        if(parentIdAttachmentMap.keyset().size() > 0){
            for(SVMXC__Service_Order__c wo: [ Select Id,Name,(Select Id, IsDeleted, ParentId, Name, IsPrivate,OwnerId, CreatedDate, CreatedById, LastModifiedDate,Description FROM Attachments Where Name LIKE :name AND Name LIKE '%.pdf' AND Id In :parentIdAttachmentMap.values()) FROM SVMXC__Service_Order__c where Id In : parentIdAttachmentMap.keyset()]){

                for(Attachment attchmnt : wo.Attachments){

                    AttcmntIdSet.add(attchmnt.Id);
                    WoIdSet.add(attchmnt.ParentId);
                }
            }
        }
        
        if(AttcmntIdSet.size()>0){
            SVMX_SAPServiceReportUtility.sendServiceReportsToSAP(AttcmntIdSet,WoIdSet);
        }
        

    }
}