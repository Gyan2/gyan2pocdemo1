public class SVMX_InstalledproductDataManager {
    
     public static list<SVMXC__Installed_Product__c> IPQuery(list<SVMXC__Site__c> LocList){
        list<SVMXC__Installed_Product__c> IPlist = new list<SVMXC__Installed_Product__c> ([select id, SVMXC__Site__c, SVMXC__Site__r.SVMXC__Account__r.name from SVMXC__Installed_Product__c  Where SVMXC__Site__c in:LocList]);
        return IPlist;
    }
    
    public static Map<id,SVMXC__Installed_Product__c> ProjectDateQuery(list<SVMXC__Installed_Product__c> ipList){
        Map<id,SVMXC__Installed_Product__c> IPmap = new Map<id,SVMXC__Installed_Product__c> ([select id, Project__c, Project__r.SVMX_Project_Delivered_on_Date__c  from SVMXC__Installed_Product__c  Where id in:ipList]);
        return IPmap;
    }
    
     
}