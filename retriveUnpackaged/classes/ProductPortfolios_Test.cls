@IsTest
public class ProductPortfolios_Test {
    testmethod public static void testAccountId(){
        Integer AMOUNT_OF_RECORDS = 100;
        List<Account> accounts = new List<Account>();
        for (Integer i = 0; i < AMOUNT_OF_RECORDS; i++) {
            accounts.add(new Account(
            	Name = 'Test Account #' + (i+1)
            ));
        }
        
        insert accounts;
        
        List<ProductPortfolio__c> productPortfolios = new List<ProductPortfolio__c>();
        
        for (Account forAcc : accounts) {
            productPortfolios.add(new ProductPortfolio__c(
            	AccountRef__c = forAcc.Id,
                ProductPortfolioDetail__c = 'Door control',
                ProductPortfolioGroup__c = 'Partner', 
                ProductPortfolioSubGroup__c = 'Door technology'
            ));
        }
            
        Test.startTest();
        	insert productPortfolios;
        	Integer i = 0;
        	for (ProductPortfolio__c forPP : [SELECT AccountRef__c, AccountId__c FROM ProductPortfolio__c WHERE ID IN :productPortfolios]){
                System.assertNotEquals(null, forPP.AccountRef__c);
            	System.assertEquals(forPP.AccountRef__c, forPP.AccountId__c);
            }
        
        	// Some calls to cover the other events
        	update productPortfolios;
        	delete productPortfolios;
        	undelete productPortfolios;
		Test.stopTest();
    }
}