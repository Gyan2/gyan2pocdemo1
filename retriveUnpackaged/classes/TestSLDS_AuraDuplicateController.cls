/**
 * Created by rebmangu on 24/04/2018.
 */

@IsTest
public with sharing class TestSLDS_AuraDuplicateController {

    @testSetup
    static void setup() {

        Building__c building = new Building__c(
                Name='Building',
                City__c='Strasbourg',
                Country__c='France',
                CountryIsoCode__c='FR',
                Street__c='24 Rue du general de gaulle',
                Zip__c='67000',
                Geolocation__Latitude__s=48.578606,
                Geolocation__Longitude__s=7.769301
        );

        insert building;


        Project__c project = new Project__c(
                Name='test project',
                City__c='Strasbourg',
                Country__c='France',
                CountryIsoCode__c='FR',
                Street__c='24 Rue du general de gaulle',
                MKS__c=true,
                Zip__c='67000',
                Geolocation__Latitude__s=48.578606,
                Geolocation__Longitude__s=7.769301,
                BuildingRef__c=building.Id


        );

        insert project;
    }


    static testMethod void testDuplicate(){
        List<Id> fixedSearchResults = new List<Id>();
        for(Project__c project : [select Id from Project__c]){
            fixedSearchResults.add(project.Id);
        }
        Test.setFixedSearchResults(fixedSearchResults);

        Map<Id,SObject> result = SLDS_AuraDuplicateController.getSuggestionFromController('Test Project','NAME FIELDS RETURNING Project__c(id, name,OwnerId WHERE name != null)');
        System.debug(result);
        System.assertEquals(1,result.size());

                        result = SLDS_AuraDuplicateController.getSuggestionFromController('Hello the world','NAME FIELDS RETURNING Project__c(Id,Name,OwnerId WHERE Name != null)');
        System.assertEquals(0,result.size());

    }
}