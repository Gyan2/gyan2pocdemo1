/*******************************************************************************************************
Description: Batch class for picking PM work orders and processing

Dependancy:    
  Class: SVMX_PM_Controller
 
Author: Pooja Singh
Date: 20-12-2017

Modification Log: 
Date            Author          Modification Comments
22-Jan-2108     Tulsi BR    Added price and entitlement histroy to the work detail lines
--------------------------------------------------------------

*******************************************************************************************************/

global class SVMX_PM_WorkOrderBatch implements Database.Batchable<sObject>{
    
    public String query = '';
    public Set<Id> woLocId = new Set<Id>();
    public Set<Id> woServContId = new Set<Id>();
    global Database.QueryLocator start(Database.BatchableContext BC) {
        //Query all PM work orders
        query = 'Select Id, name, SVMXC__Site__c, SVMXC__PM_Plan__c, SVMXC__Service_Contract__c, SVMXC__PM_SC__c, SVMXC__PM_Plan__r.SVMXC__Coverage_Type__c, SVMXC__PM_Plan__r.SVMX_Converted_Plan__c, SVMX_Work_Detail_Created__c, SVMX_Visit_Number__c  from SVMXC__Service_Order__c where SVMXC__PM_Plan__c != null and CreatedDate =TODAY and SVMX_Work_Detail_Created__c = false';
          return Database.getQueryLocator(query);
    }
    
    global void execute(Database.BatchableContext BC, List<SVMXC__Service_Order__c> woList) {
        Map<Id, Map<Id,List<Id>>> woIdLocIdIpIdListMap = new Map<Id, Map<Id,List<Id>>>();  //Map of wo id and map of loc Id and list of Ip Id
        Map<Id,Id> ipIdProdIdMap = new Map<Id,Id>();  //Map of ip id and prod id's
        Map<Id,Decimal> IPLocPriceMap = new Map<Id,Decimal>();
        Map<Id,Decimal> IPLocDiscountMap = new Map<Id,Decimal>();
        Map<id,Map<Id,SVMXC__Service_Contract_Products__c>> servicecontractcoverdProductMap = new Map<id,Map<Id,SVMXC__Service_Contract_Products__c>>();
        set<Id> conId = new set<Id>();
        //set<Id> woIdset = new  set<Id>();
     
        List<SVMXC__Entitlement_History__c> EnhCreate = new List<SVMXC__Entitlement_History__c>();
        List<SVMXC__Service_Order__c>  upWorkOrders = new List<SVMXC__Service_Order__c>();
        for(SVMXC__Service_Order__c wo : woList){
          if(wo.SVMXC__PM_Plan__r.SVMXC__Coverage_Type__c == 'Location (Must Have Location)' && wo.SVMXC__PM_Plan__r.SVMX_Converted_Plan__c == false){
                woLocId.add(wo.SVMXC__Site__c);
                conId.add(wo.SVMXC__PM_SC__c);
            }
            if(wo.SVMXC__PM_Plan__r.SVMXC__Coverage_Type__c == 'Location (Must Have Location)' && wo.SVMXC__PM_Plan__r.SVMX_Converted_Plan__c){
                woServContId.add(wo.SVMXC__PM_SC__c);
            }
        }
        system.debug('woLocId size '+woLocId);
        if(woLocId.size() > 0){
            //List<Id> ipIdList = new List<Id>();
          //Map<Id,List<Id>> locIdIpIdListMap = new Map<Id,List<Id>>();
            //Installed products query

            set<Id> woIds = new set<Id>();
            Map<Id,SVMXC__Service_Contract_Sites__c> LocCovMap=new Map<id,SVMXC__Service_Contract_Sites__c>();

            List<SVMXC__Installed_Product__c> locIpList = new List<SVMXC__Installed_Product__c>([Select Id, SVMXC__Site__c, SVMXC__Product__c from SVMXC__Installed_Product__c where SVMXC__Site__c IN :woLocId]);

            List<SVMXC__Service_Contract_Sites__c> covLocs = [select id,SVMXC__Line_Price__c,SVMXC__Site__c, SVMX_Visit_1_Charge__c, SVMX_Visit_2_Charge__c, SVMX_Visit_3_Charge__c, SVMX_Visit_4_Charge__c, SVMX_Visit_5_Charge__c,  SVMX_Visit_6_Charge__c, SVMX_Visit_7_Charge__c, SVMX_Visit_8_Charge__c, SVMX_Visit_9_Charge__c, SVMX_Visit_10_Charge__c, SVMX_Visit_11_Charge__c, SVMX_Visit_12_Charge__c,SVMXC__Service_Contract__r.SVMX_Number_of_Visits__c,SVMX_Discount__c from SVMXC__Service_Contract_Sites__c where SVMXC__Service_Contract__c In: conId];
            for(SVMXC__Service_Contract_Sites__c st: covLocs)
                LocCovMap.put(st.SVMXC__Site__c,st);
             
            for(SVMXC__Service_Order__c wo : woList){
                Map<Id,List<Id>> locIdIpIdListMap = new Map<Id,List<Id>>();
                //evaluate if Lcoation has visit
                //loop thru covered locations
                ////determine it qualifoes for visit
                integer i = integer.valueOf(wo.SVMX_Visit_Number__c);
              //  string visitCharge = 'SVMX_Visit_'+i+'_Charge__c';
               
               /* for(SVMXC__Service_Contract_Sites__c cl: covLocs){
                    List<decimal> visitCharge = new List<decimal>();
                    visitcharge.add(cl.SVMX_Visit_1_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_2_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_3_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_4_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_5_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_6_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_7_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_8_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_9_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_10_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_11_Charge__c);
                    visitcharge.add(cl.SVMX_Visit_12_Charge__c);
                  //  if(wo.SVMXC__Site__c == cl.SVMXC__Site__c && visitCharge.get(i+1) != null)
                        //
               }*/
                
                for(SVMXC__Installed_Product__c ip : locIpList){
                    if(wo.SVMXC__Site__c == ip.SVMXC__Site__c){
                        //if(woIdLocIdIpIdListMap.containsKey(wo.Id)){
                            //Map<Id,List<Id>> tempLocIdIpIdListMap = woIdLocIdIpIdListMap.get(wo.Id);
                            if(locIdIpIdListMap.containsKey(wo.SVMXC__Site__c)){
                              List<Id> tempIpList = locIdIpIdListMap.get(wo.SVMXC__Site__c);
                              tempIpList.add(ip.id);
                              locIdIpIdListMap.put(wo.SVMXC__Site__c,tempIpList);
                                woIdLocIdIpIdListMap.put(wo.id,locIdIpIdListMap);
                                
                            }
                            else{
                                List<Id> ipIdList = new List<Id>();
                                ipIdList.add(ip.id);
                              locIdIpIdListMap.put(wo.SVMXC__Site__c, ipIdList);
                              woIdLocIdIpIdListMap.put(wo.id,locIdIpIdListMap);
                              woIds.add(wo.id);
                            }
                        /*}
                        else{
                            ipIdList.add(ip.id);
                            locIdIpIdListMap.put(wo.SVMXC__Site__c, ipIdList);
                            woIdLocIdIpIdListMap.put(wo.id,locIdIpIdListMap);
                        }*/
                        ipIdProdIdMap.put(ip.id,ip.SVMXC__Product__c);
                    }
                }
            }


            //evaluate price per IP
           

            for(Id wo:woIds){
                Map<Id,List<Id>> LocIdMap = woIdLocIdIpIdListMap.get(wo);
                for(Id loc: LocIdMap.keyset()){

                    Decimal price = 0;
                    Decimal Discount = 0;
                    if(LocCovMap.containsKey(loc))
                        price = LocCovMap.get(loc).SVMXC__Line_Price__c;
                        Discount = LocCovMap.get(loc).SVMX_Discount__c;
                        //system.debug('ip '+LocCovMap.get(loc).SVMXC__Service_Contract__c);
                        String num = null;
                        //if(LocCovMap.get(loc).SVMXC__Service_Contract__r.SVMX_Number_of_Visits__c != null)
                        try{
                        num = LocCovMap.get(loc).SVMXC__Service_Contract__r.SVMX_Number_of_Visits__c;
                        }catch(exception e){}
                        Integer div =1;
                        if(num != null)
                            div = Integer.valueOf(num);
                        if(div > 0 && price >0)
                            price = price /div;

                    if(price != null && price > 0){
                        List<Id> AllIps = LocIdMap.get(loc);
                        Integer divNum = AllIps.size();
                        Decimal priceperIP = price/divNum;

                        for(Id ipid: AllIps )
                            IPLocPriceMap.put(ipid,priceperIP);
                    }
                    if(Discount != null && Discount > 0){
                        List<Id> AllIps = LocIdMap.get(loc);

                        for(Id ipid: AllIps )
                            IPLocDiscountMap.put(ipid,Discount);
                    }

                }
            }


        }
        system.debug('woServContId size '+woServContId);
        if(woServContId.size() > 0){            
            
            //Covered products query           
            List<SVMXC__Service_Contract_Products__c> scCpList = new List<SVMXC__Service_Contract_Products__c>([Select Id, SVMXC__Installed_Product__c, SVMXC__Installed_Product__r.SVMXC__Product__c, SVMXC__Service_Contract__c, SVMXC__Service_Contract__r.SVMXC__Start_Date__c,SVMXC__Service_Contract__r.SVMXC__End_Date__c,SVMXC__Installed_Product__r.SVMXC__Site__c,SVMXC__Line_Price__c, SVMX_Visit_1_Charge__c, SVMX_Visit_2_Charge__c, SVMX_Visit_3_Charge__c, SVMX_Visit_4_Charge__c, SVMX_Visit_5_Charge__c,  SVMX_Visit_6_Charge__c, SVMX_Visit_7_Charge__c, SVMX_Visit_8_Charge__c, SVMX_Visit_9_Charge__c, SVMX_Visit_10_Charge__c, SVMX_Visit_11_Charge__c, SVMX_Visit_12_Charge__c,SVMXC__Service_Contract__r.SVMX_Number_of_Visits__c,SVMX_Discount__c from SVMXC__Service_Contract_Products__c where SVMXC__Service_Contract__c IN:woServContId]);

            for(SVMXC__Service_Contract_Products__c coveredProduct:scCpList){
                
                Map<id,SVMXC__Service_Contract_Products__c> tempipcoveredProductMap = new Map<id,SVMXC__Service_Contract_Products__c>();

                if(servicecontractcoverdProductMap.containsKey(coveredProduct.SVMXC__Service_Contract__c)){

                    tempipcoveredProductMap = servicecontractcoverdProductMap.get(coveredProduct.SVMXC__Service_Contract__c);
                    
                }
                tempipcoveredProductMap.put(coveredProduct.SVMXC__Installed_Product__c,coveredProduct);
                servicecontractcoverdProductMap.put(coveredProduct.SVMXC__Service_Contract__c,tempipcoveredProductMap);
            }
            system.debug('servicecontractcoverdProductMap***'+servicecontractcoverdProductMap);
            for(SVMXC__Service_Order__c wo : woList){
                system.debug('wo--> '+wo);
                Map<Id,List<Id>> locIdIpIdListMap = new Map<Id,List<Id>>();
                ID scid = null;
                Date st =null;
                Date ed =null;                
                for(SVMXC__Service_Contract_Products__c cp : scCpList){
                    system.debug('cp.SVMXC__Service_Contract__c -->'+cp.SVMXC__Service_Contract__c+'wo.SVMXC__Site__c -->'+wo.SVMXC__Site__c+'  cp.SVMXC__Installed_Product__r.SVMXC__Site__c -->'+cp.SVMXC__Installed_Product__r.SVMXC__Site__c);
                  
                    //determine visit number and match
                    if(wo.SVMXC__PM_SC__c == cp.SVMXC__Service_Contract__c && wo.SVMXC__Site__c == cp.SVMXC__Installed_Product__r.SVMXC__Site__c){
                        
                          scid=cp.SVMXC__Service_Contract__c;
                          st = cp.SVMXC__Service_Contract__r.SVMXC__Start_Date__c;
                          ed = cp.SVMXC__Service_Contract__r.SVMXC__End_Date__c;
                    
                        if(locIdIpIdListMap.containsKey(wo.SVMXC__Site__c)){
                            List<Id> tempIpList = locIdIpIdListMap.get(wo.SVMXC__Site__c);
                            tempIpList.add(cp.SVMXC__Installed_Product__c);
                            locIdIpIdListMap.put(wo.SVMXC__Site__c,tempIpList);
                            system.debug('locIdIpIdListMap in if--> '+locIdIpIdListMap);
                            woIdLocIdIpIdListMap.put(wo.id,locIdIpIdListMap);
                            system.debug('Inside If '+woIdLocIdIpIdListMap);
                            String num = cp.SVMXC__Service_Contract__r.SVMX_Number_of_Visits__c;
                            Integer div = 1;
                            if(num != null)
                                div = Integer.valueOF(num);
                            Decimal price = cp.SVMXC__Line_Price__c;
                            Decimal discount = cp.SVMX_Discount__c;
                            if(div > 0 && cp.SVMXC__Line_Price__c > 0)
                                price =(cp.SVMXC__Line_Price__c/div);
                            IPLocPriceMap.put(cp.SVMXC__Installed_Product__c,price);
                            if(cp.SVMX_Discount__c > 0){
                               IPLocDiscountMap.put(cp.SVMXC__Installed_Product__c,discount);
                            }

                        }
                        else{
                            List<Id> ipIdList = new List<Id>();
                            ipIdList.add(cp.SVMXC__Installed_Product__c);
                            locIdIpIdListMap.put(wo.SVMXC__Site__c, ipIdList);
                            system.debug('locIdIpIdListMap in else--> '+locIdIpIdListMap);
                            woIdLocIdIpIdListMap.put(wo.id,locIdIpIdListMap);
                            system.debug('Inside else '+woIdLocIdIpIdListMap);
                            String num = cp.SVMXC__Service_Contract__r.SVMX_Number_of_Visits__c;
                            Integer div = 1;
                            if(num != null)
                                div = Integer.valueOF(num);
                            system.debug('line price '+cp.SVMXC__Line_Price__c);
                            Decimal price = cp.SVMXC__Line_Price__c;
                            Decimal discount = cp.SVMX_Discount__c;
                            if(div > 0 && cp.SVMXC__Line_Price__c > 0)
                                price =(cp.SVMXC__Line_Price__c/div);
                            IPLocPriceMap.put(cp.SVMXC__Installed_Product__c,price);
                            if(cp.SVMX_Discount__c > 0){
                               IPLocDiscountMap.put(cp.SVMXC__Installed_Product__c,discount);
                            }
                        }
                       ipIdProdIdMap.put(cp.SVMXC__Installed_Product__c,cp.SVMXC__Installed_Product__r.SVMXC__Product__c);
                    }
                    //else 
                    //delete list
                }
                 system.debug('IPLocDiscountMap***'+IPLocDiscountMap);

                //Create entitlement History
                SVMXC__Entitlement_History__c wh = new SVMXC__Entitlement_History__c();
                wh.SVMXC__Service_Order__c =wo.id;
                wh.SVMXC__Service_Contract__c = scid;
                wh.SVMXC__Date_of_entitlement__c = system.today();
                wh.SVMXC__Start_Date__c =st;
                wh.SVMXC__End_Date__c = ed;
                EnhCreate.add(wh);

                //update entitlement on WO
                SVMXC__Service_Order__c woUp = new SVMXC__Service_Order__c();
                woUp.Id = wo.id;
                woUp.SVMXC__Service_Contract__c = scid;
                woUp.SVMXC__Auto_Entitlement_Status__c ='Success';
                upWorkOrders.add(woUp);

            }
        }
       

        if(EnhCreate.size() > 0 )
            SVMX_EntitlementHistoryDataManager.createEntitleHistory(EnhCreate);
     
        if(upWorkOrders.size() >0 && !upWorkOrders.isEmpty()){
            update upWorkOrders;}
        
        system.debug('woIdLocIdIpIdListMap Map'+woIdLocIdIpIdListMap);
        system.debug('IP and prod id Map'+ipIdProdIdMap);
        if(!woIdLocIdIpIdListMap.isEmpty() && !ipIdProdIdMap.isEmpty() && !servicecontractcoverdProductMap.keyset().isEmpty()){
         SVMX_PM_Controller.createProdServicedLines(woIdLocIdIpIdListMap,ipIdProdIdMap,IPLocPriceMap,IPLocDiscountMap,servicecontractcoverdProductMap);
        }    
    }
    
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}