/************************************************************************************************************
Description: Test class for the Controller Class (SVMX_SendQuoteEmail) 

VF page Dependancy: SVMX_SendQuote_DataManager
 
Author: Maheshwar Sari
Date: 06-11-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest public class SVMX_SendQuoteController_UT 

{    
   
    static testMethod void testRun01()
    {
        Contact con = new Contact();
        con.FirstName = 'Con FN';
        con.LastName = 'Con FN';
        con.Phone = '123456';
        con.Email = 'Con@servicemax.com';
        con.SVMX_Roles__c = 'Quotation Manager';
        con.Language__c = 'EN';
        Insert con;
        
        RecordType globalStd = [select Id from RecordType WHERE SobjectType = 'SVMXC__Service_Contract__c' AND DeveloperName = 'Global_Std_Quote'];
        
        SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c();
        contract.RecordtypeId = globalStd.Id;
        contract.name = 'test';
        contract.SVMXC__Contact__c = con.Id;
        Insert contract;

        Attachment attachmentRec = new Attachment(Name = 'Test', ParentId = contract.Id, Body = Blob.valueOf('Test') );
        Insert attachmentRec;
        
        contract = [select Id, SVMX_Contract_Quotation_No__c from SVMXC__Service_Contract__c WHERE Id =: contract.Id ];

        PageReference pageRef = Page.SVMX_SendQuoteEmail;

        
        pageRef.getParameters().put('quoteNumber', contract.SVMX_Contract_Quotation_No__c);
        Test.setCurrentPage(pageRef);


        SVMX_SendQuoteController obj01 = new SVMX_SendQuoteController();
        
        obj01.isCheckedAll = TRUE;
        obj01.checkAllValue();
        obj01.sendQuotes();
       
        
    }
}