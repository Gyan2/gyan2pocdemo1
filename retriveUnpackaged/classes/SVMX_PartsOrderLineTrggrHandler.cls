public class SVMX_PartsOrderLineTrggrHandler extends SVMX_TriggerHandler {
    

    private list<SVMXC__RMA_Shipment_Line__c> newPOLList;
    private list<SVMXC__RMA_Shipment_Line__c> oldPOLList;
    private Map<Id, SVMXC__RMA_Shipment_Line__c> newPOLMap;
    private Map<Id, SVMXC__RMA_Shipment_Line__c> oldPOLMap;
     
    public SVMX_PartsOrderLineTrggrHandler() {
        this.newPOLList = (list<SVMXC__RMA_Shipment_Line__c>) Trigger.new;
        this.oldPOLList = (list<SVMXC__RMA_Shipment_Line__c>) Trigger.old;
        this.newPOLMap = (Map<Id, SVMXC__RMA_Shipment_Line__c>) Trigger.newMap;
        this.oldPOLMap = (Map<Id, SVMXC__RMA_Shipment_Line__c>) Trigger.oldMap;
    }

    public static id rmaRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('RMA','SVMXC__RMA_Shipment_Line__c');
    public static id shipmentRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Shipment','SVMXC__RMA_Shipment_Line__c');
    public static id vendorRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('VENDOR_ERP','Account');

    public override void beforeinsert() {

       set<id> partsOrdrIds = new set<id> ();
       List<SVMXC__RMA_Shipment_Line__c> partsOrderLine = new List<SVMXC__RMA_Shipment_Line__c>();

       for( SVMXC__RMA_Shipment_Line__c pol:newPOLList){
           if(pol.SVMX_Vendor__c != null && pol.SVMXC__Product__c != null){
             partsOrdrIds.add(pol.SVMX_Vendor__c);
             system.debug('partsOrdrIds-->'+partsOrdrIds);
             partsOrderLine.add(pol);
           }
       }
       if(partsOrdrIds.size() > 0){
          SVMX_PartsOrderLineServiceManager.checkValidation(partsOrdrIds,partsOrderLine);
       }
                
    } 

    public override void beforeUpdate() {

       set<id> partsOrdrIds = new set<id> ();
     
      List<SVMXC__RMA_Shipment_Line__c> partsOrderLine = new List<SVMXC__RMA_Shipment_Line__c>();
      
       for( SVMXC__RMA_Shipment_Line__c pol:newPOLList){
            if(pol.SVMXC__Product__c != null && pol.SVMXC__Product__c != oldPOLMap.get(pol.id).SVMXC__Product__c && pol.SVMX_Vendor__c != null ){
               partsOrdrIds.add(pol.SVMX_Vendor__c);
               partsOrderLine.add(pol);
              system.debug('partsOrdrIds-->'+partsOrdrIds);
            }  
       }

       if(partsOrdrIds.size() > 0){
          SVMX_PartsOrderLineServiceManager.checkValidation(partsOrdrIds,partsOrderLine);
       }
   
              
    } 

    public override void AfterInsert() {

        List<SVMXC__RMA_Shipment_Line__c> polIntegrationList = new  List<SVMXC__RMA_Shipment_Line__c>();
        List<SVMXC__RMA_Shipment_Line__c> polIntPurchaseOrdList = new  List<SVMXC__RMA_Shipment_Line__c>();
        List<SVMXC__RMA_Shipment_Line__c> polIntProcuredPartShipmtList = new  List<SVMXC__RMA_Shipment_Line__c>();
        List<SVMXC__RMA_Shipment_Line__c> polIntSparePartsShipmentList = new  List<SVMXC__RMA_Shipment_Line__c>();

        for(SVMXC__RMA_Shipment_Line__c partsOrderLine:newPOLList){
          
          if( partsOrderLine.RecordTypeId == rmaRecordTypeId) {
                
                polIntegrationList.add(partsOrderLine);            
          }
           
          if( partsOrderLine.RecordTypeId == shipmentRecordTypeId && partsOrderLine.SVMXC__Line_Type__c == 'Stock Part') {

                polIntPurchaseOrdList.add(partsOrderLine);
          }
          if( partsOrderLine.RecordTypeId == shipmentRecordTypeId && partsOrderLine.SVMXC__Line_Type__c == 'Procured Part' ){

                polIntProcuredPartShipmtList.add(partsOrderLine);
          }
          if( partsOrderLine.RecordTypeId == shipmentRecordTypeId && partsOrderLine.SVMXC__Line_Type__c == 'Spare Parts Order') {

                polIntSparePartsShipmentList.add(partsOrderLine);
          }
        }
        
        System.debug('SVMX_PartsOrderLineTrggrHandler ' + polIntegrationList);
        
        if(!polIntegrationList.isEmpty()) {
          
          SVMX_RMAServiceManager.handleSAPSalesOrderRMACreation(polIntegrationList);
        }
        
        if(!polIntPurchaseOrdList.isEmpty()) {
          
          SVMX_SAPPurchaseRequestUtility.handleSAPPurchaseRequestCreation(polIntPurchaseOrdList);
        }
        if(!polIntProcuredPartShipmtList.isEmpty()) {
          
          SVMX_ProcuredPartsShipmentManager.handleSAPSalesOrderCreation(polIntProcuredPartShipmtList);
        }
        if(!polIntSparePartsShipmentList.isEmpty()) {
          
          SVMX_SparePartsShipmentManager.handleSAPSalesOrderCreation(polIntSparePartsShipmentList);
        }
    }

    public override void afterUpdate() {

         Map<Id,SVMXC__RMA_Shipment_Line__c> polIntMap = new Map<Id,SVMXC__RMA_Shipment_Line__c>();
         Map<Id,SVMXC__RMA_Shipment_Line__c> polIntSparePartsShipmentMap= new  Map<Id,SVMXC__RMA_Shipment_Line__c>();
         Map<Id,SVMXC__RMA_Shipment_Line__c> polIntprocuredpartShipmentMap= new  Map<Id,SVMXC__RMA_Shipment_Line__c>();


         for(SVMXC__RMA_Shipment_Line__c pol: newPOLMap.values()){

            if(pol.RecordTypeId == rmaRecordTypeId) {

              //if(pol.SVMX_Awaiting_SAP_Response__c == false && pol.SVMX_Item_Number_Blank_Check__c == false) {
                
                polIntMap.put(pol.Id, pol);
              //}
            } 
            if( pol.RecordTypeId == shipmentRecordTypeId && pol.SVMXC__Line_Type__c == 'Spare Parts Order'){

                polIntSparePartsShipmentMap.put(pol.Id,pol);
            }
            if( pol.RecordTypeId == shipmentRecordTypeId && pol.SVMXC__Line_Type__c == 'Procured Part'){

                polIntprocuredpartShipmentMap.put(pol.Id,pol);
            }

         }

         if(!polIntMap.isEmpty()) {
          
          SVMX_RMAServiceManager.handleSAPSalesOrderUpdateRMA(polIntMap,oldPOLMap);
         }
         if(!polIntSparePartsShipmentMap.keyset().isEmpty()) {
          
           SVMX_SparePartsShipmentManager.handleSAPSalesOrderItemUpdates(polIntSparePartsShipmentMap,oldPOLMap);
         }
         if(!polIntprocuredpartShipmentMap.keyset().isEmpty()) {
          
           SVMX_ProcuredPartsShipmentManager.handleSAPSalesOrderItemUpdates(polIntprocuredpartShipmentMap,oldPOLMap);
         }   
    }
     
     
}