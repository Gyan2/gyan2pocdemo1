@isTest
public class LCC001_Generic_Hierarchy_Controller_Test {

    static testMethod void validateGenerateQueryString(){
        String lookupAPIName = 'LookUp__c';
        String formulaAPIName = 'Ultimate_Parent__c';
        String masterParentId = 'aaaa11113333';
        String objectAPIName = 'Custom_Object__c';
        String[] columns = new String[]{'column1__c','column2__c'};      
            System.assertEquals('SELECT Id,LookUp__c,Ultimate_Parent__c,column1__c,column2__c FROM Custom_Object__c WHERE Ultimate_Parent__c=\'aaaa11113333\'',LCC001_Generic_Hierarchy_Controller.generateQueryString(masterParentId,lookupAPIName,formulaAPIName,objectAPIName,columns));    
    }
    
    
    static testMethod void validateGetColumnLabels(){
        String objectName = 'Account';
        String[] columns = new String[2];
        columns[0] = 'Name';
        columns[1] = 'OwnerId';
        List<String> columnLabels = new List<String>();
        columnLabels.add(Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(columns[0]).getDescribe().getLabel());
        columnLabels.add(Schema.getGlobalDescribe().get(objectName).getDescribe().fields.getMap().get(columns[1]).getDescribe().getLabel());
        System.assertEquals(columnLabels, LCC001_Generic_Hierarchy_Controller.getColumnLabels(objectName,columns));     
        System.assertEquals(null,LCC001_Generic_Hierarchy_Controller.getColumnLabels('',columns));
    }
    
    static testMethod void validateGetMasterParentId(){
        Account parent = new Account(Name='TestParentAccount');
        insert parent;
        Account child = new Account(Name='TestChildAccount', ParentId=parent.Id);
        insert child;
        System.assertEquals(parent.Id,LCC001_Generic_Hierarchy_Controller.getMasterParentId(child.Id, 'Account' , 'ParentId'));   
        System.assertEquals(null,LCC001_Generic_Hierarchy_Controller.getMasterParentId(child.Id, '' , ''));
    }  
    
}