@isTest
public class SVMX_IntegrationErrorManager_UT {

    static testMethod void integrationErrorHandler(){
        
        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'WorkOrder';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
        
        SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
              intg.name= 'SAP PO';
              intg.WS_USER__c ='test';
              intg.WS_PASS__c ='test';
        insert intg;

        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;

        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',false);
        acc.SAPNumber__c ='893493';
        insert acc;

        SVMXC__Site__c loct=new SVMXC__Site__c(Name='test location',SVMXC__Account__c=acc.id,SVMXC_Global_Location_Name__c ='87363497');
        
        insert loct;
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers',false);
        prod.SAPNumber__c= '1234567';
        insert prod;
        
        Product2 prod1 = SVMX_TestUtility.CreateProduct('Door', false);
        prod1.SAPNumber__c= '1234567';
        insert prod1;
        
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
        
        Case cs = SVMX_TestUtility.CreateCase(acc.Id,con.Id, 'New', false);
        cs.SVMX_Service_Sales_Order_Number__c =null;
        cs.SVMX_Awaiting_SAP_Response__c = True;
        cs.SVMXC__Site__c =loc.id;
        cs.SVMX_Invoice_Status__c='Requested on all cases';
        insert cs;

        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id,true);
        
        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        SVMX_Rate_Tiers__c rt2 = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, true, true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Germany',true);
        
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Planned Services (PPM)','Global Std',sc.Id,tech.Id,cs.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.today();
        wo.SVMX_Service_Number_Blank_Check__c =false;
        wo.SVMX_Service_Sales_Order_Number__c = '6846957';
        insert wo;

        SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Planned Services (PPM)','Global Std',sc.Id,tech.Id,cs.id,false);
        wo1.SVMXC__Scheduled_Date_Time__c = system.today();
        wo1.SVMX_Service_Number_Blank_Check__c =false;
        wo1.SVMX_Service_Sales_Order_Number__c = '6846957';
        insert wo1;

        list<SVMXC__Service_Order_Line__c> wol = new list<SVMXC__Service_Order_Line__c>();
        
        SVMXC__Service_Order_Line__c oli = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id,prod.Id,'Usage/Consumption',wo.Id,system.now(), sc.Id, tech.Id, cs.id,'Parts','Warranty',false);
        oli.SVMXC__Actual_Price2__c =4;
        oli.SVMXC__Actual_Quantity2__c = 2;
        oli.SVMXC__Billable_Quantity__c = 1;
        oli.SVMX_PM_Charge__c = 'yes';
        wol.add(oli);
        
        SVMXC__Service_Order_Line__c oli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id,prod.Id,'Usage/Consumption',wo1.Id,system.now(), sc.Id, tech.Id, cs.id,'parts','Warranty',false);
        oli1.SVMXC__Actual_Price2__c =5;
        oli1.SVMXC__Actual_Quantity2__c = 2;
        oli1.SVMX_Sales_Order_Item_Number__c='123';
        oli1.SVMXC__Billable_Quantity__c = 7;
        oli1.SVMX_PM_Charge__c = 'yes';
        wol.add(oli1);

        insert wol;

        List<SVMX_Integration_Error_Tracking__c> ErrorTrackingList = new List<SVMX_Integration_Error_Tracking__c> ();
        
        SVMX_Integration_Error_Tracking__c errorTarck1 = new SVMX_Integration_Error_Tracking__c();
        errorTarck1.SVMX_Resend_to_SAP__c = false;
        errorTarck1.SVMX_Header_Record_Id__c = string.valueof(wo.Id);
        errorTarck1.SVMX_Item_Record_Ids__c = string.valueof(oli.Id);
        errorTarck1.SVMX_Error_Code__c = '001';
        ErrorTrackingList.add(errorTarck1);

        SVMX_Integration_Error_Tracking__c errorTarck2 = new SVMX_Integration_Error_Tracking__c();
        errorTarck2.SVMX_Resend_to_SAP__c = false;
        errorTarck2.SVMX_Header_Record_Id__c = string.valueof(wo1.Id);
        errorTarck2.SVMX_Item_Record_Ids__c = string.valueof(oli1.Id);
        errorTarck2.SVMX_Error_Code__c = '002';
        ErrorTrackingList.add(errorTarck2);

        insert ErrorTrackingList;
        test.startTest();

        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder salesOrderreq=new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt.CreateSync(messageheader,salesOrderreq);

        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader1=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder salesOrderreq1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt1.UpdateSync(messageheader1,salesOrderreq1); 

        errorTarck1.SVMX_Resend_to_SAP__c=true;

        errorTarck2.SVMX_Resend_to_SAP__c=true;
        
        update errorTarck1;
        
        update errorTarck2;

        test.stopTest();

    }
}