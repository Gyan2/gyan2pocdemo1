@isTest(SeeAllData=true)
public class SVMX_GetPartsOrderPrice_UT {
   
    public static testMethod void createTestData(){
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
      
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
           
        Case cs = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', true);
       
        
        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        //SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
       Account vendor = new Account() ;
        RecordType rt= [select Id from RecordType where DeveloperName='VENDOR_ERP' and SobjectType='Account' ];
        //Id recordTypeId = Schema.SObjectType.Account.getRecordTypeInfosByName().get('VENDOR_ERP').getRecordTypeId();
        vendor.RecordTypeId = rt.id  ;
        
        vendor.Name = 'Test vendor' ;
        vendor.BillingCountry = 'Norway' ;
        vendor.SVMX_Automatic_billing__c='Automatic' ;
        vendor.CurrencyIsoCode = 'EUR' ;
        vendor.Status__c = 'Active' ;
        //system.debug('RecordType '+Schema.SObjectType.Account.getRecordTypeInfosByName().get('VENDOR_ERP').getRecordTypeId()) ;
        
        insert vendor;
      
       
        Product2 product = SVMX_TestUtility.CreateProduct('Test Product',true);
        Contact contact = SVMX_TestUtility.CreateContact(vendor.id,true); 
        
        VendorMaterial__c vendorMaterial = new VendorMaterial__c() ;
        vendorMaterial.Account__c = vendor.id ;
        vendorMaterial.ExternalId__c = 'Test Door' ;
        vendorMaterial.Product__c = product.id ;
        insert vendorMaterial ;
        
        SVMXC__Site__c location = SVMX_TestUtility.CreateLocation('Product Location',vendor.id,true) ;
        SVMXC__Service_Order__c  workOrder = SVMX_TestUtility.CreateWorkOrder(vendor.id,location.id,product.id,null,null,null,null,null,null,true);
        workOrder.SVMXC__Scheduled_Date_Time__c = system.today();
        update workOrder ;
         RecordType rt2= [select Id from RecordType where DeveloperName='Shipment' and SobjectType='SVMXC__RMA_Shipment_Order__c' ];
        SVMXC__RMA_Shipment_Order__c partsOrder = new SVMXC__RMA_Shipment_Order__c();
        partsOrder.SVMXC__Company__c = vendor.id ;
        partsOrder.SVMX_Vendor__c = vendor.id ;
        partsOrder.SVMXC__Contact__c = contact.id ;
        partsOrder.SVMXC__Order_Status__c = 'Open' ;
        partsOrder.SVMXC__Expected_Receive_Date__c = Date.today();
        partsOrder.SVMXC__Service_Order__c = workOrder.id ;
        partsOrder.recordtypeid=rt2.id;
        insert partsOrder;
        
        SVMXC__RMA_Shipment_Line__c partsRequestLine = new SVMXC__RMA_Shipment_Line__c();
        partsRequestLine.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
        partsRequestLine.SVMXC__Product__c = product.id ;
        partsRequestLine.SVMXC__RMA_Shipment_Order__c = partsOrder.id ;
        partsRequestLine.SVMXC__Expected_Quantity2__c = 1.0 ;
        partsRequestLine.SVMXC__Line_Status__c = 'Open' ;
        partsRequestLine.SVMXC__Expected_Condition__c = 'Good/Working' ;
        partsRequestLine.SVMXC__Disposition__c = 'Repair' ;
        partsRequestLine.SVMXC__Expected_Receipt_Date__c = Date.today();
        //partsRequestLine.SVMXC__Line_Type__c='test';

        //partsRequestLine.SVMX_Item_Number_Blank_Check__c = true ;
        //partsRequestLine.SVMX_Awaiting_SAP_Response__c = false ;

        insert partsRequestLine;
    
       Test.StartTest();
        List<SVMXC__RMA_Shipment_Line__c> lstWOItem = [SELECT Id FROM SVMXC__RMA_Shipment_Line__c];
        SVMXC.SFM_WrapperDef.SFM_TargetRecord targetTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecord();
        List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject> detailRecordsList = new List<SVMXC.SFM_WrapperDef.SFM_TargetRecordObject>();


        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap1 = new SVMXC.SFM_WrapperDef.SFM_StringMap('SVMX_recordId',partsOrder.Id);       

        SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap2 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',partsOrder.Id);
        //SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',workDetails[0].Id);

        //SVMXC.INTF_WebServicesDef.INTF_StringMap tempStringMap3 = new SVMXC.INTF_WebServicesDef.INTF_StringMap('SVMXC__End_Date__c','2019-05-02');
        targetTemp.stringMap.add(tempStringMap1);
        
        targetTemp.headerRecord.objName = 'SVMXC__RMA_Shipment_Order__c';
        //targetTemp.headerRecord.pageLayoutId='a357A0000005qBtQAI';
        
        //targetTemp.detailRecords.objName = 'SVMXC__Service_Order_Line__c';
        
        SVMXC.SFM_WrapperDef.SFM_Record recordstemp = new SVMXC.SFM_WrapperDef.SFM_Record();
        //SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();

        recordstemp.targetRecordAsKeyValue.add(tempStringMap2);
        //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
        recordstemp.targetRecordId=partsOrder.Id;

        
        for(SVMXC__RMA_Shipment_Line__c wdl:lstWOItem){
            SVMXC.SFM_WrapperDef.SFM_Record recordstemp1 = new SVMXC.SFM_WrapperDef.SFM_Record();
            SVMXC.SFM_WrapperDef.SFM_StringMap tempStringMap3 = new SVMXC.SFM_WrapperDef.SFM_StringMap('ID',wdl.Id);
            SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
            recordstemp1.targetRecordAsKeyValue.add(tempStringMap3);
            //recordstemp.targetRecordAsKeyValue.add(tempStringMap3);
            recordstemp1.targetRecordId=wdl.Id;
            detailRecordTemp.records.add(recordstemp1);
            detailRecordTemp.objName = 'SVMXC__RMA_Shipment_Line__c';
            detailRecordsList.add(detailRecordTemp);
        }
        SVMXC.SFM_WrapperDef.SFM_TargetRecordObject detailRecordTemp1 = new SVMXC.SFM_WrapperDef.SFM_TargetRecordObject();
        detailRecordTemp1.aliasName='';
        //detailRecordTemp1.pageLayoutId='pageLayoutI';
        detailRecordTemp1.objName = 'SVMXC__RMA_Shipment_Line__c';
        detailRecordTemp1.parentColumnName = 'SVMXC__RMA_Shipment_Order__c';

        detailRecordsList.add(detailRecordTemp1);
        targetTemp.headerRecord.records.add(recordstemp);
        targetTemp.detailRecords = detailRecordsList;

        system.debug('calling web service pre');
        system.debug('targetTemp '+targetTemp);
        
        SVMXC.SFM_WrapperDef.SFM_PageData response = SVMX_GetPartsOrderPrice.getPartsOrderLinePrice(targetTemp);
        system.debug('calling web service post');
      
        Test.stopTest();
        
        
    } 
}