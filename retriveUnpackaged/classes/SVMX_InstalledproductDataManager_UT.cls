@isTest
public class SVMX_InstalledproductDataManager_UT {

    public static testMethod void checkIPQuery(){
	
    Project__c project = new Project__c();
    project.SVMX_Project_Delivered_on_Date__c = system.today();
    insert project ;
    Account account = SVMX_TestUtility.CreateAccount('Test Account','Norway',true) ;
    SVMXC__Site__c location = SVMX_TestUtility.CreateLocation('Product Location',account.id,true) ;
    
    SVMXC__Installed_Product__c installedProduct = SVMX_TestUtility.CreateInstalledProduct(account.id,null,location.id,true);
    installedProduct.SVMXC__Site__c = location.id ;
    installedProduct.Project__c = project.id ;    
    update installedProduct ; 
    
   
        List<SVMXC__Site__c> locationList = new List<SVMXC__Site__c>();
    	locationList.add(location);
        SVMX_InstalledproductDataManager.IPQuery(locationList);
        
        List<SVMXC__Installed_Product__c> ipList = new List<SVMXC__Installed_Product__c>();
        ipList.add(installedProduct);
        SVMX_InstalledproductDataManager.ProjectDateQuery(ipList);
    
    }
}