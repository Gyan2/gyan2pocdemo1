/*******************************************************************************************************
Description:
  This test class is used to test the SVMX_EventTrggrHandler class.
 
Author: Nivedita S
Date: 25-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/



@isTest
public class SVMX_EventTrggrHandler_UT {

static testMethod void invokeEventtrigger() {  
    SVMX_PS_TS_Timesheet_Settings__c ps = new SVMX_PS_TS_Timesheet_Settings__c ();
           ps.name='test2';
           ps.SVMX_PS_TS_Field_Type__c ='test';
           ps.SVMX_PS_TS_Text_Value__c ='test1';
           ps.SVMX_PS_TS_Value__c =true;
          insert ps;
    
  Profile profileId = [SELECT Id FROM Profile WHERE Name = 'SVMX - Global Technician' LIMIT 1];

      User usr = new User();
       usr.LastName = 'LIVESTON';
      usr.FirstName='JASON';
      usr.Alias = 'jliv';
      usr.Email = 'jason.liveston@asdf.com';
      usr.Username = 'test123000030@gmail.com';
      usr.ProfileId = profileId.Id;
      usr.TimeZoneSidKey = 'GMT';
      usr.LanguageLocaleKey = 'en_US';
      usr.EmailEncodingKey = 'UTF-8';
      usr.LocaleSidKey = 'en_US';
      usr.Country='India';
      usr.SalesOrg__c ='9999';
      usr.isActive=true;
     
      insert usr;  
    
Event ev = new Event();
  ev.StartDateTime =Datetime.newInstance(2017, 12, 7);
  ev.EndDateTime =Datetime.newInstance(2017, 12, 7);
  ev.OwnerId =usr.id;
  ev.SVMX_Event_Type__c ='Training';
  insert ev;
 ev.StartDateTime =Datetime.newInstance(2017, 12, 8);
 ev.EndDateTime=Datetime.newInstance(2017, 12, 8);
    
    update ev;
  ev.EndDateTime=Datetime.newInstance(2017, 12, 10);
    update ev;
   ev.SVMX_Event_Type__c='Training';
    update ev;
   ev.SVMX_Event_Type__c='Administrative';
    delete ev;
    
    
}

}