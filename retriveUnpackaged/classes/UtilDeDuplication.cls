/**
 * Created by rebmangu on 20/04/2018.
 */

public class UtilDeDuplication  // Uncomment if we need to use it, for not it's useless
{
//    /*
//    *******************************************************************
//    * Provides various methods for data deduplication
//    *******************************************************************
//    */
//    private MultiLingualText ml;
//    /*
//    *******************************************************************
//    * This takes some text and converts it to a search string
//    * Example:
//    * input:  'this is a test'
//    * output: '*this* OR *test*'
//    *******************************************************************
//    */
//    public string BuildSoqlQueryFromMultilingualText(string TextToParse)
//    {
//        string result = '';
//        ml =  new MultiLingualText(TextToParse);
//        result = this.searchTermsToSOQL (ml.GetAllSearchTerms());
//        return result;
//    }
//    /*
//    *******************************************************************
//    * This does a word count on search results for the purpose
//    * of optimisation
//    *******************************************************************
//    */
//    public list<string> MatchWordsFromQuery(list<sObject> QueryResult)
//    {
//        list<string> namesToMatch = new list<string>();
//        for (sObject so : QueryResult)
//        {
//            namesToMatch.add(String.valueOf(so.get('Name')).tolowercase());
//        }
//
//
//        ml.MatchWords(namesToMatch);
//        return ml.GetAllMatchedTerms();
//    }
//
//    /*
//   *******************************************************************
//   * Converts a list of words to a string
//   * that can be used in a wildcard search for each word
//   *******************************************************************
//   */
//    private string searchTermsToSOQL(list<string> searchTerms)
//    {
//        string result = '*';
//        for (string term : searchTerms)
//        {
//            // Search term must be longer than one character
//            if (term.length()> 1 && result.indexOf('*'+term+'*')==-1)
//            {
//                result = result + term  + '* OR *';
//            }
//        }
//        result= result.left(result.length()-5);
//        return result;
//    }
//
//
//    /*
//    *******************************************************************
//    * Contains a multi-lingual text broken down into sequential words
//    * or textual phrases.
//    * Each phrase is a continual set of characters or words
//    * of a given type or language.
//    * The phrases can then be combined into query search terms.
//    *******************************************************************
//    */
//
//    private class MultilingualText
//    {
//        //parameters
//        private string stop_words_latin;
//        private string special_characters_and_delimiters;
//        private integer largest_chineese_word;
//        private integer minimum_word_count_exclusion;
//        public integer maximum_word_count_exclusion;
//        // Regular expression to pick up chineese characters
//        private Pattern CNPat = Pattern.compile('\\p{script=Han}'); // filter for Chineese Characters
//        //Regurlar expresion to pick up standard Latin alpha characters
//        private Pattern LNPat =  Pattern.compile('\\p{script=Latin}'); // filter for English words
//        //Regular expression to pick up standard numeric characters
//        private Pattern NMPat = Pattern.compile('^(0|[1-9][0-9]*)$'); //filter for numbers
//
//        //variables
//        private string fullText; // This text is set in the constructor and represents a block of text to be parsed.
//        private list<IPhrase> phrases; // A sequential collection of prhases. In English, each  phrase represents a series of words separated by spaces.
//        private integer phraseCounter;// The number of different blocks of texts that the full text is broken into.
//        // The stopCharacters are special characters that will not be stored in the text blocks but rather
//        // these characters act as string delimiters between each block of text.
//
//
//        // Constructor
//        public MultilingualText(string TextToParse)
//        {
//            // Load custom setting parameters to optimise data deduplication process
//
//            Map<string,Deduplication__mdt> params = new Map<String,Deduplication__mdt>();
//            for(Deduplication__mdt dedu : [select id,Label,Value__c from Deduplication__mdt]){
//                params.put(dedu.Label,dedu);
//            }
//            // This is a list of words which will not be used in the search query string. Words like "The" "is" "For" as examples.
//            this.stop_words_latin =  params.get('stop_words_latin').value__c;
//            // These special characters and delimeters will be ignored (not included) in the resulting texts
//            this.special_characters_and_delimiters = params.get('special_characters_and_delimiters').value__c;
//            // For chineese characters, this is the largest sequential set of characters that would be considered as a single word.
//            // A large number (5 or 6) will return very specific resulting search terms but requires more processing. Likely to miss some relevant results.
//            // A small number (2 or 3) will return more general search terms and requre less processing. Likely to return more irrelevant results.
//            this.largest_chineese_word = integer.valueof(params.get('largest_chineese_word').value__c);
//            // The minimum number of times a word needs to be found in a search result in order to be considered a "key word".
//            // Set this value to 2 if the term you are looking for is also in the search set, otherwise set it to 1.
//            this.minimum_word_count_exclusion = integer.valueof(params.get('minimum_word_count_exclusion').value__c);
//            // This is the maximum number of times a word can occur in a search result before it is no longer considered a key word.
//            // a low value (3 or 4) will result in a short list of possible matches.
//            // a large value (10 or 20) will result in a longer list of possible matches.
//            this.maximum_word_count_exclusion = integer.valueof(params.get('maximum_word_count_exclusion').value__c);
//            // Set method parameters
//            this.fullText = TextToParse;
//            this.parseMultilingualText();
//
//        }
//
//
//        public void MatchWords(list<string> WordsToMatch)
//        {
//            for (IPhrase nextPhrase : phrases)
//            {
//                nextPhrase.MatchWords(WordsToMatch);
//            }
//        }
//
//        //Get all the possible individual search terms found in the full text.
//        public list<string> GetAllSearchTerms()
//        {
//            list<string> result = new list<string>();
//            for (IPhrase ph: this.phrases)
//            {
//                result.AddAll(ph.getSearchTerms());
//            }
//            return result;
//        }
//
//        public list<string> GetAllMatchedTerms()
//        {
//            list<string> result = new list<string>();
//            for (IPhrase ph: this.phrases)
//            {
//                result.AddAll(ph.GetMatchedTerms(this.minimum_word_count_exclusion,this.maximum_word_count_exclusion));
//            }
//            return result;
//        }
//
//        //Parses text into a structured MultilingualText class
//        //which stores the text into small blocks of single words or
//        //continuous blocks of chineese characters.
//
//        private void parseMultilingualText()
//        {
//            this.phrases = new list <IPhrase>();
//            this.phraseCounter = 0;
//            //Counters and placeholders for looping through texts
//            string currentWord ='';
//            string nextMode ='';
//            string currentMode ='';
//            string c= '';
//
//            // step through each character in the string and extract out the relevant text blocks.
//            for (integer i = 0; i < fullText.length(); i++)
//            {
//                c = fullText.substring(i,i+1);
//                Matcher CNMat = CNPat.matcher(c);  // determines if next character is a Chineese character
//                Matcher LNMat = LNPat.matcher(c); // determines if next character is a Latan Based character
//                Matcher NMMat = NMPat.matcher(c); // determines if next character is a number
//                if (special_characters_and_delimiters.indexOf(c)>-1) // Special characters are disregarded apart from separating text into phrases
//                {
//                    nextMode = 'Special'; // we are in a block of special characters
//                    c='';
//                }
//                else if (CNMat.matches())
//                {
//                    nextMode ='CN'; // we are in a block of chineese characters
//                }
//                else if (LNMat.matches() || c==' ' || NMMat.matches())
//                {
//                    nextMode='LN'; // we are in a block of latan characters.
//                }
//                else
//                {
//                    nextMode = 'UNKNOWN';
//                    c='';
//                }
//                // if we are in a current block of text, just continue to build up our text block placeholder
//                if (nextMode == currentMode )
//                {
//                    currentWord = currentWord + c;
//                }
//                // If we are entering into a different kind of text block, then we need to do something with the previous block of text (placeholder)
//                else
//                {
//                    // if the previous block of texts was Chineese or Latin then add the block of text into our parser
//                    this.addPhrase(currentWord,currentMode);
//                    currentWord = c;
//                    currentMode = nextMode;
//                }
//                // have we reached the end of our text to parse?
//                if (i+1==fullText.length())
//                {
//                    if (currentMode == 'LN'  || currentMode =='CN')
//                    {
//                        this.addPhrase(currentWord,currentMode);
//                    }
//                }
//            }
//        }
//
//        // Adds a complete phrase to the list of phrases in the full text.
//        private void addPhrase(string phrase, string mode)
//        {
//            if (mode == 'LN')
//            {
//                this.phrases.add(new LatinPhrase(phrase,phraseCounter++,stop_words_latin));
//
//            }
//            else if (mode =='CN')
//            {
//                this.phrases.add(new ChineesePhrase(phrase,phraseCounter++,largest_chineese_word));
//            }
//        }
//
//
//    }
//
//
//    /*
//   *******************************************************************
//   * Defines an interface for containing a phrase of various
//   * types or in different languages. The way in which words
//   * or characters are represented and managed are implimented
//   * differently between the various languages.
//   *******************************************************************
//   */
//    public interface IPhrase
//    {
//        list<string> GetSearchTerms();
//        list<string> GetMatchedTerms(integer MinCount, integer MaxCount);
//        void MatchWords(list<string> WordsToMatch);
//    }
//
//    /*
//    *******************************************************************
//    * Contains a single phrase based on Latin characters
//    * A phrase in this context is a series of words
//    * with each word separated by a single space
//    *******************************************************************
//    */
//    private class LatinPhrase implements IPhrase
//    {
//        private integer sortOrder;
//        private string text;
//        private Map<string,integer> wordCount;
//        // stopWords is a list of english words that will not be included in search terms.
//        // Generally you do not want to include these stop words in a search string.
//        private string stopWords;
//        // Constructor
//        public LatinPhrase (string Text, integer SortOrder, string StopWords)
//        {
//            // remove extra spaces
//            this.text = Text.replaceAll('s/ +/ /g',' ');
//            this.sortOrder = sortOrder;
//            this.stopWords = StopWords;
//        }
//
//        // Returns the possible words that are contained in this block of text.
//        public list<string> GetSearchTerms()
//        {
//            list<string> result = new list<string>();
//            for (string nextWord : this.text.split(' '))
//            {
//                string lc = nextWord.toLowerCase();
//                system.debug(Logginglevel.ERROR,'lower case result '+lc);
//                if (stopWords.indexOf(lc)==-1)
//                {
//                    result.add(lc);
//                }
//            }
//            return result;
//        }
//
//        public list<string> GetMatchedTerms(integer MinCount, integer MaxCount)
//        {
//            list<string> result = new list<string>();
//            if (this.wordCount != null)
//            {
//                for (string nextWord : wordCount.keyset())
//                {
//                    integer cnt = wordCount.get(nextWord);
//                    if (cnt>=MinCount && cnt<MaxCount)
//                    {
//                        result.add(nextWord);
//                    }
//                }
//            }
//            return result;
//        }
//
//        public void MatchWords(list<string> WordsToMatch)
//        {
//            this.clearWordCount();
//            for (string nextWord : this.getSearchTerms())
//            {
//                this.wordCount.put(nextWord,0);
//            }
//
//            for (string nextWordToMatch : WordsToMatch)
//            {
//                for (string nextWord : wordCount.keyset())
//                {
//                    if (nextWordToMatch.indexOf(nextWord)>-1)
//                    {
//                        wordCount.put(nextWord,wordCount.get(nextWord)+1);
//                    }
//                }
//            }
//
//            for (string nextWord : wordCount.keyset())
//            {
//                system.debug(Logginglevel.ERROR,'**********************************word '+nextWord + ' count '+wordCount.get(nextWord));
//            }
//        }
//
//        private void clearWordCount()
//        {
//            this.wordCount = new Map<string,integer>();
//            for(string nextWord: this.getSearchTerms())
//            {
//                wordCount.put(nextWord,0);
//            }
//        }
//
//    }
//
//    /*
//    *******************************************************************
//    * Contains a continuous block of chineese characters which may be
//    * composed of one or more 'words'
//    *******************************************************************
//    */
//    private class ChineesePhrase implements IPhrase
//    {
//        private integer sortOrder;
//        private string text;
//        private integer maximumWordLength;
//        private Map<string,integer> wordCount;
//
//        // Constructor
//        public ChineesePhrase(string text,integer sortOrder, integer MaximumWordLength)
//        {
//            // remove all spaces
//            this.text = text.replaceAll('s/ +/ /g','');
//            this.sortOrder = sortOrder;
//            this.maximumWordLength = MaximumWordLength;
//        }
//
//        // Returns the possible search terms that are contained in this block of text.
//        public list<string> GetSearchTerms()
//        {
//            return this.getCharacterCombinations(2);
//        }
//
//        // Returns all possible (chineese) character combinations in groups of wordLength
//        // In this context, the word length is the number of chineese characters
//        // that may represent a single chineese word.
//        private list<string> getCharacterCombinations(integer wordLength)
//        {
//            list<string> result = new list<string>();
//            for (integer offset=0; offset<wordLength; offset++)
//            {
//                boolean EOF = false;
//                integer pointer = offset;
//                While (EOF==false)
//                {
//                    if (pointer+wordLength>this.text.length())
//                    {
//                        EOF = true;
//                    }
//                    else
//                    {
//                        result.add(this.text.substring(pointer,pointer+wordLength));
//                        pointer = pointer + wordLength;
//
//                    }
//                }
//            }
//            Return result;
//        }
//
//        public void MatchWords(list<string> WordsToMatch)
//        {
//            this.clearWordCount();
//            string foundWords = '';
//            for (string nextWordToMatch : WordsToMatch)
//            {
//                foundWords = '';
//                for (string nextWord : wordCount.keyset())
//                {
//                    if (nextWordToMatch.indexOf(nextWord)>-1)
//                    {
//                        if (foundWords.indexOf(nextWord)==-1)
//                        {
//                            foundWords = foundWords +' '+ nextWord;
//                            wordCount.put(nextWord,wordCount.get(nextWord)+1);
//                        }
//                    }
//                }
//            }
//
//            for (string nextWord : wordCount.keyset())
//            {
//                system.debug(Logginglevel.ERROR,'*********************************word '+nextWord + ' count '+wordCount.get(nextWord));
//            }
//
//        }
//
//        public list<string> GetMatchedTerms(integer MinCount, integer MaxCount)
//        {
//            list<string> result = new list<string>();
//            if (this.wordCount != null)
//            {
//                for (string nextWord : wordCount.keyset())
//                {
//                    if (nextWord.length()>1)
//                    {
//                        integer cnt = wordCount.get(nextWord);
//                        if (cnt>=MinCount && cnt<=MaxCount)
//                        {
//                            boolean addedHigher = false;
//                            for (string added : result)
//                            {
//                                if (added.indexOf(nextWord)>-1)
//                                {
//                                    addedHigher = true;
//                                    break;
//                                }
//                            }
//                            if (!addedHigher)
//                            {
//                                result.add(nextWord);
//                            }
//                        }
//                    }
//                }
//            }
//            return result;
//        }
//
//        private void clearWordCount()
//        {
//            this.wordCount = new Map<string,integer>();
//            integer longestWord = this.text.length();
//            if (longestWord >maximumWordLength) longestWord = maximumWordLength;
//            for (integer f = longestWord; f>0; f--)
//            {
//                for (string nextWord : this.getCharacterCombinations(f))
//                {
//                    wordCount.put(nextWord,0);
//                }
//            }
//        }
//    }
}