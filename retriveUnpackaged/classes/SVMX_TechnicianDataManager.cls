/************************************************************************************************************
Description: Data Manager for Technician/Equipment. All the SOQL queries and DML Operations are performed in this class.

Dependency: 
 
Author: Ranjitha S
Date: 24-10-2017

Modification Log: 
Date            Author          Modification Comments

--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_TechnicianDataManager {

    public static id technicianQuery(){
        list<SVMXC__Service_Group_Members__c> techs = new List<SVMXC__Service_Group_Members__c>();
        techs = [select id from SVMXC__Service_Group_Members__c where SVMXC__Salesforce_User__c =: userInfo.getUserId()];
        if(!techs.isEmpty())
        	return techs[0].id;
        else
            return null;
    }
}