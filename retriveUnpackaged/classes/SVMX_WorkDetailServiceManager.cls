/************************************************************************************************************
Description: Service Manager for Work Detail. All the methods and the logic for Work Detail object resides here.

Dependancy: 
    Data Manager:SVMX_ServiceContractDataManager
                 SVMX_WorkDetailDataManager
    
Author: Ranjitha S
Date: 19-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
10-11-17        Ranjitha        Sprint2 Changes for misuse and abuse
9-Nov-2017      Pooja Singh     Added wdTravelQtyUpdate method
****************************************************************************************************************/
public class SVMX_WorkDetailServiceManager {

    public static String laborRate= Label.Hourly_Charge;
    public static String CalloutCharge1 = Label.TECH_1;
    public static String CalloutCharge2 = Label.TECH_2;
    public static String workComplete = Label.Work_Complete;

    public static void addTechnician(list<SVMXC__Service_Order_Line__c> WDList){
        
        id tech = SVMX_TechnicianDataManager.technicianQuery();
        for(SVMXC__Service_Order_Line__c wl: WDList){
            wl.SVMXC__Group_Member__c = tech;
        }
    } 
    
    public static void addExpenseMarkup(list<SVMXC__Service_Order_Line__c> expenseLineList){
    
        List<SVMXC__Expense_Pricing__c> expensePricingList = SVMX_WorkDetailDataManager.ExpenseMarkupQuery(expenseLineList);
        Map<id,Map<string,SVMXC__Expense_Pricing__c>> contractExpensePricMap = new Map<id,Map<string,SVMXC__Expense_Pricing__c>>();
        for(SVMXC__Expense_Pricing__c ep: expensePricingList){
            Map<string,SVMXC__Expense_Pricing__c> tempMap = new Map<string,SVMXC__Expense_Pricing__c>();
            String expType = ep.SVMXC__Expense_Type__c;
            if(contractExpensePricMap.containsKey(ep.SVMXC__Service_Contract__c)){
                tempMap = contractExpensePricMap.get(ep.SVMXC__Service_Contract__c);
            }
            tempMap.put(expType,ep);
            contractExpensePricMap.put(ep.SVMXC__Service_Contract__c,tempMap);
        }
        
        for(SVMXC__Service_Order_Line__c wl: expenseLineList){
            if(contractExpensePricMap.containskey(wl.SVMX_Service_Contract__c)){
                if(contractExpensePricMap.get(wl.SVMX_Service_Contract__c).containsKey(wl.SVMXC__Expense_Type__c)){
                    if(contractExpensePricMap.get(wl.SVMX_Service_Contract__c).get(wl.SVMXC__Expense_Type__c).SVMXC__Rate__c != null)
                            wl.SVMX_Surcharge__c = contractExpensePricMap.get(wl.SVMX_Service_Contract__c).get(wl.SVMXC__Expense_Type__c).SVMXC__Rate__c;
                }
            }
            else
                wl.SVMX_Surcharge__c = 0;                
        }
    } 
    

    public static void updateWarrantyDiscount(map<id,List<SVMXC__Service_Order_Line__c>> WDList2){
        
        map<id,SVMXC__Service_Order__c> warrMap = new map<id,SVMXC__Service_Order__c>();
        
        warrMap = SVMX_WorkOrderDataManager.warrantyQuery(WDList2);
        system.debug('warrMap '+warrMap);
        if(!warrMap.isEmpty()){

            for(SVMXC__Service_Order__c wo: warrMap.values()){
                List<SVMXC__Service_Order_Line__c> wdList = WDList2.get(wo.id);

                for(SVMXC__Service_Order_Line__c wl: wdList){
                    system.debug('wl in update warranty '+wl);
                    if(wl.SVMXC__Line_Type__c == System.Label.Labor ){
                        wl.SVMXC__Discount__c = warrMap.get(wl.SVMXC__Service_Order__c).SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Time_Covered__c; 
                        system.debug('labor discount '+warrMap.get(wl.SVMXC__Service_Order__c).SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Time_Covered__c);
                    }
                    else if(wl.SVMXC__Line_Type__c == System.Label.Parts ){
                        wl.SVMXC__Discount__c = warrMap.get(wl.SVMXC__Service_Order__c).SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Material_Covered__c;              
                        system.debug('parts discount '+warrMap.get(wl.SVMXC__Service_Order__c).SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Material_Covered__c);
                    }
                    else if(wl.SVMXC__Line_Type__c == System.Label.Expenses){
                        wl.SVMXC__Discount__c = warrMap.get(wl.SVMXC__Service_Order__c).SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Expenses_Covered__c;              
                        system.debug('expense discount '+warrMap.get(wl.SVMXC__Service_Order__c).SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Expenses_Covered__c);
                    }
                    else if(wl.SVMXC__Line_Type__c == System.Label.Travel){
                        wl.SVMXC__Discount__c = warrMap.get(wl.SVMXC__Service_Order__c).SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Travel_Covered__c;              
                        system.debug('expense discount '+warrMap.get(wl.SVMXC__Service_Order__c).SVMXC__Case__r.SVMXC__Warranty__r.SVMXC__Travel_Covered__c);
                    }
                }

            }
        }
    } 
    
    public static Map<Id,SVMXC__Service_Order_Line__c> SetLaborActivityTypes(map<id,SVMXC__Service_Order_Line__c> woli,id usageRecordTypeId, id estimateRecordTypeId) {
        
        set<id> serviceContractId = new set<id>();
        map<id,SVMXC__Service_Contract__c> serviceConMap = new map<id,SVMXC__Service_Contract__c>();
        map<id,list<SVMX_Rate_Tiers__c>> rateTierMap = new map<id,list<SVMX_Rate_Tiers__c>>();
        Map<Id,SVMXC__Service_Order_Line__c> UpWDLines = new Map<Id,SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> InsWDLines = new List<SVMXC__Service_Order_Line__c>();
        
        for(SVMXC__Service_Order_Line__c ol: woli.values()){
            serviceContractId.add(ol.SVMXC__Service_Order__r.SVMXC__Service_Contract__c);
        }  
        
        serviceConMap = SVMX_ServiceContractDataManager.rateTierQuery(serviceContractId);
        
        for(SVMXC__Service_Contract__c sc: serviceConMap.values()){
            if(sc.Rate_Tiers__r.size()>0)
                rateTierMap.put(sc.id,sc.Rate_Tiers__r);
        }
        
        if(!rateTierMap.isEmpty()){                       
        for(SVMXC__Service_Order_Line__c wl : woli.values()){ 
            String AcRateTier = '';
            String oohTier ='';
            Decimal NoOfTechs;
            
            for(SVMX_Rate_Tiers__c rateTier: rateTierMap.get(wl.SVMXC__Service_Order__r.SVMXC__Service_Contract__c)) {
                if(wl.RecordTypeId == usageRecordTypeId){
                    if(rateTier.SVMX_Business_Hours__c != null && wl.SVMXC__Start_Date_and_Time__c != null){
                        Boolean isWithin = BusinessHours.isWithin(rateTier.SVMX_Business_Hours__c, wl.SVMXC__Start_Date_and_Time__c);
                        if (isWithin && rateTier.SVMX_Rate__c != null) {
                            AcRateTier = rateTier.SVMX_Rate__c; 
                            break;
                        }
                    }
                }

                if(rateTier.SVMX_Out_Of_Hours__c  == true)
                    oohTier = rateTier.SVMX_Rate__c; 
            }

            if(wl.RecordTypeId == usageRecordTypeId){ 
                NoOfTechs = 1;
                if(AcRateTier == null || AcRateTier =='' )
                    AcRateTier  = oohTier; 
            }
            else if(wl.RecordTypeId == estimateRecordTypeId){
                NoOfTechs = wl.SVMX_No_of_Technicians__c;
                if(wl.SVMX_Out_Of_Hours__c)
                    AcRateTier = oohTier;
                else
                    AcRateTier = 'Rate 1';
            }

                //Activity types
            if(wl.RecordTypeId == usageRecordTypeId){ 
                SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
                wd.SVMXC__Activity_Type__c = laborRate+AcRateTier;
                wd.id = wl.id;
                UpWDLines.put(wd.id,wd);
            }
            else if(wl.RecordTypeId == estimateRecordTypeId && trigger.isInsert){
                for(integer i=1; i<= NoOfTechs; i++){
                    SVMXC__Service_Order_Line__c wd2 = new SVMXC__Service_Order_Line__c();
                    wd2.RecordTypeId = wl.RecordTypeId;
                    wd2.SVMXC__Estimated_Quantity2__c = wl.SVMX_No_of_Hours_Onsite__c;
                    wd2.SVMXC__Line_Type__c = wl.SVMXC__Line_Type__c;
                    wd2.SVMXC__Service_Order__c = wl.SVMXC__Service_Order__c;
                    wd2.SVMXC__Group_Member__c = wl.SVMXC__Group_Member__c;
                    wd2.SVMXC__Activity_Type__c = laborRate+AcRateTier;
                    wd2.SVMX_Out_Of_Hours__c = wl.SVMX_Out_Of_Hours__c;
                    wd2.SVMXC__Serial_Number__c = wl.SVMXC__Serial_Number__c;
                    InsWDLines.add(wd2);
                }
            }
            
            //Callout Charge
            if(trigger.isInsert){
                for(integer i=1; i<= NoOfTechs; i++){ 
                    SVMXC__Service_Order_Line__c wd2 = new SVMXC__Service_Order_Line__c();
                    wd2.RecordTypeId = wl.RecordTypeId;
                    wd2.SVMXC__Line_Type__c = wl.SVMXC__Line_Type__c;
                    wd2.SVMXC__Service_Order__c = wl.SVMXC__Service_Order__c;
                    wd2.SVMXC__Group_Member__c = wl.SVMXC__Group_Member__c;
                    if(wl.RecordTypeId == usageRecordTypeId){ 
                        wd2.SVMXC__Actual_Quantity2__c = 1;
                        wd2.SVMX_Billing_Type__c = wl.SVMX_Billing_Type__c;
                        if(wl.SVMXC__Group_Member__c == wl.SVMXC__Service_Order__r.SVMXC__Group_Member__c ){ 
                            wd2.SVMXC__Activity_Type__c = CalloutCharge1+' '+AcRateTier;
                        }
                        else{ 
                            wd2.SVMXC__Activity_Type__c = CalloutCharge2+' '+AcRateTier;    
                        }
                    }
                    else if(wl.RecordTypeId == estimateRecordTypeId){
                        wd2.SVMXC__Estimated_Quantity2__c = 1;
                        wd2.SVMX_Out_Of_Hours__c = wl.SVMX_Out_Of_Hours__c;
                        wd2.SVMXC__Serial_Number__c = wl.SVMXC__Serial_Number__c;
                        if(i==1)
                            wd2.SVMXC__Activity_Type__c = CalloutCharge1+' '+AcRateTier;
                        else
                            wd2.SVMXC__Activity_Type__c = CalloutCharge2+' '+AcRateTier;
                    }
                    InsWDLines.add(wd2);
                }
            }
        }
        }

        if(InsWDLines.size() > 0)
            SVMX_WorkDetailDataManager.InsWorkDetails(InsWDLines);

        return UpWDLines;
    }

    public static void wdTravelQtyUpdate(List<SVMXC__Service_Order_Line__c> wdList) {
        List<SVMXC__Service_Order_Line__c> wdListUpdate = new List<SVMXC__Service_Order_Line__c>();
        Map<Id,SVMXC__Service_Order_Line__c> wdMapQuery = SVMX_WorkDetailDataManager.workDetailQuery(wdList);
        for(SVMXC__Service_Order_Line__c wl : wdMapQuery.values()){

            Long dt1Long = wl.SVMXC__Start_Date_and_Time__c.getTime();
            Long dt2Long = wl.SVMXC__End_Date_and_Time__c.getTime();
            Long milliseconds = dt2Long - dt1Long;
            Long seconds = milliseconds / 1000;
            Long minutes = seconds / 60;
            system.debug('minutes--->'+minutes);
            Long hours = minutes / 60;
            system.debug('hours--->'+hours);
            //Long days = hours / 24;
            String billQtyString = String.valueOf(hours)+'.'+String.valueOf(minutes);
            system.debug('billQtyString--->'+billQtyString);
            Decimal toRound = Decimal.valueOf(billQtyString);
            Decimal billQtyDecimal = toRound.setScale(2);

            wl.SVMXC__Billable_Quantity__c = billQtyDecimal;
            system.debug('wl.SVMXC__Billable_Quantity__c--->'+wl.SVMXC__Billable_Quantity__c);
            wdListUpdate.add(wl);
            system.debug('wdListUpdate--->'+wdListUpdate);
        }
        if(wdListUpdate.size() > 0)
            SVMX_WorkDetailDataManager.UpdaWorkDetails(wdListUpdate);
    }  
    
    public static void setWorkComplete(Map<id,List<SVMXC__Service_Order_Line__c>> newWOLaborLinesMap, set<id> laborLinesid){
        
        List<SVMXC__Service_Order__c> updateWOList = new List<SVMXC__Service_Order__c>();
        Map<id,List<SVMXC__Service_Order_Line__c>> allLaborLinesMap = new  Map<id,List<SVMXC__Service_Order_Line__c>>();
        
        allLaborLinesMap = SVMX_WorkDetailDataManager.laborLinesQuery(newWOLaborLinesMap,laborLinesid);
        integer count1 =0, count2 = 0;
        for(id w : allLaborLinesMap.keySet()){
            for(SVMXC__Service_Order_Line__c wl: allLaborLinesMap.get(w)){
                if(wl.SVMXC__End_Date_and_Time__c != null)
                    count1++;
                else
                    count2++;
            }
            if(count2 == 0 && count1 != 0){
                SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
                wo.SVMXC__Order_Status__c = workComplete;
                wo.id = w;
                updateWOList.add(wo);
            }
        }
        
        if(!updateWOList.isEmpty()){
            SVMX_WorkOrderDataManager.updateWO(updateWOList);
        }
    }

    @future (callout=true)
    public static  void getPricefromSAP(set<Id> wlines){
        set<Id> productIds =new set<Id>();
        String country =null;
        String wlcurrency = null;
        List<SVMXC__Service_Order_Line__c> wlList = new List<SVMXC__Service_Order_Line__c>();
        id estimateId = SVMX_RecordTypeDataManager.GetRecordTypeID('Estimate','SVMXC__Service_Order_Line__c ');

          Map<Id,SVMXC__Service_Order_Line__c> workdetails = new Map<Id,SVMXC__Service_Order_Line__c>([select Id,REcordTypeId, SVMXC__Estimated_Quantity2__c,SVMXC__Actual_Quantity2__c,SVMX_Country__c,CurrencyISOCode,SVMXC__Product__c,SVMXC__Service_Order__r.SVMXC__Company__r.SAPNumber__c from SVMXC__Service_Order_Line__c where Id=:wlines ]);     

        for(SVMXC__Service_Order_Line__c wl :workdetails.values()){
            productIds.add(wl.SVMXC__Product__c);
            country = wl.SVMX_Country__c;
            wlcurrency = wl.CurrencyISOCode;
        }

        Map<Id,Product2> productMap = new MAp<Id,Product2>([SELECT Name ,Id,SAPNumber__c,QuantityUnitOfMeasure, ExternalId__c FROM Product2 WHERE Id IN :productIds]);
      

        Map<String,SVMXC__Service_Order_Line__c> productWithPriceMap = new Map<String,SVMXC__Service_Order_Line__c>();
          //create input
       // SVMX_Sales_Office__c so =[select Id,SVMX_Price_Order_Account__c,SVMX_Price_Order_Account__r.SAPNumber__c from SVMX_Sales_Office__c where SVMX_Country__c=:country];
                    Account acc = new Account();
                    acc.SAPNumber__c= workdetails.values()[0].SVMXC__Service_Order__r.SVMXC__Company__r.SAPNumber__c;
                    acc.Name= 'test account';
                    acc.CurrencyISOCode=wlcurrency;

                    Quote q = new Quote();
                    q.Name= 'Test';
                    q.ScheduledDate__c =system.today();
                    q.CurrencyISOCode = wlcurrency;
                    q.GLN__c ='9921226005491';
                    q.ExternalQuoteNumber__c ='Dummy number';

                    List<QuoteLineItem> qList = new List<QuoteLineItem>();
                    for(Id prd :productMap.keyset()){
                            QuoteLineItem ql1 = new QuoteLineItem(Quantity=1,Product2=productMap.get(prd));
                            qList.add(ql1);
                    }


                    SAPPO_SalesOrderService.SalesOrderSimulationWrapper wrapper = new SAPPO_SalesOrderService.SalesOrderSimulationWrapper();
                    wrapper.account= acc;
                    wrapper.quote = q;
                    wrapper.quoteLineItems = qList;

                    SAPPO_SalesOrderService priceService = new SAPPO_SalesOrderService();
                    List<Map<String,String>> results=  new List<Map<String,String>>();
                    try{
                        results = priceService.syncSimulatedSalesOrder(wrapper,true).data;
                    }catch(Exception e){
                        System.debug('Error in SAP '+e);
                    }
                if(results != null && results.size() > 0){
                    for(Map<String,String> rs:results){
                           SVMXC__Service_Order_Line__c wl = new SVMXC__Service_Order_Line__c();
                            Decimal price = null;
                            String MaterialId = rs.get('MaterialId');
                            String NetUnitPrice =rs.get('NetUnitPrice');
                            String UnitPrice =rs.get('UnitPrice');
                            Decimal BaseDiscount = rs.get('BaseDiscount') != null ? Decimal.valueOf(rs.get('BaseDiscount')): 0 ;
                            Decimal AdditionalDiscount =rs.get('AdditionalDiscount') != null ? Decimal.valueOf(rs.get('AdditionalDiscount')):0 ;
                            Decimal SpecialDiscount =rs.get('SpecialDiscount') != null ? Decimal.valueOf(rs.get('SpecialDiscount')):0 ;

                            //Decimal discount = BaseDiscount +AdditionalDiscount+SpecialDiscount;
                            
                           if(BaseDiscount != null)
                                 wl.SVMXC__Discount__c =Math.abs(BaseDiscount);
                            else
                                wl.SVMXC__Discount__c =0;

                             if(AdditionalDiscount != null)
                                wl.SVMX_Additional_Discount__c= Math.abs(AdditionalDiscount);
                            else
                                wl.SVMX_Additional_Discount__c=0;

                            //if(NetUnitPrice != null)
                                //price =Decimal.valueOf(NetUnitPrice);
                             if(UnitPrice != null)
                                price =Decimal.valueOf(UnitPrice);

                             wl.SVMXC__Actual_Price2__c = price;

                            productWithPriceMap.put(MaterialId,wl);

                    }
                }

                    for(SVMXC__Service_Order_Line__c wl : workdetails.values()){

                           Product2 prd = productMap.get(wl.SVMXC__Product__c);
                           String sapnum = prd.SAPNumber__c;
                                      

                           if(prd !=null  && productWithPriceMap.size() > 0  && productWithPriceMap.containsKey(sapnum)  &&  productWithPriceMap.get(sapnum) != null){
                                    SVMXC__Service_Order_Line__c temp = productWithPriceMap.get(sapnum);
                                    SVMXC__Service_Order_Line__c wol =new SVMXC__Service_Order_Line__c(Id=wl.id);


                                    if(temp.SVMXC__Discount__c > 0 && temp.SVMXC__Actual_Price2__c > 0 && wl.SVMXC__Actual_Quantity2__c >0){
                                        wol.SVMXC__Billable_Line_Price__c= (temp.SVMXC__Actual_Price2__c * wl.SVMXC__Actual_Quantity2__c - ((temp.SVMXC__Discount__c/100) *(temp.SVMXC__Actual_Price2__c * wl.SVMXC__Actual_Quantity2__c) ));
                                    }else if(wl.SVMXC__Actual_Quantity2__c >0){
                                        wol.SVMXC__Billable_Line_Price__c = temp.SVMXC__Actual_Price2__c * wl.SVMXC__Actual_Quantity2__c;
                                    }

                                     if(temp.SVMXC__Discount__c >0  && temp.SVMXC__Actual_Price2__c > 0 && wl.SVMXC__Estimated_Quantity2__c >0){
                                        wol.SVMXC__Billable_Line_Price__c= (temp.SVMXC__Actual_Price2__c * wl.SVMXC__Estimated_Quantity2__c - ((temp.SVMXC__Discount__c/100) *(temp.SVMXC__Actual_Price2__c * wl.SVMXC__Estimated_Quantity2__c) ));
                                    }else if(wl.SVMXC__Estimated_Quantity2__c >0){
                                        wol.SVMXC__Billable_Line_Price__c = temp.SVMXC__Actual_Price2__c * wl.SVMXC__Estimated_Quantity2__c;
                                    }
                                    
                                     if(wl.RecordTypeId != estimateId){
                                        wol.SVMXC__Actual_Price2__c = temp.SVMXC__Actual_Price2__c;
                                         wol.SVMXC__Billable_Quantity__c = wl.SVMXC__Actual_Quantity2__c;
                                     }else{
                                         wol.SVMXC__Estimated_Price2__c = temp.SVMXC__Actual_Price2__c;
                                          wol.SVMXC__Billable_Quantity__c = wl.SVMXC__Estimated_Quantity2__c;
                                     }


                                    wol.SVMXC__Discount__c = temp.SVMXC__Discount__c;
                                    wol.SVMX_Additional_Discount__c = temp.SVMX_Additional_Discount__c;
                                    wlList.add(wol);
                            }else{
                                            system.debug('no value from SAP ');
                            }
                                
                            
                    }
            

                if(wlList.size() > 0)
                    SVMX_WorkDetailDataManager.UpdaWorkDetails(wlList);

    }

    //Direct creation of Parts ORder and Parts Order Line for SPare Parts Order types
    public static void createPartsOrder(List<SVMXC__Service_Order_Line__c> wlList){
        id shipRecId = SVMX_RecordTypeDataManager.GetRecordTypeID('Shipment','SVMXC__RMA_Shipment_Order__c');
        id shipRecTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Shipment','SVMXC__RMA_Shipment_Line__c');
        List<SVMXC__RMA_Shipment_Order__c> Porder = new List<SVMXC__RMA_Shipment_Order__c>();
        List<SVMXC__RMA_Shipment_Line__c> partOrLineList= new List<SVMXC__RMA_Shipment_Line__c>();
        set<id> woIds = new set<Id>();

        for(SVMXC__Service_Order_Line__c wl: wlList)
            woIds.add(wl.SVMXC__Service_Order__c);

        if(woIds.size() > 0){
            Map<Id,SVMXC__Service_Order__c> woList = new Map<Id,SVMXC__Service_Order__c> ([select id,SVMXC__Site__c,SVMX_Language__c,SVMXC__City__c,SVMXC__Country__c,SVMXC__State__c,SVMXC__Zip__c,SVMXC__Street__c,SVMX_From_SAP_Storage_Location__c from SVMXC__Service_Order__c where id IN: woIds ]);
        
            for(SVMXC__Service_Order__c wo:woList.values() ){
                    SVMXC__RMA_Shipment_Order__c prtOrder = new SVMXC__RMA_Shipment_Order__c();
                    prtOrder.SVMXC__Destination_Location__c = wo.SVMXC__Site__c;
                    prtOrder.SVMX_Shipping_Location__c = wo.SVMXC__Site__c;
                    prtOrder.RecordTypeId = shipRecId;
                    prtOrder.Language__c = wo.SVMX_Language__c ;
                    prtOrder.SVMXC__Source_Location__c = wo.SVMX_From_SAP_Storage_Location__c;
                    prtOrder.SVMXC__Destination_City__c = wo.SVMXC__City__c;
                    prtOrder.SVMXC__Destination_Country__c = wo.SVMXC__Country__c;
                    prtOrder.SVMXC__Destination_State__c = wo.SVMXC__State__c;
                    prtOrder.SVMXC__Destination_Zip__c = wo.SVMXC__Zip__c;
                    prtOrder.SVMXC__Destination_Street__c =wo.SVMXC__Street__c;
                    prtOrder.SVMXC__Service_Order__c = wo.id;
                    prtOrder.SVMXC__Order_Type__c = System.label.Spare_Parts_Order;
                    Porder.add(prtOrder);

            }


             if(Porder.size() > 0){
                insert Porder;

                for(SVMXC__RMA_Shipment_Order__c sp:Porder){
                    for(SVMXC__Service_Order_Line__c wl: wlList){
                        if(wl.SVMXC__Service_Order__c == sp.SVMXC__Service_Order__c ){
                             SVMXC__RMA_Shipment_Line__c prOLine = new SVMXC__RMA_Shipment_Line__c();
                              prOLine.SVMXC__Product__c = wl.SVMXC__Product__c;
                              prOLine.SVMXC__Expected_Quantity2__c = wl.SVMXC__Estimated_Quantity2__c;
                              prOLine.SVMXC__Expected_Receipt_Date__c = system.today();
                              prOLine.SVMXC__RMA_Shipment_Order__c  = sp.id;
                              prOLine.SVMX_ProductDetail__c = wl.SVMX_ProductDetail__c;
                              prOLine.RecordTypeId = shipRecTypeId;
                              prOLine.SVMXC__Service_Order_Line__c =wl.id;
                              prOLine.SVMXC__Line_Price2__c = wl.SVMXC__Estimated_Price2__c;
                              proLine.SVMXC__Discount_Percentage__c =wl.SVMXC__Discount__c;
                              proLine.SVMXC__Line_Type__c = System.label.Spare_Parts_Order;
                              partOrLineList.add(prOLine);
                        }

                    }

                    
                }

                if(partOrLineList.size() > 0)
                    insert partOrLineList;
            }
        }
	}
    
    public static void populateProductDetail(List<SVMXC__Service_Order_Line__c> wlList){
        
        List<SVMXC__Service_Order_Line__c> wdListToBeUpdated = new List<SVMXC__Service_Order_Line__c>() ;
        List<ProductDetail__c> productDetailList = new List<ProductDetail__c>() ;
        Map<id,ProductDetail__c> productIdProductDetailMap = new Map<id,ProductDetail__c>();
        Set<String> WDLanguageSet = new Set<String>();
        Set<id> WDproductIdSet = new Set<id>() ;
 		
     	if(!wlList.isEmpty()){
            for(SVMXC__Service_Order_Line__c wd : wlList){
                WDLanguageSet.add(wd.SVMX_Language__c) ;
                WDproductIdSet.add(wd.SVMXC__Product__c) ;
            }
        }
		
        productDetailList = [SELECT id,Language__c,ProductRef__c FROM ProductDetail__c WHERE Language__c IN :WDLanguageSet AND ProductRef__c IN :WDproductIdset] ;

        for(ProductDetail__c pd: productDetailList){
            productIdProductDetailMap.put(pd.ProductRef__c, pd);
        }
        
       	if(!productIdProductDetailMap.isEmpty()){
			for(SVMXC__Service_Order_Line__c wd : wlList ) {
                SVMXC__Service_Order_Line__c wdNew = new SVMXC__Service_Order_Line__c();
				wdNew.SVMX_ProductDetail__c = productIdProductDetailMap.get(wd.SVMXC__Product__c).id ; 
                wdNew.id = wd.id;
                wdListToBeUpdated.add(wdNew) ;
            }
        }
     	update wdListToBeUpdated ; 
    }
}