public class SVMX_ServiceContractProductDataManager{

    public static List<SVMXC__Service_Contract_Products__c> SCProductQuery(set<id> serviceContractProdId){
        List<SVMXC__Service_Contract_Products__c> serviceConProductMap = new List<SVMXC__Service_Contract_Products__c>([select Id,name, SVMXC__Is_Billable__c, SVMX_Billing_schedule__c ,SVMX_Discount__c, SVMX_Total_Line_Price__c, SVMX_SAP_Material_Number__c ,CurrencyIsoCode,SVMXC__Notes__c,SVMXC__Installed_Product__r.Name,SVMXC__Installed_Product__r.SVMXC__Product__r.SVMXC__Unit_Of_Measure__c, SVMX_Line_Price_Per_Unit__c, SVMX_Pricing_Method__c,SVMXC__Line_Price__c,SVMX_Billing_Start_Date__c,SVMX_Billing_End_Date__c,SVMXC__Start_Date__c,SVMXC__End_Date__c, SVMXC__Service_Contract__c, SVMX_SAP_Contract_Item_Number__c, SVMXC__Service_Contract__r.SVMXC__Company__r.Language__c,SVMX_SAP_Status__c from SVMXC__Service_Contract_Products__c where ID  IN :serviceContractProdId  AND SVMX_Pricing_Method__c = 'Time Based']);
        return serviceConProductMap;
    }
    

    public static List<SVMXC__Service_Contract_Products__c> timeBasedSCProductQuery(set<id> serviceContractId){
        List<SVMXC__Service_Contract_Products__c> serviceConProductMap = new List<SVMXC__Service_Contract_Products__c>([select Id,name, SVMXC__Is_Billable__c, SVMX_Billing_schedule__c ,SVMX_Discount__c, SVMX_Total_Line_Price__c, SVMX_SAP_Material_Number__c ,SVMXC__Installed_Product__r.Name,SVMXC__Installed_Product__r.SVMXC__Product__r.SVMXC__Unit_Of_Measure__c, SVMX_Line_Price_Per_Unit__c, CurrencyIsoCode,SVMXC__Notes__c, SVMX_Pricing_Method__c,SVMXC__Line_Price__c,SVMX_Billing_Start_Date__c,SVMX_Billing_End_Date__c,SVMXC__Start_Date__c,SVMXC__End_Date__c, SVMXC__Service_Contract__c, SVMX_SAP_Contract_Item_Number__c, SVMXC__Service_Contract__r.SVMXC__Company__r.Language__c,SVMX_SAP_Status__c from SVMXC__Service_Contract_Products__c where SVMXC__Service_Contract__c  IN :serviceContractId  AND SVMX_Pricing_Method__c = 'Time Based' AND SVMXC__Is_Billable__c = True]);
        return serviceConProductMap;
    }
    
    public static List<SVMXC__Service_Contract_Products__c> timeBasedSCProductBlankCheckQuery(set<id> serviceContractId){
        List<SVMXC__Service_Contract_Products__c> serviceConProductMap = new List<SVMXC__Service_Contract_Products__c>([select Id,name, SVMXC__Is_Billable__c, SVMX_Billing_schedule__c ,SVMX_Discount__c, SVMX_Total_Line_Price__c, SVMX_SAP_Material_Number__c ,SVMXC__Installed_Product__r.Name,SVMXC__Installed_Product__r.SVMXC__Product__r.SVMXC__Unit_Of_Measure__c,CurrencyIsoCode, SVMX_Line_Price_Per_Unit__c ,SVMXC__Notes__c, SVMX_Pricing_Method__c,SVMXC__Line_Price__c,SVMX_Billing_Start_Date__c,SVMX_Billing_End_Date__c,SVMXC__Start_Date__c,SVMXC__End_Date__c, SVMXC__Service_Contract__c, SVMX_SAP_Contract_Item_Number__c, SVMXC__Service_Contract__r.SVMXC__Company__r.Language__c,SVMX_SAP_Contract_Item_Num_Blank_Check__c,SVMX_SAP_Status__c from SVMXC__Service_Contract_Products__c where SVMXC__Service_Contract__c  IN :serviceContractId  AND SVMX_Pricing_Method__c = 'Time Based' AND SVMX_SAP_Contract_Item_Num_Blank_Check__c = True]);
        return serviceConProductMap;
    }
    
}