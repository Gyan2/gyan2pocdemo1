/**
 * Created by rebmangu on 23/03/2018.
 */

global class BatchFixeProductSAPNumber implements Database.Batchable<sObject>{

    global final List<String> fields = new List<String>{'Id','SAPNumber__c','ExternalId__c','SAPInstance__c'};

    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator('select '+String.join(fields,',')+' FROM Product2 where SAPNumber__c <> null and SAPInstance__c = \'PS8\'');
    }

    global void execute(Database.BatchableContext BC, List<sObject> scope){
        List<Product2> products = (List<Product2>)scope;
        List<String> productsToDelete = new List<String>();
        for(Product2 product : products){
            product.SAPNumber__c =  product.SAPNumber__c.leftPad(18,'0');
            if(product.ExternalId__c == null || product.ExternalId__c.length() < 22){
                productsToDelete.add('PS8-'+product.SAPNumber__c);
            }
            product.ExternalId__c = 'PS8-'+product.SAPNumber__c;
            product.SAPInstance__c ='PS8';
        }
        delete [select id from Product2 where ExternalId__c in: productsToDelete];
        update products;
    }

    global void finish(Database.BatchableContext BC){

    }
}