//Generated by wsdl2apex

public class AsyncWwwDormakabaComEConnectBcSalesforc {
    public class DocumentCreateMultipleSyncResponseFuture extends System.WebServiceCalloutFuture {
        public wwwDormakabaComEConnectBcSalesforc.DocumentCreateMultipleSyncResponse getValue() {
            wwwDormakabaComEConnectBcSalesforc.DocumentCreateMultipleSyncResponse response = (wwwDormakabaComEConnectBcSalesforc.DocumentCreateMultipleSyncResponse)System.WebServiceCallout.endInvoke(this);
            return response;
        }
    }
    public class AsyncHTTPS_Port {
        public String endpoint_x = 'https://dorjidx33100.tsi-dc.mydorma.com:53101/XISOAPAdapter/MessageServlet?senderParty=&senderService=Salesforce_Global&receiverParty=&receiverService=&interface=DocumentOut&interfaceNamespace=http%3A%2F%2Fwww.dormakaba.com%2Fe-connect%2Fbc%2Fsalesforce%2Fcrossapp';
        public Map<String,String> inputHttpHeaders_x;
        public String clientCertName_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://soap.sforce.com/2005/09/outbound', 'soapSforceCom200509Outbound', 'http://www.dorma.com/e-connect/bc/erp/application/crossapp', 'wwwDormaComEConnectBcErpApplicati', 'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp', 'wwwDormakabaComEConnectBcSalesforc', 'urn:dorma:esm:edt:1.0', 'dormaEsmEdt10', 'urn:dorma:esm:esi:document:1.0', 'dormaEsmEsiDocument10'};
        public AsyncWwwDormakabaComEConnectBcSalesforc.DocumentCreateMultipleSyncResponseFuture beginCreateMultipleSync(System.Continuation continuation,dormaEsmEdt10.BusinessDocumentMessageHeader MessageHeader,wwwDormakabaComEConnectBcSalesforc.DocumentCreateRequestDocument[] Document) {
            wwwDormakabaComEConnectBcSalesforc.DocumentCreateMultipleRequest request_x = new wwwDormakabaComEConnectBcSalesforc.DocumentCreateMultipleRequest();
            request_x.MessageHeader = MessageHeader;
            request_x.Document = Document;
            return (AsyncWwwDormakabaComEConnectBcSalesforc.DocumentCreateMultipleSyncResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncWwwDormakabaComEConnectBcSalesforc.DocumentCreateMultipleSyncResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',
              'DocumentCreateMultipleRequest',
              'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',
              'DocumentCreateMultipleSyncResponse',
              'wwwDormakabaComEConnectBcSalesforc.DocumentCreateMultipleSyncResponse'}
            );
        }
        public AsyncSoapSforceCom200509Outbound.notificationsResponse_elementFuture beginCreateSync(System.Continuation continuation,wwwDormakabaComEConnectBcSalesforc.BusinessDocumentMessageHeader MessageHeader,wwwDormakabaComEConnectBcSalesforc.DocumentCreateRequestDocument Document) {
            wwwDormakabaComEConnectBcSalesforc.DocumentCreateRequest request_x = new wwwDormakabaComEConnectBcSalesforc.DocumentCreateRequest();
            request_x.MessageHeader = MessageHeader;
            request_x.Document = Document;
            return (AsyncSoapSforceCom200509Outbound.notificationsResponse_elementFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncSoapSforceCom200509Outbound.notificationsResponse_elementFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',
              'DocumentCreateRequest',
              'http://soap.sforce.com/2005/09/outbound',
              'notificationsResponse',
              'soapSforceCom200509Outbound.notificationsResponse_element'}
            );
        }
        public AsyncDormaEsmEsiDocument10.DocumentReadResponseFuture beginReadSync(System.Continuation continuation,dormaEsmEsiDocument10.ArchiveObject ArchiveObject,dormaEsmEsiDocument10.DocumentReadRequestDocumentingParty DocumentingParty,String CosmosCorrelation) {
            dormaEsmEsiDocument10.DocumentReadRequest request_x = new dormaEsmEsiDocument10.DocumentReadRequest();
            request_x.ArchiveObject = ArchiveObject;
            request_x.DocumentingParty = DocumentingParty;
            request_x.CosmosCorrelation = CosmosCorrelation;
            return (AsyncDormaEsmEsiDocument10.DocumentReadResponseFuture) System.WebServiceCallout.beginInvoke(
              this,
              request_x,
              AsyncDormaEsmEsiDocument10.DocumentReadResponseFuture.class,
              continuation,
              new String[]{endpoint_x,
              'http://sap.com/xi/WebService/soap1.1',
              'urn:dorma:esm:esi:document:1.0',
              'DocumentReadSyncRequest',
              'http://www.dorma.com/e-connect/bc/erp/application/crossapp',
              'DocumentReadSyncResponse',
              'dormaEsmEsiDocument10.DocumentReadResponse'}
            );
        }
    }
}