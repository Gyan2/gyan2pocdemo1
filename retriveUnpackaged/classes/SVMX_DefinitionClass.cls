/*******************************************************************************************************
Description: Definition class for stopping recursions

Dependancy:
 
Author: Pooja Singh
Date: 23-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_DefinitionClass {
    public static Boolean stopTaskRecursion = false;
    public static Boolean runOnceDynamicPM = true;
}