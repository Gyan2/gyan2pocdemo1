public with sharing class JsonViewerController {

    
    @AuraEnabled
    public static String getJsonValue(String field,String objectName,Id recordId){
        SObject o = Database.query('Select Id, '+field+' FROM '+ObjectName+' where id =: recordId');
        
        return (String)o.get(field);
    }
}