@IsTest
private class SVMX_wwwDormakabaComSalesOrder_UT
{
    static testMethod void coverTypes(){

        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestGoodsRecipientDocument();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestInvoiceTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestBusiTransDocRef();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseBuyerParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemProduct();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseTotalValues();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequest();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestStatusObject();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestDateTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemStatusObject();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseSalesOrder();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestPriceComponent();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemStatus();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequest();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemSalesTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestItem();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestItem();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestItemTotalValues();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestGoodsRecipientParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseItemScheduleLine();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestDeliveryTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestInvoiceTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestStatus();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequest();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestShipToLocation();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestSalesOrder();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestSellerParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestItemProduct();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestPricingTerms();
         new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestSellerParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponsePriceComponent();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateItemSalesTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseItemTotalValues();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestDeliveryTerms();
         new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestShipToLocation();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestBuyerParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemStatusObjectUserStatus();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestTransportationTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponse();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemInvoiceTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestDateTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestCashDiscountTerms();
         new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestBuyerDocument();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestTransportationTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestDeliveryTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseGoodsRecipientParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestItemDeliveryTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestItemBOMVariant();
         new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponsePartyName();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestItem();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemDateTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemDeliveryTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponsePartyAddress();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestPricingTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemBuyerDocument();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseItemProduct();
         new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemTotalValues();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestItemBOM();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestItemBOMVariantItem();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestItemDateTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponsePriceComponentCalculationBasis();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseDeliveryTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestSalesChannel();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestItemBusiTransDocRef();
         new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestStatusObjectUserStatus();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestBuyerParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesDocumentRequestSalesTerms();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateRequestGoodsRecipientParty();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseItem();
         new SVMX_wwwDormakabaComSalesOrder.SalesOrderSimulateResponseItemPriceComponent();
        new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestTransportationTerms();
        



        
        
    }
}