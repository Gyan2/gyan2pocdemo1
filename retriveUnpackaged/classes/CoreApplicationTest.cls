/**
 * Copyright (c) 2014, FinancialForce.com, inc
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, 
 *   are permitted provided that the following conditions are met:
 *
 * - Redistributions of source code must retain the above copyright notice, 
 *      this list of conditions and the following disclaimer.
 * - Redistributions in binary form must reproduce the above copyright notice, 
 *      this list of conditions and the following disclaimer in the documentation 
 *      and/or other materials provided with the distribution.
 * - Neither the name of the FinancialForce.com, inc nor the names of its contributors 
 *      may be used to endorse or promote products derived from this software without 
 *      specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND 
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES 
 *  OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL 
 *  THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, 
 *  EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
 *  OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 *  OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
**/


@IsTest
private class CoreApplicationTest 
{
		

	@IsTest
	private static void callingDomainFactoryWithGenericListShouldGiveException()
	{
		try {
			Domain.newInstance(new List<SObject>());
			System.assert(false, 'Expected exception');
		} catch (CoreApplication.DeveloperException e) {
			System.assertEquals('Unable to determine SObjectType', e.getMessage());
		}
	}

	@IsTest
	private static void callingDomainFactoryWithNoSObjectTypeShouldGiveException()
	{
		try {
			Domain.newInstance(new List<SObject>(), null);
			System.assert(false, 'Expected exception');
		} catch (CoreApplication.DeveloperException e) {
			System.assertEquals('Must specify sObjectType', e.getMessage());
		}
	}	

	@IsTest
	private static void callingDomainFactoryWithInAccessableConstructorShouldGiveException()
	{
		try {
			Domain.newInstance(new List<Product2>{ new Product2(Name = 'Test Product') });
			System.assert(false, 'Expected exception');
		} catch (CoreApplication.DeveloperException e) {
			System.assertEquals('Domain constructor class not found for SObjectType Product2', e.getMessage());
		}

		try {
			Domain.newInstance(new List<SObject>{ new Product2(Name = 'Test Product') }, Product2.SObjectType);
			System.assert(false, 'Expected exception');
		} catch (CoreApplication.DeveloperException e) {
			System.assertEquals('Domain constructor class not found for SObjectType Product2', e.getMessage());
		}		
	}

	@IsTest
	private static void callingDomainFactoryWithContructorClassThatDoesNotSupportIConstructableShouldGiveException()
	{
		try {
			Domain.newInstance(new List<Contact>{ new Contact(LastName = 'TestContactLName') });
			System.assert(false, 'Expected exception');
		} catch (System.TypeException e) {
			System.assert(Pattern.Matches('Invalid conversion from runtime type \\w*\\.?CoreApplicationTest\\.ContactsConstructor to \\w*\\.?CoreSObjectDomain\\.IConstructable',
				e.getMessage()), 'Exception message did not match the expected pattern: ' + e.getMessage());
		}	

		try {
			Domain.newInstance(new List<SObject>{ new Contact(LastName = 'TestContactLName') }, Contact.SObjectType);
			System.assert(false, 'Expected exception');
		} catch (System.TypeException e) {
			System.assert(Pattern.Matches('Invalid conversion from runtime type \\w*\\.?CoreApplicationTest\\.ContactsConstructor to \\w*\\.?CoreSObjectDomain\\.IConstructable2',
				e.getMessage()), 'Exception message did not match the expected pattern: ' + e.getMessage());
		}		
	}	

	@IsTest
	private static void callingUnitOfWorkFactoryShouldGivenStandardImplsAndMockImpls()
	{
		// Standard behaviour
		System.assert(UnitOfWork.newInstance() instanceof CoreSObjectUnitOfWork);
 		
	}

	@IsTest
	private static void callingServiceFactoryShouldGiveRegisteredImplsAndMockImpls()
	{
		// Standard behaviour
		System.assert(Service.newInstance(IAccountService.class) instanceof AccountsServiceImpl);
		System.assert(Service.newInstance(IOpportunitiesService.class) instanceof OpportunitiesServiceImpl);
		try {
			Service.newInstance(IContactService.class);
			System.assert(false, 'Expected exception');
		} catch (CoreApplication.DeveloperException e) {
			System.assertEquals('No implementation registered for service interface ' + IContactService.class.getName(), e.getMessage());
		}

		// Mocking behaviour		
		Service.setMock(IAccountService.class, new AccountsServiceMock());
		System.assert(Service.newInstance(IOpportunitiesService.class) instanceof OpportunitiesServiceImpl);
		System.assert(Service.newInstance(IAccountService.class) instanceof AccountsServiceMock);
	}

	@IsTest
	private static void callingSelectorFactoryShouldGiveRegisteredImpls()
	{
		// Standard behaviour
		System.assert(Selector.newInstance(Account.SObjectType) instanceof AccountsSelector);
		System.assert(Selector.newInstance(Opportunity.SObjectType) instanceof OpportuntiesSelector);
		try {
			Selector.newInstance(User.SObjectType);
			System.assert(false, 'Expected exception');
		} catch (CoreApplication.DeveloperException e) {
			System.assertEquals('Selector class not found for SObjectType User', e.getMessage());
		}
	}

	@IsTest
	private static void callingSelectorFactorySelectByIdWithEmptyListShouldGiveException()
	{
		try {
			Selector.selectById(null);
			System.assert(false, 'Expected exception');
		} catch (CoreApplication.DeveloperException e) {
			System.assertEquals('Invalid record Id\'s set', e.getMessage());
		}
		try {
			Selector.selectById(new Set<Id>());
			System.assert(false, 'Expected exception');
		} catch (CoreApplication.DeveloperException e) {
			System.assertEquals('Invalid record Id\'s set', e.getMessage());
		}
	}

	@IsTest
	private static void callingSelectorByRelationship() {
		Opportunity opp = new Opportunity(Name='test',StageName='Qualification',CloseDate=Date.today());
		Account acc = new Account(Name='test',Language__c='EN');

		insert new List<SObject>{opp,acc};

		opp.AccountId = acc.Id;
		update opp;
		List<Account> accountList = (List<Account>)Selector.selectByRelationship(new List<Opportunity>{opp},Opportunity.AccountId);
		AccountsDomain AccountDo = (AccountsDomain)Domain.newInstance(accountList);
		System.assertEquals(1,AccountDo.getRecords().size(),'There should be 1 Account in the list');

	}

	@IsTest
	private static void callingSelectorFactorySelectByIdWithMixedIdTypeListShouldGiveException() {
		/* TODO: REPLACE THE ICoreDGenerator
		try {
			Selector.selectById(
				new Set<Id> { 
					ICoreDGenerator.generate(Opportunity.SObjectType), 
					ICoreDGenerator.generate(Account.SObjectType) });
			System.assert(true, 'Expected exception');
		} catch (CoreApplication.DeveloperException e) {
			System.assertEquals('Unable to determine SObjectType, Set contains Id\'s from different SObject types', e.getMessage());
		}	
		*/	
	}


	// Configure and create the ServiceFactory for this Application
	public static final CoreApplication.ServiceFactory Service = 
		new CoreApplication.ServiceFactory( 
			new Map<Type, Type> {
					IOpportunitiesService.class => OpportunitiesServiceImpl.class,
					IAccountService.class => AccountsServiceImpl.class });

	// Configure and create the UnitOfWorkFactory for this Application
	public static final CoreApplication.UnitOfWorkFactory UnitOfWork = 
		new CoreApplication.UnitOfWorkFactory(
				new List<SObjectType> { 
					Account.SObjectType,
					Opportunity.SObjectType,
					OpportunityLineItem.SObjectType });	

	// Configure and create the SelectorFactory for this Application
	public static final CoreApplication.SelectorFactory Selector = 
		new CoreApplication.SelectorFactory(
			new Map<SObjectType, Type> {
					Account.SObjectType => AccountsSelector.class,
					Opportunity.SObjectType => OpportuntiesSelector.class });

	// Configure and create the DomainFactory for this Application
	public static final CoreApplication.DomainFactory Domain = 
		new CoreApplication.DomainFactory(
			CoreApplicationTest.Selector,
			new Map<SObjectType, Type> {
					Account.SObjectType => AccountsConstructor.class,
					Opportunity.SObjectType => OpportuntiesConstructor.class,
					Contact.SObjectType => ContactsConstructor.class });

	public class AccountsDomain extends CoreSObjectDomain
	{
		public AccountsDomain(List<Account> sObjectList)
		{
			super(sObjectList);
		}

		public AccountsDomain(List<SObject> sObjectList, SObjectType sObjectType)
		{
			super(sObjectList, sObjectType);
		}
	}			

	public class AccountsConstructor implements CoreSObjectDomain.IConstructable2
	{
		public CoreSObjectDomain construct(List<SObject> sObjectList)
		{
			return new AccountsDomain(sObjectList);
		}

		public CoreSObjectDomain construct(List<SObject> sObjectList, SObjectType sObjectType)
		{
			return new AccountsDomain(sObjectList, sObjectType);
		}		
	}	

	public class OpportuntiesDomain extends CoreSObjectDomain
	{
		public OpportuntiesDomain(List<Opportunity> sObjectList)
		{
			super(sObjectList);
		}

		public OpportuntiesDomain(List<SObject> sObjectList, SObjectType sObjectType)
		{
			super(sObjectList, sObjectType);
		}		
	}	

	public class OpportuntiesConstructor implements CoreSObjectDomain.IConstructable2
	{
		public CoreSObjectDomain construct(List<SObject> sObjectList)
		{
			return new OpportuntiesDomain(sObjectList);
		}

		public CoreSObjectDomain construct(List<SObject> sObjectList, SObjectType sObjectType)
		{
			return new OpportuntiesDomain(sObjectList, sObjectType);
		}		
	}

	public class ContactsDomain extends CoreSObjectDomain
	{
		public ContactsDomain(List<Opportunity> sObjectList)
		{
			super(sObjectList);
		}

		public ContactsDomain(List<SObject> sObjectList, SObjectType sObjectType)
		{
			super(sObjectList, sObjectType);
		}
	}			

	// Intentionally does not support IConstructable or IConstructable2 interfaces in order to support testing
	public class ContactsConstructor
	{

	}		

	class OpportuntiesSelector extends CoreSObjectSelector
	{
		public List<Schema.SObjectField> getSObjectFieldList()
		{
			return new List<Schema.SObjectField> {
				Opportunity.Name,
				Opportunity.Id
			};
		}
		
		public Schema.SObjectType getSObjectType()
		{
			return Opportunity.sObjectType;
		}
	}
	
	class AccountsSelector extends CoreSObjectSelector
	{
		public List<Schema.SObjectField> getSObjectFieldList()
		{
			return new List<Schema.SObjectField> {
				Account.Name,
				Account.Id,
				Account.AccountNumber,
				Account.AnnualRevenue
			};
		}
		
		public Schema.SObjectType getSObjectType()
		{
			return Account.sObjectType;
		}
	}

	public interface IContactService { }

	public interface IOpportunitiesService { }

	public interface IAccountService { }

	public class OpportunitiesServiceImpl implements IOpportunitiesService { }

	public class AccountsServiceImpl implements IAccountService { }

	public class AccountsServiceMock implements IAccountService { }
}