public class WallBoardCase {
    @AuraEnabled
    public static string LTNG_getUserSession() {
        return userInfo.getSessionId(); //This method is used to return the session id (to implement streaming api)
    }
    
    //wrapper class contains list of case, list of case type, dml exception if any
    public class caseDataTableWrapper {
        @AuraEnabled
        public List<Case> caseList {get;set;}
        @AuraEnabled
        public List<String> caseType {get;set;}
        @AuraEnabled
        public String dmlException {get;set;}
        @AuraEnabled
        public Map<String, String> translationMap {get; set;}
        public caseDataTableWrapper() {
            translationMap = new Map<String, String>();
            translationMap.put('Caption', Label.WB_Caption);
            translationMap.put('TYPE', Label.WB_Type);
            translationMap.put('OWNER', Label.WB_Owner);
            translationMap.put('SERVICEAREA', Label.WB_ServiceArea);
            translationMap.put('VIEW', Label.WB_View);
            translationMap.put('WB_SLADate1', Label.WB_SLADate.substring(0,8));
            translationMap.put('WB_SLADate2', Label.WB_SLADate.substring(10));
            translationMap.put('STATUS', Label.WB_Status);
            translationMap.put('CASETYPE', Label.WB_CaseType);
            translationMap.put('ACCOUNT', Label.WB_AccountName);
            translationMap.put('CITY', Label.WB_City);
            translationMap.put('INSTALLEDPRODUCT', Label.WB_InstalledProduct);
            translationMap.put('CASESUBJECT', Label.WB_CaseSubject);
            translationMap.put('CASENO', Label.WB_CaseNo);
        }
        
    }
    
    //wrapper class contains list of case, dml exception if any
    public class caseWrapperClass {
        @AuraEnabled
        public List<Case> caseList {get;set;}
        @AuraEnabled
        public String dmlException {get;set;}
        
    }
    
    
    //Onload of the component this method is called to get all the case record & picklist values of type field
    @AuraEnabled
    public static caseDataTableWrapper ltng_getCaseList(List<String> searchType , String serviceArea) {
        //get the values of Type pick list from schema
        List<String> pickListValuesList = new List<String>();
        Schema.DescribeFieldResult fieldResult = Case.Type.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        for( Schema.PicklistEntry pickListVal : ple){
            pickListValuesList.add(pickListVal.getLabel());
        } 
        
        
        
        
        
        
        
        
        
        
        //LTNG_filterCaseList returns the list of case record
        //
        List<String> ownerList = new List<String>();
        caseWrapperClass caseWrap = WallBoardCase.LTNG_filterCaseList(searchType, serviceArea, ownerList);
        caseDataTableWrapper caseWrapper = new caseDataTableWrapper();
        //caseWrapper.ownerName = allOwner;
        caseWrapper.caseList = caseWrap.caseList;
        caseWrapper.dmlException = caseWrap.dmlException;
        caseWrapper.caseType = pickListValuesList;
        
        
        System.debug('Check Map: ' + caseWrapper.translationMap);
        
        
        
        
        System.debug('Check Return Wrapper class: ' + caseWrapper);
        return caseWrapper;
    }
    
    //this method returns the list of case record
    @AuraEnabled   
    public static caseWrapperClass LTNG_filterCaseList(List<string> searchType, string serviceArea, List<String> ownerList){
        String ownerString = '';
        //System.debug('Check search Type: ' + searchType);
        for(String owner : ownerList) {
            ownerString = ownerString + ' owner.name = \'' + owner + '\' or';
        }
        
        
        String serviceType = '';
        System.debug('Check search Type: ' + searchType);
        for(String serType : searchType) {
            serviceType = serviceType + ' Type = \'' + serType + '\' or';
        }
        System.debug('Check search Type query form: ' + serviceType);
        List<Case> returnList = New List<Case>();
        String caseQuery = 'SELECT Id,SVMXC__Component__r.Name,Account.Name,Next_SLA_Milestone_Due_Date__c,Real_Owner__c,Contract_Type__c,Client_Location__c,Type,New_Service_Area__c,Service_Area__c,CaseSubject__c,SVMXC__Service_Contract__r.SVMX_Contract_Types__c,SVMXC__Site__r.Name,No_Email__c,Subject,CaseNumber,Contact.Name,Priority,Status,Owner.Name,CreatedDate,Time__c from Case where Show_On_Wallboard__c = true and ';
        if(ownerList.size() != 0) {
            caseQuery = caseQuery + '('+ ownerString.substring(0,ownerString.length()-2) + ') and ';
        }
        
        
        if(searchType.size() != 0){
            caseQuery = caseQuery +'('+ serviceType.substring(0,serviceType.length()-2) + ') and ';
        }
        if(serviceArea != ''){
            caseQuery = caseQuery + ' Service_Area__c =:serviceArea and ';
        }
        caseQuery = caseQuery +  ' SVMX_Internal_Country__c = \'France\' and IsClosed = False and Status != \'Escalated\' order by Next_SLA_Milestone_Due_Date__c ASC ';
        System.debug('Check Query: ' + caseQuery);
        caseWrapperClass caseWrap = new caseWrapperClass();
        caseWrap.dmlException = 'Success';
        try {
            returnList = Database.query(caseQuery);
        }
        catch(Exception e) {
            caseWrap.dmlException = String.valueOf(e);
        }
        caseWrap.caseList = returnList;
        return caseWrap ;
    }
    
    @AuraEnabled   
    public static List<String> LTNG_getOwnerList(String searchKKeyword) {
        List<String> ownerOptionList = new List<String>();
        Set<String> ownerOptionSet = new Set<String>();
        
        searchKKeyword = '%'+searchKKeyword+'%';
        
        List<User> userList = [select Name from User where Name like :searchKKeyword];
        List<Group> queueList = [select Name from Group where  Type = 'Queue' and Name like :searchKKeyword];
        
        for(User userListElement : userList) {
            ownerOptionSet.add(userListElement.Name);
            
        }
        
        for(Group queueListElement : queueList) {
            ownerOptionSet.add(queueListElement.Name);
            
        }
        ownerOptionList.addAll(ownerOptionSet);
        
        
        
        return ownerOptionList;
        
    }
    
}