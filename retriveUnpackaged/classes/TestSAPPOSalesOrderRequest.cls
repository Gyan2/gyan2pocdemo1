/**
 * Created by rebmangu on 14/02/2018.
 */

@IsTest
private class TestSAPPOSalesOrderRequest {

    @testSetup static void setup(){

        Product2 product = new Product2(
                Name='Test',
                SalesSpecifications__c='{"blabla":"blabla"}',
                ExternalId__c = 'PS8-00000001',
                SAPNumber__c = '00000001',
                QuantityUnitOfMeasure = 'PCE'
        );
        insert product;


        PricebookEntry pbE = new PricebookEntry(Pricebook2Id=Test.getStandardPricebookId(),Product2Id=product.Id,UnitPrice=1000,IsActive=true);
        insert pbE;

        Account acc = new Account(SAPNumber__c='123456789',Name='Test Account');
        insert acc;

        Opportunity opp = new Opportunity(Name='Test opp',AccountId=acc.Id,StageName='Proposal/Price Quote',CloseDate=Date.valueOf(System.now()));
        insert opp;

        Quote quo = new Quote(OpportunityId=opp.Id,Name='Test Quote',SalesOrg__c='5500',Pricebook2Id=Test.getStandardPricebookId(),ScheduledDate__c=Date.valueOf(System.now()));
        insert quo;

        QuoteLineItem quoItem = new QuoteLineItem(Product2Id=product.Id,Quantity=10,QuoteId=quo.Id,PricebookEntryId=pbE.Id,UnitPrice=1000);
        insert quoItem;



    }


    static testMethod void testSimulateOrder() {

        Test.setMock(WebServiceMock.class, new SalesOrderSimulateRequestMockImpl(
                new List<Map<String,String>>{
                        new Map<String,String>{
                                'internalId'=>'00000001',
                                'quantity' => '10',
                                'netAmount' => String.valueOf(500*10),
                                'GRSP' => '500'
                        }
                }
        ));
        SAPPO_SalesOrderService orderSimulator = new SAPPO_SalesOrderService();
        Quote quo = orderSimulator.Utility.getQuotes([select id from Quote limit 1].Id);

        Test.startTest();
        System.assertEquals(1000.0,[select id,UnitPrice,TotalPrice from QuoteLineItem where QuoteId = : quo.Id limit 1].UnitPrice);
        SAPPO_SalesOrderService.SalesOrderSimulationWrapper wrapper = new SAPPO_SalesOrderService.SalesOrderSimulationWrapper();
        wrapper.quote = quo;
        wrapper.account = quo.Account;
        wrapper.quoteLineItems = quo.QuoteLineItems;
        SAPPO_SalesOrderService.SalesOrderResultWrapper res = orderSimulator.syncSimulatedSalesOrder(wrapper,true);
        system.debug(res);
        System.assert(res.status == 200);
        System.assertEquals(500.0,[select id,UnitPrice,TotalPrice from QuoteLineItem where QuoteId = : quo.Id limit 1].UnitPrice);
        Test.stopTest();
    }

    static testMethod void testATPCheck() {

        Test.setMock(WebServiceMock.class, new SalesOrderSimulateRequestMockImpl(
                new List<Map<String,String>>{
                        new Map<String,String>{
                                'internalId'=>'00000001',
                                'quantity' => '10',
                                'netAmount' => String.valueOf(500*10),
                                'GRSP' => '500'
                        }
                }
        ));


        Map<String,SAPPO_SalesOrderService.ATPCheckMapping> materialMapping = new Map<String,SAPPO_SalesOrderService.ATPCheckMapping>{
                '00000001' =>new SAPPO_SalesOrderService.ATPCheckMapping(10,'','')
        };

        Test.startTest();
        List<Map<String,String>> result = new SAPPO_SalesOrderService().getATPCheck('0005002766',System.today(),'9921226005491',materialMapping).data;
        System.assertEquals(result[0].get('AvailableOnDate'),String.valueOf(System.today()),String.valueOf(System.today())+' != '+result[0].get('AvailableOnDate'));
        Test.stopTest();
    }


    public class SalesOrderSimulateRequestMockImpl implements WebServiceMock  {
        private List<Map<String,String>> mapping;
        public SalesOrderSimulateRequestMockImpl(List<Map<String,String>> mapping){
            this.mapping = mapping;
        }

        public void doInvoke(
                Object stub,
                Object request,
                Map<String, Object> response,
                String endpoint,
                String soapAction,
                String requestName,
                String responseNS,
                String responseName,
                String responseType) {

            SAPPO_SalesOrderRequest.SalesOrderSimulateResponse response_x = this.generateResponse(this.mapping);

            response.put('response_x', response_x);
        }

        public SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem generateItem(Map<String,String> mapping,Integer index){
            SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem item = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem();
            item.ID = String.valueOf(index);
            item.Product = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemProduct();
            item.Product.InternalId = new SAPPO_CommonDataTypes.ProductInternalID();
            item.Product.InternalId.Content = mapping.get('internalId');
            item.TotalValues = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemTotalValues();
            item.TotalValues.RequestedQuantity = new SAPPO_CommonDataTypes.Quantity();
            item.TotalValues.RequestedQuantity.Content = mapping.get('quantity');
            item.TotalValues.ConfirmedQuantity = new SAPPO_CommonDataTypes.Quantity();
            item.TotalValues.ConfirmedQuantity.Content = mapping.get('quantity');
            item.TotalValues.NetAmount = new SAPPO_CommonDataTypes.Amount();
            item.TotalValues.NetAmount.Content = mapping.get('netAmount');
            item.TotalValues.NetAmount.CurrencyCode = 'CHF';
            item.PriceComponent = new List<SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent>{
                    new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent(),
                    new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent(),
                    new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent(),
                    new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent(),
                    new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemPriceComponent()
            };
            item.PriceComponent[0].PriceSpecificationElementTypeCode = 'MWST';
            item.PriceComponent[0].Rate = new SAPPO_EsmEdt.Rate();
            item.PriceComponent[0].Rate.DecimalValue = '25';

            item.PriceComponent[1].PriceSpecificationElementTypeCode = 'SPED';
            item.PriceComponent[1].Rate = new SAPPO_EsmEdt.Rate();
            item.PriceComponent[1].Rate.DecimalValue = '10';

            item.PriceComponent[2].PriceSpecificationElementTypeCode = 'BASD';
            item.PriceComponent[2].Rate = new SAPPO_EsmEdt.Rate();
            item.PriceComponent[2].Rate.DecimalValue = '10';

            item.PriceComponent[3].PriceSpecificationElementTypeCode = 'ADDD';
            item.PriceComponent[3].Rate = new SAPPO_EsmEdt.Rate();
            item.PriceComponent[3].Rate.DecimalValue = '10';

            item.PriceComponent[4].PriceSpecificationElementTypeCode = 'GRSP';
            item.PriceComponent[4].Rate = new SAPPO_EsmEdt.Rate();
            item.PriceComponent[4].Rate.DecimalValue = mapping.get('GRSP');

            item.ScheduleLine = new List<SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemScheduleLine>{
                    new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItemScheduleLine()
            };

            item.ScheduleLine[0].Date_x = String.valueOf(System.today());
            item.ScheduleLine[0].RequestedQuantity = new SAPPO_CommonDataTypes.Quantity();
            item.ScheduleLine[0].RequestedQuantity.Content = mapping.get('quantity');
            item.ScheduleLine[0].ConfirmedQuantity = new SAPPO_CommonDataTypes.Quantity();
            item.ScheduleLine[0].ConfirmedQuantity.Content = mapping.get('quantity');

            return item;
        }

        public SAPPO_SalesOrderRequest.SalesOrderSimulateResponse generateResponse(List<Map<String,String>> data){
            SAPPO_SalesOrderRequest.SalesOrderSimulateResponse response_x = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponse();
            response_x.SalesOrder = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseSalesOrder();
            response_x.SalesOrder.Item = new List<SAPPO_SalesOrderRequest.SalesOrderSimulateResponseItem>();
            Integer index = 1;
            for(Map<String,String> mapping : data){
                response_x.SalesOrder.Item.add(this.generateItem(mapping,index));
                index++;
            }

            response_x.SalesOrder.TotalValues = new SAPPO_SalesOrderRequest.SalesOrderSimulateResponseTotalValues();

            return response_x;
        }
    }
}