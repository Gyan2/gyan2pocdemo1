@IsTest
private class WS001_CustomerMaster_Test {

    private static testMethod void testAccount() {
        WS001_CustomerMaster.CustomerMasterRequest request = new WS001_CustomerMaster.CustomerMasterRequest();

        WS001_CustomerMaster.CustomerMaster record1 = new WS001_CustomerMaster.CustomerMaster();
        record1.accountGroup = 'ZSOP';


        record1.sapInstance = 'ps8';
        record1.sapNumber = '9876543210';
        record1.taxRegNumber2 = '123456789';
        record1.billingBlock = false;
        record1.deliveryBlock = false;
        record1.orderBlock = true;
        record1.countryCode = 'CH';

        record1.name = 'Testing Account 123';

        record1.salesArrangements = new List<WS001_CustomerMaster.SalesArrangement>();



        WS001_CustomerMaster.CustomerContact contact1 = new WS001_CustomerMaster.CustomerContact();
        WS001_CustomerMaster.CustomerContact contact2 = new WS001_CustomerMaster.CustomerContact();

        Map<String, String> sapNumberLastNameMapping = new Map<String,String>{'98765'=>'Testing Contact 123-1', '43210'=>'Testing Contact 123-2'};
        contact1.lastName = sapNumberLastNameMapping.get('98765');
        contact1.sapNumber = '98765';
        contact1.deletionFlag = false;
        contact1.language = 'EN';

        contact2.lastName = sapNumberLastNameMapping.get('43210');
        contact2.sapNumber = '43210';
        contact2.deletionFlag = false;
        contact2.language = 'EN';

        record1.customerContacts = new List<WS001_CustomerMaster.CustomerContact>{contact1,contact2};



        request.customers = new List<WS001_CustomerMaster.CustomerMaster>{record1};
        WS001_CustomerMaster.CustomerMasterResponse response = WS001_CustomerMaster.upsertCustomer(request);
        //System.assert(response.success);
        System.assertEquals(null, response.errors);

        List<Account> accounts = [SELECT Name FROM Account];

        System.assertEquals(1, accounts.size());
        System.assertEquals('Testing Account 123', accounts.get(0).Name);

        List<Contact> contacts = [SELECT LastName, SAPNumber__c FROM Contact];
        System.assertEquals(2, contacts.size());

        System.assertEquals(sapNumberLastNameMapping.get(contacts.get(0).SAPNumber__c), contacts.get(0).LastName);
        System.assertEquals(sapNumberLastNameMapping.get(contacts.get(1).SAPNumber__c), contacts.get(1).LastName);

    }

    private static testMethod void testLocation() {
        WS001_CustomerMaster.CustomerMasterRequest request = new WS001_CustomerMaster.CustomerMasterRequest();

        WS001_CustomerMaster.CustomerMaster record1 = new WS001_CustomerMaster.CustomerMaster();
        record1.accountGroup = 'ZLOC';



        record1.sapInstance = 'ps8';
        record1.sapNumber = '9876543210';

        record1.name = 'Testing Location 123';

        record1.salesArrangements = new List<WS001_CustomerMaster.SalesArrangement>();

        request.customers = new List<WS001_CustomerMaster.CustomerMaster>{record1};
        WS001_CustomerMaster.CustomerMasterResponse response = WS001_CustomerMaster.upsertCustomer(request);
        System.assert(response.success);
        System.assertEquals(null, response.errors);

        List<Location__c> locations = [SELECT Name FROM Location__c];

        System.assertEquals(1, locations.size());
        System.assertEquals('Testing Location 123', locations.get(0).Name);
    }

}