public class SVMX_VendorMaterialDataManager {   
     public static list<VendorMaterial__c> vendorMaterialQuery(set<id> accIDs){
        list<VendorMaterial__c> vendrMaterlist = new list<VendorMaterial__c> ([select id,Name,Account__c,Account__r.name,Product__c from VendorMaterial__c  Where Account__c in:accIDs]);
        return vendrMaterlist;
    }     
}