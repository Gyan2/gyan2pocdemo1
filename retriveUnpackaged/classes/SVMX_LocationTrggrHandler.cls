/*******************************************************************************************************
Description: Trigger handler for Location Trigger

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger: SVMX_LocationTrggr.cls
    Service Manager: SVMX_LocationServiceManager
    Test class: SVMX_LocationTrggrHandler_UT.cls
    
 
Author: Tulsi B R
Date: 14-11-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_LocationTrggrHandler extends SVMX_TriggerHandler {

   private Map<Id,SVMXC__Site__c> OldLocMap;
    private Map<Id,SVMXC__Site__c> newLocMap;
   private list<SVMXC__Site__c> newLocList;

   public SVMX_LocationTrggrHandler() {
       this.newLocList = (list<SVMXC__Site__c>) Trigger.new;
       this.newLocMap = (Map<Id,SVMXC__Site__c>) Trigger.newMap;
       this.OldLocMap = (MAp<Id,SVMXC__Site__c>) Trigger.oldMap;
       
   }
      
   public override void beforeupdate(){
       list<SVMXC__Site__c> LocList = new list<SVMXC__Site__c>();
     //list<SVMXC__Installed_Product__c> ips=new  list<SVMXC__Installed_Product__c>();
     //map<id,list<SVMXC__Installed_Product__c>> ipmap=new  map<id,list<SVMXC__Installed_Product__c>>();
       list<SVMXC__Installed_Product__c> updateIPs= new list<SVMXC__Installed_Product__c>();
       list<SVMXC__Site__c> updateLoc = new list<SVMXC__Site__c>();
       Map<Id,SVMXC__Site__c> updateLocMap = new Map<Id,SVMXC__Site__c>();
       list<SVMX_Related_Account__c> RelAccountList = new list<SVMX_Related_Account__c>();
       List<SVMXC__Site__c> childLocList = new List<SVMXC__Site__c>();
       List<SVMXC__Site__c> allLocList = new List<SVMXC__Site__c>();
       set<Id> tempLocIds = new Set<Id>();
       Map<id,id> LocOldAccountMap = new Map<id,id>();
       Map<id,string> OldAccountRole = new Map<id,string>();
          
       for(SVMXC__Site__c loc : newLocList ){
              if(loc.SVMX_Update_Account__c == true && loc.SVMXC__Account__c != OldLocMap.get(loc.id).SVMXC__Account__c){
                            LocList.add(loc);
                            updateLocMap.put(loc.id,loc);
                           tempLocIds.add(loc.id);
                            loc.SVMX_Update_Account__c=false;
                  			LocOldAccountMap.put(loc.id,OldLocMap.get(loc.id).SVMXC__Account__c);
                  			OldAccountRole.put(loc.id,loc.SVMX_Old_Account_Role__c);
               }
       }

      while(!tempLocIds.isEmpty()){
              list<SVMXC__Site__c> tempLocList = new list<SVMXC__Site__c> ([select id, SVMXC__Account__c, SVMXC__Account__r.name, SVMXC__Parent__c from SVMXC__Site__c Where  SVMXC__Parent__c in :tempLocIds]);
              tempLocIds.clear();

              for(SVMXC__Site__c loc : tempLocList){
                    tempLocIds.add(loc.id);
                    childLocList.add(loc);
                    updateLocMap.put(loc.id,loc);
                    LocOldAccountMap.put(loc.id,LocOldAccountMap.get(loc.SVMXC__Parent__c));
                  	OldAccountRole.put(loc.id,OldAccountRole.get(loc.SVMXC__Parent__c));
              }
              tempLocList.clear();
      }
      
      allLocList.addAll(LocList);
	  allLocList.addAll(childLocList);

       if(allLocList.size() > 0){
           for(SVMXC__Site__c loc: childLocList){
                if(updateLocMap.containsKey(loc.SVMXC__Parent__c)){
               		loc.SVMXC__Account__c = updateLocMap.get(loc.SVMXC__Parent__c).SVMXC__Account__c;
                    updateLoc.add(loc);
                    RelAccountList.add(new SVMX_Related_Account__c(name = loc.SVMXC__Account__r.name, SVMX_Location__c=loc.Id,SVMX_Account__c=LocOldAccountMap.get(loc.id), SVMX_Accounts_Role__c = OldAccountRole.get(loc.id), SVMX_Date_Ended__c =system.today()));
                }
           }
               
              
           		Map<id,string> locAccNameMap = new Map<id,string>();
              for(SVMXC__Installed_Product__c ip: SVMX_InstalledproductDataManager.IPQuery(allLocList)){
                      if(updateLocMap.containsKey(ip.SVMXC__Site__c)){
                                 ip.SVMXC__Company__c = updateLocMap.get(ip.SVMXC__Site__c).SVMXC__Account__c;
                                 updateIPs.add(ip);
                          		 locAccNameMap.put(ip.SVMXC__Site__c, ip.SVMXC__Site__r.SVMXC__Account__r.name);
                       }                 
              } 
           
           for(SVMXC__Site__c loc:LocList){
						loc.SVMXC__Parent__c = null;
                        RelAccountList.add(new SVMX_Related_Account__c(name= locAccNameMap.get(loc.id), SVMX_Location__c=loc.Id,SVMX_Account__c=OldLocMap.get(loc.id).SVMXC__Account__c,SVMX_Accounts_Role__c = loc.SVMX_Old_Account_Role__c, SVMX_Date_Ended__c =system.today()));
                       
              } 
             
        }
       if(updateLoc.size() > 0){
           SVMX_LocationDataManager.updateLoc(updateLoc);
       }
        if(RelAccountList.size() > 0){
                    SVMX_LocationDataManager.inserRelAcc(RelAccountList);
        }        
        if(updateIPs.size() > 0){
                  SVMX_LocationDataManager.updateIP(updateIPs);
        }  
    }
}