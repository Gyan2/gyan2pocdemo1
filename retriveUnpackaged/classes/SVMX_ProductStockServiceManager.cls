/************************************************************************************************************
Description: Service Manager for Product Stock. All the methods and the logic for Product Stock object resides here.

Dependancy: 
 
Author: Ranjitha S
Date: 22-02-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

**************************************************************************************************/

public class SVMX_ProductStockServiceManager {
    
    public static String Parts= Label.Parts;
    public static String Available= Label.Available;
    public static String increase= Label.Increase;
    public static String decrease= Label.Decrease;
    public static String UsedNotConsu= Label.Used_Not_Consumed;
    public static id usageRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('UsageConsumption','SVMXC__Service_Order_Line__c');
    public static id stockHistoryRecordType = SVMX_RecordTypeDataManager.GetRecordTypeID('Stock_History','SVMXC__Stock_History__c');
        
    public static void updateProductStock(List<SVMXC__Service_Order_Line__c> newPartsList, List<SVMXC__Service_Order_Line__c> rejectedWDList){
       
        Map<id,decimal> partQtyMap = new Map<id,decimal>();
        List<SVMXC__Product_Stock__c> stkList = new List<SVMXC__Product_Stock__c>();
        set<id> techLoc = new set<id>();
        List<SVMXC__Service_Order_Line__c> workDetailList = new list<SVMXC__Service_Order_Line__c>();
        set<string> status = new set<string>();
        List<SVMXC__Stock_History__c> stkHistoryList = new List<SVMXC__Stock_History__c>();
        
        status.add(Available);
        status.add(UsedNotConsu);
        if(newPartsList != null)
            workDetailList = [select id, SVMXC__Line_Type__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__c, SVMXC__Product__c , SVMXC__Actual_Quantity2__c  from SVMXC__Service_Order_Line__c  where SVMXC__Line_Type__c =: parts and id in :newPartsList and recordtypeid = :usageRecordTypeId];  
        if(rejectedWDList != null)
            workDetailList = [select id, SVMXC__Line_Type__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__c, SVMXC__Product__c , SVMXC__Actual_Quantity2__c  from SVMXC__Service_Order_Line__c  where SVMXC__Line_Type__c =: parts and id in :rejectedWDList and recordtypeid = :usageRecordTypeId];  
        
        if(!workDetailList.isEmpty()){
            for(SVMXC__Service_Order_Line__c wd: workDetailList){
                partQtyMap.put(wd.SVMXC__Product__c, wd.SVMXC__Actual_Quantity2__c);
                techLoc.add(wd.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c);
            }
        }
        if(!partQtyMap.isEmpty())
            stkList = [select id, SVMXC__Product__c ,SVMXC__Location__c , SVMXC__Quantity2__c , SVMXC__Status__c from SVMXC__Product_Stock__c  where SVMXC__Product__c in :partQtyMap.keySet() and  SVMXC__Location__c  in :techLoc and SVMXC__Status__c in :status];
        
        Map<id,Map<string,SVMXC__Product_Stock__c>> locProductStkMap = new Map<id,Map<string,SVMXC__Product_Stock__c>>();
        for(SVMXC__Product_Stock__c stk: stkList){
            Map<string,SVMXC__Product_Stock__c> tempMap = new Map<string,SVMXC__Product_Stock__c>();
            String prodStatus = stk.SVMXC__Product__c+'-'+stk.SVMXC__Status__c;
            if(locProductStkMap.containsKey(stk.SVMXC__Location__c)){
                tempMap = locProductStkMap.get(stk.SVMXC__Location__c);
            }
            tempMap.put(prodStatus,stk);
            locProductStkMap.put(stk.SVMXC__Location__c,tempMap);
        }
        
        List<SVMXC__Product_Stock__c> upsertProdStckList = new List<SVMXC__Product_Stock__c>();
        for(SVMXC__Service_Order_Line__c wd: workDetailList){
            String prodAvailable = wd.SVMXC__Product__c+'-'+Available;
            String prodUsedNotConsumed = wd.SVMXC__Product__c+'-'+UsedNotConsu;
            if(locProductStkMap.containsKey(wd.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c)){
                Map<string,SVMXC__Product_Stock__c> tempMap = locProductStkMap.get(wd.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c);
                SVMXC__Product_Stock__c notconsumedProdStk = new SVMXC__Product_Stock__c();
                SVMXC__Product_Stock__c availableProdStk = new SVMXC__Product_Stock__c();
                
                if(newPartsList != null){ 
                    //Check for Used Not Consumed
                    system.debug(' ### workorder ');
                    if(tempMap.containskey(prodUsedNotConsumed)){
                        notconsumedProdStk = tempMap.get(prodUsedNotConsumed);
                        notconsumedProdStk.SVMXC__Quantity2__c = notconsumedProdStk.SVMXC__Quantity2__c + wd.SVMXC__Actual_Quantity2__c;
                    }else{
                        notconsumedProdStk.SVMXC__Quantity2__c = wd.SVMXC__Actual_Quantity2__c;
                        notconsumedProdStk.SVMXC__Location__c = wd.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c;
                        notconsumedProdStk.SVMXC__Product__c = wd.SVMXC__Product__c;
                        notconsumedProdStk.SVMXC__Status__c = UsedNotConsu;
                    }
                    //Check for Available
                    if(tempMap.containskey(prodAvailable)){
                        availableProdStk = tempMap.get(prodAvailable);
                        availableProdStk.SVMXC__Quantity2__c = availableProdStk.SVMXC__Quantity2__c - wd.SVMXC__Actual_Quantity2__c;
                    }
                    stkHistoryList.add(createStockHistory(availableProdStk,notconsumedProdStk,availableProdStk.SVMXC__Quantity2__c + wd.SVMXC__Actual_Quantity2__c, availableProdStk.SVMXC__Quantity2__c, decrease));
                    stkHistoryList.add(createStockHistory(notconsumedProdStk,availableProdStk,notconsumedProdStk.SVMXC__Quantity2__c - wd.SVMXC__Actual_Quantity2__c, notconsumedProdStk.SVMXC__Quantity2__c, increase));
               }
                if(rejectedWDList != null){
                    system.debug(' ### workdetail ');
                    if(tempMap.containskey(prodUsedNotConsumed)){
                        notconsumedProdStk = tempMap.get(prodUsedNotConsumed);
                        notconsumedProdStk.SVMXC__Quantity2__c = notconsumedProdStk.SVMXC__Quantity2__c - wd.SVMXC__Actual_Quantity2__c;
                    }
                    //Check for Available
                    if(tempMap.containskey(prodAvailable)){
                        availableProdStk = tempMap.get(prodAvailable);
                        availableProdStk.SVMXC__Quantity2__c = availableProdStk.SVMXC__Quantity2__c + wd.SVMXC__Actual_Quantity2__c;
                    }
                    stkHistoryList.add(createStockHistory(availableProdStk,notconsumedProdStk,availableProdStk.SVMXC__Quantity2__c - wd.SVMXC__Actual_Quantity2__c, availableProdStk.SVMXC__Quantity2__c, increase));
                    stkHistoryList.add(createStockHistory(notconsumedProdStk,availableProdStk,notconsumedProdStk.SVMXC__Quantity2__c + wd.SVMXC__Actual_Quantity2__c, notconsumedProdStk.SVMXC__Quantity2__c, decrease));
                }
                upsertProdStckList.add(notconsumedProdStk);
                upsertProdStckList.add(availableProdStk);
            }    
        }
        
        if(!upsertProdStckList.isEmpty()) 
            upsert upsertProdStckList;
        
        if(!stkHistoryList.isEmpty())
            insert stkHistoryList;
        
    }
    
    public static SVMXC__Stock_History__c createStockHistory(SVMXC__Product_Stock__c trgps, SVMXC__Product_Stock__c srcps, decimal bq, decimal aq, string changeType){
        
        if(trgps.SVMXC__Quantity2__c == null)
            trgps.SVMXC__Quantity2__c = 0;
        
        SVMXC__Stock_History__c sh = new SVMXC__Stock_History__c();
        sh.RecordTypeId = stockHistoryRecordType; 
        sh.SVMXC__Changed_By__c = UserInfo.getUserId(); 
        // sh.SVMXC__Location__c = trgps.SVMXC__Location__c; 
        sh.SVMXC__Product__c = trgps.SVMXC__Product__c;
        
        sh.SVMXC__Product_Stock__c = trgps.id;                                           
        sh.SVMXC__Status__c = trgps.SVMXC__Status__c;
        sh.SVMXC__Quantity_after_change2__c =  aq; 
        sh.SVMXC__Quantity_before_change2__c = bq; 
        sh.SVMXC__Transaction_Type__c = 'Work Order Immediate Consumption';
        sh.SVMXC__Date_Changed__c = system.now();
        if(changeType == increase){
            sh.SVMXC__Transaction_Quantity2__c = aq - bq;
            sh.SVMXC__Change_Type__c = increase; 
            sh.SVMXC__From_Product_Stock__c = srcps.id;
            sh.SVMXC__To_Product_Stock__c = trgps.id;
        }
        else{
            sh.SVMXC__Transaction_Quantity2__c  = bq - aq;
            sh.SVMXC__Change_Type__c = decrease;
            sh.SVMXC__From_Product_Stock__c = trgps.id;
            sh.SVMXC__To_Product_Stock__c = srcps.id;
        }
        return sh;
    }
}