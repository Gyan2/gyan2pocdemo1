public with sharing class ProjectServices {

    public static void calculateRollUps(List<Project__c> Records){

        for(Project__c forProject : Records) {
            forProject.TotalAmount__c = 0;
        }
        Map<Id,Project__c> projects = new Map<Id, Project__c>(Records);

        for(Opportunity forOpp : [SELECT convertCurrency(Amount) convertedAmount, ProjectRef__c FROM Opportunity WHERE ProjectRef__c IN :Records]) {
            projects.get(forOpp.ProjectRef__c).TotalAmount__c += (Decimal)forOpp.get('convertedAmount');
        }

    }

    public static void handleAddressChangesFromBuilding(List<Project__c> Records,Map<Id,Project__c> existingRecords){
        Set<Id> projectIds = new Set<Id>();
        for(Project__c project : Records){
            if(existingRecords == null && project.BuildingRef__c <> null){
                projectIds.add(project.Id);
            }else if(existingRecords <> null && project.BuildingRef__c <> existingRecords.get(project.Id).BuildingRef__c){
                projectIds.add(project.Id);
            }
        }

        if(projectIds.size() > 0){
            List<Project__c> projects = [select
                    id,City__c,Street__c,Zip__c,Country__c,CountryIsoCode__c,
                    BuildingRef__c,BuildingRef__r.City__c,BuildingRef__r.Street__c,BuildingRef__r.Zip__c,BuildingRef__r.Country__c,BuildingRef__r.CountryIsoCode__c
            from Project__c where Id in : projectIds
            ];

            for(Project__c project : projects){
                project.CountryIsoCode__c = project.BuildingRef__r.CountryIsoCode__c;
                project.Street__c = project.BuildingRef__r.Street__c;
                project.City__c = project.BuildingRef__r.City__c;
                project.Zip__c = project.BuildingRef__r.Zip__c;
                project.Country__c = project.BuildingRef__r.Country__c;
            }

            update projects;
        }



    }


}