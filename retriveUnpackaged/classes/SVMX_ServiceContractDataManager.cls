/************************************************************************************************************
Description: Data Manager for Service Contract. All the SOQL queries and DML Operations are performed in this class.

Dependency: 
 
Author: Ranjitha S
Date: 17-10-2017

Modification Log: 
Date            Author          Modification Comments

--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_ServiceContractDataManager {
    
    public static list<SVMXC__Service_Contract__c> serviceContractQuery(set<string> countrySet, id nonContractRecordTypeId){
        List<SVMXC__Service_Contract__c> nonContractList = [select Id, name, SVMXC__Start_Date__c,SVMXC__End_Date__c,SVMXC__Company__r.BillingCountry, SVMXC__Service_Level__c  from SVMXC__Service_Contract__c where SVMXC__Active__c=true and RecordTypeId=:nonContractRecordTypeId and SVMXC__Start_Date__c<=:system.today() and SVMXC__End_Date__c>=:system.today() and SVMXC__Company__r.BillingCountry IN :countrySet];
        return nonContractList;
    }

    public static map<id,SVMXC__Service_Contract__c> rateTierQuery(set<id> serviceContractId){
        map<id,SVMXC__Service_Contract__c> serviceConMap = new map<id,SVMXC__Service_Contract__c>([select Id,(SELECT id,SVMX_Business_Hours__c, SVMX_Rate__c,SVMX_Out_Of_Hours__c FROM Rate_Tiers__r ), (select id,SVMX_Product_Family__c, SVMX_Contract__c from Product_Surcharges__r )  from SVMXC__Service_Contract__c where id IN :serviceContractId]);
        return serviceConMap;
    }
    
    public static map<id,SVMXC__Service_Contract__c> cliQuery(set<id> scIds){
        map<id,SVMXC__Service_Contract__c> cliContractMap = new map<id,SVMXC__Service_Contract__c>([select Id,SVMX_Contract_Line_Items_Total_Price__c, (select id, SVMX_Service_Contract__c, SVMX_Line_Price__c, SVMX_Line_Qty__c, SVMX_Total_Line_Price__c  from  Contract_Line_Items__r )  from SVMXC__Service_Contract__c where id IN :scIds]);
        return cliContractMap;
    }
    
    public static List<SVMXC__Service_Contract__c> servContractList(Id scRecTypeId, String contractQuotationNumber){
        List<SVMXC__Service_Contract__c> scList = [SELECT id,Name,SVMXC__Service_Plan__r.Name,SVMXC__Contact__r.Email, SVMXC__Contract_Price2__c, SVMX_Contract_Quotation_No__c, SVMX_Contract_Status__c, SVMX_Opportunity__c, SVMXC__Contact__r.Name, SVMXC__Contact__r.SVMX_Roles__c,
                                        SVMXC__Company__c, SVMXC__Contact__c,
                                (Select Id,Name From Attachments ORDER BY CreatedDate DESC) 
                                FROM SVMXC__Service_Contract__c WHERE SVMX_Contract_Quotation_No__c = :contractQuotationNumber 
                                AND RecordTypeId = :scRecTypeId];
        
        for(SVMXC__Service_Contract__c sc: scList ){
            system.debug('Contact email '+sc.SVMXC__Contact__r.Email);
            System.debug('SVMXC__Contact__r.SVMX_Roles__c-->'+sc.SVMXC__Contact__r.SVMX_Roles__c);
        }
            System.debug('scList-->'+scList);
            return scList;
    }
    
    public static List<SVMXC__Service_Contract__c> pmOfferingQuery(List<SVMXC__Service_Contract__c> sclist){
        List<SVMXC__Service_Contract__c> scPMOfferingList = [SELECT id,(Select Id,Name, SVMXC__PM_Plan_Template__c  From SVMXC__PM_Offerings__r) 
                                FROM SVMXC__Service_Contract__c WHERE id in: sclist];
                                
        return scPMOfferingList;
    }
    
    public static map<id,SVMXC__Service_Contract__c> serviceContractQueryIntegn(set<ID> serviceContractIds){
        map<id,SVMXC__Service_Contract__c> serviceContractMap = new map<id,SVMXC__Service_Contract__c>([select Id,Name, SVMXC__Company__c ,SVMXC__Company__r.SAPNumber__c, SVMX_Country__c, SVMXC__Start_Date__c, SVMX_GlobalContract_Number__c, SVMXC__End_Date__c, SVMX_SAP_Contract_Number__c from SVMXC__Service_Contract__c where Id IN :serviceContractIds]);
        return serviceContractMap;
    }
    
     public static void delPMOffering(List<SVMXC__PM_Offering__c> delPMoff){
        delete delPMoff;
    }   
    
     public static void insertPMoffering(List<SVMXC__PM_Offering__c> newPMOfList){
        insert newPMOfList;
    }   
    
    
    public static void updateContractLIPrice(List<SVMXC__Service_Contract__c> contractList){
        update contractList;
    }
}