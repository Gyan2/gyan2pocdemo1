/************************************************************************************************************
Description:.

Dependency: 
 
Author: Tulsi B R
Date: 16-01-2018

Modification Log: 
Date            Author          Modification Comments

--------------------------------------------------------------
 
*******************************************************************************************************/

global class SVMX_UpdateWorkOrderStatusBatch  implements Database.Batchable<sObject>,Database.Stateful  {
  
  
 public string gupdateWorkOrderQuery;

 global Database.QueryLocator start(Database.BatchableContext BC) {
   
   List<String> enabledCountries=new List<String>();
   List<SVMX_Custom_Feature_Enabler__c> allCountry = SVMX_Custom_Feature_Enabler__c.getAll().values();
   for(SVMX_Custom_Feature_Enabler__c  country:allCountry){
        if(country.Auto_Verification__c == true){
             enabledCountries.add(country.Name);
        }
   }
   Map<String,String>  fieldstoQuery=SVMX_UpdateWorkOrderStatusBatchHelper.allfieldstoQuery(enabledCountries);
   String workOrdreStatus=system.Label.Status_Work_Complete;
   String accountbilling='Automatic';
   DateTime rightNow =  DateTime.now();
   DateTime d1hAgo = rightNow.addHours(-1);
   /*List<String>  whereCon=new List<String>();

   List<String> lineType = fieldstoQuery.get('Line Type').split(', ');
   if(lineType!=null){
   for(string strWher:lineType){
     
     if(strWher!=''){
     whereCon.add(strWher);}
   }
   }*/
   String workorderfields=fieldstoQuery.get('Work Order');
   //String workdetailfields=fieldstoQuery.get('Work Details');
 
   gupdateWorkOrderQuery='Select Id,Name,SVMXC__Country__c,SVMXC__Order_Status__c';
   if(workorderfields!=null){
   //gupdateWorkOrderQuery+=''+fieldstoQuery.get('Work Order');
   }

   /*gupdateWorkOrderQuery+=' , (Select id,SVMXC__Line_Type__c'; 
   if(workdetailfields!=null){
   gupdateWorkOrderQuery+=''+fieldstoQuery.get('Work Details');
   }
   gupdateWorkOrderQuery+=' From SVMXC__Service_Order_Line__r';
   if(whereCon!=null && !whereCon.IsEmpty()){
   gupdateWorkOrderQuery+=' where SVMXC__Line_Type__c IN(\'' + string.join(New List<String>(whereCon),'\',\'') + '\')';
   }
   gupdateWorkOrderQuery+=' )';*/
   gupdateWorkOrderQuery+=' From SVMXC__Service_Order__c';
   gupdateWorkOrderQuery+=' Where SVMXC__Order_Status__c =:workOrdreStatus';
   gupdateWorkOrderQuery+=' AND SVMX_Auto_Verified__c =false';
   
   gupdateWorkOrderQuery+=' AND SVMXC__Country__c IN(\'' + string.join(New List<String>(enabledCountries),'\',\'') + '\')';
   gupdateWorkOrderQuery+=' AND SVMXC__Company__r.SVMX_Automatic_billing__c=:accountbilling';
   //gupdateWorkOrderQuery+=' AND CreateDate <:'+d1hAgo ;
   
   system.debug('====>>>  gupdateWorkOrderQuery'+gupdateWorkOrderQuery);
  
   return Database.getQueryLocator(gupdateWorkOrderQuery);
 
 }
 global void execute(Database.BatchableContext BC, List<SVMXC__Service_Order__c> workOrders) {
 
 
       system.debug('workOrders 88888'+workOrders);
       set<ID> woIds =new set<Id>();


       for(SVMXC__Service_Order__c wo:  workOrders)
        woIds.add(wo.id);
       
       List<String> enabledCountries=new List<String>();
       List<SVMX_Custom_Feature_Enabler__c> allCountry = SVMX_Custom_Feature_Enabler__c.getAll().values();
       for(SVMX_Custom_Feature_Enabler__c  country:allCountry){
          if(country.Auto_Verification__c == true){
             enabledCountries.add(country.Name);
          }
       }
       List<SVMX_Billing_Rules__c> billingRules;
       set<String>   billingRulefields=new set<String>();
       set<String>   lineType=new set<String>();
       set<String>   WDbillingRulefields=new set<String>();
       String wdFields ='';
        List<String>  whereCon=new List<String>();
        Map<Id,List<SVMXC__Service_Order_Line__c>> MapOfWorkDetails =new  Map<Id,List<SVMXC__Service_Order_Line__c>>();
       String queryBillingRule='Select Id,SVMX_Country__c,SVMX_Field__c,SVMX_Object__c,SVMX_Value__c,SVMX_Line_Type__c from SVMX_Billing_Rules__c';
       queryBillingRule+=' where SVMX_Country__c IN(\'' + string.join(New List<String>(enabledCountries),'\',\'') + '\')';
       billingRules=Database.query(queryBillingRule);     
       for(SVMX_Billing_Rules__c   billingRule:billingRules){
           if(billingRule.SVMX_Object__c =='Work Order'){
            billingRulefields.add(billingRule.SVMX_Field__c);
           }

           if(billingRule.SVMX_Object__c =='Work Details'){
                if(!WDbillingRulefields.contains(billingRule.SVMX_Field__c)){
                    
                    wdFields += ','+billingRule.SVMX_Field__c;
                    WDbillingRulefields.add(billingRule.SVMX_Field__c);
                }
                lineType.add(billingRule.SVMX_Line_Type__c);
            }
       }
       //add query to get work details and its related info
        if(lineType!=null){
           for(string strWher:lineType){
             
             if(strWher!=''){
             whereCon.add(strWher);}
           }
        }
       String wdQuery = null;
       if(wdFields !=null && wdFields.length() > 0){
            wdQuery = 'Select id,SVMXC__Service_Order__c,SVMXC__Line_Type__c '+wdFields+' From SVMXC__Service_Order_Line__c where SVMXC__Service_Order__c IN :woIds and SVMXC__Line_Type__c IN(\'' + string.join(New List<String>(whereCon),'\',\'') + '\')';
             system.debug('wd query '+wdQuery);
            List<SVMXC__Service_Order_Line__c> workDetails = Database.Query(wdQuery);
            for(SVMXC__Service_Order_Line__c wd:workDetails){
                if(MapOfWorkDetails.containsKey(wd.SVMXC__Service_Order__c)){
                    List<SVMXC__Service_Order_Line__c> newWd=MapOfWorkDetails.get(wd.SVMXC__Service_Order__c);
                    newwd.add(wd);
                    MapOfWorkDetails.put(wd.SVMXC__Service_Order__c, newwd);

                }else{
                    MapOfWorkDetails.put(wd.SVMXC__Service_Order__c, new List<SVMXC__Service_Order_Line__c> {wd});
                }
                
            }

        }

       boolean serviceOrderLineFlag=true;
       List<SVMXC__Service_Order__c> updatedWorkOrderList=new List<SVMXC__Service_Order__c>();
       for(SVMXC__Service_Order__c workOrder:workOrders){
       
            boolean workOrderflag=SVMX_UpdateWorkOrderStatusBatchHelper.billingRuleValidation(workOrder,billingRules);
          
            if(workOrderflag){
                /*if(workOrder.SVMXC__Service_Order_Line__r.size()>0){
                 for(SVMXC__Service_Order_Line__c  workdetail:workOrder.SVMXC__Service_Order_Line__r){
                    serviceOrderLineFlag=SVMX_UpdateWorkOrderStatusBatchHelper.billingRuleWorkDetails(workdetail,billingRules);
                 }
                }*/

                if(MapOfWorkDetails.size() > 0 && MapOfWorkDetails.containsKey(workOrder.id)){
                    for(SVMXC__Service_Order_Line__c  workdetail:MapOfWorkDetails.get(workOrder.id)){
                    serviceOrderLineFlag=SVMX_UpdateWorkOrderStatusBatchHelper.billingRuleWorkDetails(workdetail,billingRules);
                 }
                }
             
                 if(serviceOrderLineFlag){
                    workOrder.SVMXC__Order_Status__c=system.Label.Ready_To_Bill;
                    workOrder.SVMX_Auto_Verified__c=true;
                    updatedWorkOrderList.add(workOrder);
                 } 
           }
       }
      
       if(updatedWorkOrderList.size()>0){
          SVMX_WorkOrderDataManager.updateWO(updatedWorkOrderList);
       }
 }
 
 global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
 }
 
}