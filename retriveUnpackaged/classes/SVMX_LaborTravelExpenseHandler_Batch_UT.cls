@isTest
public class SVMX_LaborTravelExpenseHandler_Batch_UT { 

    static testMethod void testPartsPriceChangeHandlerBatch(){

        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'WorkOrder';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
        
        SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
              intg.name= 'SAP PO';
              intg.WS_USER__c ='test';
              intg.WS_PASS__c ='test';
        insert intg;

        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;
        
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers',false);
        prod.SAPNumber__c= '1234567';
        insert prod;
        
        Product2 prod1 = SVMX_TestUtility.CreateProduct('Door', false);
        prod1.SAPNumber__c= '1234567';
        insert prod1;
        
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
        
        Case cs = SVMX_TestUtility.CreateCase(acc.Id,con.Id, 'New', false);
        cs.SVMX_Service_Sales_Order_Number__c ='7928374982';
        cs.SVMX_Awaiting_SAP_Response__c = false;
        
        
      
        insert cs;
         
        Case cs1 = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', False);
        cs1.Type ='Quoted Works';
        cs1.SVMX_Service_Sales_Order_Number__c = '';
        cs1.SVMX_Awaiting_SAP_Response__c = false;
       
        insert cs1;
        
        Case cs2 = SVMX_TestUtility.CreateCase(acc.Id,con.Id, 'New', false);
        //cs2.SVMX_Service_Sales_Order_Number__c =null;
        cs2.SVMX_Service_Sales_Order_Number__c ='6846957';
        cs2.SVMX_Awaiting_SAP_Response__c = True;
      
        insert cs2;
       
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id,true);
        
        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        SVMX_Rate_Tiers__c rt2 = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, true, true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Germany',true);
        
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Installation','Usage/Consumption',sc.Id,tech.Id,cs.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.today();
        wo.SVMX_Service_Number_Blank_Check__c =false;
        wo.SVMX_Service_Sales_Order_Number__c = '6846957';
        wo.SVMXC__Order_Type__c='Installation';
        
        insert wo;
        
        SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Installation','Usage/Consumption',sc.Id,tech.Id,cs1.id,false);
        wo1.SVMXC__Scheduled_Date_Time__c = system.today();
        wo1.SVMX_Service_Sales_Order_Number__c = '6846957';
        wo1.SVMXC__Order_Type__c='Installation';
       
        insert wo1;
        
        SVMXC__Service_Order__c wo2 = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Installation','Usage/Consumption',sc.Id,tech.Id,cs2.id,false);
        wo2.SVMXC__Scheduled_Date_Time__c = system.today();
        wo2.SVMX_Service_Number_Blank_Check__c =false;
        wo2.SVMX_Service_Sales_Order_Number__c = '6846957';
        wo2.SVMXC__Order_Type__c='Installation';
        
        //wo.SVMX_Service_Sales_Order_Number__c = '6846957';
        insert wo2;
      
        list<SVMXC__Service_Order_Line__c> wol = new list<SVMXC__Service_Order_Line__c>();
        
        SVMXC__Service_Order_Line__c oli = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id,prod.Id,'Usage/Consumption',wo.Id,system.now(), sc.Id, tech.Id, cs.id,'Labor','Warranty',false);
        //oli.SVMXC__Actual_Price2__c =4;
        oli.SVMXC__Actual_Quantity2__c = 2;
        oli.SVMXC__Billable_Quantity__c = 1;
        oli.SVMX_PM_Charge__c = 'yes';
        oli.SVMX_Sales_Order_Item_Number__c='';
        oli.SVMX_Awaiting_SAP_Response__c=false;
        //oli.SVMXC__End_Date_and_Time__c = system.today();
        oli.SVMX_Parts_Price_Changed__c=true;
        oli.SVMX_Chargeable_Qty__c =3;
        wol.add(oli);
        
        SVMXC__Service_Order_Line__c oli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id,prod.Id,'Usage/Consumption',wo1.Id,system.now(), sc.Id, tech.Id, cs1.id,'Travel','Warranty',false);
        //oli1.SVMXC__Actual_Price2__c =5;
        oli1.SVMXC__Actual_Quantity2__c = 2;
        oli1.SVMX_Sales_Order_Item_Number__c='';
        oli1.SVMX_Awaiting_SAP_Response__c=true;
        oli1.SVMXC__Start_Date_and_Time__c = system.today();
        oli1.SVMXC__End_Date_and_Time__c = system.today()+1;
        oli1.SVMXC__Billable_Quantity__c = 7;
        oli1.SVMX_Parts_Price_Changed__c=true;
        oli1.SVMX_PM_Charge__c = 'yes';
        oli1.SVMX_Chargeable_Qty__c =3;
        wol.add(oli1);
        
        SVMXC__Service_Order_Line__c oli2 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id,prod.Id,'Usage/Consumption',wo2.Id,system.now(), sc.Id, tech.Id, cs2.id,'Expenses','Warranty',false);
        //oli2.SVMXC__Actual_Price2__c =3;
        oli2.SVMXC__Actual_Quantity2__c = 2;
        oli2.SVMX_Sales_Order_Item_Number__c='';
        oli2.SVMX_Awaiting_SAP_Response__c =false;
        oli2.SVMX_PM_Charge__c = 'yes';
        oli2.SVMXC__Billable_Quantity__c=2;
        oli2.SVMX_Parts_Price_Changed__c=true;
        oli2.SVMX_Chargeable_Qty__c =3;
        wol.add(oli2);
        insert wol;

        Test.startTest();

            Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
            SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
            SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder salesOrderreq=new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder();
            SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
            requstelemnt.CreateSync(messageheader,salesOrderreq);

            Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
            SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader1=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
            SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder salesOrderreq1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder();
            SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
            requstelemnt1.UpdateSync(messageheader1,salesOrderreq1);
            
            SVMX_LaborTravelExpenseHandler_Batch obj = new SVMX_LaborTravelExpenseHandler_Batch();
            DataBase.executeBatch(obj);

        Test.stopTest();
    }       
 }