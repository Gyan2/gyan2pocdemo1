/*******************************************************************************************************
Description: Trigger handler for Preventive Maintenance Trigger

Dependancy:     
 
Author: Pooja Singh
Date: 09-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_PmTriggerHandler extends SVMX_TriggerHandler{
    private list<SVMXC__PM_Plan__c> newPMList;
    private list<SVMXC__PM_Plan__c> oldPMList;
    private Map<Id, SVMXC__PM_Plan__c> newPMMap;
    private Map<Id, SVMXC__PM_Plan__c> oldPMMap;
        
    public SVMX_PmTriggerHandler() {
        this.newPMList = (list<SVMXC__PM_Plan__c>) Trigger.new;
        this.oldPMList = (list<SVMXC__PM_Plan__c>) Trigger.old;
        this.newPMMap = (Map<Id, SVMXC__PM_Plan__c>) Trigger.newMap;
        this.oldPMMap = (Map<Id, SVMXC__PM_Plan__c>) Trigger.oldMap;
    }
    
    public override void beforeInsert(){
        for(SVMXC__PM_Plan__c pm : newPMList){
            if(pm.SVMX_Activated_Batch__c == false && pm.SVMXC__Coverage_Type__c == 'Product (Must Have IB)'){
                pm.SVMX_Activated_Batch__c = true;
            }
        }
    }
}