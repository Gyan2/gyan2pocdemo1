/************************************************************************************************************
Description: Test class for SVMX_ActivatedContractsPmPlanBatch class
 
Author: Pooja Singh
Date: 27-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest
public class SVMX_ActivatedContractsPmPlanBatch_UT { 
 
    static testMethod void testActivateContrPmPlanBatch()
    {
       
        Account acc = SVMX_TestUtility.CreateAccount('Test account', 'United Kingdom', true);
        Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('test location', acc.Id, true);

        //Create product
        Product2 p = SVMX_TestUtility.CreateProduct('Test Product', true);

        //Create IP
        SVMXC__Installed_Product__c ip = SVMX_TestUtility.CreateInstalledProduct(acc.Id, p.Id, loc.Id, true);
        SVMXC__Installed_Product__c ip1 = SVMX_TestUtility.CreateInstalledProduct(acc.Id, p.Id, loc.Id, true);
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Service Contract', 'Global Std', con.Id, true);

        SVMXC__PM_Plan__c pmPlan = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
        pmPlan.SVMXC__Coverage_Type__c = 'Product (Must Have IB)';
        insert pmPlan;
		
        List<SVMXC__PM_Coverage__c> pmcList = new List<SVMXC__PM_Coverage__c>();
        SVMXC__PM_Coverage__c pmc = new SVMXC__PM_Coverage__c(SVMXC__PM_Plan__c = pmPlan.Id, SVMXC__Product_Name__c = ip.Id);
        SVMXC__PM_Coverage__c pmc1 = new SVMXC__PM_Coverage__c(SVMXC__PM_Plan__c = pmPlan.Id, SVMXC__Product_Name__c = ip1.Id);
        pmcList.add(pmc);
        pmcList.add(pmc1);
        insert pmcList;

        Test.startTest();

            SVMX_ActivatedContractsPmPlanBatch obj = new SVMX_ActivatedContractsPmPlanBatch();
            DataBase.executeBatch(obj);

        Test.stopTest();
    }
}