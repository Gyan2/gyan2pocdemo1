public class ProductPortfolios extends CoreSObjectDomain {
    public ProductPortfolios(List<ProductPortfolio__c> sObjectList){
        super(sObjectList);
    }

    public override void onApplyDefaults() {
        // Apply defaults to Products

    }

    public override void onValidate() {
        // Validate Products
    }

    public override void onValidate(Map<Id,SObject> existingRecords) {
        // Validate changes to Products

    }

    public override void onBeforeInsert(){
        for (ProductPortfolio__c forPP : (List<ProductPortfolio__c>)Records) {
            forPP.AccountId__c = forPP.AccountRef__c;
        }
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {

    }

    public override void onBeforeDelete(){

    }

    public override void onAfterInsert(){

    }


    public override void onAfterUpdate(Map<Id,SObject> existingRecords){

    }

    public override void onAfterDelete(){

    }


    public override void onAfterUndelete(){

    }


    public class Constructor implements CoreSObjectDomain.IConstructable
    {
        public CoreSObjectDomain construct(List<SObject> sObjectList)
        {
            return new ProductPortfolios(sObjectList);
        }
    }
}