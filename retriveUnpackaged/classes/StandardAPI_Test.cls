/*  The aim of this class is not to test any functionality, but to have a reference to the fields that are used
    through the Standard API (Enterprise / Partner), so we do know when we might be breaking something.
 */

@isTest
private class StandardAPI_Test {
    private static void SAPPO_Project() {
        System.debug(
            [
                SELECT
                    ExternalId__c,
                    SAPNumber__c,
                    SAPInstance__c,
                    Name,
                    CountryIsoCode__c,
                    LanguageIsoCode__c,
                    City__c,
                    Street__c,
                    Zip__c
                FROM Project__c
                LIMIT 1
        ]);
    }
}