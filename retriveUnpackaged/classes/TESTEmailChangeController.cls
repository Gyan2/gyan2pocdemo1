/**
 * Created by rebmangu on 01/02/2018.
 */

@isTest
private with sharing class TESTEmailChangeController {


    @testSetup static void generateData(){

        Profile profileRecord = [Select Id from profile where Name='System Administrator'];
        User u1 = new User(
                Email='test=test.com@example.com',
                Username='test@test.com.blabla',
                Alias='Test1',
                EmailEncodingKey = 'ISO-8859-1',
                lastName = 'Test',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                ProfileId = profileRecord.Id
        );
        insert u1;

        User u2 = new User(
                Email='test2@test2.com',
                Username='test2@test2.com.blabla',
                Alias='Test2',
                EmailEncodingKey = 'ISO-8859-1',
                lastName = 'Test',
                TimeZoneSidKey = 'America/Los_Angeles',
                LocaleSidKey = 'en_US',
                LanguageLocaleKey = 'en_US',
                ProfileId = profileRecord.Id
        );
        insert u2;
    }

    @isTest static void testUserChanges(){
        System.assertEquals(EmailChangeController.JUST_FIXED, EmailChangeController.changeUserEmail('test@test.com'));
        System.assertEquals(EmailChangeController.ERROR, EmailChangeController.changeUserEmail('test@test2.com'));
        System.assertEquals(EmailChangeController.ALREADY_FIXED, EmailChangeController.changeUserEmail('test2@test2.com'));
    }
}