@isTest
public class SVMX_TimeEntryTrggrHandler_UT {
    
    private static Id testGroupMemberId;

    private static Id userId;

    private static Id workOrderId;
    
    public static testMethod void timeEntryTrigTest() {
    	Profile systemAdminProfile = [SELECT Id FROM Profile WHERE Name='System Administrator'];

        BusinessHours businessHoursRec = [SELECT Id from BusinessHours where IsDefault = true];
        
        //Inserting Users
        User techUser = new User();
        techUser.Alias = 'smax-xxx';
        techUser.Email='standarduser@testorg.com'; 
        techUser.EmailEncodingKey='UTF-8'; 
        techUser.LastName='Testing';
        techUser.LanguageLocaleKey='en_US'; 
        techUser.LocaleSidKey='en_US';
        techUser.ProfileId = systemAdminProfile.Id;
        techUser.TimeZoneSidKey='America/Los_Angeles';
        techUser.UserName='standard@testorg.servicemax.com';
        techUser.SalesOrg__c='9999';
        insert techUser;

        userId = techUser.Id;

        Account testAccount = new Account();
        testAccount.Name = 'Apex Test Account 1';
        insert testAccount;

        Contact testContact = new Contact();
        testContact.LastName = 'TestUser';
        testContact.Phone = '732-222-2222';
        testContact.Language__c='EN';
        insert testContact;

        SVMXC__Site__c testSite = new SVMXC__Site__c();
        testSite.SVMXC__Account__c = testAccount.Id;
        testSite.SVMXC__State__c = 'NY';
        testSite.SVMXC__City__c = 'New York';
        testSite.SVMXC__Country__c = 'United States';
        insert testSite;

        Product2 testProduct = new Product2();
        testProduct.Name = 'Prod';
        insert testProduct;

        SVMXC__Installed_Product__c testIP = new SVMXC__Installed_Product__c();
        testIP.SVMXC__Product__c = testProduct.Id;
        testIP.SVMXC__Date_Installed__c = Date.newInstance(2016, 10, 19);
        insert testIP;

        SVMXC__Service_Group__c testServiceGroup = new SVMXC__Service_Group__c();
        testServiceGroup.Name = 'testGroupName';
        insert testServiceGroup;

        SVMXC__Service_Group_Members__c testGroupMember = new SVMXC__Service_Group_Members__c();
        testGroupMember.Name = 'testGroupMem';
        testGroupMember.SVMXC__Service_Group__c = testServiceGroup.Id;
        testGroupMember.SVMXC__Salesforce_User__c = techUser.Id;
        if(businessHoursRec != null){  
            testGroupMember.SVMXC__Working_Hours__c = businessHoursRec.Id;
        }
        insert testGroupMember;

        testGroupMemberId = testGroupMember.Id;
		SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(testAccount.id, 'Test Contract', 'Global Std', testContact.id, true)  ;
        
        SVMXC__Service_Order__c serviceOrder1 = new SVMXC__Service_Order__c();
        serviceOrder1.SVMXC__State__c = 'NJ';
        serviceOrder1.SVMXC__Country__c = 'Germany' ;
        serviceOrder1.SVMXC__Service_Contract__c = sc.id ;
        serviceOrder1.SVMXC__Purpose_of_Visit__c = System.Label.Technician_Onsite ;
        serviceOrder1.SVMXC__Site__c = testSite.Id;
        serviceOrder1.SVMXC__Component__c = testIP.Id;
        serviceOrder1.SVMXC__Company__c = testAccount.Id;
        serviceOrder1.SVMXC__Contact__c = testContact.Id;
        serviceOrder1.SVMXC__Group_Member__c = testGroupMember.Id;
        serviceOrder1.SVMXC__Order_Status__c = 'Open';
        insert serviceOrder1;

        workOrderId = serviceOrder1.Id;
        
        Schema.DescribeSObjectResult workDetailObj = Schema.SObjectType.SVMXC__Service_Order_Line__c;
        Map<String,Schema.RecordTypeInfo> workDetailObjRecordTypeMap = workDetailObj.getRecordTypeInfosByName();
        Id recTypeId = workDetailObjRecordTypeMap.get('Usage/Consumption').getRecordTypeId();
        
        DateTime startTime = DateTime.newInstance(2018, 5, 16, 8, 30, 0) ;
            
        DateTime closeTime = DateTime.newInstance(2018, 5, 16, 12, 30, 0)  ;
            

        DateTime startTime2 = DateTime.newInstance(2018, 5, 17, 8, 30, 0) ;
        DateTime closeTime2 = DateTime.newInstance(2018, 5, 17, 12, 30, 0) ;
        
        SVMXC__Service_Order_Line__c orderLine1 = new SVMXC__Service_Order_Line__c();
        orderLine1.RecordTypeId = recTypeId;
        orderLine1.SVMXC__Line_Type__c = System.Label.Labor;
        orderLine1.SVMXC__Service_Order__c = workOrderId;
        orderLine1.SVMXC__Start_Date_and_Time__c = startTime;
        orderLine1.SVMXC__End_Date_and_Time__c = closeTime;
        orderLine1.SVMXC__Group_Member__c = testGroupMemberId;
        orderLine1.SVMXC__Work_Description__c = 'Test';
        
        insert orderLine1 ;
        
        SVMXC__Timesheet__c timesheet = SVMX_TestUtility.CreateTimeSheet('open', true );    
        SVMXC__Timesheet_Day_Entry__c td= new SVMXC__Timesheet_Day_Entry__c();
        td.SVMXC__Timsheet_Day__c =startTime.date();
            td.SVMXC__Timesheet__c =timesheet.Id;
            insert td;
        
         SVMXC__Timesheet_Day_Entry__c td2= new SVMXC__Timesheet_Day_Entry__c();
        td2.SVMXC__Timsheet_Day__c =startTime2.date();
            td2.SVMXC__Timesheet__c =timesheet.Id;
            insert td2;
        SVMXC__Timesheet_Entry__c te= new SVMXC__Timesheet_Entry__c();
            
            te.SVMX_Overlaps__c = 'No';
            te.SVMXC__Start_Time__c=startTime;
            te.SVMXC__End_Time__c =closeTime;
        te.SVMXC__Work_Detail__c = orderLine1.id ;
            te.SVMXC__Timesheet_Day__c= td.id;
            te.SVMXC__Timesheet__c=timesheet.id;
        	
            insert te;
            te.SVMXC__Start_Time__c=startTime2;
            te.SVMXC__End_Time__c =closeTime2;
            //te.SVMX_Overlaps__c = 'Yes';
              update te;
    }
}