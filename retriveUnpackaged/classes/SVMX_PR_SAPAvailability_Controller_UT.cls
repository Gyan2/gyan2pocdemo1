/************************************************************************************************************
Description: UT for SVMX_PR_SAPAvailability_Controller class.
 
Author: Keshava Prasad
Date: 19-04-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest(SeeAllData = true)
public class SVMX_PR_SAPAvailability_Controller_UT {
    
    public static testMethod void checkSapAvailability() {
    Account account = SVMX_TestUtility.CreateAccount('Testing Account', 'Norway',true) ;
            account.SAPNumber__c = 'P1234' ;
        update account ;
            
    Contact contact = SVMX_TestUtility.CreateContact(account.id,true) ;
    
        
         SVMXC__Site__c  location = SVMX_TestUtility.CreateLocation('Service Location',account.id,true);
        location.SVMXC_SAP_Storage_Location__c = '1234667';
        location.SVMXC_Plant_Number__c = 'P122';
        location.SVMXC__Stocking_Location__c = true;
        location.SVMXC__Location_Type__c = 'External';
        location.SVMXC__Country__c = 'Norway' ;
        //location.id = partsReq.SVMX_From_SAP_Storage_Location__c ;
        update location;
        
         SVMXC__Site__c  stockLocation = SVMX_TestUtility.CreateLocation('From Location',account.id,true);
        stockLocation.SVMXC_SAP_Storage_Location__c = '112345644';
        stockLocation.SVMXC_Plant_Number__c = 'T677';
        stockLocation.SVMXC__Stocking_Location__c = true;
        stockLocation.SVMXC__Country__c = 'Norway' ;
        stockLocation.SVMXC__Location_Type__c = 'External';
       update stockLocation;
        
        
        
        SVMXC__Parts_Request__c  partsReq = new SVMXC__Parts_Request__c () ;
        partsReq.SVMXC__Requested_From__c = location.id ;
        partsReq.SVMXC__Required_At_Location__c = stockLocation.id ;
        partsReq.SVMX_From_SAP_Storage_Location__c = location.id ;
        insert partsReq ;
        
        SVMX_Sales_Office__c salesOffice = new SVMX_Sales_Office__c ();
        salesOffice.SVMX_Price_Order_Account__c = account.id ;
        salesOffice.SVMX_Country__c = 'Germany' ;
        insert salesOffice ;
    
    
    
    SVMXC__Installed_Product__c installedProduct = SVMX_TestUtility.CreateInstalledProduct(account.id,null,location.id,true);
        Product2 product = SVMX_TestUtility.CreateProduct('Test prod',true);
        product.SVMX_SAP_Product_Type__c = 'Stock Part';
        Update product;
    
    Profile profile = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
      User user = SVMX_TestUtility.CreateUser('Testtech', profile.id, true);
      SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Svmx Tech2', user.id, null, true);
        tech.SVMXC__Inventory_Location__c = location.id;
        update tech;

        


        SVMXC__Parts_Request_Line__c partsReqLine = new SVMXC__Parts_Request_Line__c() ;
        partsReqLine.SVMXC__Product__c = product.id ;
        partsReqLine.SVMXC__Quantity_Required2__c = 1 ;
        partsReqLine.SVMX_Request_Lines_Converted__c = false ;
        partsReqLine.SVMXC__Line_Status__c = 'Open' ;
        partsReqLine.SVMXC__Parts_Request__c = partsReq.id ;
        
        insert partsReqLine;



       List<SVMXC__Product_Stock__c> psList = new List<SVMXC__Product_Stock__c>();
       SVMXC__Product_Stock__c ps = new SVMXC__Product_Stock__c();
       ps.SVMXC__Product__c = product.id;
       ps.SVMXC__Status__c = 'Available';
       ps.SVMXC__Quantity2__c = 2;
       ps.SVMXC__Location__c = stockLocation.id;
       insert ps;
        
       List<SVMX_PR_SAPAvailability_Controller.WrapPR> wrap = new List<SVMX_PR_SAPAvailability_Controller.WrapPR>() ;
       PageReference pageRef = Page.SVMX_PR_SAPAvailability ;
         pageRef.getParameters().put('partReqId', partsReq.id) ;
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        
        //Test.setMock(WebServiceMock.class, new SAPPurchaseRequestUtilityMock());
        /*SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader();
            SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest purchaseRequest = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest();
            SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort requstelemnt=new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort();
            requstelemnt.CreateSync(messageheader,purchaseRequest);*/
            
        try{
            Test.setMock(WebServiceMock.class, new SalesOrderSimulateRequestMockImpl(
                new List<Map<String,String>>{
                        new Map<String,String>{
                                'internalId'=>'00000001',
                                'quantity' => '10',
                                'netAmount' => String.valueOf(500*10)
                        }
                }
            ));
        
        SAPPO_CommonDataTypes.BasicMessageHeader MessageHeader1 =new SAPPO_CommonDataTypes.BasicMessageHeader();
        SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder SalesOrder = new SAPPO_SalesOrderRequest.SalesOrderRequestSalesOrder();
        SAPPO_SalesOrderRequest.SalesOrderOutPort requstelemnt1=new SAPPO_SalesOrderRequest.SalesOrderOutPort();
        requstelemnt1.SimulateSync(messageheader1,SalesOrder);   
            
         SVMX_PR_SAPAvailability_Controller sapAvailability = new SVMX_PR_SAPAvailability_Controller() ;
        sapAvailability.partReqLineWrap.get(0).availableFrmLocName = location.Name ;
       // sapAvailability.partReqLineWrap.get(0).avlQty = '1';
        sapAvailability.partReqLineWrap.get(0).availableFrmLoc = location.id;
        
        
        
     
                 
        sapAvailability.chkbox();
        sapAvailability.ch();
        sapAvailability.change();
        sapAvailability.ChkAvailability(); 
        sapAvailability.toLocChange();
        sapAvailability.Back();
        sapAvailability.partReqLineWrap.get(0).availableFrmLocName = location.Name ;
        sapAvailability.partReqLineWrap.get(0).avlQty = '1';
        sapAvailability.partReqLineWrap.get(0).availableFrmLoc = location.id;
        sapAvailability.partReqLineWrap.get(0).avlDate = String.valueOf(system.today().addDays(1));
        sapAvailability.PartOrder();
        }
        
        catch(Exception e){}
        
        Test.stopTest();
        
       
    
    
    }
}