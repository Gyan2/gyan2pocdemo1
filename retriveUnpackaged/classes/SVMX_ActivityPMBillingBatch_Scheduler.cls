global class SVMX_ActivityPMBillingBatch_Scheduler implements Schedulable {
    // Execute at regular intervals
    global void execute(SchedulableContext sc){
      SVMX_ActivityPMBillingBatch acpBatch = new SVMX_ActivityPMBillingBatch();
      Database.executebatch(acpBatch, 20);
    }
}