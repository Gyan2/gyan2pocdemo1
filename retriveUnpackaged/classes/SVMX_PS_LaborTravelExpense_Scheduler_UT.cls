@istest
public with sharing class SVMX_PS_LaborTravelExpense_Scheduler_UT {
    static testmethod void testLaborTravelExpense() {
        Test.startTest();
        SVMX_PS_LaborTravelExpense_Scheduler  obj = new SVMX_PS_LaborTravelExpense_Scheduler();
        obj.execute(null);
        Test.stopTest();
    }

}