public without sharing class Locations extends CoreSObjectDomain{
    public static Boolean deactivateMirroring = !Test.isRunningTest() && Boolean.valueOf(Label.TMP_MirrorEngineTest);
    public static Boolean disableTriggerCRUDSecurity = false;

    public Locations(List<Location__c> sObjectList) {
        super(sObjectList);
        if (disableTriggerCRUDSecurity) configuration.disableTriggerCRUDSecurity();
    }

    public override void onBeforeInsert() {
        if (deactivateMirroring != true) doMirroring();
    }

    public override void onAfterInsert() {
        if (deactivateMirroring != true) {
            List<SVMXC__Site__c> sites = new List<SVMXC__Site__c>();
            for (Location__c forLocation : (List<Location__c>)Records) {
                sites.add(new SVMXC__Site__c(Id = forLocation.SVMXC_SiteRef__c, LocationRef__c = forLocation.Id, LocationId__c = forLocation.Id));
            }
            SVMXC_Sites.deactivateMirroring = true;
            SVMXC_Sites.disableTriggerCRUDSecurity = true;
            update sites;
        }
    }

    public override void onBeforeUpdate(Map<Id,sObject> existingRecords) {
        if (deactivateMirroring != true) doMirroring();
    }

    private void doMirroring(){
        List<SVMXC__Site__c> mirrorsToUpsert = new List<SVMXC__Site__c>();

        MirrorService ms = new MirrorService.DirectMirror('Location__c', 'SVMXC__Site__c');
        mirrorsToUpsert.addAll((List<SVMXC__Site__c>)ms.doMirroring(Records, SVMXC__Site__c.fields.LocationId__c));

        // TODO: database.upsert to check for errors
        SVMXC_Sites.deactivateMirroring = true;
        SVMXC_Sites.disableTriggerCRUDSecurity = true;
        if (Trigger.IsInsert) upsert mirrorsToUpsert;
        else upsert mirrorsToUpsert LocationId__c;

        Integer i = 0;
        for (Location__c forLocation : (List<Location__c>)Records){
            forLocation.SVMXC_SiteRef__c = forLocation.SVMXC_SiteId__c = mirrorsToUpsert.get(i++).Id;
        }
    }

    public class Constructor implements CoreSObjectDomain.IConstructable{
        public CoreSObjectDomain construct(List<sObject> sObjectList) {
            return new Locations(sObjectList);
        }
    }
}