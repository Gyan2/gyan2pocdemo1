public class SVMX_PartsOrderLineDataManager {  

    public static id shipmentRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Shipment','SVMXC__RMA_Shipment_Line__c');
     
     public static list<SVMXC__RMA_Shipment_Line__c> ParOrdLineQuery(set<id> ParOrdLineIdset){
        list<SVMXC__RMA_Shipment_Line__c> ParOrdLinelist = new list<SVMXC__RMA_Shipment_Line__c> ([select id,Name,SVMXC__RMA_Shipment_Order__c,SVMX_Sales_Order_Item_Number__c, 
                                                      SVMXC__Discount_Percentage__c ,SVMXC__Line_Price2__c,SVMXC__Delivery_Location__r.SVMXC_Plant_Number__c,
                                                      SVMXC__Expected_Quantity2__c,SVMXC__Product__r.SVMXC__Unit_Of_Measure__c, 
                                                      SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__r.SVMXC_Plant_Number__c,  
                                                      SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__r.SVMXC_SAP_Storage_Location__c, 
                                                      SVMXC__Product__r.SAPNumber__c, SVMXC__RMA_Shipment_Order__r.SVMXC__Source_Location__r.SVMXC_Plant_Number__c, 
                                                      SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Location__r.SVMXC_SAP_Storage_Location__c, SVMXC__Expected_Receipt_Date__c,
                                                      SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Street__c,SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_City__c,
                                                      SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_State__c,SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Zip__c,
                                                      SVMXC__RMA_Shipment_Order__r.SVMXC__Destination_Country__c from SVMXC__RMA_Shipment_Line__c  
                                                      Where Id in:ParOrdLineIdset]);
        return ParOrdLinelist;
    } 

    public static list<SVMXC__RMA_Shipment_Line__c> PartOrdIdParOrdLineQuery(set<id> ParOrdIdset){
        list<SVMXC__RMA_Shipment_Line__c> ParOrdLinelist = new list<SVMXC__RMA_Shipment_Line__c> ([select id,Name,SVMXC__RMA_Shipment_Order__c,SVMX_Sales_Order_Item_Number__c 
                                                       from SVMXC__RMA_Shipment_Line__c  
                                                      Where SVMXC__RMA_Shipment_Order__c in:ParOrdIdset]);
        return ParOrdLinelist;
    } 
     
     
    public static list<SVMXC__RMA_Shipment_Line__c> ParOrdLineBlankcheckQuery(set<id> ParOrdIdset){
        list<SVMXC__RMA_Shipment_Line__c> ParOrdLinelist = new list<SVMXC__RMA_Shipment_Line__c> ([select id,Name,SVMXC__RMA_Shipment_Order__c,SVMXC__Delivery_Location__r.SVMXC_Plant_Number__c,SVMXC__Expected_Quantity2__c,SVMXC__Product__r.SVMXC__Unit_Of_Measure__c from SVMXC__RMA_Shipment_Line__c  Where SVMXC__RMA_Shipment_Order__c in:ParOrdIdset AND SVMX_Item_Number_Blank_Check__c = true AND RecordTypeId =: shipmentRecordTypeId]);
        return ParOrdLinelist;
    }

    public static void updatepartsOrderline(list<SVMXC__RMA_Shipment_Line__c> PartsOrderLineList){
        if(!PartsOrderLineList.isEmpty()){
            update PartsOrderLineList;
        }    
    }         
}