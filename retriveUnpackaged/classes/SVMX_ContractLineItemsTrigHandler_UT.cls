@isTest
public class SVMX_ContractLineItemsTrigHandler_UT{

     static testMethod void invokeContractLineItemsTrigHandler() { 
     
     SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'ServiceContract';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
              
        SVMX_SAP_Integration_UserPass__c cs1 = new SVMX_SAP_Integration_UserPass__c();
              cs1.name ='SAP PO';
              cs1.WS_USER__c ='test';
              cs1.WS_PASS__c ='test';
        insert cs1;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig1= new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig1';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Contract Line Item';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMX_Line_Qty__c';
        insert trigconfig1;
         
      Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
      
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
           
        Case cs = SVMX_TestUtility.CreateCase(acc.Id, con.Id, 'New', true);
       
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id,false);
        sc.SVMXC__Start_Date__c = Date.newInstance(2018, 1, 1);
        sc.SVMXC__End_Date__c = Date.newInstance(2019, 12, 10);
        insert sc;
     
        
        //SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id, loc.Id, prod.Id, 'open', 'Reactive','Global Std', sc.Id, tech.Id, cs.id, true);
        
        Date startDate=system.today();
        Date startDateSec;
        if(system.today().day() > 15){
         startDate=system.today();
        }else{
         startDateSec=Date.newInstance(2018, 12, 9); //system.today().addDays(-2);
        }
        SVMX_Contract_Line_Items__c serContrProduct=SVMX_TestUtility.CreateContractLineItem(sc.Id,'Monthly on First of Month',startDate,false);
        serContrProduct.SVMX_Contract_Start__c=system.today();
        insert serContrProduct;
        
        //SVMXC__Service_Contract_Products__c serContrProductthree=SVMX_TestUtility.CreateServiceContractProduct(sc.Id,'Monthly on First of the Month',startDateSec,true);
       // serContrProductthree.SVMX_Contract_Start_Date__c=startDateSec;
        
        SVMX_Contract_Line_Items__c serContrProductSec=SVMX_TestUtility.CreateContractLineItem(sc.Id,'Yearly on Last of Start month',startDateSec,true);
        serContrProductSec.SVMX_Line_Price__c =500;
        serContrProductSec.SVMX_Contract_Start__c=startDateSec;
        //CreateServiceContract
        Test.startTest();
       
        Test.setMock(WebServiceMock.class, new SAPServiceContractUtilityMock());
        SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader messageheader = new SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract serviceContractUpdatereq = new SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract();
        SVMX_wwwDormakabaComContract.ServiceContractOutPort requstelemnt = new SVMX_wwwDormakabaComContract.ServiceContractOutPort();
        requstelemnt.UpdateSync(messageheader,serviceContractUpdatereq);
        
        update serContrProduct;
        update serContrProductSec;
        delete serContrProductSec;
        //update serContrProductthree;
    
        // System.assertEquals(,);
        Test.stopTest();
      }


}