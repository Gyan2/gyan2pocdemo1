/**
 * Created by trakers on 14.06.18.
 */

@IsTest
private class TestSAPPODocumentService {
    static testMethod void TestGetInvoicePayload() {

        Test.setMock(WebServiceMock.class, new SAPPO_DocumentRequestMockImpl(
                        new Map<String,String>{
                                'Id'=>'123456',
                                'Payload'=> 'blablablablablablablablablabla'
                        }
        ));

        Test.startTest();

        SAPPO_DocumentService.InvoiceResult result = InvoiceTableController.downloadFileFromSAPPO('123456');
        System.debug(result);
        Map<String,Object> jsonResult = (Map<String,Object>)JSON.deserializeUntyped(result.data);
        System.assertEquals('blablablablablablablablablabla',(String)jsonResult.get('Payload'));

        Test.stopTest();
    }
}