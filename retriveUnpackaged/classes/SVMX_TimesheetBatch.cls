/*******************************************************************************************************
Description: Batch class for creating daily, weekly and monthly timesheets

Dependancy: 
    Batch class: SVMX_Daily_TimesheetBatch
	Batch class: SVMX_Weekly_TimesheetBatch
    Batch class: SVMX_Monthly_TimesheetBatch
	Class: SVMX_TimesheetBatch_Controller
 
Author: Pooja Singh
Date: 14-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

global class SVMX_TimesheetBatch implements Database.Batchable<sObject>, Database.Stateful {
    
    global Set<String> dailyCountrySet = new Set<String>();
    global Set<String> weeklyCountrySet = new Set<String>();
    global Set<String> monthlyCountrySet = new Set<String>();
    global List<String> dailyProfileList = new List<String>();
    global List<String> weeklyProfileList = new List<String>();
    global List<String> monthlyProfileList = new List<String>();

    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
         
        String query = 'SELECT Id, Name, SVMX_Frequency__c, SVMX_Profiles__c FROM SVMX_Timesheet_Configuration__c';
        return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<SVMX_Timesheet_Configuration__c> tsConfigList) {
         
        for(SVMX_Timesheet_Configuration__c tsc : tsConfigList)
        {     
            if(tsc.SVMX_Frequency__c == 'Daily'){
                String profStr = tsc.SVMX_Profiles__c;
                dailyProfileList = profStr.split(';');
                dailyCountrySet.add(tsc.name);
                system.debug('Set of Daily countrys '+dailyCountrySet);
            }
            if(tsc.SVMX_Frequency__c == 'Weekly'){
                String profStr = tsc.SVMX_Profiles__c;
                weeklyProfileList = profStr.split(';');
                weeklyCountrySet.add(tsc.name); 
                system.debug('Set of Weekly countrys '+weeklyCountrySet);
            }
            if(tsc.SVMX_Frequency__c == 'Monthly'){
                String profStr = tsc.SVMX_Profiles__c;
                monthlyProfileList = profStr.split(';');
                monthlyCountrySet.add(tsc.name); 
                system.debug('Set of Monthly countrys '+monthlyCountrySet);
            }
        }
    }   
     
    global void finish(Database.BatchableContext BC) {
        system.debug('In finish');
       	system.debug('dailyCountrySet list'+dailyCountrySet);
            if(dailyCountrySet.size() > 0){
                Database.executeBatch(new SVMX_Daily_TimesheetBatch(dailyCountrySet, dailyProfileList));
            }
            if(weeklyCountrySet.size() > 0){
                Database.executeBatch(new SVMX_Weekly_TimesheetBatch(weeklyCountrySet, weeklyProfileList));
            }
			Integer cuWeekOfMonth = Math.ceil((Double)(system.today().Day()) / 7).intValue();	//count of current week in a month
        	Date date1 = System.Date.today().toStartOfMonth();
			Date date2 =date1.addMonths(1).addDays(-1);
			Integer wksInMonth = date1.daysBetween(date2) /7;	//number of weeks in a month
        	//Runs only on last week of every month
            if(monthlyCountrySet.size() > 0 && cuWeekOfMonth == wksInMonth){
                Database.executeBatch(new SVMX_Monthly_TimesheetBatch(monthlyCountrySet, monthlyProfileList));
            }
        
    }    
}