@isTest
public class TEST_SapPoUpload {
    
    private static Opportunity op;
    private static Contact primContact;
    private static void setup()
    {
        TestData.setupOpportunityAccountContact();
        
        op = TestData.opportunity;
        op.zOfferCreationDate__c = Date.today();
        update op;
        
        //Primary contact role
        primContact = TestData.contact;        
        OpportunityContactRole r1 = new OpportunityContactRole(Opportunityid = op.Id, Contactid = primContact.Id, isPrimary = true);
        insert r1;
        
        //Team Member 1,2,3 for kabaContact 1,2,3
        List<User> userlist = [Select Id from User Where IsActive = true limit 3];        
        OpportunityTeamMember ot1 = new OpportunityTeamMember(UserId = userlist[0].Id, OpportunityId = op.Id, TeamMemberRole = 'Sales Rep');
        insert ot1;        
        if(userlist.size()>1)
        {
            OpportunityTeamMember ot2 = new OpportunityTeamMember(UserId = userlist[1].Id, OpportunityId = op.Id, TeamMemberRole = 'Project Manager');
            insert ot2;
        }
        if(userlist.size()>2)
        {
            OpportunityTeamMember ot3 = new OpportunityTeamMember(UserId = userlist[2].Id, OpportunityId = op.Id, TeamMemberRole = 'Back Office');
            insert ot3;
        }
        
        //Partner accounts and contact roles for kabaContact 5,6
        
        Account a2 = TestData.createAccount('TestAccountPartner1',true);
        Contact c2 = TestData.createContact('TestPartnerContactLastName1', 'EN', a2.id, true);
        Account a3 = TestData.createAccount('TestAccountPartner2',true);       
        Contact c3 = TestData.createContact('TestPartnerContactLastName2', 'EN', a3.id, true);
        
        Partner opartner1 = new Partner(AccountToId = a2.id, OpportunityId = op.Id);
        insert opartner1;
        Partner opartner2 = new Partner(AccountToId = a3.id, OpportunityId = op.Id);
        insert opartner2;
              
        OpportunityContactRole r2 = new OpportunityContactRole(Opportunityid = op.Id, Contactid = c2.Id, isPrimary = false);
        insert r2;      
        OpportunityContactRole r3 = new OpportunityContactRole(Opportunityid = op.Id, Contactid = c3.Id, isPrimary = false);
        insert r3;
        
    }
    
    @isTest
    public static void testUpload()
    {
        setup();     
        SapPoUpload.sendMessage(op.Id,null);
    }
}