@isTest(seeAllData = True)
public class SVMX_SAPPurchaseRequestUtility_UT {

    static testMethod void PurchaseRequestCreate(){

        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'PurchaseRequisition';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
        try{
        insert mycs;
        }catch(Exception e){}
        
        SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
              intg.name= 'SAP PO';
              intg.WS_USER__c ='test';
              intg.WS_PASS__c ='test';
        
        
        try{
        insert intg;
        }catch(Exception e){}

        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers',false);
        prod.SAPNumber__c= '1234567';
        insert prod;
        
        Product2 prod1 = SVMX_TestUtility.CreateProduct('Door', false);
        prod1.SAPNumber__c= '1234567';
        insert prod1;
        
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
        
        Case cs = SVMX_TestUtility.CreateCase(acc.Id,con.Id, 'New', false);
        cs.SVMX_Service_Sales_Order_Number__c ='';
        cs.SVMX_Awaiting_SAP_Response__c = false;
        cs.SVMX_Invoice_Status__c='Requested on all cases';
        insert cs;
              
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id,true);
       
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Reactive','Global Std',sc.Id,tech.Id,cs.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.today();
        wo.SVMX_Service_Sales_Order_Number__c = '6846957';
        insert wo;

        SVMXC__RMA_Shipment_Order__c partsOrder1=new SVMXC__RMA_Shipment_Order__c();
        partsOrder1.SVMXC__Company__c=acc.id;
        partsOrder1.SVMXC__Contact__c=con.id;
        partsOrder1.SVMXC__Service_Order__c = wo.id;
        //partsOrder1.SVMX_Purchase_Requisition_Number__c =74387048;
        partsOrder1.SVMX_Awaiting_SAP_Response__c =false;
        partsOrder1.SVMXC__Order_Status__c='open';
        partsOrder1.SVMX_Service_Sales_Order_Number__c = '123454';
        partsOrder1.SVMX_Awaiting_SAP_Response__c = false;
        partsOrder1.SVMXC__Expected_Receive_Date__c=Date.Today();
        insert partsOrder1;

        SVMXC__RMA_Shipment_Line__c partsline=new SVMXC__RMA_Shipment_Line__c();
        partsline.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
        partsline.SVMXC__RMA_Shipment_Order__c=partsOrder1.id;
        partsline.SVMXC__Product__c=prod.id;       
        partsline.SVMXC__Expected_Quantity2__c=1;
        partsline.SVMXC__Line_Status__c='open';
        partsline.SVMXC__Expected_Receipt_Date__c=Date.Today();
        partsline.SVMXC__Expected_Condition__c='Defective';
        partsline.SVMXC__Disposition__c='Repair';

        test.startTest();
            
            Test.setMock(WebServiceMock.class, new SAPPurchaseRequestUtilityMock());
            SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaPurchase.BusinessDocumentMessageHeader();
            SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest purchaseRequest = new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestCreateRequestPurchaseRequest();
            SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort requstelemnt=new SVMX_wwwDormakabaSalesPurchase.PurchaseRequestOutPort();
            requstelemnt.CreateSync(messageheader,purchaseRequest);

            insert  partsline;

        test.stopTest();

    }
}