//spareParts shipment Manger calls the Future handler methods
public class SVMX_SparePartsShipmentManager{

    public static void handleSAPSalesOrderCreation(list<SVMXC__RMA_Shipment_Line__c> partsOrderLineList){

        Set<Id> validPartsOrderLineIdSet = new Set<Id>();
        Set<Id> existingPartsOrderLineIdSet = new Set<Id>();
        Set<Id> partsOrderIds = new Set<Id>();
        Set<Id> partsOrderItemCheckIds = new Set<Id>();

        List<SVMXC__RMA_Shipment_Line__c> blankCheckUpdates = new List<SVMXC__RMA_Shipment_Line__c>();
        List<SVMXC__RMA_Shipment_Line__c> poLineList = new List<SVMXC__RMA_Shipment_Line__c>();


        if(partsOrderLineList != null && !partsOrderLineList.isEmpty()){
              
            System.debug('SVMX_SAPSalesOrderUtility : Handle Sales Order Creation for SpareParts');

            for(SVMXC__RMA_Shipment_Line__c partOrderLine : partsOrderLineList) {

                partsOrderIds.add(partOrderLine.SVMXC__RMA_Shipment_Order__c);
            }

            Map<Id,SVMXC__RMA_Shipment_Order__c> partsOrderMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>([SELECT Id, Name, SVMX_Service_Sales_Order_Number__c,
                                                                                SVMXC__Case__c, SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c,
                                                                                SVMX_Awaiting_SAP_Response__c
                                                                                FROM SVMXC__RMA_Shipment_Order__c
                                                                                WHERE Id IN : partsOrderIds]);
            for(SVMXC__RMA_Shipment_Line__c partsordline : partsOrderLineList){
                
                //Scenario 1 - New Shipment Parts Order Line created. Sales Order request not yet made to SAP    
                if(partsOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Service_Sales_Order_Number__c == null
                     && partsOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Awaiting_SAP_Response__c == false) {

                    System.debug('SVMX_SparePartsShipmentManager :' + partsordline.Name + ' is ready for SAP Sales Order Creation for SpareParts');
                    validPartsOrderLineIdSet.add(partsordline.Id);
                }

                //Scenraio 2 - New PO Shipment Line created. Sales Order Create request already made, but no Sales Order Number yet returned
                else if(partsOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Service_Sales_Order_Number__c == null
                        && partsOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Awaiting_SAP_Response__c == true) {

                        System.debug('SVMX_SparePartsShipmentManager :' + partsordline.Name + ' is waiting for SAP Sales Order Number. Setting SVMX_Item_Number_Blank_Check__c to True');
                        
                        poLineList.add(partsordline);
                        partsOrderItemCheckIds.add(partsordline.Id);   
                }

                //Scenario 3 - New PO Shipment Line created. Sales Order Number already exists on Parts Order
                else if(partsOrderMap.get(partsordline.SVMXC__RMA_Shipment_Order__c).SVMX_Service_Sales_Order_Number__c != null) {
                    // These PartsOrderLine Details need to sent as Update, although they are new. This is because SAP considers
                    // these as updates to an existing Sales Order, and not a create
                    System.debug('SVMX_SparePartsShipmentManager :' + partsordline.Name + ' is ready to be added to existing Sales Order - Update request will be sent to SAP, not Create');
                    existingPartsOrderLineIdSet.add(partsordline.Id);       
                }
            }
        

            if(!poLineList.isEmpty()) {
                        
                for(SVMXC__RMA_Shipment_Line__c pol : [SELECT Id,Name,SVMX_Item_Number_Blank_Check__c 
                                                       FROM SVMXC__RMA_Shipment_Line__c 
                                                       WHERE Id IN : partsOrderItemCheckIds]) {
                                                                                
                    pol.SVMX_Item_Number_Blank_Check__c = true;
                    blankCheckUpdates.add(pol);
                }
            }   

            if(!validPartsOrderLineIdSet.isEmpty()) {
                        
                SVMX_SAPSalesOrderUtility.soapRequestForSAPSalesOrderSparePartsShipmentCreation(validPartsOrderLineIdSet);    
            }

            if(!existingPartsOrderLineIdSet.isEmpty()) {

                SVMX_SAPSalesOrderUtility.soapRequestForSAPSalesOrderSparePartsShipmentupdate(existingPartsOrderLineIdSet,true);
            }

            if(!blankCheckUpdates.isEmpty()) {
                update blankCheckUpdates;
            }    
        }else{
            System.debug('SVMX_SAPSalesOrderUtil : No Parts Detail Line for SalesOrder SpareParts Creation');
        }    
    }

    public static void handleSAPSalesOrderHeaderUpdates(Map<Id, SVMXC__RMA_Shipment_Order__c> newPartsOrderMap,Map<Id, SVMXC__RMA_Shipment_Order__c> oldPartsOrderMap){
       
        if(newPartsOrderMap.keyset() != null) {
                    
            System.debug('SVMX_SAPSalesOrderUtility : Handle Sales Order spare parts Header Update');
            
            String tiggerFieldSplit;
            List<SVMXC__RMA_Shipment_Order__c> updatepartsOrderList = new List<SVMXC__RMA_Shipment_Order__c>();
            Set<Id> sapPartsOrderUpdateSet = new Set<id>();
            Set<Id> sapPartsOrderUpdateSet1 = new Set<id>();
            Set<Id> partsOrderIds = new Set<Id>();

            SVMX_Interface_Trigger_Configuration__c wdtriggerFildQuery = [SELECT id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c 
                                                                          FROM SVMX_Interface_Trigger_Configuration__c 
                                                                          WHERE SVMX_Object__c = 'Parts Order -Spare Parts'];
            
            tiggerFieldSplit = wdtriggerFildQuery.SVMX_Trigger_on_Field__c;
            List<String> triggerFieldTrmList = tiggerFieldSplit.split(';');
            List<String> triggerFieldList = new List<String>();
            
            for(String TRFld:triggerFieldTrmList){
                triggerFieldList.add(TRFld.Trim());
            }
            
            system.debug(triggerFieldList);

            for(Id newPartsOrderId : newPartsOrderMap.keyset()) {

                //Scenario 1 - Sales Order Number already populated on Parts Order
                //A change is made to a Parts Order, which already has an SO Number
                if(oldPartsOrderMap.get(newPartsOrderId).SVMX_Service_Sales_Order_Number__c != null 
                    && newPartsOrderMap.get(newPartsOrderId).SVMX_Service_Sales_Order_Number__c != null) {

                    for(String trigField : triggerFieldList) {
                    
                        if(newPartsOrderMap.get(newPartsOrderId).get(trigField) != oldPartsOrderMap.get(newPartsOrderId).get(trigField)) {

                            System.debug('trigFieldcheck1-->' + trigField);
                            sapPartsOrderUpdateSet.add(newPartsOrderId);
                            break;    
                        }    
                    }    
                }

                //Scenario 2 - Sales Order Number is blank on the Parts Order
                //A Creation request has already been sent to SAP, but we are waiting for the SO Number to return
                else if(newPartsOrderMap.get(newPartsOrderId).SVMX_Service_Sales_Order_Number__c == null
                    && newPartsOrderMap.get(newPartsOrderId).SVMX_Awaiting_SAP_Response__c == true) {

                    for(String trigField : triggerFieldList) {
                    
                        if(newPartsOrderMap.get(newPartsOrderId).get(trigField) != oldPartsOrderMap.get(newPartsOrderId).get(trigField)) {

                            System.debug('trigFieldcheck1-->' + trigField);
                            SVMXC__RMA_Shipment_Order__c parOrdLine = new SVMXC__RMA_Shipment_Order__c(id = newPartsOrderId, SVMX_Sales_Order_Number_Blank_Check__c = true);
                            updatepartsOrderList.add(parOrdLine);
                            break;    
                        }    
                    }     
                }

                //Scenario 3 - Parts Order waiting for Sales Order Number. Now the SO Number has arrived
                //Before the SO Number arrived, changes were made to the Parts Order, causing the Blank Check Flag to be True
                //Need to send the Parts Order for Update to SAP
                else if(oldPartsOrderMap.get(newPartsOrderId).SVMX_Service_Sales_Order_Number__c == null
                    && newPartsOrderMap.get(newPartsOrderId).SVMX_Service_Sales_Order_Number__c != null 
                    && newPartsOrderMap.get(newPartsOrderId).SVMX_Sales_Order_Number_Blank_Check__c == true
                    && newPartsOrderMap.get(newPartsOrderId).SVMX_Awaiting_SAP_Response__c == true) {
                                                                
                    sapPartsOrderUpdateSet.add(newPartsOrderId);
                    SVMXC__RMA_Shipment_Order__c parOrdLine1 = new SVMXC__RMA_Shipment_Order__c(id = newPartsOrderId, SVMX_Sales_Order_Number_Blank_Check__c = false,SVMX_Awaiting_SAP_Response__c = false);
                                                        
                    updatepartsOrderList.add(parOrdLine1);
                }

                //Scenario 4 - Parts Order waiting for Sales Order Number. Now the SO Number has arrived
                //No Interim changes were made before the SO Number arrived. Therefore, no need to send Parts Order for Update to SAP
                if(oldPartsOrderMap.get(newPartsOrderId).SVMX_Service_Sales_Order_Number__c == null 
                    && newPartsOrderMap.get(newPartsOrderId).SVMX_Service_Sales_Order_Number__c != null
                    && newPartsOrderMap.get(newPartsOrderId).SVMX_Sales_Order_Number_Blank_Check__c == false 
                    && newPartsOrderMap.get(newPartsOrderId).SVMX_Awaiting_SAP_Response__c == true ){
                    
                    sapPartsOrderUpdateSet1.add(newPartsOrderId);
                    SVMXC__RMA_Shipment_Order__c parOrdLine1 = new SVMXC__RMA_Shipment_Order__c(id = newPartsOrderId, SVMX_Awaiting_SAP_Response__c = false);
                                                        
                    updatepartsOrderList.add(parOrdLine1);
                }
            }
                                
            if(!sapPartsOrderUpdateSet.isEmpty()) {
               
               SVMX_SAPSalesOrderUtility.soapRequestForSAPSalesOrderSparePartsShipmentHeaderupdate(sapPartsOrderUpdateSet);

               //Check Item Updates
               handleSAPSalesOrdercreationofSparePartsofexistingHeader(sapPartsOrderUpdateSet);
            }


            if(!sapPartsOrderUpdateSet1.isEmpty()) {
               
               handleSAPSalesOrdercreationofSparePartsofexistingHeader(sapPartsOrderUpdateSet1);
            }


            if(!updatepartsOrderList.isEmpty()) {
                        
                update updatepartsOrderList;                
            }
        }      
    }

    public static void handleSAPSalesOrdercreationofSparePartsofexistingHeader(set<Id> partsorderIds) {
            
        List<SVMXC__RMA_Shipment_Line__c> partsOrderLineUpdates = new List<SVMXC__RMA_Shipment_Line__c>();
        Set<Id> polIds = new Set<Id>();
        Set<Id> polWithoutSONumIds = new Set<Id>();
         
        List<SVMXC__RMA_Shipment_Line__c> awaitingSONumPOLineList = [SELECT Id, Name, SVMX_Item_Number_Blank_Check__c, 
                                                                     SVMX_Awaiting_SAP_Response__c, SVMX_Sales_Order_Item_Number__c
                                                                     FROM SVMXC__RMA_Shipment_Line__c
                                                                     WHERE SVMXC__RMA_Shipment_Order__c IN : partsorderIds
                                                                     AND SVMX_Item_Number_Blank_Check__c = true];

        if(!awaitingSONumPOLineList.isEmpty()) {
            
            for(SVMXC__RMA_Shipment_Line__c partsOrderLine : awaitingSONumPOLineList) {
                //Could also check for 'Awaiting SAP Response' Flag instead on SO Number
                if(partsOrderLine.SVMX_Sales_Order_Item_Number__c != null) {

                    polIds.add(partsOrderLine.Id);    
                }
                else {

                    polWithoutSONumIds.add(partsOrderLine.Id);
                }
                
                partsOrderLine.SVMX_Item_Number_Blank_Check__c = false;
                partsOrderLineUpdates.add(partsOrderLine);
            }
        }

        if(polIds.size() > 0) {
            
            SVMX_SAPSalesOrderUtility.soapRequestForSAPSalesOrderSparePartsShipmentupdate(polIds, false);
        }

        if(polWithoutSONumIds.size() > 0) {

            SVMX_SAPSalesOrderUtility.soapRequestForSAPSalesOrderSparePartsShipmentupdate(polWithoutSONumIds, true);    
        }

        if(partsOrderLineUpdates.size() > 0){
            update partsOrderLineUpdates;
        }
    }

    public static void handleSAPSalesOrderItemUpdates(Map<Id, SVMXC__RMA_Shipment_Line__c> newPartsOrderLineMap,Map<Id, SVMXC__RMA_Shipment_Line__c> oldPartsOrderLineMap) {
          
        if(newPartsOrderLineMap.keyset() != null) {
                    
            System.debug('SVMX_SAPSalesOrderUtility : Handle Sales Order spare parts items Update');
            
            String tiggerFieldSplit;
            List<SVMXC__RMA_Shipment_Line__c> updatepartsOrderLineList = new List<SVMXC__RMA_Shipment_Line__c>();
            Set<Id> sapPartsOrderLineUpdateSet = new Set<id>();
            Set<Id> partsOrderIds = new Set<Id>();

            SVMX_Interface_Trigger_Configuration__c wdtriggerFildQuery = [SELECT id, Name, SVMX_Country__c,SVMX_Object__c,
                                                                          SVMX_Trigger_on_Field__c 
                                                                          FROM SVMX_Interface_Trigger_Configuration__c 
                                                                          WHERE SVMX_Object__c = 'Parts Order Line-Spare Parts'];
            
            tiggerFieldSplit = wdtriggerFildQuery.SVMX_Trigger_on_Field__c;
            List<String> triggerFieldTrmList = tiggerFieldSplit.split(';');
            List<String> triggerFieldList = new List<String>();
            
            for(String TRFld:triggerFieldTrmList){
                triggerFieldList.add(TRFld.Trim());
            }
            
            System.debug(triggerFieldList);

            for(SVMXC__RMA_Shipment_Line__c partsOrderLine : newPartsOrderLineMap.values()) {

                partsOrderIds.add(partsOrderLine.SVMXC__RMA_Shipment_Order__c);
            }

            for(SVMXC__RMA_Shipment_Line__c newPartsOrdLine : newPartsOrderLineMap.values()) {
                 
                System.debug('newPODetail.Id' + newPartsOrdLine.Id);
                    
                //Scenario 1 - Sales Order Item Number already populated on Parts Order Line
                //A change is made to the Parts Order Line Trigger field, which already has an SO Item Number
                if(oldPartsOrderLineMap.get(newPartsOrdLine.Id).SVMX_Sales_Order_Item_Number__c != null
                    && newPartsOrdLine.SVMX_Sales_Order_Item_Number__c != null) {

                    for(String trigField : triggerFieldList) {
                    
                        if(newPartsOrderLineMap.get(newPartsOrdLine.Id).get(trigField) != oldPartsOrderLineMap.get(newPartsOrdLine.Id).get(trigField)) {
                        
                            system.debug('trigFieldcheck1-->' + trigField);  
                            sapPartsOrderLineUpdateSet.add(newPartsOrdLine.Id);
                            break;    
                        }                 
                    }    
                }

                //Scenario 2 - Sales Order Item Number is blank on the Parts Order Line
                //A Creation request has already been sent to SAP, but we are waiting for the SO Item Number to return
                //A change is made to a trigger field on the Parts Order Line
                else if(newPartsOrdLine.SVMX_Sales_Order_Item_Number__c == null
                    && newPartsOrdLine.SVMX_Awaiting_SAP_Response__c == true) {

                    for(String trigField : triggerFieldList) {
                    
                        if(newPartsOrderLineMap.get(newPartsOrdLine.Id).get(trigField) != oldPartsOrderLineMap.get(newPartsOrdLine.Id).get(trigField)) {
                        
                            SVMXC__RMA_Shipment_Line__c parOrdLine = new SVMXC__RMA_Shipment_Line__c(id = newPartsOrdLine.Id, SVMX_Item_Number_Blank_Check__c = true);
                            updatepartsOrderLineList.add(parOrdLine);
                            break;   
                        }                 
                    }    
                }

                //Scenario 3 - PO Line was waiting for Sales Order Item Number. Now the SO Item Number has arrived
                //Before the SO Item Number arrived, changes were made to the PO Line, causing the Blank Check Flag to be True
                //Need to send the PO Line for Update to SAP 
                else if(oldPartsOrderLineMap.get(newPartsOrdLine.Id).SVMX_Sales_Order_Item_Number__c == null 
                    && newPartsOrdLine.SVMX_Sales_Order_Item_Number__c != null 
                    && newPartsOrdLine.SVMX_Item_Number_Blank_Check__c == true
                    && newPartsOrdLine.SVMX_Awaiting_SAP_Response__c == true) {

                    sapPartsOrderLineUpdateSet.add(newPartsOrdLine.Id);
                    
                    SVMXC__RMA_Shipment_Line__c parOrdLine1 = new SVMXC__RMA_Shipment_Line__c(id = newPartsOrdLine.Id, SVMX_Item_Number_Blank_Check__c = false, SVMX_Awaiting_SAP_Response__c = false);
                    updatepartsOrderLineList.add(parOrdLine1);    
                }

                //Scenario 4 - PO Line was waiting for Sales Order Item Number. Now the SO Item Number has arrived
                //No changes were made to the PO Line, therefore, no need to send Update to SAP
                else if(oldPartsOrderLineMap.get(newPartsOrdLine.Id).SVMX_Sales_Order_Item_Number__c == null 
                    && newPartsOrdLine.SVMX_Sales_Order_Item_Number__c != null 
                    && newPartsOrdLine.SVMX_Item_Number_Blank_Check__c == false
                    && newPartsOrdLine.SVMX_Awaiting_SAP_Response__c == true) {

                    SVMXC__RMA_Shipment_Line__c parOrdLine1 = new SVMXC__RMA_Shipment_Line__c(id = newPartsOrdLine.Id, SVMX_Awaiting_SAP_Response__c = false);
                    updatepartsOrderLineList.add(parOrdLine1);    
                }                            
            }                       
            
            if(!sapPartsOrderLineUpdateSet.isEmpty()) {
               
                SVMX_SAPSalesOrderUtility.soapRequestForSAPSalesOrderSparePartsShipmentupdate(sapPartsOrderLineUpdateSet, false);
            } 

            if(!updatepartsOrderLineList.isEmpty()) {
                
                update updatepartsOrderLineList;                
            } 
  
        }
        else{

            System.debug('SVMX_SAPSalesOrderUtility : No partsOrderLine Line for Sales Order spare Parts update');
        }
    }  
}