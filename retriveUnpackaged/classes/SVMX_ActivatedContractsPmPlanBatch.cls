/*******************************************************************************************************
Description: Batch class for PM Plans for recently activated service contracts

Dependancy:    
    Class: SVMX_PM_Controller
 
Author: Pooja Singh
Date: 22-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

global class SVMX_ActivatedContractsPmPlanBatch implements Database.Batchable<sObject>{
    String query = '';
    String PM_Cov_Type_Str = 'Product (Must Have IB)';
    global Database.QueryLocator start(Database.BatchableContext BC) {
        query = 'Select Id, Name, SVMX_Converted_Plan__c, SVMXC__Coverage_Type__c, SVMX_Activated_Batch__c from SVMXC__PM_Plan__c where  SVMXC__Coverage_Type__c =: PM_Cov_Type_Str and SVMX_Activated_Batch__c = true and SVMX_Converted_Plan__c =false';
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<SVMXC__PM_Plan__c> pmList) {
        Set<Id> pmIdSet = new Set<Id>();
        for(SVMXC__PM_Plan__c pmPlan : pmList){
            pmPlan.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
            pmPlan.SVMX_Converted_Plan__c = true;
            pmPlan.SVMX_Activated_Batch__c = false;
            pmIdSet.add(pmPlan.id);
        }
        if(pmList.size() > 0)
            update pmList;
        
        if(pmIdSet.size() > 0){
            Map<Id,Map<Id,List<Id>>> pmIdLocIdIpIdListMap = new Map<Id,Map<Id,List<Id>>>(); 
            Map<Id,List<Id>> locIdIpIdListMap = new Map<Id,List<Id>>(); //Map of PM plan Id and List of IP Id
            //Map<Id,Id> ipIdLocIdMap = new Map<Id,Id>();   //Map of IP Id and location Id
            //List<Id> ipIdList = new List<Id>();
            List<SVMXC__PM_Coverage__c > pmCoverageQuery = [Select Id, Name, SVMXC__PM_Plan__c, SVMXC__Product_Name__c, SVMXC__Product_Name__r.SVMXC__Site__c from SVMXC__PM_Coverage__c where SVMXC__PM_Plan__c IN:pmIdSet and SVMXC__Product_Name__r.SVMXC__Site__c!= null];
            for(SVMXC__PM_Coverage__c pmc : pmCoverageQuery){  
                if(pmIdLocIdIpIdListMap.containsKey(pmc.SVMXC__PM_Plan__c)){
                    Map<Id,List<Id>> tempLocIdIpIdListMap = pmIdLocIdIpIdListMap.get(pmc.SVMXC__PM_Plan__c);
                    if(tempLocIdIpIdListMap.containsKey(pmc.SVMXC__Product_Name__r.SVMXC__Site__c)){
                        List<Id> tempIpIdList = tempLocIdIpIdListMap.get(pmc.SVMXC__Product_Name__r.SVMXC__Site__c);
                        tempIpIdList.add(pmc.SVMXC__Product_Name__c);
                        tempLocIdIpIdListMap.put(pmc.SVMXC__Product_Name__r.SVMXC__Site__c,tempIpIdList);
                        system.debug('tempLocIdIpIdListMap-->'+tempLocIdIpIdListMap);
                        pmIdLocIdIpIdListMap.put(pmc.SVMXC__PM_Plan__c,tempLocIdIpIdListMap);
                        system.debug('pmIdLocIdIpIdListMap1-->'+pmIdLocIdIpIdListMap);
                    }
                    else{
                        List<Id> ipIdNewElseList = new List<Id>();
                        ipIdNewElseList.add(pmc.SVMXC__Product_Name__c);
                        //Map<Id,List<Id>> tempLocIdIpIdListMapNew =  new Map<Id,List<Id>>();
                        tempLocIdIpIdListMap.put(pmc.SVMXC__Product_Name__r.SVMXC__Site__c,ipIdNewElseList);
                        system.debug('tempLocIdIpIdListMapelse-->'+tempLocIdIpIdListMap);
                        pmIdLocIdIpIdListMap.put(pmc.SVMXC__PM_Plan__c,tempLocIdIpIdListMap);
                        system.debug('pmIdLocIdIpIdListMap1else-->'+pmIdLocIdIpIdListMap);
                    }
                }
                else{
                    List<Id> ipIdNewList = new List<Id>();
                    ipIdNewList.add(pmc.SVMXC__Product_Name__c);
                    Map<Id,List<Id>> locIdIpIdListMapNew =  new Map<Id,List<Id>>();
                    locIdIpIdListMapNew.put(pmc.SVMXC__Product_Name__r.SVMXC__Site__c,ipIdNewList);
                    system.debug('locIdIpIdListMapNew-->'+locIdIpIdListMapNew);
                    pmIdLocIdIpIdListMap.put(pmc.SVMXC__PM_Plan__c,locIdIpIdListMapNew);
                    system.debug('pmIdLocIdIpIdListMap2else-->'+pmIdLocIdIpIdListMap);
                }
            }
            if(pmCoverageQuery.size() > 0)
                delete pmCoverageQuery;
            //Method to create PM Coverages for the PM Plans
            system.debug('pmIdLocIdIpIdListMap-->'+pmIdLocIdIpIdListMap);
            if(!pmIdLocIdIpIdListMap.isEmpty())
                SVMX_PM_Controller.createPmCoverage(pmIdLocIdIpIdListMap);
        }
    }
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}