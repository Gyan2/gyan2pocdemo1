@isTest
private class LocationMirrorTest {

	@testSetup
	static void setup() {
		/*insert new List<MirrorEngineMapping__c> {
			new MirrorEngineMapping__c (
				Name = 'Location Name',
				MappingType__c = 'Field',
				SObjectA__c = 'SVMXC__Site__c',
				FieldA__c = 'Name',
				SObjectB__c = 'Location__c',
				FieldB__c = 'Name'
			)
		};*/
	}
	/*
		@Fields: 	Location__c.Name, Location__c.SVMXC_SiteId__c, Location__c.SVMXC_Site__c 
					SVMXC__Site__c.Name
	*/
	@isTest static void SF2SVMX() {
		List<Location__c> locationsLst = new List<Location__c>();

		for (Integer i = 0; i < 200; i++) {
			locationsLst.add(new Location__c(Name = 'Test - ' + i));
		}

		insert locationsLst;
        Locations.deactivateMirroring = Locations.disableTriggerCRUDSecurity = false;
		locationsLst = [SELECT Name, SVMXC_SiteId__c, SVMXC_SiteRef__c, SVMXC_SiteRef__r.Name FROM Location__c];

		// Check that the mirror objects has been created
		System.assertEquals(200, [SELECT Name FROM SVMXC__Site__c].size());

		for (Location__c forLocation : locationsLst) {
			System.assertNotEquals(null, forLocation.SVMXC_SiteRef__c);
			System.assertEquals(forLocation.SVMXC_SiteRef__r.Name, forLocation.Name);
			forLocation.Name += 'XX';
		}

		update locationsLst;

		locationsLst = [SELECT Name, SVMXC_SiteId__c, SVMXC_SiteRef__c, SVMXC_SiteRef__r.Name FROM Location__c];

		for (Location__c forLocation : locationsLst) {
			System.assert(forLocation.Name.endsWith('XX'));
			System.assertEquals(forLocation.SVMXC_SiteRef__r.Name, forLocation.Name);
		}

	}
	
	@isTest static void SVMX2SF() {
		List<SVMXC__Site__c> sites = new List<SVMXC__Site__c>();

		for (Integer i = 0; i < 200; i++) {
			sites.add(new SVMXC__Site__c(Name = 'Test - ' + i));
		}

		insert sites;
		SVMXC_Sites.deactivateMirroring = SVMXC_Sites.disableTriggerCRUDSecurity = false;
		
		sites = [SELECT Name FROM SVMXC__Site__c];
		System.assertEquals(200, [SELECT Id FROM Location__c].size());

		Map<Id, Location__c> siteIdToLocationMap = new Map<Id, Location__c>();
		List<Location__c> locations = [SELECT Name, SVMXC_SiteRef__c FROM Location__c WHERE SVMXC_SiteRef__c IN :sites];

		System.assertEquals(200, locations.size());

		for (Location__c forLocation : locations) {
			siteIdToLocationMap.put(forLocation.SVMXC_SiteRef__c, forLocation);
		}

		for (SVMXC__Site__c forSite : sites) {
			System.assertEquals(forSite.Name, siteIdToLocationMap.get(forSite.Id).Name);
			forSite.Name += 'XX';
		}

		update sites;

		sites = [SELECT Name FROM SVMXC__Site__c];
		siteIdToLocationMap.clear();
		locations = [SELECT Name, SVMXC_SiteRef__c FROM Location__c WHERE SVMXC_SiteRef__c IN :sites];
		for (Location__c forLocation : locations) {
			siteIdToLocationMap.put(forLocation.SVMXC_SiteRef__c, forLocation);
		}

		for (SVMXC__Site__c forSite : sites) {
			System.assert(forSite.Name.endsWith('XX'));
			System.assertEquals(forSite.Name, siteIdToLocationMap.get(forSite.Id).Name);
		}


	}
	
}