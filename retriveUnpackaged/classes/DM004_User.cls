/*******************************************************************************************************
* Class Name      	: DM004_User
* Description     	: Data Manager for object User, Profile & Role
* Author          	: Paul Carmuciano
* Created On      	: 2017-09-27
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-09-27         1000                 Initial version
******************************************************************************************************/
public class DM004_User {
    
    /***
* Method name	: getProfiles
* Description	: Returns a map of Profile sobjects with relevant fields
* Author		: Paul Carmuciano
* Return Type	: Map<Id,Profile>
* Parameter		: none
*/
    public static Map<Id,Profile> getCustomProfiles() {
        // 
        return new Map<Id,Profile>([
            SELECT Id,Name,UserLicenseId,UserType 
            FROM Profile
        ]);
    }
    
    /***
* Method name	: getActiveUsersForProfile
* Description	: Returns a map of User sobjects with relevant fields
* Author		: Paul Carmuciano
* Return Type	: Map<Id,User>
* Parameter		: List<Id> profileIds
*/
    public static Map<Id,User> getActiveUsersForProfile(List<Id> profileIds) {
        // 
        return new Map<Id,User>([
            SELECT Email,FirstName,Id,LastName,ProfileId,Username,UserRoleId,UserCountry__c,UserRegion__c,UserSegment__c
            FROM User 
            WHERE IsActive = true
            AND ProfileId IN :profileIds
        ]);
    }
    

    /***
* Method name	: createUniqueUser
* Description	: Returns a User sobjects created with unique referneces (for Test Execution, do we not want this in the Data Manager layer?)
* Author		: Paul Carmuciano
* Return Type	: User
* Parameter		: Id roleId, Id profileID, String firstName, String lastName
*/
    public static User createRandomUser(Id roleId, Id profileID, String firstName, String lastName, boolean doInsert) {
        String orgId = UserInfo.getOrganizationId();
        String dateString = String.valueof(Datetime.now()).replace(' ','').replace(':','').replace('-','');
        Integer randomInt = Integer.valueOf(math.rint(math.random()*1000000));
        String uniqueName = orgId + dateString + randomInt;
        User tuser = new User(
            firstName = firstName,
            lastName = lastName,
            email = uniqueName + '@test' + orgId + '.org',
            Username = uniqueName + '@test' + orgId + '.org',
            EmailEncodingKey = 'ISO-8859-1',
            Alias = uniqueName.substring(18, 23),
            TimeZoneSidKey = 'America/Los_Angeles',
            LocaleSidKey = 'en_US',
            LanguageLocaleKey = 'en_US',
            ProfileId = profileId,
            UserRoleId = roleId,
            SalesOrg__c = '9999'
        );
        if (doInsert) insert tuser;
        return tuser;
    }    
    
    
}