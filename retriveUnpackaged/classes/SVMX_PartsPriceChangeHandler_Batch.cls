/************************************************************************************************************
Description: Batch Apex Class to send Labor, Expense and Travel Work Detail Lines to SAP
 
Author: Naveed Sharif
Date: 23-05-2018

Modification Log: 
Date            Author          Modification Comments

--------------------------------------------------------------
 
*******************************************************************************************************/

global class SVMX_PartsPriceChangeHandler_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {

    global Database.QueryLocator start (Database.BatchableContext BC) {

        String wdQuery;
        String reactive = System.Label.Reactive;
        String installation = System.Label.Installation;
        String ppm = System.Label.Planned_Services_PPM;
        String quotedWorks = System.Label.Quoted_Works;
        String closed = System.Label.Closed;
        String readyToInvoice = System.Label.Ready_To_Bill;
        Id usageRecordTypeId = SVMX_RecordTypeDataManager.getRecordTypeID('UsageConsumption','SVMXC__Service_Order_Line__c');

        wdQuery = 'SELECT Id, Name, SVMXC__Service_Order__r.SVMXC__Case__c, SVMX_Sales_Order_Item_Number__c, SVMX_Awaiting_SAP_Response__c, SVMX_Parts_Price_Changed__c, SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Awaiting_SAP_Response__c, SVMXC__Service_Order__c, SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c, SVMXC__Line_Type__c, SVMXC__Service_Order__r.SVMXC__Order_Type__c';
        wdQuery += ' FROM SVMXC__Service_Order_Line__c';
        wdQuery += ' WHERE RecordTypeId =: usageRecordTypeId';
        wdQuery += ' AND (SVMXC__Service_Order__r.SVMXC__Order_Type__c =: reactive OR SVMXC__Service_Order__r.SVMXC__Order_Type__c =: installation OR SVMXC__Service_Order__r.SVMXC__Order_Type__c =: ppm OR SVMXC__Service_Order__r.SVMXC__Order_Type__c =: quotedWorks)';
        wdQuery += ' AND SVMXC__Service_Order__r.SVMXC__Order_Status__c !=: closed';
        wdQuery += ' AND SVMXC__Service_Order__r.SVMXC__Order_Status__c !=: readyToInvoice';
        wdQuery += ' AND SVMXC__Line_Type__c = \'Parts\'';
        wdQuery += ' AND SVMX_Parts_Price_Changed__c = true';
        wdQuery += ' AND SVMXC__Actual_Price2__c != null';
        wdQuery += ' AND SVMX_Chargeable_Qty__c != null';
        wdQuery += ' AND SVMX_Chargeable_Qty__c > 0';

        return Database.getQueryLocator(wdQuery);
    }

    global void execute(Database.BatchableContext BC, List<SVMXC__Service_Order_Line__c> workDetLines) {

        Set<Id> woIds = new Set<Id>();
        Set<Id> wdUpdateIds = new Set<Id>();
        Set<Id> wdUpdateExisintgIds = new Set<Id>();
        Set<Id> wdCreateIds = new Set<Id>();
        List<SVMXC__Service_Order_Line__c> blankCheckUpdateList = new List<SVMXC__Service_Order_Line__c>();

        System.debug('*** Parts Pricing Change Batch Job *** ' + workDetLines);

        for(SVMXC__Service_Order_Line__c wDet : workDetLines) {

            //Scenario 1 - Sales Order Number already exists on Case, Sales Order Item Number is null - Send for SAP Update with new line
            if(wDet.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c != null
                && wDet.SVMX_Sales_Order_Item_Number__c == null
                && wDet.SVMX_Awaiting_SAP_Response__c == false) {
            
                wdUpdateIds.add(wDet.Id);
            }
            //Scenario 2 - Sales Order Number already exists on Case, Sales Order Item Number is null but has already been sent to SAP
            //and awaiting response. Set blank check Flag to true. This will cause an update send when the response returns
            else if(wDet.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c != null
                && wDet.SVMX_Sales_Order_Item_Number__c == null
                && wDet.SVMX_Awaiting_SAP_Response__c == true) {
                
                wDet.SVMX_Item_Number_Blank_Check__c = true;
                blankCheckUpdateList.add(wDet);
            }
            //Scenario 3 - Sales Order request already made, awaiting SAP Number to be returned on Case - Set blank check flag to true
            else if(wDet.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c == null
                && wDet.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Awaiting_SAP_Response__c == true) {

                wDet.SVMX_Item_Number_Blank_Check__c = true;
                blankCheckUpdateList.add(wDet);
            }
            //Scenario 4 - No Sales Order Request made yet - Send for new Sales Order Creation request
            else if(wDet.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c == null
                && wDet.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Awaiting_SAP_Response__c == false) {
                
                wdCreateIds.add(wDet.Id);  
            }
            //Scenario 5 - Sales Order Item Number exists on Work Detail Line - Send for SAP Update to existing Line
            //Note: It doesn't matter weather or not the Awaiting SAP Response flag is true or false. In both scenarios,
            //an Update to an existing Line will be sent.
            else if(wDet.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c != null
                && wDet.SVMX_Sales_Order_Item_Number__c != null) {

                wdUpdateExisintgIds.add(wDet.Id);
            } 
        }

        if(!wdUpdateIds.isEmpty()) {
            
            if(System.isFuture() || System.isBatch()) {    
          
                SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderItemUpdate(wdUpdateIds, true);
            }
            else {

                SVMX_SAPSalesOrderUtility.asyncSalesOrderItemUpdateMethod(wdUpdateIds, true);              
            }
        }

        if(!wdCreateIds.isEmpty()) {

            if(System.isFuture() || System.isBatch()) {
              
                SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderCreation(wdCreateIds); 
            }
            else {

                SVMX_SAPSalesOrderUtility.asyncSalesOrderCreationMethod(wdCreateIds);
            }
        }

        if(!wdUpdateExisintgIds.isEmpty()) {

            if(System.isFuture() || System.isBatch()) {    
              
                SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderItemUpdate(wdUpdateExisintgIds, false);
            }
            else {

                SVMX_SAPSalesOrderUtility.asyncSalesOrderItemUpdateMethod(wdUpdateExisintgIds, false);              
            }
        }

        if(!blankCheckUpdateList.isEmpty()) {

            update blankCheckUpdateList;
        }
    }

    global void finish(Database.BatchableContext BC) {

        System.debug('*** SVMX_PartsPriceChangeHandler_Batch ***');
        System.debug('*** Parts Price Change - Batch Complete ***');
    }
}