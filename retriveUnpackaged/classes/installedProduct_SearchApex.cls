public class installedProduct_SearchApex {

    @AuraEnabled
    public static list<sobject> accountLookup(String objectName)
    {
        list<sobject> accList=new list<sobject>();
        if(objectName == 'Account')
        {
            accList=[SELECT name, id FROM Account];
        	return accList;
        }
        
        else if(objectName == 'Location')
        {
            accList=[SELECT name, id FROM Location__c];
        	return accList;
        }
        
        else
        {
            accList=[SELECT name, id FROM product2];
        	return accList;

        }
        
    }
    
    @AuraEnabled
    public static list <sobject>search_Results(String accountValue)
    {
        list<sobject>searchList=new list<sobject>();
        
        String query = 'SELECT Id FROM SVMXC__Installed_Product__c WHERE ' + 'SVMXC__Company__c' + accountValue;
 		searchList = Database.query(query);
        return searchList;
    }
}