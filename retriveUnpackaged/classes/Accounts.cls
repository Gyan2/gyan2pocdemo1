public with sharing class Accounts extends CoreSObjectDomain{

    public Accounts(List<Account> sObjectList){
        super(sObjectList);
    }

    public override void onApplyDefaults() {
        // Apply defaults to Accounts
        applyManagementFieldsDefaults((List<Account>)Records);
        AccountServices.applyInternationalPhonePrefix(Records);
    }

    private void applyManagementFieldsDefaults(List<Account> paramRecords){
        /* TODO: Check. Logic copied from the Process Builder, but it makes no sense to "freeze" from the createdBy of the Account, we might want to change it to owner since once we have an integration it will be TECH PI*/
        User currentUser = Util.CurrentUserRecord;
        for (Account forAcc : paramRecords) {
            forAcc.ManagementCountry__c = currentUser.UserCountry__c;
            forAcc.ManagementRegion__c = currentUser.UserRegion__c;
            forAcc.ManagementSegment__c = currentUser.UserSegment__c;
        }
    }

    public override void onValidate() {
        AccountServices.validateInternationalPhonePrefix(Records);
    }

    public override void onValidate(Map<Id,SObject> existingRecords) {
        // Validate changes to Accounts
    }

    public override void onBeforeInsert(){
        AccountServices.splitInternationalPrefix(Records);
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        AccountServices.countContacts((List<Account>)Records);
        AccountServices.splitInternationalPrefix(Records);
        AccountServices.manageQuoteLanguages((List<Account>)Records,(Map<Id,Account>)existingRecords);
    }

    public override void onBeforeDelete(){

    }

    public override void onAfterInsert(){

    }

    public override void onAfterUpdate(Map<Id,SObject> existingRecords){
        
    }

    public override void onAfterDelete(){

    }

    public override void onAfterUndelete(){
        update Records;
    }


    public class Constructor implements CoreSObjectDomain.IConstructable
    {
        public CoreSObjectDomain construct(List<SObject> sObjectList)
        {
            return new Accounts(sObjectList);
        }
    }
}