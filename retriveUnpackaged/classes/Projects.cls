public with sharing class Projects extends CoreSObjectDomain{
    public static Boolean calculateRollUps = false;
    public Projects(List<Project__c> sObjectList){
        super(sObjectList);
    }

    public override void onApplyDefaults() {
        // Apply defaults to Products

    }

    public override void onValidate() {
        // Validate Products
    }

    public override void onValidate(Map<Id,SObject> existingRecords) {
        // Validate changes to Products

    }

    public override void onBeforeInsert(){

    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        // Related Products
        if(calculateRollUps) ProjectServices.calculateRollUps((List<Project__c>)Records);
    }

    public override void onBeforeDelete(){

    }

    public override void onAfterInsert(){
        ProjectServices.handleAddressChangesFromBuilding((List<Project__c>)Records,null);
    }


    public override void onAfterUpdate(Map<Id,SObject> existingRecords){
        ProjectServices.handleAddressChangesFromBuilding((List<Project__c>)Records,(Map<Id,Project__c>)existingRecords);
    }

    public override void onAfterDelete(){

    }


    public override void onAfterUndelete(){

    }


    public class Constructor implements CoreSObjectDomain.IConstructable
    {
        public CoreSObjectDomain construct(List<SObject> sObjectList)
        {
            return new Projects(sObjectList);
        }
    }
}