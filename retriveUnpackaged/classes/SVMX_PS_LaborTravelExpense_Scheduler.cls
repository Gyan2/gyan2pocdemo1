global class SVMX_PS_LaborTravelExpense_Scheduler implements Schedulable {
    global void execute(SchedulableContext sc) {
        
        SVMX_LaborTravelExpenseHandler_Batch lteBatch = new SVMX_LaborTravelExpenseHandler_Batch();
        Database.executebatch(lteBatch, 100);
    }
}