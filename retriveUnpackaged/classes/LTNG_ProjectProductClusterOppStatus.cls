/**
 * Created by alcocese on 09-Apr-18.
 */

public without sharing class LTNG_ProjectProductClusterOppStatus {

    @AuraEnabled
    public static ProjectOpportunityClusterInformationWrapper auraGetOpportunityCountPerCluster(Id projectId) {
        ProjectOpportunityClusterInformationWrapper returned = new ProjectOpportunityClusterInformationWrapper();

        returned.project = [SELECT MKS__c, DHW__c, IGS__c, ENS__c, LGS__c, SAL__c, EAD__c, SVC__c FROM Project__c WHERE Id = :projectId];

        for(AggregateResult forAG : [SELECT COUNT(Id) howMany, ProductCluster__c FROM Opportunity WHERE ProjectRef__c = :projectId GROUP BY ProductCluster__c]) {
            returned.counts.put((String)forAG.get('ProductCluster__c'),(Integer)forAG.get('howMany'));
        }

        return returned;
    }

    public class ProjectOpportunityClusterInformationWrapper {
        @AuraEnabled public Project__c project;
        @AuraEnabled public Map<String, Integer> counts = new Map<String, Integer>();
    }


    @AuraEnabled
    public static void auraSaveClusters(Project__c project){
        update new Project__c(
            Id = project.Id,
            MKS__c = project.MKS__c,
            DHW__c = project.DHW__c,
            IGS__c = project.IGS__c,
            ENS__c = project.ENS__c,
            LGS__c = project.LGS__c,
            SAL__c = project.SAL__c,
            EAD__c = project.EAD__c,
            SVC__c = project.SVC__c
        );
    }
}