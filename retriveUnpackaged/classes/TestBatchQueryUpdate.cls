/**
 * Created by rebmangu on 08/05/2018.
 */

@IsTest
private class TestBatchQueryUpdate {

    static testMethod void testRun(){
        Product2 product = new Product2(Name='Test',ProductHierarchy__c='0102030405070809');
        insert product;

        Test.startTest();
        Database.executeBatch(new BatchQueryUpdate('select id from product2'),100);

        Test.stopTest();

        System.assertEquals(1,[select count() from Product2 where SVMXC__Product_Line__c = '0102' AND family = '01']);
    }
}