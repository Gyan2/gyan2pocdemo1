/************************************************************************************************************
Description: Controller for SVMX_WO_ServicStkAvailability VF page.
 
Author: Ranjitha Shenoy
Date: 26-02-2018

Modification Log: 
Date            Author          Modification Comments
-------------------------------------------------------------
****************************************************************************************************************/

public class SVMX_WO_ServicStkAvailability_Controller {
    
    public ID workOrderId{get;set;}
    public Map<id,SVMXC__Service_Order_Line__c> workDetailMap;
    public List<WrapWD> workdetailWrap {get;set;}
    public SVMXC__Service_Order__c wo {get;set;}
    public string woCountry{get;set;}
    public string state {get;set;}
    public string city {get;set;}
    public string zip {get;set;}
    public string country {get;set;}
    public string street {get;set;}
    public string shippingToLoc {get;set;}
    public string SAPStorageLoc {get;set;}
    public string plantNum {get;set;}
    public id reqAtLoc {get;set;}
    public static List<SVMXC__Product_Stock__c> psList;
    public set<id> productIds;
    public string available = Label.available;
    public boolean selDeselc{get;set;}
        
    public SVMX_WO_ServicStkAvailability_Controller(){
        
        Id reqReciRecType = SVMX_RecordTypeDataManager.GetRecordTypeID('RequestReceipt','SVMXC__Service_Order_Line__c');
        workOrderId = ApexPages.currentPage().getParameters().get('workOrderId');
        wo = [select id, name, SVMXC__Site__c, SVMX_Language__c, SVMXC__Country__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.name, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC_Plant_Number__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC_SAP_Storage_Location__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Country__c    , SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__City__c, SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Zip__c , SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__State__c , SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Street__c , SVMX_To_Stock_Location__c, (Select id, name, SVMXC__Product__c, SVMXC__Product__r.name, SVMX_Request_Lines_Converted__c, SVMXC__Product__r.SAPNumber__c, SVMXC__Requested_Quantity2__c, SVMX_ProductDetail__c  from  SVMXC__Service_Order_Line__r  where RecordTypeid =: reqReciRecType  and SVMX_Request_Lines_Converted__c = false and SVMX_SAP_Product_Type__c = :System.Label.Stock_Part)  from SVMXC__Service_Order__c   where id = :workOrderId];     
        wo.SVMX_To_Stock_Location__c = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c;
        workDetailMap = new Map<id,SVMXC__Service_Order_Line__c>();
        wo.SVMX_Default_Stock_Location__c = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__c;
        reqAtLoc = wo.SVMX_Default_Stock_Location__c;
        SAPStorageLoc = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC_SAP_Storage_Location__c;
        plantNum = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC_Plant_Number__c;
        shippingToLoc = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.name;
        state = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__State__c;
        city = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__City__c;
        zip = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Zip__c ;
        street =  wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Street__c;
        country = wo.SVMXC__Group_Member__r.SVMXC__Inventory_Location__r.SVMXC__Country__c;
        woCountry = wo.SVMXC__Country__c;
        productIds = new set<id>();
        selDeselc = true;
        for(SVMXC__Service_Order_Line__c wd : wo.SVMXC__Service_Order_Line__r ) {
            workDetailMap.put(wd.id, wd);
            productIds.add(wd.SVMXC__Product__c);
        }
        psList = [select id, SVMXC__Product__c ,SVMXC__Location__c ,SVMXC__Location__r.name, SVMXC__Location__r.SVMXC_Plant_Number__c, SVMXC__Location__r.SVMXC_SAP_Storage_Location__c , SVMX_Unit_of_Measure__c ,SVMXC__Quantity2__c ,SVMXC_SAP_Product_Number__c , SVMXC__Status__c from SVMXC__Product_Stock__c  where SVMXC__Product__c in :productIds and SVMXC__Status__c = : available and SVMXC__Quantity2__c > 0 and SVMXC__Location__r.SVMXC__Country__c = :wo.SVMXC__Country__c and SVMXC__Location__r.SVMXC__Stocking_Location__c = true and SVMXC__Location__r.SVMXC__Location_Type__c = :System.Label.Internal and SVMXC__Location__c != :reqAtLoc];
            system.debug(' ##psList'+psList);
        workdetailWrap = new List<WrapWD>();
        for(SVMXC__Service_Order_Line__c wd: workDetailMap.values()){
            workdetailWrap.add(new WrapWD(wd));
        }
    }  
    
    public class WrapWD{
        
        public id wdId{get;set;}
        public string wdName{get;set;}
        public boolean sel{get;set;}
        public string partName{get;set;}
        public decimal qty{get;set;}
        public decimal avlQty{get;set;}
        public id prod{get;set;}
        public String selectedLoc{get;set;}
        public List<selectOption> availableFrmLoc{get;set;}
        public Map<string,SVMXC__Product_Stock__c> locNameProductStockMap{get;set;}
        public string prodDetail;
        public string prodSAPnum{get;set;}
          
        public WrapWD(SVMXC__Service_Order_Line__c wd){
            
            wdId = wd.id;
            wdName = wd.name;
            sel = true;
            partName = wd.SVMXC__Product__r.name;
            prodSAPnum = wd.SVMXC__Product__r.SAPNumber__c;
            qty = wd.SVMXC__Requested_Quantity2__c;
            prod = wd.SVMXC__Product__c;
            availableFrmLoc = new List<selectOption>();
            locNameProductStockMap = new Map<string,SVMXC__Product_Stock__c>();
            prodDetail = wd.SVMX_ProductDetail__c;
            for(SVMXC__Product_Stock__c ps: psList){
                locNameProductStockMap.put(ps.SVMXC__Location__r.name , ps);
                if(ps.SVMXC__Product__c == wd.SVMXC__Product__c && ps.SVMXC__Quantity2__c >= wd.SVMXC__Requested_Quantity2__c){
                    availableFrmLoc.add(new SelectOption(ps.SVMXC__Location__r.name, ps.SVMXC__Location__r.name));
                }    
            }
            if(!availableFrmLoc.isEmpty())
                avlQty = wd.SVMXC__Requested_Quantity2__c;
            else
                avlQty = 0;
        }     
    } 
    
    public void chkbox(){
        for(WrapWD w: workdetailWrap){
            if(selDeselc==false)
                w.sel = false;
            if(selDeselc==true)
                w.sel = true;
        }
    } 
    
    public void toLoc(){
        SVMXC__Site__c loc;
        if (wo.SVMX_Default_Stock_Location__c == null) {
            loc = new SVMXC__Site__c(SVMXC__State__c = '', SVMXC__City__c = '', SVMXC__Zip__c = '', SVMXC__Street__c = '', SVMXC__Country__c = '');
        } else {
        	loc = [select id, SVMXC__City__c , SVMXC__Country__c , SVMXC__State__c , SVMXC__Street__c , SVMXC__Zip__c, name, SVMXC_SAP_Storage_Location__c, SVMXC_Plant_Number__c from SVMXC__Site__c where id =:wo.SVMX_Default_Stock_Location__c];
        }
        reqAtLoc = loc.id;
        SAPStorageLoc = loc.SVMXC_SAP_Storage_Location__c;
        plantNum = loc.SVMXC_Plant_Number__c;
        if(wo.SVMX_To_Stock_Location__c == null ){
            wo.SVMX_To_Stock_Location__c = loc.id;
            shippingToLoc = loc.id;
        	state = loc.SVMXC__State__c;
        	city = loc.SVMXC__City__c;
        	zip = loc.SVMXC__Zip__c ;
        	street =  loc.SVMXC__Street__c;
        	country = loc.SVMXC__Country__c;
        }
        psList = [select id, SVMXC__Product__c ,SVMXC__Location__c , SVMXC__Location__r.SVMXC_Plant_Number__c, SVMXC__Location__r.name, SVMXC__Location__r.SVMXC_SAP_Storage_Location__c , SVMX_Unit_of_Measure__c ,SVMXC__Quantity2__c ,SVMXC_SAP_Product_Number__c , SVMXC__Status__c from SVMXC__Product_Stock__c  where SVMXC__Product__c in :productIds and SVMXC__Status__c = : available and SVMXC__Quantity2__c > 0 and SVMXC__Location__r.SVMXC__Country__c = :woCountry and SVMXC__Location__r.SVMXC__Stocking_Location__c = true and SVMXC__Location__r.SVMXC__Location_Type__c = :System.Label.Internal and SVMXC__Location__c != :reqAtLoc];
        workdetailWrap = new List<WrapWD>();
        for(SVMXC__Service_Order_Line__c wd: workDetailMap.values()){
            workdetailWrap.add(new WrapWD(wd));
        }
    }
    
    public void change(){
        SVMXC__Site__c loc;
         if (wo.SVMX_To_Stock_Location__c == null) {
            loc = new SVMXC__Site__c (SVMXC__City__c = '', SVMXC__Country__c = '', SVMXC__State__c = '', SVMXC__Street__c = '', SVMXC__Zip__c = '');
        } else {
        	loc = [select id,  name, SVMXC__City__c , SVMXC_SAP_Storage_Location__c, SVMXC_Plant_Number__c, SVMXC__Country__c , SVMXC__State__c , SVMXC__Street__c , SVMXC__Zip__c from SVMXC__Site__c where id =: wo.SVMX_To_Stock_Location__c];
        }
        shippingToLoc = loc.name;
        state = loc.SVMXC__State__c;
        city = loc.SVMXC__City__c;
        zip = loc.SVMXC__Zip__c ;
        street =  loc.SVMXC__Street__c;
        country = loc.SVMXC__Country__c;
        psList = [select id, SVMXC__Product__c ,SVMXC__Location__c , SVMXC__Location__r.SVMXC_Plant_Number__c, SVMXC__Location__r.name, SVMXC__Location__r.SVMXC_SAP_Storage_Location__c , SVMX_Unit_of_Measure__c ,SVMXC__Quantity2__c ,SVMXC_SAP_Product_Number__c , SVMXC__Status__c from SVMXC__Product_Stock__c  where SVMXC__Product__c in :productIds and SVMXC__Status__c = : available and SVMXC__Quantity2__c > 0 and SVMXC__Location__r.SVMXC__Country__c = :woCountry and SVMXC__Location__r.SVMXC__Stocking_Location__c = true and SVMXC__Location__r.SVMXC__Location_Type__c = :System.Label.Internal and SVMXC__Location__c != :reqAtLoc];
        workdetailWrap = new List<WrapWD>();
        for(SVMXC__Service_Order_Line__c wd: workDetailMap.values()){
            workdetailWrap.add(new WrapWD(wd));
        }
        
    }
    
    public pageReference Back(){
         
        pagereference p =  new pagereference('/'+workOrderId); 
        return p;
    }
    
    public void ChkAvailability(){
        if(woCountry=='' || woCountry==null){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Enter Country to Check Availability'));
        }
        psList = [select id, SVMXC__Product__c ,SVMXC__Location__c , SVMXC__Location__r.SVMXC_Plant_Number__c, SVMXC__Location__r.name, SVMXC__Location__r.SVMXC_SAP_Storage_Location__c , SVMX_Unit_of_Measure__c ,SVMXC__Quantity2__c ,SVMXC_SAP_Product_Number__c , SVMXC__Status__c from SVMXC__Product_Stock__c  where SVMXC__Product__c in :productIds and SVMXC__Status__c = : available and SVMXC__Quantity2__c > 0 and SVMXC__Location__r.SVMXC__Country__c = :woCountry and SVMXC__Location__r.SVMXC__Stocking_Location__c = true and SVMXC__Location__r.SVMXC__Location_Type__c = :System.Label.Internal and SVMXC__Location__c != :reqAtLoc];
        workdetailWrap = new List<WrapWD>();
        for(SVMXC__Service_Order_Line__c wd: workDetailMap.values()){
            workdetailWrap.add(new WrapWD(wd));
        }
    }
    
    public pagereference StkTransfer(){
        if(wo.SVMX_Default_Stock_Location__c == null){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.Error,'To Stocking Location is Blank'));
        	return null;
        }
        Map<string,SVMXC__Stock_Transfer__c> stkTrnsMap = new Map<string,SVMXC__Stock_Transfer__c>();
        List<SVMXC__Stock_Transfer_Line__c> stkTrnsLine = new List<SVMXC__Stock_Transfer_Line__c>();
        List<SVMXC__Service_Order_Line__c> wdLines = new List<SVMXC__Service_Order_Line__c>();
        boolean selec = false;
        for(WrapWD w: workdetailWrap){
            if(w.sel == true && w.selectedLoc == null){
                apexpages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Available from location is blank for a selected Item'));
                return null;
            }
            else if(w.sel==true && !stkTrnsMap.containsKey(w.selectedLoc)){
                SVMXC__Stock_Transfer__c stkTrns = new SVMXC__Stock_Transfer__c();
                stkTrns.SVMXC__Destination_Location__c = reqAtLoc;
                stkTrns.SVMXC__Source_Location__c = w.locNameProductStockMap.get(w.selectedLoc).SVMXC__Location__c;
                stkTrns.SVMXC_SAP_Source_Location__c = w.locNameProductStockMap.get(w.selectedLoc).SVMXC__Location__r.SVMXC_SAP_Storage_Location__c;
                stkTrns.SVMXC_SAP_Destination_Location__c = SAPStorageLoc;
                stkTrns.SVMXC_Source_Plant_Number__c = w.locNameProductStockMap.get(w.selectedLoc).SVMXC__Location__r.SVMXC_Plant_Number__c;
                stkTrns.SVMXC_Destination_Plant_Number__c = plantNum;
                stkTrns.SVMXC_Global_Location_Name__c = '9921226005491';
                stkTrns.SVMX_Shipping_City__c = city;
                stkTrns.SVMX_Language__c = wo.SVMX_Language__c;
                stkTrns.SVMX_Shipping_Country__c = country;
                stkTrns.SVMX_Shipping_State__c = state;
                stkTrns.SVMX_Shipping_Zip__c = zip;
                stkTrns.SVMX_Shipping_Street__c =street;
                stkTrns.SVMX_Work_Order__c = workOrderId;
                stkTrns.SVMXC_Approved__c = true;
                stkTrnsMap.put(w.selectedLoc,stkTrns);
                selec = true;
            }
        }
        
        if(!selec){
            apexpages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Select one or more items to create stock transfer'));
            return null;
        }
            
        if(!stkTrnsMap.isEmpty()){
            insert stkTrnsMap.values();
            
            for(WrapWD w: workdetailWrap){
                if(w.sel==true && stkTrnsMap.containsKey(w.selectedLoc)){
                    SVMXC__Stock_Transfer_Line__c stkLine = new SVMXC__Stock_Transfer_Line__c();
                    stkline.SVMXC__Product__c = w.prod;
                    stkline.SVMXC__Quantity_Transferred2__c = w.avlQty;
                    stkline.SVMX_Approved__c = true;
                    stkline.SVMXC__Stock_Transfer__c = stkTrnsMap.get(w.selectedLoc).id;
                    stkline.SVMX_ProductDetail__c = w.prodDetail;
                    stkline.SVMXC_Sender_Plant_Number__c = stkTrnsMap.get(w.selectedLoc).SVMXC_Source_Plant_Number__c;
                    stkline.SVMXC_Receiver_Plant_Number__c = stkTrnsMap.get(w.selectedLoc).SVMXC_Destination_Plant_Number__c;
                    stkline.SVMX_PS_VS_Product_Stock__c =  w.locNameProductStockMap.get(w.selectedLoc).id;
                    stkline.SVMXC_Unit_of_Measure__c = w.locNameProductStockMap.get(w.selectedLoc).SVMX_Unit_of_Measure__c;
                    stkline.SVMXC_SAP_Material_Number__c = w.locNameProductStockMap.get(w.selectedLoc).SVMXC_SAP_Product_Number__c;
                    stkline.Global_Location_Name_GLN__c = stkTrnsMap.get(w.selectedLoc).SVMXC_Global_Location_Name__c;
                    stkline.SVMXC_SAP_Sender_Location__c = stkTrnsMap.get(w.selectedLoc).SVMXC_SAP_Source_Location__c;
                    stkline.SVMXC_SAP_Receiver_Location__c = stkTrnsMap.get(w.selectedLoc).SVMXC_SAP_Destination_Location__c;
                    stkline.SVMXC_Reason_For_Transfer__c = 'Restocking';
                    stkline.SVMXC_Sender_Stocking_Location__c = stkTrnsMap.get(w.selectedLoc).SVMXC__Source_Location__c;
                    stkline.SVMXC_Receiver_Stocking_Location__c = stkTrnsMap.get(w.selectedLoc).SVMXC__Destination_Location__c;
                    stkTrnsLine.add(stkline);
                    SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
                    wd.SVMX_Request_Lines_Converted__c = true;
                    wd.id = w.wdId;
                    wdLines.add(wd);
                }
            }
        }
        
        if(!stkTrnsLine.isEmpty())
            insert stkTrnsLine;
        
        if(!wdLines.isEmpty())
            update wdLines;
        
        pagereference p =  new pagereference('/'+workOrderId); 
        return p;
    }
    
}