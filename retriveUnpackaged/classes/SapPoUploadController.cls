// @description Controller for Lighthning component SapPoUploadQuickAction
// @copyright Parx
// @author MHA
// @lastmodify 24.04.2017
// 
public without sharing class SapPoUploadController {
	@AuraEnabled
    public static void upload (Id oppId)
    {
        SapPoXML.Data data = new SapPoXML.Data();
        data = SapPoUpload.setData(oppId);
        
        system.debug(json.serialize(data));
        Dom.Document doc = SapPoXML.createDocument(data);
        
        Id integrationMessageId = IntegrationMessage.createMessage(oppId,'',SapPoXML.endpoint,doc.toXmlString(),'');
        
        SapPoUpload.sendMessage(oppId,integrationMessageId);
        
    }
    @AuraEnabled
    public static String getIntegrationMessageStatus (Id oppId)
    {
        String status = IntegrationMessage.getStatus(oppId);
        system.debug('Status: '+ status);
        return status;
    }
   
}