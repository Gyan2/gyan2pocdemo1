@isTest
private class TestJsonViewerController {

    @testSetup static void setup(){
        
        Product2 product = new Product2(
            Name='Test',
            SalesSpecifications__c='{"blabla":"blabla"}',
            ExternalId__c = 'PS8-00000001'
        );
        
        insert product;
    }
    
    @isTest static void testMethod1(){
        String ProductId = [select Id from Product2 limit 1].Id;
        String val = JsonViewerController.getJsonValue('SalesSpecifications__c','Product2',ProductId);
        
        
        System.assertEquals('{"blabla":"blabla"}',val);
    }
    
}