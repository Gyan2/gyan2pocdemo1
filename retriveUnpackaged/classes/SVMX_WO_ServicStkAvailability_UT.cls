/************************************************************************************************************
Description: Test class for the Controller Class (SVMX_WO_ServicStkAvailability_Controller) 
 
Author: Ranjitha S
Date: 11-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/
@isTest
public class SVMX_WO_ServicStkAvailability_UT {

    static testMethod void checkServiceStock()
    {
        Account acc = SVMX_TestUtility.CreateAccount('Test account', 'Norway', true);
        Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
        
        SVMXC__Site__c  loc = SVMX_TestUtility.CreateLocation('Test Loc',acc.id,true);
        loc.SVMXC_SAP_Storage_Location__c = '2325212773';
        loc.SVMXC_Plant_Number__c = 'T223';
        loc.SVMXC__Stocking_Location__c = true;
        update loc;
        
        SVMXC__Site__c  loc2 = SVMX_TestUtility.CreateLocation('From Loc',acc.id,true);
        loc2.SVMXC_SAP_Storage_Location__c = '232521255';
        loc2.SVMXC_Plant_Number__c = 'T673';
        loc2.SVMXC__Stocking_Location__c = true;
        loc2.SVMXC__Location_Type__c = 'Internal';
        update loc2;
        
        SVMXC__Installed_Product__c ip1 = SVMX_TestUtility.CreateInstalledProduct(acc.id,null,loc.id,true);
        Product2  prod = SVMX_TestUtility.CreateProduct('Test prod',true);
        prod.SVMX_SAP_Product_Type__c = 'Stock Part';
        Update prod;
        
        Profile prof = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
        User tech2 = SVMX_TestUtility.CreateUser('Testtech', prof.id, true);
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Svmx Tech2', tech2.id, null, true);
        tech.SVMXC__Inventory_Location__c = loc.id;
        update tech;
        
        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.id,loc.id,null,'Open','Reactive','Global Std',null,null,null,true);
        wo.SVMXC__Group_Member__c = tech.id;
        wo.SVMX_Default_Stock_Location__c = loc.id;
        Update wo;
        
        SVMXC__Service_Order_Line__c workDetail = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id, loc.Id, prod.Id, 'Request/Receipt', wo.Id, null, null, null, null,'Parts', null, true);
        workDetail.SVMX_Request_Lines_Converted__c = false;
        workDetail.SVMXC__Requested_Quantity2__c = 1;
        update workDetail;
        
        List<SVMXC__Product_Stock__c> psList = new List<SVMXC__Product_Stock__c>();
        
        SVMXC__Product_Stock__c ps = new SVMXC__Product_Stock__c();
        ps.SVMXC__Product__c = prod.id;
        ps.SVMXC__Status__c = 'Available';
        ps.SVMXC__Quantity2__c = 2;
        ps.SVMXC__Location__c = loc2.id;
        insert ps;
        
        
        List<SVMX_WO_ServicStkAvailability_Controller.WrapWD> w = new List<SVMX_WO_ServicStkAvailability_Controller.WrapWD>();
      
        PageReference pageRef = Page.SVMX_WO_ServicStkAvailability;

        pageRef.getParameters().put('workOrderId', wo.id);
        Test.setCurrentPage(pageRef);
        
        Test.startTest();
        SVMX_WO_ServicStkAvailability_Controller obj01 = new SVMX_WO_ServicStkAvailability_Controller();
        obj01.workdetailWrap.get(0).sel = true;
        obj01.workdetailWrap.get(0).selectedLoc = obj01.workdetailWrap.get(0).availableFrmLoc[0].getValue();
        obj01.chkbox();
        obj01.toLoc();
        obj01.change();
        obj01.Back();
        obj01.ChkAvailability();
        obj01.workdetailWrap.get(0).sel = true;
        obj01.workdetailWrap.get(0).selectedLoc = obj01.workdetailWrap.get(0).availableFrmLoc[0].getValue();
		obj01.StkTransfer();
        Test.stopTest();
    }
}