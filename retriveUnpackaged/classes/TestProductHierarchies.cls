/**
 * Created by rebmangu on 16/05/2018.
 */

@IsTest
private class TestProductHierarchies {
    static testMethod void upsertProductHierarchy() {



        ProductHierarchy__c need = new ProductHierarchy__c(ExternalId__c='03');
        insert need;

        ProductHierarchy__c family = new ProductHierarchy__c(ExternalId__c='0322');
        insert family;

        ProductHierarchy__c classM = new ProductHierarchy__c(ExternalId__c='0322010');
        insert classM;

        ProductHierarchy__c line = new ProductHierarchy__c(ExternalId__c='0322010070');
        insert line;

        ProductHierarchy__c ph = new ProductHierarchy__c(ExternalId__c='0322010070002');
        insert ph;


        System.assertEquals(1,[select count() from ProductHierarchy__c where NeedRef__c =: need.id AND FamilyRef__c =: family.id AND ClassRef__c =: classM.Id AND LineRef__c =: line.Id]);
    }
}