/*******************************************************************************************************
Description:
  This test class is used to test the SVMX_WorkOrderTrggrHandler class.
 
Author: Ranjitha S
Date: 16-10-2017

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/
@isTest
public class SVMX_TestUtility_UT {
    private static Account a = SVMX_TestUtility.CreateAccount('acc','Germany',false);
    public static testmethod void  testSVMX_TestUtility(){
        SVMX_TestUtility.CreateAccount('accName','Germany',false);
        SVMX_TestUtility.CreateActivityMaster('activityType', false);
        SVMX_TestUtility.CreateActivityProduct(null, null, false);
        SVMX_TestUtility.CreateCase(null, null, 'New', false);
        SVMX_TestUtility.CreateContact(null, false);
        SVMX_TestUtility.CreateWarrentTerms(false);
        SVMX_TestUtility.CreateApplicationProduct(null,null,false);
        SVMX_TestUtility.CreateInstalledProduct(null, null, null,false);
        SVMX_TestUtility.CreateLocation('locName', null, false);
        SVMX_TestUtility.CreateOpportunity(null, null, 'stage', 50, 121, false);
        SVMX_TestUtility.CreateOpportunity(null, 'name', 'stage', 53, 555, false);
        SVMX_TestUtility.CreatePdfAttachment('name', a.Id, false);
        SVMX_TestUtility.CreatePMPlan(null, 'sName',  false);
        SVMX_TestUtility.CreateProduct('pName', false);
        SVMX_TestUtility.CreateRateTier(null, null,false, false);
        SVMX_TestUtility.CreateServiceContract(a.Id, 'sName', 'Global Std', null, false);
        SVMX_TestUtility.CreateServiceGroup('gName', false);
        SVMX_TestUtility.CreateServicePlan('sName', 'planType', '3', false);
        SVMX_TestUtility.CreateServicePricebook('pName', false);
        SVMX_TestUtility.CreateServicePricebookEntry(null, null, null, false);
        SVMX_TestUtility.CreateTechnician('tName', null, null, false);
        SVMX_TestUtility.CreateUser('uLastName', null, false);
        SVMX_TestUtility.CreateWorkOrder(null, null, null, 'Open','Reactive','Global Std', null, null, null, false);
        SVMX_TestUtility.CreateWorkOrderDetail(null, null, null,'Estimate', null, system.now(),null, null, null,null,null, false);
        SVMX_TestUtility.CreateWorkOrderDetailEstimate(null, null, null,'Estimate', null, system.now(),null, null, null,null,null,null,false,false);
        SVMX_TestUtility.SelectBusinessHours();
        SVMX_TestUtility.SelectProfile('System Administrator');
        SVMX_TestUtility.CreateWarranty(null, false);
        SVMX_TestUtility.CreateCustomFeature('Germany', false);
        SVMX_TestUtility.CreateServiceQuote(null, null,null,'Draft', false);
        SVMX_TestUtility.CreateTimeSheet('Open',true);
        SVMX_TestUtility.CreateServicecontractSite(null,null,system.today(),false);
    }
}