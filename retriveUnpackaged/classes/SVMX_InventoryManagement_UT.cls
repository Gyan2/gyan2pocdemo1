@isTest
public class SVMX_InventoryManagement_UT{

    @isTest  
    static  void inventoryManagemtapproved(){

        Account acct = SVMX_TestUtility.CreateAccount('US account','United States', true);

        Product2 prod = SVMX_TestUtility.CreateProduct('test123',True);
        
        SVMXC__Site__c loct=SVMX_TestUtility.CreateLocation('test location',acct.id,true);

        List<SVMXC__Site__c> locationList = new List<SVMXC__Site__c>();
        SVMXC__Site__c sourceLocation = new SVMXC__Site__c();
        sourceLocation.Name = 'source location';
        sourceLocation.SVMXC__Account__c = acct.Id;
        sourceLocation.SVMXC__Stocking_Location__c = true;
        sourceLocation.SVMX_Approval_Required__c = false;
        locationList.add(sourceLocation);

        SVMXC__Site__c sourceLocationapprovalrequired = new SVMXC__Site__c();
        sourceLocationapprovalrequired.Name = 'source location';
        sourceLocationapprovalrequired.SVMXC__Account__c = acct.Id;
        sourceLocationapprovalrequired.SVMXC__Stocking_Location__c = true;
        sourceLocationapprovalrequired.SVMX_Approval_Required__c = true;
        locationList.add(sourceLocationapprovalrequired);

        //Create Destination Location
        SVMXC__Site__c destinationLocation = new SVMXC__Site__c();
        destinationLocation.Name = 'destination Location';
        destinationLocation.SVMXC__Account__c = acct.Id;
        destinationLocation.SVMXC__Stocking_Location__c = true;
        locationList.add(destinationLocation);

        insert locationList;

        List<Product2> productList = new List<Product2>();
        Product2 serializedProd = new Product2();
        serializedProd.Name = 'Test Serialized Product';
        serializedProd.IsActive = true;
        serializedProd.SVMXC__Tracking__c = 'Serialized';
        serializedProd.SVMXC__Stockable__c = true;
        serializedProd.SVMXC__Enable_Serialized_Tracking__c = true;
        productList.add(serializedProd);

        Product2 serializedProd2 = new Product2();
        serializedProd2.Name = 'Test Serialized Product 1';
        serializedProd2.IsActive = true;
        serializedProd2.SVMXC__Tracking__c = 'Serialized';
        serializedProd2.SVMXC__Stockable__c = true;
        serializedProd2.SVMXC__Enable_Serialized_Tracking__c = true;
        productList.add(serializedProd2);
        
        //Create non-serialized product
        Product2 nonSerializedProd = new Product2();
        nonSerializedProd.Name = 'Test Non-Serialized Product';
        nonSerializedProd.IsActive = true;
        nonSerializedProd.SVMXC__Tracking__c = 'Non-Tracked';
        nonSerializedProd.SVMXC__Stockable__c = true;
        productList.add(nonSerializedProd);

        Product2 nonSerializedProd2 = new Product2();
        nonSerializedProd2.Name = 'Test Non-Serialized Product 1';
        nonSerializedProd2.IsActive = true;
        nonSerializedProd2.SVMXC__Tracking__c = 'Non-Tracked';
        nonSerializedProd2.SVMXC__Stockable__c = true;
        productList.add(nonSerializedProd2);

        insert productList;

        //Insert Product Stock for source location
        List<SVMXC__Product_Stock__c> productStockList = new List<SVMXC__Product_Stock__c>();
        SVMXC__Product_Stock__c sourceSerializedProdStock = new SVMXC__Product_Stock__c();
        sourceSerializedProdStock.SVMXC__Location__c = sourceLocation.Id;
        sourceSerializedProdStock.SVMXC__Product__c = serializedProd.Id;
        sourceSerializedProdStock.SVMXC__Status__c = 'Available';
        sourceSerializedProdStock.SVMXC__Quantity2__c = 2;
        productStockList.add(sourceSerializedProdStock);

        SVMXC__Product_Stock__c sourceNonSerializedProdStock = new SVMXC__Product_Stock__c();
        sourceNonSerializedProdStock.SVMXC__Location__c = sourceLocation.Id;
        sourceNonSerializedProdStock.SVMXC__Product__c = nonSerializedProd.Id;
        sourceNonSerializedProdStock.SVMXC__Status__c = 'Available';
        sourceNonSerializedProdStock.SVMXC__Quantity2__c = 10;
        productStockList.add(sourceNonSerializedProdStock);

        SVMXC__Product_Stock__c onlySourceSerializedProdStock = new SVMXC__Product_Stock__c();
        onlySourceSerializedProdStock.SVMXC__Location__c = sourceLocation.Id;
        onlySourceSerializedProdStock.SVMXC__Product__c = serializedProd2.Id;
        onlySourceSerializedProdStock.SVMXC__Status__c = 'In Transit';
        onlySourceSerializedProdStock.SVMXC__Quantity2__c = 2;
        productStockList.add(onlySourceSerializedProdStock);

        SVMXC__Product_Stock__c onlySourceNonSerializedProdStock = new SVMXC__Product_Stock__c();
        onlySourceNonSerializedProdStock.SVMXC__Location__c = sourceLocation.Id;
        onlySourceNonSerializedProdStock.SVMXC__Product__c = nonSerializedProd2.Id;
        onlySourceNonSerializedProdStock.SVMXC__Status__c = 'Available';
        onlySourceNonSerializedProdStock.SVMXC__Quantity2__c = 10;
        productStockList.add(onlySourceNonSerializedProdStock);
        
        //Insert Product Stock for destination location
        SVMXC__Product_Stock__c destinationSerializedProdStock = new SVMXC__Product_Stock__c();
        destinationSerializedProdStock.SVMXC__Location__c = destinationLocation.Id;
        destinationSerializedProdStock.SVMXC__Product__c = serializedProd.Id;
        destinationSerializedProdStock.SVMXC__Status__c = 'Available';
        destinationSerializedProdStock.SVMXC__Quantity2__c = 1;
        productStockList.add(destinationSerializedProdStock);

        SVMXC__Product_Stock__c destinationNonSerializedProdStock = new SVMXC__Product_Stock__c();
        destinationNonSerializedProdStock.SVMXC__Location__c = destinationLocation.Id;
        destinationNonSerializedProdStock.SVMXC__Product__c = nonSerializedProd.Id;
        destinationNonSerializedProdStock.SVMXC__Status__c = 'Available';
        destinationNonSerializedProdStock.SVMXC__Quantity2__c = 5;
        productStockList.add(destinationNonSerializedProdStock);
        insert productStockList;

        //Insert Stock Transfer
        SVMXC__Stock_Transfer__c stockTransfer = new SVMXC__Stock_Transfer__c();
        stockTransfer.SVMXC__Destination_Location__c = destinationLocation.Id;
        stockTransfer.SVMXC__Source_Location__c = sourceLocation.Id;
        
        insert stockTransfer;
        
        SVMXC__Stock_Transfer__c stockTransfer1 = new SVMXC__Stock_Transfer__c();
        stockTransfer1.SVMXC__Destination_Location__c = destinationLocation.Id;
        stockTransfer1.SVMXC__Source_Location__c = sourceLocationapprovalrequired.Id;
        
        insert stockTransfer1;

        List<SVMXC__Stock_Transfer_Line__c> stockedTransferLineList = new List<SVMXC__Stock_Transfer_Line__c>();
        SVMXC__Stock_Transfer_Line__c line1 = new SVMXC__Stock_Transfer_Line__c();
        line1.SVMXC__Posted_To_Inventory__c = false;
        line1.SVMXC__Product__c = serializedProd.Id;
        line1.SVMXC_Sender_Stocking_Location__c =sourceLocation.Id;
        line1.SVMXC_Receiver_Stocking_Location__c = destinationLocation.Id;
        //line1.SVMX_PS_VS_Product_Stock__c = null;
        line1.SVMXC__Quantity_Transferred2__c = 1;
        //line1.SVMX_PS_VS_Serial_Number__c = '123456';
        //line1.SVMX_PS_VS_Stocked_Serial__c = null;
        line1.SVMXC__Stock_Transfer__c = stockTransfer.Id;
        stockedTransferLineList.add(line1);

        SVMXC__Stock_Transfer_Line__c line2 = new SVMXC__Stock_Transfer_Line__c();
        line2.SVMXC__Posted_To_Inventory__c = false;
        line2.SVMXC__Product__c = serializedProd.id;
        line2.SVMXC_Sender_Stocking_Location__c = sourceLocation.Id;
        //line2.SVMX_PS_VS_Product_Stock__c = sourceSerializedProdStock.Id;
        line2.SVMXC__Quantity_Transferred2__c = 1;
        //line2.SVMX_PS_VS_Serial_Number__c = null;
        //line2.SVMX_PS_VS_Stocked_Serial__c = stockSerial2.Id;
        line2.SVMXC__Stock_Transfer__c = stockTransfer.Id;
        //stockedTransferLineList.add(line2);

        SVMXC__Stock_Transfer_Line__c line3 = new SVMXC__Stock_Transfer_Line__c();
        line3.SVMXC__Posted_To_Inventory__c = false;
        line3.SVMXC__Product__c = serializedProd.id;
        //line3.SVMX_PS_VS_Product_Stock__c = onlySourceSerializedProdStock.Id;
        line3.SVMXC__Quantity_Transferred2__c = 1;
        //line3.SVMX_PS_VS_Serial_Number__c = null;
        //line3.SVMX_PS_VS_Stocked_Serial__c = stockSerial4.Id;
        line3.SVMXC__Stock_Transfer__c = stockTransfer.Id;
        //stockedTransferLineList.add(line3);
        
        insert stockedTransferLineList;


        SVMXC__Stock_History__c StkHstTrans = new SVMXC__Stock_History__c();
                    StkHstTrans.SVMXC__Product_Stock__c = sourceSerializedProdStock.id;
                    StkHstTrans.SVMXC__Product__c = line1.SVMXC__Product__c;
                    StkHstTrans.SVMXC__Location__c = line1.SVMXC_Receiver_Stocking_Location__c;
                    StkHstTrans.SVMXC__Status__c = 'Available';    
                    StkHstTrans.SVMXC__Transaction_Quantity2__c = line1.SVMXC__Quantity_Transferred2__c;
                    StkHstTrans.SVMXC__Quantity_after_change2__c = sourceSerializedProdStock.SVMXC__Quantity2__c + line1.SVMXC__Quantity_Transferred2__c;
                    StkHstTrans.SVMXC__Quantity_before_change2__c =sourceSerializedProdStock.SVMXC__Quantity2__c;
                    StkHstTrans.SVMXC__Date_Changed__c = System.Now();
                    StkHstTrans.SVMXC__Transaction_Type__c = 'Stock Transfer';  
                    StkHstTrans.SVMXC__Change_Type__c = 'Increase';
    
        test.startTest();
         stockTransfer.SVMXC_Approved__c = true;
         update stockTransfer;

         line1.SVMXC__Posted_To_Inventory__c = true;
         update line1;

         insert StkHstTrans;
         test.stopTest();  

    }
}