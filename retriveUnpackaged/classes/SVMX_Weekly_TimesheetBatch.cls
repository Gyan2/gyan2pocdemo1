/*******************************************************************************************************
Description: Batch class for creating weekly timesheets

Dependancy:    
    Class: SVMX_TimesheetBatch_Controller
 
Author: Pooja Singh
Date: 14-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

global class SVMX_Weekly_TimesheetBatch implements Database.Batchable<sObject> {
    public String query ='';
    public Set<String> weeklyCtrySet = new Set<String>();
    public Set<String> profSet = new Set<String>();
    global SVMX_Weekly_TimesheetBatch(Set<String> passedIdSet, List<String> profileList){
        weeklyCtrySet = passedIdSet;
        for(Profile p :[Select Id, Name from Profile where Name IN: profileList]){
            profSet.add(p.Id);
        }
    }
    global Database.QueryLocator start(Database.BatchableContext BC) {
        // collect the batches of records or objects to be passed to execute
        query = 'Select Id, UserCountry__c from User where UserCountry__c IN :weeklyCtrySet and isActive = true and profileId IN:profSet';
        system.debug('Query '+query);
            return Database.getQueryLocator(query);
    }
     
    global void execute(Database.BatchableContext BC, List<User> userList) {
        List<Id> UserIdList = new List<Id>();   //New user id list
        Map<Id,Id> existTsUserMap = new Map<Id,Id>(); //existing user id and timesheet id map
        Set<Date> startDatesSet = new Set<Date>();  //Set of start dates
        Map<Date,Date> stDtEdDtMap = new Map<Date,Date>();  //Map of start date and corresponding end date
        Date startDate;
        Date endDate;
        Date firTiShStDt;
        Date firTiShEdDt;
        Date secTiShStDt;
        Date secTiShEdDt;
        Date moStartDate = System.Date.today().toStartOfMonth();    //Start date of the month
        Date moEndDate = moStartDate.addMonths(1).addDays(-1);  //end date of the month
        startdate =system.today();
       
         //code to calculate if today is saturday else get previous saturday

        Datetime dt = DateTime.newInstance(Date.today(), Time.newInstance(0, 0, 0, 0));
        String dayOfWeek=dt.format('EEEE');
         if(dayOfWeek  != 'Sunday'){
            Integer sat = 0;
            if(dayOfWeek == 'Saturday')
                sat = 6;
            else if(dayOfWeek == 'Friday')
                sat = 5;
            else if(dayOfWeek == 'Thursday')
                sat = 4;
            else if(dayOfWeek == 'Wednesday')
                sat = 3;
            else if(dayOfWeek == 'Tuesday')
                sat = 2;
            else if(dayOfWeek == 'Monday')
                sat = 1;
            startdate = system.today().addDays(-sat);
            system.debug(startdate);

         }

          startDate = startdate.adddays(1);   //Start date from Monday
          system.debug('startDate '+startDate);

        endDate = startDate+6;  //End date Saturday
        //Condition where end of month falls in the current week
        if(moEndDate < endDate){
            firTiShStDt = startDate;
            startDatesSet.add(firTiShStDt); 
            firTiShEdDt = moEndDate;
            stDtEdDtMap.put(firTiShStDt,firTiShEdDt);
            secTiShStDt = moEndDate+1;
            startDatesSet.add(secTiShStDt);
            secTiShEdDt = endDate;
            stDtEdDtMap.put(secTiShStDt,secTiShEdDt);
            system.debug('firTiShStDt'+firTiShStDt);
            system.debug('firTiShEdDt'+firTiShEdDt);
            system.debug('secTiShStDt'+secTiShStDt);
            system.debug('secTiShEdDt'+secTiShEdDt);
        }
        //Condition where end of month doesn't falls in the current week
        else{
            firTiShStDt = startDate;    //Start date from Monday
            firTiShEdDt = endDate;  //End date on Monday
            startDatesSet.add(firTiShStDt);
            stDtEdDtMap.put(firTiShStDt,firTiShEdDt);
            system.debug('Inside else of weekly TS'+stDtEdDtMap);
        }
        //Query if a timesheet exists for the given week range
        List<SVMXC__Timesheet__c> timeSheetQuery = new List<SVMXC__Timesheet__c>([Select Id, Name, SVMX_Current_Week__c, SVMXC__Start_Date__c, SVMXC__End_Date__c,SVMXC__Period__c, SVMXC__User__c from SVMXC__Timesheet__c where SVMXC__User__c IN:userList and SVMXC__Start_Date__c IN :startDatesSet and SVMXC__End_Date__c IN: stDtEdDtMap.values()]);
        for(SVMXC__Timesheet__c ts : timeSheetQuery){
            ts.SVMX_Current_Week__c = true;
            existTsUserMap.put(ts.SVMXC__User__c,ts.id);
            system.debug('existing timesheets for the user for that Week'+existTsUserMap);
        }
        
        if(timeSheetQuery.size() > 0)
            update timeSheetQuery;
        
        for(User u : userList){
            if(!existTsUserMap.containsKey(u.id)){
                UserIdList.add(u.id);
                system.debug('List of users for new timesheets for that Week'+UserIdList);
            }
        }
        if(UserIdList.size() > 0)
            SVMX_TimesheetBatch_Controller.createTimesheet(startDatesSet, stDtEdDtMap, 'Weekly', UserIdList);
        
        /* Date collection for weekly timesheet */
        Date prevWkStDt;
        Date prevWkEdDt;
        Date prevFirTiShStDt;
        Date prevFirTiShEdDt;
        Date prevSecTiShStDt;
        Date prevSecTiShEdDt;
        Set<Date> prevStDtSet = new Set<Date>();
        Map<Date,Date> prevStDtEdDtMap = new Map<Date,Date>();
        Date prevMoStartDate = System.Date.today().toStartOfMonth();    //Start date of the month
        Date prevMoEndDate = prevMoStartDate.addMonths(1).addDays(-1);  //end date of the month
        prevWkStDt = System.today()-6;  //Start date from sunday
        prevWkEdDt = System.today();    //End date Saturday
        //Condition where end of month falls in the current week
        if(prevMoEndDate <= prevWkEdDt){
            prevFirTiShStDt = prevWkStDt;
            prevStDtSet.add(prevWkStDt);    
            prevFirTiShEdDt = prevMoEndDate;
            prevStDtEdDtMap.put(prevFirTiShStDt,prevFirTiShEdDt);
            prevSecTiShStDt = prevMoEndDate+1;
            prevStDtSet.add(prevSecTiShStDt);
            prevSecTiShEdDt = prevWkEdDt;
            prevStDtEdDtMap.put(prevSecTiShStDt,prevSecTiShEdDt);
            system.debug('previous prevFirTiShStDt'+prevFirTiShStDt);
            system.debug('previous prevFirTiShEdDt'+prevFirTiShEdDt);
            system.debug('previous secTiShStDt'+prevSecTiShStDt);
            system.debug('previous prevSecTiShEdDt'+prevSecTiShEdDt);
        }
        //Condition where end of month doesn't falls in the current week
        else{
            prevFirTiShStDt = prevWkStDt;   //Start date from sunday
            prevFirTiShEdDt = prevWkEdDt;   //End date on saturday
            prevStDtSet.add(prevFirTiShStDt);
            prevStDtEdDtMap.put(prevFirTiShStDt,prevFirTiShEdDt);
            system.debug('Inside else of previous weekly TS'+prevStDtEdDtMap);
        }
        
        SVMX_TimesheetBatch_Controller.prevWeekTimeSheet(prevStDtSet,prevStDtEdDtMap);
    }   
     
    global void finish(Database.BatchableContext BC) {
        // execute any post-processing operations
    }
}