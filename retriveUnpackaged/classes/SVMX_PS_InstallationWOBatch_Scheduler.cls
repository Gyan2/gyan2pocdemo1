global class SVMX_PS_InstallationWOBatch_Scheduler implements Schedulable {

    global void execute(SchedulableContext sc) {

        SVMX_InstallationWorkOrderHandler_Batch iwoBatch = new SVMX_InstallationWorkOrderHandler_Batch();
        Database.executebatch(iwoBatch, 100);
    }
}