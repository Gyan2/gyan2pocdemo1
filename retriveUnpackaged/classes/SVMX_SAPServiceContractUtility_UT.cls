@isTest
public class  SVMX_SAPServiceContractUtility_UT{
    static testMethod void invokeServiceContractUpdatetriggerSAPserviceContractCreate(){
        
        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'ServiceContract';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
              
        SVMX_SAP_Integration_UserPass__c cs1 = new SVMX_SAP_Integration_UserPass__c();
              cs1.name ='SAP PO';
              cs1.WS_USER__c ='test';
              cs1.WS_PASS__c ='test';
        insert cs1;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig4=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig4.name='trig1';
        trigconfig4.SVMX_Country__c='germany';
        trigconfig4.SVMX_Object__c='Service Contract';
        trigconfig4.SVMX_Trigger_on_Field__c='SVMXC__Start_Date__c';
        insert trigconfig4;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Service Contract Site';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Start_Date__c';
        insert trigconfig2;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Service Contract Product';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Start_Date__c';
        insert trigconfig;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig1= new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig1';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Contract Line Item';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMX_Line_Qty__c';
        insert trigconfig1;
        
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);

        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, false);
        sc.SVMXC__Active__c = False;
        sc.SVMX_Pricing_Method__c = 'Both';
        sc.SVMXC__Start_Date__c = Date.newInstance(2018, 1, 1);
        sc.SVMXC__End_Date__c = Date.newInstance(2019, 12, 10);
        insert sc;

        Date startDate=system.today();
        
        

        SVMXC__Service_Contract_Sites__c  serContrSite = SVMX_TestUtility.CreateServicecontractSite(sc.Id,'Yearly on Last of Start month',startDate,false);
        serContrSite.SVMXC__Start_Date__c = system.today();
        serContrSite.SVMXC__End_Date__c = Date.newInstance(2018, 12, 9);
        serContrSite.SVMX_Pricing_Methos__c = 'Time Based';
        insert serContrSite;

        SVMXC__Service_Contract_Products__c serContrProduct = SVMX_TestUtility.CreateServiceContractProduct(sc.Id,'Yearly on Last of Start month',startDate,false);
        serContrProduct.SVMXC__Start_Date__c = system.today();
        serContrProduct.SVMXC__End_Date__c = Date.newInstance(2018, 12, 9);
        serContrProduct.SVMX_Pricing_Method__c ='Time Based';
        insert serContrProduct;

        SVMX_Contract_Line_Items__c  CreateContractLine = SVMX_TestUtility.CreateContractLineItem(sc.Id,'Yearly on Last of Start month',startDate,false);
        //CreateContractLine.SVMX_Contract_Start__c = Date.newInstance(2016, 12, 9);
        CreateContractLine.SVMX_Contract_End__c = Date.newInstance(2018, 12, 9);
        insert CreateContractLine;

        Test.startTest();
        Test.setMock(WebServiceMock.class, new SAPServiceContractUtilityMock());
        SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader messageheader = new SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComContract.ServiceContractCreateRequestServiceContract serviceContractreq = new SVMX_wwwDormakabaComContract.ServiceContractCreateRequestServiceContract();
        SVMX_wwwDormakabaComContract.ServiceContractOutPort  requstelemnt = new SVMX_wwwDormakabaComContract.ServiceContractOutPort();
		requstelemnt.CreateSync(messageheader,serviceContractreq);
        sc.SVMXC__Active__c = True;
        Update sc;
        
        Test.stopTest();

    }

    static testMethod void invokeServiceContractItemsInsertriggerSAPserviceContractUpdate(){

        
        
         SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'ServiceContract';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
              
        SVMX_SAP_Integration_UserPass__c cs1 = new SVMX_SAP_Integration_UserPass__c();
              cs1.name ='SAP PO';
              cs1.WS_USER__c ='test';
              cs1.WS_PASS__c ='test';
        insert cs1;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Service Contract';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Start_Date__c';
        insert trigconfig2;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Contract Line Item';
        trigconfig.SVMX_Trigger_on_Field__c='SVMX_Line_Qty__c';
        insert trigconfig;
		
		SVMX_Interface_Trigger_Configuration__c trigconfig3=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig3.name='trig1';
        trigconfig3.SVMX_Country__c='germany';
        trigconfig3.SVMX_Object__c='Service Contract Site';
        trigconfig3.SVMX_Trigger_on_Field__c='SVMXC__Start_Date__c';
        insert trigconfig3;
        
        SVMX_Interface_Trigger_Configuration__c trigconfig4=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig4.name='trig1';
        trigconfig4.SVMX_Country__c='germany';
        trigconfig4.SVMX_Object__c='Service Contract Product';
        trigconfig4.SVMX_Trigger_on_Field__c='SVMXC__Start_Date__c';
        insert trigconfig4;

        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);

        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, false);
        sc.SVMXC__Active__c = True;
        sc.SVMX_Pricing_Method__c = 'Both';
        sc.SVMXC__Start_Date__c = Date.newInstance(2018, 1, 1);
        sc.SVMXC__End_Date__c = Date.newInstance(2019, 12, 10);
        sc.SVMX_SAP_Contract_Number__c = '88927387878';
        insert sc;

        Date startDate=system.today();
        

        Test.startTest();
		
        Test.setMock(WebServiceMock.class, new SAPServiceContractUtilityMock());
        SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader messageheader = new SVMX_wwwDormakabaContract.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract serviceContractUpdatereq = new SVMX_wwwDormakabaComContract.ServiceContractUpdateRequestServiceContract();
        SVMX_wwwDormakabaComContract.ServiceContractOutPort requstelemnt = new SVMX_wwwDormakabaComContract.ServiceContractOutPort();
        requstelemnt.UpdateSync(messageheader,serviceContractUpdatereq);
        
        SVMXC__Service_Contract_Sites__c  serContrSite = SVMX_TestUtility.CreateServicecontractSite(sc.Id,'Yearly on Last of Start month',startDate,false);
        serContrSite.SVMXC__Start_Date__c = system.today();
        serContrSite.SVMXC__End_Date__c = Date.newInstance(2018, 12, 9);
        serContrSite.SVMX_Pricing_Methos__c = 'Time Based';
        serContrSite.SVMX_SAP_Contract_Item_Number__c ='78238748';

        SVMXC__Service_Contract_Products__c serContrProduct = SVMX_TestUtility.CreateServiceContractProduct(sc.Id,'Yearly on Last of Start month',startDate,false);
        serContrProduct.SVMXC__Start_Date__c = system.today();
        serContrProduct.SVMXC__End_Date__c = Date.newInstance(2018, 12, 9);
        serContrProduct.SVMX_Pricing_Method__c ='Time Based';
		serContrProduct.SVMX_SAP_Contract_Item_Number__c ='78238748';

        SVMX_Contract_Line_Items__c  CreateContractLine = SVMX_TestUtility.CreateContractLineItem(sc.Id,'Yearly on Last of Start month',startDate,false);
        //CreateContractLine.SVMX_Contract_Start__c = Date.newInstance(2016, 12, 9);
        CreateContractLine.SVMX_Contract_End__c = Date.newInstance(2018, 12, 9);
		serContrProduct.SVMX_SAP_Contract_Item_Number__c ='78238748';

        Insert serContrSite;

        Insert serContrProduct;

        Insert CreateContractLine;
        
        sc.SVMXC__Start_Date__c = Date.newInstance(2018, 2, 1);
        
        update sc;
        
        Test.stopTest();

    }
}