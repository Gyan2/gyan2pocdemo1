/*******************************************************************************************************
Description: Trigger handler for Event Trigger

Dependancy: 
 Trigger Framework: SVMX_TriggerHandler.cls
 Trigger: SVMX_EventTrggr.cls
    
    
Author: Tulsi B R
Date: 04-01-2018

*******************************************************************************************************/


    public class SVMX_TimeEntryTrggrHandler extends SVMX_TriggerHandler{

      private list< SVMXC__Timesheet_Entry__c >neweventList;
      private list< SVMXC__Timesheet_Entry__c >oldEventList;
      private Map<Id, SVMXC__Timesheet_Entry__c >newEventMap;
      private Map<Id, SVMXC__Timesheet_Entry__c >oldEventMap;

      public SVMX_TimeEntryTrggrHandler() {
         this.neweventList = (list< SVMXC__Timesheet_Entry__c >) Trigger.new;
         this.oldEventList = (list< SVMXC__Timesheet_Entry__c >) Trigger.old;
         this.newEventMap = (Map<Id, SVMXC__Timesheet_Entry__c >) Trigger.newMap;
         this.oldEventMap = (Map<Id, SVMXC__Timesheet_Entry__c >) Trigger.oldMap;
      }
                
      public override void afterUpdate() {


         List <SVMXC__Timesheet_Entry__c> eventtimelist = new  List <SVMXC__Timesheet_Entry__c>();
         set<Id> timesheetids= new set<Id>();
        
         for(SVMXC__Timesheet_Entry__c ev : neweventList){
             
             if(ev.SVMXC__Start_Time__c.date() != oldEventMap.get(ev.id).SVMXC__Start_Time__c.date()){
                
                  eventtimelist.add(ev);
                  timesheetids.add(ev.SVMXC__Timesheet__c);    
              }

         }

          if(eventtimelist.size() > 0){
            List<SVMXC__Timesheet_Entry__c> timeinsList = new List<SVMXC__Timesheet_Entry__c>();
            List<SVMXC__Timesheet_Entry__c> deleteList = new List<SVMXC__Timesheet_Entry__c>();

            List<SVMXC__Timesheet_Day_Entry__c>  tsdaily= [select Id,SVMXC__Day_of_the_Week__c from SVMXC__Timesheet_Day_Entry__c where SVMXC__Timesheet__c In: timesheetids];
            for(SVMXC__Timesheet_Entry__c ts: eventtimelist){

                for(SVMXC__Timesheet_Day_Entry__c tds: tsdaily){
                    if(ts.SVMXC__Start_Time__c.format('EEEE') == tds.SVMXC__Day_of_the_Week__c){
                        SVMXC__Timesheet_Entry__c tsclone = ts.clone(false,true,false,false);
                        tsclone.SVMXC__Timesheet_Day__c = tds.id;
                        tsclone.SVMX_Overlaps__c ='No';
                        timeinsList.add(tsclone);

                        SVMXC__Timesheet_Entry__c tsdel = new SVMXC__Timesheet_Entry__c(id=ts.id);
                        deleteList.add(tsdel);
                    }
                }
            }

            if(deleteList.size() > 0)
                delete deleteList;

            if(timeinsList.size() >0)
                insert timeinsList;

          }
      
            List <SVMXC__Timesheet_Entry__c> checevtntList = new  List <SVMXC__Timesheet_Entry__c>();
           //only 
           for(SVMXC__Timesheet_Entry__c ev:neweventList){
            if((ev.SVMXC__Start_Time__c != oldEventMap.get(ev.id).SVMXC__Start_Time__c || ev.SVMXC__End_Time__c != oldEventMap.get(ev.id).SVMXC__End_Time__c) && ev.SVMXC__Start_Time__c.date() == oldEventMap.get(ev.id).SVMXC__Start_Time__c.date())
            
              checevtntList.add(ev);
                
            }
     
           SVMX_PS_TS_TimesheetUtils timesheetUtils = new SVMX_PS_TS_TimesheetUtils();
            
            if(checevtntList.size()>0) {
              timesheetUtils.checkForTimeEntryOverlap(checevtntList);
            }

       }
       
    }