/**
 * Created by rebmangu on 09/05/2018.
 */

public with sharing class SLDS_PicklistController {
    @AuraEnabled
    public static Map<String, List<UtilDependentPicklist.PicklistEntryWrapper>> getDependentOptions(String fieldName,String controllingFieldName,String objectType){

        Schema.SObjectField theField;
        Schema.SObjectField ctrlField;

        Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

        theField  = globalDescribe.get(objectType).getDescribe().fields.getMap().get(fieldName);
        ctrlField = globalDescribe.get(objectType).getDescribe().fields.getMap().get(controllingFieldName);

        UtilDependentPicklist UtilDependentPicklist = Util.UtilDependentPicklist;
        Map<String, List<UtilDependentPicklist.PicklistEntryWrapper>> result = UtilDependentPicklist.getDependentOptionsImpl(theField,ctrlField);
        return result;
    }

    @AuraEnabled
    public static List<UtilDependentPicklist.PicklistEntryWrapper> getPicklistValues(String fieldName,String objectType){
        Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

        List<Schema.PicklistEntry> picklistEntries  = globalDescribe.get(objectType).getDescribe().fields.getMap().get(fieldName).getDescribe().getPicklistValues();
        List<UtilDependentPicklist.PicklistEntryWrapper> result = new List<UtilDependentPicklist.PicklistEntryWrapper>();
        UtilDependentPicklist.PicklistEntryWrapper wrapper = new UtilDependentPicklist.PicklistEntryWrapper();
        wrapper.active = true;
        wrapper.defaultValue = false;
        wrapper.label = '-- none --';
        wrapper.value = null;
        result.add(wrapper);
        result.addAll(UtilDependentPicklist.wrapPicklistEntries(picklistEntries));
        return result;
    }
}