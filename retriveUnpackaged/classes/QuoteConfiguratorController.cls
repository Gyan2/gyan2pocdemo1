public class QuoteConfiguratorController {

    public class QuoteConfiguratorControllerException extends Exception{}
    public static String SUCCESS = 'success';
    public static String DUPLICATE = 'duplicate';

    public class Response {
        @AuraEnabled public integer status;
        @AuraEnabled public string message;
        @AuraEnabled public List<Object> data;
    }

    public class QuoteWithLineItems {
        @AuraEnabled public Quote quote;
        @AuraEnabled public List<QuoteLineItem> quoteLineItems;
//        @AuraEnabled public List<Text__c> quotetextCollection;
//        @AuraEnabled public Map<String,String> quoteTextSettings;
//
//        @AuraEnabled public Map<String,List<Text__c>> quoteLineItemTextCollections;
//        @AuraEnabled public Map<String,String> quoteLineItemTextSettings;

        //@AuraEnabled public Map<String,String> quoteFields;
        //@AuraEnabled public Map<String,String> quoteLineItemFields;
    }

    public class QuoteLineOption {
        @AuraEnabled public String Name;
        @AuraEnabled public String Label;
        @AuraEnabled public String Value;
        @AuraEnabled public Boolean IsRequired;
        @AuraEnabled public String OptionType;
        @AuraEnabled public String HelpText;
        //public List<SelectOption> SelectOptions;
        @AuraEnabled public String SelectOptions;
    }


    @AuraEnabled
    public static QuoteWithLineItems auraGetAllData (String paramId, List<String> paramQuoteFields, List<String> paramQuoteLineItemFields) {
        QuoteWithLineItems returned = new QuoteWithLineItems();
//        returned.quoteFields = new Map<String,String>();
//        returned.quoteLineItemFields = new Map<String,String>();
//
//        Map<String, Schema.SObjectType> schemaMap = Schema.getGlobalDescribe();
//        Schema.SObjectType quoteSchema = schemaMap.get('Quote');
//        Map<String, Schema.SObjectField> quoteFieldsMap = quoteSchema.getDescribe().fields.getMap();
//        Schema.SObjectType quoteLineItemSchema = schemaMap.get('QuoteLineItem');
//        Map<String, Schema.SObjectField> quoteLineItemFieldsMap = quoteLineItemSchema.getDescribe().fields.getMap();
//
//        for(String forQuoteField : paramQuoteFields) {
//            if (!forQuoteField.contains('.')) { // TODO Prepare for fields like Account.Name
//                returned.quoteFields.put(forQuoteField, quoteFieldsMap.get(forQuoteField).getDescribe().getLabel());
//            }
//        }
//
//        for(String forQuoteLineItemField : paramQuoteLineItemFields) {System.debug(forQuoteLineItemField);
//            if (!forQuoteLineItemField.contains('.')) {// TODO Prepare for fields like Account.Name
//                returned.quoteLineItemFields.put(forQuoteLineItemField, quoteLineItemFieldsMap.get(forQuoteLineItemField).getDescribe().getLabel());
//            }
//        }



        returned.quote = database.query(
                String.format('SELECT {0},(select id,Text__c,SAPKey__c,Language__c,AutoExternalId__c from Texts__r) FROM Quote WHERE Id = \'\'{1}\'\'',
                        new List<String>{String.join(paramQuoteFields,','),paramId}));
        returned.quoteLineItems = database.query(
                String.format('SELECT {0},(select id,Text__c,SAPKey__c,Language__c,AutoExternalId__c from Texts__r) FROM QuoteLineItem where QuoteId = \'\'{1}\'\' ORDER BY Position__c NULLS LAST ',
                        new List<String>{String.join(paramQuoteLineItemFields,','),paramId}));

        //load texts
//        returned.quotetextCollection = [select id,Text__c,SAPKey__c,Language__c,ExternalId__c from Text__c where QuoteRef__c =: paramId];
//        returned.quoteLineItemTextCollections = new Map<String,List<Text__c>>(); //select id,Text__c,SAPKey__c,Language__c,ExternalId__c
//        for(Text__c text : [select id,QuoteLineItemRef__c,Text__c,SAPKey__c,Language__c,ExternalId__c from Text__c where QuoteLineItemRef__r.QuoteId =: paramId]){
//            if(!returned.quoteLineItemTextCollections.containsKey(text.QuoteLineItemRef__c)){
//                returned.quoteLineItemTextCollections.put(text.QuoteLineItemRef__c,new List<Text__c>());
//            }
//            returned.quoteLineItemTextCollections.get(text.QuoteLineItemRef__c).add(text);
//
//        }
//        //load textSettings
//        returned.quoteTextSettings = new Map<String,String>{
//                'ZH12'=>'Internal comment',
//                'ZH00'=>'HDR all begin',
//                'ZH02'=>'HDR Sales/Bil. begin',
//                'ZH05'=>'HDR Remarks begin',
//                'ZH09'=>'Sales note for customer',
//                '0012'=>'Shipping instructions',
//                'ZH08'=>'Packing instructions',
//                'ZH01'=>'HDR all end',
//                'ZH03'=>'HDR Sales/Bil. end',
//                'ZH04'=>'HDR Remarks end',
//                'ZH06'=>'HDR T&C Sales',
//                'ZH11'=>'Add. Purchase information hdr'
//        };
//
//        returned.quoteLineItemTextSettings = new Map<String,String>{
//                '0001'=>'Material Sales Text',
//                '0006'=>'Production memo',
//                'ZI01'=>'Configuration',
//                'ZI02'=>'Add. Item Information',
//                'ZI03'=>'Installation location',
//                'ZI05'=>'Add. Purchase information'
//        };


        //returned.quoteLineItems = returned.quote.QuoteLineItems;
        return returned;
    }

    @AuraEnabled
    public static Response auraSaveTextCollection(String textCollection){
        Response result = new Response();
                 result.status = 200;
                 result.message = '';
        System.debug('### AurasavetextCollection ####');
        System.debug((List<Object>)JSON.deserializeUntyped(textCollection));
        try{
            List<Text__c> texts = new List<Text__c>();
            Set<String> quoteIds = new Set<String>();
            Set<String> quoteLineItemsIds = new Set<String>();

            for(Object obj : (List<Object>)JSON.deserializeUntyped(textCollection)){
                Map<String,Object> item  = (Map<String,Object>)obj;

                // Extract recordId and SAPKey from the externalId
                List<String> mapping = ((String)item.get('externalId')).split('-');

                Text__c text = new Text__c();
                        text.Language__c = (String)item.get('key');
                        text.Text__c     = item.get('value') == null ? null : (String)item.get('value');
                        text.SAPKey__c   = mapping[0];
                        text.ExternalId__c = (String)item.get('externalId');

                if(mapping[2].startsWith('0Q0')){
                    text.QuoteRef__c = mapping[2];
                    quoteIds.add(mapping[2]);
                }else if(mapping[2].startsWith('0QL')){
                    text.QuoteLineItemRef__c = mapping[2];
                    quoteLineItemsIds.add(mapping[2]);
                }

                texts.add(text);
            }

            // System.debug(textCollection);
            // Handle the UPSERT

            List<Text__c>   toInsert = new List<Text__c>();
            List<Text__c>   toUpdate = new List<Text__c>();
            Set<String>     toDelete = new Set<String>();


            Map<String,Id> textsIdMapping = new Map<String,Id>();
            for(Text__c text : [select id,AutoExternalId__c from Text__c where QuoteRef__c in:quoteIds OR QuoteLineItemRef__c in:quoteLineItemsIds]){
                textsIdMapping.put(text.AutoExternalId__c,text.Id);
            }



            for(Text__c text : texts){

                String externalId = text.ExternalId__c;

                // Reset the externalId
                text.ExternalId__c = null;

                System.debug('External Id is: '+externalId+' Text ExternalId: ');
                // Update or Delete
                if(textsIdMapping.containsKey(externalId)){

                    text.Id = textsIdMapping.get(externalId);
                    if(String.isNotEmpty(text.Text__c)){
                        // UPDATE
                        System.debug('Update => ');
                        System.debug(text);
                        toUpdate.add(text);
                    }else{
                        // DELETE
                        toDelete.add(textsIdMapping.get(externalId));
                    }
                }else{
                    if(String.isNotEmpty(text.Text__c)){
                        // INSERT
                        toInsert.add(text);
                        System.debug('Insert => ');
                        System.debug(text);
                    }

                }


            }


            insert toInsert;
            update toUpdate;
            delete [select id from Text__c where Id in: toDelete];
        }catch(Exception e){
            result.message = e.getMessage();
            result.status  = 400;
        }


        return result;
    }

    @AuraEnabled
    public static List<QuoteLineItem> auraGetQuoteLineItems (List<String> ids,List<String> paramQuoteLineItemFields){
        List<QuoteLineItem> returned = new List<QuoteLineItem>();
        String query = String.format('SELECT {0},(select id,Text__c,SAPKey__c,Language__c,AutoExternalId__c from Texts__r) FROM QuoteLineItem where Id in : ids',new List<String>{String.join(paramQuoteLineItemFields,',')});
        System.debug(ids);
        System.debug(query);
        return (List<QuoteLineItem>)database.query(query);
    }

    @AuraEnabled
    public static Map<String,Text__c> auraGetTextCollection(String type,String id){
        Map<String,Text__c> result = new Map<String,Text__c>();
        List<Text__c> textCollection = new List<Text__c>();

        if(type == 'Quote'){
            textCollection = [select id,Text__c,SAPKey__c,Language__c from Text__c where QuoteRef__c =: id];
        }else if(type == 'QuoteLineItem'){
            textCollection = [select id,Text__c,SAPKey__c,Language__c from Text__c where QuoteLineItemRef__c =: id];
        }


        for(Text__c text : textCollection){
            result.put(text.SAPKey__c,text);
        }

        return result;
    }

    @AuraEnabled
    public static Map<String,String> auraGetTextSettings(String type){
        Map<String,String> result= new Map<String,String>();
        if(type == 'Quote'){
            result = new Map<String,String>{
                    'CUR'=>'Sales note for customer',
                    'DIN'=>'Shipping instructions',
                    'PKG'=>'Packing instruction'
            };
        }else if(type == 'QuoteLineItem'){
            result = new Map<String,String>{
                    'AGW'=>'Item text Location',
                    'PSI'=>'Item text PSI'
            };
        }

        return result;
    }

    @AuraEnabled
    public static List<QuoteLineOption> auraGetOptions(String paramQuoteLineItemId) {
        List<QuoteLineOption> returned = new List<QuoteLineOption>();

        for(Integer i = 0; i < 30; i++) {
            QuoteLineOption line = new QuoteLineOption();
                            line.Name = 'Name' + (i+1);
                            line.Label = 'Label ' + (i+1);
                            line.IsRequired = i == 1;
                            line.OptionType = 'String';
                            line.HelpText = 'This is the help text for the field "Label ' + (i+1) + '" and can be quite long.';
            returned.add(line);
        }

        return returned;
    }



    @AuraEnabled
    public static String auraSaveQuoteLineItems(List<QuoteLineItem> quoteLineItems) {
        String result = QuoteConfiguratorController.SUCCESS;
        try{
            update quoteLineItems;
        }catch(Exception e){
            result = e.getMessage();
        }

        return result;
    }

    @AuraEnabled
    public static String auraSaveQuote(Quote quote){
        String result = QuoteConfiguratorController.SUCCESS;
        try{
            update quote;
        }catch(Exception e){
            result = e.getMessage();
        }

        return result;
    }

    @AuraEnabled
    public static Response auraDeleteQuoteLineItems(List<QuoteLineItem> quoteLineItems){
        Response result = new Response();
        result.status = 200;
        result.message = '';

        try{
            delete quoteLineItems;
        }catch(Exception e){
            result.message = e.getMessage();
            result.status  = 400;
        }

        return result;
    }

    @AuraEnabled
    public static SAPPO_SalesOrderService.SalesOrderResultWrapper auraGetPrice(String quoteId,List<QuoteLineItem> quoteLineItems){
        SAPPO_SalesOrderService SOS = new SAPPO_SalesOrderService();
        Quote quo = SOS.Utility.getQuotes(quoteId);


        SAPPO_SalesOrderService.SalesOrderSimulationWrapper wrapper = new SAPPO_SalesOrderService.SalesOrderSimulationWrapper();
        wrapper.account = quo.Account;
        wrapper.quoteLineItems = quo.QuoteLineItems;
        wrapper.quote = quo;

        System.debug(JSON.serialize(wrapper));

        SAPPO_SalesOrderService.SalesOrderResultWrapper result = SOS.syncSimulatedSalesOrder(wrapper,true);
        System.debug(JSON.serialize(result));
        return result;
    }

    @AuraEnabled
    public static Map<String,String> auraCheckQuoteLineItems(String quoteId,List<String> products){
        Map<String,String> result = new Map<String,String>();
        if(quoteId == null){
            for(String key : products){
                result.put(key,'The QuoteId is empty');
            }
            return result;
        }




        // By Default
        for(String key : products){
            system.debug(key);
            result.put(key,'Doesn\'t exist for this salesorg');
        }

        // Check the pricebook if they exist in this salesOrg
        Quote quote = [select id,SalesOrg__c from Quote where Id =: quoteId];
        for(PricebookEntry pbe : [select Product2.Id,Product2.ExternalId__c from PricebookEntry where IsActive = true AND Product2.ExternalId__c IN :products and Pricebook2.ExternalId__c =: quote.SalesOrg__c]){
            result.put(pbe.Product2.ExternalId__c,'');
        }

        // Check for duplicate
        for(QuoteLineItem quoteLineItem : [select id, Product2.ExternalId__c from QuoteLineItem where QuoteId =: quoteId and Product2.ExternalId__c in: products]){
            result.put(quoteLineItem.Product2.ExternalId__c,DUPLICATE);
        }

        return result;
    }

    @AuraEnabled
    public static Response auraInsertQuoteLineItems(String quoteId,List<String> paramQuoteLineItemFields,String mapping){
        System.debug('Informations - auraInsertQuoteLineItems');
        System.debug(quoteId);
        System.debug(paramQuoteLineItemFields);
        System.debug(mapping);
        List<QuoteLineItem> QuoteLineItems = new List<QuoteLineItem>();
        Map<String,Object> tMapping = (Map<String,Object>)JSON.deserializeUntyped(mapping);
        Quote quote = [select id,SalesOrg__c,CurrencyIsoCode from Quote where Id =: quoteId];
        Id priceBookId = [select id from Pricebook2 where ExternalId__c =:quote.SalesOrg__c limit 1].Id;

        //Id standardPriceBookId = Test.isRunningTest()?Test.getStandardPricebookId():[select id from Pricebook2 where IsStandard = true limit 1].Id;
        System.debug('Id => '+priceBookId);
        System.debug('Quote => '+quote.SalesOrg__c);
        System.debug(tMapping);
        System.debug('Products =>'+tMapping.size());

        // Filter by salesorg, currencyCode (Only CHF to search ) and Product2
        List<PricebookEntry> pricebookEntriesSearch = [select Id,Product2.Id,Product2.ExternalId__c,UnitPrice,Product2.Name from PricebookEntry where IsActive = true AND Product2.ExternalId__c IN :tMapping.keySet() and Pricebook2.IsStandard = true and CurrencyIsoCode =: quote.CurrencyIsoCode];
        System.debug('Query: select Id,Product2.Id,Product2.ExternalId__c,UnitPrice,Product2.Name from PricebookEntry where IsActive = true AND Product2.ExternalId__c IN ('+tMapping.keySet()+') and Pricebook2.IsStandard = true and CurrencyIsoCode ='+quote.CurrencyIsoCode);
        for(PricebookEntry pbe : pricebookEntriesSearch){
            Map<String,Object> mappingItem = (Map<String,Object>)tMapping.get(pbe.Product2.ExternalId__c);
            Decimal listPrice = (Decimal)mappingItem.get('listPrice');
            Integer quantity  = (Integer)mappingItem.get('quantity');
            Integer section  = (Integer)mappingItem.get('section');
            Integer position = (Integer)mappingItem.get('position');
            Integer discount = (Integer)mappingItem.get('discount');
            Integer additionalDiscount = (Integer)mappingItem.get('additionalDiscount');
            String configuration = (String)mappingItem.get('configuration');
            Boolean isConfigurable = (Boolean)mappingItem.get('isConfigurable');

            QuoteLineItem quoteLineItem = new QuoteLineItem();
            quoteLineItem.PricebookEntry = new PricebookEntry(ExternalId__c='standard-'+pbe.Product2.ExternalId__c+'-'+quote.CurrencyIsoCode);
            quoteLineItem.Product2Id        = pbe.Product2.Id;
            quoteLineItem.QuoteId           = quoteId;
            quoteLineItem.UnitPrice         = listPrice != null ? listPrice : pbe.UnitPrice ;
            quoteLineItem.Description       = pbe.Product2.Name;
            quoteLineItem.Quantity          = quantity != null ?  quantity:0;
            quoteLineItem.Section__c        = section  != null ?   section:0;
            quoteLineItem.Position__c       = position != null ?  position:0;
            quoteLineItem.additionalDiscount__c = additionalDiscount != null ?  additionalDiscount:0;
            quoteLineItem.Discount          = discount != null ?  discount:0;
            quoteLineItem.Configuration__c  = configuration;
            quoteLineItem.isConfigurable__c = isConfigurable;
            System.debug(quoteLineItem);
            QuoteLineItems.add(quoteLineItem);
        }


        Response res = new Response();
        res.status = 200;
        res.data = new List<Object>();
        List<Id> quoteLineItemIds = new List<Id>();

        try{
            insert QuoteLineItems;
            System.debug(QuoteLineItems);
            for(QuoteLineItem item : QuoteLineItems){
                quoteLineItemIds.add(item.Id);
            }
            res.data = QuoteConfiguratorController.auraGetQuoteLineItems(quoteLineItemIds,paramQuoteLineItemFields);
        }catch(Exception e){
            res.status = 400;
            res.message = e.getMessage();
        }

        // In case the data is empty, that's mean there product is not in the pricebook
        if(res.status != 400 && res.data.size() == 0){
            res.status = 400;
            res.message = 'QuoteLineItems are empty, the products are not available for this SalesOrg';
        }

        return res;

    }


    /*

            Configurator V2

     */

    @AuraEnabled
    public static Response auraSaveQuoteSection(String quoteId,String section){
        Response result = new Response();
        result.status = 200;
        result.message = '';

        try{
            Quote quote = new Quote(Id=quoteId,Sections__c=section);
            update quote;
        }catch(Exception e){
            result.message = e.getMessage();
            result.status  = 400;
        }

        return result;

    }


    @AuraEnabled
    public static Response auraCreateOptions(String salesOrg,List<String> optionIds,String quoteLineItemId){

        QuoteLineItem quoteLineItem = [select id,MaterialOptions__c,CurrencyIsoCode from QuoteLineItem where Id=: quoteLineItemId];
        Response result = new Response();
        result.status = 200;
        result.message = '';
        result.data = quoteLineItem.MaterialOptions__c != null ?(List<Object>)JSON.deserializeUntyped(quoteLineItem.MaterialOptions__c):new List<Object>();

        List<Product2> products = [select id,Name,Description,SAPInstance__c,SAPNumber__c from Product2 where Id in: optionIds];

        Set<String> priceBookEntryIds = new Set<String>();
        for(Product2 prod : products){
            // We use the standard pricebook to store the basics prices (In the futur we could move it to the custom pricebook but this gonna increase the storage)
            priceBookEntryIds.add('standard-'+prod.SAPInstance__c+'-'+prod.SAPNumber__c+'-'+quoteLineItem.CurrencyIsoCode);
        }

        Map<String,Decimal> priceMapping = new Map<String,Decimal>();
        for(PricebookEntry pbe :  [select id,Product2Id,UnitPrice from PricebookEntry where ExternalId__c in: priceBookEntryIds]){
            priceMapping.put(pbe.Product2Id,pbe.UnitPrice);
        }
        System.debug(priceBookEntryIds);
        System.debug([select id,Product2Id,UnitPrice from PricebookEntry where ExternalId__c in: priceBookEntryIds]);

        for(Product2 prod : products){
            result.data.add((Object)new Map<String,Object>{
                    'id'            => prod.Id,
                    'name'          => prod.name,
                    'description'   => prod.Description,
                    'price'         => priceMapping.containsKey(prod.Id)?priceMapping.get(prod.Id):null,
                    'selected'      => false
            });
        }

        try{
            quoteLineItem.MaterialOptions__c=JSON.serialize(result.data);
            update quoteLineItem;
        }catch(Exception e){
            result.message = e.getMessage();
            result.status  = 400;
        }

        return result;
    }

    @AuraEnabled
    public static Response auraUpdateOptions(String quoteLineItemId,String options){

        Response result = new Response();
        result.status = 200;
        result.message = '';
        result.data = null;

        try{
            QuoteLineItem quoteLineItem = new QuoteLineItem(Id=quoteLineItemId,MaterialOptions__c=options);
            update quoteLineItem;
        }catch(Exception e){
            result.message = e.getMessage();
            result.status  = 400;
        }

        return result;
    }

    @AuraEnabled
    public static Response auraUpsertAlternatives(String quoteLineItemId,String alternativeProducts){

        Response result = new Response();
        result.status = 200;
        result.message = '';
        result.data = null;

        try{
            QuoteLineItem quoteLineItem = new QuoteLineItem(Id=quoteLineItemId,AlternativeProducts__c=alternativeProducts);
            update quoteLineItem;
        }catch(Exception e){
            result.message = e.getMessage();
            result.status  = 400;
        }

        return result;
    }

    @AuraEnabled
    public static User auraLoadUserSettings(){
        return [select id,QuoteConfiguratorSettings__c FROM User where id =: UserInfo.getUserId()];
    }

    @AuraEnabled
    public static Response auraUpdateUserSettings(String settings){
        Response result = new Response();
        result.status = 200;
        result.message = '';
        result.data = null;
        try{
            update new User(Id=UserInfo.getUserId(),QuoteConfiguratorSettings__c=settings);
        }catch(Exception e){
            result.message = e.getMessage();
            result.status  = 400;
        }

        return result;
    }

    @AuraEnabled
    public static List<QuoteLineItem> auraGenerateQuoteLineItems(List<String> selected,String salesOrgId,String quoteId){

        List<QuoteLineItem> result = new List<QuoteLineItem>();
        String standardPriceBookId = Test.isRunningTest()?Test.getStandardPricebookId():[SELECT id FROM Pricebook2 WHERE isStandard = true LIMIT 1].Id;

        Quote quo = [SELECT id,Name,CurrencyIsoCode FROM Quote WHERE Id =: quoteId];

        Map<Id,Product2> productIdsMap = new Map<Id,Product2>([select id,ExternalId__c from Product2 where id in: selected]);
        List<PricebookEntry> pricebookEntries = [SELECT id,UnitPrice,Product2Id,ExternalId__c,CurrencyIsoCode FROM PricebookEntry
        WHERE Product2Id IN : selected
        AND CurrencyIsoCode =: quo.CurrencyIsoCode
        AND Pricebook2Id =: standardPriceBookId];


        Map<Id,PricebookEntry> productMap = new Map<Id,PricebookEntry>();

        // Pricebook to Product mapping
        for(PricebookEntry pbE : pricebookEntries){
            productMap.put(pbE.Product2Id,pbe);
        }

        // Check for missing pricebooks
        new SelectProductsAuraController.Utility().insertMissingPricebookEntries(selected,productMap,quo,productIdsMap,standardPriceBookId);




        for(String productId : selected){
            QuoteLineItem item = new QuoteLineItem();
            item.PricebookEntry = productMap.get(productId);
            item.PricebookEntryId = productMap.get(productId).Id;
            item.Quantity = null;
            item.QuoteId = quoteId;
            item.UnitPrice = productMap.get(productId).UnitPrice;
            item.Product2 = new Product2(Id=productId,ExternalId__c=productIdsMap.get(productId).ExternalId__c);

            result.add(item);
        }
        return result;
    }

}