/*******************************************************************************************************
Description: Trigger handler for Work Detail Trigger

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger: SVMX_WorkDetailTrggr.cls
    Service Manager: SVMX_WorkDetailServiceManager
    Test class: SVMX_WorkDetailTrggrHandler_UT.cls
    
 
Author: Ranjitha S
Date: 19-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
10-11-17        Ranjitha        Sprint 2 - Added code for Misuse and Abuse
9-Nov-2017      Pooja Singh     Modified afterUpdate method to calculate travel line qty
*******************************************************************************************************/
public class SVMX_WorkDetailTrggrHandler extends SVMX_TriggerHandler {

    private list<SVMXC__Service_Order_Line__c> newWDetailList;
    private list<SVMXC__Service_Order_Line__c> oldWDetailList;
    private Map<Id, SVMXC__Service_Order_Line__c> newWDetailMap;
    private Map<Id, SVMXC__Service_Order_Line__c> oldWDetailMap;
    public static string rejected = Label.rejected;
    
    public SVMX_WorkDetailTrggrHandler() {
        this.newWDetailList = (list<SVMXC__Service_Order_Line__c>) Trigger.new;
        this.oldWDetailList = (list<SVMXC__Service_Order_Line__c>) Trigger.old;
        this.newWDetailMap = (Map<Id, SVMXC__Service_Order_Line__c>) Trigger.newMap;
        this.oldWDetailMap = (Map<Id, SVMXC__Service_Order_Line__c>) Trigger.oldMap;
    }

    public static id usageRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('UsageConsumption','SVMXC__Service_Order_Line__c');
    public static id estimateRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Estimate','SVMXC__Service_Order_Line__c');
    public static id prodServicedRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Products_Serviced','SVMXC__Service_Order_Line__c');
    public static id planedUsageRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Planned_Usage','SVMXC__Service_Order_Line__c');
    
    //Sprint 2 change - Update Warranty discount based on billing type
    public override void beforeInsert(){
        
        List<SVMXC__Service_Order_Line__c> WDList = new List<SVMXC__Service_Order_Line__c>();
        map<string,SVMX_Sales_Office__c> countrySalesOffMap = new map<string,SVMX_Sales_Office__c>();
          //list<String> countryList = new List<String>();
        list<SVMX_Sales_Office__c> salesOfficeList = new List<SVMX_Sales_Office__c>();
        List<SVMXC__Service_Order_Line__c> expenseLineList = new List<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> instWDList = new List<SVMXC__Service_Order_Line__c>();
          set<id> woId = new set<id>();
          Set<Id> instWOIds = new Set<Id>();
          map<id,string> woMap = new map<id,string>();

            for(SVMXC__Service_Order_Line__c workDetailLine : newWDetailList){
                
                system.debug('workDetailLine'+workDetailLine);
                if(workDetailLine.SVMX_SAP_Product__c == null && (workDetailLine.SVMXC__Line_Type__c == 'Labor' || workDetailLine.SVMXC__Line_Type__c == 'Travel' || workDetailLine.SVMXC__Line_Type__c == 'Expenses')){
                    //countryList.add(workDetailLine.SVMX_Country__c);
                    system.debug('workDetailLine1'+workDetailLine);
                    woId.add(workDetailLine.SVMXC__Service_Order__c);
                }
                system.debug('chargeable IN '+workDetailLine.SVMX_Chargeable_Qty__c);
                //2-05-18 Ranjitha   Update chargable Qty based on Billable Qty/Estimated Qty
                if((workDetailLine.SVMX_Chargeable_Qty__c == null || workDetailLine.SVMX_Chargeable_Qty__c == 0) && workDetailLine.SVMXC__Billable_Quantity__c != null && workDetailLine.RecordTypeId == usageRecordTypeId){
                    workDetailLine.SVMX_Chargeable_Qty__c = workDetailLine.SVMXC__Billable_Quantity__c;
                }
                else if((workDetailLine.SVMX_Chargeable_Qty__c == null || workDetailLine.SVMX_Chargeable_Qty__c == 0) && workDetailLine.SVMXC__Estimated_Quantity2__c != null && workDetailLine.RecordTypeId == estimateRecordTypeId){
                    workDetailLine.SVMX_Chargeable_Qty__c = workDetailLine.SVMXC__Estimated_Quantity2__c;
                }
                system.debug('chargeable Out '+workDetailLine.SVMX_Chargeable_Qty__c);
                //2-05-18 Ranjitha   Update surcharge from Expense Pricing on Service Contract if the expense type is Markup
                if(workDetailLine.SVMXC__Line_Type__c == System.Label.Expenses){
                    expenseLineList.add(workDetailLine);
                }
            }
        if(!expenseLineList.isEmpty())
            SVMX_WorkDetailServiceManager.addExpenseMarkup(expenseLineList);
            
            if(woId.size() > 0){
                list<SVMXC__Service_Order__c>  word = SVMX_WorkOrderDataManager.WoOfWdQuery(woId);
                
                for(SVMXC__Service_Order__c wo:word){
                    
                    system.debug('wo***'+wo);
                    if(!woMap.containskey(wo.id)){
                        
                        system.debug('wo1'+wo);
                        woMap.put(wo.id,wo.SVMXC__Country__c);
                        system.debug('woMap'+woMap);
                    }
                }
             }   

          if(woMap.size() > 0){
             salesOfficeList=SVMX_SalesOfficeDataManager.countrySalesOfficeQuery(woMap.values());
              for(SVMX_Sales_Office__c sof: salesOfficeList){
                    system.debug('sof@@@@'+sof);
                    if(!countrySalesOffMap.containskey(sof.SVMX_Country__c)){
                             system.debug('sof1'+sof.SVMX_Country__c);
                             countrySalesOffMap.put(sof.SVMX_Country__c,sof);
                        }
              }
          }
    
        if(countrySalesOffMap.size() > 0){
            for(SVMXC__Service_Order_Line__c workDetailLine : newWDetailList){
                system.debug('wo111'+woMap.get(workDetailLine.SVMXC__Service_Order__c));
                if(workDetailLine.SVMX_SAP_Product__c == null && countrySalesOffMap.size() > 0 && countrySalesOffMap.containskey(woMap.get(workDetailLine.SVMXC__Service_Order__c))){
                    system.debug('workDetailLine$$'+workDetailLine);
                    if(workDetailLine.SVMXC__Line_Type__c == 'Labor' && workDetailLine.SVMX_Travel_Flag__c == false){
                            workDetailLine.SVMX_SAP_Material_Number__c = countrySalesOffMap.get(woMap.get(workDetailLine.SVMXC__Service_Order__c)).SVMX_Labor_Product__r.SAPNumber__c;
                            workDetailLine.SVMX_SAP_Product__c = countrySalesOffMap.get(woMap.get(workDetailLine.SVMXC__Service_Order__c)).SVMX_Labor_Product__c;
                    }
                    if(workDetailLine.SVMXC__Line_Type__c == 'Travel' || workDetailLine.SVMX_Travel_Flag__c == true){
                            workDetailLine.SVMX_SAP_Material_Number__c = countrySalesOffMap.get(woMap.get(workDetailLine.SVMXC__Service_Order__c)).SVMX_Travel_Labor_Product__r.SAPNumber__c;
                            workDetailLine.SVMX_SAP_Product__c = countrySalesOffMap.get(woMap.get(workDetailLine.SVMXC__Service_Order__c)).SVMX_Travel_Labor_Product__c;
                    }
                    if(workDetailLine.SVMXC__Line_Type__c == 'Expenses'){
                            workDetailLine.SVMX_SAP_Material_Number__c=countrySalesOffMap.get(woMap.get(workDetailLine.SVMXC__Service_Order__c)).SVMX_Expense_Product__r.SAPNumber__c;
                            workDetailLine.SVMX_SAP_Product__c = countrySalesOffMap.get(woMap.get(workDetailLine.SVMXC__Service_Order__c)).SVMX_Expense_Product__c;
                    }
                }
            }
        }
        
        for(SVMXC__Service_Order_Line__c wl: newWDetailList){
            if(wl.SVMX_Work_Order_Type__c =='Planned Services (PPM)') {
               if(wl.SVMX_PM_Charge__c=='No'){
                   wl.SVMXC__Is_Billable__c=false;
                   wl.SVMXC__Discount__c = 100;
               }
               else 
                wl.SVMXC__Is_Billable__c=true;
            }

            if(wl.SVMX_Work_Order_Type__c == system.label.Installation) {
              
                   wl.SVMX_Billing_Type__c = System.label.Free_Of_Charge;
                   instWOIds.add(wl.SVMXC__Service_Order__c);
                   instWDList.add(wl);
            }
            
            if(wl.SVMX_Work_Order_Type__c == 'Quoted Works' && wl.RecordTypeId == usageRecordTypeId)
                wl.SVMXC__Is_Billable__c=false;
            
            if(wl.SVMXC__Group_Member__c == null && wl.SVMX_Work_Order_Technician__c != null && (wl.SVMXC__Line_Type__c == 'Parts' || wl.SVMXC__Line_Type__c == 'Labor' || wl.SVMXC__Line_Type__c == 'Travel') &&  ( wl.RecordTypeId == usageRecordTypeId ||  wl.RecordTypeId == estimateRecordTypeId ))
               WDList.add(wl);
        }
       
        if(!WDList.isEmpty())
            SVMX_WorkDetailServiceManager.addTechnician(WDList);

            system.debug('newWDetailList ###'+newWDetailList);
            
        if(instWOIds.size() > 0) {

            Map<Id,SVMXC__Service_Order__c> insWOMap = new Map<Id,SVMXC__Service_Order__c>([SELECT Id, Name, SVMX_SAP_Installation_SO_Item_Number__c 
                                                                                            FROM SVMXC__Service_Order__c
                                                                                            WHERE Id IN : instWOIds]); 

            for(SVMXC__Service_Order_Line__c wdl : instWDList) {

                wdl.SVMX_SAP_Installation_SO_Item_Number__c = insWOMap.get(wdl.SVMXC__Service_Order__c).SVMX_SAP_Installation_SO_Item_Number__c;
            }
        }
        
        Set<Id>   workOrderSet=new Set<Id>();
        Map<id,SVMXC__Service_Order__c> WOList= new Map<id,SVMXC__Service_Order__c>();
        
        Map<id,List<SVMXC__Service_Order_Line__c>> WDList2 = new Map<id,List<SVMXC__Service_Order_Line__c>>();
        
        for(SVMXC__Service_Order_Line__c wl: newWDetailList){
            system.debug('WL '+wl);
            workOrderSet.add(wl.SVMXC__Service_Order__c);
                system.debug('77777 ----> WL '+wl.SVMXC__Service_Order__r.SVMXC__Order_Type__c);
            
            if(wl.SVMX_Billing_Type__c == 'Warranty' && wl.RecordTypeId == usageRecordTypeId){
                if(WDList2.containsKey(wl.SVMXC__Service_Order__c))
                     WDList2.get(wl.SVMXC__Service_Order__c).add(wl);
                else
                     WDList2.put(wl.SVMXC__Service_Order__c, new List<SVMXC__Service_Order_Line__c> {wl});
            }
            else if(wl.SVMX_Billing_Type__c == 'Goodwill' && wl.RecordTypeId == usageRecordTypeId){
                wl.SVMXC__Discount__c=100;
            }
        }
        system.debug('WDList2 '+WDList2);
        if(!WDList2.isEmpty())
        SVMX_WorkDetailServiceManager.updateWarrantyDiscount(WDList2); 
        
        system.debug('After method exit WDList 2 ' +WDList2);
       
    } 
    
    public override void afterInsert(){

        List<SVMXC__Service_Order_Line__c> newWLList = new  List<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> wdIntegrationList = new  List<SVMXC__Service_Order_Line__c>();
        Map<Id,SVMXC__Service_Order_Line__c> UpWDLines = new Map<Id,SVMXC__Service_Order_Line__c>();
        Map<id,List<SVMXC__Service_Order_Line__c>> newWOLaborLinesMap = new  Map<id,List<SVMXC__Service_Order_Line__c>>();
        set<id> laborLinesid = new set<id>();
        list<SVMX_Custom_Feature_Enabler__c> customSett = SVMX_Custom_Feature_Enabler__c.getall().values();
        set<string> country = new set<string>();
        set<String> partsExt = new set<String>();
        set<Id> wlPriceLines = new set<Id>();
        List<SVMXC__Service_Order_Line__c> SparePartsOrder =new  List<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> workDetailsLinesParts = new List<SVMXC__Service_Order_Line__c>();
           
        for(SVMX_Custom_Feature_Enabler__c cs: customSett){
                if(cs.Custom_Labor_Activity__c)
                    country.add(cs.name);
                if(cs.SVMX_SAP_Parts_Price__c)
                    partsExt.add(cs.name);
        }
            
        //check if Work detail line type is labor, recordtype is usageConsumption and start date not null; 

        for(SVMXC__Service_Order_Line__c wl : newWDetailList){
            if(wl.SVMXC__Line_Type__c == System.Label.Labor &&  wl.RecordTypeId == usageRecordTypeId  && wl.SVMXC__Start_Date_and_Time__c != null)
                newWLList.add(wl);
            if(wl.SVMXC__Line_Type__c == System.Label.Labor && wl.RecordTypeId == estimateRecordTypeId)
                newWLList.add(wl);
            if(wl.SVMXC__Line_Type__c == System.Label.Labor && wl.SVMXC__Start_Date_and_Time__c != null && wl.RecordTypeId == usageRecordTypeId && wl.SVMXC__End_Date_and_Time__c != null && wl.SVMX_Check_Secondary_Technician__c) { 
                laborLinesid.add(wl.id);
                if(newWOLaborLinesMap.containsKey(wl.SVMXC__Service_Order__c))
                    newWOLaborLinesMap.get(wl.SVMXC__Service_Order__c).add(wl);                 
                else
                    newWOLaborLinesMap.put(wl.SVMXC__Service_Order__c,new List<SVMXC__Service_Order_Line__c> {wl});
            }
            system.debug('line type '+wl.SVMXC__Line_Type__c);
            system.debug('wo country '+wl.SVMX_Country__c);
            system.debug(' custom setting countries '+partsExt);
            system.debug('country qualified '+partsExt.contains(wl.SVMX_Country__c));
            system.debug('part '+wl.SVMXC__Product__c );
            if(wl.SVMXC__Line_Type__c == System.Label.Parts && wl.SVMXC__Product__c !=null && partsExt.contains(wl.SVMX_Country__c) && wl.SVMX_Received_Price_From_SAP__c == false && (wl.RecordTypeId == usageRecordTypeId || wl.RecordTypeId == estimateRecordTypeId)){
                
                wlPriceLines.add(wl.id);
            }

            if(wl.SVMX_Work_Order_Type__c == System.Label.Spare_Parts_Order && wl.SVMX_Create_Spare_Parts_Order__c == true )
                SparePartsOrder.add(wl);

            //Work Order handling for Integration with SAP

            if(wl.SVMX_Work_Order_Type__c == System.Label.Reactive || wl.SVMX_Work_Order_Type__c == System.Label.Quoted_Works
                    || wl.SVMX_Work_Order_Type__c == System.Label.Installation || wl.SVMX_Work_Order_Type__c == System.Label.Planned_Services_PPM) {

                if(wl.SVMXC__Line_Type__c == System.Label.Parts &&  wl.RecordTypeId == usageRecordTypeId && wl.SVMXC__Actual_Price2__c > 0
                    && wl.SVMXC__Actual_Quantity2__c != null && wl.SVMXC__Actual_Quantity2__c > 0) {

                    wdIntegrationList.add(wl);    
                }

                //All Planned Usage Lines must go to SAP immediately. Not just for Quoted Works
                if(wl.RecordTypeId == planedUsageRecordTypeId && (wl.SVMXC__Line_Type__c == System.Label.Parts || wl.SVMXC__Line_Type__c == System.Label.Labor
                    || wl.SVMXC__Line_Type__c == System.Label.Travel || wl.SVMXC__Line_Type__c == System.Label.Expenses
                    || wl.SVMXC__Line_Type__c == System.Label.Vendor_Goods)
                    && wl.SVMXC__Actual_Price2__c > 0 && wl.SVMXC__Actual_Quantity2__c != null && wl.SVMXC__Actual_Quantity2__c > 0) {

                    wdIntegrationList.add(wl);  
                }
            }
            
            if(wl.RecordTypeId == usageRecordTypeId && wl.SVMXC__Line_Type__c == system.label.parts && wl.SVMXC__Product__c!= null && wl.SVMX_ProductDetail__c == null )
                workDetailsLinesParts.add(wl);
            
        }
        
        if(!newWOLaborLinesMap.isEmpty()){
            SVMX_WorkDetailServiceManager.setWorkComplete(newWOLaborLinesMap,laborLinesid);
        }

        if(!wdIntegrationList.isEmpty()) {
            SVMX_SAPSalesOrderUtility.handleSAPSalesOrderCreation(wdIntegrationList);
        }
        
        //check if the country value is there in the custom setting (Custom Labor Activity)
        if(!newWLList.isEmpty()){ 
            map<id,SVMXC__Service_Order_Line__c> wrkDetailList = new map<id,SVMXC__Service_Order_Line__c>();
            map<id,SVMXC__Service_Order_Line__c> scMap = new map<id,SVMXC__Service_Order_Line__c>();
      
            scMap = SVMX_WorkDetailDataManager.workDetailQuery(newWLList);
           
        
            for(SVMXC__Service_Order_Line__c wl : scMap.values()){
                if(country.contains(wl.SVMXC__Service_Order__r.SVMXC__Country__c) && wl.SVMXC__Service_Order__r.SVMXC__Service_Contract__c != null && wl.SVMXC__Service_Order__r.SVMXC__Purpose_of_Visit__c ==  System.Label.Technician_Onsite ){
                    wrkDetailList.put(wl.id, wl);
                }
            }
            
            if(!wrkDetailList.isEmpty())
               UpWDLines=  SVMX_WorkDetailServiceManager.SetLaborActivityTypes(wrkDetailList, usageRecordTypeId, estimateRecordTypeId);
        }

        //get price from SAP
        if(wlPriceLines.size() > 0){
             SVMX_WorkDetailServiceManager.getPricefromSAP(wlPriceLines);
        }

        //create Parts Order
        if(SparePartsOrder.size() > 0 )
            SVMX_WorkDetailServiceManager.createPartsOrder(SparePartsOrder);
        
        if(!workDetailsLinesParts.isEmpty())
            SVMX_WorkDetailServiceManager.populateProductDetail(workDetailsLinesParts);

        system.debug('UpWDLines '+UpWDLines);
        if(UpWDLines.size() > 0)
            SVMX_WorkDetailDataManager.UpdaWorkDetails(UpWDLines.values());
            
             List <SVMXC__Service_Order_Line__c> timewolist = new  List <SVMXC__Service_Order_Line__c>();
              
             for(SVMXC__Service_Order_Line__c wd:newWDetailList){
                 system.debug('TestTravel'+wd.SVMXC__Line_Type__c);
                 if(wd.SVMXC__Group_Member__c!=null && wd.SVMXC__Start_Date_and_Time__c!=null && wd.SVMXC__End_Date_and_Time__c!=null &&(wd.SVMXC__Line_Type__c == System.Label.Labor || wd.SVMXC__Line_Type__c == System.Label.Travel)) 
                  timewolist.add(wd);
                   
               }
              SVMX_PS_TS_TimesheetUtils timesheetUtils = new SVMX_PS_TS_TimesheetUtils();
             if(timewolist.size()>0)
           timesheetUtils.handleEventsFromWorkDetails(timewolist,null,null,true,null,null);
    }
          
    //Sprint-2 change - Update Warranty discount based on billing type
    
    
    public override void beforeUpdate(){
        
        List<SVMXC__Service_Order_Line__c> WDList = new List<SVMXC__Service_Order_Line__c>();
        Map<id,list<SVMXC__Service_Order_Line__c>> WDMap = new Map<id,list<SVMXC__Service_Order_Line__c>>();
        List<SVMXC__Service_Order_Line__c> expenseLineList = new List<SVMXC__Service_Order_Line__c>();
        for(SVMXC__Service_Order_Line__c wl: newWDetailList){
            system.debug('WL in beforeUpdate '+wl );
          
            if(wl.SVMX_Work_Order_Type__c == System.Label.Planned_Services_PPM ){
              if(wl.SVMX_PM_Charge__c=='No'){
                   wl.SVMXC__Is_Billable__c=false;
                   wl.SVMXC__Discount__c = 100;
              }
              else
                  wl.SVMXC__Is_Billable__c=true;
            }
            
            //2-05-18 Ranjitha   Update chargable Qty based on Billable Qty/Estimated Qty
            if((wl.SVMX_Chargeable_Qty__c == null || wl.SVMX_Chargeable_Qty__c == 0|| wl.SVMX_Chargeable_Qty__c != wl.SVMXC__Billable_Quantity__c) && wl.SVMXC__Billable_Quantity__c != null && wl.RecordTypeId == usageRecordTypeId && wl.SVMXC__Is_Billable__c != false && wl.SVMXC__Billable_Quantity__c != oldWDetailMap.get(wl.id).SVMXC__Billable_Quantity__c){
                wl.SVMX_Chargeable_Qty__c = wl.SVMXC__Billable_Quantity__c;
            }
            else if((wl.SVMX_Chargeable_Qty__c == null || wl.SVMX_Chargeable_Qty__c == 0 || wl.SVMX_Chargeable_Qty__c != wl.SVMXC__Estimated_Quantity2__c) && wl.SVMXC__Estimated_Quantity2__c != null && wl.RecordTypeId == estimateRecordTypeId && wl.SVMXC__Estimated_Quantity2__c != oldWDetailMap.get(wl.id).SVMXC__Estimated_Quantity2__c){
                wl.SVMX_Chargeable_Qty__c = wl.SVMXC__Estimated_Quantity2__c;
            }
                        
            if(wl.SVMX_Work_Order_Type__c ==  System.Label.Quoted_Works && wl.RecordTypeId == usageRecordTypeId)
                wl.SVMXC__Is_Billable__c=false;
            
            if(wl.SVMX_Billing_Type__c != oldWDetailMap.get(wl.id).SVMX_Billing_Type__c && wl.RecordTypeId == usageRecordTypeId){ 
                system.debug('wl '+wl);
                if(wl.SVMX_Billing_Type__c == 'Warranty'){
                    if(WDMap.containsKey(wl.SVMXC__Service_Order__c))
                        WDMap.get(wl.SVMXC__Service_Order__c).add(wl);
                    else
                        WDMap.put(wl.SVMXC__Service_Order__c, new List<SVMXC__Service_Order_Line__c> {wl});
                }
                else if(wl.SVMX_Billing_Type__c == 'Non warranty')
                    wl.SVMXC__Discount__c=null;
                
                else if(wl.SVMX_Billing_Type__c == 'Goodwill')
                    wl.SVMXC__Discount__c=100;
            }
            if(wl.SVMXC__Line_Type__c == System.Label.Expenses){
                    expenseLineList.add(wl);
            }
            
            if(wl.SVMXC__Group_Member__c == null && wl.SVMX_Work_Order_Technician__c != null && wl.SVMXC__Line_Type__c == 'Travel' &&  wl.RecordTypeId == usageRecordTypeId)
               WDList.add(wl);
        }
        
        if(!WDList.isEmpty())
            SVMX_WorkDetailServiceManager.addTechnician(WDList);
        
        if(!expenseLineList.isEmpty())
            SVMX_WorkDetailServiceManager.addExpenseMarkup(expenseLineList);
        
        system.debug('WDMap beforeupdate '+WDMap);
        if(!WDMap.isEmpty())
            SVMX_WorkDetailServiceManager.updateWarrantyDiscount(WDMap);
        
      
    }
    
   public override void afterUpdate() {
       
        List<SVMXC__Service_Order_Line__c> newWLList = new  List<SVMXC__Service_Order_Line__c>();
        Map<Id,SVMXC__Service_Order_Line__c> UpWDLines = new Map<Id,SVMXC__Service_Order_Line__c>();
        Map<Id,SVMXC__Service_Order_Line__c> wdIntMap = new Map<Id,SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> wdTravelQtyUpdate = new List<SVMXC__Service_Order_Line__c>();
        Map<id,List<SVMXC__Service_Order_Line__c>> newWOLaborLinesMap = new  Map<id,List<SVMXC__Service_Order_Line__c>>();
        List<SVMXC__Service_Order_Line__c> rejectedWDList = new List<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> partWDList = new List<SVMXC__Service_Order_Line__c>();
        set<id> laborLinesid = new set<id>();
        List<SVMX_Custom_Feature_Enabler__c> customSett = SVMX_Custom_Feature_Enabler__c.getall().values();
        set<Id> wlPriceLines = new set<Id>();
        set<string> country = new set<string>();
        set<string> partsExt = new set<string>();
        List<SVMXC__Service_Order_Line__c> SparePartsOrder =new  List<SVMXC__Service_Order_Line__c>();
        List<SVMXC__Service_Order_Line__c> workDetailsLinesParts = new List<SVMXC__Service_Order_Line__c>();
           
        for(SVMX_Custom_Feature_Enabler__c cs: customSett){
                if(cs.Custom_Labor_Activity__c)
                    country.add(cs.name);
                if(cs.SVMX_SAP_Parts_Price__c)
                    partsExt.add(cs.name);
        }
           
        for(SVMXC__Service_Order_Line__c wl: newWDetailMap.values()){
            if(wl.SVMXC__Start_Date_and_Time__c != oldWDetailMap.get(wl.id).SVMXC__Start_Date_and_Time__c && wl.SVMXC__Line_Type__c == 'Labor' &&  ( wl.RecordTypeId == usageRecordTypeId ||  wl.RecordTypeId == estimateRecordTypeId )){
                newWLList.add(wl);
            }

            //Added to calculate travel line qty 
            if(wl.SVMXC__Start_Date_and_Time__c != null && wl.SVMXC__Line_Type__c == 'Travel' && wl.RecordTypeId == usageRecordTypeId && wl.SVMXC__End_Date_and_Time__c != null && wl.SVMXC__End_Date_and_Time__c != oldWDetailMap.get(wl.id).SVMXC__End_Date_and_Time__c && wl.SVMX_Mileage__c == null){
                wdTravelQtyUpdate.add(wl);
                system.debug('wdTravelQtyUpdate-->'+wdTravelQtyUpdate);
            }
            
            if(wl.SVMXC__Line_Type__c == system.label.labor && wl.SVMXC__Start_Date_and_Time__c != null && wl.RecordTypeId == usageRecordTypeId && wl.SVMXC__End_Date_and_Time__c != null && wl.SVMXC__End_Date_and_Time__c != oldWDetailMap.get(wl.id).SVMXC__End_Date_and_Time__c && wl.SVMX_Check_Secondary_Technician__c) { 
                laborLinesid.add(wl.id);
                if(newWOLaborLinesMap.containsKey(wl.SVMXC__Service_Order__c))
                    newWOLaborLinesMap.get(wl.SVMXC__Service_Order__c).add(wl);                 
                else
                    newWOLaborLinesMap.put(wl.SVMXC__Service_Order__c,new List<SVMXC__Service_Order_Line__c> {wl});
            }

            if(wl.SVMXC__Line_Type__c == System.Label.Parts && (wl.SVMXC__Actual_Quantity2__c != oldWDetailMap.get(wl.id).SVMXC__Actual_Quantity2__c || wl.SVMXC__Estimated_Quantity2__c != oldWDetailMap.get(wl.id).SVMXC__Estimated_Quantity2__c || wl.SVMXC__Product__c != oldWDetailMap.get(wl.id).SVMXC__Product__c) && partsExt.contains(wl.SVMX_Country__c) && wl.SVMX_Received_Price_From_SAP__c == false && (wl.RecordTypeId == usageRecordTypeId || wl.RecordTypeId == estimateRecordTypeId)){

                wlPriceLines.add(wl.id);
            }
            
            if(wl.SVMX_Work_Order_Type__c == System.Label.Parts && ((wl.SVMX_Awaiting_SAP_Response__c == true && wl.SVMX_Awaiting_SAP_Response__c != oldWDetailMap.get(wl.id).SVMX_Awaiting_SAP_Response__c) || (wl.SVMX_Sales_Order_Item_Number__c != null && wl.SVMX_Sales_Order_Item_Number__c != oldWDetailMap.get(wl.id).SVMX_Sales_Order_Item_Number__c))){
                partWDList.add(wl);
            }

             if(wl.SVMX_Work_Order_Type__c == System.Label.Spare_Parts_Order && wl.SVMX_Create_Spare_Parts_Order__c == true && oldWDetailMap.get(wl.id).SVMX_Create_Spare_Parts_Order__c != wl.SVMX_Create_Spare_Parts_Order__c )
                SparePartsOrder.add(wl);

            if(wl.CurrencyIsoCode == oldWDetailMap.get(wl.id).CurrencyIsoCode){

                if(wl.SVMX_Work_Order_Type__c == System.Label.Reactive || wl.SVMX_Work_Order_Type__c == System.Label.Quoted_Works
                        || wl.SVMX_Work_Order_Type__c == System.Label.Installation || wl.SVMX_Work_Order_Type__c == System.Label.Planned_Services_PPM) {

                    if(wl.RecordTypeId == usageRecordTypeId && (wl.SVMXC__Line_Type__c == System.Label.Parts
                        || wl.SVMXC__Line_Type__c == System.Label.Labor || wl.SVMXC__Line_Type__c == System.Label.Travel
                        || wl.SVMXC__Line_Type__c == System.Label.Expenses)) {

                        wdIntMap.put(wl.Id, wl);    
                    }
                }
            }    
            
            //Product stock update if WD part line is rejected
            if(wl.SVMX_SAP_Status__c != rejected && wl.SVMX_SAP_Status__c != oldWDetailMap.get(wl.id).SVMX_SAP_Status__c)
                rejectedWDList.add(wl);
            system.debug(' ## rejList '+rejectedWDList);
            
            if(wl.RecordTypeId == usageRecordTypeId && wl.SVMXC__Line_Type__c == system.label.parts && wl.SVMXC__Product__c!= null && wl.SVMXC__Product__c != oldWDetailMap.get(wl.id).SVMXC__Product__c && wl.SVMX_ProductDetail__c == oldWDetailMap.get(wl.id).SVMX_ProductDetail__c)
                workDetailsLinesParts.add(wl);
        }
        
        if(!newWOLaborLinesMap.isEmpty()){
            SVMX_WorkDetailServiceManager.setWorkComplete(newWOLaborLinesMap,laborLinesid);
        }

        if(!wdIntMap.isEmpty()) {
            SVMX_SAPSalesOrderUtility.handleSAPSalesOrderItemUpdate(wdIntMap,oldWDetailMap);
        }

        //Update travel line qty
        if(!wdTravelQtyUpdate.isEmpty()){
            SVMX_WorkDetailServiceManager.wdTravelQtyUpdate(wdTravelQtyUpdate);
        }
        
       if(!workDetailsLinesParts.isEmpty())
            SVMX_WorkDetailServiceManager.populateProductDetail(workDetailsLinesParts);
       
        //Product stock update method call
        if(!rejectedWDList.isEmpty())
            SVMX_ProductStockServiceManager.updateProductStock(null, rejectedWDList);
       
        if(!partWDList.isEmpty()) {
            SVMX_ProductStockServiceManager.updateProductStock(partWDList,null); 
        }
        
        if(!newWLList.isEmpty()){ 
            map<id,SVMXC__Service_Order_Line__c> wrkDetailList = new map<id,SVMXC__Service_Order_Line__c>();
            map<id,SVMXC__Service_Order_Line__c> scMap = new map<id,SVMXC__Service_Order_Line__c>();
      
            scMap = SVMX_WorkDetailDataManager.workDetailQuery(newWLList);
        
            
            /*set<string> country = new set<string>();
            for(SVMX_Custom_Feature_Enabler__c cs: customSett){
                if(cs.Custom_Labor_Activity__c)
                    country.add(cs.name);
            }*/
        
            for(SVMXC__Service_Order_Line__c wl : scMap.values()){
                if(country.contains(wl.SVMXC__Service_Order__r.SVMXC__Country__c) && wl.SVMXC__Service_Order__r.SVMXC__Service_Contract__c != null && wl.SVMXC__Service_Order__r.SVMXC__Purpose_of_Visit__c == 'Technician Onsite'){
                    wrkDetailList.put(wl.id, wl);
                }
            }
            
            if(!wrkDetailList.isEmpty())
               UpWDLines=  SVMX_WorkDetailServiceManager.SetLaborActivityTypes(wrkDetailList, usageRecordTypeId, estimateRecordTypeId);
        }

           //create Parts Order
        if(SparePartsOrder.size() > 0 )
            SVMX_WorkDetailServiceManager.createPartsOrder(SparePartsOrder);

        //get price from SAP
        if(wlPriceLines.size() > 0){
            SVMX_WorkDetailServiceManager.getPricefromSAP(wlPriceLines);
        }
        system.debug('UpWDLines '+UpWDLines);
        if(UpWDLines.size() > 0)
            SVMX_WorkDetailDataManager.UpdaWorkDetails(UpWDLines.values());
            
              List <SVMXC__Service_Order_Line__c> timewolist = new  List <SVMXC__Service_Order_Line__c>();
             for(SVMXC__Service_Order_Line__c wd:newWDetailList){

          //if(wd.SVMXC__Group_Member__c!=null && wd.SVMXC__Start_Date_and_Time__c!=null && wd.SVMXC__End_Date_and_Time__c!=null &&(wd.SVMXC__Line_Type__c=='Labor'||wd.SVMXC__Line_Type__c=='Travel')) 
          if(wd.SVMXC__Group_Member__c!=null && (wd.SVMXC__Start_Date_and_Time__c != oldWDetailMap.get(wd.id).SVMXC__Start_Date_and_Time__c || wd.SVMXC__End_Date_and_Time__c != oldWDetailMap.get(wd.id).SVMXC__End_Date_and_Time__c) && (wd.SVMXC__Line_Type__c == 'Labor' || wd.SVMXC__Line_Type__c == 'Travel'))
                  timewolist.add(wd);

                
         }
            SVMX_PS_TS_TimesheetUtils timesheetUtils = new SVMX_PS_TS_TimesheetUtils();
            if(timewolist.size()>0)
            timesheetUtils.handleEventsFromWorkDetails(timewolist,oldWDetailMap,null,false,true,false);
             
          
    } 
    
    public override void beforeDelete(){
     List <SVMXC__Service_Order_Line__c> oldtimewolist = new  List <SVMXC__Service_Order_Line__c>();
      for(SVMXC__Service_Order_Line__c wd:oldWDetailList){
      if(wd.SVMX_Sales_Order_Item_Number__c != null)
                wd.addError('Cannot delete the line as Sales Order number is already generated');
          //if(wd.SVMXC__Group_Member__c!=null && wd.SVMXC__Start_Date_and_Time__c!=null && wd.SVMXC__End_Date_and_Time__c!=null &&(wd.SVMXC__Line_Type__c=='Labor'||wd.SVMXC__Line_Type__c=='Travel')) 
           else if(wd.SVMXC__Group_Member__c!=null && wd.SVMXC__Start_Date_and_Time__c!=null && wd.SVMXC__End_Date_and_Time__c!=null &&(wd.SVMXC__Line_Type__c == 'Labor' || wd.SVMXC__Line_Type__c == 'Travel')) 
                  oldtimewolist.add(wd);

            

                //timesheetUtils.updateTimeEventsFromWorkDetails(timewolist,oldWDetailMap);
         }    
             SVMX_PS_TS_TimesheetUtils timesheetUtils = new SVMX_PS_TS_TimesheetUtils();
              //if(oldtimewolist.size()>0)
              timesheetUtils.handleEventsFromWorkDetails(null,null,oldtimewolist,false,false,true);
}
}