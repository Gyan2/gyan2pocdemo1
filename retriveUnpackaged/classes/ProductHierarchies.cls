/**
 * Created by rebmangu on 02/05/2018.
 */

public with sharing class ProductHierarchies extends CoreSObjectDomain{
        public ProductHierarchies(List<ProductHierarchy__c> sObjectList){
            super(sObjectList);
        }

        public override void onApplyDefaults() {
            // Apply defaults to Accounts

        }

        public override void onValidate() {
            // Validate Products
        }

        public override void onValidate(Map<Id,SObject> existingRecords) {
            // Validate changes

        }

        public override void onBeforeInsert(){
            ProductHierarchyServices.handleExternalIdMapping((List<ProductHierarchy__c>)Records);
        }

        public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
            ProductHierarchyServices.handleExternalIdMapping((List<ProductHierarchy__c>)Records);
            // Related Products
        }

        public override void onBeforeDelete(){

        }

        public override void onAfterInsert(){

        }


        public override void onAfterUpdate(Map<Id,SObject> existingRecords){

        }

        public override void onAfterDelete(){

        }


        public override void onAfterUndelete(){

        }


        public class Constructor implements CoreSObjectDomain.IConstructable
        {
            public CoreSObjectDomain construct(List<SObject> sObjectList)
            {
                return new ProductHierarchies(sObjectList);
            }
        }

}