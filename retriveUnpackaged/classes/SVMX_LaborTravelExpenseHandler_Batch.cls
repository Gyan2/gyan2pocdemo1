/************************************************************************************************************
Description: Batch Apex Class to send Labor, Expense and Travel Work Detail Lines to SAP
 
Author: Naveed Sharif
Date: 12-03-2018

Modification Log: 
Date            Author          Modification Comments

--------------------------------------------------------------
 
*******************************************************************************************************/

global class SVMX_LaborTravelExpenseHandler_Batch implements Database.Batchable<sObject>,Database.AllowsCallouts,Database.Stateful {

    global Database.QueryLocator start (Database.BatchableContext BC) {

        String wdQuery;
        String reactive = System.Label.Reactive;
		String installation = System.Label.Installation;
		String ppm = System.Label.Planned_Services_PPM;
		String quotedWorks = System.Label.Quoted_Works;
        String closed = System.Label.Closed;
        String readyToInvoice = System.Label.Ready_To_Bill;
        Id usageRecordTypeId = SVMX_RecordTypeDataManager.getRecordTypeID('UsageConsumption','SVMXC__Service_Order_Line__c');
        
        wdQuery = 'SELECT Id, Name, SVMXC__Service_Order__r.SVMXC__Case__c, SVMX_Item_Number_Blank_Check__c, SVMX_Sales_Order_Item_Number__c, SVMXC__Start_Date_and_Time__c, SVMXC__End_Date_and_Time__c, SVMXC__Service_Order__c, SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Invoice_Status__c, SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c, SVMX_Chargeable_Qty__c, SVMXC__Billable_Quantity__c, SVMXC__Line_Type__c, SVMX_Awaiting_SAP_Response__c, SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Awaiting_SAP_Response__c, SVMXC__Service_Order__r.SVMXC__Order_Type__c';
        wdQuery += ' FROM SVMXC__Service_Order_Line__c';
        wdQuery += ' WHERE RecordTypeId =: usageRecordTypeId';
        wdQuery += ' AND (SVMXC__Line_Type__c = \'Labor\' OR SVMXC__Line_Type__c = \'Travel\' OR SVMXC__Line_Type__c = \'Expenses\')';
        wdQuery += ' AND (SVMXC__Service_Order__r.SVMXC__Order_Type__c =: reactive OR SVMXC__Service_Order__r.SVMXC__Order_Type__c =: installation OR SVMXC__Service_Order__r.SVMXC__Order_Type__c =: ppm OR SVMXC__Service_Order__r.SVMXC__Order_Type__c =: quotedWorks)'; 
        wdQuery += ' AND SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Invoice_Status__c = null';
        wdQuery += ' AND SVMXC__Service_Order__r.SVMXC__Order_Status__c !=: closed';
        wdQuery += ' AND SVMXC__Service_Order__r.SVMXC__Order_Status__c !=: readyToInvoice';
        wdQuery += ' AND SVMXC__Actual_Price2__c != null';
        wdQuery += ' AND SVMX_Chargeable_Qty__c != null';
        wdQuery += ' AND SVMX_Chargeable_Qty__c > 0';

        return Database.getQueryLocator(wdQuery);
    }

    global void execute(Database.BatchableContext BC, List<SVMXC__Service_Order_Line__c> workDetLines) {

        Set<Id> wdUpdateIds = new Set<Id>();
        Set<Id> wdCreateIds = new Set<Id>();

        System.debug('*** Labor/Travel/Expense Batch Job *** ' + workDetLines);

        for(SVMXC__Service_Order_Line__c wdl : workDetLines) {

            if(wdl.SVMXC__Line_Type__c == 'Labor' && wdl.SVMXC__End_Date_and_Time__c != null) {

                if(wdl.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c == null
                    && wdl.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Awaiting_SAP_Response__c == false) {
                    
                    wdCreateIds.add(wdl.Id); 
                }
                else if(wdl.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c != null
                    && wdl.SVMX_Sales_Order_Item_Number__c == null
                    && wdl.SVMX_Awaiting_SAP_Response__c == false) {

                    wdUpdateIds.add(wdl.Id);    
                }  
            }
            else if(wdl.SVMXC__Line_Type__c == 'Travel') {
                if((wdl.SVMXC__Start_Date_and_Time__c != null && wdl.SVMXC__End_Date_and_Time__c != null)
                    || (wdl.SVMXC__Start_Date_and_Time__c == null && wdl.SVMXC__End_Date_and_Time__c == null)) {

                    if(wdl.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c == null
                        && wdl.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Awaiting_SAP_Response__c == false) {
                        
                        wdCreateIds.add(wdl.Id);    
                    }
                    else if(wdl.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c != null
                        && wdl.SVMX_Sales_Order_Item_Number__c == null
                        && wdl.SVMX_Awaiting_SAP_Response__c == false) {

                        wdUpdateIds.add(wdl.Id);    
                    }
                }
            }
            else if(wdl.SVMXC__Line_Type__c == 'Expenses') {

                if(wdl.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c == null
                        && wdl.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Awaiting_SAP_Response__c == false) {
                    
                    wdCreateIds.add(wdl.Id);  
                }
                else if(wdl.SVMXC__Service_Order__r.SVMXC__Case__r.SVMX_Service_Sales_Order_Number__c != null
                        && wdl.SVMX_Sales_Order_Item_Number__c == null
                        && wdl.SVMX_Awaiting_SAP_Response__c == false) {

                    wdUpdateIds.add(wdl.Id);    
                }   
            }
        }

        if(!wdUpdateIds.isEmpty()) {
            
            if(System.isFuture() || System.isBatch()) {    
              
              SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderItemUpdate(wdUpdateIds, true);
            }
            else {

              SVMX_SAPSalesOrderUtility.asyncSalesOrderItemUpdateMethod(wdUpdateIds, true);              
            }
        }

        if(!wdCreateIds.isEmpty()) {

            if(System.isFuture() || System.isBatch()) {
              
              SVMX_SAPSalesOrderUtility.createSoapRequestForSAPSalesOrderCreation(wdCreateIds); 
            }
            else {

              SVMX_SAPSalesOrderUtility.asyncSalesOrderCreationMethod(wdCreateIds);
            }
        }       
    }

    global void finish(Database.BatchableContext BC) {

        System.debug('*** SVMX_InstallationWorkOrderHandler_Batch ***');
        System.debug('*** Installation Type Work Order - Batch Complete ***');
    }
}