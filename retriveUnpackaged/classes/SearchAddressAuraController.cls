/**
 * Created by trakers on 03/04/2018.
 */

public with sharing class SearchAddressAuraController {

    public static String APIKEY = 'AIzaSyD18U3HzkN5e1NRXWv232X0FKgnA26Up0U';

    @AuraEnabled
    public static String getSuggestionFromGoogle(String searchKey){

        String url = 'https://maps.googleapis.com/maps/api/place/autocomplete/json?input='+EncodingUtil.urlEncode(searchKey, 'UTF-8')+'&types=address&key='+APIKEY;

        HttpRequest req = new HttpRequest();
        Http http = new Http();

        req.setMethod('GET');

        req.setEndPoint(url);

        HTTPResponse resp = http.send(req);

        String jsonResult = resp.getBody().replace('\n', '');
        System.debug(jsonResult);

        return jsonResult;

    }

    @AuraEnabled
    public static String getFullAddressFromGoogle(String placeId){

        String url = 'https://maps.googleapis.com/maps/api/place/details/json?placeid='+placeId+'&key='+APIKEY;
        HttpRequest req = new HttpRequest();
        Http http = new Http();

        req.setMethod('GET');

        req.setEndPoint(url);

        HTTPResponse resp = http.send(req);

        String jsonResult = resp.getBody().replace('\n', '');
        System.debug(jsonResult);

        return jsonResult;
    }
}