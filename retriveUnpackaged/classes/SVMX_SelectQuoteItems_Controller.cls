/************************************************************************************************************
Description: Controller for SVMX_SelectQuoteItems VF page.
 
Author: Ranjitha Shenoy
Date: 11-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/
public class SVMX_SelectQuoteItems_Controller {
    
    public List<SVMXC__Quote_Line__c> quoteItemsList = new List<SVMXC__Quote_Line__c>();
    public Map<id,List<SVMXC__Quote_Line__c>> IPquoteItemsMap = new Map<id,List<SVMXC__Quote_Line__c>>();
    public ID quoteId;
    private Set<ID> ipIDs = new Set<ID>();
    public List<WrapIP> ipWrap{get;set;}
    public String accepted = Label.Accepted;
    public String rejected = Label.Rejected;
    public String TechnicianOnsite = Label.Technician_Onsite;
    public String PartialWon = Label.Partial_Won;
    public String PartiallyAccepted = Label.Partially_Accepted;
    public String new1 = Label.New;
    public String QuotedWorks = Label.Quoted_works;
    public String closedWon = Label.Closed_Won;
    public String closedLost = Label.Closed_Lost;
    public String Parts = Label.Parts;
    public String Travel = Label.Travel;
    public String Expenses = Label.Expenses;
    public string Labor = Label.Labor;
    public string Vendor_Goods = Label.Vendor_Goods;
    public string cusPONumber{get;set;}
    id quoteId1 {get;set;}
   
    //constructor
    public SVMX_SelectQuoteItems_Controller(){
        quoteId = ApexPages.currentPage().getParameters().get('quoteId');
        quoteItemsList = [select id, name, SVMX_Installed_Product__c,SVMXC__Quote__r.SVMX_Opportunity__c, SVMXC__Quote__r.SVMXC__Contact__c, SVMXC__Quote__r.SVMX_PS_Country__c, SVMXC__Product__c, SVMX_Product_Detail__c , SVMX_Installed_Product__r.name, SVMX_Installed_Product__r.SVMXC__Company__c, SVMX_Installed_Product__r.SVMXC__Company__r.CurrencyIsoCode, SVMX_Installed_Product__r.SVMXC__Product__c, SVMX_Installed_Product__r.SVMXC__Country__c, SVMX_Installed_Product__r.SVMXC__Site__c, SVMX_Installed_Product__r.SVMXC__Site__r.SVMX_Zone_Type__c ,SVMX_Installed_Product__r.SVMXC__State__c, SVMX_Installed_Product__r.SVMXC__Street__c, SVMX_Installed_Product__r.SVMXC__City__c, SVMX_Installed_Product__r.SVMXC__Zip__c, SVMX_Discount__c, SVMXC__Line_Type__c, SVMXC__Quantity2__c, SVMXC__Line_Price2__c, SVMXC__Select__c, SVMXC__Quote__c, SVMX_PS_Price_per_unit__c, SVMXC__Unit_Price2__c,  SVMX_Line_Status__c , SVMX_Additional_Discount__c , SVMX_Surcharge__c, SVMXC__Line_Description__c from SVMXC__Quote_Line__c where SVMXC__Quote__c  =: quoteId and SVMX_Line_Status__c != :accepted and SVMX_Line_Status__c != :rejected];     
        IPquoteItemsMap = new Map<id,List<SVMXC__Quote_Line__c>>();
        for(SVMXC__Quote_Line__c qi : quoteItemsList){

            quoteId1=qi.SVMXC__Quote__c;
            if(IPquoteItemsMap.containsKey(qi.SVMX_Installed_Product__c))
                IPquoteItemsMap.get(qi.SVMX_Installed_Product__c).add(qi);
            else
                IPquoteItemsMap.put(qi.SVMX_Installed_Product__c,new List<SVMXC__Quote_Line__c> {qi});
        }

        ipWrap = new List<WrapIP>();
        for(id i: IPquoteItemsMap.keySet()){
            WrapIP tempWrap = new WrapIP(i,IPquoteItemsMap.get(i));
            List<WrapQuoteItems> qiWrap = new List<WrapQuoteItems>(); 
            for(SVMXC__Quote_Line__c qi: IPquoteItemsMap.get(i)){
                WrapQuoteItems tmpqiWrap = new WrapQuoteItems(qi);
                qiWrap.add(tmpqiWrap);
            }
            tempWrap.qItems = qiWrap;
            ipWrap.add(tempWrap);        
        }

    }
    
    public class WrapIP{
        public id IPid{get;set;}
        public string ipName{get;set;}
        public boolean sel{get;set;}
        public Date preferedStartDate{get;set;}
        //public string cusPONumber{get;set;}
        public List<WrapQuoteItems> qitems{get;set;}
      
        public WrapIP(id i,List<SVMXC__Quote_Line__c> qouteItemsList){
            for(SVMXC__Quote_Line__c qi: qouteItemsList){
              ipName = qi.SVMX_Installed_Product__r.name;  
            }
            IPid = i;
            sel = false;
            preferedStartDate = system.today();
            //cusPONumber='';
        }     
    } 
    
    public class WrapQuoteItems{
        public id IP{get;set;}
        public string lineType{get;set;}
        public decimal qty{get;set;}
        public decimal price{get;set;}
        public decimal discount{get;set;}
      
        public WrapQuoteItems(SVMXC__Quote_Line__c qi){
            IP = qi.SVMX_Installed_Product__c;
            lineType = qi.SVMXC__Line_Type__c;
            qty =qi.SVMXC__Quantity2__c;
            price = qi.SVMXC__Line_Price2__c;
            discount=qi.SVMX_Discount__c;
        }     
    }
    
    public pageReference CreateCaseAndWO(){
        
        List<SVMXC__Quote_Line__c> accQuoteItemsList = new List<SVMXC__Quote_Line__c>();
        //List<Case> newCaseList = new List<Case>();
        Map<id,case> quoteidcaseMap = new Map<id,case>();
        SVMXC__Service_Order__c newWO = new SVMXC__Service_Order__c();
        Opportunity op = new Opportunity();
        SVMXC__Quote__c Quote = new SVMXC__Quote__c();
        set<Id> InstallProductId = new set<Id>();
        id caseGlobalStd = SVMX_RecordTypeDataManager.GetRecordTypeID('Global_Std','Case'); //Schema.SObjectType.Case.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        id WORecType = SVMX_RecordTypeDataManager.GetRecordTypeID('Global_Std','SVMXC__Service_Order__c'); //Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        id reqReciRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('RequestReceipt','SVMXC__Service_Order_Line__c');
        id plannedUsageRecTyId = SVMX_RecordTypeDataManager.GetRecordTypeID('Planned_Usage','SVMXC__Service_Order_Line__c');
        id productServedRecTyId = SVMX_RecordTypeDataManager.GetRecordTypeID('Products_Serviced','SVMXC__Service_Order_Line__c');
        Map<id,SVMXC__Installed_Product__c> installedproductMap = new Map<id,SVMXC__Installed_Product__c>();
        Map<Id,Map<Id,SVMXC__Installed_Product__c>> intstallproductlocationMap = new Map<Id,Map<Id,SVMXC__Installed_Product__c>>();
        Map<Id,SVMXC__Service_Order__c> intstallproductworkorderMap = new Map<Id,SVMXC__Service_Order__c>();
        SVMXC__Service_Order__c wo;
       
        integer acc=0;

        List<case> cs1=[select id,AccountId,CurrencyIsoCode,Subject,SVMX_Internal_Country__c,ContactId,SVMXC__Site__c,SVMXC__Preferred_Start_Time__c,SVMXC__Component__c,SVMX_Invoice_Amount__c,SVMX_Service_Quote__c from case where SVMX_Service_Quote__c =:quoteId1 Limit 1];
        for(WrapIP w: ipWrap){
             system.debug('w.sel'+w.sel);
            if(w.sel==true){                
                InstallProductId.add(w.IPid);
            }    
        }

        installedproductMap = new Map<id,SVMXC__Installed_Product__c>([select id,name,SVMXC__Site__c from SVMXC__Installed_Product__c where Id in:InstallProductId]);

        for(SVMXC__Installed_Product__c installprd:installedproductMap.values()){

            Map<Id,SVMXC__Installed_Product__c> installproductmap = new Map<Id,SVMXC__Installed_Product__c>();
            if(intstallproductlocationMap.containsKey(installprd.SVMXC__Site__c)){

                installproductmap = intstallproductlocationMap.get(installprd.SVMXC__Site__c);
            }
            installproductmap.put(installprd.Id,installprd);
            intstallproductlocationMap.put(installprd.SVMXC__Site__c,installproductmap);
        }
                    
        if(cs1.size() == 0){
            system.debug('cs1'+cs1);
            for(WrapIP w: ipWrap){
                 system.debug('w.sel'+w.sel);
                if(w.sel==true){
                    acc++;
                    //InstallProductId.add(w.IPid);

                                
                        case cs = new case();
                        cs.RecordTypeId = caseGlobalStd;
                        cs.Status = new1;
                        cs.SVMX_Customer_PO_Number__c = cusPONumber;
                        cs.SVMXC__Component__c = w.IPid;
                        cs.SVMXC__Perform_Auto_Entitlement__c = true;
                        cs.SVMXC__Preferred_Start_Time__c = w.preferedStartDate;
                        cs.Type = QuotedWorks;
                        cs.SVMX_Invoice_Amount__c = 0;
                        cs.Subject = QuotedWorks;
                        
                        for(SVMXC__Quote_Line__c qi: IPquoteItemsMap.get(w.IPid)){
                            if(qi.SVMXC__Line_Price2__c != null)
                                cs.SVMX_Invoice_Amount__c = cs.SVMX_Invoice_Amount__c + qi.SVMXC__Line_Price2__c;
                            qi.SVMX_Line_Status__c = accepted;
                            cs.SVMX_Service_Quote__c = qi.SVMXC__Quote__c;
                            accQuoteItemsList.add(qi);
                        }
                           
                        for(SVMXC__Quote_Line__c qi: IPquoteItemsMap.get(w.IPid)){
                            cs.AccountId = qi.SVMX_Installed_Product__r.SVMXC__Company__c;
                            cs.ContactId = qi.SVMXC__Quote__r.SVMXC__Contact__c;
                            cs.CurrencyIsoCode = qi.SVMX_Installed_Product__r.SVMXC__Company__r.CurrencyIsoCode;
                            cs.SVMXC__Product__c = qi.SVMX_Installed_Product__r.SVMXC__Product__c;
                            cs.SVMXC__Site__c = qi.SVMX_Installed_Product__r.SVMXC__Site__c;
                            cs.SVMX_Internal_Country__c = qi.SVMXC__Quote__r.SVMX_PS_Country__c;
                            Quote.id = qi.SVMXC__Quote__c;
                            op.id = qi.SVMXC__Quote__r.SVMX_Opportunity__c;
                            
                            if(!quoteidcaseMap.containsKey(qi.SVMXC__Quote__c)){

                                quoteidcaseMap.put(qi.SVMXC__Quote__c,cs);
                            }
                            break;
                        }
                        
                        //newCaseList.add(cs);
                }
                    
                
            }
            
        
            if(acc==0){
               apexpages.addmessage(new ApexPages.message(ApexPages.severity.Error,'Please select at least one Line Item to create Case and Work Order')); 
              return null;
            }
            
            if(!quoteidcaseMap.values().isEmpty()){

                List<Case> newCaseList = quoteidcaseMap.values();
                insert newCaseList;
                
                for(case cs: newCaseList){

                    for(id site:intstallproductlocationMap.keyset()){
                             wo = new SVMXC__Service_Order__c();
                            wo.RecordTypeId = WORecType;
                            wo.SVMXC__Company__c = cs.AccountId;
                            wo.SVMXC__Case__c = cs.id;
                            wo.CurrencyIsoCode = cs.CurrencyIsoCode;
                            wo.SVMXC__Country__c = cs.SVMX_Internal_Country__c;
                            wo.SVMXC__Component__c = cs.SVMXC__Component__c;
                            wo.SVMXC__Contact__c = cs.ContactId;
                            wo.SVMXC__Order_Type__c = QuotedWorks;
                            wo.SVMXC__Perform_Auto_Entitlement__c = true;
                            wo.SVMXC__Purpose_of_Visit__c = TechnicianOnsite;
                            wo.SVMXC__SM_Scheduling_Options__c = 'All options disabled';
                            wo.SVMXC__Site__c = cs.SVMXC__Site__c;
                            wo.SVMX_Amount_to_Invoice__c = cs.SVMX_Invoice_Amount__c;
                            wo.SVMX_PS_Subject__c  = cs.Subject;
                            wo.SVMXC__Preferred_Start_Time__c = cs.SVMXC__Preferred_Start_Time__c;
                            for(SVMXC__Quote_Line__c qi: quoteItemsList){
                                map<id,SVMXC__Installed_Product__c> installprductmap = new map<id,SVMXC__Installed_Product__c>();
                                 installprductmap = intstallproductlocationMap.get(site);
                                 system.debug('installprductmap&&'+installprductmap);
                                 system.debug('qi.SVMX_Installed_Product__c@@@@@'+qi.SVMX_Installed_Product__c);
                                 //system.debug('installprductmap.get(qi.SVMX_Installed_Product__c).id!!!!'+installprductmap.get(qi.SVMX_Installed_Product__c).id);
                                 //system.debug('installprductmap.get(qi.SVMX_Installed_Product__c)$$$$'+installprductmap.get(qi.SVMX_Installed_Product__c));
                               if(installprductmap.containskey(qi.SVMX_Installed_Product__c)){
                                if(qi.SVMX_Installed_Product__c == installprductmap.get(qi.SVMX_Installed_Product__c).id){
                                    wo.SVMXC__City__c = qi.SVMX_Installed_Product__r.SVMXC__City__c;
                                    wo.SVMXC__State__c = qi.SVMX_Installed_Product__r.SVMXC__State__c;
                                    wo.SVMXC__Street__c = qi.SVMX_Installed_Product__r.SVMXC__Street__c;
                                    wo.SVMXC__Zip__c = qi.SVMX_Installed_Product__r.SVMXC__Zip__c;
                                    wo.SVMX_Zone_Type__c = qi.SVMX_Installed_Product__r.SVMXC__Site__r.SVMX_Zone_Type__c;
                                    intstallproductworkorderMap.put(qi.SVMX_Installed_Product__c,wo);
                                    break;
                                }
                               }
                            }
                            
                    }        
                }
            }
        }else{
            for(id site:intstallproductlocationMap.keyset()){
                  wo = new SVMXC__Service_Order__c();
                        wo.RecordTypeId = WORecType;
                        wo.SVMXC__Company__c = cs1[0].AccountId;
                        wo.SVMXC__Case__c = cs1[0].id;
                        wo.CurrencyIsoCode = cs1[0].CurrencyIsoCode;
                        wo.SVMXC__Country__c = cs1[0].SVMX_Internal_Country__c;
                        wo.SVMXC__Component__c = cs1[0].SVMXC__Component__c;
                        wo.SVMXC__Contact__c = cs1[0].ContactId;
                        wo.SVMXC__Order_Type__c = QuotedWorks;
                        wo.SVMXC__Perform_Auto_Entitlement__c = true;
                        wo.SVMXC__Purpose_of_Visit__c = TechnicianOnsite;
                        wo.SVMXC__SM_Scheduling_Options__c = 'All options disabled';
                        wo.SVMXC__Site__c = cs1[0].SVMXC__Site__c;
                        wo.SVMX_Amount_to_Invoice__c = cs1[0].SVMX_Invoice_Amount__c;
                        wo.SVMX_PS_Subject__c  = cs1[0].Subject;
                        wo.SVMXC__Preferred_Start_Time__c = cs1[0].SVMXC__Preferred_Start_Time__c;
                        for(SVMXC__Quote_Line__c qi: quoteItemsList){
                            map<id,SVMXC__Installed_Product__c> installprductmap = intstallproductlocationMap.get(site);
                           if(installprductmap.containskey(qi.SVMX_Installed_Product__c)){
                            if(qi.SVMX_Installed_Product__c == installprductmap.get(qi.SVMX_Installed_Product__c).id){
                                wo.SVMXC__City__c = qi.SVMX_Installed_Product__r.SVMXC__City__c;
                                wo.SVMXC__State__c = qi.SVMX_Installed_Product__r.SVMXC__State__c;
                                wo.SVMXC__Street__c = qi.SVMX_Installed_Product__r.SVMXC__Street__c;
                                wo.SVMXC__Zip__c = qi.SVMX_Installed_Product__r.SVMXC__Zip__c;
                                wo.SVMX_Zone_Type__c = qi.SVMX_Installed_Product__r.SVMXC__Site__r.SVMX_Zone_Type__c;
                                intstallproductworkorderMap.put(qi.SVMX_Installed_Product__c,wo);
                                break;
                            }
                           } 
                        }
                        //intstallproductworkorderMap.put(intstallproductlocationMap.get(site),wo);
            }            

        }
        if(intstallproductworkorderMap.values().size()>0){

            insert  intstallproductworkorderMap.values();   
        }
        
        
        List<sObject> updateObjList = new List<sObject>();
        
        /*if(!newWOList.isEmpty()){
            insert newWOList;
        }*/
        
        set<string> country = new set<string>();
        for(SVMXC__Quote_Line__c qi : quoteItemsList){
            country.add(qi.SVMXC__Quote__r.SVMX_PS_Country__c);
        }
        
        List<SVMX_Sales_Office__c> salesOffList = [select id, SVMX_Country__c , SVMX_Expense_Product__c, SVMX_Expense_Product__r.SAPNumber__c, SVMX_Labor_Product__c, SVMX_Labor_Product__r.SAPNumber__c, SVMX_Quoted_Works_Dummy_Part__c, SVMX_Quoted_Works_Dummy_Part__r.SAPNumber__c, SVMX_Travel_Labor_Product__c, SVMX_Travel_Labor_Product__r.SAPNumber__c from SVMX_Sales_Office__c where SVMX_Country__c in: country ];
        Map<String,SVMX_Sales_Office__c> countrySOMap = new Map<String,SVMX_Sales_Office__c>();
        for(SVMX_Sales_Office__c so: salesOffList){
            countrySOMap.put(so.SVMX_Country__c, so);
        }
            
        List<SVMXC__Service_Order_Line__c> wdList = new List<SVMXC__Service_Order_Line__c>();
        map<Id,SVMXC__Service_Order_Line__c> productservedwdMap = new Map<Id,SVMXC__Service_Order_Line__c>();
        for(ID insproID:InstallProductId){
            for(SVMXC__Quote_Line__c qi: IPquoteItemsMap.get(insproID)){

                    SVMXC__Service_Order_Line__c wi1 = new SVMXC__Service_Order_Line__c();
                    if(intstallproductworkorderMap.containskey(qi.SVMX_Installed_Product__c)){
                        wi1.SVMXC__Service_Order__c = intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).id;
                        wi1.CurrencyIsoCode = intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).CurrencyIsoCode;
                        wi1.SVMX_Country__c = intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c;
                    }else{
                        wi1.SVMXC__Service_Order__c = wo.id;
                        wi1.CurrencyIsoCode = wo.CurrencyIsoCode;
                        wi1.SVMX_Country__c = wo.SVMXC__Country__c; 
                    }
                       
                    wi1.SVMXC__Serial_Number__c = qi.SVMX_Installed_Product__c;
                    wi1.SVMXC__Work_Description__c = qi.SVMXC__Line_Description__c;
                    wi1.RecordTypeId = productServedRecTyId;
                    
                    productservedwdMap.put(qi.SVMX_Installed_Product__c,wi1);
            }
                  

            for(SVMXC__Quote_Line__c qi: IPquoteItemsMap.get(insproID)){

                if(qi.SVMXC__Line_Type__c == Parts || qi.SVMXC__Line_Type__c == Vendor_Goods){
                    SVMXC__Service_Order_Line__c wi = new SVMXC__Service_Order_Line__c();
                    if(intstallproductworkorderMap.containskey(qi.SVMX_Installed_Product__c)){
                        wi.SVMXC__Service_Order__c = intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).id;
                        
                        wi.CurrencyIsoCode = intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).CurrencyIsoCode;
                        wi.SVMX_Country__c = intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c;
                    }else{
                        wi.SVMXC__Service_Order__c = wo.id;
                        wi.CurrencyIsoCode = wo.CurrencyIsoCode;
                        wi.SVMX_Country__c = wo.SVMXC__Country__c;
                    }          
                    wi.SVMXC__Work_Detail__c = productservedwdMap.get(qi.SVMX_Installed_Product__c).Id;    
                    wi.SVMXC__Work_Description__c = qi.SVMXC__Line_Description__c;
                    wi.SVMXC__Serial_Number__c = qi.SVMX_Installed_Product__c;                    
                    wi.RecordTypeId = reqReciRecordTypeId;
                    wi.SVMXC__Product__c = qi.SVMXC__Product__c;
                    wi.SVMX_ProductDetail__c = qi.SVMX_Product_Detail__c;
                    wi.SVMXC__Requested_Quantity2__c = qi.SVMXC__Quantity2__c;
                    wi.SVMXC__Line_Type__c = qi.SVMXC__Line_Type__c;
                    wi.SVMXC__Estimated_Price2__c = qi.SVMXC__Line_Price2__c;
                    wdList.add(wi);
                }
                //if(/*qi.SVMX_Installed_Product__c == wo.SVMXC__Component__c  && */){
                    SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
                    if(intstallproductworkorderMap.containskey(qi.SVMX_Installed_Product__c)){
                        wd.SVMXC__Service_Order__c = intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).id;                       
                        wd.CurrencyIsoCode = intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).CurrencyIsoCode;
                        wd.SVMX_Country__c = intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c;
                    }else{
                        wd.SVMXC__Service_Order__c = wo.id;
                        wd.CurrencyIsoCode = wo.CurrencyIsoCode;
                        wd.SVMX_Country__c = wo.SVMXC__Country__c;
                    }
    
                    wd.SVMXC__Work_Detail__c = productservedwdMap.get(qi.SVMX_Installed_Product__c).Id;
                    wd.SVMXC__Serial_Number__c = qi.SVMX_Installed_Product__c;
                    wd.SVMXC__Work_Description__c = qi.SVMXC__Line_Description__c;
                    wd.RecordTypeId = plannedUsageRecTyId;
                    wd.SVMXC__Line_Type__c = qi.SVMXC__Line_Type__c;
                    wd.SVMXC__Requested_Quantity2__c = qi.SVMXC__Quantity2__c;
                    wd.SVMXC__Estimated_Price2__c = qi.SVMXC__Unit_Price2__c;
                    wd.SVMXC__Discount__c = qi.SVMX_Discount__c;
                    wd.SVMXC__Actual_Price2__c = qi.SVMXC__Unit_Price2__c;
                    wd.SVMXC__Actual_Quantity2__c = qi.SVMXC__Quantity2__c;
                    wd.SVMXC__Estimated_Quantity2__c = qi.SVMXC__Quantity2__c;
                    wd.SVMX_Chargeable_Qty__c = qi.SVMXC__Quantity2__c;
                    wd.SVMX_Additional_Discount__c = qi.SVMX_Additional_Discount__c;
                    wd.SVMX_Surcharge__c = qi.SVMX_Surcharge__c;
                    if(intstallproductworkorderMap.containskey(qi.SVMX_Installed_Product__c)){

                        if((qi.SVMXC__Line_Type__c == Parts || qi.SVMXC__Line_Type__c == Vendor_Goods) && countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Quoted_Works_Dummy_Part__c != null){ 
                            wd.SVMXC__Product__c = countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Quoted_Works_Dummy_Part__c;
                            wd.SVMX_SAP_Material_Number__c = countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Quoted_Works_Dummy_Part__r.SAPNumber__c;
                            wd.SVMX_ProductDetail__c = qi.SVMX_Product_Detail__c;
                        }
                        if(qi.SVMXC__Line_Type__c == Labor && countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Labor_Product__c != null){
                            wd.SVMX_SAP_Product__c = countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Labor_Product__c;
                            wd.SVMX_SAP_Material_Number__c = countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Labor_Product__r.SAPNumber__c;
                        }
                        if(qi.SVMXC__Line_Type__c == System.Label.Expense && countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Expense_Product__c != null){
                            wd.SVMXC__Line_Type__c = Expenses;
                            wd.SVMX_SAP_Product__c = countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Expense_Product__c;
                            wd.SVMX_SAP_Material_Number__c = countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Expense_Product__r.SAPNumber__c;
                        }
                        if(qi.SVMXC__Line_Type__c == Travel && countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Travel_Labor_Product__c != null){
                            wd.SVMX_SAP_Product__c = countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Travel_Labor_Product__c;
                            wd.SVMX_SAP_Material_Number__c = countrySOMap.get(intstallproductworkorderMap.get(qi.SVMX_Installed_Product__c).SVMXC__Country__c).SVMX_Travel_Labor_Product__r.SAPNumber__c;
                        }
                        wdList.add(wd);
                    }else{
                        if((qi.SVMXC__Line_Type__c == Parts || qi.SVMXC__Line_Type__c == Vendor_Goods) && countrySOMap.get(wo.SVMXC__Country__c).SVMX_Quoted_Works_Dummy_Part__c != null){ 
                            wd.SVMXC__Product__c = countrySOMap.get(wo.SVMXC__Country__c).SVMX_Quoted_Works_Dummy_Part__c;
                            wd.SVMX_SAP_Material_Number__c = countrySOMap.get(wo.SVMXC__Country__c).SVMX_Quoted_Works_Dummy_Part__r.SAPNumber__c;
                            wd.SVMX_ProductDetail__c = qi.SVMX_Product_Detail__c;
                        }
                        if(qi.SVMXC__Line_Type__c == Labor && countrySOMap.get(wo.SVMXC__Country__c).SVMX_Labor_Product__c != null){
                            wd.SVMX_SAP_Product__c = countrySOMap.get(wo.SVMXC__Country__c).SVMX_Labor_Product__c;
                            wd.SVMX_SAP_Material_Number__c = countrySOMap.get(wo.SVMXC__Country__c).SVMX_Labor_Product__r.SAPNumber__c;
                        }
                        if(qi.SVMXC__Line_Type__c == System.Label.Expense && countrySOMap.get(wo.SVMXC__Country__c).SVMX_Expense_Product__c != null){
                            wd.SVMXC__Line_Type__c = Expenses;
                            wd.SVMX_SAP_Product__c = countrySOMap.get(wo.SVMXC__Country__c).SVMX_Expense_Product__c;
                            wd.SVMX_SAP_Material_Number__c = countrySOMap.get(wo.SVMXC__Country__c).SVMX_Expense_Product__r.SAPNumber__c;
                        }
                        if(qi.SVMXC__Line_Type__c == Travel && countrySOMap.get(wo.SVMXC__Country__c).SVMX_Travel_Labor_Product__c != null){
                            wd.SVMX_SAP_Product__c = countrySOMap.get(wo.SVMXC__Country__c).SVMX_Travel_Labor_Product__c;
                            wd.SVMX_SAP_Material_Number__c = countrySOMap.get(wo.SVMXC__Country__c).SVMX_Travel_Labor_Product__r.SAPNumber__c;
                        }
                        wdList.add(wd);

                    }
                    
                
                //}     
            }
        }    
        if(!wdList.isEmpty())
            insert wdList;
        
        if(acc == ipWrap.size()){
           Quote.SVMXC__Status__c = accepted;
           op.StageName = ClosedWon;
        }

        else if(acc<ipWrap.size() && acc!=0){
           Quote.SVMXC__Status__c = PartiallyAccepted;
           op.StageName = PartialWon;     
        }
        
        if(quote.id!=null){
            updateObjList.add(quote);  
        }
        
        if(op.id!=null){
            updateObjList.add(op);
        }
        
        if(!accQuoteItemsList.isEmpty()){
            updateObjList.addAll(accQuoteItemsList); 
        }
        
        if(!updateObjList.isEmpty()){
            update updateObjList;
        }

        if(productservedwdMap.keyset().size()>0){

                //List<SVMXC__Service_Order_Line__c> wdproductserved = productservedwdMap.values();
            insert productservedwdMap.values();
        }   
        
        pagereference p =  new pagereference('/'+quoteId); 
        return p;
    }
    
    public pageReference Cancel(){
        
        pagereference p =  new pagereference('/'+quoteId); 
        return p;
    }
    
    public pageReference RejectQuoteLines(){
        
        List<SVMXC__Quote_Line__c> rejQuoteItemsList = new List<SVMXC__Quote_Line__c>();
        SVMXC__Quote__c Quote = new SVMXC__Quote__c();
        Opportunity op = new Opportunity();
        List<sObject> updateObjList = new List<sObject>();
        integer acc =0;
        
        for(WrapIP w: ipWrap){
            system.debug('w.sel'+w.sel);
            if(w.sel==true){
                acc++;
                for(SVMXC__Quote_Line__c qi: IPquoteItemsMap.get(w.IPid)){
                    qi.SVMX_Line_Status__c = rejected;
                    rejQuoteItemsList.add(qi);
                    Quote.id = qi.SVMXC__Quote__c;
                    op.id = qi.SVMXC__Quote__r.SVMX_Opportunity__c;
                }
            }
        }
        if(acc==ipWrap.size()){
           Quote.SVMXC__Status__c = rejected;
           op.StageName = ClosedLost;
        }
        
        if(quote.id!=null){
            updateObjList.add(quote);  
        }
        
        if(op.id!=null){
            updateObjList.add(op);
        }
        
        if(!rejQuoteItemsList.isEmpty()){
            updateObjList.addAll(rejQuoteItemsList); 
        }
        
        if(!updateObjList.isEmpty()){
            update updateObjList;
        } 
                
        pagereference p =  new pagereference('/'+quoteId); 
        return p;
    }
    
    public pageReference RejectQuote(){
        
        SVMXC__Quote__c Quote = new SVMXC__Quote__c();
        Opportunity op = new Opportunity();
        List<sObject> updateObjList = new List<sObject>();
        List<SVMXC__Quote_Line__c> rejQuoteItemsList = new List<SVMXC__Quote_Line__c>();
        
        for(WrapIP w: ipWrap){
            for(SVMXC__Quote_Line__c qi: IPquoteItemsMap.get(w.IPid)){
                Quote.id = qi.SVMXC__Quote__c;
                op.id = qi.SVMXC__Quote__r.SVMX_Opportunity__c;
                break;
            }
            
            for(SVMXC__Quote_Line__c qi: IPquoteItemsMap.get(w.IPid)){
                    qi.SVMX_Line_Status__c = rejected;
                    rejQuoteItemsList.add(qi);
                }
        }
        Quote.SVMXC__Status__c = rejected;
        op.StageName = ClosedLost; 
        
        if(quote.id!=null){
            updateObjList.add(quote);  
        }
        
        if(op.id!=null){
            updateObjList.add(op);
        }
        
        if(!rejQuoteItemsList.isEmpty()){
            updateObjList.addAll(rejQuoteItemsList); 
        }
        
        if(!updateObjList.isEmpty()){
            update updateObjList;
        } 
        
        pagereference p =  new pagereference('/'+quoteId); 
        return p;
    }
}