@isTest
public class SVMX_PMScheduleTrggrHandler_UT {

 public static testMethod void checkTrigger() {
  Account account = SVMX_TestUtility.CreateAccount('Test Account', 'Norway', true);
  Contact contact = SVMX_TestUtility.CreateContact(account.id, true);
  SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(account.Id, 'Test Service Contract', 'Global Std', contact.Id, true);


  SVMXC__PM_Plan__c pmPlan = SVMX_TestUtility.CreatePMPlan(sc.Id, 'test Pm Plan', false);
  pmPlan.SVMXC__Coverage_Type__c = 'Location (Must Have Location)';
  insert pmPlan;


  SVMXC__PM_Schedule_Definition__c scheduleDef = new SVMXC__PM_Schedule_Definition__c();
  scheduleDef.SVMXC__PM_Plan__c = pmPlan.id;
  insert scheduleDef;


  SVMXC__PM_Schedule__c pmSchedule = new SVMXC__PM_Schedule__c();
  pmSchedule.SVMXC__Scheduled_On__c = Date.today();
  pmSchedule.SVMXC__PM_Plan__c = pmPlan.id;
  pmSchedule.SVMXC__PM_Schedule_Definition__c = scheduleDef.id;
  insert pmSchedule;


 }

}