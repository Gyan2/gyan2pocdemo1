global with sharing class SAPPO_CustomerService {

    public static Boolean sync(Account paramAccount) {

        SAPPO_CommonDataTypes.BusinessDocumentMessageHeader MessageHeader = new SAPPO_CommonDataTypes.BusinessDocumentMessageHeader();
        SAPPO_CustomerCreateRequest.CustomerMasterDataCreateRequestCustomer Customer = new SAPPO_CustomerCreateRequest.CustomerMasterDataCreateRequestCustomer();

        if(String.isBlank(paramAccount.SAPNumber__c)) { // this is only working for creation on SAP


            MessageHeader.ID = new SAPPO_CommonDataTypes.BusinessDocumentMessageID();
            MessageHeader.ID.Content = paramAccount.Id;

            SAPPO_CommonDataTypes.BusinessScope businessScope = new SAPPO_CommonDataTypes.BusinessScope();
            businessScope.TypeCode = 'SFG';
            businessScope.ID = new SAPPO_CommonDataTypes.BusinessScopeID();
            businessScope.ID.Content = 'CustomerMasterData';
            MessageHeader.BusinessScope = new List<SAPPO_CommonDataTypes.BusinessScope>{businessScope};

            MessageHeader.PositiveAckRequestedIndicator = 'true';
            MessageHeader.NegativeAckRequestedIndicator = 'false';


            Customer.BasicData = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCustomer();
            Customer.BasicData.AcquisitionUnit = new SAPPO_CommonDataTypes.PartyStandardID();
            Customer.BasicData.AcquisitionUnit.SchemeAgencyID = '009';
            if (Util.CurrentUserRecord.SalesOrg__c == '1001') {
                Customer.BasicData.AcquisitionUnit.Content = '9921226000878';// TODO remove hard-coding - Germany GLN
            } else {
                Customer.BasicData.AcquisitionUnit.Content = '9921226005491';// TODO remove hard-coding - Norway GLN
            }

            Customer.BasicData.MaintenanceProfileCode = 'BY';
            Customer.BasicData.Common = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommon();

            Customer.BasicData.Common.KeyWordsText = paramAccount.Name.left(20);
            Customer.BasicData.Common.Name = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommonName();
            Customer.BasicData.Common.Name.FirstLineName = paramAccount.Name;

            Customer.BasicData.Common.Name.SecondLineName = paramAccount.AccountName2__c;
            Customer.BasicData.Common.Name.ThirdLineName = paramAccount.AccountName3__c;
            Customer.BasicData.Common.Name.FourthLineName = paramAccount.AccountName4__c;

            Customer.BasicData.AddressInformation = new SAPPO_CustomerCreateRequest.CustomerCreateRequestAddressInformation();
            Customer.BasicData.AddressInformation.Address = new SAPPO_CustomerCreateRequest.CustomerCreateRequestAddrInfoAddress();
            Customer.BasicData.AddressInformation.Address.PhysicalAddress = new SAPPO_CustomerCreateRequest.CustomerCreateRequestAddrInfoAddrPhysicalAddress();

            /*List<CountryISO__mdt> IsoCodes = [SELECT DeveloperName FROM CountryISO__mdt WHERE MasterLabel = :paramAccount.BillingCountry];
            if (!IsoCodes.isEmpty()) {
                Customer.BasicData.AddressInformation.Address.PhysicalAddress.CountryCode = IsoCodes.get(0).DeveloperName;
            }*/

            Customer.BasicData.AddressInformation.Address.PhysicalAddress.CountryCode = paramAccount.BillingCountryCode;

            Customer.BasicData.AddressInformation.Address.PhysicalAddress.StreetPostalCode = paramAccount.BillingPostalCode;
            Customer.BasicData.AddressInformation.Address.PhysicalAddress.POBoxPostalCode = paramAccount.POBoxPostalCode__c;
            Customer.BasicData.AddressInformation.Address.PhysicalAddress.POBoxCityName = paramAccount.POBoxCity__c;
            Customer.BasicData.AddressInformation.Address.PhysicalAddress.CityName = paramAccount.BillingCity;
            Customer.BasicData.AddressInformation.Address.PhysicalAddress.POBoxID = paramAccount.POBox__c;
            Customer.BasicData.AddressInformation.Address.PhysicalAddress.StreetName = paramAccount.BillingStreet;

            Customer.BasicData.CommunicationData = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommunicationData();
            Customer.BasicData.CommunicationData.Address = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommunicationDataAddress();
            Customer.BasicData.CommunicationData.Address.Communication = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommDataAddrCommunication();
            Customer.BasicData.CommunicationData.Address.Communication.CorrespondenceLanguageCode = paramAccount.Language__c;
            Customer.BasicData.CommunicationData.Address.Communication.Telephone = new SAPPO_CommonDataTypes.Telephone();
            Customer.BasicData.CommunicationData.Address.Communication.Telephone.Number_x = new SAPPO_CommonDataTypes.PhoneNumber();
            Customer.BasicData.CommunicationData.Address.Communication.Telephone.Number_x.CountryCode = paramAccount.PhoneCountryCode__c;
            Customer.BasicData.CommunicationData.Address.Communication.Telephone.Number_x.SubscriberID = paramAccount.PhoneWithoutInternationalPrefix__c;

            Customer.BasicData.CommunicationData.Address.Communication.Facsimile = new SAPPO_CommonDataTypes.Facsimile();
            Customer.BasicData.CommunicationData.Address.Communication.Facsimile.Number_x = new SAPPO_CommonDataTypes.PhoneNumber();
            Customer.BasicData.CommunicationData.Address.Communication.Facsimile.Number_x.CountryCode = paramAccount.FaxCountryCode__c;
            Customer.BasicData.CommunicationData.Address.Communication.Facsimile.Number_x.SubscriberID = paramAccount.FaxWithoutInternationalPrefix__c;

            Customer.BasicData.CommunicationData.Address.Communication.EMail = new SAPPO_CommonDataTypes.Email();
            Customer.BasicData.CommunicationData.Address.Communication.EMail.URI = new SAPPO_CommonDataTypes.EmailURI();
            Customer.BasicData.CommunicationData.Address.Communication.Email.URI.EmailURI = paramAccount.CorporateEmail__c;

            Customer.BasicData.CommunicationData.Address.Communication.Web = new SAPPO_CommonDataTypes.Web();
            Customer.BasicData.CommunicationData.Address.Communication.Web.URI = paramAccount.Website;

            Customer.BasicData.SalesChannel = new SAPPO_CustomerCreateRequest.CustomerCreateRequestSalesChannel();
            Customer.BasicData.SalesChannel.SalesChannelCode = paramAccount.SalesChannel__c;
            Customer.BasicData.SalesChannel.CustomerBusinessCategoryCode = paramAccount.BusinessCategory__c;
            Customer.BasicData.SalesChannel.CustomerBusinessTypeCode = paramAccount.BusinessType__c;

            Customer.BasicData.TaxNumber = new SAPPO_CustomerCreateRequest.CustomerCreateRequestTaxNumber();
            Customer.BasicData.TaxNumber.TaxNumber1 = new SAPPO_CommonDataTypes.PartyTaxID();
            Customer.BasicData.TaxNumber.TaxNumber1.Content = paramAccount.TaxNumber1__c;
            Customer.BasicData.TaxNumber.TaxNumber2 = new SAPPO_CommonDataTypes.PartyTaxID();
            Customer.BasicData.TaxNumber.TaxNumber2.Content = paramAccount.TaxNumber2__c;


            SAPPO_CustomerCreateRequest.CustomerCreateRequestVATIdentificationNumber tempTaxNumber = new SAPPO_CustomerCreateRequest.CustomerCreateRequestVATIdentificationNumber();
            tempTaxNumber.partyTaxID = new SAPPO_CommonDataTypes.PartyTaxID();
            tempTaxNumber.partyTaxID.Content = paramAccount.VatNumber__c;
            Customer.BasicData.VATIdentificationNumber = new List<SAPPO_CustomerCreateRequest.CustomerCreateRequestVATIdentificationNumber>{
                    tempTaxNumber
            };

            Customer.BasicData.Relationship = new List<SAPPO_CustomerCreateRequest.CustomerCreateRequestRelationship>();

            for (Contact forContact : paramAccount.Contacts) {
                SAPPO_CustomerCreateRequest.CustomerCreateRequestRelationship forRelationship = new SAPPO_CustomerCreateRequest.CustomerCreateRequestRelationship();

                forRelationship.ContactPerson = new SAPPO_CustomerCreateRequest.CustomerCreateRequestRelshpContactPerson();
                forRelationship.ContactPerson.ContactPersonNote = new SAPPO_CommonDataTypes.SHORT_Note();
                forRelationship.ContactPerson.ContactPersonNote.Content = forContact.Id;
                forRelationship.ContactPerson.ContactPersonNote.LanguageCode = forContact.Language__c; // TODO Check

                forRelationship.ContactPerson.Address = new SAPPO_CustomerCreateRequest.CustomerCreateRequestRelshpContactPersonAddress();
                forRelationship.ContactPerson.Address.PersonName = new SAPPO_CustomerCreateRequest.CustomerCreateRequestPersonName();
                forRelationship.ContactPerson.Address.PersonName.GivenName = forContact.FirstName;
                forRelationship.ContactPerson.Address.PersonName.FamilyName = forContact.LastName;
                forRelationship.ContactPerson.Address.PersonName.AcademicTitleCode = forContact.AcademicTitle__c; // TODO Check Salutation

                forRelationship.ContactPerson.Address.Communication = new SAPPO_CustomerCreateRequest.CustomerCreateRequestRelshpContactPersonCommunication();
                forRelationship.ContactPerson.Address.Communication.Telephone = new SAPPO_CommonDataTypes.Telephone();
                forRelationship.ContactPerson.Address.Communication.Telephone.Number_x = new SAPPO_CommonDataTypes.PhoneNumber();
                forRelationship.ContactPerson.Address.Communication.Telephone.Number_x.CountryCode = forContact.PhoneCountryCode__c;
                forRelationship.ContactPerson.Address.Communication.Telephone.Number_x.SubscriberID = forContact.PhoneWithoutInternationalPrefix__c;

                forRelationship.ContactPerson.Address.Communication.MobilePhone = new SAPPO_CommonDataTypes.Telephone();
                forRelationship.ContactPerson.Address.Communication.MobilePhone.Number_x = new SAPPO_CommonDataTypes.PhoneNumber();
                forRelationship.ContactPerson.Address.Communication.MobilePhone.Number_x.CountryCode = forContact.MobilePhoneCountryCode__c;
                forRelationship.ContactPerson.Address.Communication.MobilePhone.Number_x.SubscriberID = forContact.MobilePhoneWithoutInternationalPrefix__c;

                forRelationship.ContactPerson.Address.Communication.EMail = new SAPPO_CommonDataTypes.Email();
                forRelationship.ContactPerson.Address.Communication.EMail.URI = new SAPPO_CommonDataTypes.EmailURI();
                forRelationship.ContactPerson.Address.Communication.Email.URI.EmailURI = forContact.Email;

                Customer.BasicData.Relationship.add(forRelationship);
            }
        }

        return new SAPPO_CustomerCreateRequest.CustomerMasterDataOutPort().CreateSync(MessageHeader, Customer);
    }


    public static Boolean sync(Location__c paramLocation) {

        SAPPO_CommonDataTypes.BusinessDocumentMessageHeader MessageHeader = new SAPPO_CommonDataTypes.BusinessDocumentMessageHeader();
        SAPPO_CustomerCreateRequest.CustomerMasterDataCreateRequestCustomer Customer = new SAPPO_CustomerCreateRequest.CustomerMasterDataCreateRequestCustomer();

        //if(String.isBlank(paramAccount.SAPNumber__c)) { // this is only working for creation on SAP

        MessageHeader.ID = new SAPPO_CommonDataTypes.BusinessDocumentMessageID();
        MessageHeader.ID.Content = paramLocation.Id;

        SAPPO_CommonDataTypes.BusinessScope businessScope = new SAPPO_CommonDataTypes.BusinessScope();
        businessScope.TypeCode = 'SFG';
        businessScope.ID = new SAPPO_CommonDataTypes.BusinessScopeID();
        businessScope.ID.Content = 'Location';
        MessageHeader.BusinessScope = new List<SAPPO_CommonDataTypes.BusinessScope>{businessScope};

        MessageHeader.PositiveAckRequestedIndicator = 'true';
        MessageHeader.NegativeAckRequestedIndicator = 'false';

        Customer.BasicData = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCustomer();

        if (String.isNotBlank(paramLocation.SAPInstance__c) && String.isNotBlank(paramLocation.SAPNumber__c)) {
            Customer.BasicData.ID = new SAPPO_CommonDataTypes.CustomerID();
            Customer.BasicData.ID.SchemeAgencyID = 'urn:dorma:esm:' + paramLocation.SAPInstance__c;
            Customer.BasicData.ID.Content = paramLocation.SAPNumber__c;
        }

        Customer.BasicData.AcquisitionUnit = new SAPPO_CommonDataTypes.PartyStandardID();
        Customer.BasicData.AcquisitionUnit.SchemeAgencyID = '009';
        if (Util.CurrentUserRecord.SalesOrg__c == '1001') {
            Customer.BasicData.AcquisitionUnit.Content = '9921226000878';// TODO remove hard-coding - Germany GLN
        } else {
            Customer.BasicData.AcquisitionUnit.Content = '9921226005491';// TODO remove hard-coding - Norway GLN
        }
        Customer.BasicData.MaintenanceProfileCode = 'IT';

        Customer.BasicData.Common = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommon();

        Customer.BasicData.Common.KeyWordsText = paramLocation.Name.left(20);
        Customer.BasicData.Common.Name = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommonName();
        Customer.BasicData.Common.Name.FirstLineName = paramLocation.Name;

        Customer.BasicData.Common.Name.SecondLineName = paramLocation.SVMX_LocationName2__c;
        Customer.BasicData.Common.Name.ThirdLineName = paramLocation.SVMX_LocationName3__c;
        Customer.BasicData.Common.Name.FourthLineName = paramLocation.SVMX_LocationName4__c;

        Customer.BasicData.AddressInformation = new SAPPO_CustomerCreateRequest.CustomerCreateRequestAddressInformation();
        Customer.BasicData.AddressInformation.Address = new SAPPO_CustomerCreateRequest.CustomerCreateRequestAddrInfoAddress();
        Customer.BasicData.AddressInformation.Address.PhysicalAddress = new SAPPO_CustomerCreateRequest.CustomerCreateRequestAddrInfoAddrPhysicalAddress();

        Customer.BasicData.AddressInformation.Address.PhysicalAddress.CountryCode = paramLocation.SVMXC_Country__c;//'NO';//paramLocation.BillingCountry;

        Customer.BasicData.AddressInformation.Address.PhysicalAddress.HouseID = paramLocation.SVMX_HouseNumber__c;
        Customer.BasicData.AddressInformation.Address.PhysicalAddress.StreetPostalCode = paramLocation.SVMXC_Zip__c;
        Customer.BasicData.AddressInformation.Address.PhysicalAddress.POBoxPostalCode = paramLocation.SVMX_POBoxPostalCode__c;
        Customer.BasicData.AddressInformation.Address.PhysicalAddress.POBoxCityName = paramLocation.SVMX_POBoxCity__c;
        Customer.BasicData.AddressInformation.Address.PhysicalAddress.CityName = paramLocation.SVMXC_City__c;
        Customer.BasicData.AddressInformation.Address.PhysicalAddress.POBoxID = paramLocation.SVMX_POBox__c;
        Customer.BasicData.AddressInformation.Address.PhysicalAddress.StreetName = paramLocation.SVMXC_Street__c;

        Customer.BasicData.CommunicationData = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommunicationData();
        Customer.BasicData.CommunicationData.Address = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommunicationDataAddress();
        Customer.BasicData.CommunicationData.Address.Communication = new SAPPO_CustomerCreateRequest.CustomerCreateRequestCommDataAddrCommunication();
        Customer.BasicData.CommunicationData.Address.Communication.CorrespondenceLanguageCode = paramLocation.SVMXC_SiteRef__r.SVMX_Language__c;//'EN'; // Location do not have a Language, but its required on SAP
        /*Customer.BasicData.CommunicationData.Address.Communication.Telephone = new SAPPO_CommonDataTypes.Telephone();
        Customer.BasicData.CommunicationData.Address.Communication.Telephone.Number_x = new SAPPO_CommonDataTypes.PhoneNumber();
        Customer.BasicData.CommunicationData.Address.Communication.Telephone.Number_x.SubscriberID = paramAccount.Phone;
*/ /*
        Customer.BasicData.CommunicationData.Address.Communication.Facsimile = new SAPPO_CommonDataTypes.Facsimile();
        Customer.BasicData.CommunicationData.Address.Communication.Facsimile.Number_x = new SAPPO_CommonDataTypes.PhoneNumber();
        Customer.BasicData.CommunicationData.Address.Communication.Facsimile.Number_x.SubscriberID = paramAccount.Fax;
*/
        Customer.BasicData.CommunicationData.Address.Communication.EMail = new SAPPO_CommonDataTypes.Email();
        Customer.BasicData.CommunicationData.Address.Communication.EMail.URI = new SAPPO_CommonDataTypes.EmailURI();
        Customer.BasicData.CommunicationData.Address.Communication.Email.URI.EmailURI = paramLocation.SVMXC_Email__c;
/*
            Customer.BasicData.CommunicationData.Address.Communication.Web = new SAPPO_CommonDataTypes.Web();
            Customer.BasicData.CommunicationData.Address.Communication.Web.URI = paramAccount.Website;
*/

        Customer.BasicData.SalesChannel = new SAPPO_CustomerCreateRequest.CustomerCreateRequestSalesChannel();
        Customer.BasicData.SalesChannel.SalesChannelCode = paramLocation.SVMX_SalesChannel__c;

        Customer.BasicData.TaxNumber = new SAPPO_CustomerCreateRequest.CustomerCreateRequestTaxNumber();
        Customer.BasicData.TaxNumber.TaxNumber1 = new SAPPO_CommonDataTypes.PartyTaxID();
        Customer.BasicData.TaxNumber.TaxNumber1.Content = paramLocation.SVMX_TaxRegistrationNumber1__c;
        Customer.BasicData.TaxNumber.TaxNumber2 = new SAPPO_CommonDataTypes.PartyTaxID();
        Customer.BasicData.TaxNumber.TaxNumber2.Content = paramLocation.SVMX_TaxRegistrationNumber2__c;


        SAPPO_CustomerCreateRequest.CustomerCreateRequestVATIdentificationNumber tempTaxNumber = new SAPPO_CustomerCreateRequest.CustomerCreateRequestVATIdentificationNumber();
        tempTaxNumber.partyTaxID = new SAPPO_CommonDataTypes.PartyTaxID();
        tempTaxNumber.partyTaxID.Content = paramLocation.SVMX_VATRegistrationNumber__c;
        Customer.BasicData.VATIdentificationNumber = new List<SAPPO_CustomerCreateRequest.CustomerCreateRequestVATIdentificationNumber>{
                tempTaxNumber
        };

        return new SAPPO_CustomerCreateRequest.CustomerMasterDataOutPort().CreateSync(MessageHeader, Customer);
    }

    private static Account validate(Account paramAccount){
        if (String.isNotBlank(paramAccount.SAPNumber__c)) {
            throw new UPDATE_NOT_SUPPORTED_EXCEPTION('Attempt to update Account with Id ' + paramAccount.Id + ' and SAPNumber__c ' + paramAccount.SAPNumber__c);
        }

        List<String> missingFields = new List<String>();

        if (String.isBlank(paramAccount.Language__c))           missingFields.add('Language__c');
        if (String.isBlank(paramAccount.BillingStreet))         missingFields.add('BillingStreet');
        if (String.isBlank(paramAccount.BillingCity))           missingFields.add('BillingCity');
        if (String.isBlank(paramAccount.BillingPostalCode))     missingFields.add('BillingPostalCode');
        if (String.isBlank(paramAccount.BillingCountryCode))    missingFields.add('BillingCountryCode');

        if (missingFields.isEmpty()) {
            return paramAccount;
        } else {
            throw new REQUIRED_FIELDS_MISSING_EXCEPTION(String.join(missingFields,','));
        }
    }

    public static Boolean syncAccount(Id paramAccountId) {
        List<Account> accounts = [
                SELECT
                        Name,
                        AccountName2__c,
                        AccountName3__c,
                        AccountName4__c,
                        SAPNumber__c,
                        BillingPostalCode,
                        BillingCountryCode,
                        POBoxPostalCode__c,
                        POBoxCity__c,
                        POBox__c,
                        BillingCity,
                        BillingStreet,
                        Language__c,
                        PhoneCountryCode__c,
                        PhoneWithoutInternationalPrefix__c,
                        FaxCountryCode__c,
                        FaxWithoutInternationalPrefix__c,
                        CorporateEmail__c,
                        Website,
                        SalesChannel__c,
                        BusinessCategory__c,
                        BusinessType__c,
                        TaxNumber1__c,
                        TaxNumber2__c,
                        VatNumber__c,
                (
                        SELECT
                                Name ,
                                Language__c,
                                //Function__c,
                                AcademicTitle__c,
                                FirstName,
                                LastName,
                                PhoneCountryCode__c,
                                PhoneWithoutInternationalPrefix__c,
                                MobilePhoneCountryCode__c,
                                MobilePhoneWithoutInternationalPrefix__c,
                                Email

                        FROM Contacts
                )
                FROM Account
                WHERE Id = :paramAccountId];
        if (accounts.isEmpty()) {
            return null;
        } else {
            return sync(validate(accounts.get(0)));
        }
    }

    public static Boolean syncLocation(Id paramLocationId) {
        List<Location__c> locations = [
                SELECT
                        Name,
                        SVMX_LocationName2__c,
                        SVMX_LocationName3__c,
                        SVMX_LocationName4__c,
                        SVMX_HouseNumber__c,
                        SVMXC_Zip__c,
                        SVMX_POBoxPostalCode__c,
                        SVMX_POBoxCity__c,
                        SVMXC_City__c,
                        SVMX_POBox__c,
                        SVMXC_Street__c,
                        SVMXC_Email__c,
                        SVMX_SalesChannel__c,
                        SVMX_TaxRegistrationNumber1__c,
                        SVMX_TaxRegistrationNumber2__c,
                        SVMX_VATRegistrationNumber__c,
                        SAPInstance__c,
                        SAPNumber__c,
                        SVMXC_SiteRef__r.SVMX_Language__c,
                        SVMXC_Country__c
                FROM
                        Location__c
                WHERE
                Id = :paramLocationId  OR
                SVMXC_SiteRef__c = :paramLocationId
        ];

        if (locations.isEmpty()) {
            return null;
        } else {
            return sync(locations.get(0));
        }
    }

    webservice static SVMXC.INTF_WebServicesDef.INTF_Response SVMX_sync(SVMXC.INTF_WebServicesDef.INTF_Request request) {
        SVMXC.INTF_WebServicesDef.INTF_Response returned = new SVMXC.INTF_WebServicesDef.INTF_Response();

        try {
            Id recordId;
            for (SVMXC.INTF_WebServicesDef.SVMXMap objSVXMMap : request.valueMap) {
                if (objSVXMMap.key == 'Id') {
                    recordId = objSVXMMap.value;
                }
            }
            if (String.isBlank(recordId)) {
                returned.success = false;
                returned.message = 'Config Problem. Id is missing';
            } else {
                String sObjectType = recordId.getSObjectType().getDescribe().getName();

                if (sObjectType == 'Account') {
                    returned.success = syncAccount(recordId);
                } else if (sObjectType == 'SVMXC__Site__c' || sObjectType == 'Location__c') {
                    returned.success = syncLocation(recordId);
                } else {
                    returned.success = false;
                    returned.message = 'Invalid sObjectType for Id. (' + recordId + ' => ' + sObjectType + ')';
                }

            }

            if (returned.success) returned.message = 'Sent';
            returned.messageType = 'INFO';
        } catch(VALIDATION_EXCEPTION e) {
            returned.success = false;
            returned.message = e.getMessage();
        } catch(Exception e) {
            returned.success = false;
            returned.message = 'Unexpected Error: ' +e.getMessage();
        }

        return returned;
    }

    @future(callout=true)
    public static void futureSyncAccount(Id paramAccountId) {
        syncAccount(paramAccountId);
    }

    @future(callout=true)
    public static void futureSyncLocation(Id paramLocationId) {
        syncLocation(paramLocationId);
    }

    public abstract class VALIDATION_EXCEPTION extends Exception {}
    public class REQUIRED_FIELDS_MISSING_EXCEPTION extends SAPPO_CustomerService.VALIDATION_EXCEPTION {}
    public class UPDATE_NOT_SUPPORTED_EXCEPTION extends SAPPO_CustomerService.VALIDATION_EXCEPTION {}
}