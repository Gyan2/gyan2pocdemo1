public without sharing class LTNG_HomeNotifications {
    @AuraEnabled public static NotificationsWrapper auraGetNotifications() {
        // TODO optimise query to fetch only default + user's
        return new NotificationsWrapper([
            SELECT
                Name,
                DefaultContent__c,
                Icon__c,
                en_USTitle__c,
                en_USContent__c,
                deTitle__c,
                deContent__c,
                frTitle__c,
                frContent__c
            FROM Notification__c
            WHERE Id IN (
                SELECT NotificationRef__c
                FROM UserNotification__c
                WHERE UserRef__c = :UserInfo.getUserId()
                    AND ReadAt__c = NULL
            )
                AND StartAt__c <= :System.now()
        ]);
    }

    @AuraEnabled public static void auraMarkAsRead(Id notificationId) {
        List<UserNotification__c> toUpdate = [SELECT Id FROM UserNotification__c WHERE NotificationRef__c = :notificationId AND UserRef__c = :UserInfo.getUserId()];

        for (UserNotification__c forUserNotification : toUpdate) {
            forUserNotification.ReadAt__c = System.now();
        }

        update toUpdate;
    }

    @AuraEnabled public static List<User> auraSearchUsers(String searchString, Map<String,Boolean> searchFields) {
        List<User> returned = new List<User>();
        List<String> userFilters = new List<String>();

        String operator;
        if (searchString.startsWith('"') && searchString.endsWith('"')) {
            operator = '=';
            searchString = searchString.replace('"','');
        } else {
            operator = 'LIKE';
            searchString += '%';
        }

        if (searchFields.get('Name')) {
            userFilters.add('Name ' + operator + ':searchString');
        }
        if (searchFields.get('Email')) {
            userFilters.add('Email ' + operator +  ':searchString');
        }
        if (searchFields.get('Alias')) {
            userFilters.add('Alias ' + operator + ':searchString');
        }
        if (searchFields.get('Username')) {
            userFilters.add('Username ' + operator + ':searchString');
        }
        if (searchFields.get('Profile')) {
            userFilters.add('Profile.Name ' + operator + ':searchString');
        }
        if (searchFields.get('UserRole')) {
            userFilters.add('UserRole.Name ' + operator + ':searchString');
        }

        /* They will be filtered by code *
        if (searchFields.get('PermissionSet')) {
            userFilters.add('Id IN (SELECT AssigneeId FROM PermissionSetAssignment WHERE PermissionSet.Name ' + operator + ':searchString )');
        }
        */

        String query = 'SELECT Name, Email, Alias, Username, Profile.Name, UserRole.Name, ';
            query += '(SELECT PermissionSet.Name FROM PermissionSetAssignments WHERE PermissionSet.IsCustom = true' + (searchFields.get('PermissionSet')?' AND PermissionSet.Name ' + operator + ':searchString':'') + ')';
            query += ' FROM User ';
            query += 'WHERE ' + (userFilters.isEmpty()?'IsActive = true AND Profile.UserLicense.MasterLabel LIKE \'Salesforce%\'' :String.join(new List<String>{ 'IsActive = true  AND Profile.UserLicense.MasterLabel LIKE \'Salesforce%\'','(' + String.join(userFilters, ' OR ') + ')' },' AND '));

        System.debug(query);
        for(User forUser : database.query(query)) {
            if (!searchFields.get('PermissionSet') || !forUser.PermissionSetAssignments.isEmpty()) {
                returned.add(forUser);
            }
        }

        return returned;
    }

    public class NotificationsWrapper {
        @AuraEnabled public List<Notification__c> notifications;
        @AuraEnabled public String language = [SELECT LanguageLocaleKey FROM User WHERE Id = :UserInfo.getUserId()].LanguageLocaleKey;
        @AuraEnabled public Map<String,Map<String,String>> labels = new Map<String,Map<String,String>>{
            'Notification__c' => new Map<String,String>{
                'LabelPlural' => Notification__c.getSObjectType().getDescribe().getLabelPlural()
            }
        };

        public NotificationsWrapper(List<Notification__c> paramNotifications) {
            notifications = paramNotifications;
        }

    }

    @AuraEnabled public static AddUsersInitialDataWrapper auraGetInitialAddUsersInformation(Id notificationId) {
        AddUsersInitialDataWrapper returned = new AddUsersInitialDataWrapper();
        returned.userNotifications = [SELECT UserRef__c FROM UserNotification__c WHERE NotificationRef__c = :notificationId];
        return returned;
    }


    public class AddUsersInitialDataWrapper {
        @AuraEnabled public List<UserNotification__c> userNotifications;
        @AuraEnabled public Map<String,Map<String,String>> labels = new Map<String,Map<String,String>>{
            'User' => new Map<String,String> {
                'Label' => User.getSObjectType().getDescribe().getLabel(),
                'Name' => Util.Describe.getField('User','Name').getLabel(),
                'Email' => Util.Describe.getField('User','Email').getLabel(),
                'Alias' => Util.Describe.getField('User','Alias').getLabel(),
                'Username' => Util.Describe.getField('User','Username').getLabel()
            },
            'Profile' => new Map<String,String> {
                'Label' => Profile.getSObjectType().getDescribe().getLabel()
            },
            'UserRole' => new Map<String,String> {
                'Label' => UserRole.getSObjectType().getDescribe().getLabel()
            },
            'PermissionSet' => new Map<String,String> {
                'LabelPlural' => PermissionSet.getSObjectType().getDescribe().getLabelPlural()
            },
            'PublicGroup' => new Map<String,String> {
                'Label' => Group.getSObjectType().getDescribe().getLabel()
            }
        };
    }

    @AuraEnabled
    public static void auraAddUsers(Id notificationId, List<User> users) {
        List<UserNotification__c> userNotifications = new List<UserNotification__c>();
        for (User forUser : users) {
            userNotifications.add(new UserNotification__c(UserRef__c = forUser.Id, NotificationRef__c = notificationId));
        }

        insert userNotifications;
    }
}