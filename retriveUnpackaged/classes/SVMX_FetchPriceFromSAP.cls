/**==================================================================================================================================
 * DormaKaba
 * Name: SVMX_FetchPriceFromSAP
 * Description: Web service Class called from 'Get Parts Price'
 * Created Date: Mar 1, 2018
 * Created By: Tulsi B R
 *
 
 ===================================================================================================================================*/
global class SVMX_FetchPriceFromSAP{
    
    
    
    public static boolean blankSFMFlag = false;
    public static boolean testClassVar = false;
    
    webservice static SVMXC.SFM_WrapperDef.SFM_PageData getPartsPrice(SVMXC.SFM_WrapperDef.SFM_TargetRecord request){
        // Describe all objects
        system.debug('Request = ' + request);
        
        SVMXC.SFM_WrapperDef.SFM_PageData pagedata = new SVMXC.SFM_WrapperDef.SFM_PageData();
        map<String,Schema.SObjectType> Allobj = new map<String, Schema.SObjectType>();
        map<String, Schema.SObjectType> gd = new Map<String, Schema.SObjectType>();
        SVMXC.SFM_ServicesDef def = new SVMXC.SFM_ServicesDef();
        map<String,List<Sobject>> detailSobjectMap = new map<String, List<Sobject>>();
        map<String,List<Sobject>> newDetailSobjectMap = new map<String, List<Sobject>>();
        
        gd = Schema.getGlobalDescribe();
        if(gd.size() > 0){
            for(Schema.SObjectType s : gd.values()){
                Schema.DescribeSObjectResult result = s.getDescribe();
                Allobj.put(result.getName(), s);
            }
        }
       

        // The method below returns the Header Sobject record
        Sobject headerSobj = def.SFM_Page_GetHeaderRecord(request, Allobj);
        SVMXC__Service_Order__c objOrder = new SVMXC__Service_Order__c();
        objOrder = (SVMXC__Service_Order__c)headerSobj;
        //accountId = objOrder.SVMXC__Company__c;
        //System.debug('Work Order Account #### '+objOrder.SVMXC__Company__c);
        //Fetching name of Account and Installed Asset External Id  // Added by @Pankaj 29-March-17
        //SVMXC__Service_Order__c woObject = [SELECT SVMXC__Company__r.Oracle_Account_Number__c,SVMXC__Company__r.ExternalId__c,SVMXC__Component__r.External_ID__c         
                                            //FROM SVMXC__Service_Order__c WHERE Id = :objOrder.Id LIMIT 1];
        //objOrder.SVMXC__Company__r = woObject.SVMXC__Company__r;
        //objOrder.SVMXC__Component__r = woObject.SVMXC__Component__r;
        //objOrder.Work_Order_Quote__r = woObject.Work_Order_Quote__r;
        //objOrder.Bill_To_Location__r = woObject.Bill_To_Location__r;
        
               
     

        // The method below returns the detail Sobject records in a map (Key: Tab Id, Value: List of Sobject records)
        detailSobjectMap = def.SFM_Page_GetDetailRecords(request, Allobj);
        system.debug('detailSobjectMap = ' + detailSobjectMap);


        //**** revisit -tulsi
        /*if(Test.isRunningTest()){
            if(testClassVar == false) {
            List<SVMXC__Service_Order_Line__c> wodetailLsitTemp = new List<SVMXC__Service_Order_Line__c>();
            wodetailLsitTemp = [SELECT Id,SVMXC__Service_Order__c,recordTypeId,SVMXC__Discount__c,Total_Line_Price_MFS__c,
                                SVMXC__Is_Billable__c,SVMXC__Line_Type__c,Quarter_Hour_Unit__c,SVMXC__Actual_Quantity2__c,SVMXC__Product__c FROM SVMXC__Service_Order_Line__c];
            for(SVMXC__Service_Order_Line__c wdl:wodetailLsitTemp){
                List<SVMXC__Service_Order_Line__c> tempWDL = new List<SVMXC__Service_Order_Line__c>();
                if(detailSobjectMap.containsKey(wdl.SVMXC__Service_Order__c)){
                    tempWDL = detailSobjectMap.get(wdl.SVMXC__Service_Order__c);
                }
                tempWDL.add(wdl);
                detailSobjectMap.put(wdl.SVMXC__Service_Order__c,tempWDL);
            }
            }
        }*/

        Map<SVMXC__Service_Order_Line__c,String> workDetailToProduct = new Map<SVMXC__Service_Order_Line__c,String>();
        Map<String,String> tempProductMap = new Map<String,String>();
        Set<Id> workOrderLineSet = new Set<Id>();
        // Loop through the map to get the list of Sobject records and process them
        if(detailSobjectMap.size() > 0){

            //check to see if there are no detail records
            Boolean testNonBlankRecords = false;
            if(request.detailRecords != null){
                
                for(SVMXC.SFM_WrapperDef.SFM_TargetRecordObject targetRecordObject : request.detailRecords){
                    if(targetRecordObject.records != null && targetRecordObject.records.size() > 0){
                        testNonBlankRecords = true;
                    }
                }
            }


            if(testNonBlankRecords ){
                //Create Map of <Product ID , Product Name>
                set<Id> productIds = new set<Id>();

                for(String str : detailSobjectMap.keyset()){
                    list<SVMXC__Service_Order_Line__c> lstWoLines = new list<SVMXC__Service_Order_Line__c>();
                    lstWoLines = detailSobjectMap.get(str);
                    if(lstWoLines.size() > 0){
                        for(Integer i = 0; i < lstWoLines.size(); i++){
                            if(lstWoLines[i].SVMXC__Line_Type__c == 'Parts'){
                                
                        
                                String productId = (String)lstWoLines[i].SVMXC__Product__c;
                                productIds.add((Id)productId);
                                //workOrderLineSet.add(lstWoLines[i].id);
                                //workDetailToProduct.put(lstWoLines[i],(String)lstWoLines[i].SVMXC__Product__c);
                            }
                        }
                    }
                }

                system.debug('product set '+productIds);
               
            
                if(productIds.size()>0){

                    Map<Id,Product2> productMap = new MAp<Id,Product2>([SELECT Name ,Id,SAPNumber__c,QuantityUnitOfMeasure,ExternalId__c FROM Product2 WHERE Id IN :productIds]);
                
                    Map<String,SVMXC__Service_Order_Line__c> productWithPriceMap = new Map<String,SVMXC__Service_Order_Line__c>();

                    SVMXC__Service_Order__c wo = [select Id,SVMXC__Country__c,CurrencyISOCode,SVMXC__Company__r.SAPNumber__c from SVMXC__Service_Order__c where Id=:objOrder.id];

                    id usageId = SVMX_RecordTypeDataManager.GetRecordTypeID('UsageConsumption','SVMXC__Service_Order_Line__c');
                     //id estimateId = SVMX_RecordTypeDataManager.GetRecordTypeID('Estimate','SVMXC__Service_Order_Line__c');
                    //invoke service

                    //create input
                    //SVMX_Sales_Office__c so =[select Id,SVMX_Price_Order_Account__c,SVMX_Price_Order_Account__r.SAPNumber__c from SVMX_Sales_Office__c where SVMX_Country__c=:wo.SVMXC__Country__c];
                    Account acc = new Account();
                    acc.SAPNumber__c= wo.SVMXC__Company__r.SAPNumber__c;
                    acc.Name= 'test account';
                    acc.CurrencyISOCode= wo.CurrencyISOCode;

                    Quote q = new Quote();
                    q.Name= 'Test';
                    q.ScheduledDate__c =system.today();
                    q.CurrencyISOCode = wo.CurrencyISOCode;
                    q.GLN__c ='9921226005491';
                    q.ExternalQuoteNumber__c ='Dummy number';

                    List<QuoteLineItem> qList = new List<QuoteLineItem>();
                    for(Id prd :productMap.keyset()){

                            QuoteLineItem ql1 = new QuoteLineItem(Quantity=1,Product2=productMap.get(prd));
                            qList.add(ql1);
                            //system.debug('q prod '+ql1.Product2);
                            //system.debug('prod '+productMap.get(prd));
                    }

                    system.debug('Quoteitems '+qList); 
                    SAPPO_SalesOrderService.SalesOrderSimulationWrapper wrapper = new SAPPO_SalesOrderService.SalesOrderSimulationWrapper();
                    wrapper.account= acc;
                    wrapper.quote = q;
                    wrapper.quoteLineItems = qList;

                    system.debug('request from web service '+wrapper);
                    SAPPO_SalesOrderService priceService = new SAPPO_SalesOrderService();
                    List<Map<String,String>> results =new List<Map<String,String>>();
                    try{
                        SAPPO_SalesOrderService.SalesOrderResultWrapper resultWrapper = priceService.syncSimulatedSalesOrder(wrapper,false);
                      results = resultWrapper.data;
                    system.debug('response from web service:');
                        System.debug(resultWrapper);
                    }catch(Exception e){
                        system.debug('Error in SAP '+e);
                    }
                    //SAPPO_SalesOrderService.SalesOrderResultWrapper wrapperResult = priceService.syncSimulatedSalesOrder(wrapper,false);
                    //List<Map<String,String>> results = <<wrapperResult.data>>;

                    if(results != null && results.size() > 0){
                    
                        //construct results map -productWithPriceMap
                        for(Map<String,String> rs:results){
                            String MaterialId = rs.get('MaterialId');
                            String NetUnitPrice =rs.get('NetUnitPrice');
                            String UnitPrice =rs.get('UnitPrice');
                            Decimal BaseDiscount = rs.get('BaseDiscount') != null ? Decimal.valueOf(rs.get('BaseDiscount')): 0 ;
                            Decimal AdditionalDiscount =rs.get('AdditionalDiscount') != null ? Decimal.valueOf(rs.get('AdditionalDiscount')):0 ;
                            //Decimal SpecialDiscount =rs.get('SpecialDiscount') != null ? Decimal.valueOf(rs.get('SpecialDiscount')):0 ;

                            //Decimal discount = BaseDiscount +AdditionalDiscount+SpecialDiscount;
                            SVMXC__Service_Order_Line__c wl = new SVMXC__Service_Order_Line__c();

                            if(BaseDiscount != null)
                                 wl.SVMXC__Discount__c =Math.abs(BaseDiscount);
                            else
                                wl.SVMXC__Discount__c =0;
                             if(AdditionalDiscount != null)
                                wl.SVMX_Additional_Discount__c= Math.abs(AdditionalDiscount);
                            else
                                wl.SVMX_Additional_Discount__c=0;

                            system.debug('UnitPrice '+UnitPrice);
                            system.debug('NetUnitPrice '+NetUnitPrice);
                            if(NetUnitPrice != null)
                                wl.SVMXC__Actual_Price2__c =Decimal.valueOf(NetUnitPrice);
                            else if(UnitPrice != null)
                                wl.SVMXC__Actual_Price2__c =Decimal.valueOf(UnitPrice);

                            productWithPriceMap.put(MaterialId,wl);

                        }
                    }
                    system.debug('productWithPriceMap '+productWithPriceMap);

                    //REVSIT - TULSI
                    /*if(Test.isRunningTest()){
                        System.debug('==>productCodeMap1 '+ productCodeMap);
                        for(String str:productCodeMap.keySet()){
                            productWithPriceMap = new Map<String,String>();
                            productWithPriceMap.put(str,'100'); 
                            break;   
                        }
                        
                    }*/
                    for(String str : detailSobjectMap.keyset()){
                        list<SVMXC__Service_Order_Line__c> lstWoLines = new list<SVMXC__Service_Order_Line__c>();
                        lstWoLines = detailSobjectMap.get(str);
                        if(lstWoLines.size() > 0){
                            for(Integer i = 0; i < lstWoLines.size(); i++){
                                if(lstWoLines[i].SVMXC__Line_Type__c == 'Parts'){

                                    Product2 prd = productMap.get(lstWoLines[i].SVMXC__Product__c);
                                    String sapnum = prd.SAPNumber__c;
                                    // System.debug('==>pName'+productCode);

                                    if(prd !=null  && productWithPriceMap.size() > 0 &&  productWithPriceMap.containsKey(sapnum) && productWithPriceMap.get(sapnum) != null ){
                                     
                                         SVMXC__Service_Order_Line__c wl2= productWithPriceMap.get(sapnum);
                                         //lstWoLines[i].SVMXC__Billable_Line_Price__c = productWithPriceMap.get(sapnum).SVMXC__Actual_Price2__c;
                                         
                                         lstWoLines[i].SVMXC__Discount__c = wl2.SVMXC__Discount__c;

                                         lstWoLines[i].SVMX_Additional_Discount__c = wl2.SVMX_Additional_Discount__c;
                                         if(lstWoLines[i].RecordTypeID == usageId )
                                            lstWoLines[i].SVMXC__Actual_Price2__c = wl2.SVMXC__Actual_Price2__c;
                                         else
                                            lstWoLines[i].SVMXC__Estimated_Price2__c = wl2.SVMXC__Actual_Price2__c;

                                         lstWoLines[i].SVMXC__Use_Price_From_Pricebook__c =false;

                                        if(wl2.SVMXC__Actual_Price2__c > 0 &&  lstWoLines[i].RecordTypeID == usageId){
                                           if(wl2.SVMXC__Discount__c >0 && lstWoLines[i].SVMXC__Actual_Quantity2__c > 0 ) 
                                             lstWoLines[i].SVMXC__Billable_Line_Price__c= (wl2.SVMXC__Actual_Price2__c *lstWoLines[i].SVMXC__Actual_Quantity2__c - ((wl2.SVMXC__Discount__c/100) *(wl2.SVMXC__Actual_Price2__c * lstWoLines[i].SVMXC__Actual_Quantity2__c) ));
                                            else
                                             lstWoLines[i].SVMXC__Billable_Line_Price__c = wl2.SVMXC__Actual_Price2__c * lstWoLines[i].SVMXC__Actual_Quantity2__c;

                                            lstWoLines[i].SVMXC__Billable_Quantity__c = lstWoLines[i].SVMXC__Actual_Quantity2__c;
                                        }else if(wl2.SVMXC__Actual_Price2__c > 0  ){
                                            if(wl2.SVMXC__Discount__c >0 && lstWoLines[i].SVMXC__Estimated_Quantity2__c > 0) 
                                             lstWoLines[i].SVMXC__Billable_Line_Price__c= (wl2.SVMXC__Actual_Price2__c *lstWoLines[i].SVMXC__Estimated_Quantity2__c - ((wl2.SVMXC__Discount__c/100) *(wl2.SVMXC__Actual_Price2__c * lstWoLines[i].SVMXC__Estimated_Quantity2__c) ));
                                            else
                                             lstWoLines[i].SVMXC__Billable_Line_Price__c = wl2.SVMXC__Actual_Price2__c * lstWoLines[i].SVMXC__Estimated_Quantity2__c;

                                            lstWoLines[i].SVMXC__Billable_Quantity__c = lstWoLines[i].SVMXC__Estimated_Quantity2__c;
                                        }
                                   
                                    }else{
                                        system.debug('no value from SAP ');
                                    }

                                     lstWoLines[i].SVMX_Received_Price_From_SAP__c =true;
                                }
                            }
                            

                            newDetailSobjectMap.put(str, lstWoLines);
                        }
                    }
                     
                }
            }else{
                system.debug('blankSFMFlag####'+blankSFMFlag);
                blankSFMFlag = true;
            }

            //NOT REQUIRED
            /*if (!Test.isRunningTest()) {
                if(!blankSFMFlag)
                    pagedata = def.SFM_Page_BuildResponse(request, headerSobj, newDetailSobjectMap);
                else
                    pagedata = def.SFM_Page_BuildResponse(request, headerSobj, detailSobjectMap);
            }*/
        }
       
        
        system.debug('>>>>>>testttttt>>>>'+newDetailSobjectMap);
        system.debug('>>>>>>testtttt>>>>'+detailSobjectMap);
        
        // Call the method below with the Header Sobject record and processed Detail Sobjects map 
        if (!Test.isRunningTest()) {
            if(!blankSFMFlag)
                pagedata = def.SFM_Page_BuildResponse(request, headerSobj, newDetailSobjectMap);
            else
                pagedata = def.SFM_Page_BuildResponse(request, headerSobj, detailSobjectMap);
        }
        system.debug('Response = ' + pagedata);
        return pagedata;
        
        //return null;
    }
 
    
}