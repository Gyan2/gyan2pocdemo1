/************************************************************************************************************
Description: Test class for SVMX_PS_EntitledByServiceTrggrHandler class
 
Author: Keshava Prasad
Date: 14-05-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

@isTest
public class SVMX_PS_EntitledByServiceTrggrHandler_UT {
    public static testMethod void checkEntitlementWO() {
        
        Account account = SVMX_TestUtility.CreateAccount('Test Account', 'Norway', true) ;
        SVMXC__Site__c  location = SVMX_TestUtility.CreateLocation('Test Location', account.id, true) ;
        Product2 product = SVMX_TestUtility.CreateProduct('Test Product', true) ;
        SVMXC__Service_Order__c workOrder = SVMX_TestUtility.CreateWorkOrder(account.id, location.id, product.id, 'Open', system.label.Quoted_Works, 'Global_Std ', null, null, null, true) ;
        Contact contact = SVMX_TestUtility.CreateContact(account.id, true)  ;
        SVMXC__Service_Contract__c  contract = SVMX_TestUtility.CreateServiceContract(account.id, 'Test contract', 'Default', contact.id, true) ;
        
        SVMXC__Service_Contract_Services__c incServices = new SVMXC__Service_Contract_Services__c() ;
        incServices.SVMXC__Service_Contract__c = contract.id ;
        incServices.SVMX_Type__c = 'Quoted Works' ;
        insert incServices ;
            
        List<SVMXC__Entitlement_History__c> entList = new List<SVMXC__Entitlement_History__c>() ;
        SVMXC__Entitlement_History__c entitlementHistory = new SVMXC__Entitlement_History__c();
        entitlementHistory.SVMXC__Date_of_entitlement__c = Date.today().addDays(10) ;
        entitlementHistory.SVMXC__Start_Date__c = Date.today() ;
        entitlementHistory.SVMXC__End_Date__c = Date.today().addMonths(8);
        entitlementHistory.SVMXC__Service_Order__c = workOrder.id ;
        entitlementHistory.SVMXC__Service_Contract__c = contract.id ;
        
        
        SVMXC__Entitlement_History__c entitlementHistory2 = new SVMXC__Entitlement_History__c();
        entitlementHistory2.SVMXC__Date_of_entitlement__c = Date.today().addDays(10) ;
        entitlementHistory2.SVMXC__Start_Date__c = Date.today() ;
        entitlementHistory2.SVMXC__End_Date__c = Date.today().addMonths(8);
        entitlementHistory2.SVMXC__Service_Order__c = workOrder.id ;
        entitlementHistory2.SVMXC__Service_Contract__c = contract.id ;
        
        
        entList.add(entitlementHistory) ;
        entList.add(entitlementHistory2) ;
        insert entList ;
    }

}