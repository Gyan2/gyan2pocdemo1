public class SVMX_PartsOrderTriggerHandler extends SVMX_TriggerHandler {

  private List<SVMXC__RMA_Shipment_Order__c> newPartsOrderList;
    private List<SVMXC__RMA_Shipment_Order__c> oldPartsOrderList;
    private Map<Id, SVMXC__RMA_Shipment_Order__c> newPartsOrderMap;
    private Map<Id, SVMXC__RMA_Shipment_Order__c> oldPartsOrderMap;
     
    public SVMX_PartsOrderTriggerHandler() {
        this.newPartsOrderList = (List<SVMXC__RMA_Shipment_Order__c>) Trigger.new;
        this.oldPartsOrderList = (List<SVMXC__RMA_Shipment_Order__c>) Trigger.old;
        this.newPartsOrderMap = (Map<Id, SVMXC__RMA_Shipment_Order__c>) Trigger.newMap;
        this.oldPartsOrderMap = (Map<Id, SVMXC__RMA_Shipment_Order__c>) Trigger.oldMap;
    }

    public static id rmaRecordTypeId = SVMX_RecordTypeDataManager.getRecordTypeID('RMA','SVMXC__RMA_Shipment_Order__c');
    public static id shipmentRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Shipment','SVMXC__RMA_Shipment_Order__c');

    public override void afterUpdate() {

    Map<Id,SVMXC__RMA_Shipment_Order__c> rmaPartsOrderMap = new Map<Id,SVMXC__RMA_Shipment_Order__c>();
    set<Id> poId = new set<Id>(); 
    List<SVMXC__RMA_Shipment_Line__c> partsOrdLine = new  List<SVMXC__RMA_Shipment_Line__c>();
    List<SVMXC__RMA_Shipment_Line__c> partsOrdLinelistUpdate = new  List<SVMXC__RMA_Shipment_Line__c>();
    List<SVMXC__RMA_Shipment_Order__c> partsOrdlistUpdate = new  List<SVMXC__RMA_Shipment_Order__c>();
    Map<Id,SVMXC__RMA_Shipment_Order__c> sparepartsPartsOrdMapUpdate = new  Map<Id,SVMXC__RMA_Shipment_Order__c>();
    Map<Id,SVMXC__RMA_Shipment_Order__c> procuredPartsPartsOrdMapUpdate = new  Map<Id,SVMXC__RMA_Shipment_Order__c>();
    List<SVMXC__RMA_Shipment_Order__c> partsOrderclosedList = new  List<SVMXC__RMA_Shipment_Order__c>();
    list<SVMXC__RMA_Shipment_Line__c> partsorderLineList = new List<SVMXC__RMA_Shipment_Line__c>();
    set<Id> partsOrderIdset = new set<Id>(); 
      for(SVMXC__RMA_Shipment_Order__c po : newPartsOrderList) {

          if(po.RecordTypeId == rmaRecordTypeId) {
            
            rmaPartsOrderMap.put(po.Id, po);
          }
          //parts order of shipment and order type spare parts order
          if(po.RecordTypeId == shipmentRecordTypeId && po.SVMXC__Order_Type__c == 'Spare Parts Order') {
            
            sparepartsPartsOrdMapUpdate.put(po.Id,po);
          }

          //parts order of shipment and order type Procured Part
          if(po.RecordTypeId == shipmentRecordTypeId && po.SVMXC__Order_Type__c == 'Procured Part') {
            
            procuredPartsPartsOrdMapUpdate.put(po.Id,po);
          }
          // Arrival of PO Number to parts Order
          if(po.SVMX_Purchase_Order_Number__c != null && oldPartsOrderMap.get(po.id).SVMX_Purchase_Order_Number__c == null){
                
            if(po.SVMXC__Order_Type__c =='Stock Part' && po.SVMXC__Service_Order__c == null){
                SVMXC__RMA_Shipment_Order__c partsOrder = new SVMXC__RMA_Shipment_Order__c();
                partsOrder.Id= po.Id;
                partsOrder.SVMXC__Order_Status__c='Closed';
                partsOrderclosedList.add(partsOrder);
                partsOrderIdset.add(partsOrder.Id);
            }

          }
      }
      if(!partsOrderIdset.isEmpty()) {
            partsorderLineList = SVMX_PartsOrderLineDataManager.PartOrdIdParOrdLineQuery(partsOrderIdset);
        }
      if(partsorderLineList.size()>0){
          for(SVMXC__RMA_Shipment_Line__c partsOrderLine:partsorderLineList){
                  
                  partsOrderLine.SVMXC__Line_Status__c ='Completed';
          }
      }   

      if(partsOrderclosedList.size()>0){
          update partsOrderclosedList;
      }

      if(partsorderLineList.size()>0) {
            SVMX_PartsOrderLineDataManager.updatepartsOrderline(partsorderLineList);
      }

      if(!rmaPartsOrderMap.isEmpty()) {

        SVMX_RMAServiceManager.handleSAPSalesOrderUpdateRMAHeader(rmaPartsOrderMap, oldPartsOrderMap);
      }
      //calls the SVMX_SparePartsShipmentManager for SOAP CallOut
      if(!sparepartsPartsOrdMapUpdate.keyset().isEmpty()) {

        SVMX_SparePartsShipmentManager.handleSAPSalesOrderHeaderUpdates(sparepartsPartsOrdMapUpdate, oldPartsOrderMap);
      }
      //calls the SVMX_ProcuredPartsShipmentManager for SOAP CallOut
      if(!procuredPartsPartsOrdMapUpdate.keyset().isEmpty()) {

        SVMX_ProcuredPartsShipmentManager.handleSAPSalesOrderHeaderUpdates(procuredPartsPartsOrdMapUpdate, oldPartsOrderMap);
      }

      //To check sap recrution number is not blank of shipment 
      for(SVMXC__RMA_Shipment_Order__c po : newPartsOrderList) {
            
            system.debug('po-->'+po);
            if(po.RecordTypeId == shipmentRecordTypeId && po.SVMX_Purchase_Requisition_Number__c != null && po.SVMX_Purchase_Requisition_Number__c != oldPartsOrderMap.get(po.id).SVMX_Purchase_Requisition_Number__c) {
              system.debug('po-->'+po);
              poId.add(po.Id);
            }            
      }

      if(poId.size() > 0 ){
         for(id partsordid :poId){
            system.debug('partsordid-->'+partsordid);
            SVMXC__RMA_Shipment_Order__c partOrd = new SVMXC__RMA_Shipment_Order__c (id = partsordid,SVMX_Awaiting_SAP_Response__c = false);
            partsOrdlistUpdate.add(partOrd);
         }
      }    

      if(poId.size() >0 ){
        partsOrdLine = SVMX_PartsOrderLineDataManager.ParOrdLineBlankcheckQuery(poId);
        system.debug('partsOrdLine-->'+partsOrdLine);
      }

      if(partsOrdLine.size() >0 ){  
          for(SVMXC__RMA_Shipment_Line__c pol : partsOrdLine ){
               
              system.debug('pol-->'+pol);
              SVMXC__RMA_Shipment_Line__c partOrdLine = new SVMXC__RMA_Shipment_Line__c (id=pol.id,SVMX_Item_Number_Blank_Check__c = false);
              partsOrdLinelistUpdate.add(partOrdLine);
          }
      }    

      if(partsOrdLine.size() >0 ){
        
        system.debug('partsOrdLine.size()-->'+partsOrdLine.size());
        SVMX_SAPPurchaseRequestUtility.handleSAPPurchaseRequestCreation(partsOrdLine);
      }

      if(partsOrdLinelistUpdate.size() >0 ){
        
        system.debug('partsOrdLinelistUpdate.size()-->'+partsOrdLinelistUpdate.size());
        update partsOrdLinelistUpdate;
      }

      if(partsOrdlistUpdate.size() >0 ){
        
        system.debug('partsOrdLinelistUpdate.size()-->'+partsOrdLinelistUpdate.size());
        update partsOrdlistUpdate;
      }

       
    }

}