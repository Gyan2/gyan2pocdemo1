public without sharing class AttachmentServices {

    public static void applyRenamingRules(List<Attachment> paramAttachments) {
        Map<String, List<Attachment>> sObjectTypeToAttachmentMap = new Map<String, List<Attachment>>();
        Map<String, Set<Id>> sObjectTypeToParentIdSet = new Map<String, Set<Id>>();
        Map<String, List<AttachmentRenamingRule__mdt>> sObjectTypeToAttachmentRenamingRules = new Map<String, List<AttachmentRenamingRule__mdt>>();
        Map<String, Set<String>> sObjectTypeFields = new Map<String, Set<String>>();

        // 1st classify attachments per type
        for (Attachment forAtt : paramAttachments) {
            String attachmentType = String.valueOf(forAtt.ParentId.getSobjectType());
            if (sObjectTypeToAttachmentMap.containsKey(attachmentType)) {
                sObjectTypeToAttachmentMap.get(attachmentType).add(forAtt);
                sObjectTypeToParentIdSet.get(attachmentType).add(forAtt.ParentId);
            } else {
                sObjectTypeToAttachmentMap.put(attachmentType, new List<Attachment>{forAtt});
                sObjectTypeToParentIdSet.put(attachmentType, new Set<Id>{forAtt.ParentId});
                sObjectTypeToAttachmentRenamingRules.put(attachmentType, new List<AttachmentRenamingRule__mdt>());
                sObjectTypeFields.put(attachmentType, new Set<String>());
            }
        }

        // 2nd get renaming rules for affected types
        for (AttachmentRenamingRule__mdt forRule : [SELECT MasterLabel, RelatedObject__c, RenameTo__c FROM AttachmentRenamingRule__mdt WHERE IsActive__c = true AND RelatedObject__c IN :sObjectTypeToAttachmentRenamingRules.keySet()]) {
            sObjectTypeToAttachmentRenamingRules.get(forRule.RelatedObject__c).add(forRule);
            sObjectTypeFields.get(forRule.RelatedObject__c).addAll(extractFieldFromString(forRule.RenameTo__c));
        }

        // fetch all related Records
        for (String forSObjectName : sObjectTypeToParentIdSet.keySet()) {
            if (!sObjectTypeFields.get(forSObjectName).isEmpty()) {
                Set<Id> ids = sObjectTypeToParentIdSet.get(forSObjectName);

                applyRenamingRules(
                    sObjectTypeToAttachmentMap.get(forSObjectName),
                    sObjectTypeToAttachmentRenamingRules.get(forSObjectName),
                    new Map<Id, sObject>(database.query('SELECT ' + String.join(new List<String>(sObjectTypeFields.get(forSObjectName)), ',') + ' FROM ' + forSObjectName + ' WHERE Id IN :ids')),
                    sObjectTypeFields.get(forSObjectName)
                );
            }
        }
    }

    private static void applyRenamingRules(List<Attachment> paramAttachments, List<AttachmentRenamingRule__mdt> paramRules, Map<Id, sObject> paramParentRecords, Set<String> paramFields) {
        System.debug(paramAttachments);
        System.debug(paramRules);
        System.debug(paramParentRecords);
        System.debug(paramFields);

        for (Attachment forAttachment : paramAttachments) {
            sObject forParentRecord = paramParentRecords.get(forAttachment.ParentId);
            for (AttachmentRenamingRule__mdt forRule : paramRules) {
                if (forAttachment.Name.startsWith(forRule.MasterLabel)) {
                    String newName = forRule.RenameTo__c + forAttachment.Name.substring(forAttachment.Name.lastIndexOf('.'));
                    for (String forField : paramFields) {
                        String fieldValue = forParentRecord.get(forField)==null?'':String.valueOf(forParentRecord.get(forField));
                        newName = newName.replace('{!' + forField + '}', fieldValue);
                    }
                    forAttachment.Name = newName;
                }
            }
        }
    }

    public static Set<String> extractFieldFromString(String paramString) {
        return extractFieldFromString(paramString, new Set<String>());
    }
    private static Set<String> extractFieldFromString(String paramString, Set<String> paramCurrentFields) {
        String field = paramString.substringBetween('{!','}');
        if (field != null) {
            paramCurrentFields.add(field);
            extractFieldFromString(paramString.substring(1+paramString.indexOf('{!' + field + '}')), paramCurrentFields);
        }
        return paramCurrentFields;
    }

}