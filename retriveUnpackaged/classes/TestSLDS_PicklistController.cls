/**
 * Created by rebmangu on 09/05/2018.
 */

@IsTest
public class TestSLDS_PicklistController {

    static testMethod void testDependentMethod(){
        // I don't really know how to test tha one without creating a dummy dependent picklist
        Map<String, List<UtilDependentPicklist.PicklistEntryWrapper>> result = SLDS_PicklistController.getDependentOptions('Origin','Reason','Case');
        System.assertEquals(true,result.size() > 0);
    }

    static testMethod void testGetPicklistMethod(){
        List<UtilDependentPicklist.PicklistEntryWrapper> result = SLDS_PicklistController.getPicklistValues('Origin','Case');
        System.assertEquals(true,result.size() > 0);

    }
}