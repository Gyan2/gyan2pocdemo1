@isTest
public class SVMX_ServiceContractProdctDataManger_UT{
    
    public static testMethod void checkTimeBasedscproductBlankcheck(){
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
           
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);

        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id, false);
        sc.SVMXC__Active__c = False;
        sc.SVMX_Pricing_Method__c = 'Both';
        sc.SVMXC__Start_Date__c = Date.newInstance(2018, 1, 1);
        sc.SVMXC__End_Date__c = Date.newInstance(2019, 12, 10);
        insert sc;

        set<id> idset = new set<id>();
        idset.add(sc.id);

        Date startDate=system.today();

        SVMX_Contract_Line_Items__c  CreateContractLine = SVMX_TestUtility.CreateContractLineItem(sc.Id,'Yearly on Last of Start month',startDate,false);
        //CreateContractLine.SVMX_Contract_Start__c = Date.newInstance(2016, 12, 9);
        CreateContractLine.SVMX_Contract_End__c = Date.newInstance(2018, 12, 9);
        CreateContractLine.SVMX_SAP_Contract_Item_Num_Blank_Check__c = true;
        insert CreateContractLine;

        SVMX_ServiceContractProductDataManager.timeBasedSCProductBlankCheckQuery(idset);
    }    
}