/*******************************************************************************************************
Description: 

Dependancy: 
    Trigger Framework: 
    Trigger: 
    Service Manager: 
    Test class: 
    
 
Author: Tulsi B R
Date: 17-01-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

public with sharing class SVMX_CaseTriggerHandler extends SVMX_TriggerHandler {

   private list<Case> newWOList;
   private list<Case> oldWOList;
   private map<Id, Case> newWOMap;
   private map<Id, Case> oldWOMap;
   private list<Id>    accountIds;
   private map<Id,string>   accountStatusAutomatic;
    
   public SVMX_CaseTriggerHandler(){
        this.newWOList = (list<Case>) Trigger.new;
        this.oldWOList = (list<Case>) Trigger.old;
        this.newWOMap = (Map<Id, Case>) Trigger.newMap;
        this.oldWOMap = (Map<Id, Case>) Trigger.oldMap;
   }
   public override void beforeupdate(){
      accountIds=new List<Id>();
      for(Case  accNewIdsOfCase:newWOList){
      //check for entry creiteri 
          if(oldWOMap.get(accNewIdsOfCase.Id).status != newWOMap.get(accNewIdsOfCase.Id).status
            && accNewIdsOfCase.status == System.Label.Ready_To_Bill){
            accountIds.add(accNewIdsOfCase.AccountId); 
        }
       
         
      }
      accountStatusAutomatic=new  Map<Id,string> ();
      //check if list >0

      if(accountIds.size() > 0){
      List<Account>  accounts=[select Id,SVMX_Automatic_billing__c from account where Id IN:accountIds];
   
      if(accounts!=null){
          for(Account acc:accounts){
            if(acc.SVMX_Automatic_billing__c == 'Automatic'){
                 accountStatusAutomatic.put(acc.Id,acc.SVMX_Automatic_billing__c);
              
            }
          }
      }
      }
      for(Case updateCaseList:newWOList){
          if(oldWOMap.get(updateCaseList.Id).status != newWOMap.get(updateCaseList.Id).status 
                && updateCaseList.status == System.Label.Ready_To_Bill){
             String accountStatus= accountStatusAutomatic.get(updateCaseList.AccountId);
             if(accountStatus =='Automatic'){
               updateCaseList.SVMX_Invoice_Status__c='Requested';
                
             }

           }

       }
    }

    public override void afterUpdate(){

      Map<Id,Case> newCaseMap = new Map<Id,Case>();
      map<id,case> caseIdtoUpdatSalOrdnumMap = new map<id,case>();
      list<SVMXC__Service_Order__c> woList = new list<SVMXC__Service_Order__c> ();
      
      for(Case cas : newWOMap.values()) {
        
          newCaseMap.put(cas.Id, cas);
      }

      for(case cs : newWOMap.values()){
          if(cs.SVMX_Service_Sales_Order_Number__c != null && oldWOMap.get(cs.Id).SVMX_Service_Sales_Order_Number__c == null ){

            caseIdtoUpdatSalOrdnumMap.put(cs.Id,cs);
          }
      }

      if(!newCaseMap.isEmpty()) {
        
        if(!System.isBatch()){
          SVMX_SAPSalesOrderUtility.handleCaseUpdatesForSalesOrderUpdate(newCaseMap,oldWOMap);
        }  
      } 

      if(caseIdtoUpdatSalOrdnumMap.keyset().size() > 0){
          for(SVMXC__Service_Order__c wo:SVMX_WorkOrderDataManager.WoOfcase(caseIdtoUpdatSalOrdnumMap.keyset())){
                 
                 SVMXC__Service_Order__c wo1 = new SVMXC__Service_Order__c (id = wo.id , SVMX_Service_Sales_Order_Number__c =caseIdtoUpdatSalOrdnumMap.get(wo.SVMXC__Case__c).SVMX_Service_Sales_Order_Number__c);
                 woList.add(wo1);
          }
      }
      if(woList.size()>0)  {
        SVMX_WorkOrderDataManager.updateWO(woList);
      }
    }   
}