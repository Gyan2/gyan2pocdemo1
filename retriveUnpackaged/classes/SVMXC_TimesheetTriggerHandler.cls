/*******************************************************************************************************
Description: Trigger handler for TimeSheet Trigger

Dependancy: 
    
    
 
Author: UZMA K
Date: 15-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
19-12-2017      Pooja Singh     Added validation on timesheet if overlap time entry is exists
21-12-2017      uzma khan       line 64 to 74 uncheck the previous week of previous records

*******************************************************************************************************/

public class SVMXC_TimesheetTriggerHandler extends SVMX_TriggerHandler {

    private list<SVMXC__Timesheet__c> newTSList;
    private list<SVMXC__Timesheet__c> oldTSList;
    public Map<Id, SVMXC__Timesheet__c> newTSMap;
    public Map<Id, SVMXC__Timesheet__c> oldTSMap;
    public set<Date> TSdate = new set<Date>();
    
    public SVMXC_TimesheetTriggerHandler() {
        this.newTSList = (list<SVMXC__Timesheet__c>) Trigger.new;
        this.oldTSList = (list<SVMXC__Timesheet__c>) Trigger.old;
        this.newTSMap = (Map<Id, SVMXC__Timesheet__c>) Trigger.newMap;
        this.oldTSMap = (Map<Id, SVMXC__Timesheet__c>) Trigger.oldMap;
    }

    Public override void beforeUpdate(){
        Set<Id> tsIdSet = new Set<Id>();
        for(SVMXC__Timesheet__c SVMXTS : newTSmap.values()){
            SVMXC__Timesheet__c oldTS = oldTSMap.get(SVMXTS.id);
            if(SVMXTS.SVMXC__Status__c == System.Label.submitted && oldTS.SVMXC__Status__c != System.Label.submitted) {
                tsIdSet.add(SVMXTS.id);
                System.debug('set of timesheet id' +tsIdSet);
                TSdate.add(SVMXTS.SVMXC__Start_Date__c);
               system.debug('@@' +TSdate);
               
            }

            if(SVMXTS.SVMXC__Status__c == System.Label.Approved && oldTS.SVMXC__Status__c != System.Label.Approved) {

                if(SVMXTS.SVMX_Time_Entries_without_SO_Number__c > 0) {

                    SVMXTS.addError('Sales Order Item Number does on exist on all Time Entries. The timesheet cannot be approved at this time. Please try again later');    
                }
            }
         }
         if(tsIdSet.size() > 0){
         List<SVMXC__Timesheet_Entry__c> teListQuery = new List<SVMXC__Timesheet_Entry__c>([Select Id, SVMXC__Timesheet__c from SVMXC__Timesheet_Entry__c where SVMXC__Timesheet__c IN:tsIdSet and SVMX_Overlaps__c='Yes']);
            for(SVMXC__Timesheet__c SVMXTS : newTSmap.values()){
                for(SVMXC__Timesheet_Entry__c te : teListQuery){
                    if(SVMXTS.id == te.SVMXC__Timesheet__c){
                        SVMXTS.addError('An overlapping time entry is open for this timesheet');
                    }
                }
                if(teListQuery.size() == 0){
                        System.debug('Inside approval process');
                        Approval.ProcessSubmitRequest req = new Approval.ProcessSubmitRequest();
                        req.setComments('Submitted for approval. Please approve.');
                        req.setObjectId(SVMXTS.Id);
                        // submit the approval request for processing
                    try {
                        Approval.ProcessResult result = Approval.process(req);
                    }catch(Exception e) {
                        system.debug('Exception on Approval'+ e.getMessage()) ;
                    }
                }
            }     
            List<date> lstDate = new List<Date>();
             lstDate.addall(TSdate);
             Date maxdate = lstDate.get(lstDate.size()-1); 
            List<SVMXC__Timesheet__c> tsQuery = new List<SVMXC__Timesheet__c>([Select id,SVMX_Previous_Week__c,SVMXC__Start_Date__c from SVMXC__Timesheet__c where SVMX_Previous_Week__c = true AND SVMXC__Start_Date__c <: maxdate]); 
             for(SVMXC__Timesheet__c prevTs : tsQuery){
                 prevTs.SVMX_Previous_Week__c = false;
                 system.debug('previous timesheet'+prevTs);
             }
             
             if(tsQuery.size() >0 ){
      update tsQuery;               
             }    }
    }   

    Public override void afterUpdate(){

            set<id> tsIdset = new set<id>();
            List<SVMXC__Timesheet_Entry__c> timeentryUpdate = new  List<SVMXC__Timesheet_Entry__c>();
            List<SVMXC__Timesheet_Entry__c> teListQuery = new List<SVMXC__Timesheet_Entry__c>();
            
            for(SVMXC__Timesheet__c Ts :newTSList){

                if(Ts.SVMXC__Status__c == System.Label.Approved && Ts.SVMXC__Status__c != oldTSMap.get(Ts.Id).SVMXC__Status__c){
                    
                    system.debug('SVMXC__Status__c'+Ts.SVMXC__Status__c);
                    tsIdset.add(Ts.Id);
                }

            }
            if(tsIdset.size() > 0){
              
              system.debug('tsIdset'+tsIdset.size());
              teListQuery = [Select Id, SVMXC__Timesheet__c from SVMXC__Timesheet_Entry__c where SVMXC__Timesheet__c IN: tsIdset ];
            }
            
            if(teListQuery.size() > 0){
                for(SVMXC__Timesheet_Entry__c timeEntry :teListQuery){
                    
                    system.debug('teListQuery'+teListQuery.size());
                    SVMXC__Timesheet_Entry__c te = new SVMXC__Timesheet_Entry__c( Id=timeEntry.Id,SVMX_Timesheet_Approveed__c = True );
                    timeentryUpdate.add(te);
                }
            }    

            if(timeentryUpdate.size() > 0){
                system.debug('timeentryUpdate'+timeentryUpdate.size());
                update timeentryUpdate;
            }
    }
    
}