/************************************************************************************************************
Description: Service Manager for Work Order. All the methods and the logic for Work Order object resides here.

Dependancy: 
    Data Manager: SVMX_WorkOrderDataManager.cls
                  SVMX_ServiceContractDataManager.cls
                  SVMX_EntitlementHistoryDataManager.cls
 
Author: Ranjitha S
Date: 17-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
13 Jan 2018     Pooja Singh     Added setWorkOrderCurrencyFromAcc method. The logic inside sets the work order currency to account's currency.
28 Jun 2018     Keshava Prasad  Added create createEntitleHistory method. Used to create entitlement history upon insert of a workorder.
****************************************************************************************************************/
public class SVMX_WorkOrderServiceManager {
    
    public static String PrioritySurcharge= Label.Priority_Surcharge;
    public static String ProductSurcharge= Label.Product_Surcharge;
    public static String Labor= Label.Labor;
    public static String Expenses= Label.Expenses;
    public static String Travel= Label.Travel;
    public static String readyToBill = Label.Ready_To_Bill;
    
    public static Map<Id,SVMXC__Service_Order__c> woQueryMap = new Map<Id,SVMXC__Service_Order__c> ();
    
    public static Id usageRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('UsageConsumption','SVMXC__Service_Order_Line__c');
        
    public static void getWorkOrders(List<SVMXC__Service_Order__c> nonContractWOList){
       woQueryMap = SVMX_WorkOrderDataManager.woQuery(nonContractWOList);
    }

    /* 2018-04-20   Yan Anderson    Set the work order billing type to 'Free Of Charge' 
                                        if work order type is installation, ppm, quoted works */
    public static void updateBillingType(list<SVMXC__Service_Order__c> woList){
        
        for(SVMXC__Service_Order__c wo : woList){
            if( wo.SVMXC__Order_Type__c  == System.Label.Installation || wo.SVMXC__Order_Type__c  == System.Label.Planned_Services_PPM || wo.SVMXC__Order_Type__c  == System.Label.Quoted_Works){
                wo.SVMX_Billing_Type2__c = System.Label.Free_Of_Charge;
            }
        }
    }
    
    
    /* Called from AfterInsert WO trigger. 
     * If warranty field on case is not null, then the Active Warranty field on the work order is checked. */
    public static void updateActiveWarranty(list<SVMXC__Service_Order__c> woList,Map<Id,Case> caseList){

        map<id,SVMXC__Service_Order__c> woUpdateList = new map<id,SVMXC__Service_Order__c>();
        //list<SVMXC__Service_Order__c> woQueryMap = SVMX_WorkOrderDataManager.woQuery(woList);


        for(SVMXC__Service_Order__c wo : woList){
            
            if( !caseList.isEmpty() && caseList.containskey(wo.SVMXC__Case__c) && caseList.get(wo.SVMXC__Case__c).SVMXC__Warranty__c != null){
                wo.SVMX_Active_Warranty__c = TRUE;
               
            }
        }

       
    }

    public static void createEntitleHistory(List<SVMXC__Service_Order__c> woList) {
        List<SVMXC__Entitlement_History__c> entList = new List<SVMXC__Entitlement_History__c>() ;
        for(SVMXC__Service_Order__c wo : woList) {
            SVMXC__Entitlement_History__c entitlementHistory = new SVMXC__Entitlement_History__c() ;
            entitlementHistory.SVMXC__Case__c = wo.SVMXC__Case__c ;
            entitlementHistory.SVMXC__Service_Contract__c = wo.SVMXC__Service_Contract__c ;
            entitlementHistory.SVMXC__Service_Order__c = wo.id ;
            entList.add(entitlementHistory) ;
        }

        if(!entList.isEmpty()) {
        SVMX_EntitlementHistoryDataManager.createEntitleHistory(entList) ;
    }
    }

    public static void createCase(List<SVMXC__Service_Order__c> woList){


        Map<Id, Case> caseMap = new Map<Id,Case>() ;
        
        for(SVMXC__Service_Order__c wo : woList) {
           
                Case newCase = new Case() ;
                newCase.SVMXC__Site__c = wo.SVMXC__Site__c ;
                newCase.ContactId = wo.SVMXC__Contact__c ;
                newCase.Priority = 'Medium' ;
                newCase.Status = 'New' ;
                newCase.Type = System.Label.Planned_Services_PPM ;
                newCase.AccountId = wo.SVMXC__Company__c ; 
                newCase.Subject = wo.SVMX_PS_Subject__c ;
                newCase.SVMX_Customer_PO_Number__c = wo.SVMX_PS_Customer_P_O_Number__c ;
                newCase.SVMX_Account_Country_Use__c = wo.SVMXC__Country__c ;

                caseMap.put(wo.id,newCase) ;
              

        }


        if(!caseMap.isEmpty()) {

            SVMX_CaseDataManager.insertCase(caseMap) ;
        }

         for(Id woId : caseMap.keyset() ) {
             for(SVMXC__Service_Order__c wo : woList) {
                if(wo.id == woid){
                    wo.SVMXC__Case__c = caseMap.get(wo.id).id ;
                      system.debug('wo ID '+ caseMap.get(wo.id).id) ;
                }

            
             }

        }

    }

  
    
    /* Called from AfterUpdate WO trigger. 
     * This method is used to find the default contract for the country when work order Entitlement Status = ‘Failed’ and attach to the work order.
     * Entitlement history record is added */
        
    /*public static Map<Id,SVMXC__Service_Order__c> nonContractMethod(list<SVMXC__Service_Order__c> nonContractWOList,set<String> countrySet,map<id,SVMXC__Service_Order__c> UpList){
        
        List<SVMXC__Entitlement_History__c> entitleHistoryList = new List<SVMXC__Entitlement_History__c>();
         
        map<id,SVMXC__Service_Order__c> woreturnList = new map<id,SVMXC__Service_Order__c>();   
   
        
        if(!woQueryMap.isEmpty()){
            Id nonContractRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Default','SVMXC__Service_Contract__c');
            List<SVMXC__Service_Contract__c> nonContractList = SVMX_ServiceContractDataManager.serviceContractQuery(countrySet,nonContractRecordTypeId);
            if(!nonContractList.isEmpty()){
                Map<String,SVMXC__Service_Contract__c> countryContractMap = new Map<String,SVMXC__Service_Contract__c>();
                                        
                for(SVMXC__Service_Contract__c sc:nonContractList){
                    countryContractMap.put(sc.SVMXC__Company__r.BillingCountry,sc);
                }
                
            
                if(!countryContractMap.isEmpty()){ 
                    for(SVMXC__Service_Order__c wo2: nonContractWOList){
                         SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
                        if(!UpList.isEmpty() && UpList.containskey(wo2.id))
                                wo = UpList.get(wo2.id);
                            else
                                wo.id = wo2.id;

                            system.debug('woQueryMap '+woQueryMap);
                            system.debug(' country '+woQueryMap.get(wo2.id).SVMXC__Country__c);
                        wo.SVMXC__Billing_Type__c = 'Contract';
                        wo.SVMXC__Is_Entitlement_Performed__c = true;
                        wo.SVMXC__Entitlement_Type__c = 'AUTO';
                        wo.SVMXC__Auto_Entitlement_Status__c = 'Success';
                        wo.SVMXC__Service_Contract__c = countryContractMap.get(woQueryMap.get(wo2.id).SVMXC__Country__c).Id;
                        wo.SVMXC__SLA_Terms__c = countryContractMap.get(woQueryMap.get(wo2.id).SVMXC__Country__c).SVMXC__Service_Level__c;
                     
                        SVMXC__Entitlement_History__c entitleHistory = new SVMXC__Entitlement_History__c();
                        entitleHistory.SVMXC__Service_Contract__c = countryContractMap.get(woQueryMap.get(wo2.id).SVMXC__Country__c).Id;
                        entitleHistory.SVMXC__Service_Order__c = woQueryMap.get(wo2.id).Id;
                        entitleHistory.SVMXC__Date_of_entitlement__c = system.today();
                        entitleHistory.SVMXC__Start_Date__c = system.today();
                        entitleHistory.SVMXC__End_Date__c = countryContractMap.get(woQueryMap.get(wo2.id).SVMXC__Country__c).SVMXC__End_Date__c;
                    
                        entitleHistoryList.add(entitleHistory);
                        UpList.put(wo.id,wo);
                    }   
                    //SVMX_WorkOrderDataManager.updateWO(updateWOList);
                    SVMX_EntitlementHistoryDataManager.createEntitleHistory(entitleHistoryList); 
                }
            }   
        }

        
        return UpList;
    }*/
    @future(callout=true)
    public static void nonContractMethod(list<Id> nonContractWOList,set<String> countrySet){
        
        List<SVMXC__Entitlement_History__c> entitleHistoryList = new List<SVMXC__Entitlement_History__c>();
         
        map<id,SVMXC__Service_Order__c> woreturnList = new map<id,SVMXC__Service_Order__c>();   
        
        Map<Id,SVMXC__Service_Order__c> UpList =new map<id,SVMXC__Service_Order__c>();

        Map<Id,SVMXC__Service_Order__c> woQueryMap2 = SVMX_WorkOrderDataManager.woQuery2(nonContractWOList);
        
        if(!woQueryMap2.isEmpty()){
            Id nonContractRecordTypeId = SVMX_RecordTypeDataManager.GetRecordTypeID('Default','SVMXC__Service_Contract__c');
            List<SVMXC__Service_Contract__c> nonContractList = SVMX_ServiceContractDataManager.serviceContractQuery(countrySet,nonContractRecordTypeId);
            if(!nonContractList.isEmpty()){
                Map<String,SVMXC__Service_Contract__c> countryContractMap = new Map<String,SVMXC__Service_Contract__c>();
                                        
                for(SVMXC__Service_Contract__c sc:nonContractList){
                    countryContractMap.put(sc.SVMXC__Company__r.BillingCountry,sc);
                }
                
                system.debug('countryContractMap '+countryContractMap);
                if(!countryContractMap.isEmpty()){ 
                    for(Id wo2: nonContractWOList){
                         SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c(Id=wo2);
                         

                            system.debug('woQueryMap '+woQueryMap2);
                            system.debug(' country '+woQueryMap2.get(wo2).SVMXC__Country__c);
                        wo.SVMXC__Billing_Type__c = 'Contract';
                        wo.SVMXC__Is_Entitlement_Performed__c = true;
                        wo.SVMXC__Entitlement_Type__c = 'AUTO';
                        wo.SVMXC__Auto_Entitlement_Status__c = 'Success';
                        Date startDate = system.today();
                        Date endDate = countryContractMap.get(woQueryMap2.get(wo2).SVMXC__Country__c).SVMXC__End_Date__c;
                        wo.SVMXC__Entitlement_Notes__c = 'Entitled by ServiceMax auto-entitlement process.\n'
                                                          +'Auto-Entitlement Rule: Default Contract\n'
                                                            +'Service Contract: '+countryContractMap.get(woQueryMap2.get(wo2).SVMXC__Country__c).name
                                                            +'\nStart Date: '+startdate
                                                            +'\nEnd Date: '+enddate;
                        wo.SVMXC__Service_Contract__c = countryContractMap.get(woQueryMap2.get(wo2).SVMXC__Country__c).Id;
                        wo.SVMXC__SLA_Terms__c = countryContractMap.get(woQueryMap2.get(wo2).SVMXC__Country__c).SVMXC__Service_Level__c;
                     
                        SVMXC__Entitlement_History__c entitleHistory = new SVMXC__Entitlement_History__c();
                        entitleHistory.SVMXC__Service_Contract__c = countryContractMap.get(woQueryMap2.get(wo2).SVMXC__Country__c).Id;
                        entitleHistory.SVMXC__Service_Order__c = woQueryMap2.get(wo2).Id;
                        entitleHistory.SVMXC__Date_of_entitlement__c = system.today();
                        entitleHistory.SVMXC__Start_Date__c = system.today();
                        entitleHistory.SVMXC__End_Date__c = countryContractMap.get(woQueryMap2.get(wo2).SVMXC__Country__c).SVMXC__End_Date__c;
                    
                        entitleHistoryList.add(entitleHistory);
                        UpList.put(wo.id,wo);
                    }   
                    //SVMX_WorkOrderDataManager.updateWO(updateWOList);
                    SVMX_EntitlementHistoryDataManager.createEntitleHistory(entitleHistoryList); 
                }
            }   
        }

           if(UpList.size() > 0)
            SVMX_WorkOrderServiceManager.UpdateWorkOrders(UpList.values());
        
    }

    public static void UpdateWorkOrders(list<SVMXC__Service_Order__c> UpList){
        SVMX_WorkOrderDataManager.updateWO(UpList);
    }   
    
    @future(callout=true)
    public static void addPrioritySurchargefuture(list<id> priorityWOList){
        addPrioritySurcharge(priorityWOList);
    }
    
    public static void addPrioritySurcharge(list<id> priorityWOList){
        
        List<SVMXC__Service_Order__c> woPrioritySurList = new list<SVMXC__Service_Order__c>();
        Map<SVMXC__Service_Order__c,string> woProductSurMap = new Map<SVMXC__Service_Order__c,string>();
        Set<id> scIds = new Set<id>();
        set<String> countries = new set<String>();
        map<string,SVMX_Sales_Office__c> countrySalesOffMap = new map<string,SVMX_Sales_Office__c>();
        
      //  Map<Id,SVMXC__Service_Order__c> woQueryMap2 = SVMX_WorkOrderDataManager.woQuery2(priorityWOList);
        Map<Id,SVMXC__Service_Order__c> woQueryMap = SVMX_WorkOrderDataManager.woQuery2(priorityWOList);
        
        for(SVMXC__Service_Order__c wo: woQueryMap.valueS()){
            system.debug('woquery product family '+woQueryMap.get(wo.id).SVMXC__Product__r.Family);
            if(wo.SVMXC__Priority__c != null && woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Priority_Surcharge__c != null){
                if(!woQueryMap.isEmpty() && woQueryMap.containsKey(wo.id) && (woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Priority_Surcharge__c.contains('One Time') || woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Priority_Surcharge__c.contains('Labor')) && woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Priority__c.contains(wo.SVMXC__Priority__c))
                   woPrioritySurList.add(wo);
                  countries.add(wo.SVMXC__Country__c);
            }
            if(woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Product_Surcharge_Type__c != null){
                if(!woQueryMap.isEmpty() && woQueryMap.containsKey(wo.id) && woQueryMap.get(wo.id).SVMXC__Product__r.Family != null && (woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Product_Surcharge_Type__c.contains('One Time') || woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Product_Surcharge_Type__c.contains('Labor')) && woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Product_Surcharge__c == true){ 
                    scIds.add(wo.SVMXC__Service_Contract__c);
                    woProductSurMap.put(wo,woQueryMap.get(wo.id).SVMXC__Product__r.Family);
                    countries.add(wo.SVMXC__Country__c);
                }
            }
        }
        
        map<id,SVMXC__Service_Contract__c> productFamily = new map<id,SVMXC__Service_Contract__c>();
         list<SVMX_Sales_Office__c> salesOfficeList = new List<SVMX_Sales_Office__c>();
        
        if(!scIds.isEmpty())
            productFamily = SVMX_ServiceContractDataManager.rateTierQuery(scIds);
            
        
        map<id,list<SVMX_Product_Surcharge__c>> productFamilyMap = new map<id,list<SVMX_Product_Surcharge__c>>();
        for(SVMXC__Service_Contract__c sc: productFamily.values()){
            if(sc.Product_Surcharges__r.size()>0)
                productFamilyMap.put(sc.id,sc.Product_Surcharges__r);
        }
        
        List<SVMXC__Service_Order_Line__c> wdList = new List<SVMXC__Service_Order_Line__c> ();
        if(countries.size() > 0)
        {
            List<String> countriesList = new List<String>();
            countriesList.addAll(countries);
         salesOfficeList=SVMX_SalesOfficeDataManager.countrySalesOfficeQuery(countriesList);
     }
         for(SVMX_Sales_Office__c sof: salesOfficeList){
                    system.debug('sof@@@@'+sof);
                    if(!countrySalesOffMap.containskey(sof.SVMX_Country__c)){
                             system.debug('sof1'+sof.SVMX_Country__c);
                             countrySalesOffMap.put(sof.SVMX_Country__c,sof);
                        }
              }
        if(!woPrioritySurList.isEmpty()){ 
            for(SVMXC__Service_Order__c wo : woPrioritySurList){
                if(woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Priority_Surcharge__c.contains('One Time')){
                    SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
                    wd.RecordTypeId = usageRecordTypeId;
                    wd.SVMXC__Service_Order__c = wo.id;
                    wd.SVMXC__Actual_Quantity2__c = 1;
                    wd.SVMXC__Line_Type__c = Expenses;
                    wd.SVMXC__Expense_Type__c = PrioritySurcharge;
                    wd.CurrencyIsoCode = wo.CurrencyIsoCode;
                    if(countrySalesOffMap.containskey(wo.SVMXC__Country__c)){
                        wd.SVMX_SAP_Product__c =  countrySalesOffMap.get(wo.SVMXC__Country__c).SVMX_Expense_Product__c;
                        wd.SVMX_SAP_Material_Number__c=countrySalesOffMap.get(wo.SVMXC__Country__c).SVMX_Expense_Product__r.SAPNumber__c;
                     }
                    wdList.add(wd);
                }
                if(woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Priority_Surcharge__c.contains('Labor')){
                    SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
                    wd.RecordTypeId = usageRecordTypeId;
                    wd.SVMXC__Service_Order__c = wo.id;
                    wd.SVMXC__Actual_Quantity2__c = 1;
                    wd.SVMXC__Line_Type__c = Labor;
                    wd.SVMXC__Activity_Type__c = PrioritySurcharge;
                    wd.CurrencyIsoCode = wo.CurrencyIsoCode;
                    wd.SVMX_Country__c = wo.SVMXC__Country__c;
                     if(countrySalesOffMap.containskey(wo.SVMXC__Country__c)){
                        wd.SVMX_SAP_Product__c =  countrySalesOffMap.get(wo.SVMXC__Country__c).SVMX_Labor_Product__c;
                        wd.SVMX_SAP_Material_Number__c=countrySalesOffMap.get(wo.SVMXC__Country__c).SVMX_Labor_Product__r.SAPNumber__c;
                     }
                    wdList.add(wd);
                } 
            }
        }
        
        if(!woProductSurMap.isEmpty() && !productFamilyMap.isEmpty()){
            for(SVMXC__Service_Order__c wo : woProductSurMap.keyset()){
                for(SVMX_Product_Surcharge__c prodSur : productFamilyMap.get(wo.SVMXC__Service_Contract__c)) {
                    system.debug('wo product family '+woProductSurMap.get(wo));
                    system.debug('sc product family '+prodSur.SVMX_Product_Family__c);
                    if(woProductSurMap.get(wo) == prodSur.SVMX_Product_Family__c) {
                        if(woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Product_Surcharge_Type__c.contains('One Time')){ 
                            SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
                            wd.RecordTypeId = usageRecordTypeId;
                            wd.SVMXC__Service_Order__c = wo.id;
                            wd.SVMXC__Actual_Quantity2__c = 1;
                            wd.SVMXC__Line_Type__c = Expenses;
                            wd.SVMXC__Expense_Type__c = ProductSurcharge;
                            wd.CurrencyIsoCode = wo.CurrencyIsoCode;
                            if(countrySalesOffMap.containskey(wo.SVMXC__Country__c)){
                                 wd.SVMX_SAP_Product__c =  countrySalesOffMap.get(wo.SVMXC__Country__c).SVMX_Expense_Product__c;
                                wd.SVMX_SAP_Material_Number__c=countrySalesOffMap.get(wo.SVMXC__Country__c).SVMX_Expense_Product__r.SAPNumber__c;
                             }
                            wdList.add(wd);
                        }
                        if(woQueryMap.get(wo.id).SVMXC__Service_Contract__r.SVMX_Product_Surcharge_Type__c.contains('Labor')){
                            SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
                            wd.RecordTypeId = usageRecordTypeId;
                            wd.SVMXC__Service_Order__c = wo.id;
                            wd.SVMXC__Actual_Quantity2__c = 1;
                            //wd.SVMX_Surcharge__c = true;
                            wd.SVMXC__Line_Type__c = Labor;
                            wd.SVMXC__Activity_Type__c = ProductSurcharge;
                            wd.CurrencyIsoCode = wo.CurrencyIsoCode;
                            wd.SVMX_Country__c = wo.SVMXC__Country__c;
                             if(countrySalesOffMap.containskey(wo.SVMXC__Country__c)){
                                wd.SVMX_SAP_Product__c =  countrySalesOffMap.get(wo.SVMXC__Country__c).SVMX_Labor_Product__c;
                                wd.SVMX_SAP_Material_Number__c=countrySalesOffMap.get(wo.SVMXC__Country__c).SVMX_Labor_Product__r.SAPNumber__c;
                             }
                            wdList.add(wd);
                        } 
                    }
                }
            }
        }
            
        if(!wdList.isEmpty())
            SVMX_WorkDetailDataManager.InsWorkDetails(wdList);
    } 
    
    public static List<case> caseReadytoInvoice(list<SVMXC__Service_Order__c> woList){
        
        List<case> caseList = new List<case>();
        Map<id,List<SVMXC__Service_Order__c>> caseWOMap = new Map<id,List<SVMXC__Service_Order__c>>();
        integer count1=0, count2=0;
        set<Id> caseIds = new set<Id>();

          for(SVMXC__Service_Order__c wo: woList)
            caseIds.add(wo.SVMXC__Case__c);

        caseWOMap = SVMX_WorkOrderDataManager.caseWOQuery(caseIds);
        for(id i: caseWOMap.keyset()){
            String accbillingType = null;
            List<SVMXC__Service_Order__c> CasewoList = new List<SVMXC__Service_Order__c>();
            CasewoList = caseWOMap.get(i);
            for(SVMXC__Service_Order__c wo: CasewoList){
                if((wo.SVMXC__Order_Status__c  == system.label.Ready_To_Bill || wo.SVMXC__Order_Status__c == system.label.Canceled ) && (!wo.SVMX_Revisit_Required__c || (wo.SVMX_Revisit_Required__c && wo.SVMX_Revisit_Work_Order_Created__c))){
                    count1++;
                    accbillingType = wo.SVMXC__Company__r.SVMX_Automatic_billing__c;
                }
                /*else{
                    count2++;
                }*/
            }
            //if(count2 == 0 && count1 != 0){
            if(count1 != 0 && CasewoList.size() == count1){
                case cs = new case();
                cs.id = i;
                cs.Status = readyToBill;
                if(accbillingType == 'Automatic')
                    cs.SVMX_Invoice_Status__c ='Requested';
                caseList.add(cs);
            }
        }
        return caseList;
    }

    //Close Work Order and Case
     public static List<case> caseReadytoClose(list<SVMXC__Service_Order__c> woList){
        
        List<case> caseList = new List<case>();
        Map<id,List<SVMXC__Service_Order__c>> caseWOMap = new Map<id,List<SVMXC__Service_Order__c>>();
        integer count1=0, count2=0;
         set<Id> caseIds = new set<Id>();

          for(SVMXC__Service_Order__c wo: woList)
            caseIds.add(wo.SVMXC__Case__c);

        caseWOMap = SVMX_WorkOrderDataManager.caseWOQuery(caseIds);
        for(id i: caseWOMap.keyset()){
            String accbillingType = null;
            List<SVMXC__Service_Order__c> CasewoList = new List<SVMXC__Service_Order__c>();
            CasewoList = caseWOMap.get(i);
            for(SVMXC__Service_Order__c wo:CasewoList ){
                if((wo.SVMXC__Order_Status__c  == system.label.Closed || wo.SVMXC__Order_Status__c == system.label.Canceled ) && (!wo.SVMX_Revisit_Required__c || (wo.SVMX_Revisit_Required__c && wo.SVMX_Revisit_Work_Order_Created__c))){
                    count1++;
                    
                }
                /*else{
                    count2++;
                }*/
            }
           //if(count2 == 0 && count1 != 0){
            if(count1 != 0 && CasewoList.size() == count1){
                case cs = new case();
                cs.id = i;
                cs.Status = system.label.Closed;
               
                caseList.add(cs);
            }
        }
        return caseList;
    }
    /* Called from afterInsert WO trigger. 
         * This method is used to set the work order's currency to it's account's currency */
    public static void createTaskForPPMWorkOrdersOnAssignment(list<SVMXC__Service_Order__c> ppmWoList){
        //SVMX_DefinitionClass defCls = new SVMX_DefinitionClass();
        Map<Id,Id> woIdAndPmSchIdMap = new Map<Id,Id>();
        Map<Id, SVMXC__Service_Order__c> workOrderQueryMap = SVMX_WorkOrderDataManager.woQuery(ppmWoList);
        Map<Id,Id> woIdAndTechIdMap = new Map<Id,Id>();
        Map<Id,Id> woIdAndWoPurposeIdMap = new Map<Id,Id>();
        //List<Id> woTechList = new List<Id>();
        for(SVMXC__Service_Order__c wo : workOrderQueryMap.values()){
            if(wo.SVMX_PM_Schedule_Definition__c != null)
                woIdAndPmSchIdMap.put(wo.Id, wo.SVMX_PM_Schedule_Definition__c);
            if(wo.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c != null)
                woIdAndTechIdMap.put(wo.Id, wo.SVMXC__Group_Member__r.SVMXC__Salesforce_User__c);
            if(wo.SVMX_PM_Schedule_Definition__r.SVMXC__Work_Order_Purpose__c != null && wo.SVMX_PM_Schedule_Definition__r.SVMX_Is_Present_WO_Task__c == true)
                woIdAndWoPurposeIdMap.put(wo.Id, wo.SVMX_PM_Schedule_Definition__r.SVMXC__Work_Order_Purpose__c);
                
        }
        system.debug('woIdAndPmSchIdMap-->'+woIdAndPmSchIdMap);
        system.debug('woIdAndWoPurposeIdMap-->'+woIdAndWoPurposeIdMap);
        if(!woIdAndPmSchIdMap.isEmpty() && !woIdAndWoPurposeIdMap.isEmpty()){
                List<Task> insertTaskList = new List<Task>();
                //List<SVMXC__Task_Template__c> ttListQuery = [Select id, SVMXC__Task_Title__c, SVMXC__Description__c, SVMXC__Task_Template__c, SVMXC__Priority__c from SVMXC__Task_Template__c where SVMXC__Task_Template__c IN :woIdAndWoPurposeIdMap.values()];
                List<SVMXC__Task_Template__c> ttListQuery = SVMX_WorkOrderDataManager.taskTemplateQuery(woIdAndWoPurposeIdMap);
            for(Id woId : woIdAndPmSchIdMap.keySet()){
                for(SVMXC__Task_Template__c tt : ttListQuery){
                    Id psdWOPurposeId = woIdAndWoPurposeIdMap.get(woId);
                    system.debug('psdWOPurposeId-->'+psdWOPurposeId);
                    system.debug('tt.SVMXC__Task_Template__c-->'+tt.SVMXC__Task_Template__c);
                    if(psdWOPurposeId == tt.SVMXC__Task_Template__c){
                        Task t = new Task();
                        t.whatId = woId;
                        t.OwnerId = woIdAndTechIdMap.get(woId);
                        t.subject = tt.SVMXC__Task_Title__c;
                        t.Description = tt.SVMXC__Description__c;
                        t.priority = tt.SVMXC__Priority__c;
                        t.Status = 'Open';
                        insertTaskList.add(t);
                    }
                }
            }
            system.debug('insertTaskList-->'+insertTaskList);
            if(insertTaskList.size() > 0)
                SVMX_WorkOrderDataManager.insertTask(insertTaskList);
            SVMX_DefinitionClass.stopTaskRecursion = true;
        }  
    }
    
    @future(callout=true)
    public static void addTravelLinefuture(List<Id> travelWOList, String country,String currencyWO,string language){
        addTravelLine(travelWOList, country,currencyWO, language);
    }
    
    public static void addTravelLine(List<Id> travelWOList, String country,String currencyWO,string language){
        set<String> countries = new set<string>();
         list<SVMX_Sales_Office__c> salesOfficeList = new List<SVMX_Sales_Office__c>();
         map<string,SVMX_Sales_Office__c> countrySalesOffMap = new map<string,SVMX_Sales_Office__c>();
         /*for(SVMXC__Service_Order__c wo: travelWOList)
            countries.add(wo.SVMXC__Country__c);*/

        if(country != null ){
            List<String> countriesList = new List<String>();
            countriesList.add(country);
            salesOfficeList=SVMX_SalesOfficeDataManager.countrySalesOfficeQuery(countriesList);
        }
         for(SVMX_Sales_Office__c sof: salesOfficeList){
                    system.debug('sof@@@@'+sof);
                    if(!countrySalesOffMap.containskey(sof.SVMX_Country__c)){
                             system.debug('sof1'+sof.SVMX_Country__c);
                             countrySalesOffMap.put(sof.SVMX_Country__c,sof);
                        }
        }
        List<SVMXC__Service_Order_Line__c> travelwdList = new List<SVMXC__Service_Order_Line__c>();
        for(Id wo: travelWOList){
            SVMXC__Service_Order_Line__c wd = new SVMXC__Service_Order_Line__c();
            wd.CurrencyIsoCode = currencyWO;
            wd.SVMX_Country__c = country;
            wd.SVMX_Language__c = language;
            wd.SVMXC__Line_Type__c = Travel;
            wd.SVMX_Travel_Type__c = System.Label.Zone_Distance;
            wd.RecordTypeId = usageRecordTypeId;
            wd.SVMXC__Service_Order__c = wo;
             if(countrySalesOffMap.containskey(country)){
                                wd.SVMX_SAP_Product__c =  countrySalesOffMap.get(country).SVMX_Travel_Distance_Product__c;
                                wd.SVMX_SAP_Material_Number__c=countrySalesOffMap.get(country).SVMX_Travel_Distance_Product__r.SAPNumber__c;
                             }
            travelwdList.add(wd);
        }
        if(travelwdList.size()> 0)
         SVMX_WorkDetailDataManager.InsWorkDetails(travelwdList);
        //return travelwdList;
    }
}