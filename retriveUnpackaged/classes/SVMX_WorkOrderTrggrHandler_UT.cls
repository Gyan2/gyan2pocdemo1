/*******************************************************************************************************
Description:
  This test class is used to test the SVMX_WorkOrderTrggrHandler class.
 
Author: Ranjitha S
Date: 16-10-2017

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

@isTest
public class SVMX_WorkOrderTrggrHandler_UT {
    
    
    static testMethod void invokeWOtrigger() {  

        Account acc = SVMX_TestUtility.CreateAccount('Default Account','Germany', true);
        Account acct = SVMX_TestUtility.CreateAccount('US account','United States', true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany  Location',acc.id,true);
        
       // Product2 prod = SVMX_TestUtility.CreateProduct('Door',true);
       
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,null,loc.id,true);
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Germany',true);
 
       // SVMXC__Warranty__c warr = SVMX_TestUtility.CreateWarranty(IB.id,true);
        
       // SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
        
        Case cs = new Case();
        cs.Status ='New';
        cs.Priority = 'Medium';
        cs.Origin = 'Email';  
        cs.CurrencyIsoCode = 'EUR';
        cs.AccountId=acct.Id; 
        //cs.SVMXC__Warranty__c = warr.id;
        insert cs;
  
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c();
        sc.RecordTypeId = Schema.SObjectType.SVMXC__Service_Contract__c.getRecordTypeInfosByName().get('Default').getRecordTypeId();
        sc.Name = 'Test Contract';
        sc.SVMXC__Company__c = acc.id;
        sc.SVMXC__Start_Date__c = system.today();
        sc.SVMXC__End_Date__c = system.today().addMonths(7);
        sc.SVMX_Priority__c = 'Critical';
        sc.SVMX_Priority_Surcharge__c = 'Labor';
        sc.SVMXC__Active__c=true;
        insert sc; 
                  
        SVMXC__Service_Order__c wo = new SVMXC__Service_Order__c();
        wo.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        wo.SVMXC__Company__c = acct.Id;
        wo.SVMXC__Order_Status__c = 'Open';
        wo.SVMXC__Order_Type__c = 'Reactive';
        wo.SVMXC__Street__c = '16260 Monterey St.';
        wo.SVMXC__City__c = 'Morgan Hill';
        wo.SVMXC__State__c = 'California';
        wo.SVMXC__Zip__c = '95037';
        wo.SVMXC__Country__c = 'Germany';
        wo.SVMXC__Site__c = loc.Id;
        wo.SVMXC__Auto_Entitlement_Status__c = 'Failed';
        wo.SVMXC__Priority__c = 'Medium';
        wo.SVMXC__Case__c = cs.Id; 
        
        SVMXC__Service_Order__c wo1 = new SVMXC__Service_Order__c();
        wo1.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        wo1.SVMXC__Company__c = acc.Id;
        wo1.SVMXC__Order_Status__c = 'Open';
        wo1.SVMXC__Order_Type__c = 'Reactive';
        wo1.SVMXC__Street__c = '16260 Monterey St.';
        wo1.SVMXC__City__c = 'Morgan Hill';
        wo1.SVMXC__State__c = 'California';
        wo1.SVMXC__Zip__c = '95037';
        wo1.SVMXC__Country__c = 'Germany';
        wo1.SVMXC__Site__c = loc.Id;
        wo1.SVMXC__Service_Contract__c = sc.id;
        wo1.SVMXC__Priority__c = 'Medium';
        wo1.SVMXC__Case__c = cs.Id; 
        
        test.startTest();
        insert wo;
        insert wo1;
        wo.SVMXC__Order_Status__c = system.label.Ready_To_Bill;
        wo1.SVMXC__Order_Status__c = system.label.Ready_To_Bill;
        update wo;
        update wo1;
        wo.SVMXC__Order_Status__c = system.label.Work_Complete;
        update wo;
        wo.SVMXC__Order_Status__c = System.label.Closed;
        wo1.SVMXC__Order_Status__c = System.label.Closed;
        update wo;
        update wo1;
        test.stopTest(); 
        
    }   
    
    static testMethod void priorityMethodtest(){
        
        Account acc = SVMX_TestUtility.CreateAccount('Default Account','Germany', true);
        Account acct = SVMX_TestUtility.CreateAccount('US account','United States', true);
        Account acc2 = SVMX_TestUtility.CreateAccount('UK account','United Kingdom', true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acct.id,true);
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
        Product2 prod = SVMX_TestUtility.CreateProduct('Door',true);
        
        Product2 exProd = new Product2(Name = 'Expense Prod', SAPNumber__c='1287236');
        Product2 LoProd = new Product2(Name = 'Labor Prod', SAPNumber__c='1287237');
        Product2 TravProd = new Product2(Name = 'Travel Prod', SAPNumber__c='1287238');
        
         List<product2> prods = new List<Product2>();
         prods.add(exProd);
         prods.add(LoProd);
         prods.add(TravProd);
         
         SVMX_Sales_Office__c so = SVMX_TestUtility.createSalesOffice('Germany', exProd.id, LoProd.id, TravProd.id, true );   
     
        
        SVMXC__Service_Contract__c sc1 = new SVMXC__Service_Contract__c();
        sc1.RecordTypeId = Schema.SObjectType.SVMXC__Service_Contract__c.getRecordTypeInfosByName().get('Default').getRecordTypeId();
        sc1.Name = 'Test Contract';
        sc1.SVMXC__Company__c = acc2.id;
        sc1.SVMXC__Start_Date__c = system.today();
        sc1.SVMXC__End_Date__c = system.today().addMonths(7);
        sc1.SVMX_Priority__c = 'Critical';
        sc1.SVMX_Priority_Surcharge__c = 'One Time;Labor';
        insert sc1; 
        
        SVMXC__Service_Contract__c sc2 = new SVMXC__Service_Contract__c();
        sc2.RecordTypeId = Schema.SObjectType.SVMXC__Service_Contract__c.getRecordTypeInfosByName().get('Default').getRecordTypeId();
        sc2.Name = 'Test Contract';
        sc2.SVMXC__Company__c = acc.id;
        sc2.SVMX_Product_Surcharge__c = true;
        sc2.SVMX_Product_Surcharge_Type__c = 'One Time;Labor';
        sc2.SVMXC__Start_Date__c = system.today();
        sc2.SVMXC__End_Date__c = system.today().addMonths(7);
        sc2.SVMX_Priority__c = 'Critical';
        sc2.SVMX_Priority_Surcharge__c = 'One Time;Labor';
        insert sc2; 
        
        SVMXC__Service_Contract__c sc3 = new SVMXC__Service_Contract__c();
        sc3.RecordTypeId = Schema.SObjectType.SVMXC__Service_Contract__c.getRecordTypeInfosByName().get('Default').getRecordTypeId();
        sc3.Name = 'Test Contract';
        sc3.SVMXC__Company__c = acct.id;
        sc3.SVMX_Product_Surcharge__c = true;
        sc2.SVMX_Product_Surcharge_Type__c = 'One Time';
        sc3.SVMXC__Start_Date__c = system.today();
        sc3.SVMXC__End_Date__c = system.today().addMonths(7);
        sc3.SVMX_Priority__c = 'Medium';
        sc3.SVMX_Priority_Surcharge__c = 'Labor';
        insert sc3; 
        
        SVMX_Product_Surcharge__c ps = new SVMX_Product_Surcharge__c();
        ps.SVMX_Contract__c = sc2.id;
        ps.SVMX_Product_Family__c = 'Product Line';
        insert ps; 
        
        SVMX_Product_Surcharge__c ps1 = new SVMX_Product_Surcharge__c();
        ps1.SVMX_Contract__c = sc3.id;
        ps1.SVMX_Product_Family__c = 'Product Line';
        insert ps1;
        
        SVMXC__Service_Order__c wo2 = new SVMXC__Service_Order__c();
        wo2.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        wo2.SVMXC__Company__c = acc.Id;
        wo2.SVMXC__Order_Status__c = 'Open';
        wo2.SVMXC__Order_Type__c = 'Reactive';
        wo2.SVMXC__Street__c = '16260 Monterey St.';
        wo2.SVMXC__City__c = 'Morgan Hill';
        wo2.SVMXC__State__c = 'California';
        wo2.SVMXC__Zip__c = '95037';
        wo2.SVMXC__Country__c = 'Germany';
        wo2.SVMXC__Site__c = loc.Id;
        wo2.SVMXC__Service_Contract__c = sc2.id;
        wo2.SVMXC__Priority__c = 'Critical';
        wo2.SVMXC__Product__c = prod.id;
        
        SVMXC__Service_Order__c wo3 = new SVMXC__Service_Order__c();
        wo3.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        wo3.SVMXC__Company__c = acc.Id;
        wo3.SVMXC__Order_Status__c = 'Open';
        wo3.SVMXC__Order_Type__c = 'Reactive';
        wo3.SVMXC__Street__c = '16260 Monterey St.';
        wo3.SVMXC__City__c = 'Morgan Hill';
        wo3.SVMXC__State__c = 'California';
        wo3.SVMXC__Zip__c = '95037';
        wo3.SVMXC__Country__c = 'Germany';
        wo3.SVMXC__Site__c = loc.Id;
        wo3.SVMXC__Service_Contract__c = sc3.id;
        wo3.SVMXC__Priority__c = 'Medium';
        wo3.SVMXC__Product__c = prod.id;
        
        SVMXC__Service_Order__c wo4 = new SVMXC__Service_Order__c();
        wo4.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        wo4.SVMXC__Company__c = acc.Id;
        wo4.SVMXC__Order_Status__c = 'Open';
        wo4.SVMXC__Order_Type__c = 'Reactive';
        wo4.SVMXC__Street__c = '16260 Monterey St.';
        wo4.SVMXC__City__c = 'Morgan Hill';
        wo4.SVMXC__State__c = 'California';
        wo4.SVMXC__Zip__c = '95037';
        wo4.SVMXC__Country__c = 'Germany';
        wo4.SVMXC__Site__c = loc.Id;
        wo4.SVMXC__Service_Contract__c = sc1.id;
        wo4.SVMXC__Priority__c = 'Critical';     
        
        List<SVMXC__Service_Order__c> woList = new List<SVMXC__Service_Order__c>();
        woList.add(wo2);
        woList.add(wo3);
        woList.add(wo4);
        
        test.startTest();
        insert woList;


        test.stopTest();  
        
    } 
    @isTest(seealldata = true)
   static void testcreateTaskForPPMWorkOrdersOnAssignment(){
   
        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'WorkOrder';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
                 try{
            insert mycs;
            }catch(Exception e){}
        
        SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
              intg.name= 'SAP PO';
              intg.WS_USER__c ='test';
              intg.WS_PASS__c ='test';
              try{
        insert intg;
        }catch(Exception e){}

        List<SVMX_Interface_Trigger_Configuration__c> trigcon1 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Order'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon2 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Details'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon3 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order Line-Spare Parts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon4 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order -Spare Parts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon5 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order Line'];

      if(trigcon1.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
       } 
      if(trigcon2.size() == 0){  
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;
       }
        
       if(trigcon3.size() == 0){ 
        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Parts Order Line-Spare Parts';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig2;
      }

      if(trigcon4.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig3=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig3.name='trig1';
        trigconfig3.SVMX_Country__c='germany';
        trigconfig3.SVMX_Object__c='Parts Order -Spare Parts';
        trigconfig3.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig3;
      }
      if(trigcon5.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig4=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig4.name='trig1';
        trigconfig4.SVMX_Country__c='germany';
        trigconfig4.SVMX_Object__c='Parts Order Line';
        trigconfig4.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig4;
      }

        
        Account acct = SVMX_TestUtility.CreateAccount('US account','United States', true);
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('US Location',acct.id,true);
        SVMX_Interface_Trigger_Configuration__c inTrigConfig=SVMX_TestUtility.CreateInterfaceTrigger('Work Order',true);
        Product2 prod = SVMX_TestUtility.CreateProduct('Door',true);
        
        Profile prof = SVMX_TestUtility.SelectProfile('SVMX - Global Technician');
        User tech2 = SVMX_TestUtility.CreateUser('Testtech', prof.id, true);
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('Svmx Tech2', tech2.id, null, true);
       
        SVMXC__Service_Contract__c sc1 = new SVMXC__Service_Contract__c();
        sc1.RecordTypeId = Schema.SObjectType.SVMXC__Service_Contract__c.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        sc1.Name = 'Test Contract';
        sc1.SVMXC__Company__c = acct.id;
        sc1.SVMXC__Start_Date__c = system.today();
        sc1.SVMXC__End_Date__c = system.today().addMonths(7);
        sc1.SVMX_Priority__c = 'Critical';
        sc1.SVMX_Priority_Surcharge__c = 'One Time';
        insert sc1; 
        
        SVMXC__Task_Template__c  tt = new SVMXC__Task_Template__c ();
        tt.SVMXC__Priority__c = 'Low';
        insert tt;
       
        SVMXC__Task_Template__c  tt2 = new SVMXC__Task_Template__c ();
        tt2.SVMXC__Priority__c = 'Low';
        tt2.SVMXC__Task_Template__c = tt.id;
        insert tt2;
       
        SVMXC__PM_Plan__c pm = SVMX_TestUtility.CreatePMPlan(sc1.Id, sc1.Name, true);
        SVMXC__PM_Schedule_Definition__c pmsche = SVMX_TestUtility.CreatePMDefinition(pm.id, true);
        pmsche.SVMXC__Work_Order_Purpose__c = tt2.SVMXC__Task_Template__c;
        update pmsche;
            
        SVMXC__Service_Order__c wo2 = new SVMXC__Service_Order__c();
        wo2.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        wo2.SVMXC__Company__c = acct.Id;
        wo2.SVMXC__Order_Status__c = 'Open';
        wo2.SVMXC__Order_Type__c = System.Label.Planned_Services_PPM;
        wo2.SVMXC__Street__c = '16260 Monterey St.';
        wo2.SVMXC__City__c = 'Morgan Hill';
        wo2.SVMXC__State__c = 'California';
        wo2.SVMXC__Zip__c = '95037';
        wo2.SVMXC__Country__c = 'United States';
        wo2.SVMXC__Site__c = loc.Id;
        wo2.SVMXC__Service_Contract__c = sc1.id;
        wo2.SVMXC__Priority__c = 'Critical';
        wo2.SVMXC__Product__c = prod.id;
        wo2.SVMX_Task_Present__c = true;
        wo2.SVMX_PM_Schedule_Definition__c = pmsche.id;
        insert wo2;

        SVMXC__Service_Order__c wo3 = new SVMXC__Service_Order__c();
        wo3.RecordTypeId = Schema.SObjectType.SVMXC__Service_Order__c.getRecordTypeInfosByName().get('Global Std').getRecordTypeId();
        wo3.SVMXC__Company__c = acct.Id;
        wo3.SVMXC__Order_Status__c = 'Open';
        wo3.SVMXC__Order_Type__c = System.Label.Planned_Services_PPM;
        wo3.SVMXC__Street__c = '16260 Monterey St.';
        wo3.SVMXC__City__c = 'Morgan Hill';
        wo3.SVMXC__State__c = 'California';
        wo3.SVMXC__Zip__c = '95037';
        wo3.SVMXC__Country__c = 'United States';
        wo3.SVMXC__Site__c = loc.Id;
        wo3.SVMXC__Service_Contract__c = sc1.id;
        wo3.SVMXC__Priority__c = 'Critical';
        wo3.SVMXC__Product__c = prod.id;
        wo3.SVMX_Task_Present__c = true;
        wo3.SVMX_PM_Schedule_Definition__c = pmsche.id;
        insert wo3;

        SVMXC__RMA_Shipment_Order__c partsOrder1=new SVMXC__RMA_Shipment_Order__c();
          partsOrder1.SVMXC__Company__c=acct.id;
          //partsOrder1.SVMXC__Contact__c=con.id;
          partsOrder1.SVMXC__Service_Order__c = wo3.id;
          partsOrder1.SVMXC__Order_Status__c='open';
          partsOrder1.SVMX_Service_Sales_Order_Number__c = '123454';
          partsOrder1.SVMX_Awaiting_SAP_Response__c = false;
          partsOrder1.SVMXC__Expected_Receive_Date__c=Date.Today();
          insert partsOrder1;

        SVMXC__RMA_Shipment_Line__c partsline1=new SVMXC__RMA_Shipment_Line__c();
          //partsline1.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('RMA').getRecordTypeId();
          partsline1.SVMXC__RMA_Shipment_Order__c=partsOrder1.id;
          //partsline1.SVMXC__Product__c=prod.id;
          partsline1.SVMXC__Expected_Quantity2__c=1;
          partsline1.SVMXC__Line_Status__c='open';
          //partsline1.SVMX_Awaiting_SAP_Response__c =true;
          partsline1.SVMXC__Expected_Receipt_Date__c=Date.Today();
          partsline1.SVMXC__Expected_Condition__c='Defective';
          //partsline1.SVMX_Item_Number_Blank_Check__c = true;
          partsline1.SVMXC__Disposition__c='Repair';

          insert partsline1;  
       
        test.startTest();
        
        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder salesOrderreq=new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt.UpdateSync(messageheader,salesOrderreq);
        wo2.SVMXC__Group_Member__c = tech.id;
        update wo2;
        wo3.SVMXC__Order_Status__c ='Ready to Invoice';
        update wo3;
        test.stopTest();
 
   } 
}