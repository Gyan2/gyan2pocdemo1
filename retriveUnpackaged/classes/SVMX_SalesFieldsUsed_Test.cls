/**
 * Created by alcocese on 24-Apr-18.
 * The goal of this class is to add a field dependency on the fields that ServiceMax is using that do not have a prefix (common fields between Sales and Service)
 *      by doing so, if there is an attempt to rename the api name of any of them, this class will show up (because fields used on SFMs and similar do not show up)
 *      and whoever is renaming it, will know that there is a dependency out there.
 */

@IsTest
public with sharing class SVMX_SalesFieldsUsed_Test {
    @TestSetup public static void setup() {
        System.debug(
            [
                SELECT
                    Country_Map__c
                FROM Account
                LIMIT 1
            ]
        );
        System.debug(
            [
                SELECT
                        Status__c
                FROM Contact
                LIMIT 1
            ]
        );

    }
}