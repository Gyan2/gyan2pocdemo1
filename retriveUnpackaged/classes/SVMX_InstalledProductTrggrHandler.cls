/*******************************************************************************************************
Description: Trigger handler for Installed Product Trigger

Dependancy: 
 
Author: Ranjitha S
Date: 23-03-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------
*******************************************************************************************************/

public with sharing class SVMX_InstalledProductTrggrHandler extends SVMX_TriggerHandler {

    private list<SVMXC__Installed_Product__c> newIBList;
    private list<SVMXC__Installed_Product__c> oldIBList;
    private Map<Id, SVMXC__Installed_Product__c> newIBMap;
    private Map<Id, SVMXC__Installed_Product__c> oldIBMap;
     
    public SVMX_InstalledProductTrggrHandler() {
        this.newIBList = (list<SVMXC__Installed_Product__c>) Trigger.new;
        this.oldIBList = (list<SVMXC__Installed_Product__c>) Trigger.old;
        this.newIBMap = (Map<Id, SVMXC__Installed_Product__c>) Trigger.newMap;
        this.oldIBMap = (Map<Id, SVMXC__Installed_Product__c>) Trigger.oldMap;
    }

    public override void beforeInsert(){
        List<SVMXC__Installed_Product__c> ibList = new List<SVMXC__Installed_Product__c>();
        Map<id,SVMXC__Installed_Product__c> IPmap = new Map<id,SVMXC__Installed_Product__c>();
        
        for(SVMXC__Installed_Product__c ib : newIBList ){
            if(ib.Project__c != null){ 
                ibList.add(ib);
            }
            else if(ib.SVMXC__Date_Installed__c != null){ 
                ib.SVMXC_Warranty_Trigger_Date__c = ib.SVMXC__Date_Installed__c;
            }
        }
        
        if(!ibList.isEmpty())
            IPmap = SVMX_InstalledproductDataManager.ProjectDateQuery(ibList);
        
        for(SVMXC__Installed_Product__c ib : ibList){
            ib.SVMXC_Warranty_Trigger_Date__c = IPmap.get(ib.id).Project__r.SVMX_Project_Delivered_on_Date__c;
        }
    }
    
    public override void beforeUpdate(){
        List<SVMXC__Installed_Product__c> ibList = new List<SVMXC__Installed_Product__c>();
        for(SVMXC__Installed_Product__c ib : newIBList ){
            if(ib.SVMXC__Date_Installed__c != null && ib.SVMXC__Date_Installed__c != oldIBMap.get(ib.id).SVMXC__Date_Installed__c){ 
                ib.SVMXC_Warranty_Trigger_Date__c = ib.SVMXC__Date_Installed__c;
            }
        }
    }    
}