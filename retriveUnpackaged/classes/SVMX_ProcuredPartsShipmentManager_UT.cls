@isTest(seealldata=true)
public class SVMX_ProcuredPartsShipmentManager_UT {
    @isTest  
    static  void invokeWorkDetailriggersalesOrderProcuredPartCreate(){

      list<SVMX_Integration_SAP_Endpoint__c> customSett = SVMX_Integration_SAP_Endpoint__c.getall().values();

      if(customSett.size() == 0){
        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'WorkOrder';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
            
             SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
              intg.name= 'SAP PO';
              intg.WS_USER__c ='test';
              intg.WS_PASS__c ='test';
        insert intg;
      }

      List<SVMX_Interface_Trigger_Configuration__c> trigcon1 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Order'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon2 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Details'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon3 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order Line-Spare Parts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon4 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order -Spare Parts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon5 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order Line-ProcuredParts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon6 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order-ProcuredParts'];

      if(trigcon1.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
       } 
      if(trigcon2.size() == 0){  
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;
       }
        
       if(trigcon3.size() == 0){ 
        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Parts Order Line-Spare Parts';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig2;
      }

      if(trigcon4.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig3=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig3.name='trig1';
        trigconfig3.SVMX_Country__c='germany';
        trigconfig3.SVMX_Object__c='Parts Order -Spare Parts';
        trigconfig3.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig3;
      }

      if(trigcon5.size() == 0){ 
        SVMX_Interface_Trigger_Configuration__c trigconfig4=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig4.name='trig1';
        trigconfig4.SVMX_Country__c='germany';
        trigconfig4.SVMX_Object__c='Parts Order Line-ProcuredParts';
        trigconfig4.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig4;
      }

      if(trigcon6.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig5=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig5.name='trig1';
        trigconfig5.SVMX_Country__c='germany';
        trigconfig5.SVMX_Object__c='Parts Order-ProcuredParts';
        trigconfig5.SVMX_Trigger_on_Field__c='SVMXC__Order_Status__c';
        insert trigconfig5;
      }
        
        List<SVMXC__RMA_Shipment_Line__c> partsOrderList=new List<SVMXC__RMA_Shipment_Line__c>();
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',false);
        acc.SAPNumber__c='687474';
        insert acc;
        
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id,true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
        Product2 prod1 = SVMX_TestUtility.CreateProduct('Door', true);
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);

        Case cs = SVMX_TestUtility.CreateCase(acc.Id,con.Id, 'New', false);
        cs.SVMX_Service_Sales_Order_Number__c ='';
        insert cs;

        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Reactive','Global Std',sc.Id,tech.Id,cs.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.Today();
        insert wo;
        SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Reactive','Global Std',sc.Id,tech.Id,cs.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.Today();
        insert wo1;

        SVMXC__Service_Order_Line__c woli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'parts','Warranty',false);
        woli1.SVMXC__Actual_Quantity2__c = 2 ;
        woli1.SVMX_Sales_Order_Item_Number__c='123'; 
        insert woli1;
        
        SVMXC__RMA_Shipment_Order__c partsOrder=new SVMXC__RMA_Shipment_Order__c();
        partsOrder.SVMXC__Company__c=acc.id;
        partsOrder.SVMXC__Contact__c=con.id;
        partsOrder.SVMXC__Service_Order__c = wo.id;
        partsOrder.SVMXC__Order_Status__c='open';
        partsOrder.SVMX_Service_Sales_Order_Number__c = '';
        partsOrder.SVMX_Awaiting_SAP_Response__c = false;
        partsOrder.SVMXC__Expected_Receive_Date__c=Date.Today();
        insert partsOrder;

        
        
        SVMXC__RMA_Shipment_Order__c partsOrder1=new SVMXC__RMA_Shipment_Order__c();
          partsOrder1.SVMXC__Company__c=acc.id;
          partsOrder1.SVMXC__Contact__c=con.id;
          partsOrder1.SVMXC__Service_Order__c = wo.id;
          partsOrder1.SVMXC__Order_Status__c='open';
          partsOrder1.SVMX_Service_Sales_Order_Number__c = '123454';
          partsOrder1.SVMX_Awaiting_SAP_Response__c = false;
          partsOrder1.SVMXC__Expected_Receive_Date__c=Date.Today();
          

          SVMXC__RMA_Shipment_Order__c partsOrder2=new SVMXC__RMA_Shipment_Order__c();
          partsOrder1.SVMXC__Company__c=acc.id;
          partsOrder1.SVMXC__Contact__c=con.id;
          partsOrder1.SVMXC__Service_Order__c = wo.id;
          partsOrder1.SVMXC__Order_Status__c='open';
          partsOrder1.SVMX_Service_Sales_Order_Number__c = '';
          partsOrder1.SVMX_Awaiting_SAP_Response__c = true;
          partsOrder1.SVMXC__Expected_Receive_Date__c=Date.Today();
        
        SVMXC__RMA_Shipment_Order__c partsOrder3=new SVMXC__RMA_Shipment_Order__c();
          partsOrder1.SVMXC__Company__c=acc.id;
          partsOrder1.SVMXC__Contact__c=con.id;
          partsOrder1.SVMXC__Service_Order__c = wo1.id;
          partsOrder1.SVMXC__Order_Status__c='open';
          partsOrder1.SVMX_Service_Sales_Order_Number__c = '';
          partsOrder1.SVMX_Awaiting_SAP_Response__c = true;
          partsOrder1.SVMXC__Expected_Receive_Date__c=Date.Today();
         // insert partsOrder3;

        

        SVMXC__RMA_Shipment_Line__c partsline=new SVMXC__RMA_Shipment_Line__c();
          partsline.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
          partsline.SVMXC__RMA_Shipment_Order__c=partsOrder.id;
          partsline.SVMXC__Product__c=prod.id;
          partsline.SVMXC__Line_Type__c ='Procured Part';
          partsline.SVMXC__Expected_Quantity2__c=1;
          partsline.SVMXC__Line_Status__c='open';
          partsline.SVMXC__Expected_Receipt_Date__c=Date.Today();
          partsline.SVMXC__Expected_Condition__c='Defective';
          partsline.SVMXC__Disposition__c='Repair';
        
        SVMXC__RMA_Shipment_Line__c partsline1=new SVMXC__RMA_Shipment_Line__c();
          partsline1.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
          partsline1.SVMXC__RMA_Shipment_Order__c=partsOrder1.id;
          partsline1.SVMXC__Product__c=prod.id;
          partsline.SVMXC__Line_Type__c ='Procured Part';
          partsline1.SVMXC__Expected_Quantity2__c=1;
          partsline1.SVMXC__Line_Status__c='open';
          partsline1.SVMXC__Expected_Receipt_Date__c=Date.Today();
          partsline1.SVMXC__Expected_Condition__c='Defective';
          partsline1.SVMXC__Disposition__c='Repair';

        SVMXC__RMA_Shipment_Line__c partsline2=new SVMXC__RMA_Shipment_Line__c();
          partsline2.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
          partsline2.SVMXC__RMA_Shipment_Order__c = partsOrder3.id;
          partsline2.SVMXC__Product__c=prod.id;
          partsline2.SVMXC__Line_Type__c ='Procured Part';
          partsline2.SVMXC__Expected_Quantity2__c=1;
          partsline2.SVMXC__Line_Status__c='open';
          partsline2.SVMXC__Expected_Receipt_Date__c=Date.Today();
          partsline2.SVMXC__Expected_Condition__c='Defective';
          partsline2.SVMXC__Disposition__c='Repair';  
        //insert partsline2;
        partsOrderList.add(partsline);
        //partsOrderList.add(partsline1);
        //partsOrderList.add(partsline2);
        
        test.startTest();
       
        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder salesOrderreq=new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt.CreateSync(messageheader,salesOrderreq);
        
        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader1=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder salesOrderreq1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt1.UpdateSync(messageheader1,salesOrderreq1);
        
        insert partsOrderList;
        
        //partsline1.SVMXC__Product__c=prod1.id;
        //partsline1.SVMXC__Line_Status__c = 'Processing';
        //partsline1.SVMX_Sales_Order_Item_Number__c='354352345';
        
        //update partsline1;
        //insert partsline1;

        //partsline1.SVMXC__Line_Status__c = 'Processing';
        //partsline1.SVMX_Sales_Order_Item_Number__c='354352345';
        //update partsline1;
        
        test.stopTest();
    }

    @isTest
    static  void invokeWorkDetailriggersalesOrderProcuredPartupdate(){
     list<SVMX_Integration_SAP_Endpoint__c> customSett = SVMX_Integration_SAP_Endpoint__c.getall().values();

      if(customSett.size() == 0){

          SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'WorkOrder';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
            
             SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
              intg.name= 'SAP PO';
              intg.WS_USER__c ='test';
              intg.WS_PASS__c ='test';
        insert intg;
      }
       List<SVMX_Interface_Trigger_Configuration__c> trigcon1 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Order'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon2 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Details'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon3 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order Line-Spare Parts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon4 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order -Spare Parts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon5 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order Line-ProcuredParts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon6 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order-ProcuredParts'];

      if(trigcon1.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
       } 
      if(trigcon2.size() == 0){  
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;
       }
        
       if(trigcon3.size() == 0){ 
        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Parts Order Line-Spare Parts';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig2;
      }

      if(trigcon4.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig3=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig3.name='trig1';
        trigconfig3.SVMX_Country__c='germany';
        trigconfig3.SVMX_Object__c='Parts Order -Spare Parts';
        trigconfig3.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig3;
      }

      if(trigcon5.size() == 0){ 
        SVMX_Interface_Trigger_Configuration__c trigconfig4=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig4.name='trig1';
        trigconfig4.SVMX_Country__c='germany';
        trigconfig4.SVMX_Object__c='Parts Order Line-ProcuredParts';
        trigconfig4.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig4;
      }

      if(trigcon6.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig5=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig5.name='trig1';
        trigconfig5.SVMX_Country__c='germany';
        trigconfig5.SVMX_Object__c='Parts Order-ProcuredParts';
        trigconfig5.SVMX_Trigger_on_Field__c='SVMXC__Order_Status__c';
        insert trigconfig5;
      }

        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id,true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
        Product2 prod1 = SVMX_TestUtility.CreateProduct('Door', true);
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);

        Case cs = SVMX_TestUtility.CreateCase(acc.Id,con.Id, 'New', false);
        cs.SVMX_Service_Sales_Order_Number__c ='';
        insert cs;

        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Reactive','Global Std',sc.Id,tech.Id,cs.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.Today();
        insert wo;

        SVMXC__Service_Order_Line__c woli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'parts','Warranty',false);
        woli1.SVMXC__Actual_Quantity2__c = 2 ;
        woli1.SVMX_Sales_Order_Item_Number__c='123'; 
        insert woli1;
        
        SVMXC__RMA_Shipment_Order__c partsOrder=new SVMXC__RMA_Shipment_Order__c();
        partsOrder.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
        partsOrder.SVMXC__Company__c=acc.id;
        partsOrder.SVMXC__Contact__c=con.id;
        partsOrder.SVMXC__Order_Type__c ='Procured Part';
        partsOrder.SVMXC__Service_Order__c = wo.id;
        partsOrder.SVMXC__Order_Status__c='open';
        partsOrder.SVMX_Service_Sales_Order_Number__c = '';
        partsOrder.SVMX_Awaiting_SAP_Response__c = false;
        partsOrder.SVMXC__Expected_Receive_Date__c=Date.Today();
        insert partsOrder;
        
        SVMXC__RMA_Shipment_Order__c partsOrder1=new SVMXC__RMA_Shipment_Order__c();
          partsOrder1.SVMXC__Company__c=acc.id;
          partsOrder1.SVMXC__Contact__c=con.id;
          partsOrder1.SVMXC__Service_Order__c = wo.id;
          partsOrder1.SVMXC__Order_Status__c='open';
          partsOrder1.SVMX_Service_Sales_Order_Number__c = '123454';
          partsOrder1.SVMX_Awaiting_SAP_Response__c = false;
          partsOrder1.SVMXC__Expected_Receive_Date__c=Date.Today();
          //insert partsOrder1;

          

        SVMXC__RMA_Shipment_Line__c partsline=new SVMXC__RMA_Shipment_Line__c();
          partsline.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
          partsline.SVMXC__RMA_Shipment_Order__c=partsOrder.id;
          partsline.SVMXC__Product__c=prod.id;
          partsline.SVMXC__Line_Type__c ='Procured Part';
          partsline.SVMXC__Expected_Quantity2__c=1;
          partsline.SVMXC__Line_Status__c='open';
          partsline.SVMXC__Expected_Receipt_Date__c=Date.Today();
          partsline.SVMXC__Expected_Condition__c='Defective';
          partsline.SVMXC__Disposition__c='Repair';
        //insert partsline;

        SVMXC__RMA_Shipment_Line__c partsline1=new SVMXC__RMA_Shipment_Line__c();
          partsline1.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
          partsline1.SVMXC__RMA_Shipment_Order__c=partsOrder.id;
          partsline1.SVMXC__Product__c=prod.id;      
          partsline1.SVMXC__Expected_Quantity2__c=1;
          partsline1.SVMXC__Line_Status__c='open';
          partsline1.SVMXC__Expected_Receipt_Date__c=Date.Today();
          partsline1.SVMXC__Expected_Condition__c='Defective';
          partsline1.SVMXC__Disposition__c='Repair';
          partsline1.SVMXC__Line_Type__c ='Procured Part';
        insert partsline1; 
        
        partsline1.SVMXC__Product__c=prod1.id;
        partsline1.SVMXC__Line_Status__c = 'Processing';
        partsline1.SVMX_Sales_Order_Item_Number__c='354352345';
        
        test.startTest();
        
        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder salesOrderreq=new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt.CreateSync(messageheader,salesOrderreq);

        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader1=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder salesOrderreq1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt1.UpdateSync(messageheader1,salesOrderreq1);
        
        
        
        update partsline1;
        
        partsOrder.SVMX_Service_Sales_Order_Number__c = '3232';
        partsOrder.SVMXC__Order_Status__c ='Closed';
        update partsOrder;
        
        test.stopTest();
    }
    
    @isTest  
    static  void invokeWorkDetailriggersalesOrderProcuredPartexistingCreate(){

       list<SVMX_Integration_SAP_Endpoint__c> customSett = SVMX_Integration_SAP_Endpoint__c.getall().values();

      if(customSett.size() == 0){
        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                     mycs.Name = 'WorkOrder';
                     mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
                insert mycs;
                
                 SVMX_SAP_Integration_UserPass__c intg = new SVMX_SAP_Integration_UserPass__c();
                  intg.name= 'SAP PO';
                  intg.WS_USER__c ='test';
                  intg.WS_PASS__c ='test';
            insert intg;

      }
      List<SVMX_Interface_Trigger_Configuration__c> trigcon1 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Order'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon2 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Work Details'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon3 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order Line-Spare Parts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon4 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order -Spare Parts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon5 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order Line-ProcuredParts'];
      List<SVMX_Interface_Trigger_Configuration__c> trigcon6 =[select id,name,SVMX_Country__c,SVMX_Object__c,SVMX_Trigger_on_Field__c from SVMX_Interface_Trigger_Configuration__c where SVMX_Object__c ='Parts Order-ProcuredParts'];

      if(trigcon1.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig1=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig1.name='trig2';
        trigconfig1.SVMX_Country__c='germany';
        trigconfig1.SVMX_Object__c='Work Order';
        trigconfig1.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c';
        insert trigconfig1;
       } 
      if(trigcon2.size() == 0){  
        SVMX_Interface_Trigger_Configuration__c trigconfig=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig.name='trig1';
        trigconfig.SVMX_Country__c='germany';
        trigconfig.SVMX_Object__c='Work Details';
        trigconfig.SVMX_Trigger_on_Field__c='SVMXC__Product__c';
        insert trigconfig;
       }
        
       if(trigcon3.size() == 0){ 
        SVMX_Interface_Trigger_Configuration__c trigconfig2=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig2.name='trig1';
        trigconfig2.SVMX_Country__c='germany';
        trigconfig2.SVMX_Object__c='Parts Order Line-Spare Parts';
        trigconfig2.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig2;
      }

      if(trigcon4.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig3=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig3.name='trig1';
        trigconfig3.SVMX_Country__c='germany';
        trigconfig3.SVMX_Object__c='Parts Order -Spare Parts';
        trigconfig3.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig3;
      }

      if(trigcon5.size() == 0){ 
        SVMX_Interface_Trigger_Configuration__c trigconfig4=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig4.name='trig1';
        trigconfig4.SVMX_Country__c='germany';
        trigconfig4.SVMX_Object__c='Parts Order Line-ProcuredParts';
        trigconfig4.SVMX_Trigger_on_Field__c='SVMXC__Line_Status__c';
        insert trigconfig4;
      }

      if(trigcon6.size() == 0){
        SVMX_Interface_Trigger_Configuration__c trigconfig5=new SVMX_Interface_Trigger_Configuration__c();
        trigconfig5.name='trig1';
        trigconfig5.SVMX_Country__c='germany';
        trigconfig5.SVMX_Object__c='Parts Order-ProcuredParts';
        trigconfig5.SVMX_Trigger_on_Field__c='SVMXC__Order_Status__c';
        insert trigconfig5;
      }

        List<SVMXC__RMA_Shipment_Line__c> partsOrderList=new List<SVMXC__RMA_Shipment_Line__c>();
        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',false);
        acc.SAPNumber__c='687474';
        insert acc;
        
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id,true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers', true);
        Product2 prod1 = SVMX_TestUtility.CreateProduct('Door', true);
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);

        Case cs = SVMX_TestUtility.CreateCase(acc.Id,con.Id, 'New', false);
        cs.SVMX_Service_Sales_Order_Number__c ='';
        insert cs;

        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Reactive','Global Std',sc.Id,tech.Id,cs.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.Today();
        insert wo;
        SVMXC__Service_Order__c wo1 = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Reactive','Global Std',sc.Id,tech.Id,cs.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.Today();
        insert wo1;

        SVMXC__Service_Order_Line__c woli1 = SVMX_TestUtility.CreateWorkOrderDetail(acc.Id,loc.Id, prod.Id, 'Usage/Consumption', wo.Id, system.now(), sc.Id, tech.Id, cs.id,'parts','Warranty',false);
        woli1.SVMXC__Actual_Quantity2__c = 2 ;
        woli1.SVMX_Sales_Order_Item_Number__c='123'; 
        insert woli1;
        
          

          SVMXC__RMA_Shipment_Order__c partsOrder2=new SVMXC__RMA_Shipment_Order__c();
        partsOrder2.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Order__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
          partsOrder2.SVMXC__Company__c=acc.id;
          partsOrder2.SVMXC__Contact__c=con.id;
          partsOrder2.SVMXC__Service_Order__c = wo.id;
          partsOrder2.SVMXC__Order_Type__c ='Procured Part';
          partsOrder2.SVMXC__Order_Status__c='open';
          partsOrder2.SVMX_Service_Sales_Order_Number__c = '';
          partsOrder2.SVMX_Awaiting_SAP_Response__c = true;
          partsOrder2.SVMX_Sales_Order_Number_Blank_Check__c = false;
          partsOrder2.SVMXC__Expected_Receive_Date__c=Date.Today();
          insert partsOrder2;
        

        

        SVMXC__RMA_Shipment_Line__c partsline=new SVMXC__RMA_Shipment_Line__c();
          partsline.RecordTypeId = Schema.SObjectType.SVMXC__RMA_Shipment_Line__c.getRecordTypeInfosByName().get('Shipment').getRecordTypeId();
          partsline.SVMXC__RMA_Shipment_Order__c=partsOrder2.id;
          partsline.SVMXC__Product__c=prod.id;
          partsline.SVMXC__Line_Type__c ='Procured Part';
          partsline.SVMXC__Expected_Quantity2__c=1;
          partsline.SVMXC__Line_Status__c='open';
          partsline.SVMXC__Expected_Receipt_Date__c=Date.Today();
          partsline.SVMXC__Expected_Condition__c='Defective';
          partsline.SVMXC__Disposition__c='Repair';
          partsline.SVMX_Item_Number_Blank_Check__c = true;
         
              
        test.startTest();
       
        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder salesOrderreq=new SVMX_wwwDormakabaComSalesOrder.SalesOrderCreateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt.CreateSync(messageheader,salesOrderreq);
        
        Test.setMock(WebServiceMock.class, new SAPSalesOrderUtilityMock());
        SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader messageheader1=new SVMX_wwwDormakabaSalesOrder.BusinessDocumentMessageHeader();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder salesOrderreq1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderUpdateRequestSalesOrder();
        SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort requstelemnt1=new SVMX_wwwDormakabaComSalesOrder.SalesOrderOutPort();
        requstelemnt1.UpdateSync(messageheader1,salesOrderreq1);
        
        insert partsline; 
        partsOrder2.SVMX_Service_Sales_Order_Number__c = '3434454';
        update partsOrder2;
        
        
        
        test.stopTest();
    }
    
}