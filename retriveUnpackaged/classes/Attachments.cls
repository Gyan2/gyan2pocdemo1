public with sharing class Attachments extends CoreSObjectDomain {

    public Attachments(List<Attachment> sObjectList){
        super(sObjectList);
    }

    public override void onBeforeInsert() {
        AttachmentServices.applyRenamingRules(Records);
    }

    public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
        AttachmentServices.applyRenamingRules(Records);
    }

    public class Constructor implements CoreSObjectDomain.IConstructable {
        public CoreSObjectDomain construct(List<SObject> sObjectList) {
            return new Attachments(sObjectList);
        }
    }
}