@isTest
public class TEST_IntegrationMessage 
{
    static Opportunity op;
    private static void setup()
    {        
       TestData.setupOpportunity();
       op = TestData.opportunity;
    }
    
    @isTest
    public static void testIntegrationMessage()
    {
		setup();        
        Id messId = IntegrationMessage.createMessage(op.Id, 'Sent', 'Endpoint', 'Request ', 'Response ');
        IntegrationMessage.updateMessage(messId, 'Success', 'Result');
        String status = IntegrationMessage.getStatus(op.Id);
        
        system.assertEquals('Success', status);
    }  
    @isTest
    public static void testNegativeIntegrationMessage()
    {
        setup();
		delete op;   
        String status = IntegrationMessage.getStatus(op.Id);        
        system.assertNotEquals('Success', status);
    }
}