@isTest
public class  SVMX_SAPServiceReportUtility_UT{
    static testMethod void attachmentCreate(){

        SVMX_Integration_SAP_Endpoint__c mycs = new SVMX_Integration_SAP_Endpoint__c();
                 mycs.Name = 'ServiceReportAttachment';
                 mycs.SVMX_EndPoint__c = 'https://hookb.in/vLNNOjdN';
            insert mycs;
              
        SVMX_SAP_Integration_UserPass__c cs = new SVMX_SAP_Integration_UserPass__c();
              cs.name ='SAP PO';
              cs.WS_USER__c ='test';
              cs.WS_PASS__c ='test';
        insert cs;

        Account acc = SVMX_TestUtility.CreateAccount('Germany Account','Germany',true);
       
        Contact con = SVMX_TestUtility.CreateContact(acc.Id, true);
        
        SVMXC__Site__c loc = SVMX_TestUtility.CreateLocation('Germany Location',acc.id,true);
       
        Product2 prod = SVMX_TestUtility.CreateProduct('Door Closers',false);
        prod.SAPNumber__c= '1234567';
        insert prod;
        
        Product2 prod1 = SVMX_TestUtility.CreateProduct('Door', false);
        prod1.SAPNumber__c= '1234567';
        insert prod1;
        
        SVMXC__Installed_Product__c IB = SVMX_TestUtility.CreateInstalledProduct(acc.id,prod.id,loc.id,true);
        
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.Id, 'Test Contract', 'Default', con.id,true);
        
        BusinessHours bh1 = SVMX_TestUtility.SelectBusinessHours();
     
        SVMX_Rate_Tiers__c rt = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, false, true);
        
        SVMX_Rate_Tiers__c rt2 = SVMX_TestUtility.CreateRateTier(sc.id, acc.Id, true, true);
        
        SVMXC__Service_Group_Members__c tech = SVMX_TestUtility.CreateTechnician('SVMX Tech', null, null, true);
        
        SVMX_Custom_Feature_Enabler__c cf = SVMX_TestUtility.CreateCustomFeature('Germany',true);
        
        Case cs1 = SVMX_TestUtility.CreateCase(acc.Id,con.Id, 'New', false);
        //cs.SVMX_Service_Sales_Order_Number__c =null;
        //cs.SVMX_Awaiting_SAP_Response__c = false;
        cs1.SVMX_Invoice_Status__c='Requested on all cases';
        insert cs1;

        SVMXC__Service_Order__c wo = SVMX_TestUtility.CreateWorkOrder(acc.Id,loc.Id,prod.Id,'open','Planned Services (PPM)','Global Std',sc.Id,tech.Id,cs1.id,false);
        wo.SVMXC__Scheduled_Date_Time__c = system.today();
        wo.SVMX_Service_Number_Blank_Check__c =false;
        //wo.SVMX_Service_Sales_Order_Number__c = '6846957';
        insert wo;

        Attachment attach=new Attachment();    
        attach.Name='Unit_Test_Service_Report_Attachment.pdf';
        Blob bodyBlob=Blob.valueOf('Unit Test Attachment Body');
        attach.body=bodyBlob;
        attach.parentId=wo.id;

        

        Test.startTest();
        Test.setMock(WebServiceMock.class, new SAPServiceReportUtilityMock());
        SVMX_wwwDormakabaComServiceReport.BusinessDocumentMessageHeader messageheader = new SVMX_wwwDormakabaComServiceReport.BusinessDocumentMessageHeader();
        list<SVMX_wwwDormakabaComServiceReport.DocumentCreateRequestDocument> servicereportreq = new list<SVMX_wwwDormakabaComServiceReport.DocumentCreateRequestDocument>();
        SVMX_wwwDormakabaComServiceReport.HTTPS_Port requstelemnt = new SVMX_wwwDormakabaComServiceReport.HTTPS_Port();
        requstelemnt.CreateSync(messageheader,servicereportreq);
        
        insert attach;
        
        wo.SVMX_Service_Sales_Order_Number__c = '6846957';
        update wo;
        
        Test.stopTest();

    }
}