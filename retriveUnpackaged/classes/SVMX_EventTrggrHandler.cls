/*******************************************************************************************************
Description: Trigger handler for Event Trigger

Dependancy: 
 Trigger Framework: SVMX_TriggerHandler.cls
 Trigger: SVMX_EventTrggr.cls
    
    
Author: Nivedita S
Date: 12-12-2017

*******************************************************************************************************/


    public class SVMX_EventTrggrHandler extends SVMX_TriggerHandler{

      private list<Event>neweventList;
      private list<Event>oldEventList;
      private Map<Id,Event>newEventMap;
      private Map<Id,Event>oldEventMap;

      public SVMX_EventTrggrHandler() {
        this.neweventList = (list<Event>) Trigger.new;
         this.oldEventList = (list<Event>) Trigger.old;
         this.newEventMap = (Map<Id,Event>) Trigger.newMap;
         this.oldEventMap = (Map<Id,Event>) Trigger.oldMap;
        }

      public override void afterInsert(){
       
       
        List <Event> eventtimelist = new  List <Event>();
           for(Event ev: neweventList){
              if( ev.StartDateTime!=null && ev.EndDateTime!=null && ev.SVMX_Event_Type__c!=null ) 
                  eventtimelist.add(ev);
                  
            }
              SVMX_PS_TS_TimesheetUtils timesheetUtils = new SVMX_PS_TS_TimesheetUtils();
              if(eventtimelist.size()>0)
              timesheetUtils.handleEventsFromsalesforceEvents(eventtimelist,null,null,true,null,null);   
        }
                
        public override void afterUpdate(){
      
        List <Event> eventtimelist = new  List <Event>();
        
       for(Event ev:neweventList){
        if((ev.StartDateTime != oldEventMap.get(ev.id).StartDateTime || ev.EndDateTime != oldEventMap.get(ev.id).EndDateTime) || (ev.SVMX_Event_Type__c!=oldEventMap.get(ev.id).SVMX_Event_Type__c )) 
        
          eventtimelist.add(ev);
            
        }
       
       SVMX_PS_TS_TimesheetUtils timesheetUtils = new SVMX_PS_TS_TimesheetUtils();
       if(eventtimelist.size()>0)
        timesheetUtils.handleEventsFromsalesforceEvents(eventtimelist,oldEventMap,null,false,true,false);
       }
       
       public override void beforeDelete(){

        List <Event> oldeventtimelist = new  List <Event>();
         
        for(Event ev:oldEventList){
            
           if(ev.SVMX_Event_Type__c!=null)

             oldeventtimelist.add(ev);
          }
            
             SVMX_PS_TS_TimesheetUtils timesheetUtils = new SVMX_PS_TS_TimesheetUtils();
             if(oldeventtimelist.size()>0)
              timesheetUtils.handleEventsFromsalesforceEvents(null,null,oldeventtimelist,false,false,true);
       }
       
       
    }