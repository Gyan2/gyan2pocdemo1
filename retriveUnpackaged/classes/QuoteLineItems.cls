/**
 * Created by trakers on 19.07.18.
 */

public with sharing class QuoteLineItems extends CoreSObjectDomain{

        public QuoteLineItems(List<Quote> sObjectList){
            super(sObjectList);
        }

        public override void onApplyDefaults() {
            // Apply defaults to Products

        }

        public override void onValidate() {
            // Validate Products
        }

        public override void onValidate(Map<Id,SObject> existingRecords) {
            // Validate changes to Products

        }

        public override void onBeforeInsert(){

        }

        public override void onBeforeUpdate(Map<Id,SObject> existingRecords) {
            // Related Products

        }

        public override void onBeforeDelete(){
            QuoteLineItemsServices.deleteText((List<QuoteLineItem>)Records);
        }

        public override void onAfterInsert(){

        }


        public override void onAfterUpdate(Map<Id,SObject> existingRecords){

        }

        public override void onAfterDelete(){

        }


        public override void onAfterUndelete(){

        }


        public class Constructor implements CoreSObjectDomain.IConstructable
        {
            public CoreSObjectDomain construct(List<SObject> sObjectList)
            {
                return new QuoteLineItems(sObjectList);
            }
        }
}