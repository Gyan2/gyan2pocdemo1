@isTest
public class Util_Test {

    static testMethod void recordTypeTest () {
        List<RecordType> allRecordTypes = [SELECT Name, DeveloperName, NamespacePrefix, SobjectType FROM RecordType];

        for (RecordType forRT : allRecordTypes) {
            System.assertEquals(forRT, Util.RecordTypes.get(forRT.SobjectType + ':' + (String.isBlank(forRT.NamespacePrefix)?'':forRT.NamespacePrefix + '__' ) + forRT.DeveloperName));
            System.assertEquals(forRT, Util.RecordTypes.get(forRT.Id));
            System.assertEquals(forRT.SobjectType + ':' + (String.isBlank(forRT.NamespacePrefix)?'':forRT.NamespacePrefix + '__') + forRT.DeveloperName, Util.RecordTypes.getCombinedDeveloperName(forRT.Id));
            System.assertEquals((String.isBlank(forRT.NamespacePrefix)?'':forRT.NamespacePrefix + '__') + forRT.DeveloperName, Util.RecordTypes.getDeveloperName(forRT.Id));
        }
    }

    static testMethod void describeFieldTest () {
        System.assertEquals(Account.Name.getDescribe(),Util.Describe.getField('Contact','Account.Name'));
    }

    static testMethod void testGetPicklistMethod(){

        Map<String,Schema.SObjectType> globalDescribe = Schema.getGlobalDescribe();

        List<Schema.PicklistEntry> picklistEntries  = globalDescribe.get('Case').getDescribe().fields.getMap().get('Origin').getDescribe().getPicklistValues();
        List<UtilDependentPicklist.PicklistEntryWrapper> result = new List<UtilDependentPicklist.PicklistEntryWrapper>();

        UtilDependentPicklist.PicklistEntryWrapper wrapper = new UtilDependentPicklist.PicklistEntryWrapper();
                                                    wrapper.active = true;
                                                    wrapper.defaultValue = false;
                                                    wrapper.label = '-- none --';
                                                    wrapper.value = null;
        result.add(wrapper);
        result.addAll(UtilDependentPicklist.wrapPicklistEntries(picklistEntries));

        System.assertEquals(true,result.size() > 0);

    }
}