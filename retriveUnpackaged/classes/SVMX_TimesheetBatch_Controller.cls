/*******************************************************************************************************
Description: Controller class for creating timesheets, timesheet daily entries and 
			 marking previous timesheets
Dependancy:    
	Class: SVMX_TimesheetBatch_Controller
 
Author: Pooja Singh
Date: 14-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_TimesheetBatch_Controller{
	public static void createTimesheet(Set<Date> startDateSet, Map<Date,Date> endDateMap, String frequency, List<Id> uIdList){
    	List<SVMXC__Timesheet__c> tsInsertList = new List<SVMXC__Timesheet__c>();
    	//Create timesheet
            for(Id uId : uIdList){
                for(Date stdt : startDateSet){
                    SVMXC__Timesheet__c ts = new SVMXC__Timesheet__c();
                    ts.SVMXC__Start_Date__c = stdt;
                    ts.SVMXC__End_Date__c = endDateMap.get(stdt);
                    ts.SVMXC__Period__c = frequency;
                    ts.SVMXC__Status__c = 'Open';
                    ts.SVMX_Current_Week__c = true;
                    ts.SVMXC__User__c = uId;
                    ts.OwnerId = uId;
                    tsInsertList.add(ts);
                }
                //system.debug('Timesheet insert list collect'+tsInsertList);
            }
            system.debug('Timesheet insert list'+tsInsertList);
            if(tsInsertList.size() > 0){
                try{
                    insert tsInsertList;
                }
                catch(exception e){
                    system.debug('Exception during insert'+e);
                }
            }
            
            List<SVMXC__Timesheet__c> insertedTsQuery = new List<SVMXC__Timesheet__c>([Select Id, Name, SVMXC__Start_Date__c, SVMXC__End_Date__c, SVMXC__User__c, SVMXC__Period__c from SVMXC__Timesheet__c where Id IN: tsInsertList]);
            List<SVMXC__Timesheet_Day_Entry__c> insertTdeList = new List<SVMXC__Timesheet_Day_Entry__c>();
            for(SVMXC__Timesheet__c tsNew : insertedTsQuery){
                //Create timesheet daily entry for the inserted timesheets                
                List<SVMXC__Timesheet_Day_Entry__c> tsTdeList = createTsDailySum(tsNew.SVMXC__Start_Date__c,tsNew.SVMXC__End_Date__c,tsNew.Id,tsNew.SVMXC__User__c);
                system.debug('timesheet daily summary list'+tsTdeList);
                insertTdeList.addAll(tsTdeList);
            }
            system.debug('Timesheet daily summary insert list'+insertTdeList);
            if(insertTdeList.size() > 0){
                try{
                    insert insertTdeList;
                }
                catch(exception e){
                    System.debug('Exception '+e);
                }
            }
	}
    //Create timesheet daily summary
    public static List<SVMXC__Timesheet_Day_Entry__c> createTsDailySum(Date startDate, Date endDate, Id tsId, Id uId){
        List<SVMXC__Timesheet_Day_Entry__c> returnTdeList = new List<SVMXC__Timesheet_Day_Entry__c>();
        Integer daysDiff = startDate.daysBetween(endDate);
        for(Integer i=0; i <= daysDiff; i++){        
            SVMXC__Timesheet_Day_Entry__c tde = new SVMXC__Timesheet_Day_Entry__c();
            tde.SVMXC__Timesheet__c = tsId;
            tde.SVMXC__Timsheet_Day__c = startDate+i;
            tde.OwnerId = uId;
            returnTdeList.add(tde);
        }
            return returnTdeList;
    }
    //Method for previous week/month timesheet
	public static void prevWeekTimeSheet(Set<Date> prevStDtSet, Map<Date,Date> prevEdDtMap){ 
        //Previous week timesheet query
    	List<SVMXC__Timesheet__c> prevTsQuery = new List<SVMXC__Timesheet__c>([Select Id, Name, SVMX_Current_Week__c, SVMX_Previous_Week__c, SVMXC__Start_Date__c, SVMXC__End_Date__c from SVMXC__Timesheet__c where SVMXC__Start_Date__c IN: prevStDtSet and SVMXC__End_Date__c IN: prevEdDtMap.values() and SVMX_Current_Week__c = true]);
        for(SVMXC__Timesheet__c prevTs : prevTsQuery){
            prevTs.SVMX_Current_Week__c = false;
            prevTs.SVMX_Previous_Week__c = true;
            system.debug('Previous timesheet in controller '+prevTs);
        }
        if(prevTsQuery.size() > 0)
        	update prevTsQuery;
    }
}