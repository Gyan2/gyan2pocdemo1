/**==================================================================================================================================
 * DormaKaba
 * Name: SVMX_GetPartsOrderPrice
 * Description: Web service Class called from 'Get Parts Price'
 * Created Date: 03-04-2018
 * Created By: Ranjitha S
 *
 
 ===================================================================================================================================*/
global class SVMX_GetPartsOrderPrice{
    
    public static boolean blankSFMFlag = false;
    public static boolean testClassVar = false;
    
    webservice static SVMXC.SFM_WrapperDef.SFM_PageData getPartsOrderLinePrice(SVMXC.SFM_WrapperDef.SFM_TargetRecord request){
        // Describe all objects
        system.debug('Request = ' + request);
        
        SVMXC.SFM_WrapperDef.SFM_PageData pagedata = new SVMXC.SFM_WrapperDef.SFM_PageData();
        map<String,Schema.SObjectType> Allobj = new map<String, Schema.SObjectType>();
        map<String, Schema.SObjectType> gd = new Map<String, Schema.SObjectType>();
        SVMXC.SFM_ServicesDef def = new SVMXC.SFM_ServicesDef();
        map<String,List<Sobject>> detailSobjectMap = new map<String, List<Sobject>>();
        map<String,List<Sobject>> newDetailSobjectMap = new map<String, List<Sobject>>();
        
        gd = Schema.getGlobalDescribe();
        if(gd.size() > 0){
            for(Schema.SObjectType s : gd.values()){
                Schema.DescribeSObjectResult result = s.getDescribe();
                Allobj.put(result.getName(), s);
            }
        }
       

        // The method below returns the Header Sobject record
        Sobject headerSobj = def.SFM_Page_GetHeaderRecord(request, Allobj);
        SVMXC__RMA_Shipment_Order__c  objOrder = new SVMXC__RMA_Shipment_Order__c ();
        objOrder = (SVMXC__RMA_Shipment_Order__c)headerSobj;

        // The method below returns the detail Sobject records in a map (Key: Tab Id, Value: List of Sobject records)
        detailSobjectMap = def.SFM_Page_GetDetailRecords(request, Allobj);
        system.debug('detailSobjectMap = ' + detailSobjectMap);


        if(Test.isRunningTest()){
            
            List<SVMXC__RMA_Shipment_Line__c> wodetailLsitTemp = new List<SVMXC__RMA_Shipment_Line__c>();
            wodetailLsitTemp = [SELECT Id,SVMXC__RMA_Shipment_Order__c,recordTypeId,SVMXC__Expected_Quantity2__c,SVMXC__Product__c,SVMXC__Line_Price2__c FROM SVMXC__RMA_Shipment_Line__c];
            for(SVMXC__RMA_Shipment_Line__c wdl:wodetailLsitTemp){
                List<SVMXC__RMA_Shipment_Line__c> tempWDL = new List<SVMXC__RMA_Shipment_Line__c>();
                if(detailSobjectMap.containsKey(wdl.SVMXC__RMA_Shipment_Order__c)){
                    tempWDL = detailSobjectMap.get(wdl.SVMXC__RMA_Shipment_Order__c);
                }
                tempWDL.add(wdl);
                detailSobjectMap.put(wdl.SVMXC__RMA_Shipment_Order__c,tempWDL);
            }
            
        }

        Map<SVMXC__RMA_Shipment_Line__c,String> PartsOrLineToProduct = new Map<SVMXC__RMA_Shipment_Line__c,String>();
        Map<String,String> tempProductMap = new Map<String,String>();
        Set<Id> PartsOrderLineSet = new Set<Id>();
        // Loop through the map to get the list of Sobject records and process them
        if(detailSobjectMap.size() > 0){

            //check to see if there are no detail records
            Boolean testNonBlankRecords = false;
            if(request.detailRecords != null){
                
                for(SVMXC.SFM_WrapperDef.SFM_TargetRecordObject targetRecordObject : request.detailRecords){
                    if(targetRecordObject.records != null && targetRecordObject.records.size() > 0){
                        testNonBlankRecords = true;
                    }
                }
            }


            if(testNonBlankRecords ){
                //Create Map of <Product ID , Product Name>
                set<Id> productIds = new set<Id>();

                for(String str : detailSobjectMap.keyset()){
                    list<SVMXC__RMA_Shipment_Line__c> lstPOLines = new list<SVMXC__RMA_Shipment_Line__c>();
                    lstPOLines = detailSobjectMap.get(str);
                    if(lstPOLines.size() > 0){
                        for(Integer i = 0; i < lstPOLines.size(); i++){
                               String productId = (String)lstPOLines[i].SVMXC__Product__c;
                                productIds.add((Id)productId);
                               // lstPOLines[i].SVMXC__Line_Price2__c = 0;
                        }
                    }
                }

                system.debug('product set '+productIds);
               
            
                if(productIds.size()>0){

                    List<VendorMaterial__c> vendorMaterialList = new List<VendorMaterial__c>();
                    vendorMaterialList = [select Id, Account__c, Price__c, Product__c  from VendorMaterial__c where Account__c=:objOrder.SVMX_Vendor__c ];
                    
                    Map<id,decimal> productVendorMaterialMap = new Map<id,decimal>();
                    
                    for(VendorMaterial__c vm: vendorMaterialList){
                        productVendorMaterialMap.put(vm.Product__c ,vm.Price__c);
                    }
                    
                    for(String str : detailSobjectMap.keyset()){
                        list<SVMXC__RMA_Shipment_Line__c> lstPOLines = new list<SVMXC__RMA_Shipment_Line__c>();
                        lstPOLines = detailSobjectMap.get(str);
                        if(lstPOLines.size() > 0){
                            for(Integer i = 0; i < lstPOLines.size(); i++){
                                    id prd = lstPOLines[i].SVMXC__Product__c;
                                    
                                system.debug(' ## Unit Price '+lstPOLines[i].SVMXC__Line_Price2__c);
                                    if(prd !=null && (lstPOLines[i].SVMXC__Line_Price2__c == null || lstPOLines[i].SVMXC__Line_Price2__c == 0) && productVendorMaterialMap.size() > 0 &&  productVendorMaterialMap.containsKey(prd) && productVendorMaterialMap.get(prd) != null ){
                                     
                                         SVMXC__RMA_Shipment_Line__c pl = lstPOLines[i];
                                         lstPOLines[i].SVMXC__Line_Price2__c  = productVendorMaterialMap.get(prd);
                                         
                                    }else{
                                        system.debug('no value');
                                    }
                            }
                            newDetailSobjectMap.put(str, lstPOLines);
                        }
                    }
                     
                }
            }else{
                system.debug('blankSFMFlag####'+blankSFMFlag);
                blankSFMFlag = true;
            }

        }
       
        
        system.debug('>>>>>>testttttt>>>>'+newDetailSobjectMap);
        system.debug('>>>>>>testtttt>>>>'+detailSobjectMap);
        
        // Call the method below with the Header Sobject record and processed Detail Sobjects map 
        if (!Test.isRunningTest()) {
            if(!blankSFMFlag)
                pagedata = def.SFM_Page_BuildResponse(request, headerSobj, newDetailSobjectMap);
            else
                pagedata = def.SFM_Page_BuildResponse(request, headerSobj, detailSobjectMap);
        }
        system.debug('Response = ' + pagedata);
        return pagedata;
        
        //return null;
    }  
}