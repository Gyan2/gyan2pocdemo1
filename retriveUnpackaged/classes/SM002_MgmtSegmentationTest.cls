/*******************************************************************************************************
* Class Name      	: SM002_MgmtSegmentationTest
* Description		: Test class for Service Manager
* Author          	: Paul Carmuciano
* Created On      	: 2017-07-10
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-07-10         1000                 Initial version
******************************************************************************************************/
@isTest
public class SM002_MgmtSegmentationTest {

    @testSetup
    public static void setupData() {
        
    }
    


    /***
* Method name	: applyManagementSegmentationTest
* Description	: Can test as running user / admin as were only testing the method works here 
*					and not in context of an end-user transaction.
* Author		: Paul Carmuciano
* Return Type	: n/a
* Parameter		: n/a
*/
    @isTest
    public static void applyManagementSegmentationTest() {
        // Arrange
        Map<Id,Profile> profiles = DM004_User.getCustomProfiles(); // don't use it, but could

        // get the custom metadata so we can assert against
        Map<String,Management_Segmentation__mdt> ManagementSegmentationMapping = new Map<String,Management_Segmentation__mdt>();
        ManagementSegmentationMapping = MDM001_ManagementSegmentation.ManagementSegmentationMapping;
        
        // get my user do I can access the Custom fields
        Map<Id,User> users = DM004_User.getActiveUsersForProfile(new List<Id>{UserInfo.getProfileId()} );
        User u = users.get(UserInfo.getUserId());

        // contextualise to the user object
        Schema.DescribeFieldResult mgmtSegSourceCountry = Schema.SObjectType.User.Fields.UserCountry__c;
        Schema.DescribeFieldResult mgmtSegTargetRegionField = Schema.SObjectType.User.Fields.UserRegion__c;
        Schema.DescribeFieldResult mgmtSegTargetSegmentField = Schema.SObjectType.User.Fields.UserSegment__c;

        // contextualise to a target so we can assert the result
        Management_Segmentation__mdt targetManagementSegmentationMapping;
            
        // Act
        Test.startTest();
        // find a different Country/Region/Segment from the custommetadata
        for (Management_Segmentation__mdt msm : ManagementSegmentationMapping.values() ) {
            if (msm.Segment__c != u.UserSegment__c) {
                targetManagementSegmentationMapping = msm;
                break;
            }
        }
        
        // use generic sobjects in the method, as it's used cross system
        Sobject oldSo = (Sobject)u.clone();
        // change the country
        u.UserCountry__c = targetManagementSegmentationMapping.DeveloperName;
        Sobject so = (Sobject)u;
        
        Sobject soResult = SM002_MgmtSegmentation.applyManagementSegmentation(
            oldSo,
            so,
            ManagementSegmentationMapping.get( (String)so.get(mgmtSegSourceCountry.name)),
            mgmtSegSourceCountry,
            mgmtSegTargetRegionField,
            mgmtSegTargetSegmentField
        );
        
        //also cover no change (inverse of above)
        SM002_MgmtSegmentation.applyManagementSegmentation(
            (Sobject)u.clone(),
            so,
            ManagementSegmentationMapping.get( (String)so.get(mgmtSegSourceCountry.name)),
            mgmtSegSourceCountry,
            mgmtSegTargetRegionField,
            mgmtSegTargetSegmentField
        );

        //also cover the exception, ie sobject error
        SM002_MgmtSegmentation.applyManagementSegmentation(
            null,
            ManagementSegmentationMapping.get( (String)so.get(mgmtSegSourceCountry.name)),
            mgmtSegTargetRegionField,
            mgmtSegTargetSegmentField
        );
        
        Test.stopTest();

        // Assert
        system.assertEquals(targetManagementSegmentationMapping.Region__c, soResult.get(mgmtSegTargetRegionField.name));
        system.assertEquals(targetManagementSegmentationMapping.Segment__c, soResult.get(mgmtSegTargetSegmentField.name));
        
    }
    
    
}