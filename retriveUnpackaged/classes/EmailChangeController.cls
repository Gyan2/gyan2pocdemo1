/**
 * Created by rebmangu on 31/01/2018.
 */

public with sharing class EmailChangeController {

    public static String ALREADY_FIXED  = 'It has already been fixed';
    public static String JUST_FIXED     = 'The email has been fixed';
    public static String ERROR          = 'This email doesn\'t exist';
    public static String SANDBOX        = 'Only work in a Sandbox';

    @AuraEnabled
    public static String changeUserEmail(String newEmail){
        if(newEmail == null)
            return ERROR;

        if(!TEST.isRunningTest() && ![select id,IsSandbox from Organization limit 1].IsSandbox)
            return SANDBOX;

        String email = newEmail;
        email = email.replace('@','=');
        email = email+'@example.com';

        if([select count() from User where Email =: email] > 0){
            User u = [select id, Email from User where Email =: email limit 1];
            u.Email = newEmail;
            update u;
            return JUST_FIXED;
        }else if([select count() from User where Email =: newEmail] > 0){
            return ALREADY_FIXED;
        }else{
            return ERROR;
        }
    }
}