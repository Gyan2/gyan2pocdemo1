/*******************************************************************************************************
* Class Name        : WSMaterial
* Description       : Webservice to handle materials coming from SAP
* Author            : Rebmann Guillaume
* Created On        : 2017-12-01
* Modification Log  :
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Rebmann Guillaume  2017-09-27                 1000           Initial version
******************************************************************************************************/

global class WSMaterials {

    public class WSMaterialsException extends Exception{}

    global class MaterialResponse {
        webService Boolean success;
        webService List<MaterialError> errors;
    }

    global class MaterialError {
        webService String message;
    }

    global class MaterialRequest {
        webService List<Material> materials;
        webService String SenderSystem;
    }

    global class Description {
        webservice String languageCode;
        webService String value;
    }

    global class SalesSpecifications {
        webservice String salesOrganisationId;
        webservice List<Description> descriptions;
        webservice String distributenChannelCode;
        webservice String salesMeasureUnitCode;
        webservice Boolean blockedForSalesIndicator;
        webservice Boolean configurableIndicator;
        webservice Boolean makeToOrderIndicator;
        webService String previousMaterialInternalID;
        webService String mrpProfile;

    }

    global class Material {
        webService String baseMeasureUnitCode;
        webService Boolean deletionIndicator;
        webservice List<Description> descriptions;
        webservice String internalId;
        webService String producthierarchy;
        webService List<SalesSpecifications> salesSpecifications;
        webService String standardID;
    }



/***

        Upsert Materials

 ***/


    webService static MaterialResponse upsertMaterial(MaterialRequest materials){

        HandleMaterials handlerInstance = new HandleMaterials(materials);

        return handlerInstance.response();
    }


    private class HandleMaterials{

        List<ProductDetail__c> productDetails = new List<ProductDetail__c>();
        Map<String,Material> materialMapping = new Map<String,Material>();
        Map<String,Product2> productMapping = new Map<String,Product2>();


        List<String> errors     = new List<String>();
        Datetime LastSyncDate   = System.now();
        String LastSyncFrom;


        String standardPriceBookId;


        handleMaterials(MaterialRequest request){
            this.LastSyncFrom = request.SenderSystem;
            if(Test.isRunningTest()){
                this.standardPriceBookId =  Test.getStandardPricebookId();
            }else{
                this.standardPriceBookId = [select id from Pricebook2 where isStandard=true limit 1].Id;
            }


            if(request.materials != null){

                // Parse the products
                for(Material material : request.materials) {
                    this.parseProduct(material);
                }

                // Map the product Hierarchy
                this.mapProductHierarchy(productMapping.values());

                // Handle errors if the SAPNumber are smaller than 18 digits
                for(Product2 product : productMapping.values()){
                    if(product.SAPNumber__c.length() < 18){
                        this.errors.add('SAPNumber "'+product.SAPNumber__c+'" should have a length of 18 digits');
                    }
                }

            }


        }

        private void mapProductHierarchy(List<Product2> products){

            Map<String,String> fullIdMapping = new Map<String,String>();

            //Extract the externalIds
            for(Product2 product : products){
                if(product.ProductHierarchy__c != null){
                    // map long id to short id (ex: 0212042055030000 => 0212042055030). This only work if it's trully respected
                    fullIdMapping.put(product.ProductHierarchy__c,product.ProductHierarchy__c.left(ProductHierarchyServices.MAXLENGTH));
                }
            }

            //Fetch the Salesforce Id and create the mapping
            Map<String,Id> productHierarchyMapping = new Map<String,Id>();
            for(ProductHierarchy__c ph : [select id,ExternalId__c from ProductHierarchy__c where ExternalId__c in: fullIdMapping.values()]){
                productHierarchyMapping.put(ph.ExternalId__c,ph.Id);
            }

            // Map the correct ProductHierarchy record to the product
            for(Product2 product : products){
                if(productHierarchyMapping.containsKey(fullIdMapping.get(product.ProductHierarchy__c))){
                    product.ProductHierarchyRef__c = productHierarchyMapping.get(fullIdMapping.get(product.ProductHierarchy__c));
                }

            }


        }

        private void parseProduct(Material material){


            Product2 product = new Product2();

            /*
                    Mapping here
            */
            product.EAN__c                      = material.standardId;
            product.SAPNumber__c                = material.internalId;
            product.ExternalId__c               = this.LastSyncFrom+'-'+material.internalId;
            product.isActive                    = !material.deletionIndicator;
            product.ProductHierarchy__c         = material.producthierarchy;
            product.Family                      = material.producthierarchy!=null?material.producthierarchy.left(2):null;
            product.LastSyncFrom__c             = this.LastSyncFrom;
            product.SAPInstance__c              = this.LastSyncFrom;
            product.LastSyncDate__c             = this.LastSyncDate;
            product.SVMXC__Unit_Of_Measure__c   = material.baseMeasureUnitCode; // Uncomment in case we need to add the value to the serviceMax field
            product.QuantityUnitOfMeasure       = material.baseMeasureUnitCode;
            //product.MRPProfile__c               = material.mrpProfile;
            product.SalesSpecifications__c      = material.salesSpecifications.size()>0?JSON.serialize(material.salesSpecifications):'[]';
            //PreviousExternalId__c
            Set<String> previousExternalIds = new Set<String>();
            for(SalesSpecifications specification : material.salesSpecifications){
                previousExternalIds.add(specification.previousMaterialInternalID);

            }
            product.PreviousExternalId__c = String.join(new List<String>(previousExternalIds),' ');

            // Handle SalesSpecification
            List<Object> salesSpecifications = (List<Object>)JSON.deserializeUntyped(product.SalesSpecifications__c);
            Integer index = 0;
            for(Object item :salesSpecifications){
                Map<String,Object> salesSpecification = (Map<String,Object>)item;
                                   salesSpecification.put('SVMXC__Stockable__c',true);
                                   salesSpecification.put('SVMX_SAP_Product_Type__c','Stock Part');

                // PC16
                if(salesSpecification.get('mrpProfile') == 'PC16'){
                    // By Default:
                    salesSpecification.put('SVMXC__Stockable__c',false);


                    String pointer = product.SAPNumber__c;
                    System.debug(60000+' <= '+Long.valueOf(pointer)+' && '+68999+' >=  '+Long.valueOf(pointer));
                    // Range - 60000-68999
                    if(60000 <=  Long.valueOf(pointer) && 68999 >=  Long.valueOf(pointer)){
                        salesSpecification.put('SVMX_SAP_Product_Type__c','Service Part');
                    // Range - 69000-69999
                    }else if(69000 <=  Long.valueOf(pointer) && 69999 >=  Long.valueOf(pointer)){
                        salesSpecification.put('SVMX_SAP_Product_Type__c','Contract Product');
                    }
                // PC17
                }else if(salesSpecification.get('mrpProfile') == 'PC17'){
                    // By Default:
                    salesSpecification.put('SVMXC__Stockable__c',false);


                    String pointer = product.SAPNumber__c;
                    // Range - 60000-68999
                    if(60000 <=  Long.valueOf(pointer) && 68999 >=  Long.valueOf(pointer)){
                        salesSpecification.put('SVMX_SAP_Product_Type__c','Procured Part');
                    }

                }

                // Take the first one and put the values in the products
                if(index == 0){
                    product.MRPProfile__c            = (String)salesSpecification.get('mrpProfile');
                    product.SVMX_SAP_Product_Type__c = (String)salesSpecification.get('SVMX_SAP_Product_Type__c');
                    product.SVMXC__Stockable__c      = (Boolean)salesSpecification.get('SVMXC__Stockable__c');
                }

                index++;
            }
            product.SalesSpecifications__c = JSON.serialize(salesSpecifications);


            for(Description description : material.descriptions){
                // Take the english description as a name otherwise create a productDetail__c
                if(description.languageCode.toUpperCase() == 'EN'){
                    product.name = description.value.toUpperCase();
                }
                this.parseProductDetail(product,description);

            }



            this.materialMapping.put(product.ExternalId__c,material);
            this.productMapping.put(product.ExternalId__c,product);
        }

        private void parseProductDetail(Product2 product,Description description){
            Set<String> languages = new Set<String>();
            for(PicklistEntry entry : ProductDetail__c.Language__c.getDescribe().getPicklistValues()){
                languages.add(entry.getValue());
            }

            if(!languages.contains(description.languageCode.toUpperCase()))
                return ;

            ProductDetail__c productDetail = new ProductDetail__c(ProductRef__r=new Product2(ExternalId__c=product.ExternalId__c));
            /*
                    Mapping here
             */
            productDetail.ExternalId__c             = product.ExternalId__c+'-'+description.languageCode.toUpperCase();
            productDetail.Language__c               = description.languageCode.toUpperCase();
            productDetail.Description__c            = description.value;
            productDetail.Name                      = description.value;
            productDetail.LastSyncFrom__c           = this.LastSyncFrom;
            productDetail.LastSyncDate__c           = this.LastSyncDate;
            this.productDetails.add(productDetail);

        }


        private void handleBooks(){
            Set<Pricebook2> books                   = new Set<Pricebook2>();
            Map<String,PricebookEntry> pricebookEntries    = new Map<String,PricebookEntry>();

            Set<String> priceEntriesId = new Set<String>();


            // priceEntriesId = Set of pricebooksId to insert



            for(String key :  this.productMapping.keySet()){
                Product2 product    = this.productMapping.get(key);
                Material material   = this.materialMapping.get(key);

                for(SalesSpecifications specification : material.salesSpecifications){

                    books.add(new Pricebook2(ExternalId__c=specification.salesOrganisationId,Name='SalesOrg-'+specification.salesOrganisationId,IsActive=true));

                    String externalId = specification.salesOrganisationId+'-'+product.ExternalId__c;
                    String standardId = 'standard-'+product.ExternalId__c+'-CHF';

                    pricebookEntries.put(standardId,new PricebookEntry(
                            Pricebook2Id    = this.standardPriceBookId,
                            UnitPrice       = 0,
                            Product2Id      = Product.Id,
                            ExternalId__c   = standardId,
                            CurrencyIsoCode = 'CHF',
                            IsActive        = true
                    ));

                    pricebookEntries.put(externalId,new PricebookEntry(
                            Pricebook2         = new Pricebook2(ExternalId__c=specification.salesOrganisationId),
                            IsActive           = product.isActive?!specification.blockedForSalesIndicator:false,
                            LegacyId__c        = specification.previousMaterialInternalID,
                            IsConfigurable__c  = specification.configurableIndicator,
                            Product2           = new Product2(ExternalId__c=product.ExternalId__c),
                            ExternalId__c      = externalId,
                            UnitPrice          = 0,
                            CurrencyIsoCode    = 'CHF'
                            //Description__c     = specification.descriptions.size()>0?JSON.serialize(specification.descriptions):null
                    ));




                }
            }

            upsert new List<Pricebook2>(books) ExternalId__c;

            // Util to upsert Pricebooks (Fake upsert -> Insert Standard -> Insert Custom -> Update Standard & Custom
            Util.UtilPricebook.add(pricebookEntries).upsertPricebooks();


        }


        public MaterialResponse response(){

            if(this.errors.size() == 0 && this.productMapping.size() > 0){
                try{
                    upsert this.productMapping.values() ExternalId__c;
                    upsert this.productDetails ExternalId__c;
                    this.handleBooks();
                }catch(Exception e){
                    this.errors.add(e.getCause()+' - '+e.getLineNumber()+' - '+e.getMessage());
                }
            }


            MaterialResponse res = new MaterialResponse();
            res.errors = new List<MaterialError>();

            for(String error : this.errors){
                MaterialError materialError = new MaterialError();
                materialError.message = error;
                res.errors.add(materialError);
            }

            res.success = res.errors.size() == 0;

            return res;

        }









    }




}