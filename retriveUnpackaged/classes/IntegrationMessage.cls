// @description IntegrationMessage logging methods
// @copyright Parx
// @author MHA
// @lastmodify 24.04.2017
// 
public without sharing class IntegrationMessage 
{
  	public static Id createMessage (Id objectId,String status,String endpoint,String request,String response)
    {
        Integration_Message__c im = new Integration_Message__c();
        im.EndpointName__c = endpoint;
        im.MessageBody__c = request;
        im.SourceObjectId__c = objectId;  
        im.ResponseMessage__c = response;
        im.Delivery_Status__c = status;
        
        if(objectId.getSobjectType() == Opportunity.getSobjectType())
        {
            im.Opportunity__c = objectId;
        }
        
        insert im;
        
        return im.Id;
    }
    
    public static void updateMessage (Id messageId,String status,String result)
    {
        Integration_Message__c im = new Integration_Message__c();
        im.Id = messageId;
        im.Delivery_Status__c = status;
        im.DeliveryDate__c = Datetime.now();
        im.ResponseMessage__c = result;
           
        update im;
    } 
    public static String getStatus (Id objectId)
    {
        try
        {
            return [Select Delivery_Status__c From Integration_Message__c Where SourceObjectId__c = :objectId Order By CreatedDate Desc Limit 1].Delivery_Status__c;
        }
        catch (Exception e)
        {
            System.debug(Logginglevel.Error, e.getMessage());
            return 'Integration Message Not found';
        }   
        return '';
    } 
}