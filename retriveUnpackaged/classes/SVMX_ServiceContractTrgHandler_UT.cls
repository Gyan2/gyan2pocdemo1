/************************************************************************************************************
Description: Unit test class for SVMX_ServiceContractTrgHandler class
 
Author: Pooja Singh
Date: 30-11-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/
@isTest
public class SVMX_ServiceContractTrgHandler_UT {
    static testMethod void testServiceContractTrgHandler(){
    
        //Create Account
        Account acc = SVMX_TestUtility.CreateAccount('Test Account','United Kingdom', true);
        SVMX_Custom_Feature_Enabler__c  cfe=SVMX_TestUtility.CreateCustomFeature('Germany',true);
        //Create Contact
        Contact con = SVMX_TestUtility.CreateContact(acc.id, true);
        
        //Create Opportunity
        Opportunity opp = SVMX_TestUtility.CreateOpportunity(acc.id, 'Test Opportunity','Needs Analysis', 0.4, 100, true);
        
        //Create service contract
        List<SVMXC__Service_Contract__c> scList = new List<SVMXC__Service_Contract__c>();
        //List<SVMXC__Service_Contract__c> scUpdateList = new List<SVMXC__Service_Contract__c>();
        SVMXC__Service_Contract__c sc11 = SVMX_TestUtility.CreateServiceContract(acc.id,'Test contract', 'Global Std - Quote', con.id,true);
        SVMXC__Service_Contract__c sc = SVMX_TestUtility.CreateServiceContract(acc.id,'Test contract', 'Global Std - Quote', con.id,false);
        sc.SVMX_Opportunity__c = opp.id;
        List<SVMXC__PM_Offering__c>  pmOf=new List<SVMXC__PM_Offering__c>();
      
        
        scList.add(sc);
         
        SVMXC__Service_Contract__c sc1 = SVMX_TestUtility.CreateServiceContract(acc.id,'Test contract1', 'Global Std - Quote', con.id,false);
        sc1.SVMX_Opportunity__c = opp.id;
        sc1.SVMXC__Active__c=false;
        sc1.SVMXC__Contact__c = con.Id;
        sc1.SVMXC__Start_Date__c=system.today();
        sc1.SVMXC__End_Date__c=system.today().addMonths(7);
        
       
        
       // insert pm1;
        
        scList.add(sc1);
        
        SVMXC__Service_Contract__c sc2 = SVMX_TestUtility.CreateServiceContract(acc.id,'Test contract2', 'Global Std', con.id,true);
        sc2.SVMXC__Contact__c = con.Id;
        sc2.SVMXC__Active__c=false;
        sc2.SVMXC__Start_Date__c=system.today();
        sc2.SVMXC__End_Date__c=system.today().addMonths(7);  
        
        SVMXC__PM_Offering__c  pm2=new SVMXC__PM_Offering__c();
        pm2.SVMXC__Service_Contract__c=sc.Id;
        
        SVMXC__PM_Plan_Template__c  pmPlanTemp=new SVMXC__PM_Plan_Template__c();
        pmPlanTemp.SVMXC__Coverage_Type__c='Account (Visit Only)';
        insert pmPlanTemp;
        
        SVMX_Dynamic_PM_Configuration__c      dPMConfi=new SVMX_Dynamic_PM_Configuration__c();
        dPMConfi.SVMX_PM_Plan_Template__c=pmPlanTemp.Id;
        dPMConfi.SVMX_Year__c='25';
        
        insert dPMConfi;
        
        //insert pm2;   
     
        Test.startTest();
        
        insert scList;
        
        SVMXC__PM_Offering__c  pm=new SVMXC__PM_Offering__c();
        pm.SVMXC__Service_Contract__c=sc.Id;
        insert pm;
        
        SVMXC__PM_Offering__c  pm1=new SVMXC__PM_Offering__c();
        pm1.SVMXC__Service_Contract__c=sc.Id;
        
        insert pm1;
        
        sc.SVMX_Contract_Status__c = 'Quote Accepted';
        //sc.SVMXC__Renewed_From__c=sc11.Id;
        sc.SVMXC__Active__c=true;
        sc.SVMX_Country__c='Germany';
        sc.SVMX_Renewed_Through_Job__c=true;
        sc.SVMX_Dynamic_PM_Plan__c=true;
        sc.SVMX_Current_Renewal_Year__c=25;
        
        update sc;
        
        sc1.SVMX_Contract_Status__c = 'Quote Rejected';
        update sc1;
        
        sc2.SVMXC__Active__c = true;
        update sc2;
        
        Test.stopTest();
        
    }
}