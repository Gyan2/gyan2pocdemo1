/**
 * Created by rebmangu on 23/04/2018.
 */

@IsTest
public with sharing class TestProjects {

    static testMethod void testUpsertBuilding() {



        Building__c building = new Building__c(
                Name='Building',
                City__c='Bordeaux',
                Country__c='France',
                CountryIsoCode__c='FR',
                Street__c='2 place de la victoire',
                Zip__c='33000'
        );

        insert building;

        Project__c project1 = new Project__c(
                Name='Test Project 1',
                City__c='Strasbourg',
                Country__c='France',
                CountryIsoCode__c='FR',
                Street__c='24 Rue du general de gaulle',
                MKS__c=true,
                Zip__c='67000'
        );

        Project__c project2 = new Project__c(
                Name='Test Project 2',
                City__c='Strasbourg',
                Country__c='France',
                CountryIsoCode__c='FR',
                Street__c='24 Rue du general de gaulle',
                MKS__c=true,
                Zip__c='67000',
                BuildingRef__c=building.Id

        );

        insert new List<Project__c>{project1,project2};

        project1.BuildingRef__c = building.Id;
        update project1;

        System.assertEquals('Bordeaux',[select id,City__c from Project__c where Id =: project2.Id].City__c);
        System.assertEquals('Bordeaux',[select id,City__c from Project__c where Id =: project1.Id].City__c);
    }

}