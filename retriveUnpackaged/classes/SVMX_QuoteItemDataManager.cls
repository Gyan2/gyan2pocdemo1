public class SVMX_QuoteItemDataManager {
    
    public static List<SVMXC__Quote__c> serviceQuoteIds(Set<id> serviceQuoteID) {
       List<SVMXC__Quote__c> serviceQuotes =  [SELECT id, SVMX_Special_Approval__c FROM SVMXC__Quote__c WHERE Id IN :serviceQuoteID] ;
        return serviceQuotes ;
    }
    

}