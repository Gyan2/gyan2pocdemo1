/*******************************************************************************************************
Description:
  This class is used to create data for test classes. 
 
Author: Ranjitha S
Date: 02-11-2017

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/
public class SVMX_TestUtility {

    public static Account CreateAccount(string accName, string country, boolean doInsert)
    {
        Account ac = new Account( Name = accName,
                                  BillingCountry = country,
                                  SVMX_Automatic_billing__c='Automatic',
                               
                                  CurrencyIsoCode = 'EUR',
                                  Status__c = 'Active' );    
        if(doInsert)
            insert ac;
        return ac; 
    }
    
     public static Contact CreateContact(id accId, boolean doInsert)
    {
        id accountId = (accId == null)?CreateAccount('Test','Germany', true).id : accId;
        Contact con = new Contact(LastName='Last',
                                  AccountId =accountId,
                                  Language__c = 'EN',
                                  email='Test@test.com',
                                  Status__c = 'Active',
                                  SVMX_Roles__c = 'Quotation Manager');
        if(doInsert)
            insert con;
        return con;
    }
    
    public static SVMXC__Site__c  CreateLocation(string locName, id accId, boolean doInsert)
    {
        id accountId = (accId == null)?CreateAccount('Test','Germany', true).id : accId;
        SVMXC__Site__c location1 = new SVMXC__Site__c(Name = locName,
                                                       SVMXC__Account__c = accountId,
                                                       RecordTypeId = SelectRecordType('SVMXC__Site__c','Standard Location'),
                                                       SVMXC__Street__c = 'Potsdamer',
                                                       SVMXC__city__c = 'Berlin',
                                                       SVMXC__Country__c = 'Germany',
                                                       SVMXC__Zip__c = '10785');
        if(doInsert)
            insert location1;
        return location1;
    }
    
    public static Product2 CreateProduct(string pName, boolean doInsert)
    {
        Product2 pr = new Product2 ( Name =pName,
                                    family = 'Product Line',
                                    ProductCode='CS'); 
        if(doInsert)
            insert pr;
        return pr;
    } 
    
    
    public static SVMXC__Warranty__c CreateWarranty(id IBid, boolean doInsert)
    {
        
        SVMXC__Warranty__c warr = new SVMXC__Warranty__c();
        warr.SVMXC__Installed_Product__c = IBid;
        warr.SVMXC__Start_Date__c = system.today();
        if(doInsert)
            insert warr;
        return warr;
    } 
    
    public static Id SelectRecordType(string sObj,string recordTypeName)
    {
        Schema.DescribeSObjectResult objSchemaDesc = Schema.getGlobalDescribe().get(sObj).getDescribe();
        Map<string,schema.RecordTypeInfo> mapRecType =objSchemaDesc.getRecordTypeInfosByName();
        Schema.RecordTypeInfo objRecTypeName = mapRecType.get(recordTypeName);
        return objRecTypeName.getRecordTypeId();
    }
    
    public static SVMXC__Service_Order__c CreateWorkOrder (id accId, id locId, id prodId, string orderStatus, string orderType, string recordTypeName, id scId, id techId, id caseId,boolean doInsert)
    {
        id accountId = (accId == null)?CreateAccount('Test','Germany', true).id : accId;
        id locationId = (locId == null)?CreateLocation('Test',accountId, true).id : locId;
        id productId = (prodId == null)?CreateProduct('Prod',true).id:prodId;
        
        SVMXC__Service_Order__c svo = new SVMXC__Service_Order__c (
                                        SVMXC__Country__c = 'Germany',
                                        SVMXC__Company__c = accountId , 
                                        SVMXC__Order_Status__c = orderStatus,
                                        SVMXC__Priority__c ='Medium',
                                        SVMXC__Order_Type__c=orderType,
                                        SVMXC__Product__c = productId,
                                        RecordTypeId = SelectRecordType('SVMXC__Service_Order__c','Global Std'),
                                        SVMXC__Service_Contract__c = scId,
                                        SVMXC__Group_Member__c = techId,
                                        SVMXC__Case__c = caseId,
                                        SVMXC__Site__c = locationId,
                                        SVMX_Auto_Verified__c=false,
                                        SVMX_Work_Detail_Created__c=false,
                                        SVMXC__Purpose_of_Visit__c = 'Technician Onsite'  );
        
        if(doInsert)
            insert svo;
        return svo;
    }
    
    public static SVMXC__Service_Order_Line__c CreateWorkOrderDetail(id accId, id locId, id prodId,string recordTypeName, id woId, DateTime dtTime, id scId, id techId, id caseId ,string lineType, string billingType, boolean doInsert)
    {
         id workOrderId = (woId==null)?CreateWorkOrder(accId,locId,prodId,'open', 'Reactive','Global Std',scId,techId,caseId, true).id:woId;
         SVMXC__Service_Order_Line__c oli = new SVMXC__Service_Order_Line__c(RecordTypeId = SelectRecordType('SVMXC__Service_Order_Line__c',recordTypeName),
                                                    SVMXC__Service_Order__c = workOrderId, 
                                                    SVMXC__Start_Date_and_Time__c = dtTime, 
                                                    SVMXC__Product__c = prodId,
                                                    SVMXC__Line_Type__c = lineType,
                                                    SVMX_Billing_Type__c = billingType);
        if(doInsert)
            insert oli;
        return oli;
    }
    
    public static SVMXC__Service_Order_Line__c CreateWorkOrderDetailEstimate(id accId, id locId, id prodId,string recordTypeName, id woId, DateTime dtTime, id scId, id techId, id caseId ,string lineType, string billingType, decimal noOfTech, boolean ooh, boolean doInsert)
    {
         id workOrderId = (woId==null)?CreateWorkOrder(accId,locId,prodId,'open', 'Reactive','Global Std',scId,techId,caseId, true).id:woId;
         SVMXC__Service_Order_Line__c oli = new SVMXC__Service_Order_Line__c(RecordTypeId = SelectRecordType('SVMXC__Service_Order_Line__c',recordTypeName),
                                                    SVMXC__Service_Order__c = workOrderId, 
                                                    SVMXC__Start_Date_and_Time__c = dtTime, 
                                                    SVMXC__Product__c = prodId,
                                                    SVMXC__Line_Type__c = lineType,
                                                    SVMX_Billing_Type__c = billingType,
                                                    SVMX_No_of_Technicians__c = noOfTech,
                                                    SVMX_Out_Of_Hours__c = ooh);
        if(doInsert)
            insert oli;
        return oli;
    }
    
    public static SVMXC__Service_Contract__c CreateServiceContract(id accId,string sName, string recordTypeName, id contactId, boolean doInsert)
    {
        id accountId = (accId == null)?CreateAccount('Test','Norway', true).id : accId;
        
        SVMXC__Service_Contract__c sc = new SVMXC__Service_Contract__c();
        
        sc.SVMXC__Company__c=accountId;
        sc.SVMXC__Contact__c = contactId;
        sc.SVMXC__Start_Date__c=system.today();
        sc.SVMXC__End_Date__c=system.today().addMonths(7);
        sc.SVMX_Country__c = 'Norway';
        sc.Name = sName;
        sc.RecordTypeId = SelectRecordType('SVMXC__Service_Contract__c', recordTypeName);
        
        if(doInsert)
            insert sc;
        return sc;
    }
    
    public static SVMXC__PM_Plan__c CreatePMPlan(id scId,string sName, boolean doInsert)
    {
        id servContractId = (scId == null)?CreateServiceContract(null,'Test Contract','Global Std',null,true).id : scId;
        
        SVMXC__PM_Plan__c sc = new SVMXC__PM_Plan__c();
        
        sc.SVMXC__Service_Contract__c = servContractId;
        sc.Name = sName;
        
        if(doInsert)
            insert sc;
        return sc;
    }

    public static SVMX_Rate_Tiers__c CreateRateTier(id scId,id accId, boolean ooh, boolean doInsert)
    {
        SVMX_Rate_Tiers__c newTier = new SVMX_Rate_Tiers__c( SVMX_Service_Contract__c = scId,
                                                                SVMX_Business_Hours__c = SelectBusinessHours().id,
                                                                SVMX_Out_of_Hours__c = ooh,
                                                                SVMX_Rate__c = 'Rate 1' );
        if(doInsert)
            insert newTier;
        return newTier;
    
    }
    
    public static BusinessHours SelectBusinessHours ()
    {
        return[select id from BusinessHours where isDefault =true];
    }
    
    public static SVMXC__Service_Group_Members__c CreateTechnician (string tName, id sfId, id svg, boolean doInsert)
    {
        Id svgId = (svg== null)?CreateServiceGroup('Test',true).id:svg;
        SVMXC__Service_Group_Members__c tech = new SVMXC__Service_Group_Members__c();
        tech.Name=tName;
        tech.SVMXC__Service_Group__c=svgId;
        tech.SVMXC__Salesforce_User__c = sfId;
                    
        if(doInsert)
            insert tech;
        return tech;
    }
    
    public static SVMXC__Service_Group__c CreateServiceGroup(string gName, boolean doInsert)
    {
        SVMXC__Service_Group__c svg = new SVMXC__Service_Group__c();
        svg.Name=gName;
        if(doInsert)
            insert svg;
        
        return svg;
    }
              
    public static Case CreateCase(id accId, id cId, string cStatus, boolean doInsert)
    {
        id accountId = (accId == null)?CreateAccount('Test','Germany', true).id : accId;
        id contactId = (cId == null)?CreateContact(accountId, true).id : cId;
        
        Case caseObj = new Case(AccountId = accountId,
                                ContactId = contactId, 
                                Status = cStatus,
                                Priority = 'Medium',
                                Origin = 'Email',
                                CurrencyIsoCode = 'EUR',
                                SVMXC__Perform_Auto_Entitlement__c = True);
        if(doInsert)
            insert caseObj;
        return caseObj;
    }
    
    public static SVMXC__Service_Template__c CreateWarrentTerms(boolean doInsert)
    {
        //id accountId = (accId == null)?CreateAccount('Test','Germany', true).id : accId;
                
        SVMXC__Service_Template__c wt = new SVMXC__Service_Template__c();
        wt.Name = '123Test';
        wt.SVMXC__Coverage_Effective_From2__c='SVMXC__Date_Installed__c';
        //wt.SVMXC__Material_Covered__c = 100; 
        if(doInsert)
            insert wt;
        return wt;
    }
    public static SVMXC__Service_Template_Products__c CreateApplicationProduct(id wartemId,id prodId, boolean doInsert)
    {
    
    
        id warrentyTermId = (wartemId == null)?CreateWarrentTerms(true).id : wartemId;
        id productId = (prodId == null)?CreateProduct('Prod',true).id:prodId;
                
        SVMXC__Service_Template_Products__c ap = new SVMXC__Service_Template_Products__c();
        ap.SVMXC__Service_Template__c=wartemId;
        ap.SVMXC__Product__c=productId;
        //ap.SVMXC__Material_Covered__c = 100; 
        if(doInsert)
            insert ap;
        return ap;
    }
    
    public static SVMXC__Installed_Product__c CreateInstalledProduct(id accId, id pPId, id locId, boolean doInsert)
    {
        id accountId = (accId == null)?CreateAccount('Test','Germany', true).id : accId;
                
        SVMXC__Installed_Product__c ib = new SVMXC__Installed_Product__c();
        ib.SVMXC__Serial_Lot_Number__c = '123Test';
        ib.SVMXC__Status__c='Installed';
        ib.SVMXC__Company__c = accountId;
        ib.SVMXC__Product__c = pPId;
        ib.SVMXC__Site__c = locId;
        ib.SVMXC__Date_Installed__c = system.today();
        
        if(doInsert)
            insert ib;
        return ib;
    }

    
    public static User CreateUser(string uLastName, id profileId, boolean doInsert)
    {
        User u = new User(Alias = uLastName.substring(3) , Email=uLastName + '@testorg.com', 
                            EmailEncodingKey='UTF-8', LastName=uLastName, LanguageLocaleKey='en_US', 
                            LocaleSidKey='en_US', ProfileId = profileId, TimeZoneSidKey='America/Los_Angeles',
                            UserName= uLastName + '@testorg.com', SalesOrg__c = '4030');
        if(doInsert)
            insert u;
        
        return u;    
    }
    
    public static Profile SelectProfile(string profileName)
    {
        return ([Select id From Profile where name = :profileName LIMIT 1].get(0));
    }


    public static Opportunity CreateOpportunity (Id accId , String name, String stage,Decimal probability, Decimal amount,boolean doInsert)
    {
        id accountId = (accId == null)?CreateAccount('Test','Germany', true).id : accId;

       Opportunity opp = new Opportunity(Name = name,
                                         AccountId = accountId,
                                         StageName = stage,
                                         Type = 'New',
                                         Probability = probability,
                                         Amount = amount,
                                         CurrencyIsoCode = 'EUR',
                                         ProductCluster__c = 'Services',
                                         RecordTypeId = SelectRecordType('Opportunity', 'DACH'),
                                         CloseDate = Date.today() + 30 ) ;  
        if(doInsert)
            insert opp;

        return opp;
    }
    
    public static SVMXC__Service_Pricebook__c CreateServicePricebook(string pName, boolean doInsert)
    {
        SVMXC__Service_Pricebook__c spb = new SVMXC__Service_Pricebook__c();
        spb.Name = pName;
        spb.SVMXC__Active__c = true;
        spb.SVMXC__Description__c = 'Test Pricebook';
        if(doInsert)
            insert spb;
        return spb;
    }
     
    public static SVMXC__Service_Pricebook_Entry__c CreateServicePricebookEntry (id actId, id pbId, id prodId, boolean doInsert)
    {
        id aMId = (actId == null)?CreateActivityMaster('Tech1-Rate 1', true).id : actId;
        id productId = (prodId == null)?CreateProduct('Prod',true).id:prodId;
        id pricebookId = (pbId == null)?CreateServicePricebook('Test', true).id : pbId;
        
        SVMXC__Service_Pricebook_Entry__c pbe = new SVMXC__Service_Pricebook_Entry__c();
        pbe.SVMXC__Activity__c = amId;
        pbe.SVMXC__Entry_Type__c = 'Labor';
        pbe.SVMXC__Regular_Rate__c = 15.00;
        pbe.SVMXC__Unit__c = 'Flat Rate';
        pbe.SVMXC__Price_Book__c = pricebookId;
        
        if(doInsert)
            insert pbe;
        return pbe;
    }
    
    public static SVMXC__Activity_Master__c CreateActivityMaster (string activityType, boolean doInsert)
    {
        SVMXC__Activity_Master__c sam = new SVMXC__Activity_Master__c(SVMXC__Activity_Type__c = activityType, SVMXC__Description__c = 'Test');
        if(doInsert)
            insert sam;
        return sam;
    }
    
    public static SVMXC__Activity_Product__c CreateActivityProduct (id actMasterId, id prodId, boolean doInsert )
    {
        id aMId = (actMasterId == null)?CreateActivityMaster('Tech1-Rate 1', true).id : actMasterId;
        id productId = (prodId == null)?CreateProduct('Prod',true).id:prodId;
        
        SVMXC__Activity_Product__c sap = new SVMXC__Activity_Product__c();
        sap.SVMXC__Activity__c = actMasterId;
        sap.SVMXC__Product__c = productId;
        //sap.SVMXC__Product_Family__c = 'Prod';
        //sap.SVMXC__Product_Line__c = 'Prod';
        
        if(doInsert)
            insert sap;
        
        return sap;
    }
    
    public static Attachment CreatePdfAttachment(string name, id parentId, boolean doInsert)
    {
        Attachment att = new Attachment();
        att.Body = Blob.valueOf('Test Attachment Body');
        att.ContentType = 'application/x-pdf';
        att.ParentId = parentId;
        att.Name = name + '.pdf';
        if(doInsert)
            insert att;
        return att;
        
    }
  
    public static SVMXC__Service_Plan__c CreateServicePlan(string sName,string planType, String noOfVisits,boolean doInsert)
    {
        SVMXC__Service_Plan__c servicePlan = new SVMXC__Service_Plan__c();

        servicePlan.Name = sName;

        if(doInsert)
            insert servicePlan;

        return servicePlan;
    }
    
    public static SVMX_Custom_Feature_Enabler__c CreateCustomFeature(string country, boolean doInsert)
    {
        SVMX_Custom_Feature_Enabler__c cf = new SVMX_Custom_Feature_Enabler__c();
        cf.name = country;
        cf.Custom_Labor_Activity__c = true;
        cf.SVMX_Travel_Activity_Types__c =true;
        cf.Auto_Verification__c=true;
        cf.Country_Code__c='0044';
        cf.SVMX_SAP_Parts_Price__c=true;
        
        if(doInsert)
            insert cf;
        return cf;
    }
    
    public static SVMXC__Quote__c CreateServiceQuote(id accId,id woId, id oppId, string status, boolean doInsert)
    {
        SVMXC__Quote__c sq = new SVMXC__Quote__c();
        sq.SVMXC__Company__c = accId;
        sq.SVMXC__Service_Order__c = woId;
        sq.SVMXC__Status__c = status;
        sq.SVMXC__Quote_Amount2__c = 112;
        sq.SVMX_Opportunity__c = oppId;
        
        if(doInsert)
            insert sq;
        return sq;
    }    
    
     public static SVMXC__Quote_Line__c CreateQuoteItems(id ipID, id quoteId,string lineType, boolean doInsert)
    {
        SVMXC__Quote_Line__c qi = new SVMXC__Quote_Line__c();
        qi.SVMX_Installed_Product__c = ipID;
        qi.SVMXC__Line_Type__c = lineType;
        qi.SVMXC__Quantity2__c = 1;
        qi.SVMXC__Quote__c = quoteId;
        
        if(doInsert)
            insert qi;
        return qi;
    }    
 
    public static SVMXC__Timesheet__c CreateTimeSheet(string status, boolean doInsert)
    {
        SVMXC__Timesheet__c TS = new SVMXC__Timesheet__c();
        TS.SVMXC__Status__c = status;
         TS.SVMX_Current_Week__c=true;
        TS.SVMXC__Start_Date__c = system.today();
        TS.SVMXC__End_Date__c = system.today();
        
        if(doInsert)
            insert TS;
        return TS;
    }    
    
    public static SVMXC__Service_Contract_Sites__c CreateServicecontractSite(Id sContrId,String billingSchedule,Date startDate,boolean doInsert)
    {
        SVMXC__Service_Contract_Sites__c SCS = new SVMXC__Service_Contract_Sites__c();
        SCS.SVMXC__Service_Contract__c=sContrId;
        SCS.SVMX_Pricing_Methos__c = 'Time Based';
        SCS.SVMX_Billing_schedule__c=billingSchedule;
        SCS.SVMXC__End_Date__c=system.today();
        SCS.SVMXC__Start_Date__c=startDate; //system.today();
       
        if(doInsert)
            insert SCS;
        return SCS;
    }
    public static SVMXC__Service_Contract_Products__c CreateServiceContractProduct(Id sContrId,String billingSchedule,Date startDate, boolean doInsert)
    {
        SVMXC__Service_Contract_Products__c SCP = new SVMXC__Service_Contract_Products__c();
        SCP.SVMXC__Service_Contract__c=sContrId;
        SCP.SVMX_Pricing_Method__c = 'Time Based';
        SCP.SVMX_Billing_schedule__c=billingSchedule;
        SCP.SVMXC__End_Date__c=system.today();
        SCP.SVMXC__Start_Date__c =startDate; //system.today();
      
        
        if(doInsert)
            insert SCP;
        return SCP;
    }
     public static SVMX_Contract_Line_Items__c CreateContractLineItem(Id sContrId,String AllLocationspervisit,Date startDate, boolean doInsert)
    {
        SVMX_Contract_Line_Items__c CLI = new SVMX_Contract_Line_Items__c();
        CLI.SVMX_Service_Contract__c=sContrId;
        CLI.SVMX_Billing_schedule__c=AllLocationspervisit;
        CLI.SVMX_Contract_End__c=system.today();
        CLI.SVMX_Contract_Start__c=startDate; //system.today();
      
        
        if(doInsert)
            insert CLI;
        return CLI;
    }         
      public static SVMX_Interface_Trigger_Configuration__c CreateInterfaceTrigger(String objectval,boolean doInsert)
    {
        SVMX_Interface_Trigger_Configuration__c TI = new SVMX_Interface_Trigger_Configuration__c();
        TI.SVMX_Country__c='India';
        TI.SVMX_Object__c=objectval;
        TI.SVMX_Trigger_on_Field__c='SVMXC__Order_Type__c;SVMXC__Scheduled_Date_Time__c;CurrencyIsoCode';

        if(doInsert)
            insert TI;
        return TI;
    }    
       public static SVMX_Billing_Rules__c CreateBillingRule(String objectval,string fields,String value,String lineType,boolean doInsert)
    {
        SVMX_Billing_Rules__c BR = new SVMX_Billing_Rules__c();
        BR.SVMX_Country__c='India';
        BR.SVMX_Object__c=objectval;
        BR.SVMX_Field__c=fields;
        BR.SVMX_Value__c=value;
        BR.SVMX_Line_Type__c=lineType;

        if(doInsert)
            insert BR;
        return BR;
    }    
     public static SVMXC__PM_Schedule_Definition__c CreatePMDefinition(Id pmPlan,boolean doInsert)
    {
        SVMXC__PM_Schedule_Definition__c PSD = new SVMXC__PM_Schedule_Definition__c();
        PSD.SVMXC__PM_Plan__c=pmPlan;
       

        if(doInsert)
            insert PSD;
        return PSD;
    }  
    
    public static SVMX_Sales_Office__c createSalesOffice(string country, id exProd, id loProd, id travProd, boolean doInsert){
        SVMX_Sales_Office__c SO = new SVMX_Sales_Office__c();
        SO.SVMX_Country__c = country;
        SO.SVMX_Expense_Product__c = exProd;
        SO.SVMX_Labor_Product__c = loProd;
        SO.SVMX_Travel_Labor_Product__c = travProd;
        
        if(doInsert)
            insert SO;
        return SO;        
    }
  
}