/**
 * Created by trakers on 13.06.18.
 */

public with sharing class SAPPO_Document {
    public class DocumentReadRequest {
        public SAPPO_Document.ArchiveObject ArchiveObject;
        public SAPPO_Document.DocumentReadRequestDocumentingParty DocumentingParty;
        public String CosmosCorrelation;
        private String[] ArchiveObject_type_info = new String[]{'ArchiveObject','urn:dorma:esm:esi:document:1.0',null,'1','1','false'};
        private String[] DocumentingParty_type_info = new String[]{'DocumentingParty','urn:dorma:esm:esi:document:1.0',null,'1','1','false'};
        private String[] CosmosCorrelation_type_info = new String[]{'CosmosCorrelation','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'ArchiveObject','DocumentingParty','CosmosCorrelation'};
    }
    public class ArchiveDocument {
        public String ArchiveId;
        public String ArchiveObject;
        public String DocumentType;
        public String DocumentId;
        public String DocumentName;
        public String Payload;
        public Boolean AvoidDoubleEntry;
        public String ArchiveDate;
        public String ArchiveNumber;
        private String[] ArchiveId_type_info = new String[]{'ArchiveId','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ArchiveObject_type_info = new String[]{'ArchiveObject','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] DocumentType_type_info = new String[]{'DocumentType','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] DocumentId_type_info = new String[]{'DocumentId','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] DocumentName_type_info = new String[]{'DocumentName','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] Payload_type_info = new String[]{'Payload','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] AvoidDoubleEntry_type_info = new String[]{'AvoidDoubleEntry','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ArchiveDate_type_info = new String[]{'ArchiveDate','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ArchiveNumber_type_info = new String[]{'ArchiveNumber','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'ArchiveId','ArchiveObject','DocumentType','DocumentId','DocumentName','Payload','AvoidDoubleEntry','ArchiveDate','ArchiveNumber'};
    }
    public class DocumentReadRequestDocumentingParty {
        public SAPPO_EsmEdt.PartyInternalID InternalID;
        public SAPPO_EsmEdt.PartyStandardID StandardID;
        private String[] InternalID_type_info = new String[]{'InternalID','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] StandardID_type_info = new String[]{'StandardID','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'InternalID','StandardID'};
    }
    public class ObjectReference {
        public String ID;
        public String ExternalID;
        public String Type_x;
        private String[] ID_type_info = new String[]{'ID','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ExternalID_type_info = new String[]{'ExternalID','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] Type_x_type_info = new String[]{'Type','urn:dorma:esm:esi:document:1.0',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'ID','ExternalID','Type_x'};
    }
    public class DocumentReadResponse {
        public SAPPO_Document.ArchiveObject ArchiveObject;
        public SAPPO_Document.DocumentReadRequestDocumentingParty DocumentingParty;
        public String CosmosCorrelation;
        private String[] ArchiveObject_type_info = new String[]{'ArchiveObject','urn:dorma:esm:esi:document:1.0',null,'1','1','false'};
        private String[] DocumentingParty_type_info = new String[]{'DocumentingParty','urn:dorma:esm:esi:document:1.0',null,'1','1','false'};
        private String[] CosmosCorrelation_type_info = new String[]{'CosmosCorrelation','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'ArchiveObject','DocumentingParty','CosmosCorrelation'};
    }
    public class ArchiveDocuments {
        public SAPPO_Document.ArchiveDocument[] item;
        private String[] item_type_info = new String[]{'item','urn:dorma:esm:esi:document:1.0',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ObjectReferences {
        public SAPPO_Document.ObjectReference[] item;
        private String[] item_type_info = new String[]{'item','urn:dorma:esm:esi:document:1.0',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ArchivePartners {
        public SAPPO_Document.ArchivePartner[] item;
        private String[] item_type_info = new String[]{'item','urn:dorma:esm:esi:document:1.0',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'item'};
    }
    public class ArchivePartner {
        public String Role;
        public String Number_x;
        private String[] Role_type_info = new String[]{'Role','urn:dorma:esm:esi:document:1.0',null,'1','1','false'};
        private String[] Number_x_type_info = new String[]{'Number','urn:dorma:esm:esi:document:1.0',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'Role','Number_x'};
    }
    public class ArchiveObject {
        public String ObjectType;
        public String ObjectId;
        public String ExternalID;
        public String ObjectOrg;
        public String ObjectPartner;
        public String ObjectReference;
        public Boolean InsertExternalBarcode;
        public SAPPO_Document.ArchiveDocuments ArchiveDocument;
        public SAPPO_Document.ArchivePartners ArchivePartner;
        public SAPPO_Document.ObjectReferences Reference;
        private String[] ObjectType_type_info = new String[]{'ObjectType','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ObjectId_type_info = new String[]{'ObjectId','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ExternalID_type_info = new String[]{'ExternalID','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ObjectOrg_type_info = new String[]{'ObjectOrg','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ObjectPartner_type_info = new String[]{'ObjectPartner','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ObjectReference_type_info = new String[]{'ObjectReference','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] InsertExternalBarcode_type_info = new String[]{'InsertExternalBarcode','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ArchiveDocument_type_info = new String[]{'ArchiveDocument','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] ArchivePartner_type_info = new String[]{'ArchivePartner','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] Reference_type_info = new String[]{'Reference','urn:dorma:esm:esi:document:1.0',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'urn:dorma:esm:esi:document:1.0','false','false'};
        private String[] field_order_type_info = new String[]{'ObjectType','ObjectId','ExternalID','ObjectOrg','ObjectPartner','ObjectReference','InsertExternalBarcode','ArchiveDocument','ArchivePartner','Reference'};
    }
}