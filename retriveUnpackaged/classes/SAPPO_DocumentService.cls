/**
 * Created by trakers on 14.06.18.
 */

public with sharing class SAPPO_DocumentService {

    public class SAPPO_DocumentServiceException extends Exception {}

    public SAPPO_DocumentService(){

    }

    public class InvoiceResult {
        @AuraEnabled public Integer status;
        @AuraEnabled public String message;
        @AuraEnabled public String data;
    }

    public InvoiceResult getInvoicePayLoadFromSAP(String invoiceNumber){

        InvoiceResult result = new InvoiceResult();
            result.status = 200;
        try{

            //Archive Object
            SAPPO_Document.ArchiveObject archiveObject = new SAPPO_Document.ArchiveObject();
                                         archiveObject.ObjectId = invoiceNumber;

            SAPPO_Document.DocumentReadResponse response = new SAPPO_DocumentRequest.HTTPS_Port().ReadSync(archiveObject,null,null);

            for(SAPPO_Document.ArchiveDocument item : response.ArchiveObject.ArchiveDocument.item){
                if(item.Payload != null){
                    result.data = JSON.serialize(item);
                    continue;
                }
            }


            // Check if the invoice exist in SAP
            if(result.data == null){
                throw new SAPPO_DocumentServiceException('There is no invoice in SAP');
            }

        }catch(Exception e){
            result.status = 400;
            result.message = 'Line '+e.getLineNumber()+' | '+e.getMessage();
        }


        return result;
    }
}