/************************************************************************************************************
Description: Data Manager for Record type. All the SOQL queries for record type ID are performed here.

Dependency: 
 
Author: Ranjitha S
Date: 23-10-2017

Modification Log: 
Date            Author          Modification Comments

--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_RecordTypeDataManager {
    
    public static List<RecordType> RecordTypes =new List<RecordType>();
    
    public static List<RecordType> recordTypeQuery(){
        String[] customSobjects = new String[]{'SVMXC__Service_Order__c', 'SVMXC__Service_Order_Line__c', 'SVMXC__Service_Contract__c','Case','Account','SVMXC__RMA_Shipment_Order__c','SVMXC__RMA_Shipment_Line__c','SVMXC__Stock_History__c'};
        RecordTypes = [select id, sObjectType, DeveloperName from RecordType where sObjectType IN: customSobjects];
        return recordTypes;
    }
    
    public static id GetRecordTypeID( string name, string obj){
        Id recordtypeId =null;
        
        if(RecordTypes.isEmpty())
            recordTypeQuery();
        
        if(!RecordTypes.isEmpty()){
            for(RecordType rt: RecordTypes){
                if(rt.sObjectType == obj && rt.DeveloperName == name){
                    recordtypeId = rt.id;
                    break;
                }
            }
        }
        return recordtypeId;
    }
}