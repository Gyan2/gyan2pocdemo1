global class SVMX_ActiveContractPmPlanBatch_Scheduler implements Schedulable {
    // Execute at regular intervals
    global void execute(SchedulableContext sc){
      SVMX_ActivatedContractsPmPlanBatch acpBatch = new SVMX_ActivatedContractsPmPlanBatch();
      Database.executebatch(acpBatch, 20);
    }
}