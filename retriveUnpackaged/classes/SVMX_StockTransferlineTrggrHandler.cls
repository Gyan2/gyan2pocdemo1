/*Description: Trigger handler for stock Transfer Line Trigger

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger: SVMX_StockTransferlineTrgg.cls
    class:SVMX_AutomaticInventory.cls
    
    
 
Author: Yogesh
Date: 15-1-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

public class SVMX_StockTransferlineTrggrHandler extends SVMX_TriggerHandler {

    private list<SVMXC__Stock_Transfer_Line__c> newSTLList;
    private list<SVMXC__Stock_Transfer_Line__c> oldSTLList;
    private Map<Id, SVMXC__Stock_Transfer_Line__c> newSTLMap;
    private Map<Id, SVMXC__Stock_Transfer_Line__c> oldSTLMap;
    
    public SVMX_StockTransferlineTrggrHandler() {
        this.newSTLList = (list<SVMXC__Stock_Transfer_Line__c>) Trigger.new;
        this.oldSTLList = (list<SVMXC__Stock_Transfer_Line__c>) Trigger.old;
        this.newSTLMap = (Map<Id, SVMXC__Stock_Transfer_Line__c>) Trigger.newMap;
        this.oldSTLMap = (Map<Id, SVMXC__Stock_Transfer_Line__c>) Trigger.oldMap;
    }
    public override void beforeInsert(){

        set<Id> sourceLocation = new  set<Id>();
        set<Id> productId= new  set<Id>();
        List<SVMXC__Product_Stock__c> productStock = new List<SVMXC__Product_Stock__c> ();
        Map<Id,product2> productMap = new Map<Id,product2>();
        Map<id,Map<Id,SVMXC__Product_Stock__c>> MaplocationIdAndProductStockMap = new Map<id,Map<Id,SVMXC__Product_Stock__c>>();
        

        for(SVMXC__Stock_Transfer_Line__c stockTransferLine:newSTLList){
                        sourceLocation.add(stockTransferLine.SVMXC_Sender_Stocking_Location__c);
                        productId.add(stockTransferLine.SVMXC__Product__c);
        }

        if(sourceLocation.size() > 0){

            if(productId.size() > 0){
              productStock = [select id,name,SVMXC__Location__c,SVMXC__Status__c,SVMXC__Available_Qty__c,SVMXC__Product__c from SVMXC__Product_Stock__c where SVMXC__Location__c in:sourceLocation And SVMXC__Product__c in :productId And SVMXC__Status__c ='Available'];
            }
        } 

        if(productId.size() > 0){
                productMap = new Map<Id,product2>([SELECT Id,Name,SVMXC__Stockable__c FROM product2 
                                                            WHERE id IN : productId]);
        }

        if(productStock.size() > 0){
            
            for(SVMXC__Product_Stock__c ps:productStock){

                Map<Id,SVMXC__Product_Stock__c> tempProductStockMap = new Map<Id,SVMXC__Product_Stock__c>();

                if(MaplocationIdAndProductStockMap.containsKey(ps.SVMXC__Location__c)) {

                 tempProductStockMap = MaplocationIdAndProductStockMap.get(ps.SVMXC__Location__c);

                }

                tempProductStockMap.put(ps.SVMXC__Product__c,ps);
                System.debug('Product**** '+tempProductStockMap);
                System.debug('Product1223**** '+ps.SVMXC__Product__c);
                System.debug('Location**** '+ps.SVMXC__Location__c);
                MaplocationIdAndProductStockMap.put(ps.SVMXC__Location__c,tempProductStockMap);
            }
        }

        for(SVMXC__Stock_Transfer_Line__c stockTransferLine:newSTLList){

            if(MaplocationIdAndProductStockMap.containsKey(stockTransferLine.SVMXC_Sender_Stocking_Location__c)){
                 Map<Id,SVMXC__Product_Stock__c> tempProductStockMap=MaplocationIdAndProductStockMap.get(stockTransferLine.SVMXC_Sender_Stocking_Location__c);
                 system.debug('tempProductStockMap*** '+tempProductStockMap);
                 if(!tempProductStockMap.containsKey(stockTransferLine.SVMXC__Product__c)){

                    stockTransferLine.addError(productMap.get(stockTransferLine.SVMXC__Product__c).Name + ' - Product selected does not exist in Source Location');
                 }
                 if(tempProductStockMap.containsKey(stockTransferLine.SVMXC__Product__c) && tempProductStockMap.get(stockTransferLine.SVMXC__Product__c).SVMXC__Available_Qty__c < stockTransferLine.SVMXC__Quantity_Transferred2__c ){
                    
                    stockTransferLine.addError(productMap.get(stockTransferLine.SVMXC__Product__c).Name+ ' - Sufficient Quantity not availble for Product selected');
                 }
            }else{
                stockTransferLine.addError(productMap.get(stockTransferLine.SVMXC__Product__c).Name + ' - Product selected does not exist in Source Location');
            }
        }
             
    }
    public override void afterInsert(){

        set<id> sourceLocIds = new set<id> ();
        set<id> nonApprovalReqLocIds = new set<id> ();
        list<SVMXC__Site__c> loclist = new  list<SVMXC__Site__c>();
        set<id> stockTransferIds = new set<id> ();
        set<id> stockTransferupdateIds = new set<id> ();
        set<id> stockTransIdForAdjustments = new set<id> ();
        list<SVMXC__Stock_Transfer__c> stockTransferUpdateList = new  list<SVMXC__Stock_Transfer__c>();
        list<SVMXC__Stock_Transfer__c> stockTransferList = new  list<SVMXC__Stock_Transfer__c>();

        

        for(SVMXC__Stock_Transfer_Line__c stl : newSTLList){

            sourceLocIds.add(stl.SVMXC_Sender_Stocking_Location__c);
            system.debug('stl--->'+stl);
            //stockTransIds.add(stl.SVMXC__Stock_Transfer__c);
        }

       /* stockTransferList = [SELECT Id,Name FROM SVMXC__Stock_Transfer__c 
                             WHERE Id IN : stockTransIds AND SVMXC_Approved__c = true];

        for(SVMXC__Stock_Transfer__c stl : stockTransferList) {

            stockTransferIds.add(stl.Id);    
        }  */                   

        if(sourceLocIds.size() > 0){
        
            system.debug('sourceLocIds--->'+sourceLocIds);
            loclist = SVMX_LocationDataManager.LocationQuery(sourceLocIds);
        }

        if(loclist.size() > 0){
            
            system.debug('loclist--->'+loclist);
            for(SVMXC__Site__c loc:loclist){
                
                system.debug('loc--->'+loc);
                nonApprovalReqLocIds.add(loc.id);
            }
        }

        if(nonApprovalReqLocIds.size() > 0){
            
            system.debug('nonApprovalReqLocIds--->'+nonApprovalReqLocIds);
            for(SVMXC__Stock_Transfer_Line__c stl: newSTLList) {
                
                system.debug('stl1--->'+stl);
                if(nonApprovalReqLocIds.contains(stl.SVMXC_Sender_Stocking_Location__c)){
                    
                    if(!stockTransferIds.contains(stl.SVMXC__Stock_Transfer__c)){
                          stockTransferIds.add(stl.SVMXC__Stock_Transfer__c);
                    }      
                }
            }

            stockTransferList = [SELECT Id,Name,SVMXC_Approved__c FROM SVMXC__Stock_Transfer__c 
                             WHERE Id IN : stockTransferIds ];


            for(SVMXC__Stock_Transfer__c stl : stockTransferList) {
                
                if(stl.SVMXC_Approved__c == False){
                    
                    stockTransferupdateIds.add(stl.Id);    
                }else{
                    
                    stockTransIdForAdjustments.add(stl.Id);
                }
            }                   

            if(stockTransferupdateIds.size() > 0){

                for(id stocTranId : stockTransferupdateIds){

                   SVMXC__Stock_Transfer__c stockTransfer = new SVMXC__Stock_Transfer__c( id = stocTranId , SVMXC_Approved__c = true);
                   stockTransferUpdateList.add(stockTransfer);
                }  

            }


        }

        if(stockTransferUpdateList.size() > 0){
            
            system.debug('stockTransferUpdateList--->'+stockTransferUpdateList);
            update stockTransferUpdateList;
        }

        if(stockTransIdForAdjustments.size() > 0){
            SVMX_StockTransferserviceManager.InventoryHandler(stockTransIdForAdjustments);
        }
       
    }

    public override void afterupdate(){
        
        list<SVMXC__Stock_Transfer_Line__c> posttoInventryStlList = new  list<SVMXC__Stock_Transfer_Line__c>();

        for(SVMXC__Stock_Transfer_Line__c stl : newSTLList){

            system.debug('stl--->'+stl);

                if( stl.SVMXC__Posted_To_Inventory__c == true && oldSTLMap.get(stl.id).SVMXC__Posted_To_Inventory__c != stl.SVMXC__Posted_To_Inventory__c ){

                        system.debug('stl1--->'+stl);
                        posttoInventryStlList.add(stl);

                }
        } 
        system.debug('posttoInventryStlList--->'+posttoInventryStlList);
        if( posttoInventryStlList.size() >0){

                system.debug('posttoInventryStlList.size() --->'+posttoInventryStlList.size());
                SVMX_InventoryManagement AutomaticInven=new SVMX_InventoryManagement();
                AutomaticInven.postToInventoryFromStockTransferLine(posttoInventryStlList);
        }                 
    } 
        
}