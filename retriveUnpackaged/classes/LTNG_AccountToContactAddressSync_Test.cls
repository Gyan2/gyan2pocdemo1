@isTest
public class LTNG_AccountToContactAddressSync_Test {

    @isTest
    public static void simpleTest() {
        Account a1 = new Account(
                SAPNumber__c = '123456789',
                //SAPInstance__c = 'PS8',
                ExternalId__c = 'PS8-123456789',
                Name = 'Test Account',
                BillingStreet = 'Test street 1',
                BillingPostalCode = '1234',
                BillingCountryCode = 'CH',
                Language__c = 'DE'
        );
        insert a1;
        insert new List<Contact> {
                new Contact(
                        Account = new Account(ExternalId__c = 'PS8-123456789'),
                        LastName = 'Contact 1',
                        Language__c = 'DE'
                ),
                new Contact(
                        Account = new Account(ExternalId__c = 'PS8-123456789'),
                        LastName = 'Contact 2',
                        Language__c = 'IT'
                )
        };

        Test.startTest();

        LTNG_AccountToContactAddressSync.AccountWrapper wrapper = LTNG_AccountToContactAddressSync.auraGetAccountContactBillingInformation(a1.Id);

        System.assertNotEquals(null, wrapper.account);
        System.assertNotEquals(null, wrapper.account.Contacts);
        System.assertEquals(2,wrapper.account.Contacts.size());

        wrapper.account.Contacts[0].MailingStreet = 'Test 123';

        LTNG_AccountToContactAddressSync.auraSetAccountContactBillingInformation(new List<Contact>{wrapper.account.Contacts[0]});

        System.assertEquals('Test 123',[SELECT MailingStreet FROM Contact WHERE Id = :wrapper.account.Contacts[0].Id].MailingStreet);

        Test.stopTest();
    }
}