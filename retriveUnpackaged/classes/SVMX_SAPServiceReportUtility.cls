//SVMX_SAPServiceReportUtility - Integration Class
//Used of Sending Attachments of WorkOrder to SAP

public without sharing class SVMX_SAPServiceReportUtility{

    public static void handleServiceReportCreation(List<Attachment> newattachmentsList, Map<Id,Id> attachmentWorkOrderIdMap){

        //Map<Id,Id> attachmentWorkOrderIdMap = new Map<Id,Id>();
        /*
        for(Attachment attachment:newattachmentsList){

            attachmentWorkOrderIdMap.put(attachment.Id,attachment.ParentId);
        }*/

        Set<Id> AttcmntIdSet = new Set<Id>();
        Set<Id> WoIdSet = new Set<Id>();

        Map<Id,SVMXC__Service_Order__c> mapWorkOrder = new Map<Id,SVMXC__Service_Order__c>([SELECT Id, Name,SVMX_Service_Sales_Order_Number__c
                                                                                                FROM SVMXC__Service_Order__c
                                                                                                WHERE Id IN : attachmentWorkOrderIdMap.values()]);

        for(Attachment attachment:newattachmentsList){

            if(mapWorkOrder.get(attachment.ParentId).SVMX_Service_Sales_Order_Number__c != null){

                AttcmntIdSet.add(attachment.Id);
                WoIdSet.add(attachment.ParentId);
            }
        }

        if(AttcmntIdSet.size() > 0 && WoIdSet.size() > 0){
            
            sendServiceReportsToSAP(AttcmntIdSet,WoIdSet);
        }

    }

    //Triggered by update of workorder service sales order number
    public static void handleServiceReportCreationOnworkOrderupdate(set<Id> workorderIdset){

        Set<Id> AttcmntIdSet = new Set<Id>();
        Set<Id> WoIdSet = new Set<Id>();
        string name = '%'+System.Label.Service_Report+'%';

       List<Attachment> attachmntList = [Select Id, IsDeleted, ParentId, Name, IsPrivate,OwnerId, CreatedDate, CreatedById, LastModifiedDate,Description FROM Attachment Where Name LIKE :name AND Name LIKE '%.pdf' AND ParentId In :workorderIdset];

       Map<Id,SVMXC__Service_Order__c> mapWorkOrder = new Map<Id,SVMXC__Service_Order__c>([SELECT Id, Name,SVMX_Service_Sales_Order_Number__c
                                                                                                FROM SVMXC__Service_Order__c
                                                                                                WHERE Id IN : workorderIdset]);

       for(Attachment attachment:attachmntList){

            if(mapWorkOrder.get(attachment.ParentId).SVMX_Service_Sales_Order_Number__c != null){

                AttcmntIdSet.add(attachment.Id);
                WoIdSet.add(attachment.ParentId);
            }
        }

        if(AttcmntIdSet.size() > 0 && WoIdSet.size() > 0){
            
            sendServiceReportsToSAP(AttcmntIdSet,WoIdSet);
        }

    } 

    //Sends service Report to SAP
    @future(callout=true)
    public static void sendServiceReportsToSAP(Set<Id> attachmentIdSet,Set<Id> workOrderIdSet){

        System.debug('SVMX_SAPServiceReportUtility : Handle Servie Report Creation');

        List<Attachment> attachmentsList = new List<Attachment>();
        List<Integration_Message__c> integrationMessageList = new List<Integration_Message__c>();

         attachmentsList =[Select Id, IsDeleted, ParentId, Name, IsPrivate, ContentType, BodyLength,
                           Body, OwnerId, CreatedDate, CreatedById, LastModifiedDate, 
                           LastModifiedById, SystemModstamp, Description FROM Attachment WHERE Id In:attachmentIdSet];

         Map<Id,SVMXC__Service_Order__c> mapWorkOrder = new Map<Id,SVMXC__Service_Order__c>([SELECT Id, Name,SVMX_Service_Sales_Order_Number__c
                                                                                                FROM SVMXC__Service_Order__c
                                                                                                WHERE Id IN : workOrderIdSet]);

         SVMX_wwwDormakabaComServiceReport.BusinessDocumentMessageHeader MessageHeader 
                                                    = new SVMX_wwwDormakabaComServiceReport.BusinessDocumentMessageHeader();

         List<SVMX_wwwDormakabaComServiceReport.DocumentCreateRequestDocument> documents 
                                                    = new List<SVMX_wwwDormakabaComServiceReport.DocumentCreateRequestDocument>();

        for(Attachment attachment : attachmentsList)
        {
            SVMX_wwwDormakabaComServiceReport.DocumentCreateRequestDocument document 
                                                    = new SVMX_wwwDormakabaComServiceReport.DocumentCreateRequestDocument();
            
            document.ID = new SVMX_wwwDormakabaComServiceReport.AttachmentID ();
            document.ID.Content = attachment.Id;

            document.TypeCode = System.Label.SVMX_I47_TypeCode;

            document.DocumentingParty = new SVMX_wwwDormakabaComServiceReport.DocumentCreateRequestDocumentingParty ();
            document.DocumentingParty.StandardID = new SVMX_wwwDormakabaComServiceReport.PartyStandardID ();
            document.DocumentingParty.StandardID.Content = System.Label.SVMX_I47_StandardID;

            //Blob body = EncodingUtil.base64Decode(attachment.Body);

            document.DocumentContent = new  SVMX_wwwDormakabaComServiceReport.DocumentCreateRequestContent();
            //document.DocumentContent.Content = attachment.Body.toString();
            document.DocumentContent.Content = EncodingUtil.base64Encode(attachment.Body);
            //document.DocumentContent.Content ='test';
            document.DocumentContent.MimeCode = 'application/pdf';

            document.ObjectReference = new List<SVMX_wwwDormakabaComServiceReport.BusinessTransactionDocumentReference_InternalID> ();
            SVMX_wwwDormakabaComServiceReport.BusinessTransactionDocumentReference_InternalID objectRefence 
                                                        = new SVMX_wwwDormakabaComServiceReport.BusinessTransactionDocumentReference_InternalID();
            objectRefence.InternalID = new SVMX_wwwDormakabaComServiceReport.BusinessTransactionDocumentID ();
            objectRefence.InternalID.Content = mapWorkOrder.get(attachment.ParentId).SVMX_Service_Sales_Order_Number__c;
            objectRefence.TypeCode = System.Label.SVMX_I47_objectRefenceTypeCode;
            document.ObjectReference.add(objectRefence);

            documents.add(document);

            
        }
        Boolean response = null;
        if(documents.isEmpty()) {

          System.debug('*** SVMX_SAPServiceReportUtility - No Attachments to send to SAP ***');
        }

        else {
            
            try{
            system.debug('soap***'+documents);            
            SVMX_wwwDormakabaComServiceReport.HTTPS_Port sr = getPort();

            response = sr.CreateSync(MessageHeader, documents);
            System.debug('SVMX_SAPServiceReportUtility : Call Service - Done - Response: ' + response);
                if(response == true){
                    Integration_Message__c message = new Integration_Message__c();
                    message.DeliveryDate__c = System.now();
                    message.Delivery_Status__c = 'Success';
                    message.EndpointName__c = 'endpoint_x';
                    //message.MessageBody__c = documents.toString().substring(0,130999);
                    message.ResponseMessage__c = response + '';
                    integrationMessageList.add(message);
                }    

            }
            catch (System.CalloutException exceptionRec) {
                
                System.debug('SVMX_SAPServiceReportUtility : Exception while sending service Report : ' + exceptionRec.getMessage());
                Integration_Message__c message = new Integration_Message__c();
                message.DeliveryDate__c = System.now();
                message.Delivery_Status__c = 'Error';
                message.EndpointName__c = 'endpoint_x';
                //message.MessageBody__c = documents.toString().substring(0,130999);
                message.ResponseMessage__c = response + '';
                integrationMessageList.add(message);
             
            }        

        } 

        if(!integrationMessageList.isEmpty()) {
                
                insert integrationMessageList;
        }   

    }

    // getPort method to set callout header   
    private static SVMX_wwwDormakabaComServiceReport.HTTPS_Port getPort() {

        //Custom Settings to get Username and Password
        SVMX_SAP_Integration_UserPass__c cs = SVMX_SAP_Integration_UserPass__c.getInstance('SAP PO');
            
        String authorizationHeader = 'Basic ' + EncodingUtil.base64Encode(Blob.valueOf(cs.WS_USER__c + ':' + cs.WS_PASS__c));
        
        SVMX_wwwDormakabaComServiceReport.HTTPS_Port sr = new SVMX_wwwDormakabaComServiceReport.HTTPS_Port();
        sr.inputHttpHeaders_x = new Map<String, String>();
        sr.inputHttpHeaders_x.put('Content-type', 'application/soap+xml');        
        sr.inputHttpHeaders_x.put('Authorization', authorizationHeader);
        sr.timeout_x = 120000;
     
        return sr; 
    }
     

    
}