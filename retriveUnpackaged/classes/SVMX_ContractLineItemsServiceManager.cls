/************************************************************************************************************
Description: Service Manager for Contract Line Items. All the methods and the logic for Contract Line items object resides here.

Dependancy: 
 
Author: Ranjitha S
Date: 21-02-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

************************************************************************************************/

public class SVMX_ContractLineItemsServiceManager {

    public static List<SVMXC__Service_Contract__c> updateContractPrice(set<id> scIds){
        
        List<SVMXC__Service_Contract__c> contractList = new List<SVMXC__Service_Contract__c>();
        map<id,SVMXC__Service_Contract__c> cliContractMap = new map<id,SVMXC__Service_Contract__c>();
        cliContractMap = SVMX_ServiceContractDataManager.cliQuery(scIds);
        
        if(!cliContractMap.isEmpty()){
            for(SVMXC__Service_Contract__c sc : cliContractMap.values()){
                SVMXC__Service_Contract__c contract = new SVMXC__Service_Contract__c();
                contract.id = sc.id;
                sc.SVMX_Contract_Line_Items_Total_Price__c = 0;
                contract.SVMX_Contract_Line_Items_Total_Price__c =0;
                for(SVMX_Contract_Line_Items__c cli: sc.Contract_Line_Items__r){
                    if(cli.SVMX_Total_Line_Price__c != null){ 
                        contract.SVMX_Contract_Line_Items_Total_Price__c = sc.SVMX_Contract_Line_Items_Total_Price__c + cli.SVMX_Total_Line_Price__c;
                    }
                }
                contractList.add(contract);
            }
        }
        
        return contractList;
    }

}