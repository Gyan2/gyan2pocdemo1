/**
 * Created by trakers on 13.06.18.
 */

public with sharing class SAPPO_DocumentRequest {
    public class DocumentCreateMultipleRequest {
        public SAPPO_EsmEdt.BusinessDocumentMessageHeader MessageHeader;
        public SAPPO_DocumentRequest.DocumentCreateRequestDocument[] Document;
        private String[] MessageHeader_type_info = new String[]{'MessageHeader','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] Document_type_info = new String[]{'Document','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'MessageHeader','Document'};
    }
    public class DocumentCreateRequest {
        public SAPPO_DocumentRequest.BusinessDocumentMessageHeader MessageHeader;
        public SAPPO_DocumentRequest.DocumentCreateRequestDocument Document;
        private String[] MessageHeader_type_info = new String[]{'MessageHeader','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] Document_type_info = new String[]{'Document','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'MessageHeader','Document'};
    }
    public class BusinessDocumentMessageHeader {
        public SAPPO_DocumentRequest.BusinessDocumentMessageID ID;
        public String UUID;
        public String TestDataIndicator;
        public SAPPO_DocumentRequest.BusinessScope[] BusinessScope;
        public String PositiveAckRequestedIndicator;
        public String NegativeAckRequestedIndicator;
        public String DocumentAckRequestedIndicator;
        private String[] ID_type_info = new String[]{'ID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] UUID_type_info = new String[]{'UUID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] TestDataIndicator_type_info = new String[]{'TestDataIndicator','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] BusinessScope_type_info = new String[]{'BusinessScope','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','-1','false'};
        private String[] PositiveAckRequestedIndicator_type_info = new String[]{'PositiveAckRequestedIndicator','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] NegativeAckRequestedIndicator_type_info = new String[]{'NegativeAckRequestedIndicator','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] DocumentAckRequestedIndicator_type_info = new String[]{'DocumentAckRequestedIndicator','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'ID','UUID','TestDataIndicator','BusinessScope','PositiveAckRequestedIndicator','NegativeAckRequestedIndicator','DocumentAckRequestedIndicator'};
    }
    public class BusinessDocumentMessageID {
        public String Content;
        public String SchemeID;
        public String SchemeAgencyID;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] SchemeID_type_info = new String[]{'SchemeID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] SchemeAgencyID_type_info = new String[]{'SchemeAgencyID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','SchemeID','SchemeAgencyID'};
    }
    public class BusinessScope {
        public String TypeCode;
        public SAPPO_DocumentRequest.BusinessScopeInstanceID InstanceID;
        public SAPPO_DocumentRequest.BusinessScopeID ID;
        private String[] TypeCode_type_info = new String[]{'TypeCode','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] InstanceID_type_info = new String[]{'InstanceID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] ID_type_info = new String[]{'ID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'TypeCode','InstanceID','ID'};
    }
    public class BusinessTransactionDocumentID {
        public String Content;
        public String SchemeID;
        public String SchemeAgencyID;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] SchemeID_type_info = new String[]{'SchemeID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] SchemeAgencyID_type_info = new String[]{'SchemeAgencyID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','SchemeID','SchemeAgencyID'};
    }
    public class DocumentCreateMultipleSyncResponse {
        public SAPPO_EsmEdt.BusinessDocumentMessageHeader MessageHeader;
        public SAPPO_DocumentRequest.DocumentCreateResponseDocument[] Document;
        public SAPPO_EsmEdt.Log Log;
        private String[] MessageHeader_type_info = new String[]{'MessageHeader','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] Document_type_info = new String[]{'Document','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','-1','false'};
        private String[] Log_type_info = new String[]{'Log','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'MessageHeader','Document','Log'};
    }
    public class BusinessTransactionDocumentReference_InternalID {
        public SAPPO_DocumentRequest.BusinessTransactionDocumentID InternalID;
        public String ExternalID;
        public String TypeCode;
        public SAPPO_DocumentRequest.MEDIUM_Name TypeName;
        public String ItemID;
        public String ExternalItemID;
        public String ItemTypeCode;
        public SAPPO_DocumentRequest.MEDIUM_Name ItemTypeName;
        private String[] InternalID_type_info = new String[]{'InternalID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] ExternalID_type_info = new String[]{'ExternalID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] TypeCode_type_info = new String[]{'TypeCode','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] TypeName_type_info = new String[]{'TypeName','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] ItemID_type_info = new String[]{'ItemID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] ExternalItemID_type_info = new String[]{'ExternalItemID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] ItemTypeCode_type_info = new String[]{'ItemTypeCode','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] ItemTypeName_type_info = new String[]{'ItemTypeName','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'InternalID','ExternalID','TypeCode','TypeName','ItemID','ExternalItemID','ItemTypeCode','ItemTypeName'};
    }
    public class AttachmentID {
        public String Content;
        public String SchemeID;
        public String SchemeAgencyID;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] SchemeID_type_info = new String[]{'SchemeID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] SchemeAgencyID_type_info = new String[]{'SchemeAgencyID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','SchemeID','SchemeAgencyID'};
    }
    public class DocumentCreateRequestDocument {
        public SAPPO_DocumentRequest.AttachmentID ID;
        public String TypeCode;
        public SAPPO_DocumentRequest.DocumentCreateRequestDocumentingParty DocumentingParty;
        public SAPPO_DocumentRequest.DocumentCreateRequestContent DocumentContent;
        public SAPPO_DocumentRequest.BusinessTransactionDocumentReference_InternalID[] ObjectReference;
        public SAPPO_DocumentRequest.BusinessDocumentSystemAdministrativeData SystemAdministrativeData;
        public SAPPO_DocumentRequest.PropertyValuation[] PropertyValuation;
        private String[] ID_type_info = new String[]{'ID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] TypeCode_type_info = new String[]{'TypeCode','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] DocumentingParty_type_info = new String[]{'DocumentingParty','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] DocumentContent_type_info = new String[]{'DocumentContent','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] ObjectReference_type_info = new String[]{'ObjectReference','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','-1','false'};
        private String[] SystemAdministrativeData_type_info = new String[]{'SystemAdministrativeData','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] PropertyValuation_type_info = new String[]{'PropertyValuation','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'ID','TypeCode','DocumentingParty','DocumentContent','ObjectReference','SystemAdministrativeData','PropertyValuation'};
    }
    public class PropertyID {
        public String Content;
        public String SchemeID;
        public String SchemeAgencyID;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] SchemeID_type_info = new String[]{'SchemeID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] SchemeAgencyID_type_info = new String[]{'SchemeAgencyID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','SchemeID','SchemeAgencyID'};
    }
    public class DocumentCreateResponseDocument {
        public SAPPO_EsmEdt.Identifier ID;
        private String[] ID_type_info = new String[]{'ID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'ID'};
    }
    public class PropertyReference {
        public SAPPO_DocumentRequest.PropertyID ID;
        public SAPPO_EsmEdt.PropertyDefinitionClassReference PropertyDefinitionClassReference;
        private String[] ID_type_info = new String[]{'ID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] PropertyDefinitionClassReference_type_info = new String[]{'PropertyDefinitionClassReference','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'ID','PropertyDefinitionClassReference'};
    }
    public class PropertyValuation {
        public SAPPO_DocumentRequest.PropertyReference PropertyReference;
        public String ActionCode;
        public SAPPO_EsmEdt.PropertyValuationValueGroup[] ValueGroup;
        private String[] PropertyReference_type_info = new String[]{'PropertyReference','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] ActionCode_type_info = new String[]{'ActionCode','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] ValueGroup_type_info = new String[]{'ValueGroup','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','-1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'PropertyReference','ActionCode','ValueGroup'};
    }
    public class PartyStandardID {
        public String Content;
        public String SchemeAgencyID;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] SchemeAgencyID_type_info = new String[]{'SchemeAgencyID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','SchemeAgencyID'};
    }
    public class UserAccountID {
        public String Content;
        public String SchemeAgencyID;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] SchemeAgencyID_type_info = new String[]{'SchemeAgencyID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','SchemeAgencyID'};
    }
    public class DocumentCreateRequestDocumentingParty {
        public SAPPO_DocumentRequest.PartyStandardID StandardID;
        public SAPPO_DocumentRequest.PartyInternalID InternalID;
        private String[] StandardID_type_info = new String[]{'StandardID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] InternalID_type_info = new String[]{'InternalID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'StandardID','InternalID'};
    }
    public class BusinessDocumentSystemAdministrativeData {
        public String CreationDate;
        public String CreationTime;
        public SAPPO_DocumentRequest.UserAccountID CreationUserAccountID;
        public String LastChangeDate;
        public String LastChangeTime;
        public SAPPO_DocumentRequest.UserAccountID LastChangeUserAccountID;
        private String[] CreationDate_type_info = new String[]{'CreationDate','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] CreationTime_type_info = new String[]{'CreationTime','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] CreationUserAccountID_type_info = new String[]{'CreationUserAccountID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] LastChangeDate_type_info = new String[]{'LastChangeDate','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] LastChangeTime_type_info = new String[]{'LastChangeTime','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] LastChangeUserAccountID_type_info = new String[]{'LastChangeUserAccountID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'CreationDate','CreationTime','CreationUserAccountID','LastChangeDate','LastChangeTime','LastChangeUserAccountID'};
    }
    public class DocumentCreateRequestContent {
        public String Content;
        public String MimeCode;
        public String CharacterSetCode;
        public String Format;
        public String FileName;
        public String Uri;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] MimeCode_type_info = new String[]{'MimeCode','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] CharacterSetCode_type_info = new String[]{'CharacterSetCode','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] Format_type_info = new String[]{'Format','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] FileName_type_info = new String[]{'FileName','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] Uri_type_info = new String[]{'Uri','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','MimeCode','CharacterSetCode','Format','FileName','Uri'};
    }
    public class MEDIUM_Name {
        public String Content;
        public String LanguageCode;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] LanguageCode_type_info = new String[]{'LanguageCode','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','LanguageCode'};
    }
    public class PartyInternalID {
        public String Content;
        public String SchemeID;
        public String SchemeAgencyID;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] SchemeID_type_info = new String[]{'SchemeID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] SchemeAgencyID_type_info = new String[]{'SchemeAgencyID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','SchemeID','SchemeAgencyID'};
    }
    public class BusinessScopeInstanceID {
        public String Content;
        public String SchemeID;
        public String SchemeAgencyID;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] SchemeID_type_info = new String[]{'SchemeID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] SchemeAgencyID_type_info = new String[]{'SchemeAgencyID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','SchemeID','SchemeAgencyID'};
    }
    public class BusinessScopeID {
        public String Content;
        public String SchemeID;
        public String SchemeAgencyID;
        private String[] Content_type_info = new String[]{'Content','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'1','1','false'};
        private String[] SchemeID_type_info = new String[]{'SchemeID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] SchemeAgencyID_type_info = new String[]{'SchemeAgencyID','http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',null,'0','1','false'};
        private String[] apex_schema_type_info = new String[]{'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp','false','false'};
        private String[] field_order_type_info = new String[]{'Content','SchemeID','SchemeAgencyID'};
    }
    public class HTTPS_Port {
        public String endpoint_x = Util.IntegrationEndpoints.Document__c;//'https://integration-dev.mydorma.com/XISOAPAdapter/MessageServlet?senderParty=&senderService=Salesforce_Global&receiverParty=&receiverService=&interface=DocumentOut&interfaceNamespace=http%3A%2F%2Fwww.dormakaba.com%2Fe-connect%2Fbc%2Fsalesforce%2Fcrossapp';
        public Map<String,String> inputHttpHeaders_x;
        public Map<String,String> outputHttpHeaders_x;
        public String clientCertName_x;
        public String clientCert_x;
        public String clientCertPasswd_x;
        public Integer timeout_x;
        private String[] ns_map_type_info = new String[]{'http://soap.sforce.com/2005/09/outbound', 'SAPPO_ACK', 'http://www.dorma.com/e-connect/bc/erp/application/crossapp', 'wwwDormaComEConnectBcErpApplicati', 'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp', 'SAPPO_DocumentRequest', 'urn:dorma:esm:edt:1.0', 'SAPPO_EsmEdt', 'urn:dorma:esm:esi:document:1.0', 'SAPPO_Document'};
        public SAPPO_DocumentRequest.DocumentCreateMultipleSyncResponse CreateMultipleSync(SAPPO_EsmEdt.BusinessDocumentMessageHeader MessageHeader,SAPPO_DocumentRequest.DocumentCreateRequestDocument[] Document) {
            SAPPO_DocumentRequest.DocumentCreateMultipleRequest request_x = new SAPPO_DocumentRequest.DocumentCreateMultipleRequest();
            request_x.MessageHeader = MessageHeader;
            request_x.Document = Document;
            SAPPO_DocumentRequest.DocumentCreateMultipleSyncResponse response_x;
            Map<String, SAPPO_DocumentRequest.DocumentCreateMultipleSyncResponse> response_map_x = new Map<String, SAPPO_DocumentRequest.DocumentCreateMultipleSyncResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                            'http://sap.com/xi/WebService/soap1.1',
                            'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',
                            'DocumentCreateMultipleRequest',
                            'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',
                            'DocumentCreateMultipleSyncResponse',
                            'SAPPO_DocumentRequest.DocumentCreateMultipleSyncResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
        public Boolean CreateSync(SAPPO_DocumentRequest.BusinessDocumentMessageHeader MessageHeader,SAPPO_DocumentRequest.DocumentCreateRequestDocument Document) {
            SAPPO_DocumentRequest.DocumentCreateRequest request_x = new SAPPO_DocumentRequest.DocumentCreateRequest();
            request_x.MessageHeader = MessageHeader;
            request_x.Document = Document;
            SAPPO_ACK.notificationsResponse_element response_x;
            Map<String, SAPPO_ACK.notificationsResponse_element> response_map_x = new Map<String, SAPPO_ACK.notificationsResponse_element>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                            'http://sap.com/xi/WebService/soap1.1',
                            'http://www.dormakaba.com/e-connect/bc/salesforce/crossapp',
                            'DocumentCreateRequest',
                            'http://soap.sforce.com/2005/09/outbound',
                            'notificationsResponse',
                            'SAPPO_ACK.notificationsResponse_element'}
            );
            response_x = response_map_x.get('response_x');
            return response_x.Ack;
        }
        public SAPPO_Document.DocumentReadResponse ReadSync(SAPPO_Document.ArchiveObject ArchiveObject,SAPPO_Document.DocumentReadRequestDocumentingParty DocumentingParty,String CosmosCorrelation) {
            SAPPO_Document.DocumentReadRequest  request_x = new SAPPO_Document.DocumentReadRequest();
                                                request_x.ArchiveObject     = ArchiveObject;
                                                request_x.DocumentingParty  = DocumentingParty;
                                                request_x.CosmosCorrelation = CosmosCorrelation;
            SAPPO_Document.DocumentReadResponse response_x;
            Map<String, SAPPO_Document.DocumentReadResponse> response_map_x = new Map<String, SAPPO_Document.DocumentReadResponse>();
            response_map_x.put('response_x', response_x);
            WebServiceCallout.invoke(
                    this,
                    request_x,
                    response_map_x,
                    new String[]{endpoint_x,
                            'http://sap.com/xi/WebService/soap1.1',
                            'urn:dorma:esm:esi:document:1.0',
                            'DocumentReadSyncRequest',
                            'http://www.dorma.com/e-connect/bc/erp/application/crossapp',
                            'DocumentReadSyncResponse',
                            'SAPPO_Document.DocumentReadResponse'}
            );
            response_x = response_map_x.get('response_x');
            return response_x;
        }
    }
}