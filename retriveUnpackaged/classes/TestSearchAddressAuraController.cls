/**
 * Created by rebmangu on 24/04/2018.
 */

@IsTest
public with sharing class TestSearchAddressAuraController {




    public class MockHttpResponseGenerator implements HttpCalloutMock {
        public HTTPResponse respond(HTTPRequest req) {

            HttpResponse res = new HttpResponse();
            res.setHeader('Content-Type', 'application/json');
            res.setBody('{"example":"test"}'); // We don't care about the data here, as it's porcessed by the front end
            res.setStatusCode(200);
            return res;
        }
    }


    public static testMethod void getSuggestionFromGoogle(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

        String result = SearchAddressAuraController.getSuggestionFromGoogle('Test');

        System.assertEquals('{"example":"test"}',result);
    }

    public static testMethod void getFullAddressFromGoogle(){
        Test.setMock(HttpCalloutMock.class, new MockHttpResponseGenerator());

        String result = SearchAddressAuraController.getFullAddressFromGoogle('Test');

        System.assertEquals('{"example":"test"}',result);
    }

}