/**
 * Created by rebmangu on 19/04/2018.
 */

public with sharing class ProjectMapDetailController {

    public Project__c project  {get;set;}

    public ProjectMapDetailController(){
        String projectId = ApexPages.currentPage().getParameters().get('id');

        project = [select id,Name,Geolocation__Latitude__s,Geolocation__Longitude__s from Project__c where id =: projectId];
    }
}