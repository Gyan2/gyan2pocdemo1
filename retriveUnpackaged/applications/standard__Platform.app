<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-Contact</defaultLandingTab>
    <tab>standard-Contact</tab>
    <tab>standard-Account</tab>
    <tab>standard-report</tab>
    <tab>standard-Dashboard</tab>
    <tab>standard-Chatter</tab>
    <tab>standard-File</tab>
    <tab>SVMXC__Service_Group_Members__c</tab>
    <tab>SVMXC__Timesheet__c</tab>
    <tab>SVMXC__Checklist__c</tab>
    <tab>SVMXC__Timesheet_Entry__c</tab>
    <tab>SVMX_Interface_Trigger_Configuration__c</tab>
    <tab>SVMX_Billing_Rules__c</tab>
    <tab>SVMX_Dynamic_PM_Configuration__c</tab>
    <tab>SVMX_Sales_Office__c</tab>
    <tab>SVMX_Integration_Error_Tracking__c</tab>
    <tab>ProductDetail__c</tab>
    <tab>VendorMaterial__c</tab>
    <tab>SVMXC_Quote_Configuration__c</tab>
    <tab>SVMXC_Quote_Amount__c</tab>
    <tab>SVMXC_Quote_Approvers__c</tab>
    <tab>SVMXC__Product_Stock__c</tab>
    <tab>Labor_Contract_Hours__c</tab>
</CustomApplication>
