/**
 * Created by rebmangu on 05/04/2018.
 */
({
    sendToVF:function(component,message){
            message.origin = 'lightning';

            console.log(component.find("vfFrame"));

            var vfWindow = component.find("vfFrame").getElement().contentWindow;
            vfWindow.postMessage(message,'*');
    },
    handleEventFromIframe:function(component,message){
        console.log(message.type);
        switch(message.type){
            case 'select':
                component.get('v.projects').filter(val => val._selected).forEach(val => {
                    delete val._selected;
                })

                var project = component.get('v.projects').find(val => val.Id == message.selected);
                if(project){
                    project._selected = true;
                    component.set('v.projects',component.get('v.projects'));
                    document.querySelector('li.selected').scrollIntoView();
                }

            break;

//            case 'selectBuilding':
//                    console.log('Send Building');
//                var buildingId = message.selected;
//                var project = component.get('v.project');
//                    project.Building__c = buildingId;
//                component.set('v.project',project);
//            break;

            case 'changeSize':

                component.set('v.modalSize',component.get('v.modalSize') == 'large' ? '':'large');

            break;
        }

    }
})