/**
 * Created by rebmangu on 04/04/2018.
 */
({
    onInit:function(component,event,helper){

        console.log(window.document.getElementById('mainContainer'));
        //Handle message from the Iframe
        window.addEventListener("message", function(event) {
            if(event.data.origin != 'visualforce'){
                return ;
            }
            console.log(JSON.stringify(event.data));
            helper.handleEventFromIframe(component,event.data);
        }, false);
    },
    handleApplicationEvent:function(component,event,helper){
        console.log('handleApplicationEvent')
        var type = event.getParam("type");
        if(type == 'center'){
            helper.sendToVF(component,{
                  type:'center',
                  position:component.get('v.project.Geolocation__Latitude__s')+';'+component.get('v.project.Geolocation__Longitude__s')
            })
            var projects = JSON.parse(JSON.stringify(component.get('v.projects')));
            helper.sendToVF(component,{
                  type:'load',
                  projects:projects?projects:[]
            })
        }

    },
    selectClick:function(component,event,helper){
        console.log('selectClick');
        var inputs = event.currentTarget.closest('li').getElementsByClassName('itemValue');
        var selected = {};
        for(var i = 0; i < inputs.length; i++){
            selected[inputs[i].name] = inputs[i].value;
        }


        component.get('v.projects').filter(val => val._selected).forEach(val => {
            delete val._selected;
        })
        console.log(JSON.stringify(component.get('v.projects')));
        console.log(selected.id);
        component.get('v.projects').forEach(val => {
            console.log(val.Id);
        })
        var project = component.get('v.projects').find(val => val.Id == selected.id);
        console.log('project');
        console.log(project);
        if(project){
            project._selected = true;
            component.set('v.projects',component.get('v.projects'));
        }


        var message =  {
            type:'select',
            selected:selected
        }
        helper.sendToVF(component,message);
    },
    onClickSelect:function(component,event,helper){
        event.preventDefault();
        event.stopPropagation();


        var navEvt = $A.get("e.force:navigateToSObject");
            navEvt.setParams({
               recordId: event.getSource().get('v.value'),
               slideDevName: "related"
            });
            navEvt.fire();
    }
})