/**
 * Created by trakers on 23.07.18.
 */
({
    getSuggestionFromGoogle:function(component){
          var action = component.get('c.getSuggestionFromGoogle');
          var searchKey = component.get('v.searchKey');

          action.setParams({
              'searchKey':searchKey
          })


          var that = this;
          return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {

                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
          }))
    },
    getFullAddressFromGoogle:function(component,placeId){
        var action = component.get('c.getFullAddressFromGoogle');

        action.setParams({
            'placeId':placeId
        })


        var that = this;
        return new Promise($A.getCallback(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                 if(response.getState() === "SUCCESS") {

                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
        }))
    },
    handleMatches:function(obj){
        obj.predictions.forEach(item => {
            // MAIN
            item.main_text_after = [];
            if(item.structured_formatting.main_text_matched_substrings){

                item.structured_formatting.main_text_matched_substrings.forEach(val => {
                    var before   = item.structured_formatting.main_text.substr(0,val.offset);
                    var selected = item.structured_formatting.main_text.substr(val.offset,val.length);
                    item.structured_formatting.main_text = item.structured_formatting.main_text.slice(val.offset+val.length);

                    //In case of empty
                    if(before != ''){
                        item.main_text_after.push({text:before,match:false});
                    }
                    item.main_text_after.push({text:selected,match:true});
                    //console.log(item.structured_formatting.main_text.slice(val.offset,val.offset+val.length));
                })
            }
            // Last part of the string
            var after = item.structured_formatting.main_text.substr(0,item.structured_formatting.main_text.length);
            if(after != ''){
                 item.main_text_after.push({text:after,match:false});
            }

            // SECONDARY
            item.secondary_text_after = [];
            if(item.structured_formatting.secondary_text_matched_substrings){

                item.structured_formatting.main_text_matched_substrings.forEach(val => {
                    var before   = item.structured_formatting.secondary_text.substr(0,val.offset);
                    var selected = item.structured_formatting.secondary_text.substr(val.offset,val.length);
                    item.structured_formatting.secondary_text = item.structured_formatting.secondary_text.slice(val.offset+val.length);

                    //In case of empty
                    if(before != ''){
                        item.secondary_text_after.push({text:before,match:false});
                    }
                    item.secondary_text_after.push({text:selected,match:true});
                    //console.log(item.structured_formatting.main_text.slice(val.offset,val.offset+val.length));
                })
            }
            // Last part of the string
            var after = item.structured_formatting.secondary_text.substr(0,item.structured_formatting.secondary_text.length);
            if(after != ''){
                 item.secondary_text_after.push({text:after,match:false});
            }


        })

        return obj;
    }
})