(
    {
        handleInit : function(component, event, helper) {
            helper.updateDisplayStructure(component);
        },

        /*handleEnabledClick : function(component, event, helper) {
            var displayStructure = component.get('v.profilesDisplayStructure');
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');

            var pageIndex = displayStructure._pages[buttonValueSplit[0]];
            var profileIndex = displayStructure._profiles[buttonValueSplit[1]];

            displayStructure.pages[pageIndex].profiles[profileIndex].enabled = button.get('v.selected')?'false':'true';

            component.set('v.profilesDisplayStructure',displayStructure);
        },*/

        handleEnabledClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'enabled', !button.get('v.selected'));
        },

        handleSearch : function(component, event, helper) {
            var displayStructure = component.get('v.profilesDisplayStructure');
            var searchString = component.get('v.searchString').toLowerCase();
            for (var page of displayStructure.pages) {
                page.showMe = page.apexPage.toLowerCase().indexOf(searchString)!=-1;
            }
            
            component.set('v.profilesDisplayStructure',displayStructure);
        }
    }
)