(
    {
        updateDisplayStructure : function(component) {
            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            displayStructure = {
                _profiles: {},
                profiles: [],
                _pages: {},
                pages: []
            };
            for (var profile of component.get('v.profilesInformationOrder')) {
                this._extractPages(component, profilesInformation[profile], displayStructure);
            }

            component.set('v.profilesDisplayStructure', displayStructure);
        },

        _extractPages : function(component, profileInfo, displayStructure) {

            var fullName = profileInfo.fullName;
            var profileIndex = displayStructure._profiles[fullName];

            if (profileIndex === undefined) {
                profileIndex = Object.keys(displayStructure._profiles).length;
                displayStructure._profiles[fullName] = profileIndex;
                displayStructure.profiles.push(fullName);
            }
			var searchString = component.get('v.searchString').toLowerCase();
            for (var pageVisibility of profileInfo.pageAccesses) {
                var pageIndex = displayStructure._pages[pageVisibility.apexPage];
                var newPage = false;
                if (pageIndex === undefined) {// if its a new page, we need to fill for all profiles empty
                    newPage = true;
                    pageIndex = Object.keys(displayStructure._pages).length;
                    displayStructure._pages[pageVisibility.apexPage] = pageIndex;
                }

                if (newPage) {
                    displayStructure.pages[pageIndex] = {
                        apexPage : pageVisibility.apexPage, 
                        showMe : pageVisibility.apexPage.toLowerCase().indexOf(searchString) != -1,
                        profiles:[]
                    } ;
                }

                displayStructure.pages[pageIndex].profiles[profileIndex] = {enabled: pageVisibility.enabled, profile: fullName};

            }
        },

        _handleGenericClick : function(component, profileName, pageName, checkboxName, setTo /*true:false*/) {
            // it lists, per checkbox, which other checkboxes need to be set to true/false
            var actionsMapping = {
                enabled : {  falseToTrue : [],
                             trueToFalse : [] }
            };

            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            var pageIndex = displayStructure._pages[pageName];
            var profileIndex = displayStructure._profiles[profileName];

            var profilePage = profilesInformation[profileName].pageAccesses.find(x=>x.apexPage==pageName);
            var displayPage = displayStructure.pages[pageIndex].profiles[profileIndex];

            // set current default field
            var checkboxList = [checkboxName].concat(actionsMapping[checkboxName][setTo?'falseToTrue':'trueToFalse']);
            var valueToSet = setTo?'true':'false';

            for (var checkbox of checkboxList) {
                displayPage[checkbox] = profilePage[checkbox] = valueToSet;
            }

            profilePage._hasChanged = true;

            component.set('v.profilesDisplayStructure',displayStructure);
        }
    }
)