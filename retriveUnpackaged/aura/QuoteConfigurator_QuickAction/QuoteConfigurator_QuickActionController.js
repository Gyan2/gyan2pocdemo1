({
	handleInit : function(component, event, helper) {
        var evt = $A.get("e.force:navigateToComponent");
        evt.setParams({
            componentDef : "c:QuoteConfiguratorV2",
            componentAttributes: {
                recordId : component.get("v.recordId"),
                includeHeader: true,
                headerHeight: 90
            },
            isredirect:true
        });
        evt.fire();
	}
})