({
	getCaseList: function(component) {
      
       
        var action = component.get('c.getCase');
        
        
        // Set up the callback
        
        var self = this;
        
        action.setCallback(this, function(actionResult) {
            
            component.set('v.Cases', actionResult.getReturnValue());
            
        });
        
        $A.enqueueAction(action);
        
    },
    
    /*******************************Pagination*******************************/
     getCaseListpagination: function(component, pageNumber, pageSize) {
       var action = component.get("c.getCasePagination");
         
          
         var test = parseInt(pageNumber) + parseInt(pageSize);
         console.log("Check this value: " + test);
       action.setParams({
           "pageNumber": parseInt(pageNumber),
           "pageSize": parseInt(pageSize)
       });
       action.setCallback(this, function(result) {
           var state = result.getState();
           //console.log("Check state: " + state);
           if (state === "SUCCESS"){
               var resultData = result.getReturnValue();
               component.set("v.Cases", resultData.CaseList);
               component.set("v.PageNumber", resultData.pageNumber);
               component.set("v.TotalRecords", resultData.totalRecords);
               component.set("v.RecordStart", resultData.recordStart);
               component.set("v.RecordEnd", resultData.recordEnd);
               component.set("v.TotalPages", Math.ceil(resultData.totalRecords / pageSize));
           }
       });
       $A.enqueueAction(action);
   },
    
    test : function(component, event, helper){
        var action = component.get("c.getUserSession");
        action.setCallback(this, function(response)
    	{
            var state = response.getState();
            if(state === "SUCCESS") 
    		{
            	//component.set("v.sessionId", response.getReturnValue());
                $.cometd.init(
    			{
                    url: '/cometd/43.0',
		            requestHeaders: { Authorization: response.getReturnValue()},
                    appendMessageTypeToURL : false
		        });
		        $.cometd.subscribe('/topic/CaseStream', function(message) 
                {
                    
                    console.log("QQQQQQQQQQQ"+JSON.stringify(message.data.event.type));
                    if(message.data.event.type == "updated")
                    {
                        alert("Inside update");
                        var updatedRecord = message.data.sobject;
                        var caseList = component.get("v.Cases");
                        for(var index = 0; index < caseList.length; index++)
                        {
                            if(caseList[index].Id == updatedRecord.Id)
                            {
                                caseList[index] = updatedRecord;
                            }
                        }
                        component.set("v.Cases", caseList);
                        
                    }
                    if(message.data.event.type == "created")
                    {
                        // alert("Hello");
                        var caseList = component.get("v.Cases");
                        caseList.unshift(message.data.sobject);
                        component.set("v.Cases", caseList);
                    }
                });
            }
        });
		$A.enqueueAction(action);
    }
})