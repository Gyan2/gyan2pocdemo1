/**
 * Created by rebmangu on 20/04/2018.
 */
({
    getSuggestionFromController:function(component){
          var action = component.get('c.getSuggestionFromController');

          action.setParams({
              'searchKey':component.get('v.value'),
              'action':component.get('v.action')
          })


          var that = this;
          return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {

                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
          }))
    },
    getSObjectId:function(obj,type){
            var classes;
            switch(type){
                case 'event':
                        if(typeof obj.getSource !== 'undefined'){
                            classes = obj.getSource().get('v.class').split(' ');
                        }else{
                            classes = obj.currentTarget.className.split(' ');
                        }
                break;
                case 'element':
                        classes = obj.className.split(' ');
                break;
            }


            return classes.find(x=>x.indexOf('data-sobject-id-')!=-1).replace('data-sobject-id-','');
        }
})