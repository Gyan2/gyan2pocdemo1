/**
 * Created by rebmangu on 20/04/2018.
 */
({

    onKeyUp:function(component,event,helper){
        //event.currentTarget.value doesn't work .... but it work in SearchAddress.cmp
        component.set('v.value',event.currentTarget.value);
        let currentKey = event.currentTarget.value; //event.getSource().get('v.value');
        setTimeout(function(){
             //console.log(component.get('v.value')+'=='+currentKey)
             if(component.get('v.searchKeySaved') != currentKey && component.get('v.value') == currentKey && currentKey.length >= 2){
                 component.set('v.searchKeySaved',currentKey);
                 helper.getSuggestionFromController(component)
                 .then(val => {
                     console.log(val);
                     var result = [];
                     for(var key in val){
                         result.push(val[key]);
                     }
                     component.set('v.result',result);
//                     let obj = helper.handleMatches(val);
                 })
             }


        }, 300);
    },
    onSelectDuplicate:function(component,event,helper){

         var SObjectId = helper.getSObjectId(event.currentTarget,'element');
         var navEvt = $A.get("e.force:navigateToSObject");
             navEvt.setParams({
               "recordId": SObjectId,
               "slideDevName": "related"
             });
             navEvt.fire();
    },
    checkSearch:function(component,event,helper){
        component.set('v.result',[]);
    },
    onFocusInput:function(component,event,helper){
        console.log('On focus');
        component.set('v.isFocus',true);
    },
    onBlurInput:function(component,event,helper){
        console.log('On Blur');
        component.set('v.isFocus',false);
    }
})