({

    createHierarchy: function(unsortedList, LookupAPIName, FormulaAPIName){
        for(var i = 0; i<unsortedList.length;i++){
            if(unsortedList[i]['Id'].length == 18){unsortedList[i]['Id'] = unsortedList[i]['Id'].slice(0,-3);}
            if (typeof unsortedList[i][LookupAPIName] != 'undefined' && unsortedList[i][LookupAPIName].length == 18) {
                unsortedList[i][LookupAPIName] = unsortedList[i][LookupAPIName].slice(0,-3);
            }
        }
        var roots = [];
        var all = {};            
        //pushing items into the map
        unsortedList.forEach(function(item){
            all[item.Id] = item;
        });
        //Sort 
        Object.keys(all).forEach(function(Id){
            var item = all[Id];
            
            if(item.Id == item[FormulaAPIName]){
                roots.push(item);
            } else if(item[LookupAPIName] in all){
                var p = all[item[LookupAPIName]];
                
                if(typeof p.items == 'undefined'){
                    p.items = [];
                    p.items.push(item);
                }else{
                    p.items.push(item);
                }
            }
        }); return roots;
    },
    
    addHierarchyDepths: function(item,depth){
        item.depth = depth;
        (item.depth <= 2) ? item.showInGrid=true : item.showInGrid=false;
        (item.depth <= 1 && !item.isLeaf) ? item.isExpanded=true : item.isExpanded=false;     
        if(item.items && item.items.length > 0){
            item.isLeaf = false;
            for(var i = 0;i<item.items.length;i++){
                this.addHierarchyDepths(item.items[i],depth+1);
            }
        }else{
            item.isLeaf=true;
        }
    },
    
    flattenHierarchy : function(obj,array){
        var objectToAdd = new Object();
        for(var k in obj){
            if(!Array.isArray(obj[k]) && obj[k] !==null){
                objectToAdd[k] = obj[k];
            }
        }
        array.push(objectToAdd);
        //cannot for-loop here --> length undefined?!
        if(obj.items !== null){
            for(var i in obj.items){
                this.flattenHierarchy(obj.items[i],array);
            }
        }
    },
    
    toggleOff : function(items,val,lookUp){ 
        var obj = items.find(x => x.Id == val.Id);
        obj.showInGrid = false;
        obj.isExpanded = false;
        // find node array --> iterate trough them toggle grid off and recursively do it again.
        var nodesOfVal = items.filter(function(v){
            return v[lookUp] == val.Id;
        });
        if(nodesOfVal && nodesOfVal.length > 0){
            for(var i = 0; i<nodesOfVal.length;i++){
                this.toggleOff(items,nodesOfVal[i],lookUp);
            }
        }   
    },
    
    
    setDefaultPath : function(Id,items,lookUp){
        if(Id.length == 18){
            Id = Id.slice(0,-3);
        }
        var element = items.find(x => x.Id == Id);
        if(element && element[lookUp]){
            (!element.isLeaf && element.isCurrent) ? element.isExpanded = false : element.isExpanded = true;
            element.showInGrid = true;
            this.setDefaultPath(element[lookUp],items,lookUp);
        }else{
            (!element.isLeaf && element.isCurrent) ? element.isExpanded = false : element.isExpanded = true;
            element.showInGrid=true;
        }
        if(element.depth == 1){
            element.isExpanded = true;
        }
    }
})