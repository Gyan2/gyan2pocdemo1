({
    doInit : function(component, event, helper) {
        //Setting the columns field array
        var columns = component.get("v.columnFields").split(";");
        if(columns.length > 4){
            columns = columns.slice(0,4);
        }
        component.set("v.columnHeader",columns[0]);
        component.set("v.columns",columns.slice(1));
        
        //getting variable references
        var recordId = component.get("v.recordId"); 
        var LabelAPIName = columns[0];
        var ObjectAPIName = component.get("v.sObjectName");
        var LookupAPIName = component.get("v.lookUpField");
        var FormulaAPIName = component.get("v.formulaField");
        
        //defining server action 
        var action = component.get("c.fetchHierarchyData");
        
        action.setParams({
            "columns":columns,
            "recordId":recordId,
            "ObjectAPIName":ObjectAPIName,
            "LookupAPIName":LookupAPIName,
            "FormulaAPIName":FormulaAPIName
        });
        
        action.setCallback(this,function(value){         
            if(value.getState() === "SUCCESS" && value.getReturnValue()){
                var returnValue = value.getReturnValue();
                var hierarchyData = returnValue.hierarchyData;
                var columnLabels = returnValue.columnLabels;
                var currentRecord = hierarchyData.find(x => (x.Id == recordId));
                var masterParent = hierarchyData.find(x => (x.Id.substring(0,15) == currentRecord[FormulaAPIName]));
                if(typeof masterParent != "undefined" && hierarchyData && columnLabels && returnValue){
                    console.log(value.getReturnValue().hierarchyData.find(x=> x.Id == x[FormulaAPIName]));
                    var hierarchyData = value.getReturnValue().hierarchyData;
                    var roots = helper.createHierarchy(hierarchyData,LookupAPIName,FormulaAPIName);
                    var rootObject = roots.find(x => (x.Id.substring(0,15) == masterParent.Id.substring(0,15)));
                    var items = [];
                    helper.addHierarchyDepths(rootObject,1);   
                    helper.flattenHierarchy(rootObject,items);
                    //preparing the setDefaultPath method. Therefore i set an attribute on the recordId object in order to display the chevron correctly. !"§(!"§ 18-15 ID!.
                    var currentRecord = items.find(x => (recordId.length==18)?x.Id==recordId.slice(0,-3):x.Id==recordId);
                    currentRecord.isCurrent = true;
                    
                    helper.setDefaultPath(recordId,items,LookupAPIName);
                    console.log(items);
                    component.set("v.items",items);
                    
                    //setting the correct column header and column label names.
                    component.set("v.columnHeaderLabel", columnLabels[0]);
                    component.set("v.columnLabels",columnLabels.slice(1));
                    component.set("v.doneLoading",true); 
                }else{
                    component.set("v.errorMessage","Unfortunately an error occurred. This might be caused by a too deeply nested hierarchy.");
                    component.set("v.errorOccurred",true);  
                }
            }else{
                component.set("v.errorMessage","Unfortunately an error occurred. Check your configuration or contact an administrator.");
                component.set("v.errorOccurred",true);
            }
        });
        $A.enqueueAction(action);
    },
    
    handleClickedEvent : function(component,event,helper){
        var id = event.getParam("clickedItemId");
        var items = component.get("v.items");
        var lookUp = component.get("v.lookUpField")
        
        items.forEach(function(val){
            if(val[lookUp] == id){
                (val.showInGrid) ? component.set("v.items",helper.toggleOff(items,val,lookUp)) : val.showInGrid=true; 
            } 
        });
        component.set("v.items",items);
    },
    
    expandAll : function(component,event,helper){
        var items = component.get("v.items");
        items.forEach(function(val){
            val.showInGrid = true;
            val.isExpanded = true;
        });
        component.set("v.items",items);
    },
    
    hideAll : function(component,event,helper){
        var items = component.get("v.items");
        items.forEach(function(val){
            if(val.depth == 1){
                val.showInGrid = true;
                val.isExpanded = false;
            }else{
                val.showInGrid = false;
                val.isExpanded = false;
            }
        });
        component.set("v.items",items);
    },
})