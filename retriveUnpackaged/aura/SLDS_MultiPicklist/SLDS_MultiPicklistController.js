/**
 * Created by trakers on 18.07.18.
 */
({
    onInit:function(component,event,helper){
        console.log('onInit');
        if(component.get('v.picklistValues') && component.get('v.picklistValues').length > 0){
            console.log('picklistsValues is not empty');
            console.log(JSON.parse(JSON.stringify(component.get('v.picklistValues'))));
            component.set('v.options',component.get('v.picklistValues'));
            //console.log(JSON.parse(JSON.stringify(component.get('v.picklistValues'))));
            helper.handleValueWithLabel(component,component.get('v.value'));
            return;
        }


        if(component.get('v.value') == undefined){
            component.set('v.value',null);
        }

        helper.getPicklistValues(component)
        .then($A.getCallback(val => {
            console.log(JSON.parse(JSON.stringify(val)));
            component.set('v.options',val);
            helper.handleValueWithLabel(component,component.get('v.value'));
        }))

    },
    onPicklistValuesChange:function(component,event,helper){
        console.log('onPicklistValuesChange');
        component.set('v.options',component.get('v.picklistValues'));
    },
    inputOnClick:function(component,event,helper){
        console.log('inputOnClick');
        component.set('v.display',!component.get('v.display'));
    },
    onBlur:function(component,event,helper){
        setTimeout($A.getCallback(function(){
             //component.set('v.display',false);
             console.log('On blur');
        }), 150)
    },
    onHoverKeep:function(component,event,helper){
            component.set('v.mouseHover',true);
    },
    onHoverLeave:function(component,event,helper){
        component.set('v.mouseHover',false)
        setTimeout(function(){
            if(!component.get('v.mouseHover')){
                component.set('v.display',false);
           }
        }, 150);
    },
    selectOnClick:function(component,event,helper){
        event.preventDefault();
        console.log('selectOnClick');
        component.set('v.displayError',false);
        component.set('v.errorMessage',null);



        var selected = event.currentTarget.getAttribute('data-value');
        var newValues = [];

        if(selected == null){
            helper.handleValueWithLabel(component,null);
            return;
        }

        console.log(event.shiftKey);
        if(event.shiftKey){
            var last = {'distance':9999999,'start':null,'end':null};
            var selectedIndex = component.get('v.options').map(val => val.value).indexOf(selected);

            component.get('v.options')
            .forEach((val,i) => {
                if(val.selected){
                    if(last.distance > Math.abs(i - selectedIndex) ){
                        last.distance = Math.abs(i - selectedIndex);
                        last.start = i < selectedIndex ? i : selectedIndex;
                        last.end = i < selectedIndex ? selectedIndex : i;
                    }
                }

            })

            component.get('v.options')
            .filter((val,index) => index >= last.start && index <= last.end)
            .forEach(val => {
                newValues.push(val.value)
            })

            console.log(newValues);
            console.log(last);
        }else{
            newValues.push(selected);
        }



        if(newValues){
            var values = component.get('v.value')?component.get('v.value').split(';'):[];

            if(newValues.length > 1){
                values = values.concat(newValues);
            }else{
                var single = newValues[0];
                if(values.includes(single)){
                    values.splice(values.indexOf(single), 1);
                }else{
                    values.push(single);
                }
            }

            console.log('New value is: '+values.join(';'));
            helper.handleValueWithLabel(component,values.join(';'));
        }





        //component.set('v.display',false);
    },
    dummyAction:function(component,event,helper){
        console.log('Dummy action');
    }
})