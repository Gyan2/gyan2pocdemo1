/**
 * Created by trakers on 18.07.18.
 */
({
    getPicklistValues:function(component){
      var action = component.get('c.getPicklistValues');

          action.setParams({
              'fieldName':  component.get('v.field'),
              'objectType': component.get('v.sobject')
          })


          var that = this;
          return new Promise(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {
                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
          });
    },
    handleValueWithLabel:function(component,value){


        component.get('v.options').filter(val => val.selected).forEach(val => {
            val.selected = false;
        });

        var values = value ? value.split(';'):[]; // If value == null, we take an empty list
            values.forEach(val => {
                if(component.get('v.options').find(option => String(option.value) === String(val))){
                  component.get('v.options').find(option => String(option.value) === String(val)).selected = true;
                }
            })

        var selected = component.get('v.options').filter(val => val.selected);
        if(values.length > 0){
             component.set('v.value',selected.map(val => val.value).join(';'));
             component.set('v.valueLabel',selected.map(val => val.label).join(';'));
        }else{
             component.set('v.value',null);
             component.set('v.valueLabel','--None--');
             component.get('v.options').find(option => String(option.label) === '-- none --').selected = true;
        }

        component.set('v.options',component.get('v.options'));
    },
})