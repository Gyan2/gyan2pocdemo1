/**
 * Created by rebmangu on 14/03/2018.
 */
({
    onHoverKeep:function(component,event,helper){
        if(!component.get('v.keep')){
            component.set('v.keep',true);
            component.set('v.display',true);
        }
        console.log('hoverKeep');

    },
    onHoverLeave:function(component,event,helper){
        component.set('v.keep',false);
        component.set('v.display',false);
    },
    onClickClose:function(component,event,helper){
        component.set('v.keep',false);
        component.set('v.display',false);
    }
})