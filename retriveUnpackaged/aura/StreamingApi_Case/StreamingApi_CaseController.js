({
	ScriptLoaded : function(component, event, helper) {
	
        var action = component.get("c.getUserSession");
        action.setCallback(this, function(response)
    	{
            var state = response.getState();
            if(state === "SUCCESS") 
    		{
            	//component.set("v.sessionId", response.getReturnValue());
                $.cometd.init(
    			{
                    url: '/cometd/43.0',
		            requestHeaders: { Authorization: response.getReturnValue()},
                    appendMessageTypeToURL : false
		        });
		        $.cometd.subscribe('/topic/CaseWallBoard7', function(message) 
                {
                    alert("Record Changed");
                    console.log(JSON.stringify(message));
                });
            }
        });
$A.enqueueAction(action);
	}
})