({
	helperMethod : function(component) {
		var picklistSource = component.get("v.picklistSource");
        var picklistSourceList = picklistSource.slice();
        component.set("v.picklistSourceList", picklistSourceList);
        var lookupDiv = component.find("lookup-pill-owner");
        $A.util.addClass(lookupDiv, 'slds-show');
        $A.util.removeClass(lookupDiv, 'slds-hide');
	},
    getOwnerHelper : function(component) {
        var searchKKeyword = document.getElementById("search").value;
        if(searchKKeyword.length != 0) {
        var action = component.get("c.LTNG_getOwnerList");		//calling the apex controller method & passing parameters
        action.setParams(
        { 
        	searchKKeyword : searchKKeyword
        });

        action.setCallback(this, function(response) 		//store the response from controller
        {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var ownerOptionList = response.getReturnValue();
                component.set("v.picklistSource", ownerOptionList);
                
                this.helperMethod(component);
            }
            
        });
        $A.enqueueAction(action);
        }
    }
})