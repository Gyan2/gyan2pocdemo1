({
	doInit : function(component, event, helper) {
        var picklistSource = component.get("v.picklistSource");
        var picklistSourceList = picklistSource.slice();
        var index = picklistSourceList.indexOf("Reactive");
		if(index > -1) {
  			picklistSourceList.splice(index, 1);
		}
        component.set("v.picklistSourceList", picklistSourceList);
        if(component.get("v.picklistSelected").length != 0) {
            var lookupDiv = component.find("selected-pill");
        	$A.util.addClass(lookupDiv, 'slds-show');
        	$A.util.removeClass(lookupDiv, 'slds-hide');
            
        }
        
        
        $("#ownerSearchId").focusout(function () {
            
            //alert("qqqq");
            setTimeout(function(){
                var lookupDiv = component.find("lookup-pill-owner");
                $A.util.removeClass(lookupDiv, 'slds-show');
                $A.util.addClass(lookupDiv, 'slds-hide');
                var picklistDiv = component.find("picklist-comp");
                $A.util.removeClass(picklistDiv, "outline-class");
                
            }, 30);
            
        });
        
        
	},
    showLookup : function(component, event, helper) {
        //document.getElementById("picklist-comp").innerHTML.style.outline = "outline: 1px solid blue;";
        
        var picklistDiv = component.find("picklist-comp");
        $A.util.addClass(picklistDiv, "outline-class");
        helper.getOwnerHelper(component);
        
    },
    selectOption : function(component, event, helper) {
        
        var lookupDiv = component.find("lookup-pill-owner");
        $A.util.removeClass(lookupDiv, 'slds-show');
        $A.util.addClass(lookupDiv, 'slds-hide');
        
        var picklistSourceList = component.get("v.picklistSourceList");
       	var target = event.getSource();  
		var option = target.get("v.name");
        var index = picklistSourceList.indexOf(option);
		if(index > -1) {
  			picklistSourceList.splice(index, 1);
		}
        component.set("v.picklistSourceList", picklistSourceList);
        var picklistSelected = component.get("v.picklistSelected");
        if(picklistSelected.length == 0) {
            var selectedDiv = component.find("selected-pill");
        	$A.util.addClass(selectedDiv, 'slds-show');
        	$A.util.removeClass(selectedDiv, 'slds-hide');
        }
        picklistSelected.push(option);
        component.set("v.picklistSelected", picklistSelected);
        component.set("v.itemSelected", picklistSelected.length + " selected");
        
        var picklistDiv = component.find("picklist-comp");
        $A.util.removeClass(picklistDiv, "outline-class");
        
        var HMEvent = $A.get("e.c:WallboardFilterEvent");  
    	HMEvent.fire();
        
    },
    clearSelected : function(component, event, helper) {
        
        var UnselectedName = event.target.id;        
        var index= event.target.dataset.index;
        //alert("Name: " + UnselectedName);
        //alert("indexval: " + indexval);
        var picklistSourceList = component.get("v.picklistSourceList");
        var picklistSelected = component.get("v.picklistSelected");
        picklistSourceList.push(picklistSelected[index]);
        picklistSelected.splice(index, 1);
        component.set("v.picklistSourceList", picklistSourceList);
        component.set("v.picklistSelected", picklistSelected);
		
        if(picklistSelected.length == 0) {
            var selectedDiv = component.find("selected-pill");
        	$A.util.addClass(selectedDiv, 'slds-hide');
        	$A.util.removeClass(selectedDiv, 'slds-show');
            component.set("v.itemSelected", "");
        }
        else{
        	component.set("v.itemSelected", picklistSelected.length + " selected");
        }
        var HMEvent = $A.get("e.c:WallboardFilterEvent");  
    	HMEvent.fire();
        
        
        
        /*var lookupDiv = component.find("lookup-pill-owner");
        $A.util.removeClass(lookupDiv, 'slds-show');
        $A.util.addClass(lookupDiv, 'slds-hide');
        
        var picklistSourceList = component.get("v.picklistSourceList");
       	var target = event.getSource();  
		var option = target.get("v.name");
        var index = picklistSourceList.indexOf(option);
		if(index > -1) {
  			picklistSourceList.splice(index, 1);
		}
        component.set("v.picklistSourceList", picklistSourceList);
        var picklistSelected = component.get("v.picklistSelected");
        if(picklistSelected.length == 0) {
            var selectedDiv = component.find("selected-pill");
        	$A.util.addClass(selectedDiv, 'slds-show');
        	$A.util.removeClass(selectedDiv, 'slds-hide');
        }
        picklistSelected.push(option);
        component.set("v.picklistSelected", picklistSelected);*/
        
        
    
	},
    hideLookup : function(component, event, helper) {
        var lookupDiv = component.find("lookup-pill-owner");
        $A.util.removeClass(lookupDiv, 'slds-show');
        $A.util.addClass(lookupDiv, 'slds-hide');
        
    },
    getOwnerOption : function(component, event, helper) {
        helper.getOwnerHelper(component);
    }
    
    
    
})