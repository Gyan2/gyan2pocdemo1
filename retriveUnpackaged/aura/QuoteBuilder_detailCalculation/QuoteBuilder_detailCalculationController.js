/**
 * Created by trakers on 29.06.18.
 */
({
    handleInit:function(component,event,helper){
      console.log('Init detailCalculation');
      component.set('v.quoteLineItemCopy',JSON.parse(JSON.stringify(component.get('v.quoteLineItem'))));
    },
    handleQuantityChange:function(component,event,helper){

    },
    handleDiscountChange:function(component,event,helper){
        var quoteLineItem = component.get('v.quoteLineItem');

        if(quoteLineItem.Discount > 100){
            quoteLineItem.Discount = 100;
            quoteLineItem._priceAfterDiscount = 0;
        }else if (quoteLineItem.Discount <= 0){
            quoteLineItem.Discount = 0;
            quoteLineItem.UnitPrice = quoteLineItem._priceAfterDiscount;
            quoteLineItem._priceAfterDiscount = quoteLineItem.SAPPrice__c;
        }else{
            quoteLineItem.UnitPrice = quoteLineItem.SAPPrice__c;
            quoteLineItem._priceAfterDiscount = (100 - quoteLineItem.Discount)*quoteLineItem.SAPPrice__c/100;
        }
        quoteLineItem._changed = true;

        component.set('v.quoteLineItem',component.get('v.quoteLineItem'));
    },
    handlePriceChange:function(component,event,helper){
        //console.log('detail - Price changed');
        var quoteLineItem = component.get('v.quoteLineItem');
             quoteLineItem._priceAfterDiscount = Number(event.getSource().get('v.value'));

             var discount = (100 - quoteLineItem._priceAfterDiscount/quoteLineItem.SAPPrice__c*100);

             if (discount > 100){
                     quoteLineItem._priceAfterDiscount = 0;
                     quoteLineItem.Discount = 100;
             }else if(discount <= 0){
                     quoteLineItem.UnitPrice = quoteLineItem._priceAfterDiscount;
                     quoteLineItem.Discount = 0;
             }else{
                 quoteLineItem.Discount = discount;
                 quoteLineItem.UnitPrice = quoteLineItem.SAPPrice__c;
             }
             quoteLineItem._changed = true;

             component.set('v.quoteLineItem',component.get('v.quoteLineItem'));
    },
    onEnter:function(component,event,helper){
         event.currentTarget.classList.add('hover');
    },
    onExit:function(component,event,helper){
         event.currentTarget.classList.remove('hover');
    },
    changeMode:function(component,event,helper){

        if(component.get('v.mode') != 'write'){
            console.log('changeMode');
            component.set('v.mode','write');
            // For now we put _changed directly when the user decide to change the data, otherwise we have to many event. Maybe we should had a button "Saved" to save the stored information of the quoteLineItem
            component.get('v.quoteLineItem')._changed = true;
            component.set('v.quoteLineItem',component.get('v.quoteLineItem'));
        }

    }
})