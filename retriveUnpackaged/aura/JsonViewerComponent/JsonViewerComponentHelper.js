({
	getJson:function(component){
        var action = component.get('c.getJsonValue');
            action.setParams({
               	field:component.get('v.field'),
                objectName:component.get('v.sObjectName'),
                recordId:component.get('v.recordId')
            });


        var that = this;
        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {

                var state = response.getState();
                var res   = action.getReturnValue();
                 if(state === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    }
})