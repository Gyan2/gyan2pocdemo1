({
    onInit:function(component,event,helper){
        console.log('Loaded');
        helper.getJson(component)
        .then(res => {
            var json = JSON.parse(res);
            var container = document.getElementById('jsoneditor');
            var options = {
                mode: 'view'
            };
        
            var editor = new JSONEditor(container, options);
                editor.set(json);
        
            if(component.get('v.isExpanded'))
                editor.expandAll();
        })
    }
})