(
    {
        updateDisplayStructure : function(component) {
            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            displayStructure = {
                _profiles: {},
                profiles: [],
                _fields: {},
                fields: []
            };
            for (var profile of component.get('v.profilesInformationOrder')) {
                this._extractFields(component, profilesInformation[profile], displayStructure);
            }

            // final profile fillup
            /*var profilesCount = displayStructure.profiles.length;
            for (var object of displayStructure.objects) {
                object.profiles[profilesCount-1] = object.profiles[profilesCount-1];
            }*/

            component.set('v.profilesDisplayStructure', displayStructure);
        },

        _extractFields : function(component, profileInfo, displayStructure) {
            var objectName = component.get('v.objectName');

            var fullName = profileInfo.fullName;
            var profileIndex = displayStructure._profiles[fullName];

            if (profileIndex === undefined) {
                profileIndex = Object.keys(displayStructure._profiles).length;
                displayStructure._profiles[fullName] = profileIndex;
                displayStructure.profiles.push(fullName);
            }
			var searchString = component.get('v.searchString').toLowerCase();
            for (var fieldVisibility of profileInfo.fieldPermissions) {
                if (fieldVisibility.field.indexOf(objectName + '.') == 0) { // if it starts with the object
                    var fieldIndex = displayStructure._fields[fieldVisibility.field];
                    var newObject = false;
                    if (fieldIndex === undefined) {// if its a new field, we need to fill for all profiles empty
                        newObject = true;
                        fieldIndex = Object.keys(displayStructure._fields).length;
                        displayStructure._fields[fieldVisibility.field] = fieldIndex;
                    }
				
                    if (newObject) {
                        displayStructure.fields[fieldIndex] = {
                            field:fieldVisibility.field, 
                            showMe : fieldVisibility.field.toLowerCase().indexOf(searchString) != -1,
                            profiles:[]} ;
                    }

                    displayStructure.fields[fieldIndex].profiles[profileIndex] = {
                        editable: fieldVisibility.editable,
                        readable: fieldVisibility.readable,
                        profile: fullName
                    };
                }
            }
        },

        saveToProfilesInformation : function(component) {

        },

        _handleGenericClick : function(component, profileName, fieldName, checkboxName, setTo /*true:false*/) {
            // it lists, per checkbox, which other checkboxes need to be set to true/false
            var actionsMapping = {
                readable : { falseToTrue : [],
                             trueToFalse : ['editable'] },
                editable : { falseToTrue : ['readable'],
                             trueToFalse : [] }

            };

            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            var fieldIndex = displayStructure._fields[fieldName];
            var profileIndex = displayStructure._profiles[profileName];

            var profileField = profilesInformation[profileName].fieldPermissions.find(x=>x.field==fieldName);
            var displayField = displayStructure.fields[fieldIndex].profiles[profileIndex];

            // set current default field
            var checkboxList = [checkboxName].concat(actionsMapping[checkboxName][setTo?'falseToTrue':'trueToFalse']);
            var valueToSet = setTo?'true':'false';

            for (var checkbox of checkboxList) {
                displayField[checkbox] = profileField[checkbox] = valueToSet;
            }
            profileField._hasChanged = true;
            component.set('v.profilesDisplayStructure',displayStructure);
        }
    }
)