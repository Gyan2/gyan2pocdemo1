(
    {
        handleInit : function(component, event, helper) {
            helper.updateDisplayStructure(component);
        },

        // TODO: Add type of click to value of button to reduce to one the methods handle.*Click
        handleReadableClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'readable', !button.get('v.selected'));
        },

        handleEditableClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'editable', !button.get('v.selected'));
        },
        
        handleSearch : function(component, event, helper) {
            var displayStructure = component.get('v.profilesDisplayStructure');
            var searchString = component.get('v.searchString').toLowerCase();
            for (var field of displayStructure.fields) {
                field.showMe = field.field.toLowerCase().indexOf(searchString)!=-1;
            }
            
            component.set('v.profilesDisplayStructure',displayStructure);
        }
    }
)