(
    {
        handleInit : function(component, event, helper) {
            helper.loadNotifications(component);
        },
        handleSelect : function(component, event, helper) {
            var notificationId = event.currentTarget.dataset.notificationId;

            var notifications = component.get('v.notifications');
            var notificationIndex = notifications.findIndex(x=>x.Id == notificationId);

            component.set('v._selectedIndex', notificationIndex);
            component.set('v._selected', notifications[notificationIndex]);
        },
        handleMarkAsRead : function(component, event, helper) {
            helper.markAsRead(component);
        }

    }
)