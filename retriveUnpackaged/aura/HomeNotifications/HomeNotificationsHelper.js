(
    {
        loadNotifications : function(component) {
            var action = component.get('c.auraGetNotifications');

            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    var returned = response.getReturnValue();

                    var userLanguage = returned.language;
                    for (var notification of returned.notifications) {
                        notification._title = notification[userLanguage + 'Title__c'] || notification.Name;
                        notification._content = notification[userLanguage + 'Content__c'] || notification.DefaultContent__c;
                    }

                    if (document.location.href.indexOf('/flexipageEditor/') != -1 && returned.notifications.length == 0) {
                        returned.notifications.push({
                            '_title' : 'Lorem ipsum dolor sit amet',
                            '_content' : '<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>' +
                                         '<p class="slds-text-align_center" ><img src="/projRes/ui-home-private/emptyStates/noTasks.svg" aria-hidden="true" /></p>' +
                                         '<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit, sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem. Ut enim ad minima veniam, quis nostrum exercitationem ullam corporis suscipit laboriosam, nisi ut aliquid ex ea commodi consequatur? Quis autem vel eum iure reprehenderit qui in ea voluptate velit esse quam nihil molestiae consequatur, vel illum qui dolorem eum fugiat quo voluptas nulla pariatur?</p>' +
                                         '<p class="slds-text-align_center" ><img src="/projRes/ui-home-private/emptyStates/noAssistant.svg" aria-hidden="true" /></p>' +
                                         '<p>At vero eos et accusamus et iusto odio dignissimos ducimus qui blanditiis praesentium voluptatum deleniti atque corrupti quos dolores et quas molestias excepturi sint occaecati cupiditate non provident, similique sunt in culpa qui officia deserunt mollitia animi, id est laborum et dolorum fuga. Et harum quidem rerum facilis est et expedita distinctio. Nam libero tempore, cum soluta nobis est eligendi optio cumque nihil impedit quo minus id quod maxime placeat facere possimus, omnis voluptas assumenda est, omnis dolor repellendus. Temporibus autem quibusdam et aut officiis debitis aut rerum necessitatibus saepe eveniet ut et voluptates repudiandae sint et molestiae non recusandae. Itaque earum rerum hic tenetur a sapiente delectus, ut aut reiciendis voluptatibus maiores alias consequatur aut perferendis doloribus asperiores repellat.</p>',
                            'Icon__c' : 'utility:light_bulb'
                        });

                        returned.notifications.push({
                            '_title' : 'Lorem ipsum',
                            'Icon__c' : 'standard:account'
                        });

                        returned.notifications.push({
                            '_title' : 'Dolor sit amet',
                            'Icon__c' : 'utility:announcement'
                        });

                        returned.notifications.push({
                            '_title' : 'Sed ut perspiciatis',
                            'Icon__c' : 'standard:partners'
                        });

                        returned.notifications.push({
                            '_title' : 'At vero eos et accusamus',
                            'Icon__c' : 'utility:trending'
                        });

                        returned.notifications.push({
                            '_title' : 'Voluptatum deleniti',
                            'Icon__c' : 'standard:custom_notification'
                        });

                        returned.notifications.push({
                            '_title' : 'Quis nostrud exercitation',
                            'Icon__c' : 'standard:maintenance_asset'
                        });

                        returned.notifications.push({
                            '_title' : 'Totam rem aperiam,',
                            'Icon__c' : 'utility:priority'
                        });







                    }
                    component.set('v.notifications', returned.notifications);
                    component.set('v._labels', returned.labels);

                    if(returned.notifications.length > 0) {
                        component.set('v._selected', returned.notifications[0]);
                        component.set('v._selectedIndex', 0);
                    }
                }
            });

            $A.enqueueAction(action);
        },

        markAsRead : function(component) {
            var action = component.get('c.auraMarkAsRead');
            component.set('v._loading', true);
            action.setParam('notificationId', component.get('v._selected').Id);

            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    var notifications = component.get('v.notifications');
                    var oldIndex = component.get('v._selectedIndex');
                    notifications.splice(oldIndex, 1);
                    if (oldIndex > 0) oldIndex--;
                    component.set('v._selectedIndex', oldIndex);
                    component.set('v._selected', notifications[oldIndex]);
                    component.set('v.notifications', notifications);
                }

                component.set('v._loading', false);
            });

            $A.enqueueAction(action);
        }
    }
)