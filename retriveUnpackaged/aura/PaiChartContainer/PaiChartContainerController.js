({
    testMethod : function(component, event, helper) {
        
        alert("Event Fired!");
    },
    
    
	doInIt : function(component, event, helper) {
        
        var action = component.get("c.getCase");
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") 
            {
                var allCase = response.getReturnValue();
                var totalCase = allCase.length;
                var emailCount = 0;
                var holdCaseCount = 0;
                var newCaseCount = 0;
                var assignedCaseCount = 0;
                for(var index = 0; index < allCase.length; index++) {
                    if(allCase[index].No_Email__c == "@") {
                        emailCount++;
                    }
                    if(allCase[index].Status == "New") {
                        newCaseCount++;
                    }
                    if(allCase[index].Status == "On Hold") {
                        holdCaseCount++;
                    }
                    if(allCase[index].Status == "Assigned") {
                        assignedCaseCount++;
                    }
                    
                }
                var noEmailChart = {type: 'pie',data: {
										datasets: [{
											data: [
												emailCount,
												totalCase - emailCount
											],
											backgroundColor: [
												"#0a5100",
												"#9eea93",
											],
											label: 'No Email Chart'
										}],
										labels: [
											'No Email',
											'Email'
                                		]
									},
									options: {
										responsive: true
									}
								};
                component.set("v.noEmailChart", noEmailChart);
                
                                                   

				var statusChart = {type: 'pie',data: {
										datasets: [{
											data: [
												newCaseCount,
												holdCaseCount,
                                                assignedCaseCount
											],
											backgroundColor: [
												"#4F209C",
                                                "#3D0053",
												"#2E79DD"
                                                
											],
											label: 'No Email Chart'
										}],
										labels: [
											'New',
											'On Hold',
                                    		'Assigned'
                                		]
									},
									options: {
										responsive: true
									}
								};                                                   
                                                   
                component.set("v.statusChart", statusChart ); 
            
            
            	var reasonChart = {type: 'pie',data: {
										datasets: [{
											data: [
												13,
												31,
                                                57,
                                                71
											],
											backgroundColor: [
												"#233049",
												"#DC3E41",
                                                "#E56725",
                                                "#FCCE38"
											],
											label: 'No Email Chart'
										}],
										labels: [
											'Reason1',
											'Reason2',
                                    		'Reason3',
                                            'Reason4'
                                		]
									},
									options: {
										responsive: true
									}
								};                                                   
                                                   
                component.set("v.reasonChart", reasonChart ); 
                                                   
                component.set("v.loadStatus", true);
                
                
            }
        });
		$A.enqueueAction(action);
		
	},
    
    
    
    
    
    
    
    
    
    
    
    
})