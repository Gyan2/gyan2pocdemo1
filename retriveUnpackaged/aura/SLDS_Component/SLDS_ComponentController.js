/**
 * Created by trakers on 08.06.18.
 */
({
    onInit:function(component,event,helper){
        component.set('v.icon',component.get('v.isOpen')?'utility:chevrondown':'utility:chevronright');
    },
    onClickButton:function(component,event,helper){
        component.set('v.isOpen',!component.get('v.isOpen'));
        component.set('v.icon',component.get('v.isOpen')?'utility:chevrondown':'utility:chevronright');

    }
})