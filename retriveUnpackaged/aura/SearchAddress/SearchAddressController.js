/**
 * Created by trakers on 03/04/2018.
 */
({
    onKeyUp:function(component,event,helper){
        var currentKey = event.currentTarget.value;
        setTimeout(function(){
            if(component.get('v.searchKey') == currentKey){
                helper.getSuggestionFromGoogle(component)
                .then(val => {
                    var obj = JSON.parse(val);
                    obj = helper.handleMatches(obj);
                    console.log(obj);
                    component.set('v.result',obj.predictions);
                })
            }
        }, 300);

    },
    onSelectAddress:function(component,event,helper){
        event.preventDefault();
        var inputs = event.target.closest('li').getElementsByClassName('itemValue');

        var selected = {};
        for(var i = 0; i < inputs.length; i++){
            selected[inputs[i].name] = inputs[i].value;
        }

        helper.getFullAddressFromGoogle(component,selected.id)
        .then(val => {
            var obj = JSON.parse(val).result;
            console.log(obj);
            selected['location'] = obj.geometry.location;

            var fields = ['street_number','postal_code','country','locality','route']
            obj.address_components.forEach(val => {
                val.types.forEach(type => {
                    if(fields.includes(type)){
                        selected[type] = val.long_name;
                    }
                    if(type == 'country'){
                        selected['country_iso'] = val.short_name;
                    }
                })
            })
            console.log(selected);
            component.set('v.selected',selected);
            component.set('v.display',false);
        })
    }
})