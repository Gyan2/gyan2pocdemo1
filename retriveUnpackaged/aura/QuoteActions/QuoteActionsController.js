/**
 * Created by rebmangu on 22/02/2018.
 */
({
    addProducts:function(component,event,helper){
        //component.find('selectProducts')
        component.set('v.selectDisplay',true);
    },
    onInit:function(component,event,helper){

        var fields = component.get('v.selectProductFieldsSource');
            fields = fields.split("'").join('"');
        console.log(JSON.parse(fields));
        component.set('v.selectProductFields',JSON.parse(fields));
    }
})