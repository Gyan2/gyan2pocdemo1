/**
 * Created by rebmangu on 06/02/2018.
 */
({
    returnToDropzone:function(component,event,helper){
        component.set('v.isProcessing',false);
        component.set('v.fileName',null);
    },
    closeClick:function(component,event,helper){
          component.set('v.display',false);
    },
    execute:function(component,event,helper){
        var data = component.find('fileHandlerPreview').getResult();

        component.set('v.result',data.result);
        component.set('v.display',false);
    },
    handleFileChange:function(component,event,helper){
        console.log('File click');

        var options = {
            delimiter:';',
            carriage:'\n'
        }
        var file = event.target.files[0];
        helper.loadFile(file,options,helper)
        .then($A.getCallback(function(csv){
            component.set('v.fileName',file.name);
            component.set('v.csv',csv);
            component.set('v.isValid',true);
            component.set('v.isProcessing',true);
        }))
        .catch(err => {
            console.log('There is an error');
            console.log(err);
            $A.util.addClass(document.getElementById('fileCard'),'hasError');
            component.set('v.isValid',false);
            if(err.hasOwnProperty('message')){
                 component.set('v.errorMessage',err.message);
            }else{
                 component.set('v.errorMessage','Undefined error');
            }
        })

        event.stopPropagation();
        event.preventDefault();

    },






    /* For Inner Drop Zone */


    fileHandlerHover:function(component,event,helper){
        $A.util.removeClass(event.currentTarget,'hasError');
        if(!event.currentTarget.classList.contains('slds-has-drag-over')){
            $A.util.addClass(event.currentTarget , 'slds-has-drag-over');
        }

    },
    fileHandlerHoverFinish:function(component,event,helper){
        $A.util.removeClass(event.currentTarget , 'slds-has-drag-over');
    },
    fileDragHandler:function(component,event,helper){
        console.log('IN');
        if(!event.currentTarget.classList.contains('slds-has-drag-over')){
            $A.util.addClass(event.currentTarget , 'slds-has-drag-over');
        }

        event.preventDefault();
        event.stopPropagation();
    },
    fileDragStart:function(component,event,helper){
        document.getElementById('drop-layer').style.display = 'block';
    },
    fileDragEnd:function(component,event,helper){
        console.log('leave');
        document.getElementById('drop-layer').style.display = 'none';
        $A.util.removeClass(document.getElementsByClassName('slds-has-drag-over')[0], 'slds-has-drag-over');


        event.preventDefault();
        event.stopPropagation();
    },
    fileDropHandler:function(component,event,helper){
         console.log('File drop');

         $A.util.removeClass(event.currentTarget , 'slds-has-drag-over');
         document.getElementById('drop-layer').style.display = 'none';

         var options = {
             delimiter:';',
             carriage:'\n'
         }
         var file = event.dataTransfer.files[0];
         helper.loadFile(file,options,helper)
         .then($A.getCallback(function(csv){
             console.log(csv);
             component.set('v.fileName',file.name);
             component.set('v.display',true);
             component.set('v.csv',csv);
             component.set('v.isValid',true);
             component.set('v.isProcessing',true);

         }))
         .catch(err => {
             console.log('There is an error');
             console.log(err);
             $A.util.addClass(document.getElementById('fileCard'),'hasError');
             component.set('v.isValid',false);
             if(err.hasOwnProperty('message')){
                 component.set('v.errorMessage',err.message);
             }else{
                 component.set('v.errorMessage','Undefined error');
             }

         })



         event.stopPropagation();
         event.preventDefault();

     }
})