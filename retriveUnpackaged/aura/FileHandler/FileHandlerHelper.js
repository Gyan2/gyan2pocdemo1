/**
 * Created by rebmangu on 06/02/2018.
 */
({
    loadFile:function(file,options,helper){


            return new Promise(function (resolve, reject) {
                   var reader  = new FileReader();
                   reader.onloadend = function () {
                       resolve(helper.csvToJSON(reader.result,options,helper));
                   }

                   if (file) {
                      reader.readAsText(file); //reads the data as a URL
                   }

            });

    },
    CSVToArray:function(strData, strDelimiter) {

                    strDelimiter = (strDelimiter || ";");

                    var objPattern = new RegExp((
                    "(\\" + strDelimiter + "|\\r?\\n|\\r|^)" +
                    "(?:\"([^\"]*(?:\"\"[^\"]*)*)\"|" +
                    "([^\"\\" + strDelimiter + "\\r\\n]*))"), "gi");

                    var arrData = [[]];
                    var arrMatches = null;
                    while (arrMatches = objPattern.exec(strData)) {
                        var strMatchedDelimiter = arrMatches[1];
                        if (strMatchedDelimiter.length && (strMatchedDelimiter != strDelimiter)) {
                            arrData.push([]);
                        }

                        if (arrMatches[2]) {
                            // We found a quoted value. When we capture
                            // this value, unescape any double quotes.
                            var strMatchedValue = arrMatches[2].replace(
                            new RegExp("\"\"", "g"), "\"");
                        } else {
                            // We found a non-quoted value.
                            var strMatchedValue = arrMatches[3];
                        }
                        if(strMatchedValue != ''){
                             arrData[arrData.length - 1].push(strMatchedValue);
                        }

                    }
                    return arrData.filter(x => x.length > 0);
    },
    csvToJSON:function(csv,options,helper){


            var array = helper.CSVToArray(csv,options.delimiter);
            var objArray = [];
            for (var i = 1; i < array.length; i++) {
                objArray[i - 1] = {};
                if(array)
                for (var k = 0; k < array[0].length && k < array[i].length; k++) {
                    var key = array[0][k].trim();
                    objArray[i - 1][key] = array[i][k]
                }
            }
            return objArray;

    }
})