(
    {
        ajaxRequest : function(component, event, helper) {
            //Creaet new request
            var xmlhttp = new XMLHttpRequest();

            //Handle response when complete
            xmlhttp.onreadystatechange = function(component) {
                if (xmlhttp.readyState == 4 ) {
                    console.log('xmlhttp: ' , JSON.parse(JSON.stringify(xmlhttp)));
                    params.callbackMethod.call(this, xmlhttp);
                }
            };

            var params = event.getParam('arguments');
            if (params) {
                console.log('params:', JSON.parse(JSON.stringify(params)));
                //Set parameters for the request
                xmlhttp.open(params.method, params.url, params.async);

                if (params.headers !== undefined) {
                    for (var header of params.headers) {
                        xmlhttp.setRequestHeader(header.header, header.value);
                    }
                }

                //Send the request
                xmlhttp.send(params.payload);
            }
        }
    }
)