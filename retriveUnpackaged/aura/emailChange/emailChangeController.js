/**
 * Created by rebmangu on 31/01/2018.
 */
({
    onInit:function(component,event,helper){

    },
    onChange:function(component,event,helper){
        console.log('Changed');
        var button = component.find('button');
        console.log(component.get('v.email').length <= 4);
        button.set('v.disabled',component.get('v.email').length <= 4);
    },
    onClickChange:function(component,event,helper){
        console.log('Clicked');
        var button = event.getSource();
        component.set('v.result',null);
        button.set("v.disabled",true);
        var email = component.find('email').get('v.value');

        component.set('v.email',email);
        helper.saveUserInformation(component)
        .then(res => {
            console.log(res);
            component.set('v.result',res);
            button.set("v.disabled",false);
        }).catch(err => {
            console.log(err);
        })

    }
})