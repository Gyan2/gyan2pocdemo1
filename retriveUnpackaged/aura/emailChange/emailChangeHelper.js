/**
 * Created by rebmangu on 31/01/2018.
 */
({

     saveUserInformation:function(component){
        var action = component.get('c.changeUserEmail');
            action.setParams({
               newEmail :   component.get('v.email'),
            });


        var that = this;
        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {

                var state = response.getState();
                var res   = action.getReturnValue();
                 if(state === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    }
})