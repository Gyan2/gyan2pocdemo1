/**
 * Created by Jai Chaturvedi on 29/05/2017.
 */
({
    loadValues : function (component) {
        var record = component.get("v.record");
    },

    choose : function (component,event) {
        var chooseEvent = component.getEvent("lookupChoose");
        chooseEvent.setParams({
            "recordId" : component.get("v.record").Id,
            "record":component.get("v.record")
        });
        chooseEvent.fire();
        console.log('event fired');
    }
})