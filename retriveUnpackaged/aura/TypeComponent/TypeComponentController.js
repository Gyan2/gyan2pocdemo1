({
    selectType : function(component, event, helper){      
    // get the selected User from list  
      var getSelectType = component.get("v.oType");
     
    // call the event   
      var compEvent = component.getEvent("oSelectedTypeEvent");
    // set the Selected user to the event attribute.  
         compEvent.setParams({"TypeByEvent" : getSelectType});  
    // fire the event  
      compEvent.fire();
    },
    
})