(
    {
        handleSendMessage : function(component, event, helper) {
            var channel = event.getParam('channel');
            var message = event.getParam('message');

            if (channel.indexOf('PermissionsOverview_') == 0){
                var severity = channel.replace('PermissionsOverview_','');
                var msg = component.get('v.message') || {};
                if (msg.timeout != undefined) clearTimeout(msg.timeout);

                component.set('v.message', {
                    severity : severity.toLowerCase(),
                    message : message,
                    timeout : setTimeout(function(){component.set('v.message',{});}, 5000)
                });

            }
        }
    }
)