(
    {
        handleInit : function(component, event, helper) {
            helper.getProjectInformation(component);
        },

        handleEnterEditMode : function(component, event, helper) {
            helper.getProjectInformation(component, true);
        },

        handleExitEditMode : function(component, event, helper) {
            helper.getProjectInformation(component, false);
        },

        handleSave : function(component, event, helper) {
            helper.doSave(component);
        }
    }
)