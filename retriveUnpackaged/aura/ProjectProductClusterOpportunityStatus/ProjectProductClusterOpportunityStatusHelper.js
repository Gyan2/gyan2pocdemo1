(
    {
        getProjectInformation : function(component, editMode) {
            component.set('v.loading',true);
            var action = component.get('c.auraGetOpportunityCountPerCluster');

            action.setParam('projectId', component.get('v.recordId'));

            action.setCallback(this, function(response){
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    var returned = response.getReturnValue();
                    var clusterMapping = {
                        'Mechanical Key Systems':'MKS',
                        'Door Hardware' : 'DHW',
                        'Interior Glass Systems' : 'IGS',
                        'Entrance Systems' : 'ENS',
                        'Lodging Systems':'LGS',
                        'Safe Locks':'SAL',
                        'Electronic Access & Data':'EAD',
                        'Services':'SVC'
                    }
                    var newCounts = {MKS : 0, DHW : 0, IGS : 0, ENS : 0, LGS : 0, SAL : 0, EAD : 0, SVC : 0};
                    for (var i in returned.counts) {
                        newCounts[clusterMapping[i]] = returned.counts[i];
                    }

                    returned.counts = newCounts;
                    component.set('v.projectInformation', returned);
                    if (editMode != undefined) component.set('v.editMode', editMode);
                }

                component.set('v.loading', false);
            });

            $A.enqueueAction(action);
        },

        doSave : function(component) {
            component.set('v.loading', true);
            var self = this;
            var action = component.get('c.auraSaveClusters');

            action.setParam('project',component.get('v.projectInformation').project);

            action.setCallback(this, function(response){
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    component.set('v.editMode', false);
                    $A.get('e.force:refreshView').fire();
                } else {
                    self.showToast(response.getError()[0].pageErrors[0].message,null,'error');
                }
                component.set('v.loading', false);
            });

            $A.enqueueAction(action);
        },

        showToast : function (message, title, type) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                type : type || "success",
                title : title,
                message : message
            });
            toastEvent.fire();
        }
    }
)