(
    {
        handleInit : function(component, event, helper) {
            helper.getSessionId(component);
        },

        handleFetchProfileClick : function(component, event, helper) {
            helper.getProfilesInformation(component, {fullName:event.getSource().get('v.value')});
        },

        handleSelectedProfilesChange : function(component, event, helper) {
            helper.loadDiff(component);
        },

        handleSave : function(component, event, helper) {
            console.log(JSON.parse(JSON.stringify(component.get('v.profilesInformation'))));
            helper.saveProfile(component, event.getSource().get('v.value'));
        },

        loadChanges : function(component, event, helper) {
            var changesList = [];
            var profilesInformation = component.get('v.profilesInformation');

            for (var profileInformationName in profilesInformation) {
                var profileInformation = profilesInformation[profileInformationName];
                changesList.push({
                    fullName : profileInformation.fullName,
                    applicationVisibilities : profileInformation.applicationVisibilities.filter(x=>x._hasChanged==true),
                    classAccesses : profileInformation.classAccesses.filter(x=>x._hasChanged==true),
                    fieldPermissions : profileInformation.fieldPermissions.filter(x=>x._hasChanged==true),
                    objectPermissions : profileInformation.objectPermissions.filter(x=>x._hasChanged==true),
                    pageAccesses : profileInformation.pageAccesses.filter(x=>x._hasChanged==true)
                });
            }

            component.set('v._changedProfilesInformation', changesList);
        }
    }
)