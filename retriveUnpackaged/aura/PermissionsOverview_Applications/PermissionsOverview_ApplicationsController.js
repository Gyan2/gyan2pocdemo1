(
    {
        handleInit : function(component, event, helper) {
            helper.updateDisplayStructure(component);
        },

        handleVisibleClick : function(component, event, helper) {

            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'visible', !button.get('v.selected'));

            /*
            var displayStructure = component.get('v.profilesDisplayStructure');
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');

            var appIndex = displayStructure._apps[buttonValueSplit[0]];
            var profileIndex = displayStructure._profiles[buttonValueSplit[1]];

            displayStructure.apps[appIndex].profiles[profileIndex].visible = button.get('v.selected')?'false':'true';

            component.set('v.profilesDisplayStructure',displayStructure);*/

        },
        handleDefaultClick : function(component, event, helper) {

            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'default', !button.get('v.selected'));


            /*
            var displayStructure = component.get('v.profilesDisplayStructure');
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');

            var appIndex = displayStructure._apps[buttonValueSplit[0]];
            var profileIndex = displayStructure._profiles[buttonValueSplit[1]];

            // remove old default app
            displayStructure.apps.find(x=>x.profiles[profileIndex].default=='true').profiles[profileIndex].default = 'false';
            // set current default app
            displayStructure.apps[appIndex].profiles[profileIndex].visible = 'true';
            displayStructure.apps[appIndex].profiles[profileIndex].default = 'true';

            component.set('v.profilesDisplayStructure',displayStructure);*/
        },

        handleSearch : function(component, event, helper) {
            var displayStructure = component.get('v.profilesDisplayStructure');
            var searchString = component.get('v.searchString').toLowerCase();
            for (var app of displayStructure.apps) {
                app.showMe = app.application.toLowerCase().indexOf(searchString)!=-1;
            }
            
            component.set('v.profilesDisplayStructure',displayStructure);
        }
    }
)