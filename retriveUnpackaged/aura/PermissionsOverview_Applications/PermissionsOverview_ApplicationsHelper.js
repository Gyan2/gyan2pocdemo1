(
    {
        updateDisplayStructure : function(component) {
            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            displayStructure = {
                _profiles: {},
                profiles: [],
                _apps: {},
                apps: []
            };
            for (var profile of component.get('v.profilesInformationOrder')) {
                this._extractApps(component, profilesInformation[profile], displayStructure);
            }

            component.set('v.profilesDisplayStructure', displayStructure);
        },

        _extractApps : function(component, profileInfo, displayStructure) {

            var fullName = profileInfo.fullName;
            var profileIndex = displayStructure._profiles[fullName];

            if (profileIndex === undefined) {
                profileIndex = Object.keys(displayStructure._profiles).length;
                displayStructure._profiles[fullName] = profileIndex;
                displayStructure.profiles.push(fullName);
            }

            var searchString = component.get('v.searchString').toLowerCase();
            for (var appVisibility of profileInfo.applicationVisibilities) {
                var appIndex = displayStructure._apps[appVisibility.application];
                var newApp = false;
                if (appIndex === undefined) {// if its a new app, we need to fill for all profiles empty
                    newApp = true;
                    appIndex = Object.keys(displayStructure._apps).length;
                    displayStructure._apps[appVisibility.application] = appIndex;
                }

                if (newApp) {
                    displayStructure.apps[appIndex] = {
                        application:appVisibility.application,
                        showMe : appVisibility.application.toLowerCase().indexOf(searchString) != -1,
                        profiles:[]
                    } ;
                }

                displayStructure.apps[appIndex].profiles[profileIndex] = {default: appVisibility.default, visible: appVisibility.visible, profile: fullName};

            }
        },

        _handleGenericClick : function(component, profileName, appName, checkboxName, setTo /*true:false*/) {
            // it lists, per checkbox, which other checkboxes need to be set to true/false
            var actionsMapping = {
                default : {  falseToTrue : ['visible'],
                             trueToFalse : [] },
                visible : {  falseToTrue : [],
                             trueToFalse : [] }
            };

            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            var appIndex = displayStructure._apps[appName];
            var profileIndex = displayStructure._profiles[profileName];

            var profileApp = profilesInformation[profileName].applicationVisibilities.find(x=>x.application==appName);
            var defaultProfileApp = profilesInformation[profileName].applicationVisibilities.find(x=>x.default=='true');

            var defaultAppIndex = displayStructure._apps[defaultProfileApp.application];

            var displayApp = displayStructure.apps[appIndex].profiles[profileIndex];
            var defaultDisplayApp = displayStructure.apps[defaultAppIndex].profiles[profileIndex];


            // set current default field
            var checkboxList = [checkboxName].concat(actionsMapping[checkboxName][setTo?'falseToTrue':'trueToFalse']);
            var valueToSet = setTo?'true':'false';

            for (var checkbox of checkboxList) {
                displayApp[checkbox] = profileApp[checkbox] = valueToSet;
            }

            if (checkboxName == 'default' && setTo) {
                defaultDisplayApp.default = defaultProfileApp.default = 'false';
                defaultProfileApp._hasChanged = true;

            }

            profileApp._hasChanged = true;

            component.set('v.profilesDisplayStructure',displayStructure);
        }
    }
)