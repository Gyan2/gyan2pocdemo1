/**
 * Created by trakers on 24.07.18.
 */
({
    onEnter:function(component,event,helper){
         event.currentTarget.classList.add('hover');
    },
    onExit:function(component,event,helper){
         event.currentTarget.classList.remove('hover');
    },
    changeMode:function(component,event,helper){
         component.set('v.mode','write');
    }
})