({
	
    doInit: function(component, event, helper) {
        setInterval(function(){ 
            component.set('v.currentTime',new Date().toLocaleTimeString());
        }, 500);
    },
    
})