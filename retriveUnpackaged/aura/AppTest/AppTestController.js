/**
 * Created by rebmangu on 24/04/2018.
 */
({
    onHover:function(component,event,helper){
            var elementId = event.currentTarget.dataset.id
            component.set('v.elementHovered',elementId);
            component.set('v.mouseHover',true);
            console.log(elementId);
    },
    onHoverLeave:function(component,event,helper){
        component.set('v.mouseHover',false)
        setTimeout(function(){
            if(!component.get('v.mouseHover')){
                component.set('v.elementHovered',null);
           }
        }, 150);
    },
    validate:function(component,event,helper){
        component.find('comp').forEach(val => {
            //console.log(val);
            val.validate();
        })
    },
    test1:function(component,event,helper){
        console.log('Test lightning');
    },
    test2:function(component,event,helper){
         console.log('Test html');
    }
})