/**
 * Created by trakers on 20.06.18.
 */
({
    deleteAlternatives:function(component,event,helper){
            var detailId = component.get('v.quoteLineItem.Id');
            var quote = component.get('v.quote');
            var newProducts = quote.QuoteLineItems.find(val => val.Id == detailId).AlternativeProducts__c.filter(val => val.PricebookEntryId != id);

                quote.QuoteLineItems.find(val => val.Id == detailId).AlternativeProducts__c = newProducts;

            component.set('v.selectedDetail',quote.QuoteLineItems.find(val => val.Id == detailId));
            component.set('v.quote',quote);
            // Upsert
            helper.upsertAlternative(component,quote.QuoteLineItems.find(val => val.Id == detailId).AlternativeProducts__c)
            .then($A.getCallback(val => {
                console.log(val);
            }))
    },
})