/**
 * Created by trakers on 20.06.18.
 */
({
    upsertAlternative:function(component,alternativeProducts){
        var action = component.get('c.auraUpsertAlternatives');

        var that = this;
        action.setParams({
            quoteLineItemId             :  component.get('v.quoteLineItem.Id'),
            alternativeProducts         :  JSON.stringify(alternativeProducts)
        });

        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                console.log(res);
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    },
})