/**
 * Created by trakers on 28.05.18.
 */
({
    handleExtraFields : function(field,record){
            var result = [];
            var value;

            if(field.includes('.')){
                value = record;
                field.split('.').forEach(subField => {
                    if(value != undefined){
                        value = value[subField];
                    }
                })
            }else{
                value = record[field];
            }

            if(value != undefined){
                result.push(value);
            }

            return result.join(' • ');


        }
})