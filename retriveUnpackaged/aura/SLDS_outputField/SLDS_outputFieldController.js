/**
 * Created by trakers on 28.05.18.
 */
({
    handleInit:function(component,event,helper){
        component.set('v.value',helper.handleExtraFields(component.get('v.field.apiName'),component.get('v.object')));
    }
})