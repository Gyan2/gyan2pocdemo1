/**
 * Created by rebmangu on 08/02/2018.
 */
({
    generateCsvToDisplay:function(component,keys){

        var csv = component.get('v.fileHandler_csv');
        var result = [];
            csv.forEach(val => {
                var row = [];
                for(var key in keys){
                    if(keys[key] != null && val.hasOwnProperty(keys[key].value)){
                         row.push(val[keys[key].value]);
                    }else{
                        row.push(null);
                    }

                }
                result.push(row);
            })
        return result;
    },
    checkIfValid:function(){
        console.log('check validity');
        var selectors = document.getElementsByClassName('fieldSelector');
        var isValid = true;
        for(var i=0; i < selectors.length; i++){
            if(selectors[i].selectedIndex == 0){
                isValid = false;
                continue;
            }
        }
        console.log(isValid);
        return isValid;
    },
    checkSelector:function(component,event,helper){
                console.log('Change selector');
                // Reset the error handler
                if(event.currentTarget != undefined){
                    var parent = event.currentTarget.parentElement;
                    //validate
                    if(event.currentTarget.selectedIndex == 0){
                        parent.classList.remove('successSelection');
                    }else{
                        parent.classList.add('successSelection');
                    }
                }


                var selectors = document.getElementsByClassName('fieldSelector');
                var toggleOptions = function(index,val){
                                        for(var i = 0; i < selectors.length; i++){
                                            selectors[i].options[index].disabled = val;
                                        }
                                    }


                // reset all
                for(var i = 0; i < selectors.length; i++){
                       for(var j=1; j < selectors[i].length; j++){
                               selectors[i].options[j].disabled = false;
                       }
                }

                // disable the selectedOnes

                var keys = [];

                for(var i = 0; i < selectors.length; i++){
                    var key = null;

                       for(var j=1; j < selectors[i].length; j++){
                           if(selectors[i].selectedIndex == j){
                               toggleOptions(j,true);
                               key = {value:selectors[i].options[j].text,api:selectors[i].options[j].value};
                           }
                       }

                    keys.push(key);
                }

                component.set('v.fileHandler_csvToDisplay',helper.generateCsvToDisplay(component,keys));
                component.set('v.fileHandler_isValid',helper.checkIfValid());
    }
})