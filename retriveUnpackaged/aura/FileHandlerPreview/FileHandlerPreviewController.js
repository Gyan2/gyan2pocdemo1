/**
 * Created by rebmangu on 07/02/2018.
 */
({
    onInit:function(component,event,helper){
        var csv = component.get('v.fileHandler_csv');
        if(csv.length == 0)
            return;


       var csvKeys = [];
       for(var key in csv[0]){
            csvKeys.push(key);
       }
       component.set('v.fileHandler_csvKeys',csvKeys);

    },
    onChange:function(component,event,helper){
        console.log('onChange');
        var csv = component.get('v.fileHandler_csv');
        if(csv.length == 0)
           return;


       var csvKeys = [];
       for(var key in csv[0]){
            csvKeys.push(key);
       }

       component.set('v.fileHandler_csvKeys',csvKeys);
       //component.set('v.csvToDisplay',null);
       helper.checkSelector(component,event,helper);
    },
    getResult:function(component,event,helper){
        var finalCSV = component.get('v.fileHandler_csvToDisplay');
        var keys = component.get('v.fileHandler_keys');
        var result = [];
        var errors = [];

        finalCSV.forEach(val => {

            let _pushField = true;
            let _row = {};
            for(var index in val){
                if((val[index] == null || val[index] == '') && keys[index].required){
                    // We don't add it as it's required, we return it as a wrong list
                    _pushField = false;
                }
                 _row[keys[index].api] = val[index];
            }

            if(_pushField){
                result.push(_row);
            }else{
                errors.push(_row);
            }



        })


        return {result:result,errors:errors};
    },
    checkSelector:function(component,event,helper){
        helper.checkSelector(component,event,helper);
    }
})