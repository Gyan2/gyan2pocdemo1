(
    {
        getPartnersInformation : function(component) {
            var action = component.get('c.auraGetPartners');

            component.set('v._loading',true);

            action.setParams({
                fields     : component.get('v.fields').replace(/ */g,'').split(','),
                recordId   : component.get('v.recordId'),
                queryOrder : component.get('v.order'),
                queryLimit : component.get('v.limit')
            });
            var helper = this;
            action.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS' && component.isValid()) {
                    var returned = response.getReturnValue();

                    var data = [];

                    var labelOverrides = {};
                    for (var labelOverride of component.get('v.fieldLabelOverrides').split(',')) {
                        var labelOverrideSplit = labelOverride.split(':');
                        labelOverrides[labelOverrideSplit[0]] = labelOverrideSplit[1];
                    }

                    helper._createHeaders(component, returned.fields, labelOverrides );
                    component.set('v._labels', returned.labels);
                    var headers = [];
                    for (var field in returned.fields) {
                        headers.push(returned.fields[field]);
                        //headers.push($A.getReference('$Label.c.' + 'LTNG_Partners_AccountToId'));
                    }
                    //data.push([''].concat(headers));



                    for (var partner of returned.records) {
                        var record = [partner.Id];
                        for (var field of headers) {
                            record.push({
                                type : {
                                    REFERENCE : 'url',
                                    BOOLEAN : 'checkbox',
                                    ADDRESS : 'address',
                                    DATETIME : 'datetime',
                                    DATE : 'date'
                                }[field.type] || 'text',
                                value : helper._extract(partner, field.apiName),
                                label : (field.type === 'REFERENCE'?helper._extract(partner, field.apiName.replace(/__c$/,'__r.Name').replace(/Id$/,'.Name')):'')
                            });
                        }
                        data.push(record);
                    }

                    component.set('v._data', data);
                    component.set('v._hasEditAccess',returned.hasEditAccess);
                    component.set('v._roles', returned.roles);
                    component.set('v._loading',false);
                    console.log(returned);
                } else {
                    helper._showError(component, 'Unable to get Partners from Server');
                    console.log(JSON.parse(JSON.stringify(response.getError())));
                }
            });

            $A.enqueueAction(action);
        },

        deletePartner : function(component, partnerRecordId) {
            var action = component.get('c.auraDeletePartner');

            action.setParam('recordId', partnerRecordId);
            var helper = this;
            action.setCallback(this, function(response){
                if (response.getState() === 'SUCCESS' && component.isValid()) {
                    var returned = response.getReturnValue();
                    helper._showToast($A.get('$Label.ObjectHomeListViewSettings.DeleteListViewSuccessMsg'));
                    helper.getPartnersInformation(component);
                } else {
                    helper._showError(component, 'Unable to delete partner');
                    console.log(JSON.parse(JSON.stringify(response.getError())));
                }
            });

            $A.enqueueAction(action);
        },

        savePartners : function (component) {
            var action = component.get('c.auraSavePartners');
            component.set('v._newError','');

            var allNewPartners = component.get('v._newPartners');
            var newPartners = [];
            var primarySelected = component.get('v._primarySelected');
            for (var i in allNewPartners) {
                var partner = allNewPartners[i];
                partner.IsPrimary = i == primarySelected;
                if (partner.AccountToId != undefined) newPartners.push(partner);
            }

            action.setParams({
                partnersData : JSON.stringify(newPartners) //component.get('v._newPartners').filter(x=>x.AccountToId!=undefined)
            });
            var helper = this;
            action.setCallback(this, function(response){
                if (response.getState() === 'SUCCESS' && component.isValid()) {
                    component.find('add-partner-modal').set('v.open',false);
                    helper._showToast($A.get('$Label.FeedActions.GenericSuccessMessage'));
                    helper.getPartnersInformation(component);

                } else {
                    var firstError = response.getError()[0];
                    var firstErrorText;

                     if (firstError.pageErrors != undefined && firstError.pageErrors.length > 0) {
                         component.set('v._newError', firstError.pageErrors[0].message);
                     } else if (firstError.message != undefined) {
                         component.set('v._newError', firstError.message);
                     } else if (firstError.fieldErrors.AccountToId != undefined && firstError.fieldErrors.AccountToId.length > 0) {
                         component.set('v._newError', firstError.fieldErrors.AccountToId[0].message);
                     } else {
                         component.set('v._newError', 'Unexpected error');
                         console.log(JSON.parse(JSON.stringify(firstError)));
                     }
                }
            });

            $A.enqueueAction(action);
        },


        _extract : function (paramObject, paramField) {
            if (paramField.indexOf('.') == -1) {
                return paramObject[paramField];
            } else {
                var fieldSplit = paramField.split('.');
                var currentLevel = fieldSplit.shift();

                if (paramObject[currentLevel] == null){
                    return null;
                } else {
                    return this._extract(paramObject[currentLevel], fieldSplit.join('.'));
                }
            }
        },

        _createHeaders : function (component, fields, overrides) {
            var headerComponents = [];
            for (var field in fields) {
                if (overrides[field] !== undefined) {
                    headerComponents.push(["ui:outputText",{
                        "value" : $A.getReference('$Label.c.' + overrides[field])
                    }]);
                } else {
                    headerComponents.push(["ui:outputText",{
                        "value" : fields[field].label
                    }]);
                }

            }
            var helper = this;
            $A.createComponents(headerComponents,function(components, status, errorMessage){
                if (status === "SUCCESS") {
                    component.set('v._headers',components);
                } else {
                    helper._showToast(errorMessage,'Error fetching headers','error');
                }
            });
        },

        _showError : function(component, error) {
            component.set('v._error', error);
        },

        _showNewError : function(component, error) {
            component.set('v._newError', error);
        },

        _showToast : function (message, title, type) {
            var toastEvent = $A.get("e.force:showToast");
            toastEvent.setParams({
                type : type || "success",
                title : title,
                message : message
            });
            toastEvent.fire();
        },
    }
)