(
    {
        handleInit : function (component, event, helper) {
            helper.getPartnersInformation(component);
            component.set('v._accountLookupFields', component.get('v.accountLookupFields').replace(/ /g,'').split(','));
        },

        handleRedirect : function (component, event, helper) {
            event.preventDefault();
            var navEvt = $A.get('e.force:navigateToSObject');
            navEvt.setParams({
                recordId : event.currentTarget.dataset.redirectToId,
                slideDevName : 'detail'
            });
            navEvt.fire();
        },

        showAddPartner : function (component, event, helper) {
            var newPartners = [];
            component.set('v._newError','');
            if (component.get('v.sObjectName') == 'Account') {
                for (var i = 0; i < 5; i++) {
                    newPartners.push({/*sobjectType:'Partner',*/ AccountFromId:component.get('v.recordId')});
                }
            } else {
                for (var i = 0; i < 5; i++) {
                    newPartners.push({/*sobjectType:'Partner',*/ OpportunityId:component.get('v.recordId')});
                }
            }
            component.set('v._newPartners', newPartners);
            component.find('add-partner-modal').set('v.open',true);
        },

        hideAddPartner : function (component, event, helper) {
            component.find('add-partner-modal').set('v.open',false);
        },

        deletePartner : function (component, event, helper) {
            helper.deletePartner(component, event.getSource().get('v.value'));
        },

        savePartners : function (component, event, helper) {
            helper.savePartners(component);
        },

        handlePrimaryRadioClick : function(component, event, helper) {
            component.set('v._primarySelected',event.getSource().get('v.value'));
        },

        removeLimit : function (component, event, helper) {
            component.set('v.limit',0);
            helper.getPartnersInformation(component);
        }
    }
)