(
    {
        extractJSON : function(component, helper) {
            var systemSalesOrgsFilterList = ((component.get('v.systemSalesOrgsFilter')||'')==''?[]:component.get('v.systemSalesOrgsFilter').replace(/ /g,'').split(','));
            var systemSalesChannelFilterList = ((component.get('v.systemSalesChannelFilter')||'')==''?[]:component.get('v.systemSalesChannelFilter').replace(/ /g,'').split(','));
            var systemPartnerFunctionFilterList = ((component.get('v.systemPartnerFunctionFilter')||'')==''?[]:component.get('v.systemPartnerFunctionFilter').replace(/ /g,'').split(','));

            var rawSapJson = component.get('v.record.SAPJSON__c');
            var sapJson = {};

            if (rawSapJson !== null && rawSapJson !== undefined && rawSapJson.length > 0 && rawSapJson[0] == '{') {
                sapJson = JSON.parse(rawSapJson);

                sapJson.salesArrangements = (sapJson.salesArrangements || []).filter(salesArrangement=>
                    (systemSalesOrgsFilterList.length==0||systemSalesOrgsFilterList.indexOf(salesArrangement.salesOrg) > -1) &&
                    (systemSalesChannelFilterList.length==0||systemSalesChannelFilterList.indexOf(salesArrangement.distributionChannel) > -1)
                );

                sapJson.salesArrangements.forEach(salesArrangement=>{
                    salesArrangement.parties = (salesArrangement.parties || []).filter(party=>
                        (systemPartnerFunctionFilterList.length == 0 || systemPartnerFunctionFilterList.indexOf(party.roleCode) > -1)
                    );
                });
            }

            component.set('v.SAPJSON', sapJson);
        },

        initUserFilters : function(component, helper) {
            var userSalesOrgsAvailableFilter = component.get('v.userSalesOrgsAvailableFilter') || '';
            var userSalesChannelAvailableFilter = component.get('v.userSalesChannelAvailableFilter') || '';
            var userPartnerFunctionAvailableFilter = component.get('v.userPartnerFunctionAvailableFilter') || '';

            var sapJson = component.get('v.SAPJSON');

            if (userSalesOrgsAvailableFilter != '') {
                var _salesOrgFilterAsList = [];
                userSalesOrgsAvailableFilter.replace(/ /g,'').split(',').forEach(x=>{_salesOrgFilterAsList.push({label:x,value:false});});
                component.set('v._salesOrgFilterAsList', _salesOrgFilterAsList);
            } else {
                var _salesOrgFilterAsList = [];

                (sapJson.salesArrangements || []).forEach(salesArrangement=>{
                    if(_salesOrgFilterAsList.every(existing=>existing.label != salesArrangement.salesOrg))
                        _salesOrgFilterAsList.push({label:salesArrangement.salesOrg, value:false});
                });

                component.set('v._salesOrgFilterAsList', _salesOrgFilterAsList);
            }


            if (userSalesChannelAvailableFilter != '') {
                var _salesChannelFilterAsList = [];
                userSalesChannelAvailableFilter.replace(/ /g,'').split(',').forEach(x=>{_salesChannelFilterAsList.push({label:x,value:false});});
                component.set('v._salesChannelFilterAsList', _salesChannelFilterAsList);
            } else {
                var _salesChannelFilterAsList = [];
                // Add to the list if is not already there, matching by distribution channel
                (sapJson.salesArrangements || []).forEach(salesArrangement=>{
                    if(_salesChannelFilterAsList.every(existing=>existing.label != salesArrangement.distributionChannel))
                        _salesChannelFilterAsList.push({label:salesArrangement.distributionChannel, value:false});
                });

                component.set('v._salesChannelFilterAsList', _salesChannelFilterAsList);
            }


            if (userPartnerFunctionAvailableFilter != '') {
                var _partnerFunctionFilterAsList = [];
                userPartnerFunctionAvailableFilter.replace(/ /g,'').split(',').forEach(x=>{_partnerFunctionFilterAsList.push({label:x, value:false});});
                component.set('v._partnerFunctionFilterAsList', _partnerFunctionFilterAsList);
            } else {
                var _partnerFunctionFilterAsList = [];

                (sapJson.salesArrangements || []).forEach(salesArrangement=> {
                    (salesArrangement.parties || []).forEach(party=> {
                        console.log(JSON.stringify(_partnerFunctionFilterAsList), party.roleCode);
                        if(_partnerFunctionFilterAsList.every(existing=>existing.label != party.roleCode)){
                            _partnerFunctionFilterAsList.push({label:party.roleCode, value:false});
                        }
                    });
                });

                component.set('v._partnerFunctionFilterAsList', _partnerFunctionFilterAsList);
            }
        },
        extractSalesArrangements : function(component, helper) {

            var _salesOrgFilterAsList = component.get('v._salesOrgFilterAsList');
            var _salesChannelFilterAsList = component.get('v._salesChannelFilterAsList');
            var _partnerFunctionFilterAsList = component.get('v._partnerFunctionFilterAsList');




            var sapJson = component.get('v.SAPJSON');

            var tableData = [];

            for (var salesArrangement of (sapJson.salesArrangements || [])){
                // only if
                if ((_salesOrgFilterAsList.every(x=>x.value==false) || _salesOrgFilterAsList.findIndex(x=>x.label==salesArrangement.salesOrg&&x.value)> -1) &&
                    (_salesChannelFilterAsList.every(x=>x.value==false) || _salesChannelFilterAsList.findIndex(x=>x.label==salesArrangement.distributionChannel && x.value) > -1)) {
                    for (var party of salesArrangement.parties) {
                        if (_partnerFunctionFilterAsList.every(x=>x.value==false) || _partnerFunctionFilterAsList.findIndex(x=>x.label==party.roleCode && x.value) > -1) {

                            tableData.push({
                                salesOrg : salesArrangement.salesOrg,
                                distributionChannel : salesArrangement.distributionChannel,
                                roleCode : party.roleCode,
                                roleName : party.roleName,
                                customerNumber : party.customerNumber,
                                customerName : 'To be Implemented'
                            });
                        }
                    }
                }
            }

            component.set('v._salesPartiesData', tableData);
        },

        _setFilter : function(filters, filterType, filterName, systemValue, userValue, mode) {

            if (filters == undefined) filters = {};
            if (filters[filterType] == undefined) filters[filterType] = {};


            if (filters[filterType][filterName] == undefined){
                if (mode === 'upsert') filters[filterType][filterName] = {system:systemValue || true, user:userValue || true};
            } else {
                if (systemValue !== undefined) filters[filterType][filterName].system = systemValue;
                if (userValue !== undefined) filters[filterType][filterName].user = userValue;
            }
        }
    }
)