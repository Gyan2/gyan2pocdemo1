(
    {
        handleInit : function(component, event, helper) {
            component.set('v.showFiltersPanel', document.location.href.indexOf('/flexipageEditor/')!=-1); // makes the filters section to be open when editing the layout
            helper.extractJSON(component, helper);
            //helper.initSystemFilters(component, helper);
            helper.initUserFilters(component, helper);
            helper.extractSalesArrangements(component, helper);
        },

        handleFiltersButtonClick : function(component, event, helper) {
            component.set('v.showFiltersPanel',!component.get('v.showFiltersPanel'));
        },

        handleFilterChange : function(component, event, helper) {
            helper.extractSalesArrangements(component, helper);
        }


    }
)