/**
 * Created by trakers on 04.07.18.
 */
({
    onEnter:function(component,event,helper){
         event.currentTarget.classList.add('hover');
    },
    onExit:function(component,event,helper){
         event.currentTarget.classList.remove('hover');
    },
    changeMode:function(component,event,helper){
         component.set('v.mode','write');
    },
    onQuoteChange:function(component,event,helper){

        var quote = component.get('v.quote');

        var totalGrossPrice = 0;
        var totalInclTax = 0;

        var totalAfterDiscount = 0;
        var totalDiscount = 0;

        console.log('quote.QuoteLineItems');
        console.log(quote.QuoteLineItems);

        quote.QuoteLineItems.forEach(item => {
            console.log(item.AdditionalDiscount__c);
            console.log(item.Discount);
            console.log(item.Quantity);
            console.log(item.SAPPrice__c);
            console.log(item.UnitPrice);

            totalGrossPrice += item.SAPPrice__c * item.Quantity;
            totalAfterDiscount += item.SAPPrice__c * item.Quantity * (100 - item.Discount) /100 *  (100 - item.AdditionalDiscount__c)/100;

        });


        console.log('TotalGrossPrice :'+totalGrossPrice);
        console.log('totalAfterDiscount :'+totalAfterDiscount);

        component.set('v.totalGrossPrice',totalGrossPrice);
        component.set('v.totalAfterDiscount',totalAfterDiscount);
        component.set('v.totalDiscount',totalGrossPrice - totalAfterDiscount);
        component.set('v.totalInclTax',totalInclTax);

    }
})