/**
 * Created by trakers on 10.07.18.
 */
({
    onInit:function(component,event,helper){
          component.set('v.project',{'sobjectType':'Project__c'});

    },
    onClickProjectsTab:function(component,event,helper){
        component.set('v.currentTab','projects');

        // TODO: remove once it's working
        helper.checkForProjectsAround(component)
        .then(projects => {
            console.log(projects);
            component.set('v.projectList',projects);
        })
        .catch(err => {
            console.log('error');
            console.log(err);
        })
    },
    onClickDetailsTab:function(component,event,helper){
        component.set('v.currentTab','details');
    },
    onClickChangeProjectsMode:function(component,event,helper){
        var mode = event.currentTarget.getAttribute('data-mode');
        console.log(mode);
        component.set('v.projects_mode',mode);
    },
    onClickSearchAddress:function(component,event,helper){
        component.set('v.address_display',true);
    },
    onCloseSearchAddress:function(component,event,helper){

    },
    onClickClose:function(component,event,helper){
       component.set('v.display',false);
    },
    onClickCloseAddress:function(component,event,helper){
        component.set('v.address_display',false);
    },
    onChangeDisplayAddress:function(component,event,helper){
        component.set('v.hideMain',component.get('v.address_display'));
    },
    onAddressChange:function(component,event,helper){
        var selected = component.get('v.address_selected');
        console.log(selected);
        component.set('v.project.City__c',selected.locality);
        component.set('v.project.Country__c',selected.country);
        component.set('v.project.CountryIsoCode__c',selected.country_iso);


        if(selected.street_number == undefined){
            component.set('v.project.Street__c',selected.route);
        }else{
            component.set('v.project.Street__c',selected.street_number+' '+selected.route);
        }

        component.set('v.project.Geolocation__Latitude__s',selected.location.lat);
        component.set('v.project.Geolocation__Longitude__s',selected.location.lng);
        component.set('v.project.Zip__c',selected.postal_code);

        helper.checkForProjectsAround(component)
        .then(projects => {
            component.set('v.projectList',projects);
        })
        .catch(err => {
            console.log('error');
            console.log(err);
        })
    },
    onChangeName:function(component,event,helper){
        var searchName = event.getSource().get('v.value');

        setTimeout($A.getCallback(function(){
            if(component.get('v.project.Name') == searchName){
                helper.checkForProjectsAround(component)
                .then($A.getCallback(function(projects){
                    component.set('v.projectList',projects);
                }))
            }
        }), 300);
    },
    onChangeBuilding:function(component,event,helper){
      component.get('v.project').BuildingRef__c = component.get('v.building_selected');
      component.set('v.project',component.get('v.project'));
      if(component.get('v.building_selected') != null ){
          helper.getBuildingAddress(component)
          .then($A.getCallback(function(val){
                console.log(val);
          }))
      }



    },
    onEnter:function(component,event,helper){
         event.currentTarget.classList.add('hover');
    },
    onExit:function(component,event,helper){
         event.currentTarget.classList.remove('hover');
    },
    changeMode:function(component,event,helper){
     component.set('v.mode','write');
    }
})