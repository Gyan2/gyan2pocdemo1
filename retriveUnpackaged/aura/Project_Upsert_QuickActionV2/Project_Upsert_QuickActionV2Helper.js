/**
 * Created by trakers on 10.07.18.
 */
({
    checkForProjectsAround:function(component){
        console.log('checkForProjectsAround');
         var action = component.get('c.checkForProjectsAround');

         action.setParams({
             projectJson:JSON.stringify(component.get('v.project')),
             distance:component.get('v.radius')
         });

         var that = this;
         return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {

                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
         }))
    },
    getBuildingAddress:function(component){
         var action = component.get('c.getBuilding');
             action.setParams({
                 recordId:component.get('v.building_selected')
             })
             var that = this;
             return new Promise($A.getCallback(function(resolve, reject){
                 action.setCallback(that, function(response) {
                     var res = response.getReturnValue();
                      if(response.getState() === "SUCCESS") {
                         resolve(res);
                      }else{
                         reject(res);
                      }
                 });
                 $A.enqueueAction(action);
              }))
    }
})