/**
 * Created by trakers on 20.06.18.
 */
({
    saveSections:function(component){
        var action = component.get('c.auraSaveQuoteSection');

        action.setParams({
             'quoteId' : component.get('v.quoteId'),
             'section' : JSON.stringify(component.get('v.sections'))
        });


        var that = this;

        return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {
                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
        }))
    },
    deleteAllQuoteLineItems : function(component){
          var action = component.get('c.auraDeleteQuoteLineItems');
          var quoteLineItems = component.get('v.QuoteLineItems').filter(x=>x._toDelete);

          //console.log(quoteLineItems);

          action.setParams({
              'quoteLineItems' : quoteLineItems
          });

          var that = this;

          return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {
                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
          }))
    },
    sortableSection:function(component){
        console.log('Load sortable');
        $('#list').sortable({ //.section-container
             items:".sectionItem",
             //handle:'.section',
             connectWith: ".editLayout",
             update: function(event,ui) {
                 console.log('UPDATE SECTION');
                 //$('.item-container').show();
             },
             placeholder:'ui-state-highlight',
             start:function(event,ui){
                 console.log('Start');
                 $('.ui-state-highlight').height('47px');
                 component.set('v.isSorted',true);
             }
        }).disableSelection();
    },
    sortableItems:function(component){
        console.log('Load sortable');
        $('.item-container').sortable({ //.section-container
             //handle:'.section',
             helper: "clone",
             connectWith: ".item-container",
             update: function(event,ui) {
                 console.log('UPDATE SECTION');
             },
             placeholder:'ui-state-highlight',
             start:function(event,ui){
                 console.log('Start');
                 $('.ui-state-highlight').height(ui.item.height());
                 component.set('v.isSorted',true);
             }
        }).disableSelection();
    },
    generateTree:function(component){
        var sections = component.get('v.sections');
        var quoteLineItems = component.get('v.QuoteLineItems');

        var tree = []; // Include default
        sections.forEach(val => {
            tree.push({'name':val.name,'color':val.color,'discount':val.discount,'additionalDiscount':val.additionalDiscount,'id':val.id,'items':[]});
        })

        quoteLineItems.forEach(qli => {
            var section = tree.find(val => val.id == qli.Section__c);
            //console.log(section);
            if(section){
                section.items.push({'name':qli.Product2.Name,'id':qli.Id});
            }
        })

        return tree;
    },
    saveNewPositions:function(component){
         var sections = [];

         $('.section').each(function(index){
             var sectionId = $(this).data('id');

             sections.push(JSON.parse(JSON.stringify(component.get('v.sections').find(val => val.id == sectionId))));



             $(this).find('.item').each(function(index){
                 var itemId = $(this).data('id');
                 var position = index + 1;
                 //console.log(' ItemId: '+itemId);
                 var qli = component.get('v.QuoteLineItems').find(val => val.Id == itemId);
                     qli._toDelete = false;
                     //POSITION
                     if(qli.Position__c != position){
                         //console.log(qli.Position__c+' != '+position)
                         qli.Position__c = position;
                         qli._changed = true;
                     }
                     //SECTION
                     if(qli.Section__c != sectionId){
                          //console.log(qli.Section__c+' != '+sectionId)
                          qli.Section__c = sectionId;
                          qli._changed = true;
                     }
             })
         });
         console.log('saveNewPositions');
         console.log(JSON.parse(JSON.stringify(sections)));
         component.set('v.sections',sections);
         component.get('v.QuoteLineItems').sort(function(a,b){
             return a.Position__c - b.Position__c;
         })

         //component.set('v.quote',component.get('v.quote'));
    },
    checkDiscountChanges:function(component){
        var sections = component.get('v.sections')
        var copy = component.get('v.copy');

        sections.forEach(section => {
            var sectionCopy = copy.find(val => val.id == section.id);
            if(sectionCopy == undefined || section.discount != sectionCopy.discount){

                component.get('v.QuoteLineItems').filter(val => val.Section__c == section.id)
                .forEach(quoteLineItem => {
                    //set the new values
                    quoteLineItem.Discount = section.discount;
                    quoteLineItem.AdditionalDiscount__c = section.additionalDiscount;
                    quoteLineItem._priceAfterDiscount = quoteLineItem._priceAfterDiscount || 0;
                    // handle discount changes
                    this.handleDiscountCalculation(quoteLineItem);
                })

            }
        })

        //component.set('v.quote',component.get('v.quote'));
        component.set('v.sections',component.get('v.sections'));

    }
})