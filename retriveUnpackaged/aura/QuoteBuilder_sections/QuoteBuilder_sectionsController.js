/**
 * Created by trakers on 20.06.18.
 */
({
    onInit:function(component,event,helper){
         var copy = JSON.parse(JSON.stringify(component.get('v.sections')));

         if(component.get('v.section') == null){
             component.set('v.section',copy.length > 0 ? copy[0]:null);
         }

         component.set('v.tree',helper.generateTree(component));
         component.set('v.copy',copy);

        window.setTimeout(function(){
           helper.sortableSection(component);
           helper.sortableItems(component);
        },700)
    },
    editCancel:function(component,event,helper){

        component.set('v.sections',component.get('v.copy'));
        component.set('v.display',false);
    },
    editSave:function(component,event,helper){

        component.set('v.loading',true);
        //console.log('saveNewPositions');
        helper.saveNewPositions(component);
        helper.checkDiscountChanges(component);

        Promise.all([helper.saveSections(component),helper.deleteAllQuoteLineItems(component)])
        .then($A.getCallback(values => {
            console.log(values);

            if(values[0].status == 200 && values[1].status == 200){
                //component.set('v.quote',component.get('v.quote'));
            }else{
                component.set('v.global_errorMessage',values[0].message+' '+values[1].message);
            }
            component.set('v.loading',false);
            component.set('v.display',false);
        }))
    },
    select:function(component,event,helper){
      console.log(event.currentTarget.getAttribute('data-id'));
      var id = event.currentTarget.getAttribute('data-id');
      component.set('v.section',component.get('v.sections').find(val => val.id == id));
    },
    onChangeDiscount:function(component,event,helper){
                var section = component.get('v.section');
                var value = Number(component.get('v.section.discount'));
                console.log('Discount = '+value+'%');
                // SAVE tree
                component.get('v.tree').filter(val => val.id == section.id)
                .forEach(val => {
                    val['discount'] = value;
                })
                // SAVE sections
                component.get('v.sections').filter(val => val.id == section.id)
                .forEach(val => {
                    val.discount = value;
                })

                console.log(JSON.parse(JSON.stringify(component.get('v.tree'))));

                component.set('v.tree',component.get('v.tree'));
                component.set('v.sections',component.get('v.sections'));
    },
    onChangeAdditionalDiscount:function(component,event,helper){
                var section = component.get('v.section');
                var value = Number(component.get('v.section.additionalDiscount'));
                console.log('additionalDiscount = '+value+'%');
                // SAVE tree
                component.get('v.tree').filter(val => val.id == section.id)
                .forEach(val => {
                    val['additionalDiscount'] = value;
                })
                // SAVE sections
                component.get('v.sections').filter(val => val.id == section.id)
                .forEach(val => {
                    val.additionalDiscount = value;
                })

                component.set('v.tree',component.get('v.tree'));
                component.set('v.sections',component.get('v.sections'));
    },
    onChangeSection:function(component,event,helper){
        var value = event.getSource().get('v.value');
        var name  = event.getSource().get('v.name');

        var section = component.get('v.section');
            section[name] = value;

            console.log(JSON.parse(JSON.stringify(section)));

        component.set('v.section',section);
        // SAVE tree
        component.get('v.tree').filter(val => val.id == section.id)
        .forEach(val => {
            val[name] = value;
        })

        component.set('v.tree',component.get('v.tree'));

        // SAVE sections
        component.get('v.sections').filter(val => val.id == section.id)
        .forEach(val => {
            val[name] = value;
        })
        component.set('v.sections',component.get('v.sections'));

    },
    editAdd:function(component,event,helper){
        var sections    = component.get('v.sections') || [];
        var tree     = component.get('v.tree') || [];


        var max = 1;
        sections.forEach(val => {
            if(max <= val.id){
                max = val.id + 1;
            }
        })
        var newSection  = {name:'New Section',color:'#71777b',discount:0,id:max,additionalDiscount:0};

        sections.splice(1, 0, newSection);

        console.log(JSON.parse(JSON.stringify(sections)));

        component.set('v.sections',sections);
        component.set('v.tree',helper.generateTree(component));
        component.set('v.section',newSection);
        window.setTimeout(function(){
           helper.sortableSection(component);
           helper.sortableItems(component);
        },700)
    },
    editDelete:function(component,event,helper){
        var id = event.getSource().get('v.value');

        // Sections
        component.set('v.sections',component.get('v.sections').filter(val => val.id != id));
        // QuoteLineItems
        component.get('v.QuoteLineItems')
        .filter(val => val.Section__c == id).forEach(val => {
            val._toDelete = true;
            val.Section__c = 0;
        });


        component.set('v.QuoteLineItems',component.get('v.QuoteLineItems'));

        component.set('v.tree',component.get('v.tree').filter(val => val.id != id));

    },
})