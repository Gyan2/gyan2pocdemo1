/**
 * Created by trakers on 03/04/2018.
 */
({
    checkForProjectsAround:function(component){
        console.log('checkForProjectsAround');
         var action = component.get('c.checkForProjectsAround');

         action.setParams({
             projectJson:JSON.stringify(component.get('v.project')),
             distance:component.get('v.radius')
         });

         var that = this;
         return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                  console.log(res);
                   if(response.getState() === "SUCCESS") {

                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
         }))
    },
    insertProject:function(component){
        console.log('Insert Project');
        //console.log(JSON.stringify(component.get('v.project')));
           var action = component.get('c.insertProject');
            action.setParams({
                project:component.get('v.project')
            })
            var that = this;
            return new Promise($A.getCallback(function(resolve, reject){
                action.setCallback(that, function(response) {
                    var res = response.getReturnValue();
                     if(response.getState() === "SUCCESS") {

                        resolve(res);
                     }else{
                        reject(res);
                     }
                });
                $A.enqueueAction(action);
             }))
      },
    updateProject:function(component){
           console.log('Update Project');
           var action = component.get('c.updateProject');
            action.setParams({
                project:component.get('v.project')
            })
            var that = this;
            return new Promise($A.getCallback(function(resolve, reject){
                action.setCallback(that, function(response) {
                    var res = response.getReturnValue();
                     if(response.getState() === "SUCCESS") {

                        resolve(res);
                     }else{
                        reject(res);
                     }
                });
                $A.enqueueAction(action);
             }))
    },
    getBuildingAddress:function(component){
        var action = component.get('c.getBuilding');
            action.setParams({
                recordId:component.get('v.project.BuildingRef__c')
            })
            var that = this;
            return new Promise($A.getCallback(function(resolve, reject){
                action.setCallback(that, function(response) {
                    var res = response.getReturnValue();
                     if(response.getState() === "SUCCESS") {
                        resolve(res);
                     }else{
                        reject(res);
                     }
                });
                $A.enqueueAction(action);
             }))
    }
})