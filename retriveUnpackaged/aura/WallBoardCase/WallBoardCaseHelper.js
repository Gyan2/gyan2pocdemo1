({ 
    /*Temperory Checking Purpose*/ 
    loadAllRecordsHelper : function(component) {
        
        var action = component.get('c.LTNG_filterCaseList');
        action.setParams(
            { 
                "searchType" : component.get("v.typeFilterList"),
                "serviceArea" : component.get("v.serviceArea"),
                "ownerList" : component.get("v.ownerSelected")
            });
        action.setCallback(this, function(actionResult) {
            console.log("Apex Call Status: " + actionResult.getReturnValue().dmlException);
            var allCaseRecord = actionResult.getReturnValue().caseList;
            var cloneCases = [];
            for(var index = 0; index < allCaseRecord.length; index++) {
                var d = new Date(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c);
                if(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c != undefined){
                    var test = new Date(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c)
                    cloneCases.push(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c);
                    allCaseRecord[index].Next_SLA_Milestone_Due_Date__c = (d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes();
                }
            }
            component.set("v.cases", this.setMileStoneAlertHelper(component, allCaseRecord, cloneCases));
            component.set('v.cloneCases', cloneCases);
            component.set("v.loadStatus", true);
            this.pieChartDatastructure(component, component.get("v.cases"));
            
            
            
        });
        
        $A.enqueueAction(action);
    },
    
    
    setMileStoneAlertHelper : function(component, allCaseRecord, cloneCases){
        
        var timeGst = new Date().toGMTString();
        var currentDate = new Date(timeGst);
        for(var index = 0; index < allCaseRecord.length; index++) {
            allCaseRecord[index].notification = "0";
            if(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c != undefined){
                var test = new Date(cloneCases[index])
                var test1 = (test.getTime() - currentDate.getTime() );
                if(test1 < 3600000 && test1 > 0){
                    allCaseRecord[index].notification = "1";
                    
                }
                if(test1 <  0){
                    allCaseRecord[index].notification = "2";
                    
                }
            }
            
        }
        return allCaseRecord;
        
    },
    
    
    pieChartDatastructure : function(component, allCase) {
        //var allCase = component.get("v.caseList");
        //console.log("Check all reord: " + JSON.stringify(allCase));
        var totalCase = allCase.length;
        var emailCount = 0;
        var holdCaseCount = 0;
        var newCaseCount = 0;
        var assignedCaseCount = 0;
        for(var index = 0; index < allCase.length; index++) {
            if(allCase[index].No_Email__c == "@") {
                emailCount++;
            }
            if(allCase[index].Status == "New") {
                newCaseCount++;
            }
            if(allCase[index].Status == "On Hold") {
                holdCaseCount++;
            }
            if(allCase[index].Status == "Assigned") {
                assignedCaseCount++;
            }
            
        }
        var noEmailChart = {type: 'pie',data: {
            datasets: [{
                data: [
                    emailCount,
                    totalCase - emailCount
                ],
                backgroundColor: [
                    "#0a5100",
                    "#9eea93",
                ],
                    label: 'No Email Chart'
                    }],
                labels: [
                    'No Email',
                    'Email'
                ]
            },
                       options: {
                       responsive: true
                       }
                       };
                       component.set("v.noEmailChart", noEmailChart);
                       
                       console.log("chk status: " + JSON.stringify(noEmailChart))
 
                       
                       var statusChart = {type: 'pie',data: {
                       datasets: [{
                       data: [
                       newCaseCount,
                       holdCaseCount,
                       assignedCaseCount
                      ],
            backgroundColor: [
                "#4F209C",
                "#3D0053",
                "#2E79DD"
                
            ],
            label: 'No Email Chart'
        }],
                            labels: [
                            'New',
                            'On Hold',
                            'Assigned'
                            ]
                           },
            options: {
                responsive: true
            }
    };                                                   
    
    component.set("v.statusChart", statusChart ); 
    
    
    var reasonChart = {type: 'pie',data: {
    datasets: [{
    data: [
    13,
    31,
    57,
    71
    ],
    backgroundColor: [
    "#233049",
    "#DC3E41",
    "#E56725",
    "#FCCE38"
    ],
    label: 'No Email Chart'
}],
 labels: [
 'Reason1',
 'Reason2',
 'Reason3',
 'Reason4'
 ]
 },
 options: {
 responsive: true
 }
 };                                                   
 component.set("v.reasonChart", reasonChart );

console.log("++++++++++++"+JSON.stringify(component.get("v.noEmailChart").data.datasets[0].data));
$A.get('e.force:refreshView').fire();

    }
    
    
    /*showPopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');
    },
    hidePopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
        
    },*/
    
})