({
     doInit: function(component, event, helper) {
         
        /*Temperory Checking Purpose*/ 
        var action = component.get('c.ltng_getCaseList');
        action.setParams(
        { 
        	"searchType" : component.get("v.typeFilterList"),
            "serviceArea" : component.get("v.serviceArea"),
            "ownerSelected" : component.get("v.ownerSelected")
        });
        var self = this;
        action.setCallback(this, function(actionResult) {
            
            
                component.set("v.picklistOptions", actionResult.getReturnValue().caseType);
            
            
            
            //console.log("Check return value wrapper class: " + JSON.stringify(actionResult.getReturnValue()));
            var allCaseRecord = actionResult.getReturnValue().caseList;
            
            //alert(JSON.stringify(allCaseRecord));
            var cloneCases = [];
            for(var index = 0; index < allCaseRecord.length; index++) {
                var d = new Date(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c);
                if(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c != undefined){
                    var test = new Date(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c)
                    cloneCases.push(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c);
                    allCaseRecord[index].Next_SLA_Milestone_Due_Date__c = (d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes();
                }
            }
            
            component.set("v.ownerSource", actionResult.getReturnValue().ownerName);
            component.set("v.translationMap", actionResult.getReturnValue().translationMap);
            helper.pieChartDatastructure(component, allCaseRecord);
            component.set("v.cases", helper.setMileStoneAlertHelper(component, allCaseRecord, cloneCases));
            component.set('v.cloneCases', cloneCases);
            component.set("v.loadStatus", true);
            component.set("v.statusChk", true);
            
            
        });
        
        $A.enqueueAction(action);
         
         
         
         
         
         setInterval(function(){ 
            
            var allCaseRecord = component.get("v.cases");
            var cloneCases = component.get("v.cloneCases");
            component.set("v.cases", helper.setMileStoneAlertHelper(component, allCaseRecord, cloneCases));
            
            
            
        }, 60000);
         
         
     },
    
     ScriptLoaded : function(component, event, helper) {
        
        var action = component.get("c.LTNG_getUserSession");
        action.setCallback(this, function(response)
         {
             var state = response.getState();
             if(state === "SUCCESS") 
             {
                 $.cometd.init(
                     {
                         url: '/cometd/43.0',
                         requestHeaders: { Authorization: response.getReturnValue()},
                         appendMessageTypeToURL : false
                     });
                 $.cometd.subscribe('/topic/CaseWallBoard16', function(message) 
                                    {
                                        
                                        console.log("QQQQQQQQQQQ"+JSON.stringify(message.data.event.type));
                                        if(message.data.event.type == "updated")
                                        {
                                            
                                            var updatedRecord = message.data.sobject;
                                            var d = new Date(updatedRecord.Next_SLA_Milestone_Due_Date__c);
                                            console.log( "Formated time: "+(d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes());
                                            updatedRecord.Next_SLA_Milestone_Due_Date__c = (d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes();
                                            var caseList = component.get("v.cases");
                                            for(var index = 0; index < caseList.length; index++)
                                            {
                                                console.log("!!!"+index+JSON.stringify(caseList));
                                                if(caseList[index].Id == updatedRecord.Id && updatedRecord.Status != "Closed" && updatedRecord.Show_On_Wallboard__c  == true)
                                                {
                                                    //console.log("!!!"+updatedRecord.Status);
                                                    caseList[index] = updatedRecord;
                                                }
                                                else if(caseList[index].Id == updatedRecord.Id && (updatedRecord.Status == "Closed" || updatedRecord.Show_On_Wallboard__c  == true)) {
                                                    //alert(updatedRecord.Status);
                                                    caseList.splice(index, 1);
                                                }
                                            }
                                            //alert();
                                            console.log("!!!111"+caseList);
                                            component.set("v.cases", caseList);
                                            var allCaseRecord = component.get("v.cases");
            								var cloneCases = component.get("v.cloneCases");
                							component.set("v.cases", helper.setMileStoneAlertHelper(component, allCaseRecord, cloneCases));
                                            helper.pieChartDatastructure(component, allCaseRecord);
                                            
                                            
                                        }
                                        if(message.data.event.type == "created")
                                        {
                                            
                                            
                                            var newRec = message.data.sobject;
                                            var cloneCases = component.get("v.cloneCases");
                                            cloneCases.unshift(newRec.Next_SLA_Milestone_Due_Date__c);
                                            var d = new Date(newRec.Next_SLA_Milestone_Due_Date__c);
                                            newRec.Next_SLA_Milestone_Due_Date__c = (d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes();
                                            
                                            
                                            console.log("inside if");
                                            var caseList = component.get("v.cases");
                                            console.log("Check caseList:" + caseList);
                                            if(caseList.length == 25) {
                                                caseList.pop();
                                            }
                                            caseList.unshift(newRec);
                                            console.log("Check upsert:" + caseList);
                                            //component.set("v.cases", caseList);
                                            
                                            var allCaseRecord = caseList;
            								var cloneCases = component.get("v.cloneCases");
                							component.set("v.cases", helper.setMileStoneAlertHelper(component, allCaseRecord, cloneCases));
                                            component.set("v.cloneCases", cloneCases);
                                            helper.pieChartDatastructure(component, allCaseRecord);
                                            
                                            
                                        }
                                        
                                    });
             }
         });
         $A.enqueueAction(action);
     },
    
    filterEventHandler : function(component, event, helper) {
        
        helper.loadAllRecordsHelper(component);
        
    }
    
    
     /*onInputClick : function(component,event,helper){
        
        helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-');
        helper.showPopupHelper(component,'backdrop','slds-backdrop--');
        
    },
    
    CloseButton :function(component, event, helper){
        helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
        helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
    },
    
    getFilterVlaue : function(component, event, helper) {
        //alert("Hi");
    	
        //component.set("v.filterCount", filterCount.substring(0,filterCount.length));
    
    
    },
     ApplyFilterandHidePopUp : function(component, event, helper) {
         var picklistOptions = component.get("v.picklistOptions");
         var typeFilterList = [];
         var filterCount = "";
         for(var index in picklistOptions) {
             if(picklistOptions[index].checked) {
                 typeFilterList.push(picklistOptions[index].value);
                 filterCount = filterCount + picklistOptions[index].value + ",";
                 
             }
         }
         component.set("v.filterCount", filterCount.substring(0, filterCount.length - 1 ));
         component.set("v.typeFilterList", typeFilterList);
         helper.loadAllRecordsHelper(component);
         helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
         helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
         
    },
    
    RemoveFilter :  function(component, event, helper){
      
        var picklistOptions = component.get("v.picklistOptions");
        for(var index = 0; index < picklistOptions.length; index++) {
            picklistOptions[index].checked = false;
        }    
        component.set("v.picklistOptions", picklistOptions);
        
        helper.loadAllRecordsHelper(component);
        
    },*/
    
})