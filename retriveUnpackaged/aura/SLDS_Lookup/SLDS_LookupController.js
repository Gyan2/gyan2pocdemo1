({
    // Overwritten the method of the SLDS_Abstract
    doValidate:function(component,event,helper){
        console.log('doValidate');
        var isRequired = component.get('v.isRequired');
        if(isRequired && component.get('v.value') == null){
            component.set('v.displayError',true);
            component.set('v.errorMessage','This field is required');
        }
    },
    //Function to handle the LookupChooseEvent. Sets the chosen record Id and Name
    handleLookupChooseEvent : function (component,event,helper) {
        component.set("v.selectedId", event.getParam("recordId"));
        component.set("v.selectedObject",event.getParam("record"));

        //component.set('v.matchingRecords',null);
        component.set('v.inputValue',null);
    },

    //Function for finding the records as for given search input
    searchRecords : function (component,event,helper) {
        event.stopPropagation();
        component.set('v.display',true);
        var searchText = event.currentTarget.value;
        component.set('v.inputValue',searchText);

        setTimeout($A.getCallback(function(){
            if(component.get('v.inputValue') == searchText){
                if(searchText){
                    helper.searchSOSLHelper(component,searchText);
                }else{
                    helper.searchSOQLHelper(component);
                }
            }
        }), 300);



    },
    handleSelectChanged:function(component,event,helper){
        var selectedId = component.get('v.selectedId');
        var selectedObject = component.get('v.selectedId');

        if(selectedObject == null){
            // load selectedObject
            console.log('Load Selected Object');
            helper.searchSOQLHelper(component);
        }else{
            // Already loaded, we just display it
        }


    },
    onFocus:function(component,event,helper){
         component.set('v.display',true);

         if(component.get('v.matchingRecords').length == 0){
           helper.searchSOQLHelper(component);
         }
    },
    //function to hide the list on onblur event.
    onBlur :function (component,event,helper) {


            setTimeout($A.getCallback(function(){
                 console.log('onBlur');
                 //component.set('v.matchingRecords',null);
                 component.set('v.display',false);
            }), 150)
        //component.set('v.matchingRecords',null);
    },
    unselect:function(component,event,helper){
        component.set('v.selectedId',null);
        component.set('v.selectedObject',null);
    },
    onInit:function(component,event,helper){
        var selectedId = component.get('v.selectedId');
        if(selectedId != null){
            helper.searchSpecific(component,selectedId);
        }
    },
    onEnter:function(component,event,helper){
         event.currentTarget.classList.add('hover');
    },
    onExit:function(component,event,helper){
         event.currentTarget.classList.remove('hover');
    },
    changeMode:function(component,event,helper){
         component.set('v.mode','write');
    }
})