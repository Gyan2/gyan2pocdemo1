/**
 * Created by Jai Chaturvedi.
 */
({
    //Function to toggle the record list drop-down
    toggleLookupList : function (component, ariaexpanded, classadd, classremove) {
        component.find("lookupdiv").set("v.aria-expanded", true);
        $A.util.addClass(component.find("lookupdiv"), classadd);
        $A.util.removeClass(component.find("lookupdiv"), classremove);
    },

    //function to call SOSL apex method.
    searchSOSLHelper : function (component,searchText) {
        //validate the input length. Must be greater then 3.
        //This check also manages the SOSL exception. Search text must be greater then 2.
        if(searchText && searchText.length > 3){
            //show the loading icon for search input field
            component.set('v.loading',true);
            //server side callout. returns the list of record in JSON string
            var action = component.get("c.search");
            action.setParams({
                "objectAPIName": component.get("v.objectAPIName"),
                "searchText": searchText,
                "whereClause" : component.get("v.filter"),
                "extrafields":component.get("v.extraFields"),
                "queryLimit":component.get("v.limit")
            });

            action.setCallback(this, function(a){
                var state = a.getState();

                if(component.isValid() && state === "SUCCESS") {
                    //parsing JSON return to Object[]
                    var result = [].concat.apply([], JSON.parse(a.getReturnValue()));
                    this.handleResults(searchText,component.get('v.extraFields'),result);
                    component.set("v.matchingRecords", result);
                    console.log( component.get("v.matchingRecords"));

                    //Visible the list if record list has values
                    if(a.getReturnValue() && a.getReturnValue().length > 0){

                        component.set('v.loading',false);
                    }else{

                    }
                }else if(state === "ERROR") {
                    console.log('error in searchRecords');
                }
            });
            $A.enqueueAction(action);
        }else{
            component.set('v.matchingRecords',[]);
        }
    },
    searchSpecific:function(component,selectedId){
           component.set('v.loading',true);

           var action = component.get("c.getSpecific");
           action.setParams({
               "objectAPIName": component.get("v.objectAPIName"),
               "selectedId" : selectedId,
               "extrafields":component.get("v.extraFields"),
               "queryLimit":component.get("v.limit")
           });

           action.setCallback(this, function(response) {
               var state = response.getState();
               if(component.isValid() && state === "SUCCESS") {
                   if(response.getReturnValue()){
                       var result = response.getReturnValue();
                       this.handleResults('',component.get('v.extraFields'),result);
                       console.log(result);
                       if(result.length > 0){
                           component.set('v.selectedObject',result[0]);
                       }
                       component.set('v.loading',false);
                   }
               } else {
                   console.log('Error in loadRecentlyViewed: ' + state);
               }
           });
           $A.enqueueAction(action);

    },
    //function to call SOQL apex method.
    searchSOQLHelper : function (component) {
        component.set('v.loading',true);

        var action = component.get("c.getRecentlyViewed");
        action.setParams({
            "objectAPIName": component.get("v.objectAPIName"),
            "whereClause" : component.get("v.filter"),
            "extrafields":component.get("v.extraFields"),
            "queryLimit":component.get("v.limit")
        });


        // Configure response handler
        action.setCallback(this, function(response) {
            var state = response.getState();
            if(component.isValid() && state === "SUCCESS") {
                if(response.getReturnValue()){
                    var result = response.getReturnValue();
                    this.handleResults('',component.get('v.extraFields'),result);
                    component.set("v.matchingRecords",result);
                    console.log( component.get("v.matchingRecords"));
                    if(response.getReturnValue().length>0){

                    }
                    component.set('v.loading',false);
                }
            } else {
                console.log('Error in loadRecentlyViewed: ' + state);
            }
        });
        $A.enqueueAction(action);
    },
    handleResults : function(searchText,extraFields,result){

        var keys = searchText.split(' ');

        keys = keys.filter(val => val.length > 1).sort((a,b) => {
            return b.length - a.length;
        })

        result.forEach(item => {
            item._Name = item.Name.replace(new RegExp("("+keys.join('|')+")","gi"),'<strong>$1</strong>');
            item._Extra = this.handleExtraFields(extraFields,item);
        })

        console.log(result);

    },
    handleExtraFields : function(extraFields,record){
        var result = [];
        extraFields.forEach(field => {
            var value;
            if(field.includes('.')){
                value = record;
                field.split('.').forEach(subField => {
                    if(value != undefined){
                        value = value[subField];
                    }
                })
            }else{
                value = record[field];
            }

            if(value!=undefined)
                result.push(value);
        })

        return result.join(' • ');


    }
})