({
    doInit : function(component, event, helper) {
        var arr = [];
        var obj = component.get("v.hierarchyObject");
        var columns = component.get("v.columns");
        
        component.set("v.headerValue", obj[component.get("v.columnHeader")]);
        columns.forEach(function(val){
            if(obj.hasOwnProperty(val)){
                           arr.push(obj[val]);
            } else{
                arr.push("");
            }
        });
        component.set("v.attributes",arr);
        var currentRecordId = component.get("v.currentRecordId");
        if(obj.Id == currentRecordId.slice(0,-3)){
            component.set("v.isCurrent",true);
        }
    },
    
    fireExpandEvent : function(component,event){
        var itemId = component.get("v.hierarchyObject").Id;
        component.get("v.hierarchyObject").isExpanded = !component.get("v.hierarchyObject").isExpanded;
        var componentEvent = component.getEvent("itemClickedEvent");
        componentEvent.setParams({
            "clickedItemId":itemId
        });
        componentEvent.fire();
        
    },
    
    navigateToObject : function(component,event,helper){
        var navigationEvent = $A.get("e.force:navigateToSObject");
        navigationEvent.setParams({
            "recordId": component.get("v.hierarchyObject").Id 
        });
        navigationEvent.fire();
    },
})