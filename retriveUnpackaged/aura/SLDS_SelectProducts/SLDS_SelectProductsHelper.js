/**
 * Created by trakers on 30.05.18.
 */
({
    load:function(component,event){
        var that = this;
        var locale = $A.get('$Locale.language').toUpperCase();

        component.set('v.selectedLanguage',locale);
        component.set('v.selectedField','Name');

        /* In the futur, add the possibility to search without SalesOrg or to specify the SalesOrg */
        Promise.all([that.initLanguages(locale,component),that.getProducts(component)])
        .then($A.getCallback(result => {
           var languages    = result[0];
           var products     = result[1];

           console.log(products.getReturnValue());
           console.log(component.get('v.searchValue'));
           console.log(products.getParam('search'));


           component.set('v.languages',languages);


           if(component.get('v.searchValue') == products.getParam('search')){
               var selected = component.get('v.selected');
               component.set('v.data',that.processData(products.getReturnValue(),selected,component));

           }
           component.set('v.loading',false);

        }))

    },
    getSalesOrg:function(component){
        var action = component.get('c.getSalesOrg');
            action.setParams({
                quoteId:component.get('v.recordId')
            });


        var that = this;
        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
        })
    },
    initLanguages:function(selected,component){
        var action = component.get('c.getLanguages');
            action.setParams({
                selected:selected
            });


        var that = this;
        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    },
    getProducts:function(component){
        var action = component.get('c.getProducts');
        var fields = component.get('v.fields').map(val => val.ApiName);
            //fields = fields.concat(component.get('v.fieldsToQuery').filter(item => fields.indexOf(item) < 0));

            action.setParams({
                search:component.get('v.searchValue') || '',
                fields:JSON.stringify(fields),
                salesOrgId:component.get('v.salesOrg'),
                QuoteId:component.get('v.recordId'),
                language:component.get('v.selectedLanguage'),
                searchField:component.get('v.selectedField'),
                exclude:component.get('v.exclude')
            });
            console.log({
                        search:component.get('v.searchValue') || '',
                        fields:fields,
                        salesOrgId:component.get('v.salesOrg'),
                        QuoteId:component.get('v.recordId'),
                        language:component.get('v.selectedLanguage'),
                        searchField:component.get('v.selectedField'),
                        exclude:component.get('v.exclude') || []
                    })

        var that = this;
        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {

                 if(response.getState() === "SUCCESS") {
                    resolve(response);
                 }else{
                    reject(response);
                 }
            });
            $A.enqueueAction(action);
         })
    },
    processData:function(data,selected,component){
        var ids = [];

        selected.forEach(val => {
            ids.push(val.Value);
        });

        // data
        data.forEach(item => {
             item.selected = ids.includes(item.data.find(val => val.ApiName == 'ProductRef__c').Value);
        })

        return data;


    },
    generateQuoteLineItems:function(selected,component){
        var action = component.get('c.generateQuoteLineItems');
            action.setParams({
                selected:selected,
                salesOrgId:component.get('v.salesOrg'),
                quoteId:component.get('v.quoteId')
            });


        var that = this;
        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                console.log('Get list of quoteLineItem');
                console.log(res);
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    },
})