/**
 * Created by trakers on 30.05.18.
 */
({
    onInit:function(component,event,helper){
        console.log('Init SLDS_SelectProducts');
        var mandatoryFields = [{'Label':'Name','ApiName':'Name','Query':true},{'Label':'Id','ApiName':'ProductRef__c','Query':false}];
        var fields = component.get('v.fields').filter(val => !['Name','ProductRef__c'].includes(val.ApiName));
            fields = mandatoryFields.concat(fields);


          component.set('v.fields',fields);
          component.set('v.queryFields',fields.filter(val => val.Query))
          console.log(fields.filter(val => val.Query));
          helper.load(component,event,helper);
    },
    onDisplayChange:function(component,event,helper){


    },
    onChangeField:function(component,event,helper){
        var selectedValue = event.currentTarget.value;
        console.log(selectedValue);
        component.set('v.selectedField',selectedValue);
    },
    onChangeSearch:function(component,event,helper){
        var searchValue = event.currentTarget.value;
        component.set('v.searchValue',searchValue);

        if(searchValue != '' && searchValue.length < component.get('v.minSearch'))
            return;

        component.set('v.loading',true);
        helper.getProducts(component,helper)
        .then($A.getCallback(data => {

            if(component.get('v.searchValue') == data.getParam('search')){
                var selected = component.get('v.selected');
                component.set('v.data',helper.processData(data.getReturnValue(),selected,component));
                component.set('v.loading',false);
            }

        }))
    },
    onChangeSelection:function(component,event,helper){
        var selected = component.get('v.selected');

        var target = event.currentTarget;
        var parent = target.parentNode.parentNode.parentNode;

        var checked = target.checked;
        var id = parent.getElementsByClassName('row-ProductRef__c')[0].value;
        var name = parent.getElementsByClassName('row-Name')[0].value;
        console.log({
            id:id,
            name:name
        })
        if(checked){
            selected = selected.filter( val => val.Value !== id);
            selected.push({'Value':id,'Label':name,'Selected':true});
        }else{
            selected = selected.filter( val => val.Value !== id);
        }

        component.get('v.data')
        .forEach(items => {
            var _id = items.data.find(val => val.ApiName == 'ProductRef__c');

            if(_id != undefined && _id.Value == id){
                items.selected = checked;
                console.log('selected');
            }
        })

        //console.log(selected);
        component.set('v.data',component.get('v.data'));
        component.set('v.selected',selected);
        console.log(selected);

    },
    onClickRemove:function(component,event,helper){
         var id = event.currentTarget.parentNode.parentNode.getElementsByClassName('selectedItemId')[0].value;

         var selected   = component.get('v.selected').filter( val => val.Value !== id);
         var data       = helper.processData(component.get('v.data'),selected,component);

         component.set('v.selected',selected);
         component.set('v.data',data);
    },
    onCloseClick:function(component,event,helper){
      component.set('v.display',false);

    },
    onCloseClickError:function(component,event,helper){
        component.set('v.displayError',false);
    },
    onSaveClick:function(component,event,helper){
        console.log('Type: '+component.get('v.type'));
        switch(component.get('v.type')){

            case 'simple':

                var result = component.get('v.selected').map(val => val.Value);

                component.set('v.result',{mode:component.get('v.type'),result:result});
                component.set('v.displayError',false);
                component.set('v.display',false);
            break;


            case 'advanced':
                component.set('v.displayError',false);

                var elements = [].concat(component.find('quantity') || []);

                var allValid = elements.reduce(function (isValid,comp) {
                    if(comp.isRendered()){
                        comp.showHelpMessageIfInvalid();
                    }
                    return isValid && comp.get('v.validity').valid;
                }, true);


                if(allValid){
                    component.set('v.result',{mode:component.get('v.type'),result:component.get('v.quoteLineItems')});
                    component.set('v.display',false);
                }


            break;


        }


                console.log(JSON.stringify(component.get('v.result.result')));
    },
    onNextClick:function(component,event,helper){
        component.set('v.loading',true);
        component.set('v.step',component.get('v.step')+1);

        var result = component.get('v.selected').map(val => val.Value);

        helper.generateQuoteLineItems(result,component)
        .then($A.getCallback(data=> {
            var quoteLinesItems = component.get('v.quoteLineItems');
            console.log(data);
            console.log(quoteLinesItems);
            data.forEach(item => {
                var element = quoteLinesItems.find(val => val.PricebookEntry.Product2Id == item.PricebookEntry.Product2Id);

                if(!element){
                    var description = component.get('v.selected').find(val => val.Value == item.PricebookEntry.Product2Id);
                    item.Description = description?description.Label:'';
                    quoteLinesItems.push(item);
                }
            })

            component.set('v.quoteLineItems',quoteLinesItems);
            component.set('v.loading',false);
        }));
    },
    onPreviousClick:function(component,event,helper){
        component.set('v.step',component.get('v.step')-1);
    },
    /** lightning:input type number -> string so we need this **/
    parseFloat:function(component,event,helper){
        var source = event.getSource();
        var val = parseFloat(source.get('v.value'));
        source.set('v.value',val?val:null);
        console.log('parseFloat');
        console.log(source.get('v.value'));
    },
    parseInt:function(component,event,helper){
        var source = event.getSource();
        var val = parseInt(source.get('v.value'));
            source.set('v.value',val?val:null);
            console.log('parseInt');
            console.log(source.get('v.value'));
    }
})