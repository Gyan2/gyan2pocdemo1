(
    {
        handleInit : function(component, event, helper) {
            helper.updateDisplayStructure(component);
        },

        handleEnabledClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'enabled', !button.get('v.selected'));
        },

        handleSearch : function(component, event, helper) {
            var displayStructure = component.get('v.profilesDisplayStructure');
            var searchString = component.get('v.searchString').toLowerCase();
            for (var forClass of displayStructure.classes) {
                forClass.showMe = forClass.apexClass.toLowerCase().indexOf(searchString)!=-1;
            }
            
            component.set('v.profilesDisplayStructure',displayStructure);
        }
    }
)