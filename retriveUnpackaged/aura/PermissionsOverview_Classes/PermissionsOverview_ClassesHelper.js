(
    {
        updateDisplayStructure : function(component) {
            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            displayStructure = {
                _profiles: {},
                profiles: [],
                _classes: {},
                classes: []
            };
            for (var profile of component.get('v.profilesInformationOrder')) {
                this._extractClasses(component, profilesInformation[profile], displayStructure);
            }

            component.set('v.profilesDisplayStructure', displayStructure);
        },

        _extractClasses : function(component, profileInfo, displayStructure) {

            var fullName = profileInfo.fullName;
            var profileIndex = displayStructure._profiles[fullName];

            if (profileIndex === undefined) {
                profileIndex = Object.keys(displayStructure._profiles).length;
                displayStructure._profiles[fullName] = profileIndex;
                displayStructure.profiles.push(fullName);
            }

            var searchString = component.get('v.searchString').toLowerCase();
            for (var classVisibility of profileInfo.classAccesses) {
                var classIndex = displayStructure._classes[classVisibility.apexClass];
                var newClass = false;
                if (classIndex === undefined) {// if its a new class, we need to fill for all profiles empty
                    newClass = true;
                    classIndex = Object.keys(displayStructure._classes).length;
                    displayStructure._classes[classVisibility.apexClass] = classIndex;
                }

                if (newClass) {
                    displayStructure.classes[classIndex] = {
                        apexClass : classVisibility.apexClass, 
                        showMe : classVisibility.apexClass.toLowerCase().indexOf(searchString) != -1,
                        profiles:[]} ;
                }

                displayStructure.classes[classIndex].profiles[profileIndex] = {enabled: classVisibility.enabled, profile: fullName};

            }
        },

        _handleGenericClick : function(component, profileName, className, checkboxName, setTo /*true:false*/) {
            // it lists, per checkbox, which other checkboxes need to be set to true/false
            var actionsMapping = {
                enabled : { falseToTrue : [],
                             trueToFalse : [] }
            };

            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            var classIndex = displayStructure._classes[className];
            var profileIndex = displayStructure._profiles[profileName];

            var profileClass = profilesInformation[profileName].classAccesses.find(x=>x.apexClass==className);
            var displayClass = displayStructure.classes[classIndex].profiles[profileIndex];

            // set current default field
            var checkboxList = [checkboxName].concat(actionsMapping[checkboxName][setTo?'falseToTrue':'trueToFalse']);
            var valueToSet = setTo?'true':'false';

            for (var checkbox of checkboxList) {
                displayClass[checkbox] = profileClass[checkbox] = valueToSet;
            }

            profileClass._hasChanged = true;

            component.set('v.profilesDisplayStructure',displayStructure);
        }
    }
)