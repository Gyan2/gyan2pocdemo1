({
    searchAccounts:function(component,event,helper)
    {
       	var target = event.getSource();  
        var apexValue=[];
        var displayList=[];
        var inputId = target.get("v.name") ;
        var action=component.get("c.accountLookup");
        var AccountValue=component.get("v.accountValue");
        var LocationValue=component.get("v.locationValue");
		var ProductValue=component.get("v.productValue");
        component.set("v.inputObject",inputId);
        action.setParams({objectName:inputId});
        action.setCallback(this,function(response){
            var state=response.getState();
            if (state === "SUCCESS") {
                //set response value in lstOpp attribute on component.
                apexValue=response.getReturnValue();
                
                  if(inputId=='Account'){
                 if(AccountValue==undefined){
                     component.set("v.accList",apexValue);
                 }
                 else{
                     
                        for(var i=0;i<apexValue.length;i++){
                          
                        	var n = apexValue[i].Name.toLowerCase().indexOf(AccountValue.toLowerCase());
                    			if(n>=0)
                                {
                                    displayList.push(apexValue[i]);
                    			}
                    		}
                                 component.set("v.accList",displayList);
                    }
            }
                            if(inputId=='Location'){
        		if(LocationValue==undefined)
                {
             		component.set("v.accList",apexValue); 
        		}
                else{
                     for(var i=0;i<apexValue.length;i++){
                    
                    var n = apexValue[i].Name.toLowerCase().indexOf(LocationValue.toLowerCase());
                    if(n>=0){
                        displayList.push(apexValue[i]);
					 }
					
                    }
					component.set("v.accList",displayList);                    
                }
                            }
                
                if(inputId == "Product")
                {
         
            if(ProductValue==undefined){
                component.set("v.accList",apexValue);    
        		}
                else
                {
                     for(var i=0;i<apexValue.length;i++){
                  		var n = apexValue[i].Name.toLowerCase().indexOf(ProductValue.toLowerCase());
                    	if(n>=0){
                        displayList.push(apexValue[i]);
                    	}
                     }
                    component.set("v.accList",displayList);
                    
                }
                }
                
              component.set("v.isOpen", true);
               
            }
            });
		$A.enqueueAction(action);
    },
    
    accountCopy: function(component,event,helper)
    {
        
        var attribute=event.getSource().get("v.name");
        var name=component.get("v.inputObject")
        if(name == "Account")
        component.set("v.accountValue", attribute);
        if(name == "Location")
        component.set("v.locationValue",attribute)
        if(name=="Product")
        component.set("v.productValue",attribute);
        component.set("v.isOpen", false);
        
       	
    },
    closeModel: function(component, event, helper) 
    {
       component.set("v.isOpen", false);
   	},
    
    clearField: function(component, event, helper)
    {
        component.set("v.accountValue"," ");
        component.set("v.locationValue"," ");
        component.set("v.productValue"," ");
    },
    
    searchResults1: function(component,event,helper)
    {        
        var searchResults=[];
		//var apexValue=component.get("v.accList");
        
        /*for(var i=0;i<apexValue.length;i++)
                {
                   if(apexValue[i].Name == AccountValue)
                    {
                        alert(apexValue[i].Id);
                        component.set("v.SearchId",apexValue[i].Id);
                    }
                }*/
        var Account=component.get("v.accountValue");
        var Location=component.get("v.locationValue");
    	var Product=component.get("v.productValue");
        var searchCriteria=component.get("v.search_Criteria");
        var action=component.get("c.search_Results");
        action.setParams({accountValue:Account,
                          searchLocation:Location,
                          searchProduct:Product,
                          searchcriteria:searchCriteria
                          });
         action.setCallback(this,function(response){
            var state=response.getState();
             if (state === "SUCCESS") {
                 
                searchResults=response.getReturnValue();
                component.set("v.displayList",searchResults);
                  
	    var evt=$A.get("e.c:displaySearchedRecord");
        evt.setParams({"searchedInstalledProduct":searchResults});
			evt.fire();
               }
             
         });
       
        $A.enqueueAction(action);
                       
        
	},
    
    attachProduct: function(component,event,helper)
    {
        
        component.set("v.accList","");
        component.set("v.inputObject","");
        var list=component.get("v.displayList");
        var length=list.length;
        component.set("v.isOpen",true);
        if(length>0)
        {
           
           component.set("v.attached",'Installed Products are successfully attached toparent record');
        }
        else
        {
            
            component.set("v.attached",'Select records to attach');
        }
      
    }
    
 })