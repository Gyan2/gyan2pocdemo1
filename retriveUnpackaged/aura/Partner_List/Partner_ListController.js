(
    {
        handleInit : function(component, event, helper) {
            helper.loadRecords(component);
        },
        showAddPartner : function (component, event, helper) {
            component.find('add-partner-modal').set('v.open',true);
        },

        doSavePartners : function (component, event, helper) {
            helper.savePartners(component);

        },

        handleDelete : function(component, event, helper) {
            helper.deletePartner(component, event.getSource().get('v.value'));
        },

        removeLimit : function(component, event, helper) {
            component.set('v._limit', 0);
            helper.loadRecords(component);
        }
    }
)