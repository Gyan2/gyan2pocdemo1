(
    {
        loadRecords : function(component) {
            var action = component.get('c.auraGetPartners');
            component.set('v._loading', true);
            action.setParams({
                fields : component.get('v.fields').replace('/ /g','').split(','),
                recordId : component.get('v.recordId'),
                queryLimit : component.get('v._limit')
            });

            action.setCallback(this, function(response) {
                if (response.getState() === 'SUCCESS' && component.isValid()) {
                    var returned = response.getReturnValue();

                    component.set('v._roles', returned.roles);
                    var newRoles = [];
                    for (var i = 0; i < 5; i++) {
                        newRoles.push({sobjectType:'Partner', AccountFromId:component.get('v.recordId')});
                    }
                    component.set('v._newPartners', newRoles);

                    var data = [];
                    data.push(returned.labels);

                    for (var forPartner of returned.partners) {
                        var row = [];
                        for (var forField of returned.fields) {
                            row.push(this._extract(forPartner, forField));
                        }

                        data.push(row);
                    }

                    component.set('v._data', data);
                }
                component.set('v._loading',false);
            });

            $A.enqueueAction(action);
        },

        _extract : function (paramObject, paramField) {
            if (paramField.indexOf('.') == -1) {
                return paramObject[paramField];
            } else {
                var fieldSplit = paramField.split('.');
                var currentLevel = fieldSplit.shift();

                if (paramObject[currentLevel] == null){
                    return null;
                } else {
                    return this._extract(paramObject[currentLevel], fieldSplit.join('.'));
                }
            }
        },


        savePartners : function (component) {
            var action = component.get('c.auraSavePartners');


            var lookups = component.find('lookup');
            var roles = component.find('role');

            var newPartners = [];

            for (var i = 0; i < lookups.length; i++) {
                var accountId = lookups[i].get('v.selectedId');
                if (accountId != undefined) {
                    newPartners.push(accountId + '::' + roles[i].get('v.value'));
                }
            }


            if (newPartners.length > 0) {

                action.setParams({
                    accountId : component.get('v.recordId'),
                    partners : newPartners
                });

                var self = this;
                action.setCallback(this, function(response) {
                    if (response.getState() === 'SUCCESS' && component.isValid()) {
                        self.loadRecords(component);
                        component.find('add-partner-modal').set('v.open',false);
                    }

                });

                $A.enqueueAction(action);
             }
         },

         deletePartner : function (component, partnerId) {
             var action = component.get('c.auraDeletePartner');

             action.setParam('partnerId', partnerId);

             var self = this;
             action.setCallback(this, function(response) {
                 if (response.getState() === 'SUCCESS' && component.isValid()) {
                     self.loadRecords(component);
                 }
             })

             $A.enqueueAction(action);
         }
    }
)