(
    {
        handleInit : function(component, event, helper) {
            helper.getInitialAddUsersInformation(component);
            helper.searchUsers(component);
        },
        handleFilterClick : function(component, event, helper) {
            var value = event.getSource().get('v.class').replace(/^.*js-value-([^ ]*).*$/,'$1');

            var searchFields = component.get('v.searchFields');
            searchFields[value] = !searchFields[value];

            component.set('v.searchFields', searchFields);

            helper.searchUsers(component);
        },

        handleSearchChange : function(component, event, helper) {
            helper.searchUsers(component);
        },

        handleCheckboxClick : function (component, event, helper) {
            var value = event.getSource().get('v.value');

            var selectableCount = component.get('v._selectableCount');
            var selectedCount = component.get('v._selectedCount');

            if (value == '') { // selectAll
                var checked = selectedCount != selectableCount;

                var users = component.get('v._users');
                for (var user of users) {
                    if (!user._disabled) user._checked = checked;
                }
                if (checked) selectedCount = component.get('v._selectableCount');
                else selectedCount = 0;

                component.set('v._users',users);
            } else {
                var checked = event.getSource().get('v.checked');
                if (checked) selectedCount++;
                else selectedCount--;

            }
            component.set('v._selectedCount', selectedCount);
        },

        handleAddUsers : function (component, event, helper) {
            helper.addUsers(component);
        }
    }
)