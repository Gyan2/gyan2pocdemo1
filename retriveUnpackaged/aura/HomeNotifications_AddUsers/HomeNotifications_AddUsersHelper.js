(
    {
        getInitialAddUsersInformation : function(component) {
            var action = component.get('c.auraGetInitialAddUsersInformation');

            action.setParam('notificationId',component.get('v.recordId'));

            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS" && component.isValid()){
                    var returned = response.getReturnValue();
                    component.set('v._labels', returned.labels);

                    var alreadyExistingUserIds = {};
                    for (var userNotification of returned.userNotifications) {
                        alreadyExistingUserIds[userNotification.UserRef__c] = true;
                    }
                    component.set('v._alreadyExistingUserIds', alreadyExistingUserIds);
                }
            });

            $A.enqueueAction(action);
        },
        searchUsers : function(component) {
            var action = component.get('c.auraSearchUsers');
            component.set('v._loading', true);
            action.setParams({
                'searchString' : component.get('v.searchValue'),
                'searchFields' : component.get('v.searchFields')
            });

            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    if (response.getParam('searchString') == component.get('v.searchValue') &&
                        response.getParam('searchFields') == component.get('v.searchFields')
                    ) {
                        var returned = response.getReturnValue();
                        var selectableCount = 0;
                        var alreadyExistingUserIds = component.get('v._alreadyExistingUserIds');
                        for (var user of returned){
                            user._checked = user._disabled = alreadyExistingUserIds[user.Id] || false;
                            if (!user._checked) selectableCount++;
                        }
                        component.set('v._selectedCount',0);
                        component.set('v._selectableCount', selectableCount);
                        component.set('v._users', returned);
                        component.set('v._loading', false);
                    }
                } else {
                    component.set('v._loading', false);
                }
            });

            $A.enqueueAction(action);
        },

        addUsers : function (component) {
            var action = component.get('c.auraAddUsers');

            action.setParams({
                notificationId : component.get('v.recordId'),
                users : component.get('v._users').filter(x=>x._checked && !x._disabled)
            });

            action.setCallback(this, function(response){
                if (response.getState() === "SUCCESS" && component.isValid()) {

                    var alreadyExistingUserIds = component.get('v._alreadyExistingUserIds');
                    for (var user of response.getParam('users')) {
                        alreadyExistingUserIds[user.Id] = true;
                    }
                    component.set('v._alreadyExistingUserIds', alreadyExistingUserIds);
                    $A.get('e.force:refreshView').fire();
                    this.searchUsers(component);
                }
            });

            $A.enqueueAction(action);
        }
    }
)