/**
 * Created by trakers on 20.07.18.
 */
({
    setEditMode:function(component,event,helper){

        event.getSource().get('v.value')._edit = true;
        component.set('v.text',component.get('v.text'));
    },
    onFocus:function(component,event,helper){
        component.get('v.text')._changed = true;
        component.set('v.text',component.get('v.text'));
        console.log(JSON.parse(JSON.stringify(component.get('v.text'))))
    },
})