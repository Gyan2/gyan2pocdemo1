(
    {
        handleInit : function(component, event, helper) {
            var action = component.get('c.auraGetAccountContactBillingInformation');

            action.setParam('recordId', component.get('v.recordId'));

            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    var returned = response.getReturnValue();

                    var accountAddress = returned.account.BillingAddress;
                    if (accountAddress != undefined) accountAddress.country = returned.account.BillingCountryCodeLabel;
                    var contacts = [];
                    for (var contact of returned.account.Contacts || []) {
                        var same = true;
                        if(contact.MailingAddress != undefined) contact.MailingAddress.country = contact.MailingCountryCodeLabel;
                        for (var prop in contact.MailingAddress) {
                            if (contact.MailingAddress[prop] != accountAddress[prop]) {
                                same = false;
                                break;
                            }
                        }

                        if (!same || contact.MailingAddress === undefined) {
                            contacts.push(contact);
                        }
                    }

                    component.set('v._account', returned.account);
                    component.set('v._contacts', contacts);
                    component.set('v._fieldLabels', returned.fieldLabels);
                    component.set('v._checkedCount', contacts.length);
                    console.log(returned);
                }
            });

            $A.enqueueAction(action);
        },

        handleSelectAll : function(component, event, helper) {
            var selectAllCheckedValue = component.find('selectAll').get('v.checked');
            var checkboxes = component.find('select');
            if (checkboxes == undefined) {
                checkboxes = [];
            } else if (!Array.isArray(checkboxes)) {
                checkboxes = [checkboxes];
            }

            for (var checkbox of checkboxes) {
                checkbox.set('v.checked',selectAllCheckedValue);
            }
            component.set('v._checkedCount',selectAllCheckedValue?checkboxes.length:0);
        },

        handleSaveClick : function(component, event, helper) {
            var checkedContacts = component.find('select');
            if (checkedContacts == undefined){
                checkedContacts = [];
            } else if (!Array.isArray(checkedContacts)) {
                checkedContacts = [checkedContacts];
            }

            checkedContacts = checkedContacts.filter(x=>x.get('v.checked'));

            var account = component.get('v._account');
            var contacts = [];
            for (var forCheckedContact of checkedContacts) {
                var contactId = forCheckedContact.get('v.class').replace(/^.*contact-id-([a-zA-Z0-9]{18}).*$/,"$1");

                contacts.push({
                    sobjectName :               'Contact',
                    Id :                        contactId,
                    MailingGeocodeAccuracy :    (account.BillingAddress && account.BillingAddress.accuracy) || null,
                    MailingCity :               (account.BillingAddress && account.BillingAddress.city) || null,
                    MailingCountry :            (account.BillingAddress && account.BillingAddress.country) || null,
                    MailingCountryCode :        (account.BillingAddress && account.BillingAddress.countryCode) || null,
                    MailingLatitude :           (account.BillingAddress && account.BillingAddress.latitude) || null,
                    MailingLongitude :          (account.BillingAddress && account.BillingAddress.longitude) || null,
                    MailingPostalCode :         (account.BillingAddress && account.BillingAddress.postalCode) || null,
                    MailingState :              (account.BillingAddress && account.BillingAddress.state) || null,
                    MailingStateCode :          (account.BillingAddress && account.BillingAddress.stateCode) || null,
                    MailingStreet :             (account.BillingAddress && account.BillingAddress.street) || null/*
                    MailingAddress:account.BillingAddress*/});
            }

            var action = component.get('c.auraSetAccountContactBillingInformation');

            action.setParam('contacts',contacts);

            action.setCallback(this, function(response) {
                if (response.getState() === "SUCCESS" && component.isValid()) {
                    var toastEvent = $A.get("e.force:showToast");
                    toastEvent.setParams({
                        type : "success",
                        /*title : "Success!",*/
                        message : "Success !"
                    });
                    $A.get("e.force:closeQuickAction").fire();
                    toastEvent.fire();
                } else if (response.getState() === "ERROR") {
                    var error = (response.getError()[0] &&
                                response.getError()[0].pageErrors[0]) || {statusCode:'UNKNOWN_ERROR',message:'Please contact your administrator or try again'};

                    component.set('v._error', error.statusCode + ' : ' + error.message);
                }
            });

            $A.enqueueAction(action);

        },

        handleCloseError : function(component, event, helper) {
            component.set('v._error', '');
        },

        handleSelect : function(component, event, helper) {
            var selectedCount = helper._extractSelected(component.find('select')).length;
            var allCount = component.get('v._contacts').length;
            component.set('v._checkedCount',selectedCount);
            component.find('selectAll').set('v.checked',selectedCount == allCount);
        }
    }
)