(
    {
        _extractSelected : function(fullList) {
            if (fullList == undefined) return [];
            else if (Array.isArray(fullList)) return fullList.filter(x=>x.get('v.checked'));
            else if (fullList.get('v.checked')) return [fullList];
            else return [];
        }
    }
)