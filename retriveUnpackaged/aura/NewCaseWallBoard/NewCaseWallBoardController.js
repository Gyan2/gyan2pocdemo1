({
    doInit: function(component, event, helper) {
        
        helper.getCaseList(component, event, helper);
        helper.fetchPickListVal1(component,'Type','Typeid');   
        setInterval(function(){ 
            component.set('v.currentTime',new Date().toLocaleTimeString());
        }, 500);
        
        
        
        
        setInterval(function(){ 
            
            var allCaseRecord = component.get("v.Cases");
            var cloneCases = component.get("v.cloneCases");
            var timeGst = new Date().toGMTString();
            var currentDate = new Date(timeGst);
            for(var index = 0; index < allCaseRecord.length; index++) {
                allCaseRecord[index].notification = "0";
                if(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c != undefined){
                    var test = new Date(cloneCases[index])
                    var test1 = (test.getTime() - currentDate.getTime() );
                    if(test1 < 3600000 && test1 > 0){
                        allCaseRecord[index].notification = "1";
                        
                    }
                    if(test1 <  0){
                        allCaseRecord[index].notification = "2";
                        
                    }
                }
                
            }
            component.set('v.Cases', allCaseRecord);
            
            
        }, 60000);
        
        
        
        
        setInterval(function()
                    { 
                        
                        if(component.get("v.index") == 0)
                        {
                            $("#table").slideUp(3500);
                            $("#paichart").slideDown(2500);
                            
                        }
                        else if(component.get("v.index") == 1) {
                            $A.get('e.force:refreshView').fire();
                        }
                        else if(component.get("v.index") == -1){
                            
                            $("#paichart").slideUp(2500);
                            $("#table").slideDown(3500);
                            
                            component.set("v.index", 3); 
                        }
                        component.set("v.index", component.get("v.index") - 1);
                        
                    }, 10000); 
        
        
    },
    
    ScriptLoaded : function(component, event, helper) {
        
        var action = component.get("c.getUserSession");
        action.setCallback(this, function(response)
                           {
                               var state = response.getState();
                               if(state === "SUCCESS") 
                               {
                                   $.cometd.init(
                                       {
                                           url: '/cometd/43.0',
                                           requestHeaders: { Authorization: response.getReturnValue()},
                                           appendMessageTypeToURL : false
                                       });
                                   $.cometd.subscribe('/topic/CaseWallBoard20', function(message) 
                                                      {
                                                          
                                                          console.log("outside if: " + component.get("v.PageNumber"));
                                                          console.log("QQQQQQQQQQQ"+JSON.stringify(message.data.event.type));
                                                          if(message.data.event.type == "updated")
                                                          {
                                                              var updatedRecord = message.data.sobject;
                                                              var d = new Date(updatedRecord.Next_SLA_Milestone_Due_Date__c);
                                                              console.log( "Formated time: "+(d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes());
                                                              updatedRecord.Next_SLA_Milestone_Due_Date__c = (d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes();
                                                              var caseList = component.get("v.Cases");
                                                              for(var index = 0; index < caseList.length; index++)
                                                              {
                                                                  if(caseList[index].Id == updatedRecord.Id)
                                                                  {
                                                                      caseList[index] = updatedRecord;
                                                                  }
                                                              }
                                                              component.set("v.Cases", caseList);
                                                              
                                                          }
                                                          if(message.data.event.type == "created")
                                                          {
                                                              component.set("v.TotalRecords", component.get("v.TotalRecords") + 1);
                                                              component.set("v.TotalPages", Math.ceil(component.get("v.TotalRecords") / 10 ));
                                                              console.log("outside if: " + component.get("v.PageNumber"));
                                                              
                                                              var newRec = message.data.sobject;
                                                              var d = new Date(newRec.Next_SLA_Milestone_Due_Date__c);
                                                              newRec.Next_SLA_Milestone_Due_Date__c = (d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes();
                                                              
                                                              
                                                              console.log("inside if");
                                                              var caseList = component.get("v.Cases");
                                                              console.log("Check caseList:" + caseList);
                                                              if(caseList.length == 25) {
                                                                  caseList.pop();
                                                              }
                                                              caseList.unshift(newRec);
                                                              console.log("Check upsert:" + caseList);
                                                              component.set("v.Cases", caseList);
                                                              
                                                              
                                                          }
                                                          
                                                      });
                               }
                           });
        $A.enqueueAction(action);
    },
    
    
    
    RemoveFilter :  function(component, event, helper){
      
        var PicklistOptions = component.get("v.PicklistOptions1");
        for(var index = 0; index < PicklistOptions.length; index++) {
            PicklistOptions[index].checked = false;
        }    
        console.log("qwertyuio: "+JSON.stringify(PicklistOptions));
        component.set("v.PicklistOptions1", PicklistOptions);
        helper.ApplyFilterHelper(component, event, helper);
        
        
    },
    
    
    ApplyFilterandHidePopUp  :  function(component, event, helper){
        
        helper.ApplyFilterHelper(component, event,helper); 
        helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
        helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
        
    },
    
    onInputClick : function(component,event,helper){
        
        helper.showPopupHelper(component, 'modaldialog', 'slds-fade-in-');
        helper.showPopupHelper(component,'backdrop','slds-backdrop--');
        
    },
    
    CloseButton :function(component, event, helper){
        component.set("v.DateErrorPopUp","");
        helper.hidePopupHelper(component, 'modaldialog', 'slds-fade-in-');
        helper.hidePopupHelper(component, 'backdrop', 'slds-backdrop--');
    },
    
    
    getFilerValue : function(component, event, helper){
        
        var target = event.getSource();  
        var value = target.get("v.checked") ;
        console.log("Check : " + value);
        var typeFilterList = component.get("v.typeFilterList");
        if(!value){
            
            if(typeFilterList.indexOf(target.get("v.label")) == -1) {
                typeFilterList.push(target.get("v.label"));
            }
        }
       else {
            var index = typeFilterList.indexOf(target.get("v.label"));
            
            if (index !== -1) typeFilterList.splice(index, 1);
       }
        console.log("Check Array: " + typeFilterList);
        component.set("v.typeFilterList", typeFilterList);
        
        
    }
    
})