({
    getCaseList: function(component) {
        
        
        var action = component.get('c.getCase');
        
        
        // Set up the callback
        
        var self = this;
        
        
        action.setCallback(this, function(actionResult) {
            
            var allCaseRecord = actionResult.getReturnValue();
            var cloneCases = [];
            var timeGst = new Date().toGMTString();
            var currentDate = new Date(timeGst);
            
            for(var index = 0; index < allCaseRecord.length; index++) {
                var d = new Date(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c);
                allCaseRecord[index].notification = "0";
                if(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c != undefined){
                    var test = new Date(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c)
                    var test1 = (test.getTime() - currentDate.getTime() );
                    cloneCases.push(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c);
                    console.log("check: " +allCaseRecord[index].CaseNumber +"\t"+test.getTime()+"\t"+currentDate.getTime()+"\t"+test1);
                    if(test1 < 3600000 && test1 > 0){
                        allCaseRecord[index].notification = "1";
                        
                    }
                    if(test1 <  0){
                        allCaseRecord[index].notification = "2";
                        
                    }
                    allCaseRecord[index].Next_SLA_Milestone_Due_Date__c = (d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes();
                }
            }
            
            component.set('v.Cases', allCaseRecord);
            component.set('v.cloneCases', cloneCases);
            console.log("Check cloneCase: " + component.get("v.cloneCases"));
            
            
            
        });
        
        $A.enqueueAction(action);
        
    },
    
    
    ApplyFilterHelper : function(component,event,helper) {
       
        var typeFilterList = [];
        var PicklistOptions = component.get("v.PicklistOptions1");
        for(var index = 0; index < PicklistOptions.length; index++) {
            if(PicklistOptions[index].checked == true) {
                typeFilterList.push(PicklistOptions[index].value)
            }
            
        } 
        component.set("v.typeFilterList", typeFilterList)
        var action = component.get('c.filterCaseList');
        
        action.setParams({
            "SearchType" : component.get("v.typeFilterList"),
            "ServiceArea" : component.get("v.ServiceArea"),
            
        });
        
        // Set up the callback
        
        var self = this;
        
        action.setCallback(this, function(response) {
            var state = response.getState();
            if (state === "SUCCESS") {
                
                var allCaseRecord = response.getReturnValue();
                var cloneCases = [];
                var timeGst = new Date().toGMTString();
                var currentDate = new Date(timeGst);
                
                for(var index = 0; index < allCaseRecord.length; index++) {
                    
                    allCaseRecord[index].notification = "0";
                    if(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c != undefined){
                        var test = new Date(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c)
                        var test1 = (test.getTime() - currentDate.getTime() );
                        cloneCases.push(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c);
                        console.log("check: " +allCaseRecord[index].CaseNumber +"\t"+test.getTime()+"\t"+currentDate.getTime()+"\t"+test1);
                        if(test1 < 3600000 && test1 > 0){
                            allCaseRecord[index].notification = "1";
                            
                        }
                        if(test1 <  0){
                            allCaseRecord[index].notification = "2";
                            
                        }
                        
                        var d = new Date(allCaseRecord[index].Next_SLA_Milestone_Due_Date__c);
                        console.log(JSON.stringify(allCaseRecord[index]));
                        console.log(d);
                        allCaseRecord[index].Next_SLA_Milestone_Due_Date__c = (d.getDate()>9?"":"0") +d.getDate()+ "." + (d.getMonth()>9?"":"0") +d.getMonth() + "." + d.getFullYear()+ " " +(d.getHours()>9?"":"0") + d.getHours() + ":" + (d.getMinutes()>9?"":"0") +d.getMinutes();
                    }
                }
                
                component.set('v.Cases', allCaseRecord);
                component.set('v.cloneCases', cloneCases);
              
                var countfilter="";
                var filerValue = component.get("v.typeFilterList");
                if(filerValue.length != 0) {
                    countfilter = filerValue[0];
                    for (var i = 1; i < filerValue.length; i++){
                    	countfilter=countfilter+ ', ' + filerValue[i];
                	}
                }
                
                
                component.set("v.filterCount",countfilter);
                
                if(countfilter ==0){
                    component.set("v.filterCount",'');
                }
            }
            
        });
        
        $A.enqueueAction(action);
        
    },
    
    // Helper method for picklist(TYPE ) and SERVICE AREA
    fetchPickListVal1: function(component, fieldName, elementId) {
        var action = component.get("c.getselectOptions1");
        action.setParams({
            "objObject": component.get("v.objInfo"),
            "fld": fieldName
        });
        // set call back
        var Prod = [];
        action.setCallback(this, function(response) {
            if (response.getState() == "SUCCESS") {
                var allValues = response.getReturnValue();
                var pickListValue = [];
                for(var index = 0; index < allValues.length; index++) {
                    if(allValues[index] == "Reactive") {
                        pickListValue.push({"checked" : true, "value" : allValues[index]});
                    }
                    else {
                    	pickListValue.push({"checked" : false, "value" : allValues[index]});
                    }
                    
                }
                component.set("v.PicklistOptions1", pickListValue);
                
            }
        });
        $A.enqueueAction(action);
        
    },
    
    showPopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');
    },
    hidePopupHelper: function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
        
    },
    
})