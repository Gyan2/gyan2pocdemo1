/**
 * Created by trakers on 22.06.18.
 */
({
    sortableSection:function(component){
        console.log('Load sortable')
        $('#list').sortable({ //.section-container
             //handle:'.section',
             connectWith: ".editLayout",
             update: function(event,ui) {
                 console.log('UPDATE SECTION');

             },
             placeholder:'ui-state-highlight',
             start:function(event,ui){
                 $('.ui-state-highlight').height(ui.item.height());
             }
        })
    },
    sortableItems:function(component){
        console.log('Load sortable')
        $('.item-container').sortable({ //.section-container
             //handle:'.section',
             connectWith: ".item-container",
             update: function(event,ui) {
                 console.log('UPDATE SECTION');

             },
             placeholder:'ui-state-highlight',
             start:function(event,ui){
                 $('.ui-state-highlight').height(ui.item.height());
             }
        })
    },
    generateTree:function(component){
        var sections = component.get('v.quote.Sections__c');
        var quoteLineItems = component.get('v.quote.QuoteLineItems');

        var tree = [];
        sections.forEach(val => {
            tree.push({'name':val.name,'id':val.id,'items':[]});
        })

        quoteLineItems.forEach(qli => {
            var section = tree.find(val => val.id == qli.Section__c);
            console.log(section);
            if(section){
                section.items.push({'name':qli.Product2.Name,'id':qli.Id});
            }
        })

        return tree;
    }
})