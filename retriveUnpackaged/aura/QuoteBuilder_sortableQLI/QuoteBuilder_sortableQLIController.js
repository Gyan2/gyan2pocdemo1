/**
 * Created by trakers on 22.06.18.
 */
({
    onInit:function(component,event,helper){

        component.set('v.tree',helper.generateTree(component));

        window.setTimeout(function(){
           helper.sortableSection(component);
           helper.sortableItems(component);
        },700)
    },
    onCloseClick:function(component,event,helper){

    },
    onSaveClick:function(component,event,helper){
        var sections = [];

         $('.section').each(function(index){
             var sectionId = $(this).data('id');
             console.log(' SectionId: '+sectionId);
             sections.push(component.get('v.quote.Sections__c').find(val => val.id == sectionId));


             $(this).find('.item').each(function(index){
                 var itemId = $(this).data('id');
                 var position = index + 1;
                 console.log(' ItemId: '+itemId);
                 var qli = component.get('v.quote.QuoteLineItems').find(val => val.Id == itemId);
                     //POSITION
                     if(qli.Position__c != position){
                         qli.Position__c = position;
                         qli._changed = true;
                     }
                     //SECTION
                     if(qli.Section__c != sectionId){
                          qli.Section__c = sectionId;
                          qli._changed = true;
                     }
             })
         });

         component.set('v.quote.Sections__c',sections);
         component.get('v.quote.QuoteLineItems').sort(function(a,b){
             return a.Position__c - b.Position__c;
         })

         component.set('v.quote',component.get('v.quote'));
    }
})