/**
 * Created by trakers on 19.06.18.
 */
({
    onInit:function(component,event,helper){
        console.log('Value dropped has changed');
        component.set('v.loading',true);
        helper.checkQuoteLineItems(component)
        .then(val => {
            component.set('v.productMapping',val);
            helper.handleData(component);
            component.set('v.loading',false);
        });

        if(component.get('v.quote.Sections__c')){
             helper.generatePicklistValues(component);
        }

    },
    closeClick:function(component,event,helper){
        component.set('v.display',false);
        component.set('v.done',false);
    },
    nextClick:function(component,event,helper){

    },
    previousClick:function(component,event,helper){

    },
    saveClick:function(component,event,helper){
        console.log('INSERT');
        if(component.get('v.addQLI_section') == null){
            component.find('picklist').set('v.isRequiredShow',true);
            // TODO : exit and show the field as required !
            return;
        }
        component.set('v.loading',true);


        var  appEvent = $A.get('e.c:QuoteConfiguratorV2_event');
             appEvent.setParams({
                 type : 'refresh'
             });
        // If there is nothing to insert, we just close it
        if(component.get('v.quoteLineItems').filter(item => item._isValid && item._insert).length == 0){
            component.set('v.loading',false);
            component.set('v.display',false);
            return;
        }

        var quoteLineItems = component.get('v.quoteLineItems').filter(val => val._insert && val._isValid);
        var quoteLineItemFields = component.get('v.quoteLineItemFields');
        var positionStart = component.get('v.quote.QuoteLineItems').filter(val => val.Section__c == component.get('v.addQLI_section')).length;
            positionStart = positionStart + 1; // We need to add 1 more

        quoteLineItems.forEach(val => {
            val.Section__c          = component.get('v.addQLI_section');
            val.Position__c         = positionStart;
            positionStart++;
        })

        console.log('new quoteLineItems');
        console.log(JSON.parse(JSON.stringify(quoteLineItems)));


        // From Abstract
        helper.insertQuoteLineItems(component,quoteLineItems,component.get('v.quoteLineItemFields'))
        .then(function(res){
            console.log(res);
            var mapping = component.get('v.productMapping');
            if(res.status == 200){
                res.data.forEach(val => {
                    var key = val.Product2.SAPInstance__c+'-'+val.Product2.SAPNumber__c;
                    if(mapping.hasOwnProperty(key)){
                        mapping[key] = 'success';
                    }
                })
                component.set('v.productMapping',mapping);
                helper.handleData(component); // we should be able to remove this ? I have to check
                component.set('v.loading',false);
                appEvent.fire();
                component.set('v.done',true);
            }else{
                 console.log('Errors');
                 component.set('v.internal_errorMessage',err);
                 component.set('v.done',true);
                 component.set('v.loading',false);
                 console.log(err);
            }





            //component.set('v.display',false);

        })
        .catch(function(err){
            console.log('Errors');
            component.set('v.global_errorMessage',err);
            component.set('v.done',true);
            component.set('v.loading',false);
            component.set('v.display',false);
            console.log(err);
        })

    },
    insertAllChange:function(component,event,helper){
        var value = event.getSource().get('v.checked');
        component.get('v.quoteLineItems').filter(item => item._isValid).forEach(item => {
            item._insert = value;
        })

        component.set('v.quoteLineItems',component.get('v.quoteLineItems'));
    },
    picklist_onChange:function (component,event,helper){
        console.log('picklist_onChange');
        component.find('picklist').set('v.isRequiredShow',false);
    }

})