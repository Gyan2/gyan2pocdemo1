/**
 * Created by trakers on 19.06.18.
 */
({
    isNumber:function(param){
        return !isNaN(parseFloat(param)) && isFinite(param);
    },
    getQuoteLineItemId:function(obj,type){
            var classes;
            switch(type){
                case 'event':
                        if(typeof obj.getSource !== 'undefined'){
                            classes = obj.getSource().get('v.class').split(' ');
                        }else{
                            classes = obj.currentTarget.className.split(' ');
                        }
                break;
                case 'element':
                        classes = obj.className.split(' ');
                break;
            }


            return classes.find(x=>x.indexOf('data-sobject-id-')!=-1).replace('data-sobject-id-','');
    },
    checkQuoteLineItems:function(component){
         var action = component.get('c.auraCheckQuoteLineItems');
         var products = [];
         var data = component.get('v.data');

            data.forEach(item => {
                products.push(item.ExternalId__c);
            })
            action.setParams({
                quoteId:component.get('v.quote.Id'),
                products:products
            });

            console.log({
                 quoteId:component.get('v.quote.Id'),
                 products:products
            });


        var that = this;
        return new Promise($A.getCallback(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                 if(response.getState() === "SUCCESS") {
                     console.log('callback');
                     console.log(res);
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         }))
    },
    handleData:function(component){
        console.log('handleData');
        var that = this;
        var quoteLineItems = [];
        var mapping = component.get('v.productMapping');

        var data = component.get('v.data');
        var section = component.get('v.addQLI_section');
        var positionStart = component.get('v.quote.QuoteLineItems').filter(val => val.Section__c == section).length;

        console.log('Section: '+component.get('v.addQLI_section'));
        console.log('Position Start: '+positionStart);
        data.forEach((item,index) => {
            var sobject = {
                'sobjectType':'QuoteLineItem',
                'Quantity':parseInt(item.Quantity),
                'Product2':{'sobjectType':'Product2','ExternalId__c':item.ExternalId__c},
                'Position__c':parseInt((positionStart+index+1)),
                'Section__c':parseInt(section),
                '_insert':true,
                '_isValid':true
                };

            // Check number
            if(!that.isNumber(item.Quantity)){
                sobject._isValid = false;
                sobject._insert = false;
                sobject._validMessage = 'Quantity is not a number';
            }

            // Check Mapping
            if(mapping[item.ExternalId__c] != undefined){
               sobject._validMessage = mapping[item.ExternalId__c];

                // Different cases
                if(mapping[item.ExternalId__c] == 'success' || mapping[item.ExternalId__c] == ''){
                      //sobject._insert = true;
                }else if(mapping[item.ExternalId__c] == 'duplicate'){
                      sobject._insert = false;
                }else{
                    sobject._isValid = false;
                    sobject._insert = false;
                }
            }

            // Check success
            if(mapping[item.ExternalId__c] != undefined && mapping[item.ExternalId__c] == 'success'){
                sobject._validMessage = 'success';
            }


            quoteLineItems.push(sobject);
        })
        console.log(JSON.parse(JSON.stringify(quoteLineItems)));
        component.set('v.quoteLineItems',quoteLineItems);
    },
    generatePicklistValues:function(component){
         //[{'active':true,'defaultValue':'false','label':'-- none --','value':'null'}]

         var picklistValues = [];
         component.get('v.quote.Sections__c').forEach(val => {
             picklistValues.push({'active':true,'defaultValue':'false','label':val.name,'value':val.id})
         })
         console.log(picklistValues);
         component.set('v.picklistValues',picklistValues);
    }
})