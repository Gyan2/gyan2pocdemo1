/**
 * Created by rebmangu on 09/04/2018.
 */
({
    onInit:function(component,event,helper){
         var building = {'sobjectType':'Building__c'};
         component.set('v.building',building);
    },
    recordLoaded:function(component,event,helper){
       console.log('RecordLoaded');
       //console.log(JSON.stringify(component.get('v.project')));
    },
    onChangeField:function(component,event,helper){
         var building = component.get('v.building');
         building[event.getSource().get('v.fieldName')] = event.getSource().get('v.value');
         component.set('v.building',building);
    },
    closeClick:function(component,event,helper){
        var redirectEvent = $A.get("e.force:navigateToObjectHome");
            redirectEvent.setParams({
                "scope": "Building__c"
            });
            redirectEvent.fire();
    },
    createClick:function(component,event,helper){
         event.preventDefault();
         helper.insertBuilding(component)
         .then(val => {
             if(val.status == 200){
                  var navEvt = $A.get("e.force:navigateToSObject");
                      navEvt.setParams({
                        "recordId": val.message,
                        "slideDevName": "related"
                      });
                      navEvt.fire();
             }else{
                 // Display the error
                 console.log(val);
                 component.set('v.displayError',true);
                 component.set('v.errorMessage',val.messageToDisplay);
             }
         })
    },
    updateClick:function(component,event,helper){
         event.preventDefault();
         helper.updateBuilding(component)
         .then(val => {
             if(val.status == 200){
                  var navEvt = $A.get("e.force:navigateToSObject");
                      navEvt.setParams({
                        "recordId": val.message,
                        "slideDevName": "related"
                      });
                      navEvt.fire();
             }else{
                 // Display the error
                 console.log(val);
                 component.set('v.displayError',true);
                 component.set('v.errorMessage',val.messageToDisplay);
             }
         })
    },
    previousClick:function(component,event,helper){
         component.set('v.step',1);
         component.set('v.displayMap',false);
    },
    nextClick:function(component,event,helper){
         component.set('v.step',2);
         component.set('v.displayMap',true);
         var appEvent = $A.get('e.c:Project_Event');
             appEvent.setParams({
                 type : 'center',
                 message:null
             });
             appEvent.fire();
    },
    addressSelectedChange:function(component,event,helper){
        var selected = component.get('v.address_selected');
        component.set('v.building.City__c',selected.locality);
        component.set('v.building.Country__c',selected.country);
        component.set('v.building.CountryIsoCode__c',selected.country_iso);
        if(selected.street_number == undefined){
            component.set('v.building.Street__c',selected.route);
        }else{
            component.set('v.building.Street__c',selected.street_number+' '+selected.route);
        }
        component.set('v.building.Geolocation__Latitude__s',selected.location.lat);
        component.set('v.building.Geolocation__Longitude__s',selected.location.lng);
        component.set('v.building.Zip__c',selected.postal_code);

    },
    openSearchAddress:function(component,event,helper){
        component.set('v.address_display',true);
    },
})