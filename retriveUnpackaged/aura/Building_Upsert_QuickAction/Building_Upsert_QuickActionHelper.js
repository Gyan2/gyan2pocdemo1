/**
 * Created by rebmangu on 09/04/2018.
 */
({
        insertBuilding:function(component){
            console.log('Insert Building');
               var action = component.get('c.insertBuilding');
                action.setParams({
                    building:component.get('v.building')
                })
                var that = this;
                return new Promise($A.getCallback(function(resolve, reject){
                    action.setCallback(that, function(response) {
                        var res = response.getReturnValue();
                         if(response.getState() === "SUCCESS") {

                            resolve(res);
                         }else{
                            reject(res);
                         }
                    });
                    $A.enqueueAction(action);
                 }))
        },
        updateBuilding:function(component){
               console.log('Update Building');
               var action = component.get('c.updateBuilding');
                action.setParams({
                    building:component.get('v.building')
                })
                var that = this;
                return new Promise($A.getCallback(function(resolve, reject){
                    action.setCallback(that, function(response) {
                        var res = response.getReturnValue();
                         if(response.getState() === "SUCCESS") {

                            resolve(res);
                         }else{
                            reject(res);
                         }
                    });
                    $A.enqueueAction(action);
                }))
        }
})