/**
 * Created by trakers on 20.06.18.
 */
({
    deleteOption:function(component,event,helper){
        var id = event.getSource().get('v.value');
        var detailId = component.get('v.quoteLineItem.Id');
        var quote = component.get('v.quote');


        var newOptions = quote.QuoteLineItems.find(val => val.Id == detailId).MaterialOptions__c.filter(val => val.id != id);

            quote.QuoteLineItems.find(val => val.Id == detailId).MaterialOptions__c = newOptions;
        // Update
        component.set('v.selectedDetail',quote.QuoteLineItems.find(val => val.Id == detailId));
        component.set('v.quote',quote);
        helper.generateConnectorOptions(component);
        helper.updateOptions(component,newOptions)
        .then($A.getCallback(val => {
            console.log(val);
        }))
    },
    checkChange:function(component,event,helper){
        var id = event.getSource().get('v.value');

        var detailId = component.get('v.quoteLineItem.Id');
        var options = component.get('v.quote').QuoteLineItems.find(val => val.Id == detailId).MaterialOptions__c;
            options.find(val => val.id == id).selected = event.getSource().get('v.checked');

        var quote = component.get('v.quote');
            quote.QuoteLineItems.find(val => val.Id == detailId).MaterialOptions__c = options;

        // Update
        component.set('v.quoteLineItem',quote.QuoteLineItems.find(val => val.Id == detailId));
        component.set('v.quote',quote);

        helper.generateConnectorOptions(component);
        helper.updateOptions(component,options)
        .then($A.getCallback(val => {
            console.log(val);
        }))

    },
})