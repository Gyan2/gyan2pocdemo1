/**
 * Created by trakers on 20.06.18.
 */
({
    generateConnectorOptions:function(component){

        var options = component.get('v.quoteLineItem');
            options = options.hasOwnProperty('MaterialOptions__c') ? options.MaterialOptions__c : [];

        var previous = null;
        for(var i = options.length - 1; i >= 0; i--){
            if(previous == null){
                options[i]._tree = options[i].selected ? 2:0;
            }else{
                if(previous == 0){
                    options[i]._tree = options[i].selected ? 2:0;
                }else{
                    options[i]._tree = options[i].selected ? 3:1;
                }
            }

            previous = options[i]._tree;
        }

        component.set('v.quoteLineItem',component.get('v.quoteLineItem'))

    },
    updateOptions:function(component,materialOptions){
        var action = component.get('c.auraUpdateOptions');

        var options = JSON.parse(JSON.stringify(materialOptions));
        options.forEach(val => {
            delete val._tree;
        })


        var that = this;
        action.setParams({
            quoteLineItemId     :  component.get('v.selectedDetail.Id'),
            options             :  JSON.stringify(options)
        });

        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                console.log(res);
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    },
})