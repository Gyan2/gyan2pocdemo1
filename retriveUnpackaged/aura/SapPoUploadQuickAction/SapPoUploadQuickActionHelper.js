({
    startUpload : function(cmp) {
        var self = this;
        var oId = cmp.get("v.recordId");
        //console.log(oId);
		var action = cmp.get("c.upload");
        action.setParams({
            "oppId": oId
        });
        action.setCallback(self, function(response)
		{
			if ( cmp.isValid() && response.getState() === 'SUCCESS' )
			{
                window.setTimeout(
                    $A.getCallback(function() {                     
                		self.showToast('Quote Sent', 'Sending successful');
                        self.gotoURL(cmp);
                    })
                    ,1500);
			}
			else
			{
				//console.log('Exception: ', response.getError()[0].message);
                self.showToast('Error!', response.getError()[0].message);
                self.closePanel();
			}
		});
        $A.enqueueAction(action);
	},
    // Display the total in a "toast" status message
    showToast : function(title,message){
         var resultsToast = $A.get("e.force:showToast");
        resultsToast.setParams({
            "title": title,
            "message": message
        });
        resultsToast.fire();
    },       
    // Close the action panel
    closePanel : function(){   
        
        var dismissActionPanel = $A.get("e.force:closeQuickAction");
        dismissActionPanel.fire();
        
    },
    
    sleepFor : function (miliseconds){
        var start = new Date().getTime();
        while (new Date().getTime() < start + miliseconds){};
    },
    gotoURL : function (cmp)
    {
        var oId = cmp.get("v.recordId");
        var urlEvent = $A.get("e.force:navigateToURL");
        urlEvent.setParams({
          "url": "/"+oId
        });
        urlEvent.fire();
}
})