({
    handleStartUpload : function(component, event, helper)
    {
        helper.startUpload(component);
    }, 
    handleClosePanel : function(component, event, helper)
    {
        helper.closePanel(component);
    },
})