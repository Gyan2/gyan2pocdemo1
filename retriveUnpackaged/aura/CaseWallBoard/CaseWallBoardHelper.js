({
	getCaseList: function(component) {
      
       
        var action = component.get('c.getCase');
        
        
        // Set up the callback
        
        var self = this;
        
        action.setCallback(this, function(actionResult) {
            
            component.set('v.Cases', actionResult.getReturnValue());
            
        });
        
        $A.enqueueAction(action);
        
    },
    
    /*******************************Pagination*******************************/
     getCaseListpagination: function(component, pageNumber, pageSize) {
       var action = component.get("c.getCasePagination");
         
          
         var test = parseInt(pageNumber) + parseInt(pageSize);
         console.log("Check this value: " + test);
       action.setParams({
           "pageNumber": parseInt(pageNumber),
           "pageSize": parseInt(pageSize)
       });
       action.setCallback(this, function(result) {
           var state = result.getState();
           //console.log("Check state: " + state);
           if (state === "SUCCESS"){
               var resultData = result.getReturnValue();
               component.set("v.Cases", resultData.CaseList);
               component.set("v.PageNumber", resultData.pageNumber);
               component.set("v.TotalRecords", resultData.totalRecords);
               component.set("v.RecordStart", resultData.recordStart);
               component.set("v.RecordEnd", resultData.recordEnd);
               component.set("v.TotalPages", Math.ceil(resultData.totalRecords / pageSize));
           }
       });
       $A.enqueueAction(action);
   },
    
   showDefectPopupHelper : function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.removeClass(modal, className + 'hide');
        $A.util.addClass(modal, className + 'open');
    },
    
    hideDefectPopupHelper : function(component, componentId, className){
        var modal = component.find(componentId);
        $A.util.addClass(modal, className+'hide');
        $A.util.removeClass(modal, className+'open');
        
    },
    
    
    
})