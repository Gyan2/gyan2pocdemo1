({
	doInit: function(component, event, helper) {
       
        
        var pageNumber = parseInt(component.get("v.PageNumber"));  
        console.log("@@@@@@@@@@@@@@@@@@"+ pageNumber);
        var pageSize = component.find("pageSize").get("v.value");
        helper.getCaseListpagination(component, pageNumber, pageSize);
       
    },
    individualCaseRecord : function(component, event, helper){
       
         var StoreCase = event.getParam("StoreCaseVar");
         console.log("Check related value  " + JSON.stringify(StoreCase));
         component.set("v.CasevarPopup",StoreCase);
         document.getElementById("backgroundClr").style.opacity = ".2";
         component.set("v.Check", true);
         
    },
    CloseCaseButton : function(component, event, helper){
        document.getElementById("backgroundClr").style.opacity = "1";
        component.set("v.Check", false);
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.find("pageSize").get("v.value");
        helper.getCaseListpagination(component, pageNumber, pageSize);
    },
    
     /*--------------------Pagination----------------------------*/
     handleNext: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        console.log("@&&&&&&&&&&&&&&&"+pageNumber);
        var pageSize = component.find("pageSize").get("v.value");
        pageNumber++;
        helper.getCaseListpagination(component, pageNumber, pageSize);
    },
     
    handlePrev: function(component, event, helper) {
        var pageNumber = component.get("v.PageNumber");  
        var pageSize = component.find("pageSize").get("v.value");
        pageNumber--;
        helper.getCaseListpagination(component, pageNumber, pageSize);
    },
     
    onSelectChange: function(component, event, helper) {
        var page = 1
        var pageSize = component.find("pageSize").get("v.value");
        helper.getCaseListpagination(component, page, pageSize);
    },
    
    /*--------------------Pagination End----------------------------*/
    
    
    
    
    ScriptLoaded : function(component, event, helper) {
	
        var action = component.get("c.getUserSession");
        action.setCallback(this, function(response)
    	{
            var state = response.getState();
            if(state === "SUCCESS") 
    		{
            	//component.set("v.sessionId", response.getReturnValue());
                $.cometd.init(
    			{
                    url: '/cometd/43.0',
		            requestHeaders: { Authorization: response.getReturnValue()},
                    appendMessageTypeToURL : false
		        });
		        $.cometd.subscribe('/topic/caseStream1', function(message) 
                {
                    
                    console.log("QQQQQQQQQQQ"+JSON.stringify(message.data.event.type));
                    if(message.data.event.type == "updated")
                    {
                        var updatedRecord = message.data.sobject;
                        var caseList = component.get("v.Cases");
                        for(var index = 0; index < caseList.length; index++)
                        {
                            if(caseList[index].Id == updatedRecord.Id)
                            {
                                caseList[index] = updatedRecord;
                            }
                        }
                        component.set("v.Cases", caseList);
                        
                    }
                    if(message.data.event.type == "created")
                    {
                        // alert("Hello");
                        if(component.get("v.PageNumber") == 1)
                        {
                            var caseList = component.get("v.Cases");
                            caseList.unshift(message.data.sobject);
                            component.set("v.Cases", caseList);
                        }
                    }
                });
            }
        });
		$A.enqueueAction(action);
	}
})