/**
 * Created by trakers on 15.05.18.
 */
({
    /*****************************************

        MAIN DATA

        red dormakaba : #e52330
        blue dormakaba: #003595

    *****************************************/
    loadCompleteQuote : function(component) {
        var action = component.get('c.auraGetAllData');
        var that = this;

        action.setParams({
            paramId : component.get('v.recordId'),
            paramQuoteFields : Array.from(new Set(component.get('v.quoteFields'))),
            paramQuoteLineItemFields : Array.from(new Set(component.get('v.quoteLineItemFields')))
        });

        action.setCallback(this,function(response) {
            if (response.getState() === "SUCCESS" && component.isValid()) {
                var returned = response.getReturnValue();
                console.log(returned);//_priceAfterDiscount
                //component.set('v.quoteLineItemFieldNames',returned.quoteLineItemFields);

                // QUOTE
                ['Subtotal','Tax','Discount','ShippingHandling'].forEach(val =>{
                    if(returned.quote.hasOwnProperty(val)){
                        returned.quote[val] = (returned.quote[val]==null||returned.quote[val]==undefined)?0:returned.quote[val];
                    }else{
                        returned.quote[val] = 0;
                    }
                });

                // TEXTS
                returned.quote._textCollection = [];
//                this.matchCollection(
//                this.createCollection(returned.quote.Languages__c,'quote',returned.quote.Id,returned.quoteTextSettings),
//                returned.quotetextCollection);

                // SECTION & Init missing fields
                returned.quote.Sections__c = this.initJson(returned.quote.Sections__c,[{'name':'Default Section','color':'#c2ccd4','discount':0,'additionalDiscount':0,'id':0}]); //{'name':'Section 1','color':'#71777b','discount':20},{'name':'Section 2','color':'rgb(201, 199, 197)','discount':0},{'name':'Section 3','color':'rgb(201, 199, 197)','discount':10}
                returned.quote.Sections__c.forEach((val,index) => {
                    if(!val.hasOwnProperty('id')){
                        val.id = index+1;
                    }
                    // discount
                    if(!val.hasOwnProperty('discount')){
                        val.discount = 0;
                    }
                    // additionalDiscount
                    if(!val.hasOwnProperty('additionalDiscount')){
                        val.additionalDiscount = 0;
                    }
                })


                // Check if the default section is there or not
                if(!returned.quote.Sections__c.find(val => val.id == 0)){
                    returned.quote.Sections__c.unshift({'name':'Default Section','color':'#c2ccd4','discount':0,'additionalDiscount':0,'id':0});
                }

                // QUOTELINE
                returned.quote.QuoteLineItems = returned.quoteLineItems || [];
                returned.quote.QuoteLineItems.forEach(val => {
                    that.initQuoteLineItem(val,returned.quote.Sections__c);
//                    var textCollection = returned.quoteLineItemTextCollections.hasOwnProperty(val.Id) ? returned.quoteLineItemTextCollections[val.Id] : [];
//                    val._textCollection = this.matchCollection(
//                        this.createCollection(returned.quote.Languages__c,'quoteLineItem',val.Id,returned.quoteLineItemTextSettings),
//                        textCollection
//                    );
                    // For default section (If the section doesn't exit, we put the quoteLineItem in the default section)
                    if(!returned.quote.Sections__c.find(section => section.id == val.Section__c) && val.Section__c != 0){
                        val.Section__c = 0;
                        val._changed = true;
                    }
                })


                component.set('v.quote',returned.quote);
            } else {
                console.log(response.getError());
            }
            component.set('v.loading',false);


        });

        $A.enqueueAction(action);
    },
    getPrice:function(component){
        var action = component.get('c.auraGetPrice');

            action.setParams({
                quoteId:component.get('v.quote.Id'),
                quoteLineItems:component.get('v.quote.QuoteLineItems')
            });

            console.log({
                quoteId:component.get('v.quote.Id'),
                quoteLineItems:component.get('v.quote.QuoteLineItems')
            });

        component.get('v.quote.QuoteLineItems').forEach(val => {
            console.log('Length: '+val.Product2.SAPNumber__c.length+' - '+val.Product2.SAPNumber__c);
        })

        var that = this;
        return new Promise($A.getCallback(function(resolve, reject){
            action.setCallback(that, function(response) {
                component.set('v.loading',false);
                var res = response.getReturnValue();
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                    //that.loadCompleteQuote(component);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         }))
    },
    saveQuoteLineItems : function(component) {
          var action = component.get('c.auraSaveQuoteLineItems');
          var quoteLineItems = component.get('v.quote.QuoteLineItems').filter(x=>x._changed && !x._toDelete);

          quoteLineItems.forEach(item => {
              if(item.Discount == 0){
                  item.Discount = null;
              }
              // Transform JSON to String
              item.MaterialOptions__c      = JSON.stringify(item.MaterialOptions__c);
              item.AlternativeProducts__c  = JSON.stringify(item.AlternativeProducts__c);
              console.log('SaveQLI - Item');
              console.log(item.Section__c)
              console.log(item.AdditionalInformation__c)
          });

          console.log(quoteLineItems);

          action.setParams({
              'quoteLineItems' : quoteLineItems
          });


          var that = this;

          return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {
                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
          }))
    },
    saveQuote:function(component){
        var action = component.get('c.auraSaveQuote');
        var quote = component.get('v.quote')

            quote.Sections__c = JSON.stringify(quote.Sections__c);

        console.log(quote);

        action.setParams({
             'quote' : quote
        });


        var that = this;

        return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {
                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
        }))
    },
    saveTextCollections:function(component){
        // Quote
        var action = component.get('c.auraSaveTextCollection');

        var values = [];
        var textCollection = component.get('v.quote')._textCollection || [];
            textCollection.forEach(text => {
                values = values.concat(text.values);
            })

            component.get('v.quote.QuoteLineItems').forEach(val => {
                var tempCollection = val._textCollection || [];
                tempCollection.forEach(text => {
                    values = values.concat(text.values);
                })
            })


        values = values.filter(val => val._changed); // We only take the text that has Changed
        console.log('SaveTextCollections');
        console.log(JSON.parse(JSON.stringify(values)));


        //_edit
        action.setParams({
             'textCollection' : JSON.stringify(values)
        });


        var that = this;

        return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {
                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
        }))
    },
    deleteAllQuoteLineItems : function(component){
          var action = component.get('c.auraDeleteQuoteLineItems');
          var quoteLineItems = component.get('v.quote.QuoteLineItems').filter(x=>x._toDelete);

          console.log(quoteLineItems);

          action.setParams({
              'quoteLineItems' : quoteLineItems
          });

          var that = this;

          return new Promise($A.getCallback(function(resolve, reject){
              action.setCallback(that, function(response) {
                  var res = response.getReturnValue();
                   if(response.getState() === "SUCCESS") {
                      resolve(res);
                   }else{
                      reject(res);
                   }
              });
              $A.enqueueAction(action);
          }))
    },

    /*****************************************

        Options,Alternative and Products

    *****************************************/
    createOptions:function(component,optionIds){
                var action = component.get('c.auraCreateOptions');

                var that = this;
                action.setParams({
                    salesOrg : component.get('v.quote.SalesOrg__c'),
                    optionIds  : optionIds,
                    quoteLineItemId  :  component.get('v.selectedDetail.Id')
                });

                console.log('createOptions')
                console.log({
                    salesOrg : component.get('v.quote.SalesOrg__c'),
                    optionIds  : optionIds,
                    quoteLineItemId  :  component.get('v.selectedDetail.Id')
                });

                return new Promise(function(resolve, reject){
                    action.setCallback(that, function(response) {
                        var res = response.getReturnValue();
                        console.log(res);
                         if(response.getState() === "SUCCESS") {
                            resolve(res);
                         }else{
                            reject(res);
                         }
                    });
                    $A.enqueueAction(action);
                 })
    },
    updateOptions:function(component,materialOptions){
        var action = component.get('c.auraUpdateOptions');

        var options = JSON.parse(JSON.stringify(materialOptions));
        options.forEach(val => {
            delete val._tree;
        })


        var that = this;
        action.setParams({
            quoteLineItemId     :  component.get('v.selectedDetail.Id'),
            options             :  JSON.stringify(options)
        });

        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                console.log(res);
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    },
    upsertAlternative:function(component,alternativeProducts){
        var action = component.get('c.auraUpsertAlternatives');

        var that = this;
        action.setParams({
            quoteLineItemId             :  component.get('v.selectedDetail.Id'),
            alternativeProducts         :  JSON.stringify(alternativeProducts)
        });

        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                console.log(res);
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    },
    /*****************************************

        Layout,Sortable,etc

    *****************************************/


    sorTableLayoutEditor:function(component,event){

        var type_origin,type_target;
        var position_origin,position_target;

        $('.editLayout').sortable({
               connectWith: ".editLayout",
               items: "> div:not(.ui-state-disabled)",
               update: function(event,ui) {
                    console.log('UPDATE');
                    position_target = ui.item.index();
                    type_target = ui.item.parent().data('type');
                    console.log('Target: '+JSON.stringify({type_target:type_target,position_target:position_target}))
                    var columns = component.get('v.detail_Layout');
                    if($(event.target).data('type') == type_target){
                        // SORTING

                        if(type_origin == type_target){
                            // we sort
                            var target = columns[type_target][position_target];
                            columns[type_target][position_target] = columns[type_origin][position_origin];
                            columns[type_origin][position_origin] = target;
                        }else{
                            var removed = columns[type_origin].splice(position_origin,1);
                            if(type_target == 'displayed'){
                                removed[0] = {columns:'1',field:removed[0]};
                            }else{
                                removed[0] = removed[0].field;
                            }
                            console.log('Removed:'+JSON.stringify(removed[0]));
                            columns[type_target].splice(position_target, 0,removed[0]);
                        }


                        console.log('COLUMNS')
                        console.log(JSON.parse(JSON.stringify(columns)));
                        component.set('v.detail_Layout',columns);

                    }

               },
               start:function(event,ui){
                   $(ui.helper).css('width',$('#availableTitle').width());
                   type_origin = ui.item.parent().data('type');
                   position_origin = type_origin == 'available' ? (ui.item.index() - 1) :  ui.item.index();
                   console.log('Origin: '+JSON.stringify({type_origin:type_origin,position_origin:position_origin}))
               }
        }).disableSelection();

    },
    sortableQuoteLineItems:function(component){
        var that = this;


        $('.quoteLineItemsTbody').sortable({
             connectWith:'.quoteLineItemsTbody',
             handle:'.handle',
             update: function(event,ui) {
                 console.log('UPDATE QUOTELINEITEMS');
                 that.recalculatePositions(component);
                $(this).sortable("destroy");
             },
             placeholder:'ui-state-highlight',

        })

    },
    sortableSections:function(component){
            var that = this;

             $('#quoteLineItemsTable').sortable({
                 handle:'.handle',
                 update: function(event,ui) {
                     console.log('UPDATE SECTION');
                     that.recalculatePositions(component);
                   $('#quoteLineItemsTable').sortable("destroy");
                 },
                 placeholder:'ui-state-highlight',
                 start:function(){
                     $('.ui-state-highlight tr').first().height("200px");
                 }
             })

        },

    /****************************************

            UTILS

    *****************************************/
    initJson:function(value,replacement){
        //console.log(value);
        if(value !== null && value !== undefined && value.length > 0 && (value[0] == '{' || value[0] == '[')){
            return JSON.parse(value);
        }else{
            return replacement;
        }
    },
    generateConnectorOptions:function(component){

        var options = component.get('v.selectedDetail');
            options = options.hasOwnProperty('MaterialOptions__c') ? options.MaterialOptions__c : [];

        var previous = null;
        for(var i = options.length - 1; i >= 0; i--){
            if(previous == null){
                options[i]._tree = options[i].selected ? 2:0;
            }else{
                if(previous == 0){
                    options[i]._tree = options[i].selected ? 2:0;
                }else{
                    options[i]._tree = options[i].selected ? 3:1;
                }
            }

            previous = options[i]._tree;
        }

        component.set('v.selectedDetail',component.get('v.selectedDetail'))

    },
    getSObjectId:function(obj,type){
        var classes;
        switch(type){
            case 'event':
                    console.log(obj.currentTarget);
                    if(typeof obj.getSource !== 'undefined'){
                        classes = obj.getSource().get('v.class').split(' ');
                    }else{
                        classes = obj.currentTarget.className.split(' ');
                    }
            break;
            case 'element':
                    classes = obj.className.split(' ');
            break;
        }


        return classes.find(x=>x.indexOf('data-sobject-id-')!=-1).replace('data-sobject-id-','');
    },
    recalculatePositions : function(component) {
        var that = this;
        var quoteLineItems = component.get('v.quote.QuoteLineItems');
        var sections = component.get('v.quote.Sections__c');
        var newQuoteLineItems = [];
        var newSections = [];

        // Sections
        $('.quoteLineItemsTbody').each(function(i){
            var section = $(this).find('tr.quoteSection').data('section');
            newSections.push(sections[section-1]);
            // Rows
            $(this).find('tr:not(.quoteSection)').each(function(j){

                var quoteLineItemId = that.getSObjectId(this,'element');
                var quoteLineItem   = quoteLineItems.find(val => val.Id == quoteLineItemId);
                if(quoteLineItem.Position__c != (j+1)){
                    quoteLineItem._changed = true;
                    quoteLineItem.Position__c = (j+1);
                }
                if(quoteLineItem.Section__c != (i+1)){
                    quoteLineItem._changed = true;
                    quoteLineItem.Section__c = (i+1);
                }

                newQuoteLineItems.push(quoteLineItem);
            })

        })
        component.set('v.quote.Sections__c',newSections);
        component.set('v.quote.QuoteLineItems',newQuoteLineItems);



    },
    resizeListener:function(component){
        $( window ).resize(function() {
          console.log('Resize')
          console.log($('#quoteLineItemsContainer').length);
          console.log($('#quoteLineItemsContainer').width());
          $('#quoteLineItemsTable').width($('#quoteLineItemsContainer').width()+'px');
        });
    },
    generateQuoteLineItems:function(selected,component){
         var action = component.get('c.auraGenerateQuoteLineItems');
             action.setParams({
                 selected       :   selected,
                 salesOrgId     :   component.get('v.quote.SalesOrg__c'),
                 quoteId        :   component.get('v.quote.Id')
             });


         var that = this;
         return new Promise(function(resolve, reject){
             action.setCallback(that, function(response) {
                 var res = response.getReturnValue();
                 console.log('Get list of quoteLineItem');
                 console.log(res);
                  if(response.getState() === "SUCCESS") {
                     resolve(res);
                  }else{
                     reject(res);
                  }
             });
             $A.enqueueAction(action);
          })
    },
    /***** NOMENCLATURES *****/
    nomenclature_addNomenclature:function(component){
        var nomenclatures = component.get('v.nomenclatures');
        var query = component.find('searchNomenclature').get('v.value');
        var externalId = Date.now()+'-'+Math.floor((Math.random() * 1000) + 1);

        // Add new nomenclature that gonna auto load through SAP
        nomenclatures.push(
            {
                name:query,
                product:null,
                quantity:1,
                loading:false,
                externalId:externalId,
                error:false
            });
        component.set('v.nomenclatures',nomenclatures);
        component.set('v.nomenclature_isLoading',true);
        component.find('searchNomenclature').set('v.value',null);
        // Load the product from SAP
        this.nomenclature_loadProduct(component,externalId,query);
    },
    nomenclature_loadProduct:function(component,externalId,query){
        console.log('Load');

        var nomenclature = component.get('v.nomenclatures').find(val => val.externalId == externalId)
            nomenclature.loading = true;
        component.set('v.nomenclatures',component.get('v.nomenclatures'));


        // loading data from SAP here and save them
        window.setTimeout(function(){
            var test = ['9Klock','lock','9K37AB15DS3690'];
            nomenclature = component.get('v.nomenclatures').find(val => val.externalId == externalId);

            if(test.find(val => val ==  query)){
                nomenclature.product = {'SAPNumber__c':'000000000004000060','Name':'9K Lock Series','Id':'01t4E000002bG9KQAU'};
            }else{
                nomenclature.error = true;
            }

            // Stop loading
            nomenclature.loading = false;

            component.set('v.nomenclatures',component.get('v.nomenclatures'));
            component.set('v.nomenclature_isLoading',component.get('v.nomenclatures').filter(val => val.loading == true).length > 0);
        },2000);

    },
    findParentNode:function(el, className) {
            // loop up until parent element is found
            while (el && !el.classList.contains(className) ) {
                console.log(el);
                el = el.parentNode;
            }
            // return found element
            return el;
    },
    /*** SCROLLABLE FIXED ****/
    scrollableFixed:function(component){

        document.getElementById('appContainer').addEventListener('scroll', function() {
             var detailContainer = document.getElementById('detailContainer');
             var scrollableContainer = document.getElementById('scrollableContainer');
             var dropZone = document.getElementById('dropzone');


             if(component.get('v.detail_show')){
                 var positionTop = detailContainer.getBoundingClientRect().top + document.body.scrollTop;
                 //console.log(positionTop);
                 //console.log(dropZone.classList.contains('fixedTable'));
                 // set only if it's not fixed yet and the position is <= 0
                 if (positionTop < 0) {

                      if(!dropZone.classList.contains('fixedTable')){
                          //console.log('Change');
                          var top = component.get('v.headerHeight');

                          // List of items
                          dropZone.classList.add("fixedTable");
                          if(component.get('v.includeHeader')){
                            dropZone.classList.add("includeHeader");
                          }
                          scrollableContainer.style.top = top+'px';

                      }

                 } else  if (dropZone.classList.contains('fixedTable')){
                     // reset only if it was fixed before
                     dropZone.classList.remove("fixedTable");
                     dropZone.classList.remove("includeHeader");

                 }
             }


        });
    },
    /***** TEXTS ******/
    createCollection:function(languages,recordType,recordId,collection){
        var texts = [];
        languages = languages == null ? []: languages.split(';');
        for(var key in collection){
            var text = {'SAPKey':key,'name':collection[key],'recordType':recordType,'recordId':recordId,'values':[]};
            languages.forEach(lang => {
                text.values.push({'key':lang,'value':null,'externalId':key+'-'+lang+'-'+recordId});
            })
            texts.push(text);
        }

        return texts;
    },
    matchCollection:function(newCollection,textMapping){
        newCollection.forEach(val => {
            val.values.forEach(text => {
               if(textMapping.find(item => item.ExternalId__c == text.externalId)){
                   var temp = textMapping.find(item => item.ExternalId__c == text.externalId);
                   text.value = temp.Text__c;
               }
            })
        })

        return newCollection;
    }



})