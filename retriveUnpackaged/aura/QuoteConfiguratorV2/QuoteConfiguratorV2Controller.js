/**
 * Created by trakers on 15.05.18.
 */
({
    /** GENERAL METHODS **/
    handleInit : function(component, event, helper) {
        component.set('v.detail_Layout',helper.cookie_loadLayout(component));
        component.set('v.tabSettings_columns',helper.cookie_loadColumns(component));
        // Load the quote
        helper.loadCompleteQuote(component,helper);
    },
    scriptsLoaded:function(component,event,helper){
        window.setTimeout(function(){
           //helper.sortableSections(component);
           //helper.sortableQuoteLineItems(component);
           //helper.tabSettings_sortTable(component,event);
           $('#quoteLineItemsTable').width($('#quoteLineItemsContainer').width()+'px');
           helper.scrollableFixed(component);
           component.set('v.headerHeight',$('#appContainer').offset().top)
           window.setTimeout(function(){
               // For sandboxes
               component.set('v.headerHeight',$('#appContainer').offset().top)
           },3000);
        },1000);

        helper.resizeListener();
    },
    saveAllChanges:function(component,event,helper){
        component.set('v.loading',true);
        Promise.all([helper.saveQuote(component),helper.saveQuoteLineItems(component),helper.saveTextCollections(component),helper.deleteAllQuoteLineItems(component)])
        .then($A.getCallback(values => {
            console.log('Result of SaveAllChanges')
            console.log({'Quote':values[0],'QuoteLineItems':values[1],'TextCollections':values[2],'Delete':values[3]});
            helper.loadCompleteQuote(component);
            component.set('v.saveButton_show',false);
            //component.set('v.loading',false);
        }))
    },
    getPrice:function(component,event,helper){
        component.set('v.loading',true);
        helper.getPrice(component)
        .then($A.getCallback(val => {
                    console.log(val);
                    if(val.status == 400){
                        component.set('v.global_errorMessage',val.message);
                    }
                    helper.loadCompleteQuote(component);

        })).catch(e => {
                console.log(e);
                component.set('v.loading',false);
                component.set('v.global_errorMessage',e.message);
                
        })

    },
    addProducts:function(component,event,helper){

          component.set('v.selectorSection',0);
          component.set('v.selectorType','advanced');
          component.set('v.selectorMode','product');
          component.set('v.selectorExclude',component.get('v.quote.QuoteLineItems').filter(val => val.Section__c != 0).map(val => val.Product2Id));
          component.set('v.displaySelector',true);
          component.set('v.selectorTitle','Add products in : Default Section');
    },
    /*** EVENT HANDLERS ****/

    detail_handleShow:function(component,event,helper){
        console.log('handledetail_show');
        if(component.get('v.detail_show')){
            var table = document.getElementById('quoteLineItemsTable');
                        table.style.width = '100%';
        }else{
            var table = document.getElementById('quoteLineItemsTable');
            var container = document.getElementById('quoteLineItemsContainer');
            table.style.width = container.style.width+'px';
        }

    },
    handleQuoteLineItemsChanges:function(component,event,helper){
         console.log(JSON.parse(JSON.stringify(component.get('v.quote'))));
         if(!component.get('v.saveButton_show') && component.get('v.quote.QuoteLineItems').filter(val => val._changed == true).length > 0){
             console.log('handleQuoteLineItemsChanges');
             component.set('v.saveButton_show',true)
         }
    },
    handleQuoteConfiguratorEvent:function(component,event,helper){
        //TODO : this is the application event, it's used to refresh the quote and to show the save button if something has changed
        console.log('handleQuoteConfiguratorEvent');
        switch(event.getParam('type')){
            case 'refresh':
                console.log('Event - refresh')
                component.get('v.loading',true);
                helper.loadCompleteQuote(component,helper);
            break;

            case 'onChangeQLI':
                console.log('Event - onChangeQLI')
                if(!component.get('v.saveButton_show')){
                    component.set('v.saveButton_show',true);
                }
            break;
        }

    },
    handleSelectionOfProduct:function(component,event,helper){
            var selectorResult  = component.get('v.selectorResult');
            var mode    = component.get('v.selectorMode');

            // If result are null, we cancel
            if(selectorResult == null){
                return;
            }

            switch(mode){

                case 'option':
                    helper.createOptions(component,selectorResult.result)
                    .then($A.getCallback(val => {
                        if(val.status == 200){

                            // Set the new values
                            var detailId = component.get('v.selectedDetail.Id');
                            var quote = component.get('v.quote');
                                quote.QuoteLineItems.find(val => val.Id == detailId).MaterialOptions__c = val.data;
                            // Update
                            component.set('v.selectedDetail',quote.QuoteLineItems.find(val => val.Id == detailId));
                            component.set('v.quote',quote);
                        }else{
                            component.set('v.global_errorMessage',val.message);
                        }
                    }))


                break;

                case 'replacement':
                    console.log('selectorResult')
                    var detailId = component.get('v.selectedDetail.Id');
                    var quote = component.get('v.quote');
                    var products = quote.QuoteLineItems.find(val => val.Id == detailId).AlternativeProducts__c;

                        quote.QuoteLineItems.find(val => val.Id == detailId).AlternativeProducts__c = products.concat(selectorResult.result);
                        //quote.QuoteLineItems.find(val => val.Id == detailId).AlternativeProducts__c.concat(['hello the world']);
                    console.log(JSON.stringify(quote.QuoteLineItems.find(val => val.Id == detailId).AlternativeProducts__c));
                    component.set('v.selectedDetail',quote.QuoteLineItems.find(val => val.Id == detailId));
                    component.set('v.quote',quote);
                    // Upsert
                    helper.upsertAlternative(component,quote.QuoteLineItems.find(val => val.Id == detailId).AlternativeProducts__c)
                    .then($A.getCallback(val => {
                        console.log(val);
                    }))


                break;

                case 'product':
                    component.set('v.loading',true);
                    var quoteLineItems = selectorResult.result;
                    var positionStart = component.get('v.quote.QuoteLineItems').filter(val => val.Section__c == component.get('v.selectorSection')).length;
                        positionStart = positionStart + 1; // We need to add 1 more

                    quoteLineItems.forEach(val => {
                        val.Section__c          = component.get('v.selectorSection');
                        val.Position__c         = positionStart;
                        positionStart++;
                    })


                     // Upsert
                    var productMapping = {};

                    // From Abstract
                    helper.insertQuoteLineItems(component,quoteLineItems,component.get('v.quoteLineItemFields'))
                    .then($A.getCallback(val => {
                        if(val.status == 200){
                            var quote = component.get('v.quote');

                                val.data.forEach(item => {
                                    helper.initQuoteLineItem(item,quote.Sections__c);
                                })

                                quote.QuoteLineItems = quote.QuoteLineItems.concat(val.data);
                            component.set('v.quote',quote);
                        }else{
                            // Show error message
                            component.set('v.global_errorMessage',val.message);
                        }

                        component.set('v.loading',false);
                    }))


                break;

            }



    },
    /**** DISPLAY ****/
    detail_show:function(component,event,helper){
        console.log('show details');
        var quote = component.get('v.quote');
        var quoteLineItemId = event.getSource().get('v.value');

        quote.QuoteLineItems.find(val => val.Id == quoteLineItemId)._replacement = true;
        //quote.QuoteLineItems.find(val => val.Id == quoteLineItemId).SAPPrice__c = quote.QuoteLineItems.find(val => val.Id == quoteLineItemId).SAPPrice__c+11;
        //quote.QuoteLineItems.pop();
        component.set('v.quote',quote);

        component.set('v.selectedDetail',quote.QuoteLineItems.find(val => val.Id == quoteLineItemId));
        // Generate Connectors
        helper.generateConnectorOptions(component);
        component.set('v.detail_show',true);
    },
    detail_backToListview:function(component,event,helper){
          document.getElementById('dropzone').classList.remove("fixedTable");
          document.getElementById('dropzone').classList.remove("includeHeader");
          component.set('v.detail_show',false);
    },
    main_handleMenuSelection:function(component,event,helper){
        var values = event.getParam('value');
        switch(values){

            case 'Section':
                component.set('v.section_modalDisplayEdit',true);
            break;

            case 'Print Quote':
                console.log('Print quote')
                component.set('v.quote.Sections__c',component.get('v.quote.Sections__c'));
            break;

            case 'Favorite':
                console.log(JSON.parse(JSON.stringify(component.get('v.quote'))));
                /*
                component.set('v.selectedDetail',component.get('v.quote').QuoteLineItems[0]);
                component.set('v.detail_show',true);
                window.setTimeout(function(){
                    document.getElementById('yourDivID').scrollIntoView(); //todo: scrollTo
                },100);
                */

            break;

            case 'Nomenclature':
                component.set('v.selectorSection',0);
                component.set('v.nomenclature_displayModal',true);
            break;
        }
    },
//    section_display:function(component,event,helper){
//        component.set('v.section_modalDisplayEdit',true);
//    },
    tabSettings_display:function(component,event,helper){
        component.set('v.tabSettings_display',true);

    },
    /**** TAB SETTINGS **/
    tabSettings_scrollableHandler:function(component,event,helper){
        var action = event.getSource().get('v.value');
        var index = component.get('v.tabSettings_scrollableIndex');
        var maxIndex = component.get('v.tabSettings_columns.scrollable').length - 1;

        switch(action){
            case 'left':
                index = index > 0 ? index-1 : 0;
            break;

            case 'right':
                index = index < maxIndex  ? index+1 : maxIndex;
            break;
        }

        component.set('v.tabSettings_scrollableIndex',index);
    },
    /**** MENU ITEMS ***/
    handleMenuSelectQLI:function(component,event,helper){
          var values = event.getParam('value').split('-');
          var action = values[0];
          var id     = values[1];

           switch(action){
               case 'delete':
                    component.get('v.quote.QuoteLineItems').find(val => val.Id == id)._toDelete = true;
                    component.set('v.quote.QuoteLineItems',component.get('v.quote.QuoteLineItems'));

//                    helper.deleteAllQuoteLineItems(component)
//                    .then($A.getCallback(val => {
//                        console.log(val);
//                        if(val.status == 200){
//
//                        }else{
//                            component.set('v.global_errorMessage',val.message);
//                        }
//                    }))
               break;
           }
    },
    section_handleMenuSelection:function(component,event,helper){
      var values = event.getParam('value').split('-');
      var action = values[0];
      var index  = parseInt(values[1]);
      console.log('--------- section_handleMenuSelection ---------');
      console.log(values);

      switch(action){
          case 'settings':
                component.set('v.section_selected',component.get('v.quote.Sections__c').find(val => val.id == index));
                component.set('v.section_modalDisplayEdit',true);
          break;

          case 'add':
                console.log(component.get('v.quote.Sections__c').find(val => val.id == index));
                component.set('v.selectorSection',index);
                component.set('v.selectorType','advanced');
                component.set('v.selectorMode','product');
                component.set('v.selectorExclude',component.get('v.quote.QuoteLineItems').filter(val => val.Section__c != index).map(val => val.Product2Id));
                component.set('v.displaySelector',true);
                component.set('v.selectorTitle','Add products in : '+component.get('v.quote.Sections__c').find(val => val.id == index).name);
          break;

          case 'nomenclature':
                component.set('v.selectorSection',index);
                component.set('v.nomenclature_displayModal',true);
          break;

          case 'delete':
                //TODO delete the quoteLineItems linked to this section
                component.set('v.quote.Sections__c',component.get('v.quote.Sections__c').filter(val => val.id != index));

                // delete the quoteLineItems
                window.setTimeout($A.getCallback(function(){
                    component.get('v.quote.QuoteLineItems')
                    .filter(val => val.Section__c == index)
                    .forEach(val => {
                        val._toDelete = true;
                        val.Section__c = 0;
                    });
                    component.set('v.quote.QuoteLineItems',component.get('v.quote.QuoteLineItems'));
                }),20);

          break;
      }
    },
    detail_handleMenuSelection: function (component,event,helper) {
        // This will contain the string of the "value" attribute of the selected
        // lightning:menuItem
        var selectedMenuItemValue = event.getParam("value");
        switch(selectedMenuItemValue){
            case 'editLayout':
                console.log('editLayout');
                component.set('v.detail_EditMode',true);
                component.set('v.detail_LayoutCopy',JSON.parse(JSON.stringify(component.get('v.detail_Layout'))));
                window.setTimeout(function(){
                    helper.sorTableLayoutEditor(component,event);
                },500)

            break;
        }
    },
    detail_handleChange:function(component,event,helper){
        if(component.get('v.selectedDetail') != null){
            console.log('selectedDetail has changed');
            var selected = component.get('v.selectedDetail');
            var quote = component.get('v.quote');

            quote.QuoteLineItems
            .filter(val => val.Id == selected.Id)
            .forEach(val => {
                val = selected;
            });

            component.set('v.quote',quote);

        }
    },
    /****** DETAIL ****/

    detail_addOptions:function(component,event,helper){
            component.set('v.selectorType','simple');
            component.set('v.selectorExclude',component.get('v.selectedDetail.MaterialOptions__c').map(val => val.id));
            component.set('v.selectorMode','option');
            component.set('v.displaySelector',true);
            component.set('v.selectorTitle','Add options');
            component.set('v.detail_optionsIsOpen',true);
    },
    detail_addReplacements:function(component,event,helper){
            component.set('v.selectorType','advanced');
            component.set('v.selectorMode','replacement');
            var exclude = [];
            exclude = exclude.concat(component.get('v.selectedDetail.AlternativeProducts__c').map(val => val.PricebookEntry.Product2Id));
            exclude = exclude.concat(component.get('v.selectedDetail.Product2.Id'));

            component.set('v.selectorExclude',exclude);
            component.set('v.displaySelector',true);
            component.set('v.selectorTitle','Add alternative products');
            component.set('v.detail_alternativesIsOpen',true);
    },
    detail_cancel:function(component,event,helper){
        var id = component.get('v.selectedDetail.id');
        component.set('v.selectedDetail',component.get('v.quote.QuoteLineItems').find(val => val.id == id));
        component.set('v.detail_show',false);
    },
    detail_save:function(component,event,helper){
        component.set('v.detail_show',false);
    },
    /****** DETAIL - LAYOUT **/
    layout_save:function(component,event,helper){

        component.set('v.detail_EditMode',false);
        helper.cookie_saveLayout(component);

    },
    layout_cancel:function(component,event,helper){
        component.set('v.detail_Layout',JSON.parse(JSON.stringify(component.get('v.detail_LayoutCopy')))); // Reset to the previous state
        component.set('v.detail_EditMode',false);
    },
    /****** SECTIONS ******/
    goToConfigurator:function(component,event,helper){
        var id = event.getSource().get('v.value');

        component.set('v.selectedDetail',component.get('v.quote.QuoteLineItems').find(val => val.Id == id));
        component.set('v.detail_show',true);
    },
    goToConfiguratorIGS:function(component,event,helper){
        var id = event.getSource().get('v.value');

        component.set('v.selectedDetail',component.get('v.quote.QuoteLineItems').find(val => val.Id == id));
        component.set('v.detail_show',true);
        component.set('v.canvas_display',true);
    },
    onClickExpandableSection:function(component,event,helper){
        var source = event.currentTarget;
        console.log(source);
        var t = component.find('iconExpandableSection');
        console.log(t.length);
    },
    handleSelectedTabChange:function(component,event,helper){
        console.log('Has changed');
    },
    startDraggingSection:function(component,event,helper){
        console.log('Start Dragging Sections');
        helper.sortableSections(component);
    },
    startDraggingQuoteLineItems:function(component,event,helper){
        console.log('Start Dragging QuoteLineItems');
        helper.sortableQuoteLineItems(component);
    },
    redirectToSObject:function(component,event,helper){
             var element = event.currentTarget; // Same than redirectToSobject without ParentNode
             var SObjectId = helper.getSObjectId(element,'element');
             var navEvt = $A.get("e.force:navigateToSObject");
                 navEvt.setParams({
                   "recordId": SObjectId,
                   "slideDevName": "related"
                 });
                 navEvt.fire();
    },
    onHoverDescription:function(component,event,helper){
        console.log('onHoverDescription')
            var quoteLineItem = component.get('v.quote.QuoteLineItems').find(x=>x.Id == helper.getSObjectId(event,'event'));
            console.log(helper.getSObjectId(event,'event'));
            console.log(quoteLineItem);
            component.set('v.quoteLineItemHovered',quoteLineItem)
            component.set('v.mouseOverRow',true);
    },
    onHoverDescriptionLeave:function(component,event,helper){
        component.set('v.mouseOverRow',false)
        setTimeout(function(){
            if(!component.get('v.mouseOverRow')){
                component.set('v.quoteLineItemHovered',null);
        }
        }, 150);
    },


    /***** NOMENCLATURES *****/


    nomenclature_handleKeyUp:function(component,event,helper){
        var isEnterKey = event.keyCode === 13;
        if (isEnterKey) {
            helper.nomenclature_addNomenclature(component);
        }
    },
    nomenclature_delete:function(component,event,helper){
        var externalId = event.getSource().get('v.value');
        component.set('v.nomenclatures',component.get('v.nomenclatures').filter(val => val.externalId != externalId));
    },
    nomenclature_onCloseClick:function(component,event,helper){
        component.set('v.nomenclature_displayModal',false);
        component.set('v.nomenclatures',[]);
    },
    nomenclature_onAddClick:function(component,event,helper){
        var products = component.get('v.nomenclatures').filter(val => val.product != null && val.error == false);
            products = products.map(val => {
                return val.product.Id;
            });
        component.set('v.loading',true);
        helper.generateQuoteLineItems(products,component)
        .then(function(quoteLineItems){

            var positionStart = component.get('v.quote.QuoteLineItems').filter(val => val.Section__c == component.get('v.selectorSection')).length;
                positionStart = positionStart + 1; // We need to add 1 more
            quoteLineItems.forEach(val => {
                val.Section__c          = component.get('v.selectorSection');
                val.Quantity            = component.get('v.nomenclatures').find(item => item.product.Id == val.Product2.Id).quantity;
                val.Position__c         = positionStart;
                val.isConfigurable__c   = true;
                val.Configuration__c    = {2:1,4:1,5:1,6:1,7:1,8:1,9:1,10:1,11:1};
                positionStart++;
            })

            // Upsert
           var productMapping = {};

           // From Abstract
           helper.insertQuoteLineItems(component,quoteLineItems,component.get('v.quoteLineItemFields'))
            .then($A.getCallback(val => {
                if(val.status == 200){
                    var quote = component.get('v.quote');
                        val.data.forEach(item => {
                            helper.initQuoteLineItem(item,quote.Sections__c);
                        })
                        quote.QuoteLineItems = quote.QuoteLineItems.concat(val.data);
                    component.set('v.quote',quote);
                }else{
                    // Show error message
                    component.set('v.global_errorMessage',val.message);
                }
                component.set('v.loading',false);
            }))

            console.log(products);
            component.set('v.nomenclature_displayModal',false);
            component.set('v.nomenclatures',[]);
        })

    },
    /**** FILE *****/
    file_dragStart:function(component,event,helper){
        console.log('file_dragStart');
        component.set('v.file_displayDropzone',true);
        window.setTimeout(function(){
            //debugger;
        },500);
    },
    file_dragHandler:function(component,event,helper){
        console.log('file_dragHandler')
    },
    file_dropHandler:function(component,event,helper){
        console.log('file_dropHandler')
        component.set('v.file_displayDropzone',false);
    },
    file_handleValueChange:function(component,event,helper){
        component.set('v.fileQLI_display',true);
    },
    /***** GLOBAL ERROR *****/
    global_errorClose:function(component,event,helper){
        component.set('v.global_errorMessage',null);
    },
    /***** SORTABLE *****/
//    sortableQLI_show:function(component,event,helper){
//        component.set('v.sortableQLI_display',true);
//    }


})