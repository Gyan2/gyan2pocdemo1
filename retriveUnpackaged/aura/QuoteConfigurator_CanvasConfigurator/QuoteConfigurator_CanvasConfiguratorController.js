({
	handleInit : function(component, event, helper) {
        component.find('canvas').set('v.parameters', JSON.stringify({
            quoteItemId : component.get('v.quoteItemId'),
            productName : component.get('v.productName')
        }));
	}
})