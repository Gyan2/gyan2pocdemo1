/**
 * Created by trakers on 20.06.18.
 */
({
    dummyAction:function(component,event,helper){
        console.log('dummy action');
    },
//    goToConfigurator:function(component,event,helper){
//        component.set('v.selectedDetail',component.get('v.quoteLineItem'));
//        component.set('v.detail_show',true);
//    },
    showDetail:function(component,event,helper){
        //$(".quoteLineItemsTbody").sortable("destroy");
        console.log('show details');
        component.set('v.selectedDetail',component.get('v.quoteLineItem'));
        component.set('v.detail_show',true);
        //helper.sortableQuoteLineItems(component);
    },
    hideDetail:function(component,event,helper){
        console.log('hide details');
        // Reset fixed Table
        $('#dropzone').removeClass("fixedTable");
        $('#dropzone').removeClass("includeHeader");
        component.set('v.detail_show',false);
        //helper.sortableQuoteLineItems(component);
    },
    redirectToSObject:function(component,event,helper){
             var element = event.currentTarget; // Same than redirectToSobject without ParentNode
             var SObjectId = helper.getSObjectId(element,'element');
             var navEvt = $A.get("e.force:navigateToSObject");
                 navEvt.setParams({
                   "recordId": SObjectId,
                   "slideDevName": "related"
                 });
                 navEvt.fire();
    },
    handleQuantityChange:function(component,event,helper){
        console.log('handleQuantityChange');
        var quoteLineItem = component.get('v.quoteLineItem');

        helper.checkChanged(quoteLineItem);

            quoteLineItem._changed = true;
        component.set('v.quoteLineItem',component.get('v.quoteLineItem'));
        //helper.fireChangedEvent();
    },
    handleDiscountChange:function(component,event,helper){
        console.log('handleDiscountChange');
        var quoteLineItem = component.get('v.quoteLineItem');

        helper.checkChanged(quoteLineItem);
        helper.handleDiscountCalculation(quoteLineItem);

        component.set('v.quoteLineItem',component.get('v.quoteLineItem'));
        //helper.fireChangedEvent();
    },
    handleAdditionalDiscountChange:function(component,event,helper){
            console.log('handleAdditionalDiscountChange');
            var quoteLineItem = component.get('v.quoteLineItem');

            helper.checkChanged(quoteLineItem);
            helper.handleDiscountCalculation(quoteLineItem);

            component.set('v.quoteLineItem',component.get('v.quoteLineItem'));
            //helper.fireChangedEvent();
        },
    handlePriceChange:function(component,event,helper){
        console.log('handlePriceChange');
        console.log('row - Price changed');
        var quoteLineItem = component.get('v.quoteLineItem');
            quoteLineItem._priceAfterDiscount = Number(event.getSource().get('v.value'));

        helper.checkChanged(quoteLineItem);
        helper.handleNetPriceChange(quoteLineItem);

         component.set('v.quoteLineItem',component.get('v.quoteLineItem'));
         //helper.fireChangedEvent();
    },
    handleMenuSelect:function(component,event,helper){
          var values = event.getParam('value').split('-');
          var action = values[0];
          var id     = values[1];

           switch(action){
               case 'delete':
                    component.get('v.quoteLineItem')._toDelete = true;
                    component.set('v.quoteLineItem',component.get('v.quoteLineItem'));

//                    helper.deleteAllQuoteLineItems(component)
//                    .then($A.getCallback(val => {
//                        console.log(val);
//                        if(val.status == 200){
//
//                        }else{
//                            component.set('v.global_errorMessage',val.message);
//                        }
//                    }))
               break;
               case 'configure':
                    component.set('v.selectedDetail',component.get('v.quoteLineItem'));
                    component.set('v.canvas_display',true);
               break;
           }
    },
    handleMenuFocus:function(component,event,helper){

    },
    handleQuoteLineItemChanges:function(component,event,helper){
        console.log('QuoteLineItem change - row')
        //helper.fireChangedEvent();
    },
    onHoverDescription:function(component,event,helper){
        console.log('onHoverDescription')
        var quoteLineItem = component.get('v.quoteLineItem');
        component.set('v.quoteLineItemHovered',quoteLineItem)
        component.set('v.mouseOverRow',true);
    },
    onHoverDescriptionLeave:function(component,event,helper){
        component.set('v.mouseOverRow',false)
        setTimeout(function(){
            if(!component.get('v.mouseOverRow')){
                component.set('v.quoteLineItemHovered',null);
            }
        }, 100);
    },
})