/**
 * Created by trakers on 20.06.18.
 */
({
    getSObjectId:function(obj,type){
        var classes;
        switch(type){
            case 'event':
                    console.log(obj.currentTarget);
                    if(typeof obj.getSource !== 'undefined'){
                        classes = obj.getSource().get('v.class').split(' ');
                    }else{
                        classes = obj.currentTarget.className.split(' ');
                    }
            break;
            case 'element':
                    classes = obj.className.split(' ');
            break;
        }


        return classes.find(x=>x.indexOf('data-sobject-id-')!=-1).replace('data-sobject-id-','');
    },
    fireChangedEvent:function(){
        var  appEvent = $A.get('e.c:QuoteConfiguratorV2_event');
             appEvent.setParams({
                 type : 'onChangeQLI'
             });
             appEvent.fire();
    },
    checkChanged:function(quoteLineItem){
        if(!quoteLineItem._changed){
            this.fireChangedEvent();
        }
    }
})