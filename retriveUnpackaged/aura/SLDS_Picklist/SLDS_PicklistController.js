({
    // Overwritten the method of the SLDS_Abstract
    doValidate:function(component,event,helper){
        console.log('doValidate');
        var isRequired = component.get('v.isRequired');
        if(isRequired && component.get('v.value') == null){
            component.set('v.displayError',true);
            component.set('v.errorMessage','This field is required');
        }

    },
    onChange:function(component,event,helper){
        console.log('onChange');
    },
    onPicklistValuesChange:function(component,event,helper){
        component.set('v.options',component.get('v.picklistValues'));
    },
    onInit:function(component,event,helper){
        console.log('onInit');
        if(component.get('v.picklistValues') && component.get('v.picklistValues').length > 0){
            console.log('picklistsValues is not empty');
            console.log(JSON.parse(JSON.stringify(component.get('v.picklistValues'))));
            component.set('v.options',component.get('v.picklistValues'));
            //console.log(JSON.parse(JSON.stringify(component.get('v.picklistValues'))));
            helper.handleValueWithLabel(component,component.get('v.value'));
            return;
        }

        console.log(' h 1');
        try{
            if(component.get('v.value') == undefined){
                component.set('v.value',null);
            }
        }catch(e){

        }

        console.log(' h 2');
        if(component.get('v.isDependent')){
            helper.getDependentOptions(component)
            .then($A.getCallback(val => {
                component.set('v.formula',val);
                helper.handleMapping(component);
                helper.handleValueWithLabel(component,component.get('v.value'));
            }))
        }else{
            console.log('helper.getPicklistValues');
            helper.getPicklistValues(component)
            .then($A.getCallback(val => {
                console.log(JSON.parse(JSON.stringify(val)));
                component.set('v.options',val);
                helper.handleValueWithLabel(component,component.get('v.value'));
            }))
        }

    },
    onControllingValueChange:function(component,event,helper){
        console.log('OnControllingValueChange');
        // Only if it's a dependent picklist
        if(component.get('v.isDependent')){
            helper.handleMapping(component);
        }

    },
    inputOnClick:function(component,event,helper){
        component.set('v.display',!component.get('v.display'));
    },
    onBlur:function(component,event,helper){
        setTimeout($A.getCallback(function(){
             component.set('v.display',false);
        }), 150)
    },
    selectOnClick:function(component,event,helper){
        component.set('v.displayError',false);
        component.set('v.errorMessage',null);

        console.log('selectOnClick');
        event.stopPropagation();
        helper.handleValueWithLabel(component,event.currentTarget.getAttribute('data-value'));
        component.set('v.display',false);
    },
    dummyAction:function(component,event,helper){
        console.log('Dummy action');
    }
})