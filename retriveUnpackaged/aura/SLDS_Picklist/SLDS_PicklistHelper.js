({
    getDependentOptions:function(component){
        var action = component.get('c.getDependentOptions');

              action.setParams({
                  'fieldName':              component.get('v.field'),
                  'controllingFieldName':   component.get('v.controllingField'),
                  'objectType':             component.get('v.sobject')
              })


              var that = this;
              return new Promise(function(resolve, reject){
                  action.setCallback(that, function(response) {
                      var res = response.getReturnValue();
                       if(response.getState() === "SUCCESS") {
                          resolve(res);
                       }else{
                          reject(res);
                       }
                  });
                  $A.enqueueAction(action);
              })
    },
    getPicklistValues:function(component){
        var action = component.get('c.getPicklistValues');

              action.setParams({
                  'fieldName':  component.get('v.field'),
                  'objectType': component.get('v.sobject')
              })


              var that = this;
              return new Promise(function(resolve, reject){
                  action.setCallback(that, function(response) {
                      var res = response.getReturnValue();
                       if(response.getState() === "SUCCESS") {
                          resolve(res);
                       }else{
                          reject(res);
                       }
                  });
                  $A.enqueueAction(action);
              })
    },
    handleMapping:function(component){
        console.log('handleMapping');
        var formula = component.get('v.formula');
        var controllingValue = component.get('v.controllingValue');

        var options;
        if(formula!= null && formula.hasOwnProperty(controllingValue)){
            options = [{active:true,defaultValue:false,label:"-- none --",value:null}].concat(formula[controllingValue]);
        }else{
            options = [{active:true,defaultValue:false,label:"-- none --",value:null}];
        }

        // Get Default value or take --none--
        var defaultValue = options.find(val => val.defaultValue == true);
        if(defaultValue){
            defaultValue = defaultValue.value;
        }else{
            defaultValue = null;
        }

        // If the value doesn't exist in the options, then we display the defaultValue
        var newValue = options.find(val => val.value === component.get('v.value'));
        if(newValue){
            newValue = newValue.value;
        }else{
            newValue = defaultValue
        }

        // Set the options
        component.set('v.options',options);
        // set the value (if no value, we display the default value)
        this.handleValueWithLabel(component,newValue);
    },
    handleValueWithLabel:function(component,value){
        console.log('handleValueWithLabel');
        var option = component.get('v.options').find(val => String(val.value) === String(value));

        if(option){
            component.set('v.value',option.value);
            component.set('v.valueLabel',option.label);
        }else{
             component.set('v.value',null);
             component.set('v.valueLabel','--None--');
        }
    },
    validate:function(component){
        var isRequired = component.get('v.isRequired');
        var errorMessage = '';
        if(isRequired && component.get('v.value') == null){
            errorMessage = 'This field is required';
        }

        component.set('v.errorMessage',errorMessage);
        component.set('v.displayError',true);
    }
})