/**
 * Created by trakers on 21.06.18.
 */
({
    insertQuoteLineItems:function(component,quoteLineItems,quoteLineItemFields){
                var that = this;
                var productMapping = {};
                var action = component.get('c.auraInsertQuoteLineItems');



                quoteLineItems.forEach(val => {
                    productMapping[val.Product2.ExternalId__c] ={
                        quantity            : val.Quantity  ?   val.Quantity:null,
                        price               : val.ListPrice ?   val.ListPrice:null,
                        section             : val.Section__c ?  val.Section__c:null,
                        position            : val.Position__c ? val.Position__c:null,
                        discount            : val.Discount ? val.Discount : 0,
                        AdditionalDiscount  : val.AdditionalDiscount__c ? val.AdditionalDiscount__c : 0,
                        isConfigurable      : val.isConfigurable__c ? val.isConfigurable__c : false,
                        configuration       : val.Configuration__c?JSON.stringify(val.Configuration__c):null
                       };
                })

                action.setParams({
                    quoteId:component.get('v.quote.Id'),
                    paramQuoteLineItemFields:Array.from(new Set(quoteLineItemFields)),
                    mapping:JSON.stringify(productMapping)
                });

                console.log({
                    quoteId:component.get('v.quote.Id'),
                    paramQuoteLineItemFields:Array.from(new Set(quoteLineItemFields)),
                    mapping:JSON.stringify(productMapping)
                });

                return new Promise($A.getCallback(function(resolve,reject){
                    action.setCallback(that, function(response) {
                        var res = response.getReturnValue();
                         if(response.getState() === "SUCCESS") {
                            resolve(res);
                         }else{
                            reject(res);
                         }
                    });
                    $A.enqueueAction(action);
                }))


    },
    cookie_saveLayout:function(component){
        window.localStorage.setItem('layout',JSON.stringify(component.get('v.detail_Layout')))
    },
    cookie_loadLayout:function(component){
        //window.localStorage.clear();
        // We only store the displayed one so when we had a new field, it's automatically added in the available
        var layout = window.localStorage.getItem('layout');
        if(layout === null){
            layout = {'available':[],'displayed':[]}
            component.get('v.detailList').forEach(val => {
                layout.displayed.push({'columns':1,'field':val});
            })
        }else{
            layout = JSON.parse(layout);
            layout.available = []; // reset the available
            // Handle the available
            component.get('v.detailList').forEach(val => {
                if(!layout.displayed.find(item => item.field.apiName == val.apiName)){
                    layout.available.push(val);
                }
            })
        }


        return layout;
    },
    cookie_saveColumns:function(component){
        window.localStorage.setItem('columns',JSON.stringify(component.get('v.tabSettings_columns')))
    },
    cookie_loadColumns:function(component){
        //window.localStorage.clear();

        var widths = component.get('v.widths');
        //return window.localStorage.get('columns') ? window.localStorage.get('columns') : ;
        var columns = window.localStorage.getItem('columns');
        var allColumns = component.get('v.allColumns');
        if(columns === null){
            columns = {'available':[]
                        ,'selected':[]
                        ,'scrollable':[
                            {'name':'CURR'},
                            {'name':'SAP NUM'},
                            {'name':'DISC'},
                            {'name':'FAMILY'},
                            {'name':'PRODUCT'},
                            {'name':'SAP INST'}]
                        };

        }else{
            columns = JSON.parse(columns);
            columns['available'] = []; // we reset the available part
        }


        // Remove from allColumns to setup the available based on the non used columns
        ['selected','scrollable'].forEach(item => {
            columns[item].forEach(val => {
                allColumns.splice(allColumns.indexOf(val.name),1);
            })
        });
        allColumns.forEach(val => {
            columns['available'].push({'name':val});
        });

        ['selected','scrollable'].forEach(item => {
            columns[item].forEach(val => {
                val.width = widths[val.name] || null;
            })
        })

        var returned = {};
            returned.fixed = [{'name':'','width':40,'colspan':1},
                              {'name':'','width':40,'colspan':1},
                              {'name':'#','width':40,'colspan':1},
                              {'name':'Description','colspan':1}];
            returned.available  = columns.available;
            returned.selected   = columns.selected;
            returned.scrollable = columns.scrollable;

        console.log('Returned');
        console.log(returned);

        return returned;
    },

    initQuoteLineItem:function(val,sections){
        //console.log(val.MaterialOptions__c);
        var section = sections.find(item => item.id == val.Section__c);

        val._disabled = (val.SAPPrice__c == undefined || val.SAPPrice__c == null);
        val.MaterialOptions__c      = this.initJson(val.MaterialOptions__c,[]);
        val.AlternativeProducts__c  = this.initJson(val.AlternativeProducts__c,[]);


        val.Discount = val.Discount || section.discount || 0;
        val.AdditionalDiscount__c = val.AdditionalDiscount__c || section.additionalDiscount || 0;
        val._priceAfterDiscount = (100 - Number(val.Discount)) * val.UnitPrice/100; // Base Discount
        val._priceAfterDiscount = (100 - Number(val.AdditionalDiscount__c)) * val._priceAfterDiscount/100; // Additional Discount
    },
    handleDiscountCalculation:function(quoteLineItem){
        // default
        quoteLineItem._changed = true;

        // Base Discount
        if(quoteLineItem.Discount > 100){
            quoteLineItem.Discount = 100;
            quoteLineItem._priceAfterDiscount = 0;
        }else if (quoteLineItem.Discount <= 0){
            quoteLineItem.Discount = 0;
            quoteLineItem.UnitPrice = quoteLineItem.SAPPrice__c;
            quoteLineItem._priceAfterDiscount = quoteLineItem.SAPPrice__c;
        }else{
            quoteLineItem.UnitPrice = quoteLineItem.SAPPrice__c;
            quoteLineItem._priceAfterDiscount = (100 - quoteLineItem.Discount)*quoteLineItem.SAPPrice__c/100;
        }

        // Additional Discount
        if(quoteLineItem.AdditionalDiscount__c > 100){
            quoteLineItem.AdditionalDiscount__c = 100;
            quoteLineItem._priceAfterDiscount = 0;
        }else if (quoteLineItem.AdditionalDiscount__c <= 0){
            quoteLineItem.AdditionalDiscount__c = 0;
        }else{
            quoteLineItem._priceAfterDiscount = (100 - quoteLineItem.AdditionalDiscount__c)*quoteLineItem._priceAfterDiscount/100;
        }
    },
    handleNetPriceChange:function(quoteLineItem){
        // default
        quoteLineItem._changed = true;

        if(quoteLineItem._priceAfterDiscount > quoteLineItem.SAPPrice__c){
            quoteLineItem.AdditionalDiscount__c = 0;
            quoteLineItem.Discount = 0;
        }else if(quoteLineItem._priceAfterDiscount <= 0){
            quoteLineItem.AdditionalDiscount__c = 0;
            quoteLineItem.Discount = 100;
            quoteLineItem._priceAfterDiscount = 0;
        }else{
            var discount = 100 - (quoteLineItem._priceAfterDiscount/quoteLineItem.SAPPrice__c*100);
            //console.log('Discount: '+this.round(discount,1));
            quoteLineItem.Discount = this.round(discount,1);
            quoteLineItem.AdditionalDiscount__c = 0;
        }


    },
    round:function(num, digits){
      var rounder = Math.pow(10,digits);
      return (Math.round(num * rounder) / rounder).toFixed(digits);
    }
})