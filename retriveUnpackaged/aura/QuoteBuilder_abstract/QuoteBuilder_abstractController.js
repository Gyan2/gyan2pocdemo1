/**
 * Created by trakers on 21.06.18.
 */
({
    /** lightning:input type number -> string so we need this **/
        parseFloat:function(component,event,helper){
            var source = event.getSource();
            source.set('v.value',parseFloat(source.get('v.value')));
            console.log('parseFloat');
            console.log(source.get('v.value'));
        },
        parseInt:function(component,event,helper){
            var source = event.getSource();
                source.set('v.value',parseInt(source.get('v.value')));
                console.log('parseInt');
                console.log(source.get('v.value'));
        },
})