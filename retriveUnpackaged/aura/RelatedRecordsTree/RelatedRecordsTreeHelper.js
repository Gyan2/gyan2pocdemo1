({
	doExpand : function(component, helper) {
        if (component.get('v.expand') == true && component.get('v._expanded') == false) {
            
            var action = component.get('c.getsObject');
            
            var sObjectIconMapping = component.get('v.sObjectIconMapping');
            var structureDefinition = component.get('v.structureDefinition');
            var structureDefinitionFields = structureDefinition.fields;
            var structureDefinitionRelationships = structureDefinition.relationships;
            var sObjectName = component.get('v.sObjectName');
            
            /*
             * String paramId, String paramSObjectName, List<String> paramRelatedListNames 
             */
            
            var relatedListNames = [];
            var paramRelated = {};
            var paramFields = {};
            for (var relatedListIndex in structureDefinition[sObjectName].relationships) {
                var forSObjectName = structureDefinition[sObjectName].relationships[relatedListIndex].sObjectName;
                relatedListNames.push(structureDefinition[sObjectName].relationships[relatedListIndex].relationship);
                
                paramRelated[structureDefinition[sObjectName].relationships[relatedListIndex].relationship] = structureDefinition[sObjectName].relationships[relatedListIndex];
                paramFields[forSObjectName] = structureDefinition[forSObjectName].fields;
            }
            paramFields[sObjectName] = structureDefinition[sObjectName].fields;
            
            console.log(paramFields);
            
            
            action.setParams({
                paramId : component.get('v.data').recordId,
                //paramSObjectName : component.get('v.sObjectName'),
                paramFields : paramFields,//structureDefinition[sObjectName].fields,
                paramRelated : paramRelated
            });
            
            action.setCallback(this,function(response){
                
                if (response.getState() === 'SUCCESS' && component.isValid()) {
                    var data = component.get('v.data');
                    
                    var result = response.getReturnValue();
                    data.Name = result.Name;
                    
                    var extraInfo = [];
                    
                    for (var i in structureDefinition[sObjectName].fields) {
                        var fieldValue = result[structureDefinition[sObjectName].fields[i]];
                        console.log('#####', fieldValue);
                        if (fieldValue !== undefined && fieldValue !== null) {
                            extraInfo.push(fieldValue);
                        }
                    }
                    
                    data._ExtraInfo = extraInfo.join(' · ');
                    //
                    
                    var componentsArray = [];
                    console.log(JSON.stringify(structureDefinition),sObjectName)
                    for (var relatedListIndex in relatedListNames) {
                        var relatedListName = structureDefinition[sObjectName].relationships[relatedListIndex].relationship;
                        console.log('relatedListName',relatedListName);
                        var relatedList = result[relatedListNames[relatedListIndex]];
                        
                        if (relatedList !== undefined) {
                            for (var i = 0; i < relatedList.totalSize; i++) {
                                console.log(relatedList.records[i]);
                                var myChild = relatedList.records[i];
                                console.log('myChild.attributes',JSON.stringify(myChild.attributes));
                                
                                var forExtraInfo = [];
                                for (var ii in structureDefinition[myChild.attributes.type].fields){
                                    var fieldValue = myChild[structureDefinition[myChild.attributes.type].fields[ii]];
                        			
                        			if (fieldValue !== undefined && fieldValue !== null) {
                            			forExtraInfo.push(fieldValue);
                                    }
                                }
                                
                                
                                var myData = {
                                    'recordId' : myChild.Id,
                                    'sObjectName' : myChild.attributes.type,
                                    'Name' : myChild.Name,
                                    '_ExtraInfo' : forExtraInfo.join (' · ')
                                };
                                
                                
                                
                                componentsArray.push([
                                    "c:RelatedRecordsTree",
                                    {
                                        "data" : myData,
                                        "sObjectName" : myData.sObjectName,
                                        "structureDefinition" : structureDefinition                                        
                                    }
                                ]);
                            }
                        }
                    }
                    
                    
                    $A.createComponents(componentsArray,function(components, status, errorMessage){
                        if (status === "SUCCESS") {
                            component.set('v._children',components);
                        }
                    })
                    component.set('v.data',data);
                    component.set('v._expanded',true);    
                }
                
            });
            
            
            $A.enqueueAction(action);
        }
	}
})