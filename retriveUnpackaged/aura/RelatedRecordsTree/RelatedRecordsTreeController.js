({
	handleInit : function(component, event, helper) {
        var design_structureDefinition = component.get('v.design_structureDefinition');
        var structureDefinition;console.log('handleInit');
        if (design_structureDefinition != undefined && design_structureDefinition != ""){
            structureDefinition = JSON.parse(design_structureDefinition)
            component.set('v.structureDefinition', structureDefinition);
            
            component.set('v.data',{
                'recordId' : component.get('v.recordId'),
                'sObjectName' : component.get('v.sObjectName'),
                'Name':'...'
            })
        }else{
            structureDefinition = component.get('v.structureDefinition');
        }
        
        if (structureDefinition == undefined || structureDefinition == null) return;
        var iconMapping = component.get('v.sObjectIconMapping');
        var sObjectName = component.get('v.sObjectName'); console.log('>>',iconMapping,sObjectName);
		component.set('v._iconName', structureDefinition[sObjectName].icon);
        if (design_structureDefinition != undefined && design_structureDefinition != null){
             component.set('v.expand', true);
        }

        component.set('v._lineColor',structureDefinition[sObjectName].color);
        helper.doExpand(component, helper);
	},
    
    handleNavigateToRecord : function(component, event, helper) {
        var navEvt = $A.get("e.force:navigateToSObject");
        navEvt.setParams({
			"recordId": event.currentTarget.dataset.navigatorId
        });
        navEvt.fire();
    },
    
    handleExpand : function(component, event, helper) {
    	helper.doExpand(component, helper);
	},
    
    handleSectionToggle : function(component, event, helper) {
        component.set('v.expand',!component.get('v.expand'));
    }
})