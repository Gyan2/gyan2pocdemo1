/**
 * Created by trakers on 17.07.18.
 */
({
    onInit:function(component,event,helper){
        console.log('On Init Text');
        var texts = component.get('v.texts') || [];

        //component.set('v.defaultTab',texts.length > 0 ? texts[0].SAPKey:null);

        helper.loadSettings(component)
        .then($A.getCallback(val => {
            var textCollection = helper.matchCollection(
                            helper.createCollection(component.get('v.languages'),component.get('v.recordType'),component.get('v.recordId'),val),
                            texts
                        );
            component.set('v.textCollectionInterne',textCollection);
        }));

    },
    setEditMode:function(component,event,helper){

        event.getSource().get('v.value')._edit = true;
        component.set('v.textCollectionInterne',component.get('v.textCollectionInterne'));
    },
    onFocus:function(component,event,helper){
        console.log(component.get('v.test'));
    },
    onSelectFirstTab:function(component,event,helper){
        component.set('v.defaultFirstTab', event.getSource().get('v.selectedTabId'));
    },
    onSelectSecondTab:function(component,event,helper){
        component.set('v.defaultSecondTab', event.getSource().get('v.selectedTabId'));
    },
    onTextCollectionInterneChange:function(component,event,helper){
        component.set('v.textCollection',component.get('v.textCollectionInterne'));
    }
})