/**
 * Created by trakers on 17.07.18.
 */
({
    loadSettings:function(component){
        var action = component.get('c.auraGetTextSettings');

            action.setParams({
                type:component.get('v.recordType')
            });

        var that = this;
        return new Promise($A.getCallback(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         }))
    },
    /*
    loadTextCollection:function(component){
        var action = component.get('c.auraGetTextCollection');

            action.setParams({
                type:component.get('v.type'),
                id:component.get('v.id')
            });

        var that = this;
        return new Promise($A.getCallback(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         }))
    },
    */
    createCollection:function(languages,recordType,recordId,collection){
        var texts = [];
        languages = languages == null ? []: languages.split(';');
        for(var key in collection){
            var text = {'SAPKey':key,'name':collection[key],'recordType':recordType,'recordId':recordId,'values':[]};
            languages.forEach(lang => {
                text.values.push({'key':lang,'value':null,'externalId':key+'-'+lang+'-'+recordId.substring(0,15)});
            })
            texts.push(text);
        }
        console.log('CreateCollection');
        console.log(texts);

        return texts;
    },
    matchCollection:function(newCollection,textMapping){
        console.log(textMapping);
        console.log(newCollection);
        newCollection.forEach(val => {
            val.values.forEach(text => {
               if(textMapping.find(item => item.AutoExternalId__c == text.externalId)){
                   var temp = textMapping.find(item => item.AutoExternalId__c == text.externalId);
                       text.value = temp.Text__c;
                       // Edit mode if it's not empty
                       if(text.value != '' && text.value != null){
                           text._edit = true;
                       }
               }
            })
        })

        return newCollection;
    }

})