/**
 * Created by trakers on 20.06.18.
 */
({
    onInit:function(component,event,helper){
        var mapping = [];
        console.log(component.get('v.quoteLineItem').Configuration__c);
        var values  =  JSON.parse(component.get('v.quoteLineItem').Configuration__c);
        console.log(values);



        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Lock Nomenclature',
                    'isRequired':false,
                    'value':null,
                    'id':'1'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'9K37AB15DS3690','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Sys Nomenclature Parameter',
                    'isRequired':false,
                    'value':null,
                    'id':'2'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Nom Message Param Lock',
                    'isRequired':false,
                    'value':null,
                    'id':'3'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'9K > Heavy Duty Lever Lockset','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Series',
                    'isRequired':false,
                    'value':null,
                    'id':'4',
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'3 > 2 - 3/4 Latch','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Backset',
                    'isRequired':false,
                    'value':null,
                    'id':'5'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'7 > 7 Pin','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Pin Size',
                    'isRequired':false,
                    'value':null,
                    'id':'6'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'AB > Corridor','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Function',
                    'isRequired':false,
                    'value':null,
                    'id':'7'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'15 > Contour W/Angle Return','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Lever Style',
                    'isRequired':false,
                    'value':null,
                    'id':'8'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'D > Round convex 3-3/8','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Rose Style',
                    'isRequired':false,
                    'value':null,
                    'id':'9'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'S3 > ANSI Strike','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Strike Style',
                    'isRequired':false,
                    'value':null,
                    'id':'10'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'690 > Dark Bronze','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Finish',
                    'isRequired':false,
                    'value':null,
                    'id':'11'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Full Radius Latch',
                    'isRequired':false,
                    'value':null,
                    'id':'12'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Schlage Large Format Lever',
                    'isRequired':false,
                    'value':null,
                    'id':'13'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Windstorm',
                    'isRequired':false,
                    'value':null,
                    'id':'14'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Lever Texture',
                    'isRequired':false,
                    'value':null,
                    'id':'15'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Lost Motion',
                    'isRequired':false,
                    'value':null,
                    'id':'16'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Lead Lined',
                    'isRequired':false,
                    'value':null,
                    'id':'17'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Alternate Thru Bolt',
                    'isRequired':false,
                    'value':null,
                    'id':'18'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Security Screw',
                    'isRequired':false,
                    'value':null,
                    'id':'19'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Door thickness',
                    'isRequired':false,
                    'value':null,
                    'id':'20'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Throw 3/4',
                    'isRequired':false,
                    'value':null,
                    'id':'21'
                    });
        mapping.push({
                    'picklistValues':[{'active':true,'defaultValue':'false','label':'Value 1','value':1},{'active':true,'defaultValue':'false','label':'Value 2','value':2},{'active':true,'defaultValue':'false','label':'Value 3','value':3}],
                    'title':'Request to Exit',
                    'isRequired':false,
                    'value':null,
                    'id':'22'
                    });

         for(var key in values){
             var temp = mapping.find(val => val.id == key);
                 temp.value = values[key];

         }

         mapping.filter(val => val.id == '2' || val.id == '3').forEach(val => {
             val.disabled = true;
         });


        console.log(mapping);
        component.set('v.mapping',mapping);
    }
})