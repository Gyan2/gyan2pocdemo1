/**
 * Created by trakers on 12.06.18.
 */
({
    showSection:function(component,event,helper){
        if(component.get('v.sectionOpened')){
            return; // we don't handle it if it's already opened
        }

        component.set('v.sectionOpened',true);
        component.set('v.selected',component.get('v.value'))
    },
    selectColor:function(component,event,helper){
        var color = event.currentTarget.getAttribute('data-color');
        component.set('v.selected',color);
    },
    validate:function(component,event,helper){
        component.set('v.value',component.get('v.selected'));
        component.set('v.sectionOpened',false);
    },
    cancel:function(component,event,helper){
         component.set('v.sectionOpened',false);
         component.set('v.selected', component.get('v.value'));
    },
    valueHasChanged:function(component,event,helper){
        console.log('Has changed');
        var cmpEvent = component.getEvent("onChangeEvent");
            cmpEvent.fire();
    },
    dummy:function(component,event,helper){
        console.log('Dummy action');
    }
})