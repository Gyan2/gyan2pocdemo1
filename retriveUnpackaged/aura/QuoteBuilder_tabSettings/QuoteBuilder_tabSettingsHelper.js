/**
 * Created by trakers on 21.06.18.
 */
({
    tabSettings_sortTable:function(component){
         var that = this;

         if($('.dropTabSettings').sortable('instance')){
             return;
         }

                var type_origin,type_target;
                var position_origin,position_target;

                $('.dropTabSettings').sortable({
                            connectWith: ".dropTabSettings",
                            items: "li:not(.ui-state-disabled)",
                            placeholder:'tabSettings_placeholder',
                            dropOnEmpty:true,
                            update: function(event,ui) {

                                position_target = ui.item.index();
                                type_target = ui.item.parentsUntil('.tabSettingsItem').data('type');

                                console.log('Position target:'+position_target);
                                console.log('Type target:'+type_target);


                                var target = ui.item.parentsUntil('.tabSettingsItem').data('type');
                                var columns  = component.get('v.tabSettings_columns');
                                var selected = component.get('v.tabSettings_selectedColumn');
                                var buffer = [].concat(columns.available).concat(columns.selected).concat(columns.scrollable);

                                //console.log(event.target.id);
                                //console.log(ui.item.parentsUntil('.tabSettingsItem').data('type'))

                                if(event.target.id == 'dropTabSettings_'+target){
                                    // SORTING

                                    if(type_origin == type_target){
                                        // we sort
                                        var target = columns[type_target][position_target];
                                        columns[type_target][position_target] = columns[type_origin][position_origin];
                                        columns[type_origin][position_origin] = target;
                                    }else{
                                        var removed = columns[type_origin].splice(position_origin,1);
                                        console.log('Removed:'+JSON.stringify(removed[0]));
                                        columns[type_target].splice(position_target, 0,removed[0]);
                                    }



                                    console.log('COLUMNS')
                                    console.log(JSON.parse(JSON.stringify(columns)));
                                    component.set('v.tabSettings_columns',columns);
                                    //component.set('v.tabSettings_columnsCopy',columns);

                                }else{
                                    // DROPING -> we don't care


                                }






                            },
                            start:function(event,ui){
                                position_origin = ui.item.index();
                                type_origin = ui.item.parentsUntil('.tabSettingsItem').data('type');

                                var columns = component.get('v.tabSettings_columns');
                                var buffer = [].concat(columns.available).concat(columns.selected).concat(columns.scrollable);
                                var selected = buffer.find(val => val.name == ui.item.find('.tabSettingsItem')[0].title);

                                var sizeEl = that.tabWidthCalculator(component);
                                var total = sizeEl.fixed+sizeEl.visible+sizeEl.scrollable;



                                console.log(sizeEl.table +'>'+ total+'+'+(selected.width?selected.width:sizeEl.defaultWidth)+' = '+(total+(selected.width?selected.width:sizeEl.defaultWidth)));
                                if(sizeEl.table > total+(selected.width?selected.width:sizeEl.defaultWidth)){
                                    console.log('Enough space to receive this element');
                                }else{
                                    console.log('Not enough space');
                                    //console.log(sizeEl.table+' - '+(total+(selected.width?selected.width:sizeEl.defaultWidth)));

                                    switch(type_origin){
                                        case 'available':
                                            // Disable the drop on visible if there is not enough space
                                            $('#dropTabSettings_selected').sortable("disable");
                                            $('#dropTabSettings_selected').css('opacity',0.5);
                                            $(this).sortable("refresh");
                                        break;

                                        case 'selected':

                                        break;

                                        case 'scrollable':
                                            // Disable the drop on visible if there is not enough space
                                            $('#dropTabSettings_selected').sortable("disable");
                                            $('#dropTabSettings_selected').css('opacity',0.5);
                                            $(this).sortable("refresh");
                                        break;
                                    }
                                }


                            },
                            stop:function(event,ui){
                                // Reset css
                                console.log('STOP')
                                $('#dropTabSettings_selected').sortable("enable");
                                //$(this).sortable("refresh");

                                $('#dropTabSettings_selected').css('opacity',1);

                            }
                          }).disableSelection();

    },
    tabWidthCalculator:function(component){
            var tableSize = $('#quoteLineItemsContainer').width();
            var columns = component.get('v.tabSettings_columns');
            var allColumns = [].concat(columns.available).concat(columns.selected).concat(columns.scrollable);
            var defaultWidth = component.get('v.tabSettings_width');
            var fixedWidth = columns.fixed.map(val => val.width?val.width:100).reduce((buffer,val) => buffer + val);
            // Columns


            var visualColumns = {};
            ['selected','scrollable'].forEach(item => {
                visualColumns[item] = [];
                $("#dropTabSettings_"+item).find('li').each(function(index){
                    var title = $(this).find('.tabSettingsItem').attr('title');
                    if(title != undefined){
                        visualColumns[item].push(allColumns.find(val => val.name == title ))
                    }
                })
            })
    //
    //        console.log('visualColumns')
    //        console.log(JSON.parse(JSON.stringify(visualColumns)));


            var visibleWidth = visualColumns.selected.length > 0 ? (visualColumns.selected.map(val => val.width?val.width:defaultWidth).reduce((buffer,val) => buffer + val)):0;
            var scrollableWidth = visualColumns.scrollable.length > 3 ? (3*defaultWidth) : visualColumns.scrollable.length * defaultWidth;
    //        console.log('Fixed: '+fixedWidth);
    //        console.log('Visible: '+visibleWidth);
    //        console.log('Scrollable: '+scrollableWidth);
            var total = fixedWidth+visibleWidth+scrollableWidth;

            return {
                'fixed':fixedWidth,
                'visible':visibleWidth,
                'scrollable':scrollableWidth,
                'table':tableSize,
                'defaultWidth':defaultWidth
            };
        },
})