/**
 * Created by trakers on 21.06.18.
 */
({  onInit:function(component,event,helper){
            component.set('v.tabSettings_columnsCopy',JSON.parse(JSON.stringify(component.get('v.tabSettings_columns'))));
            component.set('v.tabSettings_columnsOrigin',JSON.parse(JSON.stringify(component.get('v.tabSettings_columns'))));
            console.log('tab settings')
        console.log(JSON.parse(JSON.stringify(component.get('v.tabSettings_columns'))));

        window.setTimeout(function(){
            helper.tabSettings_sortTable(component);
        },500);
    },
    tabSettings_select:function(component,event,helper){
        console.log(event.currentTarget.querySelector('.tabSettingsItem').title);
        $('.slds-listbox__option').each(function(){
            $(this).removeClass('slds-is-selected');
        })
        event.currentTarget.classList.add('slds-is-selected');
        component.set('v.tabSettings_selectedColumn',event.currentTarget.querySelector('.tabSettingsItem').title)
    },
    tabSettings_move:function(component,event,helper){
        var key = event.getSource().get('v.value');
        console.log('moveTabSettings');
        console.log(key);
        var columns = component.get('v.tabSettings_columns'); // was copy before
        var selectedColumn = component.get('v.tabSettings_selectedColumn');
        if(selectedColumn == '' || selectedColumn == null){
            return;
        }

        switch(key){
            case 'available:visible':
                var from = columns.available.find(val => val.name == selectedColumn);
                if(columns.available.findIndex(val => val.name == selectedColumn) >= 0){

                    var buffer = [].concat(columns.available).concat(columns.selected).concat(columns.scrollable);
                    var selected = buffer.find(val => val.name == selectedColumn);
                    var sizeEl = helper.tabWidthCalculator(component);
                    var total = sizeEl.fixed+sizeEl.visible+sizeEl.scrollable;

                    if(sizeEl.table > total+(selected.width?selected.width:sizeEl.defaultWidth)){
                        columns.available = columns.available.filter(val => val.name != selectedColumn);
                        columns.selected.push(from);
                    }else{
                        columns.available = columns.available.filter(val => val.name != selectedColumn);
                        columns.scrollable.push(from);
                    }

                }
            break;

            case 'visible:available':
                var from = columns.selected.find(val => val.name == selectedColumn);
                if(columns.selected.findIndex(val => val.name == selectedColumn) >= 0){
                    columns.selected = columns.selected.filter(val => val.name != selectedColumn);
                    columns.available.push(from);

                }
            break;

            case 'visible:scrollable':
                var from = columns.selected.find(val => val.name == selectedColumn);
                if(columns.selected.findIndex(val => val.name == selectedColumn) >= 0){
                    columns.selected = columns.selected.filter(val => val.name != selectedColumn);
                    columns.scrollable.push(from);
                }
            break;

            case 'scrollable:visible':
                var from = columns.scrollable.find(val => val.name == selectedColumn);
                if(columns.scrollable.findIndex(val => val.name == selectedColumn) >= 0){

                    var buffer = [].concat(columns.available).concat(columns.selected).concat(columns.scrollable);
                    var selected = buffer.find(val => val.name == selectedColumn);
                    var sizeEl = helper.tabWidthCalculator(component);
                    var total = sizeEl.fixed+sizeEl.visible+sizeEl.scrollable;

                    if(sizeEl.table > total+(selected.width?selected.width:sizeEl.defaultWidth)){
                        columns.scrollable = columns.scrollable.filter(val => val.name != selectedColumn);
                        columns.selected.push(from);
                    }else{
                        columns.scrollable = columns.scrollable.filter(val => val.name != selectedColumn);
                        columns.available.push(from);
                    }

                }
            break;

            case 'visible:up':
                var origin = columns.selected.find(val => val.name == selectedColumn);
                var index = columns.selected.findIndex(val => val.name == selectedColumn);
                if(index > 0){
                    var temp = columns.selected[index];
                    columns.selected[index] = columns.selected[index-1];
                    columns.selected[index-1] = temp;
                }
            break;

            case 'visible:down':
                var origin = columns.selected.find(val => val.name == selectedColumn);
                var index = columns.selected.findIndex(val => val.name == selectedColumn);
                if(index > -1 && index < columns.selected.length - 1){
                    var temp = columns.selected[index];
                    columns.selected[index] = columns.selected[index+1];
                    columns.selected[index+1] = temp;
                }
            break;

            case 'scrollable:up':
                var origin = columns.scrollable.find(val => val.name == selectedColumn);
                var index = columns.scrollable.findIndex(val => val.name == selectedColumn);
                if(index > 0){
                    var temp = columns.scrollable[index];
                    columns.scrollable[index] = columns.scrollable[index-1];
                    columns.scrollable[index-1] = temp;
                }
            break;

            case 'scrollable:down':
                var origin = columns.scrollable.find(val => val.name == selectedColumn);
                var index = columns.scrollable.findIndex(val => val.name == selectedColumn);
                if(index > -1 && index < columns.scrollable.length - 1){
                    var temp = columns.scrollable[index];
                    columns.scrollable[index] = columns.scrollable[index+1];
                    columns.scrollable[index+1] = temp;
                }
            break;
        }

        console.log(JSON.stringify(selectedColumn));
        console.log(JSON.stringify(columns));
        component.set('v.tabSettings_columns',columns);
        component.set('v.tabSettings_columnsCopy',columns);
    },
    tabSettings_onSaveClick:function(component,event,helper){
        component.set('v.tabSettings_columns',component.get('v.tabSettings_columns'));
        helper.cookie_saveColumns(component);
        component.set('v.display',false);
    },
    tabSettings_onCloseClick:function(component,event,helper){
        component.set('v.display',false);
        component.set('v.tabSettings_columns',component.get('v.tabSettings_columnsOrigin'));
    }
})