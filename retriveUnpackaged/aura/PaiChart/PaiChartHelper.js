({
	helperMethod : function(component) {
        //console.log(JSON.stringify(component.get("v.dataSource")));
        var obj = component.get("v.dataSource");
        var config = Object.assign({}, obj);
		var dynamicId = "chart-area" + component.get("v.chartCount");
		var ctx = document.getElementById(dynamicId).getContext('2d');
		window.myPie = new Chart(ctx, config);
		
	}
})