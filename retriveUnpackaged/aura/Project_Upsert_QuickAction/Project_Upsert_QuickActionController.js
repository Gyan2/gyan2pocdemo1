/**
 * Created by trakers on 03/04/2018.
 */
({
    onInit:function(component,event,helper){
          component.set('v.project',{'sobjectType':'Project__c'});

    },
    recordLoaded:function(component,event,helper){
      console.log('RecordLoaded');
      console.log(JSON.stringify(component.get('v.project')));
    },
    onChangeField:function(component,event,helper){
        var project = component.get('v.project');
        project[event.getSource().get('v.fieldName')] = event.getSource().get('v.value');
        component.set('v.project',project);

        if(event.getSource().get('v.fieldName') == 'BuildingRef__c' && event.getSource().get('v.value') != null){
            helper.getBuildingAddress(component)
            .then(val => {
                console.log('New Building Address');
                console.log(event.getSource().get('v.value'));
                console.log(val);
                project.Country__c          = val.Country__c;
                project.CountryIsoCode__c   = val.CountryIsoCode__c;
                project.City__c             = val.City__c;
                project.Street__c           = val.Street__c
                project.Zip__c              = val.Zip__c;
                component.set('v.project',project);
            })
        }
    },
    closeClick:function(component,event,helper){
       var redirectEvent = $A.get("e.force:navigateToObjectHome");
           redirectEvent.setParams({
               "scope": "Project__c"
           });
           redirectEvent.fire();
    },
    createClick:function(component,event,helper){
        event.preventDefault();
        helper.insertProject(component)
        .then(val => {
            if(val.status == 200){
                 var navEvt = $A.get("e.force:navigateToSObject");
                     navEvt.setParams({
                       "recordId": val.message,
                       "slideDevName": "related"
                     });
                     navEvt.fire();
            }else{
                // Display the error
                console.log(val);
                component.set('v.displayError',true);
                component.set('v.errorMessage',val.messageToDisplay);
            }
        })
    },
    updateClick:function(component,event,helper){
        event.preventDefault();
        helper.updateProject(component)
        .then(val => {
            if(val.status == 200){
                 var navEvt = $A.get("e.force:navigateToSObject");
                     navEvt.setParams({
                       "recordId": val.message,
                       "slideDevName": "related"
                     });
                     navEvt.fire();
            }else{
                // Display the error
                console.log(val);
                component.set('v.displayError',true);
                component.set('v.errorMessage',val.messageToDisplay);
            }
        })
    },
    previousClick:function(component,event,helper){
        component.set('v.displayMap',false);
        switch(component.get('v.step')){
            case '3' :
                if(component.get('v.projectList').length > 0){
                     component.set('v.step','2');
                     component.set('v.displayMap',true);
                }else{
                    component.set('v.step','1');
                }
            break;

            default:
                var step = parseInt(component.get('v.step'))-1;
                component.set('v.step',step.toString());
            break;
        }
    },
    nextClick:function(component,event,helper){
        component.set('v.displayMap',false);
        switch(component.get('v.step')){
            case '1':

                if(component.get('v.projectList').length > 0){
                    component.set('v.displayMap',true);
                    var appEvent = $A.get('e.c:Project_Event');
                        appEvent.setParams({
                            type : 'center',
                            message:null
                        });
                        appEvent.fire();
                        component.set('v.step','2');
                }else{
                    component.set('v.step','3');
                }

            break;


            default:
                var step = parseInt(component.get('v.step'))+1;
                 component.set('v.step',step.toString());
            break;
        }


    },
    openSearchAddress:function(component,event,helper){
        component.set('v.address_display',true);
    },
    openSearchName:function(component,event,helper){
        component.set('v.name_display',true);
    },
    addressSelectedChange:function(component,event,helper){
        var selected = component.get('v.address_selected');
        console.log(selected);
        component.set('v.project.City__c',selected.locality);
        component.set('v.project.Country__c',selected.country);
        component.set('v.project.CountryIsoCode__c',selected.country_iso);
        if(selected.street_number == undefined){
            component.set('v.project.Street__c',selected.route);
        }else{
            component.set('v.project.Street__c',selected.street_number+' '+selected.route);
        }
        component.set('v.project.Geolocation__Latitude__s',selected.location.lat);
        component.set('v.project.Geolocation__Longitude__s',selected.location.lng);
        component.set('v.project.Zip__c',selected.postal_code);

        helper.checkForProjectsAround(component)
        .then(projects => {
            component.set('v.projectList',projects);
        })
        .catch(err => {
            console.log('error');
            console.log(err);
        })
    },
    closeClickError:function(component,event,helper){
        console.log('click error');
        component.set('v.displayError',false);
        component.set('v.errorMessage','');
    },
    handleManualStepChanges:function(component,event,helper){
        event.stopPropagation();
        event.preventDefault();

        var currentStep = event.getSource().get('v.value');
        component.set('v.step',component.get('v.step'));
        switch(currentStep){
            case '1':
                component.set('v.step',currentStep);
                component.set('v.displayMap',false);
            break;

            case '2':
                if(component.get('v.projectList').length > 0){
                    component.set('v.step',currentStep);
                    component.set('v.displayMap',true);
                }
            break;

            case '3':
                if(component.get('v.project.City__c') != null){
                    component.set('v.step',currentStep);
                    component.set('v.displayMap',false);
                }

            break;
        }

    },
    handleSizeMapModal:function(component,event,helper){
        console.log('Map size Changed');
        component.set('v.modalSize',component.get('v.displayMap')?'large':'');
    },
    projectChange:function(component,event,helper){
        var validation = component.get('v.validation');
        var project = component.get('v.project');

        switch(component.get('v.step')){
            case '1':

                var results = [];//[project.City__c,project.Street__c,project.Zip__c,project.Country__c,project.CountryIsoCode__c];
                 validation['step1'] = results.filter(val => val == undefined || val == null || val == '').length == 0;

            break;

            case '2':

            break;

            case '3':

            break;
        }

         component.set('v.validation',validation);
    }
})