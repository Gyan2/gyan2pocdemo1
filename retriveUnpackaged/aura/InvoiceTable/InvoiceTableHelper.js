({
	convertFile : function(fileInfo) {
        let splitted = fileInfo.data.split(',');
        
        var binary = atob(splitted.length == 1 ? splitted[0]:splitted[1]);
        var buffer = new ArrayBuffer(binary.length);
        var view = new Uint8Array(buffer);
        for (var i = 0; i < binary.length; i++) {
            view[i] = binary.charCodeAt(i);
        }
        var file = new File([view],fileInfo.Name+".pdf", {type: "application/pdf;charset=utf-8"});
		window.saveAs(file);
	},
    downloadFile:function(component,fileInfo){
        var action = component.get('c.downloadFileFromSAPPO');
            action.setParams({
                fileId:fileInfo.Id
            });


        var that = this;
        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    },
    getFilesFromCase:function(component){
        var action = component.get('c.getFilesFromCase');
            action.setParams({
                recordId:component.get('v.recordId')
            });


        var that = this;
        return new Promise(function(resolve, reject){
            action.setCallback(that, function(response) {
                var res = response.getReturnValue();
                 if(response.getState() === "SUCCESS") {
                    resolve(res);
                 }else{
                    reject(res);
                 }
            });
            $A.enqueueAction(action);
         })
    }
})