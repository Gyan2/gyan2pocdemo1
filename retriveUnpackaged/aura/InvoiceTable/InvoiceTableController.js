({
	onInit:function(component,event,helper){
	    helper.getFilesFromCase(component)
	    .then(val => {
                console.log(JSON.parse(val));
                component.set('v.files',JSON.parse(val));
         })

    },
	handleDownloadClick : function(component, event, helper) {
		console.log('handleDownloadClick');
        component.set('v.error',null);
        component.set('v.loading',true);
        
        var fileInfo = component.get('v.files').find(val => val.Id == event.getSource().get('v.value'));

        // Check if there is an ID
        if(fileInfo.Id == undefined){
            component.set('v.error',"Id is null, we can't fetch the invoice from SAP");
            component.set('v.loading',false);
            return;
        }

        helper.downloadFile(component,fileInfo)
        .then(val => {
            if(val.status == 200){
                var data = JSON.parse(val.data);
                // Set the name
                console.log(JSON.stringify(fileInfo));
                fileInfo.Name = data.DocumentName != null ? data.DocumentName : 'Invoice-'+fileInfo.Id+'-'+fileInfo.LastModifiedDate.split('T')[0];
                fileInfo.data = data.Payload;
                // Convert
                helper.convertFile(fileInfo);
                component.set('v.loading',false);
            }else{
                // Error
                console.log('Error from backend');
                console.log(val.message);
                component.set('v.error',val.message.split('|')[1]);
                component.set('v.loading',false);
            }

        })
	},
    afterScriptLoaded:function(component,event,helper){
        console.log('afterScriptLoaded');
        //window.hello();
    },
    closeClickError:function(component,event,helper){
        component.set('v.error',null);
    }
})