/**
 * Created by trakers on 02.07.18.
 */
({
    handleInit:function(component,event,helper){
        // Set unique ID
       component.set('v.id',Math.floor((Math.random() * 10000) + 1));
       component.set('v.valueToDisplay',helper.changeTextInput(component,component.get('v.value')));
       component.set('v.copy',component.get('v.value'));
    },
    handleValueChange:function(component,event,helper){

      if(component.get('v.value') != component.get('v.copy')){
          //console.log('handleValueChange');
          component.set('v.copy',component.get('v.value'));
          component.set('v.valueToDisplay',helper.changeTextInput(component,component.get('v.value')));
      }

    },
    dummyAction:function(component,event,helper){
        console.log('Dummy action');
    },
    onFocus:function(component,event,helper){
        //console.log('On Focus');
        component.set('v.mode','edit');


        window.setTimeout(function(){
            component.find('editInput').getElement().focus();
        },100)
    },
    onBlur:function(component,event,helper){
        //console.log('On blur');
        //console.log(component.get('v.copy'));
        component.set('v.mode','default');
        component.set('v.valueToDisplay',helper.changeTextInput(component,component.get('v.copy')));

    },
    onchange:function(component,event,helper){
        var value = parseFloat(event.currentTarget.value || 0);

        component.set('v.valueToDisplay',helper.changeTextInput(component,value));
        component.set('v.mode','default');
        component.set('v.copy',value);
        component.set('v.value',value);
        var chooseEvent = component.getEvent("eventOnchange");
            chooseEvent.fire();
            //console.log('onChange fired');
    },
    onkeyup:function(component,event,helper){

        if(helper.isNumber(event.currentTarget.value) && event.currentTarget.value != component.get('v.value')){
            var value = event.currentTarget.value;
            //console.log('On Key Up');
            switch(component.get('v.variant')){
                case 'currency':
                    value = component.get('v.max') != '' && value > component.get('v.max') ? component.get('v.max') : value;
                    value = component.get('v.min') != '' && value < component.get('v.min') ? component.get('v.min') : value;
                break;

                case 'percentage':
                    value = value > 100 ? 100 : value;
                    value = value < 0 ? 0 : value;
                break;
            }
            //console.log('value is: '+value);
            component.set('v.copy',value);
            component.set('v.value',value);
            var chooseEvent = component.getEvent("eventOnkeyup");
                chooseEvent.fire();
                //console.log('onKeyUp fired');
        }

    }
})