/**
 * Created by trakers on 02.07.18.
 */
({
    changeTextInput:function(component,value){
        var result = '';
        //console.log('changeTextInput - '+value);
        switch(component.get('v.variant')){
            case 'currency':
                result = component.get('v.currencyIsoCode')+' '+parseFloat(value).toFixed(2).replace(/\d(?=(\d{3})+\.)/g, "$&'");
            break;

            case 'percentage':
                result = value+' %';
            break;

            case 'quantity':
                result = value+' '+component.get('v.unitOfMeasure');
            break;

            default:
                result = ''+value;
            break;
        }
        //console.log('result: '+result);
        return result;
    },
    isNumber:function(param){
        return !isNaN(parseFloat(param)) && isFinite(param);
    },
})