(
    {
        handleInit : function(component, event, helper) {
            helper.updateDisplayStructure(component);
        },

        handleCreateClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'allowCreate', !button.get('v.selected'));
        },

        handleReadClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'allowRead', !button.get('v.selected'));
        },

        handleEditClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'allowEdit', !button.get('v.selected'));
        },

        handleDeleteClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'allowDelete', !button.get('v.selected'));
        },

        handleReadAllClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'viewAllRecords', !button.get('v.selected'));
        },

        handleEditAllClick : function(component, event, helper) {
            var button = event.getSource();
            var buttonValueSplit = button.get('v.value').split(':');
            helper._handleGenericClick(component, buttonValueSplit[0], buttonValueSplit[1], 'modifyAllRecords', !button.get('v.selected'));
        },
        
        handleSearch : function(component, event, helper) {
            var displayStructure = component.get('v.profilesDisplayStructure');
            var searchString = component.get('v.searchString').toLowerCase();
            for (var object of displayStructure.objects) {
                object.showMe = object.object.toLowerCase().indexOf(searchString)!=-1;
            }
            
            component.set('v.profilesDisplayStructure',displayStructure);
        }
    }
)