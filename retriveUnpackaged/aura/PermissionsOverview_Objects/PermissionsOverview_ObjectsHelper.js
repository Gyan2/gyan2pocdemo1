(
    {
        updateDisplayStructure : function(component) {
            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            displayStructure = {
                _profiles: {},
                profiles: [],
                _objects: {},
                objects: []
            };
            for (var profile of component.get('v.profilesInformationOrder')) {
                this._extractObjects(component, profilesInformation[profile], displayStructure);
            }

            // final profile fillup
            var profilesCount = displayStructure.profiles.length;
            for (var object of displayStructure.objects) {
                object.profiles[profilesCount-1] = object.profiles[profilesCount-1];
            }

            component.set('v.profilesDisplayStructure', displayStructure);
        },

        _extractObjects : function(component, profileInfo, displayStructure) {

            var fullName = profileInfo.fullName;
            var profileIndex = displayStructure._profiles[fullName];

            if (profileIndex === undefined) {
                profileIndex = Object.keys(displayStructure._profiles).length;
                displayStructure._profiles[fullName] = profileIndex;
                displayStructure.profiles.push(fullName);
            }
            var modifyAllData = (profileInfo.userPermissions.find(x=>x.name=='ModifyAllData')||{enabled:'false'}).enabled; // If the profile does not have access, its not included.
            var viewAllData = (profileInfo.userPermissions.find(x=>x.name=='ViewAllData')||{enabled:'false'}).enabled;
			var searchString = component.get('v.searchString').toLowerCase();
            for (var objectVisibility of profileInfo.objectPermissions) {
                var objectIndex = displayStructure._objects[objectVisibility.object];
                var newObject = false;
                if (objectIndex === undefined) {// if its a new object, we need to fill for all profiles empty
                    newObject = true;
                    objectIndex = Object.keys(displayStructure._objects).length;
                    displayStructure._objects[objectVisibility.object] = objectIndex;
                }

                if (newObject) {
                    displayStructure.objects[objectIndex] = {
                        object:objectVisibility.object,
                    	showMe : objectVisibility.object.toLowerCase().indexOf(searchString)!=-1,
                        profiles:[]
                    } ;
                }

                displayStructure.objects[objectIndex].profiles[profileIndex] = {
                    allowCreate: objectVisibility.allowCreate,
                    allowDelete: objectVisibility.allowDelete,
                    allowEdit: objectVisibility.allowEdit,
                    allowRead: objectVisibility.allowRead,
                    modifyAllRecords: objectVisibility.modifyAllRecords,
                    viewAllRecords: objectVisibility.viewAllRecords,
                    profile: fullName,
                    viewAllData : viewAllData,
                    modifyAllData : modifyAllData
                };
            }
        },

        _handleGenericClick : function(component, profileName, objectName, checkboxName, setTo /*true:false*/) {
            // it lists, per checkbox, which other checkboxes need to be set to true/false
            var actionsMapping = {
                allowCreate : {         falseToTrue : ['allowRead'],
                                        trueToFalse : [] },
                allowRead : {           falseToTrue : [],
                                        trueToFalse : ['allowCreate', 'allowEdit','allowDelete','viewAllRecords','modifyAllRecords'] },
                allowEdit : {           falseToTrue : ['allowRead'],
                                        trueToFalse : ['allowDelete','modifyAllRecords'] },
                allowDelete : {         falseToTrue : ['allowRead','allowEdit'],
                                        trueToFalse : ['modifyAllRecords'] },
                viewAllRecords : {      falseToTrue : ['allowRead'],
                                        trueToFalse : ['modifyAllRecords'] },
                modifyAllRecords : {    falseToTrue : ['allowRead', 'allowEdit','allowDelete','viewAllRecords'],
                                        trueToFalse : []}
            };

            var profilesInformation = component.get('v.profilesInformation');
            var displayStructure = component.get('v.profilesDisplayStructure');

            var objectIndex = displayStructure._objects[objectName];
            var profileIndex = displayStructure._profiles[profileName];

            var profileObject = profilesInformation[profileName].objectPermissions.find(x=>x.object==objectName);
            var displayObject = displayStructure.objects[objectIndex].profiles[profileIndex];

            // set current default object
            var checkboxList = [checkboxName].concat(actionsMapping[checkboxName][setTo?'falseToTrue':'trueToFalse']);
            var valueToSet = setTo?'true':'false';

            for (var checkbox of checkboxList) {
                displayObject[checkbox] = profileObject[checkbox] = valueToSet;
            }

            profileObject._hasChanged=true;

            component.set('v.profilesDisplayStructure',displayStructure);



        }
    }
)