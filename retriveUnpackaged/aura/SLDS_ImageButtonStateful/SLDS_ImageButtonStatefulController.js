({
	handleClick : function(component, event, helper) {
        component.set('v.selected', !component.get('v.selected'));
    }
})