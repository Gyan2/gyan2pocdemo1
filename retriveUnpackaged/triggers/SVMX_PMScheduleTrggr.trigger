/*******************************************************************************************************
Description: Trigger on PM Schedule

Dependancy: 
 
Author: Ranjitha S
Date: 19-03-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

trigger SVMX_PMScheduleTrggr on SVMXC__PM_Schedule__c  (before insert, before update, after update, after insert) {

        new SVMX_PMScheduleTrggrHandler().run();
}