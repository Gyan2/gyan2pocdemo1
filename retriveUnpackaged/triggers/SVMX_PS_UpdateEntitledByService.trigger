trigger SVMX_PS_UpdateEntitledByService on SVMXC__Entitlement_History__c (before insert) {
    new SVMX_PS_EntitledByServiceTrggrHandler().run();

}