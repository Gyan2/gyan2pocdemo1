/**
 * Created by rebmangu on 12/01/2018.
 */

trigger Quote on Quote (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    CoreSObjectDomain.triggerHandler(Quotes.class);
}