/*******************************************************************************************************
Description: Trigger on Event

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls.cls
    Trigger Handler: SVMX_EventTrggrHandler.cls
   
Author: Nivedita S
Date: 12-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

trigger SVMX_EventTrggr on Event (before insert, before update, after update, after insert,before delete) {
  new SVMX_EventTrggrHandler().run();

}