trigger ProductPortfolio on ProductPortfolio__c (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
	CoreSObjectDomain.triggerHandler(ProductPortfolios.class);
}