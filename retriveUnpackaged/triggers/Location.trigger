trigger Location on Location__c (after delete, after insert, after update, before delete, before insert, before update, after undelete) {
    CoreSObjectDomain.triggerHandler(Locations.class);
}