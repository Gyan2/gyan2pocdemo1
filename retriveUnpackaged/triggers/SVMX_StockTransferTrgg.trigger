trigger SVMX_StockTransferTrgg on SVMXC__Stock_Transfer__c (before insert, before update, after update, after insert) {

new SVMX_StockTransferTrggrHandler().run();
}