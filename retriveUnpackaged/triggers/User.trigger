/*******************************************************************************************************
* Trigger Name     	: User
* Description		: Adaptation of the Tony Scott Trigger Factory Pattern. (Refer to ITrigger.cls)
* Author          	: Paul Carmuciano
* Created On      	: 2017-07-10
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-07-10         1000                 Initial version
******************************************************************************************************/
trigger User on User (after delete, after insert, after update, before delete, before insert, before update) {
    TriggerFactory.createAndExecuteHandler(SM_TriggerUser.class);
}