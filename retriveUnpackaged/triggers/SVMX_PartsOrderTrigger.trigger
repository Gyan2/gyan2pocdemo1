trigger SVMX_PartsOrderTrigger on SVMXC__RMA_Shipment_Order__c (before insert, after insert, before update, after update) {

  new SVMX_PartsOrderTriggerHandler().run();
}