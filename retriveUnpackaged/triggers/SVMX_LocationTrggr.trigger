/*******************************************************************************************************
Description: Trigger on Location

Dependancy: 
  Trigger Framework: SVMX_TriggerHandler.cls
  Trigger Handler: SVMX_LocationTrggrHandler.cls, SVMXC_Sites.cls
  Test class: SVMX_LocationTrggrHandler_UT.cls
 
Author: Tulsi B R
Date: 14-11-2017

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

trigger SVMX_LocationTrggr on SVMXC__Site__c (after delete, after insert, after update, before delete, before insert, before update, after undelete) {
    if (Trigger.isInsert || Trigger.isUpdate) {
        new SVMX_LocationTrggrHandler().run();
    }
    CoreSObjectDomain.triggerHandler(SVMXC_Sites.class);
}