/**
 * Created by alcocese on 12-Apr-18.
 */

trigger Opportunity on Opportunity (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    CoreSObjectDomain.triggerHandler(Opportunities.class);
//    if (Trigger.isAfter && !Trigger.isDelete) {
//        Set<Project__c> projectToUpdate = new Set<Project__c>();
//
//        if (Trigger.isUpdate) {
//            for (Opportunity forOpp : Trigger.new) {
//                Opportunity oldOpp = Trigger.oldMap.get(forOpp.Id);
//                if (forOpp.ProjectRef__c != oldOpp.ProjectRef__c || forOpp.Amount != oldOpp.Amount) {
//                    projectToUpdate.add(new Project__c(Id=forOpp.ProjectRef__c));
//                    projectToUpdate.add(new Project__c(Id=oldOpp.ProjectRef__c));
//                }
//            }
//        } else {
//            for (Opportunity forOpp : Trigger.new) {
//                projectToUpdate.add(new Project__c(Id=forOpp.ProjectRef__c));
//            }
//        }
//
//        projectToUpdate.remove(new Project__c(Id=null));
//        //Projects.calculateRollUps = true;
//        update new List<Project__c>(projectToUpdate);
//    }
}