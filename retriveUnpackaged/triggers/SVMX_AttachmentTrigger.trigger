/*******************************************************************************************************
Description: Trigger on Attachment

Dependancy: 
    Trigger Framework: 
    Trigger Handler: 
    Test class: 
 
Author: Yogesh
Date: 

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

trigger SVMX_AttachmentTrigger on Attachment (before insert, before update, after update, after insert) {

        new SVMX_AttachmentTriggerHandler().run();
}