/**
 * Created by rebmangu on 12/01/2018.
 */

trigger Product on Product2 (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    CoreSObjectDomain.triggerHandler(Products.class);
}