/*******************************************************************************************************
Description: 

Dependancy: 
  Trigger Framework: 
  Trigger Handler: SVMX_CaseTriggerHandler
  Test class: 
 
Author: Tulsi B R
Date: 17-11-2018

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

trigger SVMX_CaseTrigger on Case (before insert, before update, after update, after insert) {
 
  new SVMX_CaseTriggerHandler().run();
}