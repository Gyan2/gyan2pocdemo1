trigger Attachment on Attachment (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    CoreSObjectDomain.triggerHandler(Attachments.class);
}