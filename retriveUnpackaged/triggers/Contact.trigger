/**
 * Created by alcocese on 17-Apr-18.
 */

trigger Contact on Contact (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    CoreSObjectDomain.triggerHandler(Contacts.class);
}