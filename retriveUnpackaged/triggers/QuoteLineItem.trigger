/**
 * Created by trakers on 19.07.18.
 */

trigger QuoteLineItem on QuoteLineItem (before insert, before update, before delete, after insert, after update, after delete, after undelete) {
    CoreSObjectDomain.triggerHandler(QuoteLineItems.class);
}