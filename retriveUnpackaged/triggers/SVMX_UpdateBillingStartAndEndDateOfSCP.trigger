/*******************************************************************************************************
Description: 

Dependancy: 
  Trigger Framework: 
  Trigger Handler: SVMX_ServiceContractProductsTriggerHandler
  Test class: 
 
Author: Tulsi B R
Date: 19-01-2018

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

****************************************************************************************************** */
trigger SVMX_UpdateBillingStartAndEndDateOfSCP on SVMXC__Service_Contract_Products__c (before insert, before update, after update, after insert) {
  new SVMX_ServiceContractProductsTrigHandler().run();
}