/*******************************************************************************************************
Description: 

Dependancy: 
  Trigger Framework: 
  Trigger Handler: SVMX_ServiceContractProductsTriggerHandler
  Test class: 
 
Author: Tulsi B R
Date: 19-01-2018

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

****************************************************************************************************** */


trigger SVMX_UpdateBillingStartAndEndDateOfSCS on SVMXC__Service_Contract_Sites__c (before insert, before update, after update, after insert) {
  new SVMX_ServiceContractSiteTrigHandler().run();
}