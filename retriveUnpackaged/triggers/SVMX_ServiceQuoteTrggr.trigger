/*******************************************************************************************************
Description: Trigger on Service Quote

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger Handler: SVMX_ServiceQuoteTrggrHandler.cls
    Test class: SVMX_ServiceQuoteTrggrHandler_UT.cls
 
Author: Ranjitha S
Date: 16-11-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

trigger SVMX_ServiceQuoteTrggr on SVMXC__Quote__c (before insert, before update, after update, after insert) {

        new SVMX_ServiceQuoteTrggrHandler().run();
}