/*******************************************************************************************************
Description: Trigger on Work Order

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger Handler: SVMX_WorkOrderTrggrHandler.cls
    Test class: SVMX_WorkOrderTrggrHandler_UT.cls
 
Author: Ranjitha S
Date: 16-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

trigger SVMX_WorkOrderTrggr on SVMXC__Service_Order__c (before insert, before update, after update, after insert) {

        new SVMX_WorkOrderTrggrHandler().run();
}