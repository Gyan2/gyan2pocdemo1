/*******************************************************************************************************
Description: Trigger on Quote Item

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger Handler: SVMX_QuoteItemTrgHandler.cls
    Test class: SVMX_QuoteItemTrgHandler_UT.cls
 
Author: Keshava Prasad
Date: 04-06-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/
trigger SVMX_QuoteItemTrggr on SVMXC__Quote_Line__c (before insert,before update) {
    new SVMX_QuoteItemTrgHandler().run() ;
}