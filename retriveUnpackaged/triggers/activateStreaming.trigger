trigger activateStreaming on Case (before update) {
    for(integer index = 0; index < Trigger.new.size(); index++) {
        if( Trigger.new[index].Real_Owner__c != Trigger.old[index].Real_Owner__c || Trigger.new[index].Contract_Type__c != Trigger.old[index].Contract_Type__c || Trigger.new[index].Client_Location__c != Trigger.old[index].Client_Location__c ) {
            System.debug('inside trigger');
            if(Trigger.new[index].Activate_Streaming_Api__c) {
                Trigger.new[index].Activate_Streaming_Api__c = false;
            }
            else {
                Trigger.new[index].Activate_Streaming_Api__c = true;
            }
        }
    }

}