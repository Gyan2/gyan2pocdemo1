/*******************************************************************************************************
Description: Trigger on Installed Product

Dependancy: 
 
Author: Ranjitha S
Date: 29-03-2018

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/
trigger SVMX_InstalledProductTrggr on SVMXC__Installed_Product__c (before insert, before update, after update, after insert) {

    new SVMX_InstalledProductTrggrHandler().run();
}