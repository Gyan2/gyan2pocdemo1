trigger SVMX_StockTransferlineTrgg on SVMXC__Stock_Transfer_Line__c (before insert, before update, after update, after insert) {
new SVMX_StockTransferLineTrggrHandler().run();
}