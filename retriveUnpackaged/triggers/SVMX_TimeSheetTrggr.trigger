/*******************************************************************************************************
Description: Trigger on TimeSheet

Dependancy: 
  
 
Author: Uzma K
Date: 15-12-2017

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/



trigger SVMX_TimeSheetTrggr on SVMXC__Timesheet__c (before insert, before update, after update, after insert) {
      new SVMXC_TimesheetTriggerHandler().run();

}