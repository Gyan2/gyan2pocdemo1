trigger TEMP_TEST_Account on Account (before insert, before update) {
    for (Account forAccount : trigger.new) { 
        if (forAccount.SVMX_PS_Send_to_SAP__c == true) {
            forAccount.SVMX_PS_Send_to_SAP__c = false;
            if (Trigger.isInsert) {
                forAccount.SVMX_PS_Send_to_SAP__c.addError('You cannot send to SAP on Insert');
            } else {
                if (String.isBlank(forAccount.SAPNumber__c)) {
                    if (String.isBlank(forAccount.Language__c)) {
                        forAccount.Language__c.addError('Required in SAP');
                    }
                    if (String.isBlank(forAccount.BillingStreet)) {
                        forAccount.BillingStreet.addError('Required in SAP');
                    }
                    if (String.isBlank(forAccount.BillingCity)) {
                        forAccount.BillingCity.addError('Required in SAP');
                    }
                    if (String.isBlank(forAccount.BillingPostalCode)) {
                        forAccount.BillingPostalCode.addError('Required in SAP');
                    }
                    if (String.isBlank(forAccount.BillingCountryCode)) {
                        forAccount.BillingCountryCode.addError(('Required in SAP'));
                    }

                    // It will not make the async call if any error is added, so not a problem
                    SAPPO_CustomerService.futureSyncAccount(forAccount.Id);
                } else {
                    forAccount.SVMX_PS_Send_to_SAP__c.addError('You cannot resend to SAP');
                }
            }
        }
        if (Util.RecordTypes.get('Account:CRM').Id == forAccount.RecordTypeId && String.isNotBlank(forAccount.ExternalId__c)) {
            forAccount.RecordTypeId = Util.recordTypes.get('Account:SOLDTO_ERP').Id;
        }
    }
}