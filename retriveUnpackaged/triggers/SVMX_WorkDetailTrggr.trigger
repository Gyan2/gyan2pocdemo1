/*******************************************************************************************************
Description: Trigger on Work Detail

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls
    Trigger Handler: SVMX_WorkDetailTrggrHandler.cls
    Test class: SVMX_WorkDetailTrggrHandler_UT.cls
 
Author: Ranjitha S
Date: 19-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/
trigger SVMX_WorkDetailTrggr on SVMXC__Service_Order_Line__c (before insert, before update, after update, after insert,before delete) {

      new SVMX_WorkDetailTrggrHandler().run();

}