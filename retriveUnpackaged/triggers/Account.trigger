/*******************************************************************************************************
* Trigger Name     	: Account
* Description		: Adaptation of the Tony Scott Trigger Factory Pattern. (Refer to ITrigger.cls)
* Author          	: Paul Carmuciano
* Created On      	: 2017-07-10
* Modification Log	: 
* -----------------------------------------------------------------------------------------------------
* Developer            Date               Modification ID      Description
* -----------------------------------------------------------------------------------------------------
* Paul Carmuciano		2017-07-10         1000                 Initial version
******************************************************************************************************/
trigger Account on Account (after delete, after insert, after update, before delete, before insert, before update, after undelete) {
    CoreSObjectDomain.triggerHandler(Accounts.class);
}