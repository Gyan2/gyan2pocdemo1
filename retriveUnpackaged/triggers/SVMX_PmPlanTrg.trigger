/*******************************************************************************************************
Description: Trigger on Preventive Maintenance

Dependancy: 
  Trigger Framework: SVMX_TriggerHandler.cls
  Trigger Handler: SVMX_PmTriggerHandler.cls
  Test class: SVMX_PmTriggerHandler_UT.cls
 
Author: Pooja Singh
Date: 09-01-2018

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

trigger SVMX_PmPlanTrg on SVMXC__PM_Plan__c (before insert, before update, after update, after insert) {
    new SVMX_PmTriggerHandler().run();
}