trigger SVMX_DynamicPMConfigTrg on SVMX_Dynamic_PM_Configuration__c (after insert) {
    new SVMX_DynamicPMConfigTrgHandler().run();

}