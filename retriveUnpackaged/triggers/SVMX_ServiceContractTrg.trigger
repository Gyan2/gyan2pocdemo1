/************************************************************************************************************
Description: Trigger to update opportunity status when service contract is accepted or rejected
Dependency: SVMX_ServiceContractTrgHandler class
            SVMX_ServiceContractDataManager class
 
Author: Pooja Singh
Date: 18-10-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

****************************************************************************************************************/

trigger SVMX_ServiceContractTrg on SVMXC__Service_Contract__c (before insert, after insert, after Update, before Update) {
    new SVMX_ServiceContractTrgHandler().run();
}