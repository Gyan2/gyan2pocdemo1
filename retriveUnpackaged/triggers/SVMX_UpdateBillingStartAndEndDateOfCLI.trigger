/*******************************************************************************************************
Description: 

Dependancy: 
  Trigger Framework: 
  Trigger Handler: SVMX_ServiceContractProductsTriggerHandler
  Test class: 
 
Author: Tulsi B R
Date: 19-01-2018

Modification Log: 
Date      Author      Modification Comments
--------------------------------------------------------------

****************************************************************************************************** */
trigger SVMX_UpdateBillingStartAndEndDateOfCLI on SVMX_Contract_Line_Items__c (before insert, before update, after update, after insert, after delete) {
  new SVMX_ContractLineItemsTrigHandler().run();
}