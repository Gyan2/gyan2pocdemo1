/*******************************************************************************************************
Description: Trigger on Time Entry

Dependancy: 
    Trigger Framework: SVMX_TriggerHandler.cls.cls
    Trigger Handler: SVMX_EventTrggrHandler.cls
   
Author: Tulsi B R
Date: 12-12-2017

Modification Log: 
Date            Author          Modification Comments
--------------------------------------------------------------

*******************************************************************************************************/

trigger SVMX_TimeEntryTrggr on SVMXC__Timesheet_Entry__c (before insert, before update, after update, after insert,before delete) {
  new SVMX_TimeEntryTrggrHandler().run();

}