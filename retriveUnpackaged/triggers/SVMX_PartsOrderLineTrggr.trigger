trigger SVMX_PartsOrderLineTrggr on SVMXC__RMA_Shipment_Line__c (before insert, before update, after update, after insert) {

      new SVMX_PartsOrderLineTrggrHandler().run();
}