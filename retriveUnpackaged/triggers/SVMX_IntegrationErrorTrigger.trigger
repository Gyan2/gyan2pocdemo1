trigger SVMX_IntegrationErrorTrigger on SVMX_Integration_Error_Tracking__c (before insert, after insert, before update, after update) {

List<SVMX_Integration_Error_Tracking__c> resendErrorList = new List<SVMX_Integration_Error_Tracking__c>();

  if(Trigger.isBefore && Trigger.isUpdate) {
    
    for(SVMX_Integration_Error_Tracking__c intErr : Trigger.new) {

      if(Trigger.oldMap.get(intErr.Id).SVMX_Resend_to_SAP__c != intErr.SVMX_Resend_to_SAP__c
        && intErr.SVMX_Resend_to_SAP__c == true) {

        resendErrorList.add(intErr);
      }
    }

    if(!resendErrorList.isEmpty()) {

      SVMX_IntegrationErrorManager.sortSoapRequests(resendErrorList);
    }
  }

  if(Trigger.isAfter && Trigger.isInsert) {

    List<Messaging.SingleEmailMessage> mails =  new List<Messaging.SingleEmailMessage>();
    
    for(SVMX_Integration_Error_Tracking__c IntErrTrackn : Trigger.new){
        
        Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {'naveed.sharif@ge.com'};
        mail.setToAddresses(toAddresses);
        mail.setSubject('ServiceMax - Integration Error: ' + IntErrTrackn.SVMX_Error_Message__c);
        mail.setHtmlBody('An integration error has occurred in the Sales Order Interface. Integration Error <b> ' + IntErrTrackn.Id +' </b>has been created.<p>' +
       'To view the error <a href='+system.URL.getSalesforceBaseUrl().toExternalForm()+'/'+IntErrTrackn.Id+'>click here.</a>');
        
        mails.add(mail);
    }
    if(mails.size()>0){
        Messaging.sendEmail(mails);
    }
  }
}