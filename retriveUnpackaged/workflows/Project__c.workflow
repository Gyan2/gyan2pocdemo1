<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Project_SAPPO</fullName>
        <apiVersion>42.0</apiVersion>
        <endpointUrl>https://dormakaba.sfdc.tools</endpointUrl>
        <fields>City__c</fields>
        <fields>CountryIsoCode__c</fields>
        <fields>Id</fields>
        <fields>LanguageIsoCode__c</fields>
        <fields>Name</fields>
        <fields>SAPInstance__c</fields>
        <fields>SAPNumber__c</fields>
        <fields>Street__c</fields>
        <fields>Zip__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>pi.dormakaba-dev@dormakaba.com</integrationUser>
        <name>Project → SAPPO</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
</Workflow>
