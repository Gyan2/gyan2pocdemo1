<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Change_Contact_to_Active</fullName>
        <field>Status__c</field>
        <literalValue>Active</literalValue>
        <name>Change Contact to Active</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <outboundMessages>
        <fullName>Contact_SAPPO</fullName>
        <apiVersion>42.0</apiVersion>
        <endpointUrl>https://integration-dev-tls.mydormakaba.com:4436/XISOAPAdapter/MessageServlet?channel=:Salesforce_Global:ContactSOAPSender</endpointUrl>
        <fields>AcademicTitle__c</fields>
        <fields>AccountSAPInstanceAuto__c</fields>
        <fields>AccountSAPNumberAuto__c</fields>
        <fields>Email</fields>
        <fields>FirstName</fields>
        <fields>Id</fields>
        <fields>Language__c</fields>
        <fields>LastName</fields>
        <fields>MobilePhoneCountryCode__c</fields>
        <fields>MobilePhoneWithoutInternationalPrefix__c</fields>
        <fields>PhoneCountryCode__c</fields>
        <fields>PhoneWithoutInternationalPrefix__c</fields>
        <fields>SAPNumber__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>pi.dormakaba-dev@dormakaba.com</integrationUser>
        <name>Contact → SAPPO</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>GROUP %7C Send Contact</fullName>
        <actions>
            <name>Contact_SAPPO</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Sends an outbound Message to SAP PO when the account associated already has already an SAP NUMBER and either the record is created or any of the fields that are synchronized is changed.</description>
        <formula>AND(
   NOT( ISBLANK( Account.SAPNumber__c )),
   $User.OrgIdAuto__c = &apos;00D0D0000008dU3&apos;,
   OR(
    ISNEW(),
    ISCHANGED(Language__c),
    ISCHANGED(AcademicTitle__c),
    ISCHANGED(FirstName),
    ISCHANGED(LastName),
    ISCHANGED(Phone),
    ISCHANGED(MobilePhone),
    ISCHANGED(Email)
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>GROUP %7C Status Contact</fullName>
        <actions>
            <name>Change_Contact_to_Active</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.LastName</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>This rule triggers to set the Status for contacts to active on creation</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
