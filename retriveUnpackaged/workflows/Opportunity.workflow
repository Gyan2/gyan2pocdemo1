<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Active_opportunity_due_past_close_date</fullName>
        <description>Active opportunity due past close date</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>AMER_email_templates/Opportunity_past_due</template>
    </alerts>
    <rules>
        <fullName>AMER %7C US %7C Opportunity past due</fullName>
        <active>true</active>
        <description>Email notification to the user when the opportunity is past due.</description>
        <formula>AND(
  NOT(IsClosed),
  ISPICKVAL(Owner.UserCountry__c, &quot;US&quot;)
)</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Active_opportunity_due_past_close_date</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Opportunity.CloseDate</offsetFromField>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
</Workflow>
