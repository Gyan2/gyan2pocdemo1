<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AMER_US_Email_Notification_opportunity_team_member</fullName>
        <description>AMER | US | Email Notification (opportunity team member)</description>
        <protected>false</protected>
        <recipients>
            <field>UserId</field>
            <type>userLookup</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>AMER_email_templates/Opportunity_team_member_added</template>
    </alerts>
    <rules>
        <fullName>AMER %7C US %7C Add Opportunity team member</fullName>
        <actions>
            <name>AMER_US_Email_Notification_opportunity_team_member</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Email notification to the opportunity team member upon adding to an opportunity team</description>
        <formula>AND(User.FirstName != null,
 TEXT( $User.UserCountry__c ) = &quot;US&quot;
)</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
