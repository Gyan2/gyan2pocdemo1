<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Set_Expiry_Date_Contract</fullName>
        <field>ContractTerm</field>
        <formula>(YEAR( ContractExpiryDate__c)*12+MONTH(ContractExpiryDate__c))-
(YEAR(StartDate)*12+MONTH(StartDate))</formula>
        <name>Set Expiry Date Contract</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>GROUP %7C Expiry Date Contract</fullName>
        <actions>
            <name>Set_Expiry_Date_Contract</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contract.Status</field>
            <operation>equals</operation>
            <value>Activated</value>
        </criteriaItems>
        <description>Calculate and set duration in month depending on contract expiry date</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
