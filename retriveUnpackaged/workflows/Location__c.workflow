<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <outboundMessages>
        <fullName>Location_SAPPO</fullName>
        <apiVersion>42.0</apiVersion>
        <endpointUrl>https://integration-dev-tls.mydormakaba.com:4436/XISOAPAdapter/MessageServlet?channel=:Salesforce_Global:LocationSOAPSender</endpointUrl>
        <fields>Id</fields>
        <fields>Name</fields>
        <fields>SAPInstance__c</fields>
        <fields>SAPNumber__c</fields>
        <fields>SVMXC_City__c</fields>
        <fields>SVMXC_Email__c</fields>
        <fields>SVMXC_Street__c</fields>
        <fields>SVMXC_Zip__c</fields>
        <fields>SVMX_HouseNumber__c</fields>
        <fields>SVMX_LocationName2__c</fields>
        <fields>SVMX_LocationName3__c</fields>
        <fields>SVMX_LocationName4__c</fields>
        <fields>SVMX_POBoxCity__c</fields>
        <fields>SVMX_POBoxPostalCode__c</fields>
        <fields>SVMX_POBox__c</fields>
        <fields>SVMX_SalesChannel__c</fields>
        <fields>SVMX_TaxRegistrationNumber1__c</fields>
        <fields>SVMX_TaxRegistrationNumber2__c</fields>
        <fields>SVMX_VATRegistrationNumber__c</fields>
        <includeSessionId>false</includeSessionId>
        <integrationUser>sergio.alcocer-vazquez@dormakaba.com</integrationUser>
        <name>Location → SAPPO</name>
        <protected>false</protected>
        <useDeadLetterQueue>false</useDeadLetterQueue>
    </outboundMessages>
    <rules>
        <fullName>GROUP %7C Send Location</fullName>
        <actions>
            <name>Location_SAPPO</name>
            <type>OutboundMessage</type>
        </actions>
        <active>true</active>
        <description>Sends an outbound Message to SAP PO when the record has already an SAP NUMBER and any of the fields that are synchronized is changed.</description>
        <formula>AND (
  NOT(ISNULL(SAPNumber__c)),
  $User.OrgIdAuto__c = &apos;00D0D0000008dU3&apos;,
  OR(
    ISCHANGED(SVMXC_City__c),
    ISCHANGED(SVMXC_Email__c),
    ISCHANGED(SVMXC_Street__c),
    ISCHANGED(SVMXC_Zip__c),
    ISCHANGED(SVMX_HouseNumber__c),
    ISCHANGED(SVMX_LocationName2__c),
    ISCHANGED(SVMX_LocationName3__c),
    ISCHANGED(SVMX_LocationName4__c),
    ISCHANGED(SVMX_POBoxCity__c),
    ISCHANGED(SVMX_POBoxPostalCode__c),
    ISCHANGED(SVMX_POBox__c),
    ISCHANGED(SVMX_SalesChannel__c),
    ISCHANGED(SVMX_TaxRegistrationNumber1__c),
    ISCHANGED(SVMX_TaxRegistrationNumber2__c),
    ISCHANGED(SVMX_VATRegistrationNumber__c)
  )
)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
