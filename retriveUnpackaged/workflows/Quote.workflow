<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AVP_Andreas_D_notification</fullName>
        <description>AVP (Andreas D) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>andreas.dahms@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>AVP_Jason_P_notification</fullName>
        <description>AVP (Jason P) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>jason.patterson@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>BDM_Dominic_F_notification</fullName>
        <description>BDM (Dominic F) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>dominic.fonseka@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>BDM_Greg_P_notification</fullName>
        <description>BDM (Greg P) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>greg.paradis@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>BDM_Julie_C_notification</fullName>
        <description>BDM (Julie C) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>julie.collins@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>BDM_Kelly_R_notification</fullName>
        <description>BDM (Kelly R) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>kelly.ritter@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>BDM_Marc_E_notification</fullName>
        <description>BDM (Marc E) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>marc.ellis@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>BDM_Michael_B_notification</fullName>
        <description>BDM (Michael B) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>michael.baum@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>BDM_Paul_H_notification</fullName>
        <description>BDM (Paul H) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>paul.higdon@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>BDM_Tammy_M_notification</fullName>
        <description>BDM (Tammy M) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>tammy.mlak@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>BDM_Tom_M_notification</fullName>
        <description>BDM (Tom M) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>tom.modec@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>Quote_approval_notification</fullName>
        <ccEmails>sanket.desai@dormakaba.com</ccEmails>
        <ccEmails>nathan.patrick@dormakaba.com</ccEmails>
        <description>Quote approval notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Quote_discount_request_approved</template>
    </alerts>
    <alerts>
        <fullName>Quote_discount_rejection_notification</fullName>
        <ccEmails>sanket.desai@dormakaba.com</ccEmails>
        <ccEmails>nathan.patrick@dormakaba.com</ccEmails>
        <description>Quote discount rejection notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/Quote_discount_rejection_notification</template>
    </alerts>
    <alerts>
        <fullName>SRM_Brian_S_notification</fullName>
        <description>SRM (Brian S) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>brian.shrake@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>SRM_Joe_H_notification</fullName>
        <description>SRM (Joe H) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>joseph.hooper@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>SRM_John_F_notification</fullName>
        <description>SRM (John F) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>john.frazer@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>SRM_Marcello_G_notification</fullName>
        <description>SRM (Marcello G) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>marcello.gianna@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>SRM_Mark_H_notification</fullName>
        <description>SRM (Mark H) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>mark.hoger@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>SRM_Peter_L_notification</fullName>
        <description>SRM (Peter L) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>peter.lowenstein@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <alerts>
        <fullName>VP_Steve_D_notification</fullName>
        <description>VP (Steve D) notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>steve.dentinger@dormakaba.com</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Quote_request_notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Status_to_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Change Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Change Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Status_to_Review</fullName>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>Change Status to Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>In_Review</fullName>
        <description>Test</description>
        <field>Status</field>
        <literalValue>In Review</literalValue>
        <name>In Review</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
</Workflow>
